<?php

declare(strict_types = 1);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Event;

use App\Exception\ApiExceptionInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Description of ExceptionListener
 *
 * @author avill
 */
class ExceptionListener
{
    private $logger;
 
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
 
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (!$event->getException() instanceof ApiExceptionInterface) {
            return;
        }
 
        $response = new Response($event->getException()->getMessage(), $event->getException()->getStatusCode());
 
        $event->setResponse($response);
 
        $this->log($event->getException());
    }
 
    private function log(ApiExceptionInterface $exception)
    {
//        $log = [
//            'code' => $exception->getStatusCode(),
//            'message' => $exception->getMessage(),
//            'called' => [
//                'file' => $exception->getTrace()[0]['file'],
//                'line' => $exception->getTrace()[0]['line'],
//            ],
//            'occurred' => [
//                'file' => $exception->getFile(),
//                'line' => $exception->getLine(),
//            ],
//        ];
// 
//        if ($exception->getPrevious() instanceof Exception) {
//            $log += [
//                'previous' => [
//                    'message' => $exception->getPrevious()->getMessage(),
//                    'exception' => get_class($exception->getPrevious()),
//                    'file' => $exception->getPrevious()->getFile(),
//                    'line' => $exception->getPrevious()->getLine(),
//                ],
//            ];
//        }
        file_put_contents('testArthur2.log', $exception);
//        var_dump($exception);
//        $this->logger->error(json_encode($log));
    }
}