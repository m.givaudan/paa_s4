<?php

namespace App\PaaBundle\Component\EventListener;

use Symfony\Component\HttpFoundation\Response;
//
//use Symfony\Component\HttpKernel\Event\ExceptionEvent; [à utiliser  à la place GetResponseForExceptionEvent; pour les version supérieur de Symfony (exemple Symfony 4.4 et Symfony 5.1)]
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use App\PaaBundle\Component\MyLogger;
use App\PaaBundle\Component\Logs;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Formatter\LineFormatter;

use Symfony\Component\HttpKernel\KernelInterface;

class ExceptionListener {
    /**
     * @var string
     */
    private $project_dir;

	public function __construct($project_dir) {
		$this->project_dir = $project_dir ;
	}
	public function onKernelException(GetResponseForExceptionEvent $event) {
// remplacer GetRepsonseForExceptionEvent par ExceptionEvent pour les version supérieur de Symfony (exemple Symfony 4.4 et Symfony 5.1)
		if ($_ENV['APP_ENV'] === 'prod') {
//            You get the exception object from the received event
//            getThrowable(); à utiliser pour les version supérieur de Symfony (exemple Symfony 4.4 et Symfony 5.1)
//            $exception = $event->getThrowable();
            $exception = $event->getException();
            $message = sprintf(
                    'Error: %s with code: %s', $exception->getMessage(), $exception->getCode()
            );

//            Customize your response object to display the exception details
// LG 20200629 old            Logs::debug($message);
			Logs::setProjectDir($this->project_dir) ;
//			$stackTrace = $this->buildStackTrace($event->getException()->getTrace()) ;
			$stackTrace = $exception->getTraceAsString() ;
			$stackTrace = str_replace("#", chr(9) . "#", $stackTrace);
			Logs::setStackTrace($stackTrace) ;
			
            Logs::critical($message);
        }
    }

//    protected function buildStackTrace(array $paTrace) {
//		$stackTrace = "test" ;
//		return $stackTrace;
//    }

}
