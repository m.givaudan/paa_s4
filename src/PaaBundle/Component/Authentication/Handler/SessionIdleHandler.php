<?php

/* 
 * Selon https://stackoverflow.com/questions/18872721/how-to-log-users-off-automatically-after-a-period-of-inactivity
 */

namespace App\PaaBundle\Component\Authentication\Handler;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class SessionIdleHandler
{

    protected $session;
    protected $securityToken;
    protected $router;
    protected $maxIdleTime;

    public function __construct(SessionInterface $session, TokenStorageInterface $securityToken, AuthorizationChecker $securityContext, RouterInterface $router, $maxIdleTime = 0)
    {
		$this->session = $session;
        $this->securityToken = $securityToken;
        $this->securityContext = $securityContext;
        $this->router = $router;
        $this->maxIdleTime = $maxIdleTime;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
		
		if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

		if ($this->securityToken->getToken() === null) {
			// Pas de timeout si pas de token de sécurité
			return ; 
		}

		if (!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
			// Pas de timeout si l'user est anonyme ou Remembered
			return ; 
		}
		
		if ($this->maxIdleTime > 0) {

            $this->session->start();
            $lapse = time() - $this->session->getMetadataBag()->getLastUsed();
			
            if ($lapse > $this->maxIdleTime) {

                $this->securityToken->setToken(null);
                $this->session->getFlashBag()->set('info', 'Vous avez été déconnecté suite à l\'inactivité de votre session.');
				$URI = $event->getRequest()->getUri() ;
				$this->session->set('URLDemandée', $URI) ;

                // Change the route if you are not using FOSUserBundle.
                // $event->setResponse(new RedirectResponse($this->generateUrl('fos_user_security_login')));
            }
        }
    }

}