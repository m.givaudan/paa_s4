<?php

namespace App\PaaBundle\Component\Authentication\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
// LG 20200515 old	use Symfony\Component\Routing\Router;
use App\Service\RouterDecorator ;
use Doctrine\Bundle\DoctrineBundle\Registry As DoctrineRegistry ;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface {
	
	protected $router;
	protected $security;
	protected $doctrine;

// LG 20200515 old	public function __construct($caller, Router $router, DoctrineRegistry $doctrine) {
	public function __construct($caller, RouterDecorator $router, DoctrineRegistry $doctrine) {
		$security = $caller->get('security.authorization_checker') ;
		$this->router = $router;
		$this->security = $security;
		$this->doctrine = $doctrine;
	}
	
	public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
		// Il faut relancer InitConnexionPAA pour identifier le bon user
		$loUser = $token->getUser() ;
		$liUser = $loUser->getIidRes() ;
		// On ne peut pas se servir de la session de Symfony car elle n'est pas connue de la connexion Doctrine
		// cf applicationPAA\src\PaaBundle\Component\Connection\ConnectionWrapper.php
		// $session = $request->getSession();	
		// $session->set('userId', $liUser) ;
		$_SESSION["giUser"] = $liUser ;
		$loConn = $this->doctrine->getConnection();
		$loConn->InitConnexionPaa() ;

// Changer le mdp
// $user->setPassword($this->container->get('security.encoder_factory')->getEncoder($user)->encodePassword("your password in plain texte here", $user->getSalt()));
// php bin/console fos:user:change-password luc mdp

		if ($request->request->get('_authentificationParPopup')){
//			echo $toto;
			return new Response('OK');
		}
		
		if ($this->security->isGranted('ROLE_SUPER_ADMIN')){
			$response = new RedirectResponse($this->router->generate('paa_index'));			
// $response = new Response("onAuthenticationSuccess : l'URL est redirigée vers : " . $this->router->generate('paa_index'));
		} else {
			$session = $request->getSession();	
			$URLDemandée = $session->get('URLDemandée') ;
			if ($URLDemandée === null) {
				$URLDemandée = $this->router->generate("paa_index") ;
			}
			if (empty($URLDemandée)) {
				$response = new Response("Authentification réussie") ;
			} else {
				$response = new RedirectResponse($URLDemandée);
			}
			
// $response = new Response("onAuthenticationSuccess : l'URL demandée est : " . $session->get('URLDemandée') . "/" . $URLDemandée);
		}

// LG 20200513 début
// 		$session = $request->getSession()->set('DBName', '') ;
// LG 20201513 fin		
		
		return $response;
	}
	
}