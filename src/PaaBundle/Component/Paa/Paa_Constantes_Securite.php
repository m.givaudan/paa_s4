<?php

namespace App\PaaBundle\Component\Paa;

class Paa_Constantes_Securite {

	const eiDroits_ActivitésBase = 2 ^ 0;
	const eiDroits_Compétences = 2 ^ 1;
	const eiDroits_Equipements = 2 ^ 2;
	const eiDroits_Intervenants = 2 ^ 3;
	const eiDroits_IntervenantsAdresse = 2 ^ 4;
	const eiDroits_IntervenantsContrats = 2 ^ 5;
	const eiDroits_IntervenantsCompteurs = 2 ^ 6;
	const eiDroits_IntervenantsDroits = 2 ^ 7;
	const eiDroits_Groupes = 2 ^ 8;
	const eiDroits_Usagers = 2 ^ 9;
	const eiDroits_UsagersGroupes = 2 ^ 10;
	const eiDroits_UsagersRéférents = 2 ^ 11;
	const eiDroits_HorairesIntervenants = 2 ^ 12;
	const eiDroits_HorairesUsagersGroupes = 2 ^ 13;
	const eiDroits_HorairesEquipements = 2 ^ 14;
	const eiDroits_Activités = 2 ^ 15;
	const eiDroits_Séances = 2 ^ 16;
	const eiDroits_SéancesQuiConcernentIntervenant = 2 ^ 17;
	const eiDroits_CommentairesSéancesQuiConcernentIntervenant = 2 ^ 18;
	const eiDroits_Transports = 2 ^ 19;
	const eiDroits_Paramètres = 2 ^ 20;
	const eiDroits_Spécialités = 2 ^ 21;
	const eiDroits_Etablissements = 2 ^ 22;
	const eiDroits_SupprLigneCrééeParAutre = 2 ^ 23;
	const eiDroits_ModifSéanceActiCrééeParAutre = 2 ^ 24;
	const eiDroits_ModifCouleurActiBase = 2 ^ 25;
	const eiDroits_SesPropresParticipations = 2 ^ 26;
	const eiDroits_SesPropresHoraires = 2 ^ 27;
	const eiDroits_SesPropresHorairesRendreExceptionnels = 2 ^ 28;
	const eiDroits_DU_Arborescence = 2 ^ 29;
	const eiDroits_PasEncoreUtilisé = 2 ^ 30;
	// Droits prédéfinis
	const eiDroitsW_PredefAdministrateur = 2147483647; // 2^0 + 2^1 + ... + 2^31
	const eiDroitsR_PredefAdministrateur = self::eiDroitsW_PredefAdministrateur;
	const eiDroitsW_PredefAucun = 0;
	const eiDroitsR_PredefAucun = self::eiDroitsW_PredefAucun;
	const eiDroitsW_PredefConcepteur = self::eiDroits_HorairesIntervenants + self::eiDroits_HorairesUsagersGroupes + self::eiDroits_HorairesEquipements + self::eiDroits_Activités + self::eiDroits_Séances + self::eiDroits_ModifSéanceActiCrééeParAutre;
	const eiDroitsR_PredefConcepteur = self::eiDroitsW_PredefConcepteur + self::eiDroits_ActivitésBase + self::eiDroits_Compétences + self::eiDroits_Equipements + self::eiDroits_Intervenants + self::eiDroits_IntervenantsCompteurs + self::eiDroits_Groupes + self::eiDroits_Usagers + self::eiDroits_Paramètres;
	const eiDroitsW_PredefIntervenant = self::eiDroits_SéancesQuiConcernentIntervenant + self::eiDroits_CommentairesSéancesQuiConcernentIntervenant;
	const eiDroitsR_PredefIntervenant = self::eiDroitsW_PredefIntervenant + self::eiDroits_HorairesIntervenants + self::eiDroits_HorairesUsagersGroupes + self::eiDroits_HorairesEquipements + self::eiDroits_Activités + self::eiDroits_Séances + self::eiDroits_ActivitésBase + self::eiDroits_Compétences + self::eiDroits_Equipements + self::eiDroits_Intervenants + self::eiDroits_IntervenantsCompteurs + self::eiDroits_Groupes + self::eiDroits_Usagers + self::eiDroits_Paramètres;
	const eiDroitsW_PredefSecrétaire = self::eiDroits_HorairesIntervenants + self::eiDroits_HorairesUsagersGroupes + self::eiDroits_HorairesEquipements;
	const eiDroitsR_PredefSecrétaire = self::eiDroitsW_PredefSecrétaire + self::eiDroits_Activités + self::eiDroits_Séances + self::eiDroits_ActivitésBase + self::eiDroits_Compétences + self::eiDroits_Equipements + self::eiDroits_Intervenants + self::eiDroits_IntervenantsCompteurs + self::eiDroits_Groupes + self::eiDroits_Usagers + self::eiDroits_Paramètres;

}
