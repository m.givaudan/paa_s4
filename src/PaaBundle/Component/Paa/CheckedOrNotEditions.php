<?php

namespace App\PaaBundle\Component\Paa;

use App\Limoog\PartageBundle\Component\cParametresBDD;
use App\PaaBundle\Component\Paa\Paa_Constantes_Editions;

class CheckedOrNotEditions {

	private $oCnxn = null;
	private $BDD = null;
	private $constantePaa = null;
	private $jsonPredefini = null;

	public function setjsonPredefini($json) {
		$this->jsonPredefini = $json;
	}

	function __construct($poCnxn) {
		$this->oCnxn = $poCnxn;
		$this->BDD = new cParametresBDD($poCnxn);
		$this->constantePAA = new \ReflectionClass("App\PaaBundle\Component\Paa\Paa_Constantes_Editions");
	}

	public function getjsonPredefini() {  //Get de l'attribu privé jsonPredefini
		return $this->jsonPredefini;
	}

	public function IsCheckedOrNot(string $value) {

		if ($value == "true")
			return ('checked="checked"');
		else
			return "";
	}

	// Vérification de l'existance de l'attribu jsonPredefini
	// S'il existe, on récupère l'objet dans le json
	// Sinon on le réupère de la BDD
	public function getParamètre($psNomParamètre, $pvValeurDft, $psType) {
		if ($this->jsonPredefini != null) {
			// On est dans le cas d'un prédéfini : l'utiliser
			if (property_exists($this->jsonPredefini, $psNomParamètre)) {
				// Le prédéfini contient l'information sur la propriété
				return $this->jsonPredefini->$psNomParamètre;
			} else {
				// Le prédéfini NE contient PAS l'information sur la propriété
				// Anomalie qu'il faudrait logguer
				echo 'La propriété "' . $psNomParamètre . '" n\'existe pas dans l\'objet $this->jsonPredefini<br>';
			}
		}

		return $this->BDD->getParamètre($psNomParamètre, $pvValeurDft, $psType);
	}

// LG 20190626 old
//    public function getParamètre($psNomParamètre, $pvValeurDft, $psType){  
//// LG 20190626 old		if($this->jsonPredefini != null)){ 
//		if($this->jsonPredefini != null && property_exists($this->jsonPredefini, $psNomParamètre)){ 
////            var_dump($this->jsonPredefini->$psNomParamètre);
////            var_dump($psNomParamètre);
//            
//// LG 20190626 old            return $this->jsonPredefini[$psNomParamètre];
//            return $this->jsonPredefini->$psNomParamètre;
//        }
//        else{
//            return $this->BDD->getParamètre($psNomParamètre, $pvValeurDft, $psType);
//        }
//    }

	public function CreateInputCheckbox($psNomParamètre, $pvValeurDft, $psType, $name, $text) {
		//Création d'une checkbox en passant en paramètre ses valeurs, son nom et le texte associé
		$resultat = $this->getParamètre($psNomParamètre, $pvValeurDft, $psType);

//        if ($resultat == "true" || $resultat == "on") {
//            $retour = "<input type='hidden' value='off' name='" . $psNomParamètre . "'>";
//            $retour .= '<input type="checkbox" name="' . $psNomParamètre . '" id="' . $name . '"checked="checked">' . $text . '&nbsp;';
//            echo $retour;
//        } else {
//            $retour = "<input type='hidden' value='off' name='" . $psNomParamètre . "'>";
//            $retour .= '<input type="checkbox" name="' . $psNomParamètre . '" id="' . $name . '">' . $text . '&nbsp;';
//            echo $retour;
//        }
		$lbChecked = ($resultat == "true" || $resultat == "on");
		$lsChecked = $lbChecked ? 'checked="checked"' : "";
//echo $psNomParamètre . "='" . $resultat . "'<br>" ;
//echo $lsChecked . "'<br>" ;
		$retour = "<input type='hidden' value='off' name='" . $psNomParamètre . "'>";
		$retour .= '<input type="checkbox" name="' . $psNomParamètre . '" id="' . $name . '" ' . $lsChecked . '>'
				. '<label for="' . $name . '">' . $text . '&nbsp;</label>';
		echo $retour;
	}

	// -----------------------------------------------------------------------------------------------------
	// Créer les options d'une combobox (select) avec une listes de constantes et une liste de libellés
	// et déterminer laquelle est selected en se servait des paramètres
	// $psNomParamètre			: nom du paramètre qui détermine l'option sélectionnée
	// $pvValeurParamètreDft	: valeur par défat de ce paramètre
	// $psTypeParamètre			: type de ce paramètre
	// $psLstConstantValues		: liste des valeurs de la liste (e.g. "eiDo_Editions_TypeImprMutliple_Chrono,eiDo_Editions_TypeImprMutliple_AMPM")
	// $psLstLibellés			: liste des libellés de la liste (e.g. "Horaires chronologiques,Horaires Matin/Après-midi")
	public function CreateInputOption($psNomParamètre, $pvValeurParamètreDft, $psTypeParamètre, $psLstConstantValues, $psLstLibellés) {
		//Création d'une option dans un select avec le selected suivant l'option

		$laConstantValues = explode(",", $psLstConstantValues);
		$laLibellés = explode(",", $psLstLibellés);
		$lsParamètre = $this->getParamètre($psNomParamètre, $pvValeurParamètreDft, $psTypeParamètre);

		$nbCase = min(count($laLibellés), count($laConstantValues));
		for ($i = 0; $i < $nbCase; $i++) {
			// Pour chaque item de la liste
			$lsValue = $this->constantePAA->getConstant($laConstantValues[$i]);
			$retour = '<option value="' . $lsValue . '"';
			if ($lsParamètre == $lsValue) {
				// Cet item est sélectionné
				$retour .= ' selected="selected"';
			}
			$retour .= '>' . $laLibellés[$i] . '</option></br>';
			echo $retour;
		}
	}

//    public function CreateInputOption_OLD_LG_20190626($psNomParamètre, $pvValeurDft, $psType, $constant, $text) {
//        //Création d'une option dans un select avec le selected suivant l'option
//
//        $constanteExplode = explode(",", $constant);
//        $textExplode = explode(",", $text);
//        //$resultat = $this->getParamètre($psNomParamètre, $pvValeurDft, $psType);
//        $obj = array(
//            'id' => '5d11e487c2f3a',
//            'nom' => 'Thery ROBERT',
//            'node' => 'PlanningIndividuels_listing_ressources_semaines',
//            'json' => array(
//                'gbImprPlanInclActivité' => 'on',
//                'gbImprPlanInclAbsences' => 'off',
//                'gbImprPlanInclAbsencesStd' => 'on',
//                'gbImprPlanInclPr\u00e9sences' => 'on',
//                'gbLibell\u00e9ExclNomPlanType' => 'on',
//                'gbImprPlanningIndividuelUtiliseNomsCourts' => 'off',
//                'gbImprPlanningIndividuelInclRemarque' => 'on',
//                'gbPlanningsMultiplesInclD\u00e9butFin' => 'on',
//                'gbPlanningsMultiplesInclEffectif' => 'on',
//                'gbPlanningListingInclutBilansParAB' => 'off',
//                'gbPlanningListingInclutBilansParCommentaire' => 'off'
//                
//            )
//        );
//		
//        $resultat = convertToObject($obj);
//        $resultat = json_decode($resultat,true);
//		
//        $nbCase = count($textExplode);
//        for ($i = 0; $i != count($textExplode); $i++) {
//            $retour = '<option value="' . $this->constantePAA->getConstant($constanteExplode[$i]) . '"';
//            if ($resultat['json'][$i] == $this->constantePAA->getConstant($constanteExplode[$i])) {
//                $retour .= ' selected="selected"';
//            }
//            $retour .= '>' . $textExplode[$i] . '</option></br>';
//            echo $retour;
//        }
//    }

	public function CreateInputOptionTypeAffichage($psNomParamètre, $pvValeurDft, $psType, $NomPara) {

		$ValBdd = $this->oCnxn->prepare('select var_memo from paa.paa_para where variable = :NomPara');
		$ValBdd->bindParam(':NomPara', $NomPara);
		$ValBdd->execute();
		$a = $ValBdd->fetchAll();

		$text = "Horaires de jour,Horaires de nuit,24h/24,";
		$constante = "1,2,3,N,C,I";

		$tableauOption = explode("\r", $a[0]['var_memo']);
		$tableauOption[2] = str_replace(';', '', $tableauOption[2]);
		$stringText = implode(",", $tableauOption);
		$text .= $stringText;

		return $this->CreateInputOption($psNomParamètre, $pvValeurDft, $psType, $constante, $text);
	}

	function randomName() {
		echo $LG20190626_CetteFonctionEstObsolete;
		//fonction qui retourne un prénom et un nom random, 2500 possibilitées
		// Fonction utilié pour la création d'un favori
		$firstname = array(
			'Johnathon', 'Anthony', 'Erasmo', 'Raleigh', 'Nancie', 'Tama', 'Camellia', 'Augustine', 'Christeen', 'Luz', 'Diego', 'Lyndia', 'Thomas', 'Georgianna', 'Leigha', 'Alejandro', 'Marquis',
			'Joan', 'Stephania', 'Elroy', 'Zonia', 'Buffy', 'Sharie', 'Blythe', 'Gaylene', 'Elida', 'Randy', 'Margarete', 'Margarett', 'Dion', 'Tomi', 'Arden', 'Clora', 'Laine', 'Becki', 'Margherita',
			'Bong', 'Jeanice', 'Qiana', 'Lawanda', 'Rebecka', 'Maribel', 'Tami', 'Yuri', 'Michele', 'Rubi', 'Larisa', 'Lloyd', 'Tyisha', 'Samatha');

		$lastname = array(
			'Mischke', 'Serna', 'Pingree', 'Mcnaught', 'Pepper', 'Schildgen', 'Mongold', 'Wrona', 'Geddes', 'Lanz', 'Fetzer', 'Schroeder', 'Block', 'Mayoral', 'Fleishman', 'Roberie', 'Latson',
			'Lupo', 'Motsinger', 'Drews', 'Coby', 'Redner', 'Culton', 'Howe', 'Stoval', 'Michaud', 'Mote', 'Menjivar', 'Wiers', 'Paris', 'Grisby', 'Noren', 'Damron', 'Kazmierczak', 'Haslett',
			'Guillemette', 'Buresh', 'Center', 'Kucera', 'Catt', 'Badon', 'Grumbles', 'Antes', 'Byron', 'Volkman', 'Klemp', 'Pekar', 'Pecora', 'Schewe', 'Ramage');

		$name = $firstname[rand(0, count($firstname) - 1)];
		$name .= ' ';
		$name .= $lastname[rand(0, count($lastname) - 1)];

		return $name;
	}

	function CreateFavoris($psNomParamètre, $pvValeurDft, $psType) {

		$resultat = $this->getParamètre($psNomParamètre, $pvValeurDft, $psType);
		$resultat = json_decode($resultat);
		$tab = $resultat[0];
	}

	function CheckIfExist(array $arrayBdd, array $arrayPost) {
		echo $LG20190626_CetteFonctionEstObsolete;
		$nbCase = count($arrayBdd);
		for ($i = 0; $i != $nbCase; $i++) {
			if ($arrayBdd[$i]['node'] == $arrayPost['node']) {
				return $i;
			}
		}
		return null;
	}

	function CheckIfExistId(array $arrayBdd, $id) {
		echo $LG20190626_CetteFonctionEstObsolete;
		// Méthode de vérification si l'id existe dans le tableau json reçu en paramêtre
		$nbCase = count($arrayBdd);
		for ($i = 0; $i != $nbCase; $i++) {
			if ($arrayBdd[$i]['id'] == $id) {
				return $i;
			}
		}
		return null;
	}

	function CreateTabFav() {
		// Création des 
		$fav = $this->BDD->getParamètre("EditionFavoriJson", "", "MU");
		$arrayFav = json_decode($fav, true);

		$nbFav = count($arrayFav);

		for ($i = 0; $i != $nbFav; $i++) {
			echo '<a href="{{ asset(' . $arrayFav[$i]['node'] . ') }}" class="list-group-item list-group-item-action">' . $arrayFav[$i]['nom'] . '</a>';
		}
	}

	function CreateInputOptionTypeHoraire() {
		$getparametre = $this->BDD->getParamètre("gsImprPlanTypeAffichageJours", "N", "CU");
		$selectjour = '';
		$selectnuit = '';
		$select24 = '';
		$selectmatin = '';
		$selectmidi = '';
		$selectpm = '';
		($getparametre == "N" ? $selectjour = "selected='selected'" : ($getparametre == 'I' ? $selectnuit = "selected='selected'" : ($getparametre == 'C' ? $select24 = "selected='selected'" : ($getparametre == '8h00-12h00' ? $selectmatin = "selected='selected'" : ($getparametre == '12h00-14h00' ? $selectmidi = "selected='selected'" : ($getparametre == '14h00-18h00' ? $selectpm = "selected='selected'" : null))))));
		echo "<select> <option value='horaireJour'" . $selectjour . ">Horaires de jour</option>
                <option value='horaireNuit'" . $selectnuit . ">Horaires de nuit</option>
                <option value='24'" . $select24 . ">24h/24</option>
                <option value='matin'" . $selectmatin . ">Matin (8h00-12h00)</option>
                <option value='midi'" . $selectmidi . ">Midi (12h00-14h00)</option>
                <option value='PM'" . $selectpm . ">PM (14h00-18h00)</option> </select>";
	}

}
