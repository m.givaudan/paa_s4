<?php

// *********************************************************************************************
// Récupérer une instance de la classe cDataPAA_PG
function getORessources(&$poRessources, &$psErr) {
//	if (getOrReUseGlobalObject($lo, 'goDataPAA', 'cDataPAA_PG', $lsErr)) $poDataPAA = $lo ;
	// else $lbKO = True ;
	$o = new cGereRessources();
	return $o;
}

/**
 * cGereRessources
 * 
 * @package wwwPAA
 * @author 
 * @copyright 2017
 * @version $Id$
 * @access public
 */
class cGereRessources {

	protected $lIsCloning = false;
	protected $ibModified = false;
	public $cLastErreur = "";

	public function __construct() {
		$this->iTypeFiltrePrésences = isset($giFiltrePrésenceRessources) ? $giFiltrePrésenceRessources == true : $this->iTypeFiltrePrésences;
		$this->lAffiche_Remplacants = isset($gbListesAfficheRemplacants) ? $gbListesAfficheRemplacants == true : $this->lAffiche_Remplacants;
		$this->lAffiche_Extérieurs = isset($gbListesAfficheExtérieurs) ? $gbListesAfficheExtérieurs == true : $this->lAffiche_Extérieurs;
		$this->iSem = isset($giSem) ? $giSem : -1;
		$this->iSemFin = isset($giSem) ? $giSem : -1;
		$this->iEtablissement = isset($giEtablissement) ? $giEtablissement : -1;
	}

	public $cType_Res = "@Tous@";
	public $cLstResForcées = "";
	public $lAffiche_Remplacants = False;
	public $lAffiche_Extérieurs = False;
	public $lAffiche_Sortis = False;
	public $lAffiche_Présents = True;
	public $lAffiche_PasEntrés = False;
	public $lGroupePourPériodeDemandée = True;
	public $iService = -1;
	public $iSpécialité = -1;
	public $iIntervenant = -1;
	public $iGroupe = "";
	public $iParentDuSousGroupe = "";
	public $iActi_SsGpe = -1;   // Pour le type de ressources AB : que les AB de ce SGAB
	public $iTypeFiltrePrésences = 0;
	public $cIntervenant_Libre1 = '';
	public $cIntervenant_Libre2 = '';
	public $cWhereAdditionnel = '';
	public $cWhereAdditionnel_Interne = '';
	public $iActiBaseCompetence = null; // Pour n'avoir que les ressources compétentes dans cette activité de base
	public $iActiviteCompetence = null; // Pour n'avoir que les ressources compétentes dans l'activité de base de cette activité
	private $iEtablissement = 0;
	private $iEtablissement_Interne = 0;

	public function getEtablissement() {
		$liRetu = 0;
		If ($this->lIsCloning)
			$liRetu = $this->iEtablissement;
		else If (Empty($this->iEtablissement_Interne))
			$liRetu = $this->iEtablissement;
		Else
			$liRetu = $this->iEtablissement_Interne;
		Return ENvl($liRetu, 0);
	}

	public function setEtablissement($v) {
		If (Evl($v, $giEtablissement) == $giEtablissement)
			$v = 0;
		$this->ibModified = $this->ibModified or not($this->iEtablissement_Interne == $v);
		$this->iEtablissement_Interne = $v;
	}

	public function getDebut() {
		$lvDébut = $this->iSem;
//		$lvDébut = Iif(Dollar(Vartype($lvDébut), "NDT"), $lvDébut, GetNoSemaine($gvDateDébutAnnée_Lundi)) ;
		$lvDébut = Dollar(Vartype($lvDébut), "NDT") ? $lvDébut : GetNoSemaine($gvDateDébutAnnée_Lundi);
		$ldDebut = "ADéterminer";
		CvtSemDateHeure($lvDébut   // pvDateTime
				, null   // plFin
				, $toto	// piSemaine
				, $ldDebut   // pdDate
				, $toto   // ptTime
				, $toto   // pnHeure
				, false   // plTouteAnnéePourSemaineType
		);
		return $ldDebut;
	}

	public function getFin() {
		$lvFin = $this->iSemFin;
//		$lvFin = Iif(Dollar(Vartype($lvFin), "NDT"), $lvFin, GetNoSemaine($gvDateFinAnnée)) ;
		$lvFin = Dollar(Vartype($lvFin), "NDT") ? $lvFin : GetNoSemaine($gvDateFinAnnée);
		$ldFin = "ADéterminer";
		CvtSemDateHeure($lvFin   // pvDateTime
				, True   // plFin
				, $toto   // piSemaine
				, $ldFin   // pdDate
				, $toto   // ptTime
				, $toto   // pnHeure
				, false   // plTouteAnnéePourSemaineType
		);
		return $ldFin;
	}

	Function EstUsagers() {
		Return ($this->cType_Res == "U");
	}

	Function EstIntervenants() {
		Return ($this->cType_Res == "I");
	}

	Function EstEquipements() {
		Return ($this->cType_Res == "S");
	}

	Function EstGroupes() {
		Return in_Array($this->cType_Res, array("G", "SG"));
	}

	Function EstEtablissements() {
		Return ($this->cType_Res == "E");
	}

	Function EstGroupeActivitésDeBase() {
		Return ($this->cType_Res == "GA");
	}

	Function EstSousGroupeActivitésDeBase() {
		Return ($this->cType_Res == "SGA");
	}

	Function EstActivitésDeBase() {
		Return in_Array($this->cType_Res, array("A", "AB", "ABS"));
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------
	// tiTypeString		: 1 pour string de type PQSQL
	// Valeur de retour		:
	// LG 20140604
	Function ToString($tiTypeString) {

		$lcPGSQLString = "";
		if ($tiTypeString == 1) {
			// Chaine de type PGSQL
			$lcDébut = PG_ToLitteral($this->getDebut(), "Date");
			$lcFin = PG_ToLitteral($this->getFin(), "Date");
			$PG_ToLitteral = "PG_ToLitteral";
			$lcPGSQLString = <<<SQL
				Select ##cType_Res##{$this->cType_Res}##cType_Res##::Text As cType_Res
						, {$lcDébut} As iSem
						, {$lcFin} As iSemFin
						, {$PG_ToLitteral($this->lAffiche_Sortis, "Boolean")} As lAffiche_Sortis
						, {$PG_ToLitteral($this->lAffiche_Présents, "Boolean")} As lAffiche_Présents
						, {$PG_ToLitteral($this->lAffiche_PasEntrés, "Boolean")} As lAffiche_PasEntrés
						, {$PG_ToLitteral($this->lAffiche_Remplacants, "Boolean")} As lAffiche_Remplacants
						, {$PG_ToLitteral($this->lAffiche_Extérieurs, "Boolean")} As lAffiche_Extérieurs
						, {$PG_ToLitteral($this->lGroupePourPériodeDemandée, "Boolean")} As lGroupePourPériodeDemandée
						, {$this->iSpécialité}::Integer As iSpécialité
						, {$this->iService}::Integer As iService
						, {$this->getEtablissement()}::Integer As iEtablissement
						, {$this->iActi_SsGpe}::Integer As iActi_SsGpe
						, ##iGroupe##{$this->iGroupe}##iGroupe##::Text As iGroupe
						, ##iParentDuSousGroupe##{$this->iParentDuSousGroupe}##iParentDuSousGroupe##::Text As iParentDuSousGroupe
						, ##cIntervenant_Libre1##{$this->cIntervenant_Libre1}##cIntervenant_Libre1##::Text As cIntervenant_Libre1
						, ##cIntervenant_Libre2##{$this->cIntervenant_Libre2}##cIntervenant_Libre2##::Text As cIntervenant_Libre2
						, {$this->iActiBaseCompetence}::Integer As iActiBaseCompetence
						, {$this->iActiviteCompetence}::Integer As iActiviteCompetence
						, {$PG_ToLitteral($this->EstGroupes(), "Boolean")} As EstGroupes
						, {$PG_ToLitteral($this->EstUsagers(), "Boolean")} As EstUsagers
						, {$PG_ToLitteral($this->EstIntervenants(), "Boolean")} As EstIntervenants
						, {$PG_ToLitteral($this->EstEquipements(), "Boolean")} As EstEquipements
						, {$PG_ToLitteral($this->EstActivitésDeBase(), "Boolean")} As EstActivitésDeBase
						, {$PG_ToLitteral($this->EstSousGroupeActivitésDeBase(), "Boolean")} As EstSousGroupeActivitésDeBase
						, {$PG_ToLitteral($this->EstGroupeActivitésDeBase(), "Boolean")} As EstGroupeActivitésDeBase
						, ##cWhereAdditionnel##{$this->cWhereAdditionnel}##cWhereAdditionnel##::Text As cWhereAdditionnel
						, ##cWhereAdditionnel_Interne##{$this->cWhereAdditionnel_Interne}##cWhereAdditionnel_Interne##::Text As cWhereAdditionnel_Interne
						, ##cLstResForcées##{$this->cLstResForcées}##cLstResForcées##::Text As cLstResForcées
					;
SQL;

			$lcPGSQLString = Strtran($lcPGSQLString, "##", "$");
			$lcPGSQLString = Strtran($lcPGSQLString, "'", "''");
			$lcPGSQLString = Strtran($lcPGSQLString, '"', "''");
			$lcPGSQLString = utf8_decode($lcPGSQLString);
		} else {
//			&gsSetStepOn, "_4310IQLNN"
			LG_Die("La méthode " . __Class__ . ".ToString(" . $tiTypeString . ") n'est pas implémentée");
		}

		// Valeur de retour
		Return $lcPGSQLString;
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------
	// Renvoyer un curseur de liste de ressource (champ iId_Res, cNom_Res)
	// Valeur de retour		:
	// LG 20140604
	Function SauveCurseur(&$pcCurseur) {
		return "NonImplémenté";
	}

	Function restaureCurseurSauvé($psNomCurseur, &$pcCurseur) {
		return False;
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------
	Function fillCombo() {

		$lbKO = False;
		if (!$this->RtvCurRessources($loDataSet, 'cNomCourt')) {
			// Echec
			echo "Echec du remplissage : " . $this->cLastErreur . "<br>";
			$lbKO = True;
		} else {
			echo "<br>Liste des colonnes : <br>";
			for ($i = 0; $i < $loDataSet->columnCount(); $i++) {
				$col = $loDataSet->getColumnMeta($i);
				echo $col['name'] . ", ";
			}
			echo "<br>";

			echo "Liste des données : <br>";
			foreach ($loDataSet as $row) {
				echo "cRes = " . $row['ctype_res'] . $row['iid_res'] . ", cNomCourt = " . $row['cnomcourt'] . "<br>";
			}
		}


		return !$lbKO;
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------
	// Renvoyer un curseur de liste de ressource (champ iId_Res, cNom_Res)
	// Valeur de retour		:
	// LG 20140604
	Function rtvJSONRessources(&$pcJSON
	, $pcLstFlds
	/* , $psTypeRes
	  , $piSem
	  , $piSemFin
	  , $psWhere
	  , $pbSansFiltrePrésence
	  , $piActiBaseFiltre */
	) {

		if (!$this->RtvCurRessources($loDataSet, $pcLstFlds)) {
			// Echec
//			echo "Echec du remplissage : " . $this->cLastErreur . "<br>" ;
			$lbKO = True;
		} else {
			$res = $loDataSet->fetchAll();
			$pcJSON = json_encode($res);
		}
		return !$lbKO;
	}

	/**
	 * cGereRessources::rtvCurRessources()
	 * Renvoyer un curseur de liste de ressource (champ cType_Res, iId_Res, cNomCourt)
	 * 
	 * @param mixed $(pcLstFlds)			: Liste des champs à inclure, en plus de cType_Res et iId_Res (cNom, cNomCourt, cTri, ...)
	 * @return								: true si succès
	 */
	Function rtvSQLRessources($pcLstFlds = "") {

		if (!getODataPAA($loData, $lcMsgErr)) {
			$lsSQL = "Impossible de récupérer l'objet oData : " . $lcMsgErr;
		} else {
			$lsSQL = $loData->RtvSQLRessourcesFiltréeParGèreFiltre($this	// poGèreFiltre as cGereFiltre OF "c:\luc\projets vb et foxpro\PAA45 spécifiques\progs\ressources_class.prg"
					, $pcLstFlds  // pbInclChampsData
					);
		}

		return $lsSQL;
	}

	/**
	 * cGereRessources::rtvCurRessources()
	 * Renvoyer un curseur de liste de ressource (champ cType_Res, iId_Res, cNomCourt)
	 * 
	 * @param mixed $pcCurseur				: retour d'exécution de requete PDO'
	 * @param mixed $(pcLstFlds)			: Liste des champs à inclure, en plus de cType_Res et iId_Res (cNom, cNomCourt, cTri, ...)
	 * @return								: true si succès
	 */
	Function rtvCurRessources(&$pcCurseur
	, $pcLstFlds = ""
	) {

		if ($this->RestaureCurseurSauvé($this->cSavCurRessources, $pcCurseur)) {
			// Le curseur a été restauré à partir de la sauvegarde
			Return True;
		}

		$llKO = False;
		if (!getODataPAA($loData, $lcMsgErr)) {
			$this->cLastErreur = "Impossible de récupérer l'objet oData : " . $lcMsgErr;
			$llKO = True;
		} else If (!$loData->RtvCurRessourcesFiltréeParGèreFiltre($pcCurseur  // pcCurseur
						, $this	// poGèreFiltre as cGereFiltre OF "c:\luc\projets vb et foxpro\PAA45 spécifiques\progs\ressources_class.prg"
						, $pcLstFlds  // pbInclChampsData
				)) {
			$llKO = True;
			$this->cLastErreur = "Impossible de récupérer le curseur des ressources : " . $loData->cLastErreur;
		}

		// Mémoriser le curseur pour réutilisation ultérieure
		$this->cSavCurRessources = $this->SauveCurseur($pcCurseur);
		$this->ibModified = False;

		// Valeur de retour
		Return !$llKO;
	}

	//AV 25/03/2019 Début
	//Ajout d'une fonction afin de récuperer les éléments du treeview
	Function getActiArthur(&$pcCurseur, $poConnexion) {
		getODataPAA($loData, $lcMsgErr, $poConnexion);
		$loData->RessourcesActiArthur($pcCurseur, $poConnexion);

		$var = $pcCurseur->fetchAll();
		return $var;
	}

	//AV 25/03/2019 Fin
	//AV 27/05/2019 Début
	Function getActiGabSgabArthur(&$pcCurseur, $poConnexion) {
		getODataPAA($loData, $lcMsgErr, $poConnexion);
		$loData->RessourceActiGabSgabArthur($pcCurseur, $poConnexion);

		$var = $pcCurseur->fetchAll();
		return $var;
	}

	//AV 27/05/2019 Fin
	//AV 24/06/2019 Début
	Function getUsersArthur(&$pcCurseur, $poConnexion, $piIdRes) {
		getODataPAA($loData, $lcMsgErr, $poConnexion);
		$loData->RessourceUsersAthur($pcCurseur, $poConnexion, $piIdRes);

		$var = $pcCurseur->fetchAll();
		return $var;
	}

	//AV 27/05/2019 Fin

	Function InsertLogArthur(&$pcCurseur, $poConnexion, $piErrNo, $psErrStr, $psErrFile, $psErrVersion, $psErrDetail) {
		getODataPAA($loData, $lcMsgErr, $poConnexion);
		$loData->RessourceLogAthur($pcCurseur, $poConnexion, $piErrNo, $psErrStr, $psErrFile, $psErrVersion, $psErrDetail);

		$var = $pcCurseur->fetch();
		return $var;
	}

}
