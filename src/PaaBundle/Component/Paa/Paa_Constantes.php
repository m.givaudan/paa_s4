<?php

namespace App\PaaBundle\Component\Paa;

define("eiSemaineSuivie", 0);
define("eiSemaineInexistante", -100);
//LP 02/09/2020 debut
Define ("eiCompteurType_Heures",1);
Define ("eiCompteurType_Points",2);
Define ("eiCompteurType_Jours",3);
//LP 02/09/2020 fin
//MC 20200907 DEBUT
Define("eiCompteurRègle_WNuit",1);
Define("eiCompteurRègle_WJourFérié",2);
Define("eiCompteurRègle_WDimanche",3);
Define("eiCompteurRègle_Absence",4);
Define("eiCompteurRègle_Présence",5);
Define("eiCompteurRègle_Contrat",6);
Define("eiCompteurRègle_Travail",7);
Define("eiCompteurRègle_Activité",8);
Define("eiCompteurRègle_DuréeMiniRepos",9);
Define("eiCompteurRègle_DuréeMiniReposNonDérogeable",10);

Define("eiCompteurSens_Ajouter",1);
Define("eiCompteurSens_Retrancher",-1);
//MC 20200907 FIN
class Paa_Constantes {

	const eiActiSsGpeAbsenceIntervenant = -2;
	const eiActiSsGpeAbsenceSalle = -3;
	const eiActiSsGpeAbsenceEtablissement = -4;
	const eiActiSsGpePrésence = -5;
	const eiActiSsGpeAbsenceUsager = -6;
	const eiActiSsGpeAbsenceGroupe = -7;
	const eiActiSsGpeInexistante = -8;
	const eiActiSsGpeMatiere = -9;
	const eiActiSsGpeTournée = -11;
	const diFréquenceRépétitionSéance_UneFois = 0;
	const diFréquenceRépétitionSéance_Quotidien = 1;
	const diFréquenceRépétitionSéance_TousLes2Jours = 2;
	const diFréquenceRépétitionSéance_TousLes3Jours = 3;
	const diFréquenceRépétitionSéance_Hebdomadaire = 4;
	const diFréquenceRépétitionSéance_Quinzaine = 5;
	const diFréquenceRépétitionSéance_QuotidienSaufSDJF = 6;
	const diFréquenceRépétitionSéance_LundiJeudi = 7;
	const diFréquenceRépétitionSéance_MardiVendredi = 8;
        
}
