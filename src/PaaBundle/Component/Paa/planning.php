<?php

// http://localhost/PHP/ressources.php?cType_Res=@Tous@&dDebut=2000-01-01&dFin=2000-01-01
// cLstRes
// dDebut
// dFin
// cLstJoursAffichage
//	- '<Auto>' (dft)							: liste des jours qui ont des horaires non nuls
//	- '<Hier>'									: jour précédent le jour courant
//	- '<Ce jour>'								: jour courant
//	- '<Demain>'								: jour suivant le jour courant
//	- 'liste de jours séparateur ","'			: jours de la liste
// cTypeAffichageJours
// 	- dsTypeAffichageJoursNormal ("N") (défaut) : Normal, on affiche les jours d'ouverture (+ filtre éventuel de pvLstJours), sur les horaires d'ouverture
//	- dsTypeAffichageJoursInverse ("I")			: Inverse, on affiche l'inverse des horaires d'ouverture, sur les 7 jours (+ filtre éventuel de pvLstJours)
//	- dsTypeAffichageJoursComplet ("C") 		: Complet, on affiche tous les jours, 24h/24 (+ filtre éventuel de pvLstJours)
//  - chaine horaire <Debut1>-<Fin1>[;<Debut2>-<Fin2>[...]] e.g. "10:00-12:00;14:00-18:00"
require_once('Paa.php');

// psParametres = queryString 
// function Planning_RtvJSONPlanning($psParametres, $poConnexion, $pbEstConcepteur, $pbEstDevMode, &$psJSON, &$psErreur) {
function Planning_RtvJSONPlanning($psParametres, $poConnexion, $poController, $pbSymfony, &$psJSON, &$psErreur) {

	if (!getODataPAA($loData, $lcMsgErr, $poConnexion)) {
		$psErreur = "Impossible de récupérer l'objet oData : " . $lcMsgErr;
		return False;
	}

	if (true) {
		// Initialisations avec les vrais paramètres
		// Lecture et interprétation des paramètres
		$laParametres = array();
		parse_str($psParametres, $laParametres);
		$lcLstRes = isset($laParametres["cLstRes"]) ? $laParametres["cLstRes"] : "";
		$ldDebut = isset($laParametres["dDebut"]) ? $laParametres["dDebut"] : -1;
		if (is_numeric($ldDebut)) {
			$liSem = intval($ldDebut);
			$ldDebut = GetDateAvecJour(1, $liSem);
		} else if (isDate($ldDebut)) {
			($ldDebut = CTOD($ldDebut));
		}
		$ldFin = isset($laParametres["dFin"]) ? $laParametres["dFin"] : -1;
		if (is_numeric($ldFin)) {
			$liSem = intval($ldFin);
			$ldFin = GetDateAvecJour(7, $liSem);
		} else if (isDate($ldFin)) {
			($liFin = CTOD($ldFin));
		}

		$lsStyleAffichage = isset($laParametres["cStyleAffichage"]) ? $laParametres["cStyleAffichage"] : "SemaineZZZ";
		// $lsLstJoursAffichage = 'Lundi, mardi' ;
		$lsLstJoursAffichage = isset($laParametres["cLstJoursAffichage"]) ? $laParametres["cLstJoursAffichage"] : "Auto";
		// $lcTypeAffichageJours = "N", "C", "I"
		$lcTypeAffichageJours = isset($laParametres["cTypeAffichageJours"]) ? $laParametres["cTypeAffichageJours"] : "N";
		// $lcOrientation = "H", "V"
		$lcOrientation = isset($laParametres["cOrientation"]) ? $laParametres["cOrientation"] : "H";
		if ($lcOrientation == "H")
			$lcVertical = "false";
		else
			$lcVertical = "true";
	} else {
		// Initialisation fictive
		$lcLstRes = "U268";
		$ldDebut = "2017-05-01";
		$ldFin = "2017-05-1";
		$lsLstJoursAffichage = "<Auto>";
		$lcTypeAffichageJours = "N";
		$lcVertical = "false";
	}

	// $poConnexion = $poController->getDoctrine()->getConnection() ;
	$pbEstConcepteur = $poController->LGisGranted('ROLE_CONCEPTEUR');
	$pbEstDevMode = $poController->estDevMode();

	// Corriger la liste de ressources
	$lcLstRes = $poController->corrigeLstRessourcesPlanningSelonAutorisation($lcLstRes);

	// ---------------------------------------------------------------------------------------
	// Liste des jours, heures min et max
	$liHMin = 99; // Heure de début d'une journée normale'
	$liHMax = 0; // Heure de fin d'une journée normale
	If (!$loData->Planning_RtvCurJours($loDataSet
					, $ldDebut
					, $ldFin
					, $lsLstJoursAffichage
			)) {
		$psErreur = "Impossible de récupérer le curseur des jours : " . $loData->cLastErreur;
		return False;
	} else {
		// Exploiter la liste des jours
		$lcJSONJours = '';
		while ($row = $loDataSet->fetch()) {
			if ($lcJSONJours)
				$lcJSONJours .= chr(13) . ',';
			else
				$lcJSONJours .= chr(13);
			$lcJSONJours .= '{"cLibelle": "' . $row['clibelle'] . '"'
					. ', "cInfoBulle": "' . $row['cinfobulle'] . '"'
					. ', "sIdJour": "' . $row['sidjour'] . '"'
					. ', "cCouleur": "#' . color_DecToHex($row['ccouleur']) . '"'
					. '}';
			$liHMin = Min($liHMin, $row['ndebut']);
			$liHMax = Max($liHMax, $row['nfin']);
		}
		$lcJSONJours = '"aJours": [' . $lcJSONJours . ']';
	}

	// ---------------------------------------------------------------------------------------
	// Calcul des tranches
	$lcJSONTranches = '';
	if ($lcTypeAffichageJours == '')
		$lcTypeAffichageJours = 'N';
	$liHMin = $liHMin * 60;
	$liHMax = $liHMax * 60;
	if ($lcTypeAffichageJours == 'N') {
		// Horaires normaux : on a déja calculé $liHMi et n$liHMax)
		$lcJSONTranches = '{"iDebut": ' . $liHMin . ', "iFin": ' . $liHMax . '}';
	} else if ($lcTypeAffichageJours == 'I') {
		// Horaires inversés
		$lcJSONTranches = '{"iDebut": 0, "iFin": ' . $liHMin . '}'
				. ', {"iDebut": ' . $liHMax . ', "iFin": ' . 24 * 60 . '}';

		$liHMin = 0;
		$liHMax = 24 * 60;
	} else if ($lcTypeAffichageJours == 'C') {
		// 24h/24
		$lcJSONTranches = '{"iDebut": 0, "iFin": ' . 24 * 60 . '}';
		$liHMin = 0;
		$liHMax = 24 * 60;
	} else {
		// couples début/fin
		$lcJSONTranches = 'Cas non géré';
	}
	$lcJSONTranches = '"aTranches": [' . $lcJSONTranches . ']';

	// ---------------------------------------------------------------------------------------
	// Calcul des ressources
	If (!$loData->RtvCurLstRessources($loDataSet
					, $lcLstRes
					, null
					, 1
			)) {
		$llKO = True;
		$psErreur = "Impossible de récupérer le curseur des ressources : " . $loData->cLastErreur;
		return False;
	} else {
		// Exploiter la liste des ressources
		$lcJSONRessources = '';
		while ($row = $loDataSet->fetch()) {
			if ($lcJSONRessources)
				$lcJSONRessources .= chr(13) . ',';
			else
				$lcJSONRessources .= chr(13);
			$lcJSONRessources .= '{"sIdRessource": "' . trim($row['ctype_res']) . trim($row['iid_res']) . '"'
					. ', "cLibelle": "' . rtvNomRessource($row['ctype_res']) . ' ' . $row['cnom_res'] . '"'
					. ', "cLibelleCourt": "' . $row['cnom_res_court'] . '"'
					. '}';
		}
		$lcJSONRessources = '"aRessources": [' . $lcJSONRessources . ']';
	}

	// ---------------------------------------------------------------------------------------
	// Calcul des séances
	$lbInclPrésences = false;
	$lbUsagIgnoreGroupe = false;
	$lbInclSimulationHorairesType = true;
	$lbInclSimulationActivitésType = false;  // Passer cette valeur à True fait que certaines séances n'apparaissent par dans BDD ORT
	$lbVérifiePrésencesEtAbsences = true;
	$lbExclActivités = false;
	$lbExclAbsences = false;
	$lbInclInfoTypeActivite = false;
	$lbIgnoreHorsEtablissement = false;

	If (!$loData->Planning_RtvCurSeances($loDataSet	// &$poDataSet
					, $lcLstRes	// $psLstRes
					, $ldDebut	// $pdDébut
					, $ldFin	// $pdFin
					, $lbInclPrésences  // $pbInclPrésences
					, $lbExclActivités  // $pbExclActivités
					, $lbExclAbsences  // $pbExclAbsences
					, $lbUsagIgnoreGroupe   // $pbUsagIgnoreGroupe
					, $lbInclSimulationHorairesType // $pbInclSimulationHorairesType
					, $lbInclSimulationActivitésType// $pbInclSimulationActivitésType
			)) {
		$llKO = True;
		$psErreur = "Impossible de récupérer le curseur des séances : " . $loData->cLastErreur;
		return False;
	} else {
		// Exploiter la liste des ressources
		$lcJSONSéances = '';
		$lcJSONZonesInterdites = '';
		$liRow = 0;
		while ($row = $loDataSet->fetch()) {
			$liRow++;
			if ($row['lmasque']) {
				// Zone interdite
				$lcJSONZonesInterdites .= $lcJSONZonesInterdites ? ',' : '';
				$lcJSONZonesInterdites .= chr(13) . Planning_RtvJSONSéance($row, $liRow);
			} else if ($row['lestpresence']) {
				// Les présences ne sont pas affichées
			} else {
				// Séance normale
				$lcJSONSéances .= $lcJSONSéances ? ',' : '';
				$lcJSONSéances .= chr(13) . Planning_RtvJSONSéance($row, $liRow);
			}

			// break ;
		}
		$lcJSONSéances = '"aSeances": [' . $lcJSONSéances . ']';
		$lcJSONZonesInterdites = '"aZonesInterdites": [' . $lcJSONZonesInterdites . ']';
	}

	// Récupération des données et émission du JSON
	$lsEstConcepteur = ($pbEstConcepteur) ? 'true' : 'false';
	$lsEstDevMode = ($pbEstDevMode) ? 'true' : 'false';
	$liModeExecution = $pbSymfony ? 4 : 3; // 4 : mode "Symfony", 3 : Mode "Navigateur"
	$lcJsonConfig = <<<JSON
	"oConfig": {
			"cLibelle": "Planning"
			, "-": "3444.00"
			, "iHeureMin": $liHMin                    
			, "iHeureMax": $liHMax
			, "vertical": $lcVertical
			, "pas": 30
			, "dureeDftSeance": 30.00000
			, "largeurRessourceMin": 50
			, "hauteurRessourceMin": 21
			, "lCouleursDegradees": true
			, "iTypePrédéfini": 3
			, "iTypeHoraires": 1
			, "iTypeAffichageRessources": 1
			, "bMontreConflitsDansPopUp": false
			, "cTypeAffichageJours": "8-18"
			, "cLstHorairesGrille": "Matin : 8h00-12h00;PM : 14h00-17h00;Matin-PM: 8-18;"
			, "cStyle": "$lsStyleAffichage"
			, "lMontreActivités": true
			, "lAjustePolice": true
			, "iTaillePoliceSeancesDft": 10
			, "iTaillePoliceSeancesMin": 7
			, "nModeExecution": $liModeExecution
			, "lReadWrite": $lsEstConcepteur
			, "bDevel": $lsEstDevMode
			}
JSON;

	$lcJSON = <<<JSON
	{"oStructure":
	{
		$lcJSONRessources
	,	$lcJSONJours
	,	$lcJSONTranches
	,	$lcJsonConfig
	}, "oData":
	{
		$lcJSONSéances
	,	$lcJSONZonesInterdites

	}}
JSON;

	$psErreur = "OK";
	$psJSON = $lcJSON;
	return true;
}

// LG 20190410
// $paSeance : tableau renvoyé par fonction PG Paa.Planning_RtvCurSeances()
// ctype_res character varying, iid_res integer, ddate date, iid_seance integer, ndebut numeric, nfin numeric, clibelle text, icouleur integer, lestabsence boolean, lestpresence boolean, lmasque boolean, lvirtuel boolean, llibre1 boolean, llibre2 boolean, inbchevauchements integer, irangchevauchement integer
function Planning_RtvJSONSéance($paSeance, $piNoSeance) {
	$lcJSON = "";

	$lcDate = DTOC($paSeance['ddate'], 1);
	$liDebut = $paSeance['ndebut'];  // Début en heures décimales ($paSeance['idebut'] est en minutes entières)
	$liFin = $paSeance['nfin'];

	$lcLbl = $paSeance['clibelle'];
	$lcLbl = strTran($lcLbl, chr(13), "");
	$lcLbl = strTran($lcLbl, chr(10), "");
	$lcInfoBulle = uNumVersHeure($liDebut) . '-' . uNumVersHeure($liFin) . '<BR>' . $lcLbl;
	$lcInfoBulle = JSON_ToLitteral(strip_tags($lcInfoBulle, "<br>"));
	$lcLbl = JSON_ToLitteral($lcLbl);

	$lcJSONTmp = '{"sIdJour": "J' . $lcDate . '"'
			. ', "sIdRessource": "' . trim($paSeance['ctype_res']) . trim($paSeance['iid_res']) . '"'
			. ', "iDebut":' . $liDebut * 60
			. ', "iFin":' . $liFin * 60
			. ', "cLibelle": ' . $lcLbl
			. ', "cInfobulle": ' . $lcInfoBulle;

	if ($paSeance['lmasque']) {
		// Zone interdite
		$lcJSON = $lcJSONTmp . ', "sIdZoneInterdite": "Z' . $piNoSeance . '"'
				. '}';
//	} else if ($paSeance['lestpresence']) {
//		// Les présences ne sont pas affichées
	} else {
		if (array_key_exists("ccouleur", $paSeance))
			$lcCouleur = $paSeance['ccouleur'];
		else
			$lcCouleur = '#' . color_DecToHex($paSeance['icouleur']);
		$lcJSON = $lcJSONTmp . ', "sIdSeance": "S' . $piNoSeance . '"'
				. ', "iId_Seance": ' . $paSeance['iid_seance']
				. ', "iNbChevauchements": ' . $paSeance['inbchevauchements']
				. ', "iRangChevauchement": ' . $paSeance['irangchevauchement']
				. ', "cCouleur": "' . $lcCouleur . '"'
				. ', "lVirtuel": ' . JSON_ToLitteral($paSeance['lvirtuel'])
				. ', "lLibre1": ' . JSON_ToLitteral($paSeance['llibre1'])
				. ', "lLibre2": ' . JSON_ToLitteral($paSeance['llibre2'])
// LG 20190824 début
		;
		if (array_key_exists("irangsousgroupe", $paSeance)) {
			$lcJSON = $lcJSON . ', "iRangSousGroupe": ' . Nvl($paSeance['irangsousgroupe'], 0);
		}
		if (array_key_exists("inbsousgroupes", $paSeance)) {
			$lcJSON = $lcJSON . ', "iNbSousGroupes": ' . Nvl($paSeance['inbsousgroupes'], 0);
		}
		if (array_key_exists("cnomsousgroupe", $paSeance)) {
//			$lcJSON = $lcJSON . ', "cNomSousGroupe": "' . Nvl($paSeance['cnomsousgroupe'], '') . '"' ;
			$lcJSON = $lcJSON . ', "cNomSousGroupe": "' . $paSeance['cnomsousgroupe'] . '"';
		}
// $lcJSON = $lcJSON . ', "cNomSousGroupe": "toto"' ;
// LG 20190824 fin
		$lcJSON = $lcJSON . '}';
	}

	return $lcJSON;
}
