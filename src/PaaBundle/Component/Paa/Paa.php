<?php

$lcRepPartages = dirname(dirname(dirname(dirname(__FILE__)))) . "/Limoog/PartageBundle/Component/";
require_once($lcRepPartages . 'VFP.php');
require_once($lcRepPartages . 'xTools.php');
require_once($lcRepPartages . 'PG.php');
require_once($lcRepPartages . 'JSON.php');
//require_once('Connexion.php');
require_once('DataPAA_PG.php');
require_once('ressources_Class.php');

require_once('Paa_Constantes.php');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajoute au header l'information d'origige de requête acceptée
// Pour éviter l'impossibilité ""Access-Control-Allow-Origin""
// LG 20170519
Function paaAllowOrigin() {
	// Selon http://stackoverflow.com/questions/1653308/access-control-allow-origin-multiple-origin-domains
	$http_origin = $_SERVER['HTTP_ORIGIN'];

	if ($http_origin == "http://127.0.0.1:8887" || $http_origin == "http://127.0.0.1:8080" || $http_origin == "http://localhost:8383" || $http_origin == "http://www.limoogjavascript.localhost.net"
	) {
		// Origine acceptée
		header("Access-Control-Allow-Origin: $http_origin");
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoie True si cette semaine est une semaine type
// piSem				: semaine à étudier (ou datetime)
//					: ou liste de semaines (type C)
//(@piRangSemType)	: pour retour du N° de semaine type correspondante
// LG 20100601
Function estSemaineType($piSem, &$piRangSemType = 0) {

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
	// Récursivité pour une liste de semaines
	If (Vartype($piSem) == "C") {
//		$llRetu = Null ;
//		$lcLstSem = $piSem ;
//		While (ParcourtChaine($lcLstSem		// psLst
//							, $lcSem		// psElt
//							, ","			// psSépa
//							)) {
//			$liSem = Val($lcSem) ;
//			$llEstSemaineType = EstSemaineType($liSem) ;
//			if (IsNull($llRetu)) {
//				$llRetu = $llEstSemaineType ;
//			} else if (!$llRetu == $llEstSemaineType) {
//				RaiseErreur("Types de semaines variées : certaines sont type, d'autres non") ;
//				Exit ;
//			}
//		}
//		
//		Return $llRetu ;
//
		LG_die(__Function__ . " : gestion des liste de semaines non prise en charge");
	}
	// Fin de récursivité pour une liste de semaines
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
	// Convertir le paramètre de semaine en N° de semaine le cas échéant
	$piSem = GetNoSemaine($piSem);

	If ($piSem >= 0) {
		// Ce n'est pas une semaine type
		$piRangSemType = 0;
		Return False;
	}

	If ($piSem > eiSemaineInexistante / 2) {
		// C'est une semaine-type normale
		$piRangSemType = -1 * $piSem;
		Return True;
	}

	// C'est une semaine type de brouillon
	$piRangSemType = $piSem - eiSemaineInexistante;
	Return True;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoie True si cette semaine est une semaine type brouillon
// piSem				: semaine à étudier ou datetime
//(@piRangSemType)	: pour retour du N° de semaine type correspondante
// LG 20100601
Function estSemaineTypeBrouillon($piSem, &$piRangSemType = 0) {

	// Convertir le paramètre de semaine en N° de semaine le cas échéant
	$piSem = GetNoSemaine($piSem);

	If ($piSem >= 0) {
		// Ce n'est pas une semaine type
		$piRangSemType = 0;
		Return False;
	}

	If ($piSem > eiSemaineInexistante / 2) {
		// C'est une semaine-type normale
		$piRangSemType = 0;
		Return False;
	}

	// C'est une semaine type de brouillon
	$piRangSemType = $piSem - eiSemaineInexistante;
	Return True;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoyer la date pour un jour de semaine donné
// piJour		: N° du jour de la semaine, ou date dont on utilisera le jour de la semaine
// piSemaine	: N° de la semaine dans le contexte de la base de données en cours
//					, ou date dont on utilisera la semaine
//					, ou True -> période courante : 1-> $gvDateDébutAnnée_Lundi, 7 -> $gvDateFinAnnée
Function GetDateAvecJour($piJour, $piSemaine, $pbAccepteSemaineInvalide = false) {

	If (IsNull($piJour) || IsNull($piSemaine)) {
		if ($pbAccepteSemaineInvalide)
			Return NULL;
		else
			LG_Die(__Function__ . " : semaine ou jour fourni invalide");
	}

//	Local liSem, liSemActu, liDiff, lvDate, liRangSemaineType

	If (Dollar(Vartype($piJour), "DT"))
		$piJour = Dow($piJour, 2);

	If (Dollar(Vartype($piSemaine), "DT")) {
		$lvDate = dateAdd($piSemaine, -1 * Dow($piSemaine, 2) + $piJour);
		Return $lvDate;
	}

	// Lundi de la semaine demandée
	if (Vartype($piSemaine) == "L" && $piSemaine) {
		// On a passé True dans piSemaine
		// Ca signifie la période courante
		Lg_Die(__FUNCTION__ . " : période courante non encore gérée");
		if ($piJour = 1) {
			// On demande le début de la période
			$lvDate = $gvDateDébutAnnée_Lundi;
		} else if ($piJour == 7) {
			// On demande la fin de la période
			$lvDate = $gvDateFinAnnée;
		} else {
			$lvDate = $gvDateDébutAnnée_Lundi;
			Lg_Die(__FUNCTION__ . " : _3530VQ8XJ - cas non prévu");
		}
	} else if (Vartype($piSemaine) !== "N") {
		// Non numérique
		If ($pbAccepteSemaineInvalide)
			$lvDate = "1949-12-26";  // Equivaut à DateDébutSemaine({^1950/01/01})
		Else
			Lg_Die(__FUNCTION__ . " : la semaine fournie n'est pas numérique");
	} else if ($piSemaine == 0) {
		// Cas particulier : semaine reconductible
// echo $Erreur ;
		$lvDate = "1900-01-01";  // Equivaut à DateDébutSemaine({^1900/01/02})
	} else if (EstSemaineTypeBrouillon($piSemaine, $liRangSemaineType)) {
		// Brouillon de semaine type
		$lvDate = DateDébutSemaine((1800 + $liRangSemaineType) . "/02/01");
	} else if (EstSemaineType($piSemaine)) {
		// Cas particulier : semaine type
		$lvDate = DateDébutSemaine((1900 - $piSemaine) . "/02/01");
	} else {
		// Cas général
		$lvDate = dateAdd(DateDébutSemaine(DateDébutDataPAA()), ($piSemaine - 1));
	}

	// Valeur de retour
	$lvDate = dateAdd($lvDate, $piJour - 1);

	Return $lvDate;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoyer le N° de semaine (dans le contexte de la base de données en cours) correspondant à une date donnée
// pdDate		: date demandée (ou DateHeure, ou N° de semaine, qu'on retourne directement)
Function getNoSemaine($pvDate) {

	If (Vartype($pvDate) == "N") {
		// On a directement passé le N° de semaine
		Return $pvDate;
	}

	// Cas particulier : date vide
	IF (Empty_LG($pvDate)) {
		Return eiSemaineInexistante;
	}

	// Cas particulier : semaine reconductible
	$liYear = Year($pvDate);
	IF ($liYear == 1900) {
		Return eiSemaineSuivie;
	}

	IF (Vartype($pvDate) == "T") {
		// Convertir en date
		$pvDate = TTOD($pvDate);
	}

	// Cas particulier : semaines types
	if ($liYear < 1850) {
		// Brouillon de semaine-type
		$liSem = eiSemaineInexistante + ($liYear - 1800);
		$ldDateDébut = GetDateAvecJour(1, $liSem);
		If (!($pvDate >= $ldDateDébut && $pvDate <= dateAdd($ldDateDébut, 6))) {
			$liSem = eiSemaineInexistante;
		}
		Return $liSem;
	} else if ($liYear < 1950) {
		// Semaine type
		$liSem = -1;
		$ldDateDébut = GetDateAvecJour(1, $liSem);
		If (!($pvDate >= $ldDateDébut && $pvDate <= dateAdd($ldDateDébut, 6))) {
			$liSem = eiSemaineInexistante;
		}
		Return $liSem;
	}

	// Cas général
	$ldDateTest = DateDébutDataPAA(True);
//	$liNbJours = $pvDate - $ldDateTest ;
	$liNbJours = (strtotime($pvDate) - strtotime($ldDateTest)) / 86400;
	$liSem = Int($liNbJours / 7) + 1;

	// Valeur de retour
	Return $liSem;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoie la date de début des données de PAA
// Valeur de retour		:
// LG 20140925
Function dateDébutDataPaa($pbForceLundi = False) {
	/* 	LG_Die("DateDébutDataPaa : non implémentée") ;
	  $ldDate = $gvDateDébutDataPAA ;
	 */
	$ldDate = '1951/01/01';
	if (!$pbForceLundi) {
		
	} else if (!Upper(Left(CDOW($ldDate), 1)) == "L") {
		// 1er lundi de l'année scolaire
		$ldDate = DateDébutSemaine($ldDate);
	}
	Return $ldDate;
}

// Convertir une valeur semaine ou date ou time vers semaine, date et time
// pvDateTime					: N° de semaine, ou date, ou time
//(plFin)						: .T. si on souhaite la fin de la période (Dft = début de la période)
//(@piSemaine)					: pour retour du N° de semaine
//(@pdDate)						: pour retour de la date
//(@ptTime)						: pour retour de la DateTime
//(@pnHeure)						: pour retour de l'heure sous forme numérique
//(plTouteAnnéePourSemaineType)	: si .T. et pvDateTime correspond à une semaine-type, renvoyer toute la période en cours
// Valeur de retour				: .T. si succès
// LG 20110901
Function cvtSemDateHeure($pvDateTime, $plFin, &$piSemaine, &$pdDate, &$ptTime, &$pnHeure, $plTouteAnnéePourSemaineType) {

	$llKO = False;
	$plFin = Nvl($plFin, False);
	$plTouteAnnéePourSemaineType = Nvl($plTouteAnnéePourSemaineType, False);

	If ((Vartype($pvDateTime) == "L" && $pvDateTime) || (EstSemaineType($pvDateTime) && $plTouteAnnéePourSemaineType)) {
		// On a passé .T. ou un N° de semaine-type dans pvDateTime
		// Ca signifie la période courante
		If (!$plFin) {
			// On demande le début de la période
			$pvDateTime = $gvDateDébutAnnée_Lundi;
		} Else {
			// On demande la fin de la période
			$pvDateTime = $gvDateFinAnnée;
		}
	}

	if (Vartype($pvDateTime) == "N") {
		// On a fourni un N° de semaine
		$piSemaine = $pvDateTime;
		$pdDate = GetDateAvecJour(Iif($plFin, 7, 1)	// piJour
				, $pvDateTime	 // piSemaine
				);
		$pnHeure = Iif($plFin, 24, 0);
		$ptTime = uNumDateVersTime($pdDate	   // pvDate
				, $pnHeure	  // piHeure
				, null	   // piTypeArrondi
				);
	} else if (Vartype($pvDateTime) == "D") {
		// On a fourni une date
		$piSemaine = GetNoSemaine($pvDateTime);
		$pdDate = $pvDateTime;
		$pnHeure = Iif($plFin, 24, 0);
		$ptTime = uNumDateVersTime($pdDate	   // pvDate
				, $pnHeure	  // piHeure
				, null	  // piTypeArrondi
				);
	} else if (Vartype($pvDateTime) == "T") {
		// On a fourni une date
		$piSemaine = GetNoSemaine($pvDateTime);
		$pdDate = Ttod($pvDateTime);
		$pnHeure = uHeureVersNum($pvDateTime	 // pvDurée
				, null	  // pbAutoriseSup24hrs
				, null	  // pbFormatSansSéparateur
				);
		$ptTime = $pvDateTime;
	} else if (Empty($pvDateTime)) {
		// On n'a pas fourni de date
		// Prendre le début/fin de l'année
		Return CvtSemDateHeure(Iif($plFin, $gvDateFinAnnée, $gvDateDébutAnnée_Lundi) // pvDateTime 
				, $plFin			// plFin
				, $piSemaine		   // piSemaine
				, $pdDate			// pdDate
				, $ptTime			// ptTime
				, $pnHeure			// pnHeure
				, plTouteAnnéePourSemaineType	   // plTouteAnnéePourSemaineType
				);
	} else {
//		&gsSetStepOn, "_3AX0JL9WF"		&& LG 20110901 
		LG_die("cas non prévu");
		$llKO = true;
	}

	// Valeur de retour
	Return !$llKO;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoie la date de début des données de PAA
// Valeur de retour		:
// LG 20140925
Function rtvNomRessource($pcType_Res) {
	$pcType_Res = trim($pcType_Res);
	if ($pcType_Res == 'I')
		return 'Intervenant';
	if ($pcType_Res == 'U')
		return 'Usager';
	if ($pcType_Res == 'G')
		return 'Groupe';
	if ($pcType_Res == 'S')
		return 'Equipement';
	if ($pcType_Res == 'E')
		return 'Etablissement';
	if ($pcType_Res == 'AB')
		return 'Activité de base';
	return 'Type ' . $pcType_Res . ' inconnu';
}
