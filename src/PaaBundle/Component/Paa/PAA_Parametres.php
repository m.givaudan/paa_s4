<?php

namespace App\PaaBundle\Component\Paa;

use App\Limoog\PartageBundle\Component\cParametresBDD;

class PAA_Parametres extends cParametresBDD {

	protected $estDevMode = true;

	function setEstDevMode($mode) {
		$this->estDevMode = $mode;
	}

	function getGodeEtablissement() {
		return $this->getParamètre("CodeEtablissement", "", "C");
	}

	function lGèreAccompagnement() {
		return $this->estDevMode
				Or $this->getGodeEtablissement() == "DEMOCRF"
				Or $this->getGodeEtablissement() == "FLORENTIN"
				Or $this->getGodeEtablissement() == "FREYMING";
	}

	function lGèreAccompagnementParSéances() {
		return $this->getGodeEtablissement() == "FLORENTIN";
	}

	function lGèreLieuxSéances() {
		return $this->estDevMode
				Or $this->getGodeEtablissement() == "ESPACESINGULIER";
	}

	function lGèreEnvoiSéances() {
		return $this->estDevMode
				Or $this->getGodeEtablissement() == "ESPACESINGULIER";
	}

	function lGèreRelationsInterSéances() {
		return lGèreAccompagnementParSéances();
	}

}
