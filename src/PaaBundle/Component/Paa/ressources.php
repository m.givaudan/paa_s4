<?php

// http://localhost/PHP/ressources.php?cType_Res=@Tous@&vDebut=2000-01-01&vFin=2000-01-01
require_once('Paa.php');

// LG 20190411 rendu obsolète car inutilisé ?
function obsolete() {
	// Création et paramétrage de l'objet chargé de la récupération des données
	$o = new cGereRessources();
	$o->cType_Res = $_GET["cType_Res"] ? $_GET["cType_Res"] : "@Tous@";
	$o->iSem = $_GET["vDebut"];
	$o->iSemFin = $_GET["vFin"];

	// Poser le header pour éviter l'impossibilité ""Access-Control-Allow-Origin""
	// header("Access-Control-Allow-Origin: http://127.0.0.1:8887") ;
	// header("Access-Control-Allow-Origin: http://127.0.0.1:8080") ;
	// header("Access-Control-Allow-Origin: http://localhost:8383") ;
	paaAllowOrigin();

	// Récupération des données et émission du JSON
	if ($o->rtvJSONRessources($lcJSON, 'cNomCourt')) {
		echo $lcJSON;
	} else {
		//	http_response_code(400);
		//	header('HTTP/ 400 Echec de l exécution de la requête : ' + $o->cLastErreur);
		//	header("status: 400") ;
		//	header("status-text: " + "Echec de l'exécution de la requête : " + $o->cLastErreur) ;
		//	header(' ', true, 400) ;
		header(' ', true, 400);
		echo '{"error":"server_error", "error_description":' . "Echec de l'exécution de la requête : " . $o->cLastErreur . '}';
	}
}

// psRes : 
function decomposeRessource($psRes, &$psType_Res, &$piId_Res) {
	$psType_Res = substr($psRes, 0, 1);
	$piId_Res = substr($psRes, 1, strlen($psRes) - 1);
	return true;
}
