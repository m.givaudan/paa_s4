<?php

// *********************************************************************************************
// Récupérer une instance de la classe cDataPAA_PG
function getODataPAA(&$poDataPAA, &$psErr, $poConnexion = null) {
	$lbKO = false;
	if (getOrReUseGlobalObject($lo, 'goDataPAA', 'cDataPAA_PG', $lsErr, array($poConnexion))) {
		$poDataPAA = $lo;
	} else {
		$lbKO = true;
	}
	return !$lbKO;
}

// *********************************************************************************************
class cDataPAA_PG {

	protected $oConnexion = Null;

	function __construct($poConnexion) {
		$this->oConnexion = $poConnexion;
	}

	Function RtvSQLCurseur($pcSQL, &$poDataSet) {
		$lbKO = False;
		$this->cLastErreur = "";
//		if (!getOConnexion($loCnxn, $err)) {
		if ($this->oConnexion === null) {
			// Echec de la connexion
			$this->cLastErreur = $err;
			$lbKO = True;
		} else {
// echo "Exécution de : <br>" . $pcSQL ."<br>" ;
			$poDataSet = $this->oConnexion->query(utf8_encode($pcSQL), PDO::FETCH_ASSOC);
		}
		if (!$lbKO && !$poDataSet) {
			$err = $this->oConnexion->errorInfo();
			$this->cLastErreur = $err[2];
			$lbKO = True;
//			throw new Exception("échec de la requête : " . $this->cLastErreur) ;
		}

		return !$lbKO;
	}

	// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	// Renvoyer un curseur de liste de ressources
	// selon les paramètres de l'objet cGèreFiltre fourni
	// poGèreFiltre			: objet cGèreFiltre
	//(pcLstFlds)			: Liste des champs à inclure, en plus de cType_Res et iId_Res (cNom, cNomCourt, cTri, ...)
	// Valeur de retour		: True si succès
	// LG 20180711
	Function RtvSQLRessourcesFiltréeParGèreFiltre($poGèreFiltre, $pcLstFlds) {
		$lcSQL_oGèreFiltre = $poGèreFiltre->ToString(1);
		$lcOrderBy = <<<ORDER
Case When cType_Res = 'I' Then '0' When cType_Res = 'U' Then '2' When cType_Res = 'G' Then '3' Else cType_Res End
ORDER;

		// Exécuter sous PostGreSQL
		If ($pcLstFlds) {
			$lcSQL = "Select cType_Res, iId_Res, " . $pcLstFlds
					. " From Paa.RtvCurRessourcesFiltreeParGereFiltreAvecData('"
					. $lcSQL_oGèreFiltre
					. "')"
					. " Order by " . $lcOrderBy . ", " . $pcLstFlds
					. ";";
		} Else {
			$lcSQL = "Select * From Paa.RtvCurRessourcesFiltreeParGereFiltre('"
					. $lcSQL_oGèreFiltre
					. "')"
					. " Order by " . $lcOrderBy . ""
					. ";";
		}

		return $lcSQL;
	}

	// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	// Renvoyer un curseur de liste de ressources
	// selon les paramètres de l'objet cGèreFiltre fourni
	//(@pcCurseur)			: pour retour du curseur créé
	// poGèreFiltre			: objet cGèreFiltre
	//(pcLstFlds)			: Liste des champs à inclure, en plus de cType_Res et iId_Res (cNom, cNomCourt, cTri, ...)
	// Valeur de retour		: True si succès
	// LG 20120405
	Function RtvCurRessourcesFiltréeParGèreFiltre(&$pcCurseur, $poGèreFiltre, $pcLstFlds) {

		$llKO = False;
		$lcSQL = $this->RtvSQLRessourcesFiltréeParGèreFiltre($pcCurseur, $poGèreFiltre, $pcLstFlds);

		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $pcCurseur   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}

		// Valeur de retour
		Return !$llKO;
	}

//AV 25/03/2019 Début
//Ajout d'une fonction afin de récuperer les éléments du treeview
	Function RessourcesActiArthur(&$pcCurseur, $poConnexion) {
		$llKO = False;
		$this->oConnexion = $poConnexion;
		$lcSQL = 'SELECT * FROM paa.rtvcuractibases_ssgpes_gpes();';
		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $pcCurseur   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}
		// Valeur de retour
		Return !$llKO;
	}

//AV 25/03/2019 Fin
//AV 27/05/2019 Début
	Function RessourceActiGabSgabArthur(&$pcCurseur, $poConnexion) {
		$llKO = False;
		$this->oConnexion = $poConnexion;
		$lcSQL = "SELECT * FROM paa.rtvcuractibases_ssgpes_gpes() WHERE (ctype = 'GAB' OR ctype = 'SGAB') AND iid > 0;";
		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $pcCurseur   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}
		// Valeur de retour
		Return !$llKO;
	}

//AV 27/05/2019 Fin
//AV 24/06/2019 Début
	Function RessourceUsersAthur(&$pcCurseur, $poConnexion, $piIdRes) {
		$llKO = False;
		$this->oConnexion = $poConnexion;
		$lcSQL = "SELECT * FROM paa_securite.users WHERE iid_res = " . $piIdRes;
		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $pcCurseur   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}
		// Valeur de retour
		Return !$llKO;
	}

//AV 24/06/2019 Fin
//AV 24/06/2019 Début
	Function RessourceLogAthur(&$pcCurseur, $poConnexion, $piErrNo, $psErrStr, $psErrFile, $psErrVersion, $psErrDetail) {
		$llKO = False;
		$this->oConnexion = $poConnexion;
		//$lcSQL = "INSERT INTO paa.paa_erro(err_no, err_deta) VALUES(98,'".$psErrDetail."')";

		$lcSQL = "SELECT * FROM partage.adderreurclient(
                    " . $piErrNo . ",
                    '" . $psErrStr . "',
                    '" . $psErrFile . "',
                    '" . $psErrVersion . "',
                    '" . $psErrDetail . "')";
		$lcSQL = utf8_decode($lcSQL);
		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $pcCurseur   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}
		// Valeur de retour
		Return !$llKO;
	}

//AV 24/06/2019 Fin
	// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	// Récupérer un curseur de jours, utilisables pour structurer le planning
	// &$poDataSet				: pour retour du jeu de données
	// (cType_Res Char(2)
	// , iId_Res Integer
	// , cNom_Res Text
	// , cNom_Res_Court Text
	//	)
	// tcLstRes					: la liste des ressources (type "<Type><Id>", ou "<Id>"), ou simple type de ressource
	//(tcType_Res)				: type de ressource forcé (dans le cas où la liste de ressources est de la forme "<Id>")
	//(tiTypeNomsRessources)		: addition de flags à fournir pour inclure des noms de ressources
	//							(appel de RtvCurLstRessources_AjouteNomRessource)
	//							: non vide 	: appel simple (libellé standard) (DFT)
	//							: Flag 2 	: pour ne pas utiliser le libellé standard (cf .RtvCurLstRessources_AjouteNomRessource)
	//							: Flag 4 	: pour inclure le nom du type de ressource (cf .RtvCurLstRessources_AjouteNomRessource)
	//							: Flag 8 	: pour inclure le pronom devalt le type de ressource (e.g. ""le groupe", cf .RtvCurLstRessources_AjouteNomRessource)
	//							: Flag 16	: pour exlure le nom court (cNom_Res_Court sera NULL)
	//							: Flag 32	: pour exlure le nom long (cNom_Res sera NULL)
	function RtvCurLstRessources(&$poDataSet, $pcLstRes, $pcType_Res, $piTypeNomsRessources) {

		$lcSQL = <<<SQL
		Select *
			 From Paa.RtvCurLstRessources('$pcLstRes'
											, '$pcType_Res'
											, $piTypeNomsRessources
											) ;
SQL;
// echo $lcSQL ;
		$llKO = False;
		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $poDataSet   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}

		// Valeur de retour
		Return !$llKO;
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
	// Ouvrir un curseur de toutes les séances d'une liste de ressources entre deux dates
	//(@$poDataSet) 						: Nom de l'alias à créer	
	//(psLstRes) 							: Liste de ressources, du type "I253,S20,U50..." (Défaut : toutes les ressources de l'étab.)
	// pvDébut 								: N° de semaine
	//										, ou date de début
	//										, ou liste de dates de type "{^2006/11/23},..."
	//										, ou False pour "Tous plans-types"
	// pdFin									: N° de semaine ou date de fin
	//(pbInclPrésences) 						: True pour inclure les présences
	//(pbUsagIgnoreGroupe) 					: True pour ne pas inclure les activités de groupe pour les usagers
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	//(piIdSéanceForcée)						: on peut ici passer une Id de séance qui sera prise même si ses horaires ne sont pas les bons
	//(pvHorairesChevauchent)				: False pour renvoyer toutes les séances COMPLETEMENT INCLUSES entre ptDébut et ptFin
	//										: True pour renvoyer toutes les séances dont les horaires CHEVAUCHENT les ptDébut et ptFin (chevauchement mini accepté : -0.001)
	//										: valeur numérique pour donner l'intervalle mini de chavauchement (e.g. : 0)
	//(pbInclPrésencesSansRecalc)			: True pour ne pas faire le recalcul des présences si on doit inclure les présences
	//										: 2 pour ne même pas passer dans la boucle de recherche/création des activités de présence
	//(pbUsagGpesIgnoreAbsEtablissement )	: True pour ne pas inclure les absences de l'établissement dans les absences des usagers et des groupes
	//(pbInclSimulationHorairesType)			: pour inclure les séances de présence/absence simulées si semaine non spécifique pour la ressource
	//(pbInclSimulationActivitésType)		: pour inclure les séances d'activités des semaines types en cas de semains non créée
	//(pbVérifiePrésencesEtAbsences)			: pour forcer la remise à jour de la cohérence des présences et des absences
	//										: valeur numérique utilisée dans la récursivité pour éviter les récursivités infinies éventuelles
	//(pbExclActivités)						: True pour ne pas inclure les séances d'activité
	//(pbExlAbsences)						: True pour ne pas inclure les séances d'absence
	//(pbSéancesDistinct)					: True pour ne pas renvoyer de doublons de séances (les champs Id_Res et Type_Res sont exclus)
	//(pvInclLibellé) 						: True ou un format pour inclure le libellé
	//(tlInclInfoTypeActivite)				: True pour inclure les informatoins sur types d'activité : lEstPresence, lEstTournée, ... 
	//(tlIgnoreHorsEtablissement)			: True pour inclure les séances "Hors établissement" (et corriger les anomalies éventuelles)
	// Valeur de retour						: True si succès
	// LG 20140130 
	Function OuvreSéancesRessources(&$poDataSet
	, $psLstRes
	, $pdDébut
	, $pdFin
	, $pbInclPrésences
	, $pbUsagIgnoreGroupe
	, $piIdSéanceForcée
	, $pvHorairesChevauchent
	, $pbInclPrésencesSansRecalc
	, $pbUsagGpesIgnoreAbsEtablissement
	, $pbInclSimulationHorairesType
	, $pbInclSimulationActivitésType
	, $pbVérifiePrésencesEtAbsences
	, $pbExclActivités
	, $pbExclAbsences
	, $pbSéancesDistinct
	, $pvInclLibellé
	, $tlInclInfoTypeActivite
	, $tlIgnoreHorsEtablissement
	) {
		// Environnement
		$llKO = false;

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Paramètres corrigés
//		$ldFin = Iif(Vartype($pdFin) = "D", uNumDateVersTime($pdFin, 24), $pdFin) ;
		$ldFin = $pdFin;
//		Do case
//			Case Vartype($pdDébut) $ "DT"
//				// Dates ou DateTimes
		$lcDebut = PG_ToLitteral($pdDébut, "TimeStamp");
		$lcFin = PG_ToLitteral($ldFin, "TimeStamp");

//			Case Vartype($pdDébut) = "N"
//				// N°s de semaine
//				$lcDebut = PG_ToLitteral($pdDébut, "Integer")
//				$lcFin = PG_ToLitteral($pdFin, "Integer")
//
//			Case Vartype($pdDébut) = "L"
//				// N°s de semaine
//				$lcDebut = PG_ToLitteral($pdDébut, "Boolean")
//				$lcFin = 'False'
//
//			Case Vartype($pdDébut) = "C"
//				// N°s de semaine
//				$pdDébut = Chrtran($pdDébut, "{", "")
//				$pdDébut = Chrtran($pdDébut, "}", "")
//				$lcDebut = PG_ToLitteral(Chrtran($pdDébut, "^", ""), "Text")
//				$lcFin = 'Null::Boolean'
//			
//			Otherwise
//				// Non prévu
//				RaiseErreur("Type de début non prévu")
//		Endcase 
//		
		// Conversions de valeurs
		$liUsagIgnoreGroupe = call_user_func(function() {
			if (Vartype($pbUsagIgnoreGroupe) == "L" && $pbUsagIgnoreGroupe)
				return -1;
			if (Vartype($pbUsagIgnoreGroupe) == "L")
				return 0;
			return $pbUsagIgnoreGroupe;
		});
		$liIdSéanceForcée = Evl($piIdSéanceForcée, 0);
		$liHorairesChevauchent = call_user_func(function() {
			if (Vartype($pvHorairesChevauchent) == "L" && $pvHorairesChevauchent)
				return -0.001;
			if (Vartype($pvHorairesChevauchent) == "L")
				return -9999;
			return $pvHorairesChevauchent;
		});
		$liInclPresencesSansRecalc = call_user_func(function() {
			if (Vartype($pbInclPrésencesSansRecalc) == "L" && $pbInclPrésencesSansRecalc)
				return -1;
			if (Vartype($pbInclPrésencesSansRecalc) == "L")
				return 0;
			return $pbInclPrésencesSansRecalc;
		});
		$liVerifiePresencesEtAbsences = call_user_func(function() {
			if (Vartype($pbVérifiePrésencesEtAbsences) == "L" && $pbVérifiePrésencesEtAbsences)
				return -1;
			if (Vartype($pbVérifiePrésencesEtAbsences) == "L")
				return 0;
			return $pbVérifiePrésencesEtAbsences;
		});
		If ($pbExclAbsences)
			$tlIgnoreHorsEtablissement = True;

		// Conversions au format PG
		$psLstRes = PG_ToLitteral($psLstRes, "VarChar");
		$pbInclPrésences = PG_ToLitteral($pbInclPrésences);
		$liUsagIgnoreGroupe = PG_ToLitteral($liUsagIgnoreGroupe);
		$liIdSéanceForcée = PG_ToLitteral($liIdSéanceForcée);
		$liHorairesChevauchent = PG_ToLitteral($liHorairesChevauchent);
		$pbUsagGpesIgnoreAbsEtablissement = PG_ToLitteral($pbUsagGpesIgnoreAbsEtablissement);
		$pbInclSimulationHorairesType = PG_ToLitteral($pbInclSimulationHorairesType);
		$pbInclSimulationActivitésType = PG_ToLitteral($pbInclSimulationActivitésType);
		$pbExclActivités = PG_ToLitteral($pbExclActivités);
		$pbExclAbsences = PG_ToLitteral($pbExclAbsences);
		$pbSéancesDistinct = PG_ToLitteral($pbSéancesDistinct);
		$pvInclLibellé = PG_ToLitteral($pvInclLibellé, "Varchar");
		$tlInclInfoTypeActivite = PG_ToLitteral($tlInclInfoTypeActivite);
		$tlIgnoreHorsEtablissement = PG_ToLitteral($tlIgnoreHorsEtablissement);
		$liInclPresencesSansRecalc = PG_ToLitteral($liInclPresencesSansRecalc, "Integer");
		$liVerifiePresencesEtAbsences = PG_ToLitteral($liVerifiePresencesEtAbsences, "Numeric");

		// Exécuter sous PostGreSQL
		$lcSQL = <<<SQL
			Select * From 
				paa.ouvreseancesressources($psLstRes										--// tslstres character varying
										, $lcDebut											--// tddebut TimeStamp
										, $lcFin 											--// tdfin TimeStamp
										, $pbInclPrésences									--// tbinclpresences boolean DEFAULT false
										, $liUsagIgnoreGroupe 								--// tiUsagerIgnoreGroupe Integer DEFAULT 0
										, $liIdSéanceForcée 								--// tiidseanceforcee integer DEFAULT 0
										, $liHorairesChevauchent 							--// tvhoraireschevauchent numeric DEFAULT 0
										, $liInclPresencesSansRecalc 						--// tnInclPresencesSansRecalc numeric DEFAULT 0
										, $pbUsagGpesIgnoreAbsEtablissement					--// tbusaggpesignoreabsetablissement boolean DEFAULT false
										, $pbInclSimulationHorairesType						--// tbinclsimulationhorairestype boolean DEFAULT false
										, $pbInclSimulationActivitésType					--// tbinclsimulationactivitestype boolean DEFAULT false
										, $liVerifiePresencesEtAbsences						--// tnVerifiePresencesEtAbsences numeric DEFAULT 0
										, null												--// tbmuet boolean DEFAULT false
										, $pbExclActivités									--// tbexclactivites boolean DEFAULT false
										, $pbExclAbsences									--// tbexclabsences boolean DEFAULT false
										, $pbSéancesDistinct								--// tbseancesdistinct boolean DEFAULT false
										, $pvInclLibellé 									--// tcinclLibelle boolean DEFAULT false
										, $tlInclInfoTypeActivite							--// tbInclInfoTypeActivite boolean DEFAULT false
										, $tlIgnoreHorsEtablissement						--// tbIgnoreHorsEtablissement boolean DEFAULT false
										)
										
SQL;
// echo "lcSQL = $lcSQL<br>" ;

		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $poDataSet   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}

		// Valeur de retour
		Return !$llKO;
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
	// Ouvrir un curseur de toutes les séances d'une liste de ressources entre deux dates
	//(@$poDataSet) 						: Nom de l'alias à créer	
	//(psLstRes) 							: Liste de ressources, du type "I253,S20,U50..." (Défaut : toutes les ressources de l'étab.)
	// pvDébut 								: N° de semaine
	//										, ou date de début
	//										, ou liste de dates de type "{^2006/11/23},..."
	//										, ou False pour "Tous plans-types"
	// pdFin									: N° de semaine ou date de fin
	//(pbInclPrésences) 						: True pour inclure les présences
	//(pbExclActivités)						: True pour ne pas inclure les séances d'activité
	//(pbExlAbsences)						: True pour ne pas inclure les séances d'absence
	//(pbUsagIgnoreGroupe) 					: True pour ne pas inclure les activités de groupe pour les usagers
	// 						ATTENTION : Si on choisit d'inclure les séances du groupe, on a aussi ses présences et ses absences
	//(pbInclSimulationHorairesType)			: pour inclure les séances de présence/absence simulées si semaine non spécifique pour la ressource
	//(pbInclSimulationActivitésType)		: pour inclure les séances d'activités des semaines types en cas de semains non créée
	// Valeur de retour						: True si succès
	// LG 20140130 
	//(piIdSéanceForcée)						: on peut ici passer une Id de séance qui sera prise même si ses horaires ne sont pas les bons
	//(pvHorairesChevauchent)				: False pour renvoyer toutes les séances COMPLETEMENT INCLUSES entre ptDébut et ptFin
	//										: True pour renvoyer toutes les séances dont les horaires CHEVAUCHENT les ptDébut et ptFin (chevauchement mini accepté : -0.001)
	//										: valeur numérique pour donner l'intervalle mini de chavauchement (e.g. : 0)
	//(pbInclPrésencesSansRecalc)			: True pour ne pas faire le recalcul des présences si on doit inclure les présences
	//										: 2 pour ne même pas passer dans la boucle de recherche/création des activités de présence
	//(pbUsagGpesIgnoreAbsEtablissement )	: True pour ne pas inclure les absences de l'établissement dans les absences des usagers et des groupes
	//(pbVérifiePrésencesEtAbsences)			: pour forcer la remise à jour de la cohérence des présences et des absences
	//										: valeur numérique utilisée dans la récursivité pour éviter les récursivités infinies éventuelles
	//(pbSéancesDistinct)					: True pour ne pas renvoyer de doublons de séances (les champs Id_Res et Type_Res sont exclus)
	//(pvInclLibellé) 						: True ou un format pour inclure le libellé
	//(tlIgnoreHorsEtablissement)			: True pour inclure les séances "Hors établissement" (et corriger les anomalies éventuelles)

	Function Planning_RtvCurSeances(&$poDataSet
	, $psLstRes
	, $pdDébut
	, $pdFin
	, $pbInclPrésences
	, $pbExclActivités
	, $pbExclAbsences
	, $pbUsagIgnoreGroupe
	, $pbInclSimulationHorairesType
	, $pbInclSimulationActivitésType
	) {

		// Environnement
		$llKO = false;

		// Conversions de valeurs
		$ldFin = $pdFin;
		$liUsagIgnoreGroupe = call_user_func(function($pbUsagIgnoreGroupe) {
			if (Vartype($pbUsagIgnoreGroupe) == "L" && $pbUsagIgnoreGroupe)
				return -1;
			if (Vartype($pbUsagIgnoreGroupe) == "L")
				return 0;
			return $pbUsagIgnoreGroupe;
		}, $pbUsagIgnoreGroupe);

		// Conversions au format PG
		$lcDebut = PG_ToLitteral($pdDébut, "Date");
		$lcFin = PG_ToLitteral($ldFin, "Date");
		$psLstRes = PG_ToLitteral($psLstRes, "VarChar");
		$pbInclPrésences = PG_ToLitteral($pbInclPrésences);
		$liUsagIgnoreGroupe = PG_ToLitteral($liUsagIgnoreGroupe);
		$pbInclSimulationHorairesType = PG_ToLitteral($pbInclSimulationHorairesType);
		$pbInclSimulationActivitésType = PG_ToLitteral($pbInclSimulationActivitésType);
		$pbExclActivités = PG_ToLitteral($pbExclActivités);
		$pbExclAbsences = PG_ToLitteral($pbExclAbsences);

		// Exécuter sous PostGreSQL
		$lcSQL = <<<SQL
			Select * From 
				paa.Planning_RtvCurSeances($psLstRes											--// tslstres character varying
											, $lcDebut											--// tddebut TimeStamp
											, $lcFin 											--// tdfin TimeStamp
											, $pbInclPrésences									--// tbinclpresences boolean DEFAULT false
											, $pbExclActivités									--// tbexclactivites boolean DEFAULT false
											, $pbExclAbsences									--// tbexclabsences boolean DEFAULT false
											, $liUsagIgnoreGroupe 								--// tiUsagerIgnoreGroupe Integer DEFAULT 0
											, $pbInclSimulationHorairesType						--// tbinclsimulationhorairestype boolean DEFAULT false
											, $pbInclSimulationActivitésType					--// tbinclsimulationactivitestype boolean DEFAULT false
											)
SQL;
// echo "lcSQL = $lcSQL<br>" ;

		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $poDataSet   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}

		// Valeur de retour
		Return !$llKO;
	}

	// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	// Récupérer un curseur de jours, utilisables pour structurer le planning
	// &$poDataSet				: pour retour du jeu de données
	// (iSemaine Integer
	//	, dDate Date
	//	, nDebut Numeric
	//	, nFin Numeric
	//	, cDescription VarChar(254)
	//	, iCouleur Integer
	//	)
	// $pdDebut					: date de début
	// $pdFin					: date de fin
	// $psLstJoursAffichage		: 
	// pcLstJoursAffichage : 
	//	- '<Auto>' (dft)							: liste des jours qui ont des horaires non nuls
	//	- '<Hier>'									: jour précédent le jour courant
	//	- '<Ce jour>'								: jour courant
	//	- '<Demain>'								: jour suivant le jour courant
	//	- 'liste de jours séparateur ","'			: jours de la liste
	function Planning_RtvCurJours(&$poDataSet, $pdDebut, $pdFin, $psLstJoursAffichage) {

		$lcSQL = <<<SQL
		Select CDOW(dDate) As cLibelle
				, CDOW(dDate) || ' ' || To_Char(dDate, 'DD/MM/YYYY') || ' ' || cDescription As cInfoBulle
				, 'J' || To_Char(dDate, 'YYYYMMDD') As sIdJour
				, iCouleur::Text As cCouleur
				, nDebut, nFin
			 From Paa.Planning_RtvCurJours('$pdDebut'
											, '$pdFin'
											, '$psLstJoursAffichage'
											) ;
SQL;
// echo $lcSQL ;
		$llKO = False;
		If (!$this->RtvSQLCurseur($lcSQL	// tcSQL
						, $poDataSet   // tcCurseur
				)) {
			// Echec
			$llKO = True;
		}

		// Valeur de retour
		Return !$llKO;
	}

}
