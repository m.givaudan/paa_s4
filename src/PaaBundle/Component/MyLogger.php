<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PaaBundle\Component;

/**
 * Description of MyLogger
 *
 * @author Haris
 */
use \Monolog\Handler\StreamHandler;
use \Monolog\Handler\FirePHPHandler;
use \Monolog\Handler\HandlerInterface;
use \Monolog\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\InvalidArgumentException;
use Exception;
use Monolog\Formatter\LineFormatter;

class MyLogger extends \Monolog\Logger {
	private $projectDir ;
	private $logFile ;
	private $stackTraceFournie ;
	private $bSansStackTrace = false ;
	private $idLog = "" ;
	
    function __construct($name, $projectDir = null, $stackTrace = null, $idLog = null) {
        parent::__construct($name);
		if ($idLog) {
			$this->idLog = $idLog ;
		} else {
			$this->idLog = $this->uniqidReal() ;
		}
        $_ENV['lastErrorID'] = $this->idLog ;
		if ($projectDir) {
			$this->projectDir = $projectDir ;
		} else {
			$this->projectDir = \dirname(\dirname(\dirname(__DIR__)));
		}
		if ($stackTrace == null) {$stackTrace = "" ;}
		$this->stackTraceFournie = $stackTrace ;
        $this->setTimezone(new \DateTimeZone('Europe/Paris')) ;
		
        $formatter = new LineFormatter(
                null, // Format of message in log, default [%datetime%] %channel%.%level_name%: %message% %context% %extra%\n
                null, // Datetime format
                true, // allowInlineLineBreaks option, default false
                true  // ignoreEmptyContextAndExtra option, default false
        );
		
//		$projectRoot = $this->get('kernel')->getProjectDir();
		
//        if ($_ENV['APP_ENV'] === 'prod') {
//            $debugHandler = new StreamHandler($projectDir . '\var\log\prod.log', Logger::DEBUG);
//        }
//        if ($_ENV['APP_ENV'] === 'dev') {
//            $debugHandler = new StreamHandler($projectDir . '\var\log\dev.log', Logger::DEBUG);
//        }
        if ($_ENV['APP_ENV'] === 'prod') {
			$this->logFile = $this->projectDir . '\var\log\prod.log' ;
            $debugHandler = new StreamHandler($this->logFile, Logger::DEBUG);
        }
        if ($_ENV['APP_ENV'] === 'dev') {
			$this->logFile = $this->projectDir . '\var\log\dev.log' ;
            $debugHandler = new StreamHandler($this->logFile, Logger::DEBUG);
        }
        $debugHandler->setFormatter($formatter);
        $this->pushHandler($debugHandler);
    }

    function uniqidReal($lenght = 4) {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $lenght);
    }

    function debug_string_backtrace() {
        ob_start();
        debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $trace = ob_get_contents();
        ob_end_clean();

// Remove first item from backtrace as it's this function which
// is redundant.
        $tracestr = str_replace("#", chr(9) . "#", $trace);
//      preg_replace est obscolète depuis PHP 7.0; à remplacer avec preg_replace_callback
//
//
// Renumber backtrace items.
//        $trace = preg_replace('/^#(\d+)/me', '\'#\' . ($1 - 1)', $trace);
//      preg_replace est obscolète depuis PHP 7.0; à remplacer avec preg_replace_callback
        return $tracestr;
    }

	protected function getMessage($message) {
        $message = "[LogId: " . $_ENV['lastErrorID'] . "] ". $message ;
		if ($this->bSansStackTrace) {
			$stackTrace = "" ;
		} else if ($this->stackTraceFournie !== "") {
			// Une pile des appels a été fournie au constructeur : l'utiliser
			$stackTrace = $this->stackTraceFournie ;
		} else {
			// Pas de pile d'appels fournie : la construire maintenant
			$stackTrace = $this->debug_string_backtrace() ;
		}
		if ($stackTrace) {
			$message .= PHP_EOL . "Pile des appels : " . PHP_EOL . $stackTrace ;
		}
		return $message ;
	}
//    public function rapportuser($message, array $context = array()){
//        $messages = "Rapport Client: ". $message;
//        parent::notice($messages, $context);
//    }
    
    public function Log($message, $psNiveauLog = "info", array $context = array(), bool $pbAvecStackTrace = false) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] ." ". $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        $this->bSansStackTrace = !$pbAvecStackTrace ;
//$message.= $this->logFile ;
//echo "<br>" . $message . "<br>" ;
		$this->$psNiveauLog($message, $context);
//		return $_ENV['lastErrorID'] ;
		return $this->idLog ;
    }
    
    public function info($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] ." ". $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::info($this->getMessage($message), $context);
    }

    public function debug($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] ." ". $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::debug($this->getMessage($message), $context);
    }

    public function notice($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::notice($this->getMessage($message), $context);
    }

    public function warn($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::warn($this->getMessage($message), $context);
    }

    public function warning($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::warning($this->getMessage($message), $context);
    }

    public function err($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::err($this->getMessage($message), $context);
    }

    public function error($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::error($this->getMessage($message), $context);
    }

    public function crit($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::crit($this->getMessage($message), $context);
    }

    public function critical($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
//        parent::critical($messages, $context);
        parent::critical($this->getMessage($message), $context);
    }

    public function alert($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::alert($this->getMessage($message), $context);
    }

    public function emerg($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::emerg($this->getMessage($message), $context);
    }

    public function emergency($message, array $context = array()) {
//        $messages = "Log Id: ".$_ENV['lastErrorID'] . $message . PHP_EOL . " Pile des appels : " . chr(10) . $this->debug_string_backtrace();
        parent::emergency($this->getMessage($message), $context);
    }

}
