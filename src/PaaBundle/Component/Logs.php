<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PaaBundle\Component;

use App\PaaBundle\Component\MyLogger;
use \Monolog\Handler\StreamHandler;
use \Monolog\Handler\FirePHPHandler;
use \Monolog\Handler\HandlerInterface;
use \Monolog\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\InvalidArgumentException;
use Exception;
use Monolog\Formatter\LineFormatter;

/**
 * Description of Logs
 *
 * @author Haris
 */
class Logs {

    private static $logger;
	private static $projectDir ;
	private static $stackTrace ;
	
    static public function setProjectDir($projectDir) {
        self::$projectDir = $projectDir ;
    }
    static public function setStackTrace($stackTrace) {
        self::$stackTrace = $stackTrace ;
    }
    static public function info($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionInfoLogger', self::$projectDir, self::$stackTrace);
            $logger->info($message);
        }
    }

    static public function Rapportuser($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('RapportUser', self::$projectDir, self::$stackTrace);
            $logger->notice($message);
        }
    }

    static public function debug($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionDebugLogger', self::$projectDir, self::$stackTrace);
            $logger->debug($message);
        }
    }

    static public function notice($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionNoticeLogger', self::$projectDir, self::$stackTrace);
            $logger->notice($message);
        }
    }

    static public function warn($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionWarnLogger', self::$projectDir, self::$stackTrace);
            $logger->warn($message);
        }
    }

    static public function warning($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionWarningLogger', self::$projectDir, self::$stackTrace);
            $logger->warning($message);
        }
    }

    static public function err($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionErrLogger', self::$projectDir, self::$stackTrace);
            $logger->err($message);
        }
    }

    static public function error($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionErrorLogger', self::$projectDir, self::$stackTrace);
            $logger->error($message);
        }
    }

    static public function crit($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionCritLogger', self::$projectDir, self::$stackTrace);
            $logger->error($message);
        }
    }

    static public function critical($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionCriticalLogger', self::$projectDir, self::$stackTrace);
            $logger->critical($message);
        }
    }

    static public function alert($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionAlertLogger', self::$projectDir, self::$stackTrace);
            $logger->alert($message);
        }
    }

    static public function emerg($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionEmergLogger', self::$projectDir, self::$stackTrace);
            $logger->emerg($message);
        }
    }

    static public function emergency($message) {
        if (self::$logger == NULL) {
            $logger = new MyLogger('ExceptionEmergencyLogger', self::$projectDir, self::$stackTrace);
            $logger->emergency($message);
        }
    }

}
