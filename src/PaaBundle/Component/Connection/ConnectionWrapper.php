<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PaaBundle\Component\Connection;

use PDO;
use App\Limoog\PartageBundle\Component\cParametresBDD;

class ConnectionWrapper extends \Doctrine\DBAL\Connection {
	/**
	 * {@inheritDoc}
	 */
	private $bConnectedToPAA = 0;

	// Les paramètres transmis par Symfony au constructeur, début
	private $params ;
	private $driver ;
	private $config ;
	private $eventManager ;
	// Les paramètres transmis par Symfony au constructeur, fin

	private $container ;
	private $DBName ;
	
	/**
	 * Initializes a new instance of the Connection class.
	 *
	 * @param array                              $params       The connection parameters.
	 * @param \Doctrine\DBAL\Driver              $driver       The driver to use.
	 * @param \Doctrine\DBAL\Configuration|null  $config       The configuration, optional.
	 * @param \Doctrine\Common\EventManager|null $eventManager The event manager, optional.
	 *
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function __construct(array $params, \Doctrine\DBAL\Driver $driver, \Doctrine\DBAL\Configuration $config = null, \Doctrine\Common\EventManager $eventManager = null) {
		// LG 20200630 : rien n'est fait ici, mais dans le Initialise, qui est appellé après le setContainer, lequel est nécessaire pour connaitre sur quelle BDD travailler
		// (appel préparé dans \applicationPAA_S4\src\Kernel.php, méthode process)
		$this->params = $params ;
		$this->driver = $driver ;
		$this->config = $config ;
		$this->eventManager = $eventManager ;

		parent::__construct($this->params, $this->driver, $this->config, $this->eventManager);		
		
	}
	
	/**
	 * Méthode appellée par \applicationPAA_S4\src\Kernel.php, méthode process
	 * @param Container $container
	 */
	public function setContainer(\Symfony\Component\DependencyInjection\Container $container)
	{
		$this->container = $container;
		$this->DBName = $container->get('session')->get('DBName', "default") ;
		return ;
	}
	
	/**
	 * Effectue l'initialisation, une fois qu'on a reçu le container et qu'on connait la BDD sur laquelle travailler
	 * Méthode appellée par \applicationPAA_S4\src\Kernel.php, méthode process
	 *
	 * @param array                              $params       The connection parameters.
	 * @param \Doctrine\DBAL\Driver              $driver       The driver to use.
	 * @param \Doctrine\DBAL\Configuration|null  $config       The configuration, optional.
	 * @param \Doctrine\Common\EventManager|null $eventManager The event manager, optional.
	 *
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function initialise() {
if ($this->params['dbname'] === "ORT_50") {
	$toto = "toto" ;
}
			
//		$URL = $this->params['host'];
//// $URL = "51.15.181.176" ;
//		$port = $this->params['port'] ;
//		$dbname = $this->params['dbname'] ;
//		$user = $this->params['user'] ;
//		$pwd = $this->params['password'] ;
//		$driver = str_replace('pdo_', '', $this->params['driver']) ;
		$suffixe = ($this->DBName=="default") ? "" : "_" . $this->DBName ;

		$driver = str_replace('pdo_', '', getenv('doctrine_database_driver' . $suffixe)) ;
		$URL = getenv('doctrine_database_host' . $suffixe)	;
		$port = getenv('doctrine_database_port' . $suffixe) ;
		$dbname = getenv('doctrine_database_name' . $suffixe) ;
		$user = getenv('doctrine_database_user' . $suffixe) ;
		$pwd = getenv('doctrine_database_password' . $suffixe) ;
		
		$this->params['host'] = $URL ;
		$this->params['port'] = $port ;
		$this->params['dbname'] = $dbname ;
		$this->params['user'] = $user ;
		$this->params['password'] = $pwd ;
		
		// Ouvrir une connexion persistante
		$lsCnxnString = $driver . ":"
				. "host=" . $URL . ";"
				. "port=" . $port . ";"
				. "dbname=" . $dbname . ";" ;
		
// Ces fichiers de log seront créés dans le dossier web (sur le serveur : "/ort-sup/web")
// file_put_contents('cnxnString.txt', $lsCnxnString, FILE_APPEND);
// error_log($lsCnxnString, 3, 'cnxnString2.txt');
// throw new \Exception("cnxnString = " . $lsCnxnString . "|user=".$params['user']."|password=".$params['password'] . "|path=".getcwd()) ;

// LG 20200807 début : host, port, dbname sont vides ?
if (!$driver) {
// MG Modification 20200831 Début
// Le echo pose problème avec les réponses en JSON
//	echo "Erreur d'accès aux variables d'environnement lors de l'initialisation de la base de données<br>" ;
// MG Modification 20200831 Fin
//	echo "lsCnxnString = " . $lsCnxnString . "|<br>" ;
//	echo "driver = " . $driver . "|<br>" ;
//	echo "this->DBName = " . $this->DBName . "|<br>" ;
//	echo "suffixe = " . $suffixe . "|<br>" ;
//	echo "getenv('doctrine_database_driver') = " . getenv('doctrine_database_driver') . "|<br>" ;
//	echo "getenv('doctrine_database_driver' . suffixe) = " . getenv('doctrine_database_driver' . $suffixe) . "|<br>" ;

	$logger = new \App\PaaBundle\Component\MyLogger('onNotifieAction');
	$idLog = $logger->Log("Erreur d'accès aux variables d'environnement lors de l'initialisation de la base de données", "info", array(), true);
	
// 	throw new \Exception("le driver n'a pas été retrouvé !") ;

	// Cela semble se produire dans des contextes où la connexion n'est pas utilisée
	// Donc, faire un simple return semble suffire à résoudre le problème
	return ;
}
// LG 20200807 fin
		
		$loPDO = new PDO($lsCnxnString, $user, $pwd, array(PDO::ATTR_PERSISTENT => true));
		$params['pdo'] = $loPDO;
		$params['pdo']->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$params['driver'] = 'pdo_' . $params['pdo']->getAttribute(\PDO::ATTR_DRIVER_NAME);

		parent::__construct($this->params, $this->driver, $this->config, $this->eventManager);
		return ;
	}	

	public function connect() {
		// Appel à la procédure parente
		$lvConnected = parent::connect();

		// Effectuer l'identification sous Postgres dès l'ouverture de la connexion
		// Rend obsolète D:\Luc\Dev\DevWeb\Symfony\applicationPAA\src\PaaBundle\Component\EventListener\AVirer_PostConnectionListener.php ?
		if ($this->isConnected() && !$this->bConnectedToPAA) {
			// 1ere connexion : initialiser les éléments PAA
//			$lsVersionPAA = "test" ;
//			$liUser = "Null" ;
//			$liMachine = "Null" ;
//			$sql = "Select Paa.InitConnexionPaa('$lsVersionPAA', $liUser, $liMachine) As lOK" ;
//			$this->_conn->query($sql) ;
			$this->InitConnexionPaa(/* $this->_conn */);
		}
		return $lvConnected;
	}

	public function InitConnexionPaa($psVersionPAA = null, $piUser = null, $piMachine = null) {
		// 1ere connexion : initialiser les éléments PAA
		// $user = $security->getUser();
		// $poPDOConnexion = $poPDOConnexion?$poPDOConnexion:$this->_conn ;
		$lsVersionPAA = $psVersionPAA ? $psVersionPAA : (isset($_SESSION["gsVersionPAA"]) ? $_SESSION["gsVersionPAA"] : "Anonyme");

		$liUser = $piUser ? $piUser : (isset($_SESSION["giUser"]) ? $_SESSION["giUser"] : "Null");
		if ($piMachine) {
			
		} else if (isset($_SESSION["giMachine"])) {
			$liMachine = $_SESSION["giMachine"];
		} else {
			// L'Id de machine n'est pas encore défini : il faut e rechercher maintenent
			$loParamètresBDD = new cParametresBDD($this);
			$liMachine = $loParamètresBDD->rtvIdMachineByName(strtoupper(gethostname()));
			$_SESSION["giMachine"] = $liMachine;
		}
		$liMachine = $piMachine ? $piMachine : (isset($_SESSION["giMachine"]) ? $_SESSION["giMachine"] : "Null");

// Indiquer à Postgres le nom de l'application qui est connectée (selon code VFP)
// Textmerge("set application_name to '<<m._appli>>';"))

		$sql = "Select Paa.InitConnexionPaa('$lsVersionPAA', $liUser, $liMachine) As lOK";
		$this->_conn->query($sql);
		$this->bConnectedToPAA = 1;
	}

	// $psSQL : requête SQL
	public function rtvSQLResult($psSQL, $psFld = "", $pvDftValue = null, &$pbFound = true) {
		$loCur = $this->_conn->query(utf8_encode($psSQL), PDO::FETCH_ASSOC);
		$loRec = $loCur->fetch(PDO::FETCH_ASSOC);
		if ($loCur->rowCount() < 1) {
			$pbFound = false;
			$lvResultat = $pvDftValue;
		} else {
			if ($psFld) {
				// On a fourni le nom du champ
				$lvResultat = $loRec[$psFld];
			} else {
				// On prend la 1ere colonne
				$loRec = array_values($loRec);
				$lvResultat = $loRec[0];
			}
		}
		return $lvResultat;
	}

}
