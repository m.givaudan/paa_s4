<?php

namespace App\PaaBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use App\PaaBundle\Security\PaaVoter;

class PaaMenuBuilder implements ContainerAwareInterface {

	use ContainerAwareTrait;

	public function mainMenu(FactoryInterface $factory, array $options) {
		$menu = $factory->createItem('root');
		$menu->setChildrenAttribute('class', 'nav navbar-nav');

		$menu->addChild('Accueil', array('route' => 'paa_index'));
		$menu['Accueil']->setLinkAttribute('class', 'dropdown-toggle');
		$menu['Accueil']->setLinkAttribute('title', 'PAA : Planning Accueil Accompagnement');

// LG 20171214 deac : utilité ?
// Ne pas commenter : manière d'accéder aux services        $em = $this->container->get('doctrine')->getManager();
		$loController = $options["poController"];

		//Onglet Données de base
		if ($loController->LGisGranted("menu", PaaVoter::MENU_DONNEESDEBASE)) {
			$this->menuDonnéesDeBase($menu, $options);
		}
//MC 20200902 DEBUT
		//Onglet Autres listes
		if ($loController->LGisGranted("menu", PaaVoter::MENU_AUTRESLISTES)) {
			$this->menuAutresListes($menu, $options);
		}
//MC 20200902 FIN
		//Onglet Semaine
		if ($loController->LGisGranted("menu", PaaVoter::MENU_SEMAINEREELLE)) {
			$this->menuSemaineRéelle($menu);
		}

		//Onglet Semaine type
                /*
		if ($loController->LGisGranted("menu", PaaVoter::MENU_SEMAINETYPE)) {
			$this->menuSemaineType($menu);
		}
                */
                 
//MC 20200902 DEBUT
                //Onglet Editions
                if ($loController->LGisGranted("menu", PaaVoter::MENU_EDITIONS)) {
                    $menu->addChild('Editions', array('uri' => 'edit',));
                    $menu['Editions']->setLinkAttribute('class', 'dropdown-toggle');			
		}
//MC 20200902 FIN
		// Onglet Outils
		if ($loController->LGisGranted("menu", PaaVoter::MENU_OUTILS)) {
			$this->menuOutils($menu);
		}

		//Onglet Aide
		$this->menuAide($menu,$loController);

		//Onglet Connexion
		$this->menuConnexion($menu, $options);

		return $menu;
	}

	protected function menuConnexion($menu, array $options) {
		$lbAuthentifié = $options["bEstAuthentifié"];
		$lsMenu = 'Authentification';
		if ($lbAuthentifié) {
			// Il y a un utilisateur authentifié
			$loUser = $options['oUser'];
			if ($loUser !== null) {
				$lsMenu = $loUser->getusername();
			} else {
				$lbAuthentifié = false;
			}
		}
		$menu->addChild($lsMenu, array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown menuUser')
		));
		$menu[$lsMenu]->setLinkAttribute('class', ' compte dropdown-toggle');
		$menu[$lsMenu]->setLinkAttribute('data-toggle', 'dropdown');
		$menu[$lsMenu]->setChildrenAttribute('class', 'dropdown-menu');

		// Menu dans le cas où un user est actuellement identifié
		if ($lbAuthentifié) {
			$menu[$lsMenu]->addChild('Mon profil', array(
				'route' => 'paa_pageform',
				'routeParameters' => array('psNomTable' => 'users', 'piId' => $loUser->getid()),
// LG : depuis Symfony4, cette valeur empêche d'utiliser la route définie en 'route'
// 				'uri' => '#'
			));
			$menu[$lsMenu]['Mon profil']->setLinkAttribute('class', 'drop-links d-flex Dpx');


			if ($loUser->isLPeutChangerProfil()) {
				$menu[$lsMenu]->addChild('Changer de mot de passe', array(
					'route' => 'fos_user_change_password',
				));
				$menu[$lsMenu]['Changer de mot de passe']->setLinkAttribute('class', 'drop-links d-flex Dpx');
			}
			if ($loUser->isLPeutChangerProfil()) {
				$menu[$lsMenu]->addChild('Réinitialiser le mot de passe', array(
					'route' => 'fos_user_resetting_request',
				));
				$menu[$lsMenu]['Changer de mot de passe']->setLinkAttribute('class', 'drop-links d-flex Dpx');
			}
			$menu[$lsMenu]->addChild('Deconnexion', array(
				'route' => 'fos_user_security_logout',
			));
			$menu[$lsMenu]['Deconnexion']->setLinkAttribute('class', 'drop-links d-flex Dpx');
		} else {
			$menu[$lsMenu]->addChild('Authentification', array(
				'route' => 'fos_user_security_login',
			));
			$menu[$lsMenu]['Authentification']->setLinkAttribute('class', 'drop-links d-flex Dpx');

			$menu[$lsMenu]->addChild('Inscription', array(
				'route' => 'fos_user_registration_register',
			));
			$menu[$lsMenu]['Inscription']->setLinkAttribute('class', 'drop-links d-flex Dpx');
			
			$menu[$lsMenu]->addChild('Mot de passe oublié', array(
				'route' => 'fos_user_resetting_request',
			));
			$menu[$lsMenu]['Inscription']->setLinkAttribute('class', 'drop-links d-flex Dpx');
			
		}
	}

	protected function menuDonnéesDeBase($menu, $options) {
		$menu->addChild('Données de base', array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown')
		));
		$menu['Données de base']->setLinkAttribute('class', 'dropdown-toggle');
		$menu['Données de base']->setLinkAttribute('data-toggle', 'dropdown');
		$menu['Données de base']->setChildrenAttribute('class', 'dropdown-menu');

		$menu['Données de base']->addChild('Activités de base', array(
			'route' => 'actiBasesTreeView',
// LG 20190703 deac            'routeParameters' => array('psNomTable' => 'actiBaseSousGroupeGroupe')
		));
		$menu['Données de base']['Activités de base']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Données de base']->addChild('Equipements', array(
			// 'uri' => '#',
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'salles')
		));
		$menu['Données de base']['Equipements']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Données de base']->addChild('Intervenants', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'intervenants')
		));
		$menu['Données de base']['Intervenants']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Données de base']->addChild('Groupes', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'groupes')
		));
		$menu['Données de base']['Groupes']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Données de base']->addChild('Usagers', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'usagers')
		));
		$menu['Données de base']['Usagers']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Données de base']->addChild('Groupes', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'groupes')
		));
		$menu['Données de base']['Groupes']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Données de base']->addChild('Horaires et absences', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Données de base']['Horaires et absences']->setAttribute('class', 'disabled');

		// Menu de gestion des users
		$loController = $options["poController"];
		if ($loController->LGisGranted("menu", PaaVoter::MENU_USERS)) {
			$menu['Données de base']->addChild('Utilisateurs', array(
				'route' => 'paa_pageavecgrille',
				'routeParameters' => array('psNomTable' => 'users')
			));
			$menu['Données de base']['Horaires et absences']->setLinkAttribute('class', 'drop-links d-flex Dpx');
			$menu['Données de base']['Utilisateurs']->setLinkAttribute('class', 'drop-links d-flex Dpx');
		}

		//sous-liste dans l'Onglet Données de base
		/*
                $lsNomMenu = "Autres listes ...";
		$menu['Données de base']->addChild($lsNomMenu, array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown-submenu')
		));
		$this->menuListes($menu, $lsNomMenu);
               */
		
		
	}
//MC 20200902 DEBUT
/*
	protected function menuListes($menu, $psNomMenu) {
		$menu['Données de base'][$psNomMenu]->setLinkAttribute('class', 'drop-links d-flex Dpx');
		$menu['Données de base'][$psNomMenu]->setChildrenAttribute('class', 'dropdown-menu');

		$menu['Données de base'][$psNomMenu]->addChild('Activités de base : groupes', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'actiGroupesacti')
		));
		$menu['Données de base'][$psNomMenu]->addChild('Activités de base : sous-groupes', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'actiSsgroupesacti')
		));

		$menu['Données de base'][$psNomMenu]->addChild('Années', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'annees')
		));
		$menu['Données de base'][$psNomMenu]->addChild('Etablissements', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'etablissements')
		));
		$menu['Données de base'][$psNomMenu]->addChild('Spécialités des intervenants', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'specialites')
		));
		$menu['Données de base'][$psNomMenu]->addChild('Services', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'services')
		));
		$menu['Données de base'][$psNomMenu]->addChild('Organismes', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'caisses')
		));

		$menu['Données de base'][$psNomMenu]->addChild('Horaires', array(
			'uri' => '#'
				 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') 
		));
		$menu['Données de base'][$psNomMenu]['Horaires']->setAttribute('class', 'disabled');

		$menu['Données de base'][$psNomMenu]->addChild('Périodes de récapitulatifs', array(
			'uri' => '#'
				 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') 
		));
		$menu['Données de base'][$psNomMenu]['Périodes de récapitulatifs']->setAttribute('class', 'disabled');
		$menu['Données de base'][$psNomMenu]->addChild('Horaires critiques', array(
			'uri' => '#'
				 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') 
		));
		// AV 04/03/2019 début
		$menu['Données de base'][$psNomMenu]['Horaires critiques']->setAttribute('class', 'disabled');
		// AV 04/03/2019 fin

		$menu['Données de base'][$psNomMenu]->addChild('Modèles horaires', array(
			'uri' => '#'
				 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') 
		));
		$menu['Données de base'][$psNomMenu]['Modèles horaires']->setAttribute('class', 'disabled');
		$menu['Données de base'][$psNomMenu]->addChild('Jours fériés', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'joursferies')
		));
		$menu['Données de base'][$psNomMenu]->addChild('Compteurs', array(
			'uri' => '#'
				 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') 
		));
		$menu['Données de base'][$psNomMenu]['Compteurs']->setAttribute('class', 'disabled');
		$menu['Données de base'][$psNomMenu]->addChild('Numérotation des absences', array(
			'uri' => '#'
				 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') 
		));
		$menu['Données de base'][$psNomMenu]['Numérotation des absences']->setAttribute('class', 'disabled');
		$menu['Données de base'][$psNomMenu]->addChild('Conventions collectives', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'conventions')
		));
	}*/
        
        protected function menuAutresListes($menu, $options) {
            $sousMenu = 'Autres Listes';
            $menu->addChild('Autres Listes', array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown')
		));
		$menu[$sousMenu]->setLinkAttribute('class', 'dropdown-toggle');
		$menu[$sousMenu]->setLinkAttribute('data-toggle', 'dropdown');
		$menu[$sousMenu]->setChildrenAttribute('class', 'dropdown-menu');
                
		$menu[$sousMenu]->addChild('Années', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'annees')
		));
                $menu[$sousMenu]['Années']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                
                $menu[$sousMenu]->addChild('Caisses', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'caisses')
		));
                $menu[$sousMenu]['Caisses']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                
		$menu[$sousMenu]->addChild('Compteurs', array(
			
				 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => 'compteurs') 
		));
		$menu[$sousMenu]['Compteurs']->setLinkAttribute('class', 'drop-links d-flex Dpx');
		
		$menu[$sousMenu]->addChild('Conventions collectives', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'conventions')
		));
                $menu[$sousMenu]['Conventions collectives']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                
                
		$menu[$sousMenu]->addChild('Etablissements', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'etablissements')
		));
                $menu[$sousMenu]['Etablissements']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                
		$menu[$sousMenu]->addChild('Jours fériés', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'joursferies')
		));
                $menu[$sousMenu]['Jours fériés']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                
		$menu[$sousMenu]->addChild('Services', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'services')
		));
                $menu[$sousMenu]['Services']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                
		$menu[$sousMenu]->addChild('Spécialités des intervenants', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'specialites')
		));
                $menu[$sousMenu]['Spécialités des intervenants']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                		
                $menu[$sousMenu]->addChild('Types de caisses', array(
			'route' => 'paa_pageavecgrille',
			'routeParameters' => array('psNomTable' => 'caissesType')
		));
                $menu[$sousMenu]['Types de caisses']->setLinkAttribute('class', 'drop-links d-flex Dpx');
                      
        }
//MC 20200902 FIN

	protected function menuSemaineRéelle($menu) {
		$menu->addChild('Semaine', array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown')
		));
		$menu['Semaine']->setLinkAttribute('class', 'dropdown-toggle');
		$menu['Semaine']->setLinkAttribute('data-toggle', 'dropdown');
		$menu['Semaine']->setChildrenAttribute('class', 'dropdown-menu');
		$menu['Semaine']->addChild('Activités', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));

//MG Modification  Début 20200201
//        $menu['Semaine']['Activités']->setAttribute('class', 'disabled');
		$menu['Semaine']->addChild('Activités', array('route' => 'paa_pageavecgrille'
			, 'routeParameters' => array('psNomTable' => 'activitesTrad')
				)
		);
		$menu['Semaine']['Activités']->setLinkAttribute('class', 'drop-links d-flex Dpx');

//MG Modification  Fin 20200201
//MG Ajout 20200218 Début
		$menu['Semaine']->addChild('Séances', array('route' => 'paa_pageavecgrille'
			, 'routeParameters' => array('psNomTable' => 'seancesTrad')
				)
		);
		$menu['Semaine']['Séances']->setLinkAttribute('class', 'drop-links d-flex Dpx');
//MG Ajout 20200218 Fin

		$menu['Semaine']->addChild('Planning', array('route' => 'paa_planning'
			, 'routeParameters' => array('pvSemaine' => '', 'psLstRes' => '')
				)
		);
		$menu['Semaine']['Planning']->setLinkAttribute('class', 'drop-links d-flex Dpx');

//        $menu['Semaine']['Planning']->setAttribute('class', 'disabled');
		$menu['Semaine']->addChild('Gestion des conflits', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Semaine']['Gestion des conflits']->setAttribute('class', 'disabled');
		$menu['Semaine']->addChild('Créer', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Semaine']['Gestion des conflits']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Semaine']['Créer']->setAttribute('class', 'disabled');
		$menu['Semaine']->addChild('Journal de création', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Semaine']['Créer']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Semaine']['Journal de création']->setAttribute('class', 'disabled');
		$menu['Semaine']['Journal de création']->setLinkAttribute('class', 'drop-links d-flex Dpx');
	}

	protected function menuSemaineType($menu) {
		$menu->addChild('Semaine type', array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown')
		));
		$menu['Semaine type']->setLinkAttribute('class', 'dropdown-toggle');
		$menu['Semaine type']->setLinkAttribute('data-toggle', 'dropdown');
		$menu['Semaine type']->setChildrenAttribute('class', 'dropdown-menu');
		$menu['Semaine type']->addChild('Activités', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Semaine type']['Activités']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Semaine type']['Activités']->setAttribute('class', 'disabled');
		$menu['Semaine type']->addChild('Planning', array('route' => 'paa_planning'
			, 'routeParameters' => array('pvSemaine' => -1, 'psLstRes' => '')
				)
		);
		$menu['Semaine type']['Planning']->setLinkAttribute('class', 'drop-links d-flex Dpx');

//        $menu['Semaine type']['Planning']->setAttribute('class', 'disabled');
		$menu['Semaine type']->addChild('Gestion des conflits', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Semaine type']['Gestion des conflits']->setAttribute('class', 'disabled');
		$menu['Semaine type']['Gestion des conflits']->setLinkAttribute('class', 'drop-links d-flex Dpx');
	}

	protected function menuOutils($menu) {
		$menu->addChild('Outils', array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown')
		));
		$menu['Outils']->setLinkAttribute('class', 'dropdown-toggle');
		$menu['Outils']->setLinkAttribute('data-toggle', 'dropdown');
		$menu['Outils']->setChildrenAttribute('class', 'dropdown-menu');
		$menu['Outils']->addChild('Paramètres généraux', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Outils']['Paramètres généraux']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Outils']['Paramètres généraux']->setAttribute('class', 'disabled');
		$menu['Outils']->addChild('Paramètres plannings', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Outils']['Paramètres plannings']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Outils']['Paramètres plannings']->setAttribute('class', 'disabled');
		$menu['Outils']->addChild('Outils', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Outils']['Outils']->setAttribute('class', 'disabled');
		$menu['Outils']->addChild('d1');
		$menu['Outils']['d1']->setAttribute('class', 'dropdown-divider');
		$menu['Outils']->addChild('Vérif. / convention', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Outils']['Outils']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Outils']['Vérif. / convention']->setAttribute('class', 'disabled');
		$menu['Outils']->addChild('d2');
		$menu['Outils']['d2']->setAttribute('class', 'dropdown-divider');
		$menu['Outils']->addChild('Sécurité', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Outils']['Vérif. / convention']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Outils']['Sécurité']->setAttribute('class', 'disabled');
		$menu['Outils']->addChild('Sauvegarde', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Outils']['Sécurité']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Outils']['Sauvegarde']->setAttribute('class', 'disabled');
		$menu['Outils']->addChild('Restaurer', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['Outils']['Sauvegarde']->setLinkAttribute('class', 'drop-links d-flex Dpx');

		$menu['Outils']['Restaurer']->setAttribute('class', 'disabled');
		$menu['Outils']['Restaurer']->setLinkAttribute('class', 'drop-links d-flex Dpx');
	}

	protected function menuAide($menu, $loController) {
		$menu->addChild('?', array(
			'uri' => '#',
			'attributes' => array('class' => 'dropdown')
		));
		$menu['?']->setLinkAttribute('class', 'dropdown-toggle');
		$menu['?']->setLinkAttribute('data-toggle', 'dropdown');
		$menu['?']->setChildrenAttribute('class', 'dropdown-menu');
		$menu['?']->addChild('A propos de ...', array(
			'uri' => '#'
				/* 'route' => 'paa_pageavecgrille',
				  'routeParameters' => array(
				  'psNomTable' => '') */
		));
		$menu['?']['A propos de ...']->setLinkAttribute('class', 'drop-links d-flex Dpx');
		$menu['?']['A propos de ...']->setAttribute('class', 'disabled');

        if ($loController->estDevMode()){	// MC 20200915
			$menu['?']->addChild('Evolutions', array(
				'uri' => '#'
					/* 'route' => 'paa_pageavecgrille',
					  'routeParameters' => array(
					  'psNomTable' => '') */
			));
			$menu['?']['Evolutions']->setAttribute('class', 'disabled');
			$menu['?']['Evolutions']->setLinkAttribute('class', 'text-danger drop-links d-flex Dpx');
		} // MC 20200915

	}

}
