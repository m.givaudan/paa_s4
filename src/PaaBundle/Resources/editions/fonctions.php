<?php
// 20190905 début
// Ajouté pour compilation de Symfony sur le serveur de prod (pourquoi que sur le serveur de prod ?)

// LG 20201001 deac namespace App\PaaBundle\Resources\editions;

class fonctions {
	
}

// 20190905 fin
?>
<!DOCTYPE HTML>
<html>
    <head><title>test</title>
    </head>
	<body>
		<?php

		// afficheCouleur(255, 0, 0); echo "<br>" ;
		// afficheCouleur(0, 255, 0); echo "<br>" ;
		// afficheCouleur(0, 0, 255); echo "<br>" ;

		function afficheCouleur($red, $green, $blue) {
			echo "RGB(" . $red . ", " . $green . ", " . $blue . ") = " . RGB($red, $green, $blue);

			echo '<span style = "background-color: RGB(' . $red . ', ' . $green . ', ' . $blue . ')' . '">couleur</span>';

			$CouleurDec = RGB($red, $green, $blue);

			$CouleurHex = CouleurRGBToHex($CouleurDec);
			echo '<span style = "background-color: ' . $CouleurHex . '";>CouleurConvertie : ' . $CouleurHex . '</span>';
		}

		function CouleurRGBToHex($couleur) {

			$Reste1 = fmod($couleur, 256 * 256);
			$blue = ($couleur - $Reste1) / (256 * 256);
			$Reste2 = fmod($Reste1, 256);

			$green = ($Reste1 - $Reste2) / 256;

			$red = $Reste2;

			$val = '#' . str_pad(dechex($red), 2, '0', STR_PAD_LEFT)
					. str_pad(dechex($green), 2, '0', STR_PAD_LEFT)
					. str_pad(dechex($blue), 2, '0', STR_PAD_LEFT);

			if ($val == '#000000') {
				$val = '#ffffff';
				return $val;
			} else {
				return $val;
			}
		}

		function RGB($red, $green, $blue) {
			return 256 * $blue * 256 + $green * 256 + $red;
		}

		function corrigeChevrons($psTexte) {
			if ($_SESSION['corrigeChevrons'] == 1) {
				$lsCorrige1 = array("<", ">");
				$lsCorrige2 = array("&lt;", "&gt");

				$lsCorrige3 = str_replace($lsCorrige1, $lsCorrige2, $psTexte);

				return $lsCorrige3;
			} else {
				return $psTexte;
			}
		}
		?>
	</body>
</html>