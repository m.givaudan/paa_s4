<?php
// 20190905 début
// Ajouté pour compilation de Symfony sur le serveur de prod (pourquoi que sur le serveur de prod ?)

// LG 20021001 début
// namespace App\PaaBundle\Resources\editions;
 use App\PaaBundle\Resources\editions\reportico_LG;
// LG 20021001 fin


class listings {
	
}

// 20190905 fin
?>
<!DOCTYPE HTML>
<html>
    <head><title>listing</title>
		<link href="reportico-4.6/css/reportico_bootstrap.css" rel="stylesheet">
		<link href="reportico-4.6/js/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
    </head>
	<body>

		<?php
//  require_once ("../../../reportico.php");
//if (isset($_GET['choix'])) {
//	executeRapport($_GET['choix'], $_GET['choix1'], $_GET['identifiant']) ;
//}
		require_once ("reportico_LG.php");
		require_once ("fonctions.php");

		function executeListing(string $psTable, string $psDestination, string $psParametres, $poDoctrineConnexion) {

			$q = new reportico_LG();
			$q->clear_reportico_session = true;
			$q->access_mode = "REPORTOUTPUT";
			$q->initial_execute_mode = "EXECUTE";
			$q->projects_folder = dirname(__Dir__);
			$q->initial_project = "editions";
			$q->initial_project_password = "admin";
			$q->bootstrap_styles = "3";
			$q->force_reportico_mini_maintains = true;
			$q->bootstrap_preloaded = true;
			$q->clear_reportico_session = true;

// LG 20180705 old	$_SESSION['gsNomEtablissement'] = "Etablissement de test" ; 
// LG 20180705 old	$q->user_parameters["gsNomEtablissement"] = $_SESSION['gsNomEtablissement'];
			$q->user_parameters["gsNomEtablissement"] = "Etablissement de test";

			if ($psDestination == "PDF") {
				$q->initial_output_format = "PDF";
			} else if ($psDestination == "CSV") {
				$q->initial_output_format = "CSV";
// LG 20180704 old	} else if($_GET['choix1']==HTML){
			} else if ($psDestination == "HTML") {
				$_SESSION['corrigeChevrons'] = 1;
			} else {
				$_SESSION['corrigeChevrons'] = 2;
			}

			$lbKO = False;
			if ($psTable == "salles") {
				$lbKO = !prépareListing_Salles($q, $psParametres);
			}

			if ($psTable == "annees") {
				$lbKO = !prépareListing_Annees($q, $psParametres);
			}

			if ($psTable == "usagers") {
				$lbKO = !prépareListing_Usagers($q, $psParametres);
			}

			if ($psTable == "groupes") {
				$lbKO = !prépareListing_Groupes($q, $psParametres);
			}

			if ($psTable == "intervenants") {
				$lbKO = !prépareListing_Intervenants($q, $psParametres);
			}

			if ($psTable == "actibases") {
				$lbKO = !prépareListing_Actibases($q, $psParametres);
			}

			if (!$lbKO) {
				// La préparation est OK : on peut exécuter
// LG 20180704 début
				if (true /* defined ("SW_DB_TYPE") && SW_DB_TYPE == 'existingconnection' */) {
					// Utiliser la connexion de Doctrine
					define('SW_DB_TYPE', 'existingconnection');
					$q->external_connection = $poDoctrineConnexion->getWrappedConnection();
				} else {
					// Utiliser les informations de la connexion de Doctrine
					define('SW_FRAMEWORK_DB_DRIVER', $poDoctrineConnexion->getDriver()->getName()); //e.g. pdo_mysql, pdo_pgsql, pdo_mssql etc
					define('SW_FRAMEWORK_DB_USER', $poDoctrineConnexion->getUsername());
					define('SW_FRAMEWORK_DB_PASSWORD', $poDoctrineConnexion->getPassword());
					define('SW_FRAMEWORK_DB_HOST', $poDoctrineConnexion->getHost()); // Use ip:port to specifiy a non standard port
					define('SW_FRAMEWORK_DB_DATABASE', $poDoctrineConnexion->getDatabase());
				}
				// Augmenter le délai d'exécution, pour les rapports longs à générer
				$Old_max_execution_time = ini_get('max_execution_time');
				ini_set('max_execution_time', 300);
// LG 20180704 fin
				$q->execute();
// LG 20180704 fin
// 
				// Restaurer le délai d'exécution, pour les rapports longs à générer
				ini_set('max_execution_time', $Old_max_execution_time);
// LG 20180704 fin
			}
		}

		function prépareListing_Salles(reportico_LG $q, string $psParametres) {
			$q->initial_report = "listingEquipements";
			$q->user_parameters["ietablissement"] = "-1";
			$q->user_parameters["iid_salle"] = "iid_salle";
			return true;
		}

		function prépareListing_Annees(reportico_LG $q, string $psParametres) {
			$q->initial_report = "annees";
			$q->user_parameters["iid_annee"] = "iid_annee";
			return true;
		}

		function prépareListing_Usagers(reportico_LG $q, string $psParametres) {
			$q->initial_report = "Usagers";
			$q->user_parameters["iid_usager"] = "iid_usager";
			return true;
		}

		function prépareListing_Groupes(reportico_LG $q, string $psParametres) {
			$q->initial_report = "Groupes d'usagers";
			$q->user_parameters["iid_groupe"] = "iid_groupe";
			return true;
		}

		function prépareListing_Intervenants(reportico_LG $q, string $psParametres) {
			$q->initial_report = "Intervenants";
			$q->user_parameters["iid_intervenant"] = "iid_intervenant";
			return true;
		}

		function prépareListing_Actibases(reportico_LG $q, string $psParametres) {
			$q->initial_report = "activités de bases";
			$q->user_parameters["iid_actibase"] = "iid_actibase";
			return true;
		}
		?>
	</body>
</html>