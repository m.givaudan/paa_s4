CREATE SEQUENCE paa.typecaisses_seq START 17;

CREATE TABLE paa.ttypecaisses
(
    iid_typecaisses INT PRIMARY KEY NOT NULL DEFAULT nextval('paa.typecaisses_seq'),
    cnom CHARACTER varying(180),
    ctableusagers_caisses CHARACTER varying(180),
    cnomdft CHARACTER varying(180)
);

INSERT INTO paa.ttypecaisses(iid_typecaisses,cnom,ctableusagers_caisses,cnomdft)
    VALUES
    (1,'CPAM','Usagers_CPAM','CPAM'),
    (2,'Mesure de Protection','Usagers_Protection','Mesure de Protection'),
    (3,'Resp. civile','Usagers_RespCivile','Resp. civile'),
    (4,'MDPH','Usagers_MDPH','MDPH'),
    (5,'Aide Sociale','Usagers_AideSociale','Aide Sociale'),
    (6,'Mutuelle','Usagers_Mutuelle','Mutuelle'),
    (7,'CAF','Usagers_CAF','CAF'),
    (8,'CCAS','Usagers_CCAS','CCAS'),
    (9,'CDAPH','Usagers_CDAPH','CDAPH'),
    (10,'RQTH','Usagers_RQTH','RQTH'),
    (11,'Pole emploi','Usagers_ANPE','Pole emploi'),
    (12,'Scolarité','Usagers_Scolarite','Scolarité'),
    (13,'Logement','Usagers_Logement','Logement'),
    (14,'Ressources','Usagers_Ressources','Ressources'),
    (15,'Lieux de départ et d''arrivée','','Lieux de départ et d''arrivée'),
    (16,'Soins','Usagers_Soins','Soins');



ALTER TABLE paa.caisses
ADD FOREIGN KEY (itype) REFERENCES paa.ttypecaisses(iid_typecaisses);