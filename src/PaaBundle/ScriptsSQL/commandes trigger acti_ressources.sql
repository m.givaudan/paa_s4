--Trigger acti_ressources

LG 20200512 : OBSOLETE, à présent inclus dans Corrections.sql

Create Or Replace function paa.getType_Res(piId_Res int) Returns Char(1) As $getType_Res$

begin

    return (

    With cteRessources As (

        Select 'I' As cType_Res, iId_Intervenant As iId_Res From Paa.Intervenants
        UNION
        Select 'U' As cType_Res, iId_Usager As iId_Res From Paa.Usagers
        UNION
        Select 'G' As cType_Res, iId_Groupe As iId_Res From Paa.Groupes
        UNION
        Select 'S' As cType_Res, iId_Salle As iId_Res From Paa.Salles
        UNION
        Select 'E' As cType_Res, iId_Etablissement As iId_Res From Paa.Etablissements
        )
        Select cType_Res From cteRessources Where iId_Res = piId_Res
        ) ;
End;$getType_Res$ Language PLPGSQL STABLE ;

CREATE OR REPLACE FUNCTION paa.ftrigbiu_acti_ressources()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
	if new.ctype_res is NULL THEN
		new.ctype_res = paa.getType_Res(new.iid_res);
	end if;
	return NEW;
END;
$BODY$;

ALTER FUNCTION paa.ftrigbiu_acti_ressources()
    OWNER TO postgres;

Drop Trigger If exists trigbiu_acti_ressources ON paa.acti_ressources ;
CREATE Trigger trigbiu_acti_ressources
BEFORE INSERT OR UPDATE 
ON paa.acti_ressources
FOR EACH ROW
EXECUTE PROCEDURE paa.ftrigbiu_acti_ressources();

UPDATE paa.acti_ressources SET ctype_res = paa.gettype_res(iid_res) WHERE ctype_res is NULL;