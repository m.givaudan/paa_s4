-- Scripts de correction des données de PAA

Select paa.InitConnexionPaa('corrections') ;

Update paa.Intervenants Set iSpecialite = Null Where iSpecialite = 0 ;
Update paa.Intervenants Set iService = Null Where iService = 0 ;
Update paa.Salles Set iPermis = Null Where iPermis = 0 ;

ALTER table paa_securite.users ADD column If not exists lpeutchangerprofil boolean NOT NULL DEFAULT TRUE;

--Trigger acti_ressources

CREATE OR REPLACE function paa.ftrigbiu_acti_ressources()
RETURNS Trigger as $ftrigbiu_acti_ressources$
BEGIN
	if new.ctype_res is NULL THEN
		SELECT paa.getType_Res(new.iid_res);
	end if;
	return NEW;
END;
$ftrigbiu_acti_ressources$ 
Language PLPGSQL;
CREATE Trigger trigbiu_acti_ressources
BEFORE INSERT OR UPDATE 
ON paa.acti_ressources
FOR EACH ROW
EXECUTE PROCEDURE paa.ftrigbiu_acti_ressources();

-- ****************************************************************************************************
Create Or Replace function paa.getType_Res(piId_Res int) Returns Char(1) As $getType_Res$

begin

    return (

    With cteRessources As (

        Select 'I' As cType_Res, iId_Intervenant As iId_Res From Paa.Intervenants
        UNION
        Select 'U' As cType_Res, iId_Usager As iId_Res From Paa.Usagers
        UNION
        Select 'G' As cType_Res, iId_Groupe As iId_Res From Paa.Groupes
        UNION
        Select 'S' As cType_Res, iId_Salle As iId_Res From Paa.Salles
        UNION
        Select 'E' As cType_Res, iId_Etablissement As iId_Res From Paa.Etablissements
        )
        Select cType_Res From cteRessources Where iId_Res = piId_Res
        ) ;
End;$getType_Res$ Language PLPGSQL STABLE ;

-- 20200508 *************************************************************************************************************************************************
-- Function: paa.ftrigbiud_acti_ressources()
-- DROP FUNCTION paa.ftrigbiud_acti_ressources();
CREATE OR REPLACE FUNCTION paa.ftrigbiud_acti_ressources()
  RETURNS trigger AS
$BODY$
		Declare 
lbLog boolean = True ;
			loRecTrigger Record ;
			lbSemaineValidée Boolean = False ;
		Begin
		If lbLog Then
			Declare
				lcCurrent_Query varchar ;
				lcSepa varchar = '=================================================================================================================' || Chr(13) ;
			Begin
--&&				lcCurrent_Query = 'Trigger : ' || TG_NAME || CHR(13) || 'Quand : ' || TG_WHEN || CHR(13) || 'Opération : ' || TG_OP|| CHR(13);
				lcCurrent_Query = 'Trigger ' || TG_NAME || ', ' || TG_WHEN || ' ' || TG_OP || ' sur ' || TG_TABLE_SCHEMA || '.' || TG_TABLE_NAME || CHR(13);
--&&				Raise Info 'PAA.TraiteTrigger_Acti_Ressources, Clock = %, %%%%%', Clock_TimeStamp(),  lcSepa, Chr(13), lcCurrent_Query, CHR(13), lcSepa ;
				Raise Info 'PAA.TraiteTrigger_Acti_Ressources, Clock = %, % % % %', Clock_TimeStamp(),  chr(13), lcSepa, lcCurrent_Query, lcSepa ;
			End ;
		End If ;
			
			--&& Objet à renvoyer
			If TG_OP = 'DELETE' Then
				loRecTrigger = Old ;
			Else
				loRecTrigger = New ;
			End If ;

		 	--&& ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		 	--&& La semaine est-elle validée
	 		lbSemaineValidée = Paa.EstActiPoseeSurSemaineValidee(loRecTrigger.iActi) ;
		 	If TG_OP = 'UPDATE' Then 
			 	If lbSemaineValidée  Then
			 		--&& RAS
			 	Elseif New.iActi is distinct from Old.iActi Then
		 			--&& Il faut tester aussi Old.iActi
		 			lbSemaineValidée = Paa.EstActiPoseeSurSemaineValidee(Old.iActi) ;
				End If ;
		 	End If ;
	
		 	if lbSemaineValidée Then
		 		--&& Impossible de modifier une semaine validée
		 		Perform Partage.RaiseErreur('PAA.TraiteTrigger_Acti_Ressources', 'Select * From  PAA.ftrigBIUD_Acti_Ressources(' || ') ;' || Chr(13), 'Tentative de modification d''une semaine validée');
		 		Raise Exception 'PAA.TraiteTrigger_Acti_Ressources, Clock = %, Tentative de modification d''une semaine validée', Clock_TimeStamp() ; 
		 		Return Null ;
		 	End If ;

--&& LG 20200508 début
		 	--&& ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
			--&& Si le type de ressource n'est pas fourni (par exemple dans le cas d'une mise à jour par Symfony), il faut le retrouver et l'ajouter
		 	If TG_OP = 'UPDATE' Or TG_OP = 'INSERT' Then 
		 		If new.cType_Res is Null Then
--&& Raise Exception 'PAA.TraiteTrigger_Acti_Ressources, Clock = %, devel', Clock_TimeStamp() ; 
--&& loRecTrigger.cType_Res = 'Z' ;
					select cType_Res Into loRecTrigger.cType_Res From (
						select 'I' As cType_Res From paa.Intervenants Where iId_Intervenant = new.iId_Res
						Union
						select 'U' As cType_Res From paa.Usagers Where iId_Usager = new.iId_Res
						Union
						select 'G' As cType_Res From paa.Groupes Where iId_Groupe = new.iId_Res
						Union
						select 'S' As cType_Res From paa.Salles Where iId_Salle = new.iId_Res
						Union
						select 'E' As cType_Res From paa.Etablissements Where iId_Etablissement = new.iId_Res
						) res ;
				End if ;
		 	End If ;
--&& LG 20200508 fin

			--&& Valeur de retour ;
			Return loRecTrigger ;
		Exception
			-- Gestion en cas d'erreur
			-- Par défaut une exception annule la transaction en cours
			WHEN others then
				Declare
					lcFuncCall Text ;
				Begin
					lcFuncCall = 'Select * From  PAA.ftrigBIUD_Acti_Ressources(' || ') ;' || Chr(13) ;
					Perform Partage.AddErreurClient(1, SQLERRM::Varchar, 'Postgres', 'Inconnue', lcFuncCall) ;
					Raise Exception '%Dans PAA.TraiteTrigger_Acti_Ressources : SQLSTATE = %, %'
						, SQLERRM
						, SQLSTATE
						, 'Select * From  PAA.ftrigBIUD_Acti_Ressources(' || ') ;' || Chr(13);
				End ;
		End; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION paa.ftrigbiud_acti_ressources()
  OWNER TO "ORT";

-- 20200512, repris de commandes trigger acti_ressources.sql *************************************************************************************************************************************************
Create Or Replace function paa.getType_Res(piId_Res int) Returns Char(1) As $getType_Res$
begin

    return (

    With cteRessources As (

        Select 'I' As cType_Res, iId_Intervenant As iId_Res From Paa.Intervenants
        UNION
        Select 'U' As cType_Res, iId_Usager As iId_Res From Paa.Usagers
        UNION
        Select 'G' As cType_Res, iId_Groupe As iId_Res From Paa.Groupes
        UNION
        Select 'S' As cType_Res, iId_Salle As iId_Res From Paa.Salles
        UNION
        Select 'E' As cType_Res, iId_Etablissement As iId_Res From Paa.Etablissements
        )
        Select cType_Res From cteRessources Where iId_Res = piId_Res
        ) ;
End;$getType_Res$ Language PLPGSQL STABLE ;

-- ***********************************************************************************************************
-- Etre certain que le champ cType_Res est renseigné dans la table Acti_Ressources (trop difficile d'envoyer directement la valeur par symfony)
CREATE OR REPLACE FUNCTION paa.ftrigbiu_acti_ressources()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
	if new.ctype_res is NULL THEN
		new.ctype_res = paa.getType_Res(new.iid_res);
	end if;
	return NEW;
END;
$BODY$;

ALTER FUNCTION paa.ftrigbiu_acti_ressources()
    OWNER TO postgres;

Drop Trigger If exists trigbiu_acti_ressources ON paa.acti_ressources ;
CREATE Trigger trigbiu_acti_ressources
BEFORE INSERT OR UPDATE 
ON paa.acti_ressources
FOR EACH ROW
EXECUTE PROCEDURE paa.ftrigbiu_acti_ressources();

UPDATE paa.acti_ressources SET ctype_res = paa.gettype_res(iid_res) WHERE ctype_res is NULL;
