CREATE SCHEMA If not exists paa_securite AUTHORIZATION postgres;

DROP View IF EXISTS Paa.users;
-- DROP TABLE paa_securite.users;
CREATE TABLE paa_securite.users
(
  iid_user serial NOT NULL,
  ctype_res character varying(1),
  iid_res integer,
  username character varying(180) NOT NULL,
  username_canonical character varying(180) NOT NULL,
  email character varying(180) NOT NULL,
  email_canonical character varying(180) NOT NULL,
  enabled boolean NOT NULL,
  salt character varying(255) DEFAULT NULL::character varying,
  password character varying(255) NOT NULL,
  last_login timestamp without time zone,
  confirmation_token character varying(180) DEFAULT NULL::character varying,
  password_requested_at timestamp without time zone,
  roles text NOT NULL,
  lpeutchangerprofil boolean NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (iid_user),
  CONSTRAINT uniq_Users_username_canonical UNIQUE (username_canonical),
  CONSTRAINT uniq_Users_email_canonical UNIQUE (email_canonical),
  CONSTRAINT uniq_Users_confirmation_token UNIQUE (confirmation_token)
) ;

ALTER table paa_securite.users ADD column if not exists lpeutchangerprofil boolean NOT NULL DEFAULT FALSE;
ALTER table paa_securite.users ALTER column lpeutchangerprofil SET DEFAULT TRUE;

-- User dftadmin, mdp Limoog!65
INSERT INTO paa_securite.users (iid_user, ctype_res, iid_res, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles) 
	VALUES
	(DEFAULT, NULL, NULL, 'dftAdmin', 'dftadmin', 'dftAdmin@limoog.net', 'dftadmin@limoog.net', true, '9vmgG6IjQnGDLd37Sap4/hwhI4vjBMRb4WnKWPrby78', 'k2Q7KKr5Yo39aVy0juABLaF8EoO9fqJSnWDqqPmHDtW/ndSHMSdqibMFZeXziUVi7lTWpk1Xyt3IMk4jxzAOLA==', '2018-07-30 07:48:01', NULL, NULL, 'a:2:{i:0;s:16:"ROLE_SUPER_ADMIN";i:1;s:10:"ROLE_ADMIN";}, TRUE');

-- DROP TABLE paa_securite.authtoken;
CREATE TABLE paa_securite.authtoken
(
  id serial NOT NULL,
  value text,
  created_at timestamp without time zone,
  iuser integer,
  CONSTRAINT pk_authtoken PRIMARY KEY (id),
  CONSTRAINT fk_authtoken_user FOREIGN KEY (iuser)
      REFERENCES paa_securite.users (iid_user) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) ;