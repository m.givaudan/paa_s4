<?php

namespace App\PaaBundle\Controller;

use App\PaaBundle\Security\PaaVoter;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//use App\PaaBundle\Entity\actiRealisations;
use App\PaaBundle\Entity\seances;
//MG Modification 20200218 Fin
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Limoog\PartageBundle\Component\cParametresBDD;

//use Symfony\Component\Validator\Constraints As toto ;

$lsFich = dirname(dirname(__FILE__)) . '/Component/Paa/Paa.php';
require_once $lsFich;
$lsFich = dirname(dirname(__FILE__)) . '/Component/Paa/planning.php';
require_once $lsFich;
$lsFich = dirname(dirname(__FILE__)) . '/Component/Paa/ressources.php';
require_once $lsFich;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//MG Modification 20200313 Début
//Utilisation d'une fonction du DefaultController, de plus de Default hérite deja de PAABaseController
//class PlanningController extends PAABaseController {
class PlanningController extends DefaultController {

//MG Modification 20200313 Fin
	// Sur réception d'une demande de popup sur le planning
	// e.g. ""http://www.paa.localhost.net/app_dev.php/onPopupMenu.JSON?sIdSeance=S0_720_Selection,&sIdRessource=I26&dDate=Mon Jul 24 2017 00:00:00 GMT+0200 (heure d’été d’Europe centrale)&iDebut=720&iFin=750""
	// Valeur de retour : JSON validant le succès de l'enegistrement de la notification
	// Voir d:\luc\dev\paa45 spécifiques\cgrillehebdo4\cgrillehebdo4_ctnvisionneuse_html_procs.prg
	function onPlanningPopupMenuAction(Request $request) {
		$lsIdSeance = $request->get('sIdSeance');
		$lsIdRessource = $request->get('sIdRessource');
		$lsDate = $request->get('dDate');
		$lsDebut = $request->get('iDebut');
		$lsFin = $request->get('iFin');

		$lsJSONAuth = $this->getJSONAuth();
		$lsJSONSéance = $this->getJSONSéance();
		$lsJSONTypesAbsences = $this->getJSONTypesAbsences();
		$lsJSONFréquencesPrédéfiniesSéries = $this->getJSONFréquencesPrédéfiniesSéries();
		$lsJSONDivers = $this->getJSONDivers();

		$lsJSON = '{"oAut":' . $lsJSONAuth
				. ', "oSéance": ' . $lsJSONSéance
				. ', "aTypesAbsences": ' . $lsJSONTypesAbsences
				. ', "aFréquencesPrédéfiniesSéries": ' . $lsJSONFréquencesPrédéfiniesSéries
				. ', "oDivers": ' . $lsJSONDivers
				. '}';

		$response = new Response($lsJSON);
		// $response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	private function getJSONAuth() {
		$lsJSON = '{"bTout": false
			, "bPoseSéance": false
			, "bPoseAbsence": false
			, "bEnlèveSéance": false
			, "bModifieSéance": false
			, "bModifieActivitéDeSéance": false
			, "bChangeActivitéDeSéance": false
			}';
		return $lsJSON;
	}

	private function getJSONSéance() {
		$lsJSON = '{"iId_ActiBase": 0
			, "lEstPresence": false
			, "lEstAbsence": false
			, "lEstActivitéGénérique": false
			, "lEstActivitéDeSemaineSuivie": false
			, "lEstSérieSéance": false
			, "iSérieSéance": false
			, "cUserCréateur": ""
			}';
		return $lsJSON;
	}

	private function getJSONTypesAbsences() {
		$lsJSON = '[]';
		return $lsJSON;
	}

	private function getJSONFréquencesPrédéfiniesSéries() {
		$lsJSON = '[]';
		return $lsJSON;
	}

	private function getJSONDivers() {
		$lsJSON = '{"lEstHoraireSemaineExceptionnel":false}';
		return $lsJSON;
	}

	// Renvoyer un dateTime avec une date sous forme de caractères et une heure en minutes
	// A déplacer ailleurs
	private function newDateTime($psDate, $piMinutes) {
		$liMinutes = $piMinutes / 60;
		$lt = uNumDateVersTime($psDate   // pvDate
				, $liMinutes // piHeure
				, null   // piTypeArrondi
		);
		return new \DateTime($lt);
	}

	public function supprimeSéanceAction(Request $request) {
		if ($LoginResponse = $this->RedirectIfDisconnected($request, 'ROLE_USER', $request->headers->get('referer'))) {
			// L'utilisateur en cours n'est pas authentifié, ou sa session a expiré
			// On retourne à la page appellante ($request->headers->get('referer'))
			return $LoginResponse;
		}
		$laRequestParameters = $request->request->all();

		$lbKO = false;
		$lsJSONRep = "{}";
		if (!isset($laRequestParameters["sLstSeances"]))
			$lbKO = true;

		if (!$lbKO) {
			// $laRequestParameters["sLstSeances"] est de la forme : "<IdSeanceDansGrille>:<IdSéance>:<IdRessource>,..."
			$lsLstSéances = $laRequestParameters["sLstSeances"];
			$laSéances = explode(",", $lsLstSéances);
			$lsLstIdSéances = "";
			foreach ($laSéances as $lsSeance) {
				if (!$lsSeance)
					continue;
				if (strpos(explode(":", $lsSeance)[2], "E")) {
					// Etablissement : cas pas encore géré (cf proc_Seance_RecSeance.PRG.DelRecord)
					// De même, ccurseurasync_séances.prg.AutoriseChangeSéance n'est pas implémentée
					$lsJSONRep = "Désolé, le cas de modification de séances de l'établissement n'est pas encore gérée.";
					$lbKO = true;
				}
				if (strpos(explode(":", $lsSeance)[2], "A")) {
					// Activité de base : cas pas encore géré (cf proc_Seance_RecSeance.PRG.DemandeTypeEffacementSéance)
					// De même, ccurseurasync_séances.prg.AutoriseChangeSéance n'est pas implémentée
					$lsJSONRep = explode(":", $lsSeance)[2] . "<br>" . strpos(explode(":", $lsSeance)[2], "A") . "<br>" .
							"Désolé, le cas de suppression de séances d'une activité de base n'est pas encore gérée.";
					$lbKO = true;
				}
				$lsLstIdSéances .= explode(":", $lsSeance)[1] . ",";
			}
		}

		if ($lbKO) {
			// Echec
		} else if (!$this->isGranted(PaaVoter::MODIFIER . "EnlèveSéance", $lsLstIdSéances)) {
			// L'utilisateur en cours ne dispose pas des droits sur au moins une des séances à supprimer
			$psJSONSeances = "Désolé, vous ne disposez pas des droits nécessaires pour cette action. Contactez votre administrateur pour qu&lsquo;il étende vos droits.";
		} else if (!isset($laRequestParameters["ConfirmeSuppression"])) {
			// La question n'a pas encore été posée : la poser maintenant
			// Voir ccurseurasync_séances_procs.prg.DemandeTypeEffacementSéance()
			$liBoutonActif = $this->getParamètresBDD()->getParamètre("gsDemandeTypeEffacementSéance_Rep2", "3", "NU");
			$lsAnnulerFocused = "";
			$lsParticipationFocused = "";
			$lsSeanceFocused = "";
			if ($liBoutonActif == 1) {
				$lsParticipationFocused = '"focused": true, ';
			} else if ($liBoutonActif == 2) {
				$lsSeanceFocused = '"focused": true, ';
			} else {
				$lsAnnulerFocused = '"focused": true, ';
			}
			$lsJSONRep = '{"Dialogue": {"Message": "Souhaitez-vous réellement supprimer ces séances, ou simplement annuler la participation des ressources sélectionnées ?"'
					. ', "Titre":"Supprimer une ou plusieurs séances"'
					. ', "Boutons": [{"text":"Participation", "id": "cmdParticipation", ' . $lsParticipationFocused . '"Action":"refreshAfterAction_Dialog_SupprimeParticipation"}'
					. ', {"text":"Toute la séance", "id": "cmdTouteLaSéance", ' . $lsSeanceFocused . '"Action":"refreshAfterAction_Dialog_SupprimeTouteLaSéance"}'
					. ', {"text":"Annuler", "class":"cmdCancel", "id": "cmdAnnuler", ' . $lsAnnulerFocused . '"Action":"refreshAfterAction_Dialog_Annuler"}]}'
					. ', "DemandeInitiale": ' . json_encode($laRequestParameters)
					. ', "DialogueEnCours": "ConfirmeSuppression"'
					. '}';
		} else {
			// C'est une réponse au Dialogue1 (ci-dessus)
			// Effectuer l'action de suppression
// LG 20200513 old			$em = $this->getDoctrine()->getManager();
			$em = $this->getDoctrineManager();
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//			$repository = $em->getRepository("PaaBundle:actiRealisations");
			$repository = $em->getRepository("PaaBundle:seances");
//MG Modification 20200218 Fin			
			$lsJSONRep = '';
			foreach ($laSéances as $lsSeance) {
				if (!$lsSeance)
					continue;
				$lsIdSeanceGrille = explode(":", $lsSeance)[0];
				$liId_Seance = explode(":", $lsSeance)[1];
				$lsRes = explode(":", $lsSeance)[2];

				// Effectuer l'action de suppression
				if ($laRequestParameters["ConfirmeSuppression"] == "SupprimeParticipation") {
					// Mémoriser le choix de l'utilisateur
					$this->getParamètresBDD()->setParamètre("gsDemandeTypeEffacementSéance_Rep2", "1", "NU");
					// Effacer la participation
					$repository->AnnuleParticipationRessource($liId_Seance, $lsRes);
					// L'item à effacer de la grille est : la séance pour cette ressource
					$lsJSONItem = '{"sIdSeance": "' . $lsIdSeanceGrille . '"}';
				}
				if ($laRequestParameters["ConfirmeSuppression"] == "SupprimeTouteLaSéance") {
					// Mémoriser le choix de l'utilisateur
					$this->getParamètresBDD()->setParamètre("gsDemandeTypeEffacementSéance_Rep2", "2", "NU");
					// Effacer la séance
					$repository->EffaceSéance($liId_Seance);
					// L'item à effacer de la grille est : la séance pour toutes les ressources
					$lsJSONItem = '{"iId_Seance": "' . $liId_Seance . '"}';
				}

				// Préparer le JSON de retour 
				if ($lsJSONRep)
					$lsJSONRep .= ",";
				$lsJSONRep .= $lsJSONItem;
			}
			$lsJSONRep = '{"aSeancesEffacees": [' . $lsJSONRep . ']}';
//				echo $laRequestParameters["ConfirmeSuppression"] . "<br>" . $lsLstSéances . "<br>" ;
//				echo $lsJSONRep . "<br>" ;
//				echo $toto ;
		}

		$response = new Response($lsJSONRep);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}

	// Convertir un tableau tel que reçu de la grille du planning (généré par cGestionnaireActions_Symfony.onFinDéplacementSéance dans cGestionnaireActions_Symfony.js, )
	// en tableau contenant toutes les informations enregistrables
	public function RequestToSeance($paraJSON) {

		if (empty($paraJSON))
			return "";

		$paraJSON["tDebut"] = $this->newDateTime($paraJSON["dDate"], $liDebut = $paraJSON["iDebut"]);
		$paraJSON["tFin"] = $this->newDateTime($paraJSON["dDate"], $liDebut = $paraJSON["iFin"]);
//echo '$paraJSON["iDebut"] = ' . $paraJSON["iDebut"] . "<br>" ;
//echo '$paraJSON["iFin"] = ' . $paraJSON["iFin"] . "<br>" ;
//echo '$paraJSON["tDebut"] = ' . TTOC($paraJSON["tDebut"]) . "<br>" ;
//echo '$paraJSON["tFin"] = ' . TTOC($paraJSON["tFin"]) . "<br>" ;
//echo $err2 ;

		decomposeRessource($paraJSON["sIdRessource"], $lsType_Res, $liId_Res);
		$paraJSON["sType_Res"] = $lsType_Res;
		$paraJSON["iId_Res"] = $liId_Res;
		$liId_Seance = (isset($paraJSON["iId_Seance"])) ? $paraJSON["iId_Seance"] : 0;
		$paraJSON["bNewRecord"] = ($liId_Seance == 0) || $paraJSON["lDuplique"];
		
//MG Modification 20200813 Début
//Si l'heure de début est supérieur a l'heure de fin, on ne doit pas pouvoir enregistrer la séance.
		if($paraJSON["tDebut"] >= $paraJSON["tFin"]){
			$paraJSON["ErreurHoraire"] = true;
		}
//MG Modification 20200813 Fin
		return $paraJSON;
	}

	// Préparer la boite de dialogue pour affichage à l'utilisateur
	// Lors d'une action sur le planning
	public function jsonDialogue($pcMsgErr, $pcMsgAvertissement, $paDemandeInitiale) {
		$em = $this->getDoctrineManager();
// LG 20200720 début + MG Modification 20200803
// $paDemandeInitiale["iId_Seance"] non définie lors de la création d'une séance
//		$loSéance = $em->getRepository("PaaBundle:seances")->find($paDemandeInitiale["iId_Seance"]);
//		$iSerie = $loSéance->getIserie();
		$iSerie = 0;
		if (isset($paDemandeInitiale["iId_Seance"])) {
			$loSéance = $em->getRepository("PaaBundle:seances")->find($paDemandeInitiale["iId_Seance"]);
			$iSerie = $loSéance->getIserie();
		}
// LG 20200720 fin
		//MG Modification 20200615 Début
//		if ($pcMsgErr) {
//			// Préparer l'affichage de l'erreur
//			$lsJSONDialogue = '{"Dialogue": {"Message": ' . json_encode($pcMsgErr) . ', "Titre":"Déplacement de la séance"'
//					. ', "Boutons": [{"text":"OK", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
//					. '}';
//		} else if ($pcMsgAvertissement) {
//			$lsJSONDialogue = '{"Dialogue": {"Message": ' . json_encode($pcMsgAvertissement) . ', "Titre":"Déplacement de la séance"'
//					. ', "Boutons": [{"text":"OK", "class": "cmdOk", "Action":"refreshAfterAction_Dialog_OK"}'
//					. ', {"text":"Annuler", "class": "cmdCancel", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
//					. ', "DemandeInitiale": ' . json_encode($paDemandeInitiale)
//					. ', "DialogueEnCours": "ConfirmeDéplacement"'
//					. '}';
//		} else {
//			$lsJSONDialogue = '';
//		}
//		MG Modification 20200803 : $iSerie non définie lors de la création d'une séance
//		if($iSerie){
// LG 20200804 old		if(isset($iSerie)){
		if($iSerie){
			$pcMsgAvertissement = "Cette séance fait partie d'une série de séances dont les caractéristiques sont résumées ci-dessous.\n"
					. "Souhaitez-vous répercuter le changement fait sur cette séance aux autres séances de cette série ?";
			if ($pcMsgErr) {
				// Préparer l'affichage de l'erreur
				$lsJSONDialogue = '{"Dialogue": {"Message": ' . json_encode($pcMsgErr) . ', "Titre":"Déplacement de la séance"'
//						. ', "Boutons": [{"text":"Cette séance", "Action":"refreshAfterAction_Dialog_CetteSeance"}]}'
//						. ', "Boutons": [{"text":"Toute la série", "Action":"refreshAfterAction_Dialog_TouteSerie"}]}'
//						. ', "Boutons": [{"text":"A partir d\'ici (inclus)", "Action":"refreshAfterAction_Dialog_Depart"}]}'
//						. ', "Boutons": [{"text":"Scinder ici", "Action":"refreshAfterAction_Dialog_Scinder"}]}'
//						. ', "Boutons": [{"text":"Chaque (jour de la séance)", "Action":"refreshAfterAction_Dialog_Hebdo"}]}'
						. ', "Boutons": [{"text":"Annuler", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
						. '}';
			} else if ($pcMsgAvertissement) {
				$lsJSONDialogue = '{"Dialogue": {"Message": ' . json_encode($pcMsgAvertissement) . ', "Titre":"Déplacement de la séance"'
						. ', "Boutons": [{"text":"Cette séance", "Action":"refreshAfterAction_Dialog_CetteSeance"}';
				
				if ($pcMsgAvertissement) {
					$lsJSONDialogue = '{"Dialogue": {"Message": ' . json_encode($pcMsgAvertissement) . ', "Titre":"Modification d une séance d une série sans conflit"'
							. ', "Boutons": [{"text":"Cette seance", "Action":"refreshAfterAction_Dialog_CetteSeance"}'
							. ', {"text":"Toute la série", "Action":"refreshAfterAction_Dialog_TouteSerie"}'
							. ', {"text":"A partir d\'ici (inclus)", "Action":"refreshAfterAction_Dialog_Depart"}'
							. ', {"text":"Scinder ici", "Action":"refreshAfterAction_Dialog_Scinder"}'
							. ', {"text":"Chaque (jour de la séance)", "Action":"refreshAfterAction_Dialog_Hebdo"}'
							. ', {"text":"Annuler", "class": "cmdCancel", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
							. ', "DemandeInitiale": ' . json_encode($paDemandeInitiale)
							. ', "DialogueEnCours": "ConfirmeDéplacement"'
							. '}';
				} else {
					$lsJSONDialogue = '';
				}
			}
		}
		if ($pcMsgErr) {
			// Préparer l'affichage de l'erreur
			$lsJSONDialogue = '{"Dialogue": {"Message": ' . json_encode($pcMsgErr) . ', "Titre":"Déplacement de la séance"'
					. ', "Boutons": [{"text":"OK", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
					. '}';
		} else if ($pcMsgAvertissement) {
			$lsJSONDialogue = '{"Dialogue": {"Message": ' . json_encode($pcMsgAvertissement) . ', "Titre":"Déplacement de la séance"'
					. ', "Boutons": [{"text":"OK", "class": "cmdOk", "Action":"refreshAfterAction_Dialog_OK"}'
					. ', {"text":"Annuler", "class": "cmdCancel", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
					. ', "DemandeInitiale": ' . json_encode($paDemandeInitiale)
					. ', "DialogueEnCours": "ConfirmeDéplacement"'
					. '}';

		} else {
			$lsJSONDialogue = '';
		}
		return $lsJSONDialogue;
	}

	public function insèreSéanceAction(Request $request) {
		$this->SauvegardeOngletActif($request);
		if ($LoginResponse = $this->RedirectIfDisconnected($request, 'ROLE_USER', $request->headers->get('referer'))) {
			// L'utilisateur en cours n'est pas authentifié, ou sa session a expiré
			// On retourne à la page appellante ($request->headers->get('referer'))
			return $LoginResponse;
		}

		$lsJSONSeances = "";
		$laSéance = $this->RequestToSeance($request->request->all());
		if ($this->insèreSéanceTraitement($laSéance, $lsJSONSeances)) {
			// Autorisation d'enregistrer
			$laSéance["iId_Seance"] = 0;
			$laSéance["bNewRecord"] = true;
			$laSéance["sLibelle"] = "Nouvelle séance";
			$laSéance["cCouleur"] = "red";

			$this->sauveSéance($laSéance, $lsJSONSeances);
		}
		$response = new Response($lsJSONSeances);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}

//MG Ajout 20200305 Début	
// Récupère les données du formulaire envoyé par l'utilisateur, et update la séance grâce à updateAction
// Retourne un json qui permettra la mise a jour de l'affichage de la séance dans le planning.
	public function rafraichirSéanceAction(Request $request) {

		if ($LoginResponse = $this->RedirectIfDisconnected($request, 'ROLE_USER', $request->headers->get('referer'))) {
			// L'utilisateur en cours n'est pas authentifié, ou sa session a expiré
			// On retourne à la page appellante ($request->headers->get('referer'))
			return $LoginResponse;
		}

		$lsJSONSeances = "";
		$paSeance = $this->RequestToSeance($request->request->all());
//MG Ajout 20200313 Début
//		$this->updateAction($request, 'seances', $paSeance['iId_Seance']);
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
		$repository = $em->getRepository("PaaBundle:seances");


// LG 20200316 deac		$loEntité = null ;
// LG 20200316 deac		$lbNouvelleSéance = false ;
// LG 20200316 deac		$repository->getSéanceAvecTableau($paSeance, $loEntité, $lbNouvelleSéance) ;
		$newEntity = $repository->find($paSeance['iId_Seance']);
//		var_dump($newEntity);

		$laSeance = array();
		$laSeance['iid_seance'] = $newEntity->getIidseance();
		$laSeance['ddate'] = $newEntity->getTdebut();
		$laSeance['ndebut'] = uHeureVersNum($newEntity->getTdebut());
		$laSeance['nfin'] = uHeureVersNum($newEntity->getTfin());
		$laSeance['ccouleur'] = $newEntity->getCcouleur();
		$laSeance["clibelle"] = $repository->getLibelle($paSeance['iId_Seance'], $paSeance["sType_Res"]);
		$laSeance['llibre1'] = $newEntity->getLlibre1();
		$laSeance['llibre2'] = $newEntity->getLlibre2();

		$laSeance["ctype_res"] = $paSeance["sType_Res"];
		$laSeance["iid_res"] = $paSeance["iId_Res"];
		$laSeance["lmasque"] = false;
		$laSeance['inbchevauchements'] = 1;
		$laSeance['irangchevauchement'] = 1;
		$laSeance['lvirtuel'] = false;

		$liNoSeance = substr($laSeance['iid_seance'], 1, strlen($laSeance['iid_seance']) - 1);
		$lsJSONSeance = Planning_RtvJSONSéance($laSeance, $liNoSeance);
		$lsJSONSeances = '{"aSeances": [' . $lsJSONSeance . ']}';

//MG Ajout 20200313 Fin
//		if($laSéance){
//			$this->updateSéance($laSéance, $lsJSONSeances);
//		}

		$response = new Response($lsJSONSeances);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}

	public function SauvegardeOngletActif(Request $request) {
		$PParam = $request->request->all();
		// Récupération des Params
		//enregistrement dans la BDD  des onglets actifs

		$loCnxn = $this->getDoctrineConnection();
		$bdd = new cParametresBDD($loCnxn);

		if (isset($PParam['OngletActif']) && $PParam['OngletActif']) {
			$bdd->setParamètre('psOngletActifGetIdActivites', $PParam['OngletActif'], 'TU');
		}
		if (isset($PParam['SSOngletActif']) && $PParam['SSOngletActif']) {
			$bdd->setParamètre('psSSOngletActifGetIdActivites', $PParam['SSOngletActif'], 'TU');
		}
	}

	public function insèreSéanceTraitement($paraJSON, &$psJSONSeances) {
		if (empty($paraJSON)) {
			return "";
		}

		$lbSauve = false;
		$lcMsgErr = "";
		$lcMsgAvertissement = "";
		if (!$this->isGranted(PaaVoter::MODIFIER . "PoseSéance", $paraJSON["iActi"])) {
			// L'utilisateur en cours ne dispose pas des droits sur cette séance
			$psJSONSeances = "Désolé, vous ne disposez pas des droits nécessaires pour cette action. Contactez votre administrateur pour qu&lsquo;il étende vos droits.";
//MG Modification 20201308 Début
//Lorsque l'heure de début est supérieur a l'heure de fin
		}else if (isset($paraJSON["ErreurHoraire"])){
			$lcMsgErr = "Heure de début supérieur à l'heure de fin";
			$lbSauve = false;
//MG Modification 20201308 Fin
		} else if (isset($paraJSON["ConfirmeDéplacement"]) && $paraJSON["ConfirmeDéplacement"] == "OK") {
			// C'est une réponse au Dialogue1
			// Valider
			$lbSauve = true;
		} else if ($this->RtvAvertissementPoseSéance($paraJSON, $lcMsgAvertissement)) {
			// La pose de cette séance génèrera des anomalies
			// RAS car tout est déja dans le $lcMsgAvertissement
		} else if (true) {
			// test de message de confirmation
			$lcMsgAvertissement = "Confirmez-vous l'insertion ?";
		}

//		} else if (false) {
//			// test de message de confirmation
//			if (isset($paraJSON["ConfirmeInsertion"]) && $paraJSON["ConfirmeInsertion"] == "OK") {
//				// C'est une réponse au Dialogue1
//				// Valider
//				$lbSauve = true;
//			} else {
//				$psJSONSeances = '{"Dialogue": {"Message": "Confirmez-vous l\'insertion ?", "Titre":"Insertion d\'une séance"'
//						. ', "Boutons": [{"text":"OK", "Action":"refreshAfterAction_Dialog_OK"}, {"text":"Annuler", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
//						. ', "DemandeInitiale": ' . json_encode($paraJSON)
//						. ', "DialogueEnCours": "ConfirmeInsertion"'
//						. '}';
//			}
//		} else {
//			$lbSauve = true ;
//		}

		$psJSONSeances = $this->jsonDialogue($lcMsgErr, $lcMsgAvertissement, $paraJSON);

		return $lbSauve;
	}

	public function onFinDéplacementSéanceAction(Request $request) {
		if ($LoginResponse = $this->RedirectIfDisconnected($request, 'ROLE_USER', $request->headers->get('referer'))) {
			// L'utilisateur en cours n'est pas authentifié, ou sa session a expiré
			// On retourne à la page appellante ($request->headers->get('referer'))
			return $LoginResponse;
		}
		
		$lsJSONSeances = "";
		$laSéance = $this->RequestToSeance($request->request->all());
		if (!empty($laSéance)) {
			if ($this->onFinDéplacementSéanceTraitement($laSéance, $lsJSONSeances)) {
				// Autorisation d'enregistrer
				$this->sauveSéance($laSéance, $lsJSONSeances);
			}
		}
		$response = new Response($lsJSONSeances);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}

	public function onFinDéplacementSéanceTraitement($paSéance, &$psJSONSeances) {
		$lbSauve = false;
		$psJSONSeances = "";
		$lcMsgErr = "";
		$lcMsgAvertissement = "";
		$lbDuplique = $paSéance["lDuplique"];
		if ($paSéance["sIdRessource_Old"] !== $paSéance["sIdRessource"]) {
			// Changement de ressource : pas encore géré
			$lcMsgErr = "Désolé, le déplacement d'une séance vers une autre ressource n'est pas encore géré.";
		} else if ($lbDuplique) {
			// Ctrl+déplacement : Duplication de séance : pas encore géré
			$lcMsgErr = "Désolé, le cas de la duplication d'une séance n'est pas encore géré";
		} else if ($paSéance["lAjusteParticipation"]) {
			// Shift+déplacement : Ajustement de la participation à la séance : pas encore géré
			$lcMsgErr = "Désolé, le cas de l'ajustement de la durée de participation à une séance n'est pas encore géré";
		} else if (false) {
			// test de retour d'un msg d'erreur
			$lcMsgErr = "Test de retour d'un msg d'erreur !";
		} else if (false) {
			// test de retour d'une exception
			echo $toto;
		} else if (!$lbDuplique && !$this->isGranted(PaaVoter::MODIFIER . "ModiHorairesSéance", $paSéance["iId_Seance"])) {
			// L'utilisateur en cours ne dispose pas des droits sur cette séance
			$psJSONSeances = "Désolé, vous ne disposez pas des droits nécessaires pour cette action. Contactez votre administrateur pour qu&lsquo;il étende vos droits.";
		} else if ($lbDuplique && !$this->isGranted(PaaVoter::MODIFIER . "PoseSéance", $paSéance["iId_Seance"])) {
			// L'utilisateur en cours ne dispose pas des droits sur cette séance
			$psJSONSeances = "Désolé, vous ne disposez pas des droits nécessaires pour cette action. Contactez votre administrateur pour qu&lsquo;il étende vos droits.";
		} else if (isset($paSéance["ConfirmeDéplacement"]) && $paSéance["ConfirmeDéplacement"] == "OK") {
			// C'est une réponse au Dialogue1
			// Valider
			$lbSauve = true;
		} else if ($this->RtvAvertissementPoseSéance($paSéance, $lcMsgAvertissement)) {
			// La pose de cette séance génèrera des anomalies
			// RAS car tout est déja dans le lsMsgErr
		} else if (true) {
			// test de message de confirmation
			$lcMsgAvertissement = "Confirmez-vous le déplacement ?";
		}

//		if ($lcMsgErr) {
//			// Préparer l'affichage de l'erreur
//			$psJSONSeances = '{"Dialogue": {"Message": "' . $lcMsgErr . '", "Titre":"Déplacement de la séance"'
//					. ', "Boutons": [{"text":"OK", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
//					. '}';
//		} else if($lcMsgAvertissement) {
//			$psJSONSeances = '{"Dialogue": {"Message": "' . $lcMsgAvertissement . '", "Titre":"Déplacement de la séance"'
//					. ', "Boutons": [{"text":"OK", "Action":"refreshAfterAction_Dialog_OK"}, {"text":"Annuler", "Action":"refreshAfterAction_Dialog_Annuler"}]}'
//					. ', "DemandeInitiale": ' . json_encode($paSéance)
//					. ', "DialogueEnCours": "ConfirmeDéplacement"'
//					. '}';
//		}
		$psJSONSeances = $this->jsonDialogue($lcMsgErr, $lcMsgAvertissement, $paSéance);

		// Logguer dans D:\Luc\Dev\DevWeb\Symfony\applicationPAA_S4\var\log\Debug.log
		// file_put_contents("..\\var\log\\Debug.log" , $psJSONSeances, FILE_APPEND) ;
//		$this->trace($psJSONSeances) ;

		return $lbSauve;
	}

	// Enregistre une séance et renvoie un tableau compatible avec Planning_RtvJSONSéance (Planning.php)
	// paSéance	 : tableau contenant toutes les informations enregistrables (généré par $this->RequestToSeance)
	// &psMsg : pour retour du message d'anomalie
	// Valeur de retour : true si il y a un message d'avertissement
	private function RtvAvertissementPoseSéance($paSéance, &$psMsg) {
		// Utiliser Paa.RtvMsgInfosSeancesConcurrentes() -> repository des séances
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//			$repository = $em->getRepository("PaaBundle:actiRealisations");
		$repository = $em->getRepository("PaaBundle:seances");
//MG Modification 20200218 Fin	
		$loSéance = null;
		if (!$repository->getSéanceAvecTableau($paSéance, $loSéance)) {
			$psMsg = "Erreur de construction de la séance";
			return false;
		}
		$lsLstRes = "";
		if (!$repository->RtvLstRessourcesSéance($loSéance, $lsLstRes)) {
			$psMsg = "Erreur de récupération des ressources de la séance";
			return false;
		}
		$repository->RtvMsgInfosSeancesConcurrentes($psMsg		   // &$pcMsg
				, $loSéance->getTdebut()	  // $ptDébut
				, $loSéance->getTfin()	   // $ptFin
				, $lsLstRes		  // $pcLstResAChecker
				, $loSéance->getId()	   // $piId_SéanceAExclure
		);
		if ($psMsg) {
			// Au moins un conflit a été détecté
			$psMsg = "Attention : la séance que vous êtes en train de déplacer présente les problèmes suivants&nbsp;:"
					. "<br>Souhaitez-vous la poser tout de même ?"
					. "<hr><Strong>Les conflits suivants ont été détectés : </Strong><br>"
					. $psMsg;
		}
		return $psMsg ? true : false;
	}

	// Enregistre une séance et renvoie un tableau compatible avec Planning_RtvJSONSéance (Planning.php)
	// paSéance	 : tableau contenant toutes les informations enregistrables (généré par $this->RequestToSeance)
	// &psJSONSeances : pour retour d'une chaine JSON attendue par cGestionnaireActions_Symfony.lanceActionSymfony dans cGestionnaireActions_Symfony.js
	private function sauveSéance($paSéance, &$psJSONSeances) {
		// Récupérer la séance telle qu'elle est dans la BDD, et lui appliquer les modifications
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//			$repository = $em->getRepository("PaaBundle:actiRealisations");
		$repository = $em->getRepository("PaaBundle:seances");
//MG Modification 20200218 Fin	
// LG 20190925 début
//		$lbNouvelleSéance = false ;
//		if ($paSéance["iId_Seance"]) {
//			// Séance préexistante
//			$loEntité = $repository->find($paSéance["iId_Seance"]);
//		} else if (!isset($paSéance["iActi"]) or empty($paSéance["iActi"])) {
//			// Id d'activité manquant
//			$psJSONSeances = '{"Erreur": "Id d\'activité manquant"}' ;
//			return false ;
//		} else {
//			// Nouvelle séance
//			$lbNouvelleSéance = true ;
//			$loEntité = new actiRealisations();
//			
//			// Il faut fournir une référence à l'activité de cette séance
//			// cf. https://stackoverflow.com/questions/44955296/doctrine-2-persist-entity-with-joined-tables-foreign-key-opposed-to-foreign-e
//			$loRefActi = $em->getReference('PaaBundle:activites', $paSéance["iActi"]);
//			// LG 20190718, autre solution ? : $activites = $em->getRepository(activites::class)->find($paSéance["iActi"]);
//			
//// LG 20190718 old			$loEntité->setIacti($loRefActi) ;
//			$loEntité->setActivité($loRefActi) ;
//		}
//		// Appliquer les modifications à apporter à la séance
//		$loEntité->setTdebut($paSéance["tDebut"]);
//		$loEntité->setTfin($paSéance["tFin"]);
		$loEntité = null;
		$lbNouvelleSéance = false;
		$repository->getSéanceAvecTableau($paSéance, $loEntité, $lbNouvelleSéance);
// LG 20190925 fin
		// Vérifier la faisabilité
		$liRep = 0;
		$lcMsg = "";
		$repository->setoParamètresBDD($this->getParamètresBDD());
		$lbKO = !$repository->AutorisePoseSéance($loEntité, $paSéance["sType_Res"], $paSéance["iId_Res"], $paSéance["bNewRecord"], $lcMsg);
		if ($lbKO) {
			// On n'a pas l'autorisation de poser la séance
			$psJSONSeances = '{"Erreur": "Echec de l\'enregistrement : ' . $lcMsg . '"}';
			return false;
		}

		// Enregistrer
		$persisted = $em->persist($loEntité);
		$flushed = $em->flush(); // Cette ligne peut se planter sans passer dans un catch...
		$liIdSéance = $loEntité->getIidseance();

		if (isset($paSéance["aLstResAInclure"])) {
			// On a fourni une liste de ressources à inclure : les traiter
//			$lsLstResAInclure = implode (",", $paSéance["aLstResAInclure"]) ;
//			$lsLstResAInclure = $this->get('serializer')->serialize($paSéance["aLstResAInclure"], 'json');
			$repository->SetLstRessourcesSéance($loEntité, $paSéance["aLstResAInclure"]);
		}

		// Préparer le tableau de retour : données de la séance 
		// Sous forme compatible avec Planning_RtvJSONSéance (Planning.php)
		if ($lbNouvelleSéance) {
			// La séance est une nouvelle séance			
			// Générer un numéro unique pour cette séance dans le cadre de la grille d'affichage du planning
			$paSéance["sIdSeance"] = uniqid('S');
			// Retrouver sa couleur et son libellé
			$paSéance["cCouleur"] = $loEntité->getCcouleur();
			$paSéance["sLibelle"] = $repository->getLibelle($liIdSéance, $paSéance["sType_Res"]);

			if (!$paSéance["sLibelle"]) {
				// Libellé vide
				echo $paSéance["iActi"] . "<br>";
				echo $loRefActi->getId() . "<br>";
				echo $loEntité->getActivité()->getId() . "<br>";
				echo $persisted . "<br>";
				echo $flushed . "<br>";
				echo $liIdSéance . "<br>";
				echo $NouvelleSéanceLibelleVide;  // Pour générer une exception
			}
		}

		$laSeance = array();
		$laSeance['iid_seance'] = $liIdSéance/* $loEntité->getIidseance() */;
		$laSeance['ctype_res'] = $paSéance["sType_Res"];
		$laSeance['iid_res'] = $paSéance["iId_Res"];
		$laSeance['ddate'] = $paSéance["dDate"];
		$laSeance['ndebut'] = $paSéance["iDebut"] / 60;
		$laSeance['nfin'] = $paSéance["iFin"] / 60;
//echo '$paSéance["iDebut"] = ' . $paSéance["iDebut"] . "<br>" ;
//echo '$laSeance["ndebut"] = ' . $laSeance['ndebut'] . "<br>" ;

		$laSeance['clibelle'] = $paSéance["sLibelle"];
		$laSeance['ccouleur'] = $paSéance["cCouleur"];

		$laSeance['inbchevauchements'] = 1;
		$laSeance['irangchevauchement'] = 1;

// LG 20190824 début
		if (array_key_exists("iRangSousGroupe", $paSéance)) {
			$laSeance['irangsousgroupe'] = $paSéance['iRangSousGroupe'];
		}
		if (array_key_exists("iNbSousGroupes", $paSéance)) {
			$laSeance['inbsousgroupes'] = $paSéance['iNbSousGroupes'];
		}
		if (array_key_exists("cNomSousGroupe", $paSéance)) {
			$laSeance['cnomsousgroupe'] = $paSéance['cNomSousGroupe'];
		}
// LG 20190824 fin

		$laSeance['lvirtuel'] = false;
		$laSeance['llibre1'] = $loEntité->getLlibre1();
		$laSeance['llibre2'] = $loEntité->getLlibre2();
		$laSeance['lmasque'] = false;

		$lsNoSeance = $paSéance["sIdSeance"]; //  de la forme "SXX"
		$liNoSeance = substr($lsNoSeance, 1, strlen($lsNoSeance) - 1);
		$lsJSONSeance = Planning_RtvJSONSéance($laSeance, $liNoSeance);
		$lsJSONSeances = '{"aSeances": [' . $lsJSONSeance . ']}';
//echo "ZZZZZZZ<br>" ;
//echo $lsJSONSeances . "<br>";
//echo $toto ;
		// Valeurs de retour
//		$paSeance = $laSeance ;
		$psJSONSeances = $lsJSONSeances;
		return true;
	}

//MG Ajout 20200306 Début
	// Met à jour une séance et renvoie un tableau compatible avec Planning_RtvJSONSéance (Planning.php)
	// paSéance	 : tableau contenant toutes les informations enregistrables (généré par $this->RequestToSeance)
	private function updateSéance($paSéance, &$psJSONSeances) {
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
		$repository = $em->getRepository("PaaBundle:seances");

		$loEntité = null;
		$lbNouvelleSéance = false;
		$repository->getSéanceAvecTableau($paSéance, $loEntité, $lbNouvelleSéance);

		$em->merge($loEntité);
		$em->flush();
		$liIdSéance = $loEntité->getIidseance();
		//$loEntité = $repository->find($liIdSéance);

		if (!$loEntité) {
			return false;
		}

		$laSeance = array();
		$laSeance['iid_seance'] = $liIdSéance/* $loEntité->getIidseance() */;
		$laSeance['ctype_res'] = $paSéance["sType_Res"];
		$laSeance['iid_res'] = $paSéance["iId_Res"];
		$laSeance['ddate'] = $paSéance["dDate"];
//		$laSeance['ddate'] = uHeureVersNum($loEntité->getTdebut());
//		$laSeance['ndebut'] = $paSéance["iDebut"] / 60;
		$laSeance['ndebut'] = uHeureVersNum($loEntité->getTdebut());
//		$laSeance['nfin'] = $paSéance["iFin"] / 60;
		$laSeance['nfin'] = uHeureVersNum($loEntité->getTfin());
//		echo $laSeance['ndebut'];
//		echo $laSeance['nfin'];

		$paSéance["cCouleur"] = $loEntité->getCcouleur();
		$paSéance["sLibelle"] = $repository->getLibelle($liIdSéance, $paSéance["sType_Res"]);
		$laSeance['clibelle'] = $paSéance["sLibelle"];
		$laSeance['ccouleur'] = $paSéance["cCouleur"];

		$laSeance['inbchevauchements'] = 1;
		$laSeance['irangchevauchement'] = 1;

		if (array_key_exists("iRangSousGroupe", $paSéance)) {
			$laSeance['irangsousgroupe'] = $paSéance['iRangSousGroupe'];
		}
		if (array_key_exists("iNbSousGroupes", $paSéance)) {
			$laSeance['inbsousgroupes'] = $paSéance['iNbSousGroupes'];
		}
		if (array_key_exists("cNomSousGroupe", $paSéance)) {
			$laSeance['cnomsousgroupe'] = $paSéance['cNomSousGroupe'];
		}

		$laSeance['lvirtuel'] = false;
		$laSeance['llibre1'] = $loEntité->getLlibre1();
		$laSeance['llibre2'] = $loEntité->getLlibre2();
		$laSeance['lmasque'] = false;

		$lsNoSeance = $liIdSéance;
		$liNoSeance = substr($lsNoSeance, 1, strlen($lsNoSeance) - 1);
		$lsJSONSeance = Planning_RtvJSONSéance($laSeance, $liNoSeance);
		$lsJSONSeances = '{"aSeances": [' . $lsJSONSeance . ']}';

		$psJSONSeances = $lsJSONSeances;

		return true;
	}

//MG Ajout 20200306 Fin
}
