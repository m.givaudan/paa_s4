<?php
namespace App\PaaBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller; Deprecated since symfony 4.2
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use App\PaaBundle\Form\Type\CredentialsType;
use App\PaaBundle\Entity\AuthToken;
use App\PaaBundle\Entity\Credentials;

use FOS\RestBundle\Routing\ClassResourceInterface ;

// Gestion des tokens d'autorisation pour les webservices getJSONEntity et getJSONEntities
// Voir exemple dans \applicationPAA\web\essaisREST.html
class AuthTokenController extends AbstractController
// S4 test class AuthTokenController extends Controller implements ClassResourceInterface
{
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"auth-token"})
     * @Rest\Post("/auth-tokens")
     */
    
    public function postAuthTokensAction(Request $request)
    {
        
        $credentials = new Credentials();
        $loForm = $this->createForm(CredentialsType::class, $credentials);
      
        $loForm->submit($request->request->all());

        if (!$loForm->isValid()) {
            return $loForm;
        }

// LG 20200513 old        $em = $this->get('doctrine.orm.entity_manager');
        $em = $this->getDoctrineManager();
        $user = $em->getRepository('PaaBundle:users')
            ->findOneByEmail($credentials->getLogin());

        if (!$user) { // L'utilisateur n'existe pas
            return $this->invalidCredentials();
        }

        $encoder = $this->get('security.password_encoder');
        $isPasswordValid = $encoder->isPasswordValid($user, $credentials->getPassword());

        if (!$isPasswordValid) { // Le mot de passe n'est pas correct
            return $this->invalidCredentials();
        }

        $authToken = new AuthToken();
        $authToken->setValue(base64_encode(random_bytes(50)));
        $authToken->setCreatedAt(new \DateTime('now'));
        $authToken->setUser($user);

        $em->persist($authToken);
        $em->flush();

		// La réponse est un abjet sérialisé en JSON. Elle est de type JSON
		$lsRep = $this->get('serializer')->serialize($authToken, 'json') ;
		$response = new Response($lsRep) ;
		$response->headers->set('Content-Type', 'application/json');
		return $response ;
    }

    private function invalidCredentials()
    {
        return new Response("AuthTokenController : KO", Response::HTTP_UNAUTHORIZED) ;
    }
}

