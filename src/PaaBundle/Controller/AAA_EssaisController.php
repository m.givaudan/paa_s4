<?php

namespace App\PaaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once(dirname(dirname(__FILE__)) . '/Component/AAA_essaiDeComponent.php');

class AAA_EssaisController extends PAABaseController {

    function __construct() {
//		new \App\Components\essaiDeComponent() ;
//		new \App\Components\VFP() ;
//		new \App\Components\xTools() ;
    }

    public function swift(\Swift_Mailer $mailer) {
//  Ceci est l'essai de base pour un message stantard ; les erreurs ne sont pas loggées
//        $message = (new \Swift_Message('Hello Email'))
//                ->setFrom('luc.gilot@sfr.fr')
//                ->setTo('h.berramdane@lyon.ort.asso.fr')
//                ->setBody('You should see me from the profiler!')
//
//        ;
//        $result = $mailer->send($message);
//        var_dump($result) ;
//        $toto = "toto";
//require_once 'lib/swift_required.php'; 

        $transport = (new \Swift_SmtpTransport('smtp.sfr.fr', 587))
                ->setUsername('luc.gilot@sfr.fr')
                ->setPassword('9t92fev1');

        $mailer = (new \Swift_Mailer($transport));
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $message = (new \Swift_Message('Subject Here'))
                ->setFrom(array('luc.gilot@sfrr' => '****'))
                ->setTo(array('h.berramdane@lyon.ort.asso.fr' => '****'));

        $message->setBody('This is the message');
        try {
            if (!$mailer->send($message, $failedRecipients)) {
                // Dump the log contents
                // NOTE: The EchoLogger dumps in realtime so dump() does nothing for it. We use ArrayLogger instead.
                echo "Error:" . $logger->dump();
                echo "failedRecipients" . $failedRecipients;
            } else {
                echo "Successfull.";
            }
        } catch (exception $e) {
            echo $e;
            echo "Error:" . $logger->dump();
            echo "failedRecipients" . $failedRecipients;
        }
                    return new Response(
                    '<html><body>Page d\'essai</body></html>'
            );
    }

    public function essai($number) {

        // Pour pouvoir utiliser une fonction PHP (et pas une méthode, il faut
        // 1. mettre le use (e.g. use App\Components\essaiDeComponent;)
        // 2. instancier la classe (e.g. $monComponent = new essaiDeComponent() ;)
        // ou simplement instancier en fournissant le path (e.g. $i = new \App\Components\essaiDeComponent() ;)
        $number = /* \App\Component\ */essaiDeFonction($number);

//		$number = \App\Component\Iif(true, "vrai", "faux") ;
//		$number = dirname(__FILE__) ;
//		$number = \App\Component\uNumVersHeure(12) ;

        return new Response(
                '<html><body>Page d\'essai: ' . $number . '</body></html>'
        );
    }

    // http://127.0.0.1/TestsSymfony/PAA_S4_AutreBDD/
    public function autreBDDAction_OLD(Request $request, $psBDD, $ps1) {
        if (!$ps1)
            $ps1 = "vide";
        return new Response(
                '<html><body>Page d\'essai, BDD= ' . $psBDD . ', ps1 = ' . $ps1 . '</body></html>'
        );
    }

}
