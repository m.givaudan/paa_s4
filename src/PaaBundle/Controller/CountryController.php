<?php

declare(strict_types = 1);

namespace App\PaaBundle\Controller;

use App\Service\CountryServiceInterface;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method; deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\Routing\Annotation\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route(path="/countries")
 */
class CountryController {

	private $countryService;

	public function __construct(CountryServiceInterface $countryService) {
		$this->countryService = $countryService;
	}

	/**
	 * @Route(path="/{id}", methods={"GET"})
	 */
	public function getOneByIdAction(int $id): Response {
		$country = $this->countryService->getOneByIdAction($id);

		return new JsonResponse($country, Response::HTTP_OK);
	}

	/**
	 * @Route(path="/{id}", methods={"PATCH"})
	 */
	public function updateOneById(int $id): Response {
		$this->countryService->updateOneById($id);

		return new JsonResponse(null, Response::HTTP_NO_CONTENT);
	}

}
