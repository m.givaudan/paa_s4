<?php

namespace App\PaaBundle\Controller;

use App\PaaBundle\Controller\PAABaseController;
use App\PaaBundle\Component\Paa\Paa_Constantes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;  // LG 20180622
// use Symfony\Component\HttpFoundation\JsonResponse;	// LG 20180622
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method; deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\Routing\Annotation\Route;
use App\Limoog\PartageBundle\Component\cParametresBDD;
use App\PaaBundle\Entity\actiGroupesacti;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
use App\PaaBundle\Entity\seances;
//use App\PaaBundle\Entity\actiRealisations;
//MG Modification 20200218 Fin
use App\PaaBundle\Entity\actiRealisations;
use App\PaaBundle\Entity\actiSsgroupesacti;
use App\PaaBundle\Entity\actibases;
use App\PaaBundle\Entity\activites;
use App\PaaBundle\Security\PaaVoter;

class ArthurVillemonController extends DefaultController {

	public function actiBaseSousGroupeGroupeAction(request $request, $psId) {
		if ($this->RedirectIfNotGranted($request, $this->isGranted(PaaVoter::VOIR . 'actibases'), $response))
			return $response;

		$laOptions = Array();
		$lbADroitDEcriture = $this->getDroitsEcritureLectureOrDie('actibases', $psId);
		$laOptions["pbDroitEcriture"] = $lbADroitDEcriture;

		$psSousType = "";
		if (substr($psId, 0, 3) == 'GAB') {
			$Type = 'GAB';
			$Id = substr($psId, 3, strlen($psId) - 3);
			$lsNomTable = "actiGroupesacti";
		} elseif (substr($psId, 0, 4) == 'SGAB') {
			$Type = 'SGAB';
			$Id = substr($psId, 4, strlen($psId) - 4);
			$lsNomTable = "actiSsgroupesacti";
		} elseif (substr($psId, 0, 2) == 'AB') {
			$Type = 'AB';
			$Id = substr($psId, 2, strlen($psId) - 2);
			$lsNomTable = "actibases";
		} else {
			$lsNomTable = "";
			$Type = null;
			// throw new exception("Cas non prévu : " . $psId);
		}
// echo $Type . '/' . $Id . "/" . $lsNomTable . "<br>";
// var_dump($loEntité) ;
// echo $toot ;
		
// LG 20191217 passé + haut        $laOptions = Array();
//MG 20200113 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            $this->fillOptionsPourRenderEnTeteMenu($laOptions);
		$this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200113 Fin Modification
		$laOptions["bAutoriseListing"] = $this->autoriseListing($lsNomTable);
		$laOptions["psNomTable"] = strtolower($lsNomTable);
		$laOptions["psURLTable"] = "actiBaseSousGroupeGroupe";
		$laOptions["psLibelléTable"] = "Arborescence des activités de base";
		$laOptions["Type"] = $Type;

		//modifier le cas pour changer d'entity en fonction de l'onglet et mettre l'onglet intervenant de base
// LG 20200813 old		if ($Type == 'AB' && $Id >= 0) {
		if ($Type == 'AB' && intval($Id) > 0) {
			if ($Id > 0) {
				$lRepo = $this->getDoctrineRepository("PaaBundle:ressourcesActibases");
				$loListeIntervenants = $lRepo->findAllPourActiBase('I', $Id);
				$loListeUsagers = $lRepo->findAllPourActiBase('U', $Id);
				$loListeGroupes = $lRepo->findAllPourActiBase('G', $Id);
				$loListeSalles = $lRepo->findAllPourActiBase('S', $Id);
			} else if ($Id == 0) {
				$m = $this->getDoctrineManager();
				$loListeIntervenants = $m->getRepository("PaaBundle:intervenants")->findBy(array());
				$loListeUsagers = $m->getRepository("PaaBundle:usagers")->findBy(array());
				$loListeGroupes = $m->getRepository("PaaBundle:groupes")->findBy(array());
				$loListeSalles = $m->getRepository("PaaBundle:salles")->findBy(array());
			}

			$laOptions["poListeIntervenants"] = $loListeIntervenants;
			$laOptions["poListeUsagers"] = $loListeUsagers;
			$laOptions["poListeGroupes"] = $loListeGroupes;
			$laOptions["poListeSalles"] = $loListeSalles;
		}

		$em = $this->getDoctrineManager();
		if ($Id == null) {
			$loEntité = null;
// echo $Type . '/' . $Id . "/" . $lsNomTable . "<br>";
// var_dump($loEntité) ;
// echo $toot ;
		} else {
			$loEntité = $em->getRepository("PaaBundle:" . $lsNomTable)->find($Id);
			if ($loEntité == null) {
				return new \Symfony\Component\HttpFoundation\Response("Enregistrement inexistant ;"
						, Response::HTTP_NOT_FOUND
						, array('content-type' => 'text/html')
				);
			}

			if ($lsNomTable == "actibases") {
				switch ($loEntité->getIsousgroupe()->getIidactisousgroupe()) {
					case '-2':
						$psSousType = 'AbsenceIntervenant';
						break;
					case '-3':
						$psSousType = 'AbsenceAutre';
						break;
					case '-4':
						$psSousType = 'AbsenceAutre';
						break;
					case '-6':
						$psSousType = 'AbsenceUsager';
						break;
					case '-7':
						$psSousType = 'AbsenceAutre';
						break;
					case '-11':
						$psSousType = 'Base';
						break;
					case '-5':
						$psSousType = 'Base';
						break;
					default:
						$psSousType = '';
				}
			}
		}

		$laFormOptions = array();
		$loForm = $this->createForm('App\PaaBundle\Form\ActiBases\\' . $lsNomTable . $psSousType . 'Type', $loEntité, $laFormOptions);
		$laOptions['poForm'] = $loForm->createView();
		$laOptions['poEntité'] = $loEntité;

		return $this->render('@Paa/form/actiBases/actiBaseSousGroupeGroupe.html.twig', $laOptions);
	}

	public function actiBasesTreeViewAction(request $request, $psNomTable) {
            
//       throw new Exception("That's too bad !");
// LG 20200511 début
//        if (!$this->isGranted(PaaVoter::VOIR . 'actibases')) {
//            // On ajoute une notification et on renvoie a la page de connexion
//            $this->addFlash("error", "Vous ne possedez pas les droits requis pour accéder a cette page.");
//            $session = $request->getSession();
//            $session->set('URLDemandée', $request->getUri());
//        }
		if ($this->RedirectIfNotGranted($request, $this->isGranted(PaaVoter::VOIR . 'actibases'), $response))
			return $response;
// LG 20200511 fin

		if (!$psNomTable)
			$psNomTable = 'actiBaseSousGroupeGroupe';
		$lsNomTable = $psNomTable;
		$laOptions = Array();

//MG 20200113 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            $this->fillOptionsPourRenderEnTeteMenu($laOptions);
		$this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200113 Fin Modification


		$laOptions["bAutoriseListing"] = $this->autoriseListing($lsNomTable);
		$laOptions["psNomTable"] = $lsNomTable;
		$lsNomTwig = null;

		$o = getORessources($a, $b);
// LG 20200513 old		$loConnexion = $this->getDoctrine()->getConnection();
		$loConnexion = $this->getDoctrineConnection();

		if ($lsNomTable === 'actiBaseSousGroupeGroupe') {
			$laOptions["psLibelléTable"] = "Arborescence des activités de base";
			$lsNomTwig = 'actiBasesTreeView';
			$resultat = $o->getActiArthur($r, $loConnexion);
		}

		//tranfère de la fonction remplirNoeudArthur dans le repository ressourcesActibasesRepository
		$lRepo = $this->get('doctrine')->getRepository("PaaBundle:ressourcesActibases");
		$res = $lRepo->remplirNoeudArthur(null, $resultat);
		$laOptions["psDataNodes"] = json_encode($res->nodes);

		// Effectuer les traitements pour l'affichage
		return $this->render('@Paa/pageAvecGrille/' . $lsNomTwig . '.html.twig', $laOptions);
	}

}
