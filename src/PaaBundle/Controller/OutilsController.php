<?php

namespace App\PaaBundle\Controller;

use App\PaaBundle\Controller\PAABaseController;
use App\PaaBundle\Component\Paa\Paa_Constantes;
use App\PaaBundle\Entity\intervenants;
use App\PaaBundle\Entity\groupes;
use App\PaaBundle\Entity\users;
use \Symfony\Component\HttpFoundation\Response;

// class DefaultController extends Controller {
class OutilsController extends PAABaseController {

	// Outils
	function OutilsAction(String $psOutil, String $psParametres) {
		// Il faut être admin
		$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Désolé, vous ne disposez pas de droits suffisants pour utiliser cette fonction');

		// Rediriger vers la méthode outil correpondante
		$lsMéthode = "Outil_" . $psOutil;
		$this->$lsMéthode($psParametres);
	}

	private function Outil_CréeUserPourChaqueIntervenant($psParametres) {
// LG 20200513 old		$loManager = $this->getDoctrine()->getManager();
		$loManager = $this->getDoctrineManager();
		$laIntervenants = $loManager->getRepository("PaaBundle:intervenants")->findAll();
		$liNbCréés = 0;
		foreach ($laIntervenants as $loIntervenant) {
			$liNbCréés += $this->CréeUserPourIntervenant($loIntervenant);
		}

		return new Response('{result: "' . $liNbCréés . '"}'
				, Response::HTTP_OK
				, array('content-type' => 'text/json')
				);
	}

	private function Outil_CreeUserPourChaqueGroupe($psParametres) {
// LG 20200513 old		$loManager = $this->getDoctrine()->getManager();
		$loManager = $this->getDoctrineManager();
		$laGroupes = $loManager->getRepository("PaaBundle:groupes")->findAll();
		$liNbCréés = 0;
		foreach ($laGroupes as $loGroupe) {
			$liNbCréés += $this->CréeUserPourGroupe($loGroupe);
		}

		return new Response('{result: "' . $liNbCréés . '"}'
				, Response::HTTP_OK
				, array('content-type' => 'text/json')
				);
	}

	// Crée une ligne de user pour l'entité Groupe fournie
	private function CréeUserPourGroupe(groupes $poGroupe) {
		$pwd = trim($poGroupe->getCnomcourt()) . "2018.!";
		$lvResult = $this->CréeUserAvecInfos("G"
				, $poGroupe->getIidgroupe()
				, $poGroupe->getCnomcourt()
				, ""
				, $pwd
				);
	}

	// Crée une ligne de user pour l'entité Intervenant fournie
	private function CréeUserPourIntervenant(intervenants $poIntervenant) {
		$pwd = trim($poIntervenant->getCnom()) . "_" . $poIntervenant->getDnaissance()->format('Y-m-d');
		$lvResult = $this->CréeUserAvecInfos("I"
				, $poIntervenant->getIidintervenant()
				, $poIntervenant->getCnomcourt()
				, $poIntervenant->getCmel()
				, $pwd
				);
	}

	// Crée une ligne de user avec les infos fournies
	private function CréeUserAvecInfos($psType_Res, $piId_Res, $psNom, $psMail, $psPwd) {
// LG 20200513 old		$loManager = $this->getDoctrine()->getManager();
		$loManager = $this->getDoctrineManager();
		$laFindBy = array("cType_Res" => $psType_Res
			, "iid_res" => $piId_Res);
		$repo = $loManager->getRepository("PaaBundle:users");
		$loUser = $repo->findBy($laFindBy);
		if ($loUser) {
			// Il y a déja un user pour cet intervenant
			return 0;
		}

		// Créer et hydrater l'entité user
// quelle différence avec la ligne suivante ?
		$loUser = new users();
//		$loUser = $repo->find(0);
		$loUser->setCtypeRes($psType_Res);
		$loUser->setIidRes($piId_Res);
		$loUser->setUsername($psNom);
		$loUser->setemail($psMail);
		$loUser->setRoles(array("ROLE_ENSEIGNANT"));

		// Mot de passe encodé
		$encoder = $this->get("security.password_encoder");
		$pwdEndodé = $encoder->encodePassword($loUser, $psPwd);
		$loUser->setPassword($pwdEndodé);

		// Enregistrer
		$loManager->persist($loUser);
		$loManager->flush();
	}

}
