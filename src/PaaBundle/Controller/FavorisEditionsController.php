<?php

namespace App\PaaBundle\Controller;

use App\PaaBundle\Controller\PAABaseController;
use App\Limoog\PartageBundle\Component\cParametresBDD;
use App\PaaBundle\Component\Paa\Paa_Constantes_Editions;
use App\PaaBundle\Component\Paa\CheckedOrNotEditions;

class FavorisEditionsController extends PAABaseController {

	public function GetFavorisAction($psId) {
// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
		$loCnxn = $this->getDoctrineConnection();
		$bdd = new cParametresBDD($loCnxn);
		$bddGet = $bdd->getParamètre("EditionFavoriJson", "", "MU");

		$jsonDecode = json_decode($bddGet, true);
		$jsonCount = count($jsonDecode);

		for ($i = 0; $i != $jsonCount; $i++) {
			if ($jsonDecode[$i]['id'] == $psId) {
				foreach ($jsonDecode[$i] as $key => $value) {
					$formulaire[] = [$key => $value];
				}
				return $formulaire;
			}
		}
	}

}
