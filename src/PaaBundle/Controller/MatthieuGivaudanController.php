<?php

namespace App\PaaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class MatthieuGivaudanController extends DefaultController {

	public function getSelectActibasesAction() {
// LG 20200513 old		$m = $this->getDoctrine()->getManager();
		$m = $this->getDoctrineManager();

		$loListeEntités = $m->getRepository("PaaBundle:activitesTrad")->findActivitesParRessources($psLst_Res);

		$laOptions = array('poListeEntités' => $loListeEntités
		);

		return $this->render('@Paa/Default/entityOptionSelect.html.twig'
						, $laOptions
		);
	}

	public function updateParticipants(Request $request) {
		
	}

}
