<?php

namespace App\PaaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Persistence\ManagerRegistry;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface ;

//AV 18/06/2019 Début
//Permet d'accéder au components/Paa qui n'ont pas de namespace
$path = dirname(__DIR__) . "/Component/Paa/Paa.php";
require_once($path);

//AV 18/06/2019 Fin

class PAABaseController extends AbstractController {
	/**
	 * Override method to call render
	 * {@inheritdoc}
	 */
//    TR 07/03/2019 Début La méthode n'éxiste plus en Symfony4
//	public function render($view
//		, array $parameters = array()
//		, Response $response = NULL) {
//		// Fournir une référence au controleur
//		$parameters["poController"] = $this ;
//		return parent::render($view, $parameters, $response) ;
//	}
//	TR 07/03/2019 Fin

	/**
	 * Savoir si on est en mode développement
	 */
	public function estDevMode() {
		$lsMode = $this->container->getParameter('kernel.environment');
		return $lsMode == 'dev';
	}

	/**
	 * Récupérer un objet Paramètres configuré
	 */
	protected function getParamètresBDD() {
		if (!isset($this->oParamètresBDD)) {
// LG 20200513 old			$loCnxn = $this->getDoctrine()->getConnection();
			$loCnxn = $this->getDoctrineConnection();
			$this->oParamètresBDD = new \App\PaaBundle\Component\Paa\PAA_Parametres($loCnxn);
			$this->oParamètresBDD->setEstDevMode($this->estDevMode());
		}
		return $this->oParamètresBDD;
	}

	// Récupérer la valeur d'un paramètre
	protected function getParamètre($psNomParamètre, $pvValeurDft, $psType) {
		return $this->getParamètresBDD()->getParamètre($psNomParamètre, $pvValeurDft, $psType);
	}

	// Poser la valeur d'un paramètre
	protected function setParamètre($psNomParamètre, $pvValeur, $psType) {
		return $this->getParamètresBDD()->setParamètre($psNomParamètre, $pvValeur, $psType);
	}

	/**
	 * Override method to call #containerInitialized method when container set.
	 * {@inheritdoc}
	 */
	public function setContainer(ContainerInterface $container = null) {
		parent::setContainer($container);
		// Selon https://stackoverflow.com/questions/10913182/execute-code-before-controllers-action
		// Déclencher l'"événement" containerInitialized
		$this->containerInitialized();
	}

	/**
	 * Selon https://stackoverflow.com/questions/10913182/execute-code-before-controllers-action
	 * Déclencher l'"événement" containerInitialized
	 * Perform some operations after controller initialized and container set.
	 */
	private function containerInitialized() {
		// Initialiser les informations pour la connexion à la base de données
		// cf applicationPAA\src\PaaBundle\Component\Connection\ConnectionWrapper.php
		// On ne peut pas se servir de la session de Symfony car elle n'est pas connue de la connexion Doctrine
		// $loRequestStack = $this->get("request_stack") ;
		// $loRequest = $loRequestStack->getCurrentRequest() ;
		// $loSession = $loRequest->getSession();
		if (!isset($_SESSION["gsVersionPAA"])) {
			//$loVersionmanager = $this->get("versionmanager") ;
			//$lsVersion = $loVersionmanager->getVersion() ;
			$lsVersion = $this->getParameter('paa_version');
			$_SESSION["gsVersionPAA"] = $lsVersion;
		}
		if (!isset($_SESSION["giUser"])) {
			$token = $this->get("security.token_storage")->getToken();
			$liUser = 0;
			if ($token !== null) {
				$loUser = $token->getUser();
				if ($loUser !== null && method_exists($loUser, 'getIidRes')) {
					$liUser = $loUser->getIidRes();
				}
			}
			$_SESSION["giUser"] = $liUser;
		}

		return;
	}

	// Récupérer une chaine de caractère décrivant l'erreur de validation du formulaire
	protected function getStringErrorsForm(\Symfony\Component\Form\Form $poForm) {
		$laErr = $this->getErrorsForm($poForm);
		$lsLstErr = "";
		foreach ($laErr As $lsErr) {
			$lsLstErr .= $lsErr . "<br>";
		}
		return $lsLstErr;
	}

	// Récupérer un tableau décrivant toutes les erreurs de validation du formulaire
	// Selon https://stackoverflow.com/questions/11208992/symfony2-invalid-form-without-errors
	protected function getErrorsForm(\Symfony\Component\Form\Form $poForm) {
		$laErr = array();
		$i = 0;

		if (!$poForm->isSubmitted()) {
			// Ca déclenche une erreur qui n'est pas inscrite dans $poForm->getErrors
			// Il faut donc la rajouter pour que l'erreur soit correctement affichée
			$laErr["Formulaire" . ($i ? $i : '')] = "Call Form::isValid() with an unsubmitted form is deprecated since Symfony 3.2 and will throw an exception in 4.0. Use Form::isSubmitted() before Form::isValid() instead.";
			$i++;
		}

		foreach ($poForm->getErrors(true) as $error) {
			// $error->getCause() ;
			$laErr["Formulaire" . ($i ? $i : '')] = "Formulaire : " . $error->getMessage();
			$i++;
		}

		foreach ($poForm as $child) {
			$i = 0;
			foreach ($child->getErrors(true) as $error) {
				$lsChildName = $child->getName();
				$lsVal = $child->getViewData();
				$lsType = gettype($lsVal);
				$error->getCause();
				$laErr[$lsChildName . ($i ? $i : '')] = "Champ $lsChildName (valeur = $lsVal, $lsType), " . $error->getMessage();
				$i++;
			}
		}

		return $laErr;
	}

	// Renvoie une liste de ressources corrigée de celles auxquelles l'utilisateur en cours n'a pas le droit de voir le planning
	public function corrigeLstRessourcesPlanningSelonAutorisation($psLstRes) {
		if ($this->isGranted('ROLE_CONCEPTEUR')) {
			// RAS
			return $psLstRes;
		} else if ($this->isGranted('ROLE_ENSEIGNANT')) {
			// Formateur : a droit aux formateurs et aux groupes
			$lsRetu = "";
			$laRes = explode(',', $psLstRes);
			foreach ($laRes as $lsRes) {
				if (in_array(substr($lsRes, 0, 1), array('I', 'G'))) {
					$lsRetu .= $lsRes . ",";
				}
			}
			return $lsRetu;
		} else if ($this->isGranted('ROLE_ETUDIANT')) {
			// Etudiant : n'a droit qu'à son groupe
			$lsRetu = "";
			$laRes = explode(',', $psLstRes);
			foreach ($laRes as $lsRes) {
				if (in_array(substr($lsRes, 0, 1), array('G'))) {
					$lsRetu .= $lsRes . ",";
				}
			}
			return $lsRetu;
		} else {
			// Aucun droit
			return "";
		}
	}

	/**
	 * Pour rendre public isGranted (normalement protected)
	 * {@inheritdoc}
	 */
//	public function isGranted($attributes, $subject = NULL) {
	public function LGisGranted($attributes, $subject = NULL) {
		return parent::isGranted($attributes, $subject);
	}

	// Gère la redirection automatique si l'user est déconnecté ou qu'il n'a pas le bon rôle
	// $request	: objet Request de la demande appellante
	//($psROLE)	: un rôle minipum nécessaire
	//($psURLDemandée)	: pour imposer une URL en retour de la demande d'authentification éventuelle
	public function RedirectIfDisconnected(Request $request, $psROLE = "", $psURLDemandée = null) {
		$lbDemandeLogin = !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
		
		if ($lbDemandeLogin And ! empty($psROLE)) {
			// On a imposé un rôle minimum : le tester
			$lbDemandeLogin = !$this->get('security.authorization_checker')->isGranted($psROLE);
		}

		if ($lbDemandeLogin) {
			// Personne n'est authentifié
			$session = $request->getSession();
			if ($psURLDemandée != null) {
				// On a fourni une URL de destination
				$URLDemandée = $psURLDemandée;
			} else {
				$URLDemandée = $request->getUri();
			}
//MG Ajout 20200908 Début
			$this->addFlash("error", "Vous êtes déconnecté.");
//MG Ajout 20200908 Fin
			$session->set('URLDemandée', $URLDemandée);
// LG 20200914 old            return $this->redirectToroute('fos_user_security_login');
            return $this->redirectToLogin();
		}

		if ($lbDemandeLogin) {
			// On ajoute une notification et on renvoie a la page de connexion
			$this->addFlash("error", "Vous ne possedez pas les droits requis pour accéder a cette page.");
			$session = $request->getSession();
			$session->set('URLDemandée', $request->getUri());
// LG 20200914 old            return $this->redirectToroute('fos_user_security_login');
            return $this->redirectToLogin();
		}
	}

	// Gère la redirection automatique si l'user est déconnecté ou qu'il n'a pas le bon rôle
	// $request		: objet Request de la demande appellante
	//$pbGranted	: true si l'utilisateur a les droits
	//&$$poResponse	: objet response pour le cas où on n'a pas les droits
	public function RedirectIfNotGranted(Request $request, $pbGranted, &$poResponse) {
// LG 20200513 début
		$msgSuppl = "" ;
// Avorté
//		// Vérifier que cette session n'a pas été ouverte sur une autre base de données
//		$sessionDBName = $this->getDBName() ;
//		if (!$sessionDBName) {
//			$sessionDBName = $this->DBName ;
//			$session->set('DBName', $sessionDBName) ;
//		}
//		if ($sessionDBName !== $this->DBName) {
//			// La session a été ouverte sur une autre base de données
//			$pbGranted = false ;
//			$msgSuppl = " Votre session indique que vous êtes déja connecté sur une autre base de données : déconnectez-vous, puis reconnectez-vous." ;
//		}
// LG 20200513 fin
		$lbDemandeLogin = !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
//MG Ajout 20200908 Début
		if($lbDemandeLogin){
			$logger = new \App\PaaBundle\Component\MyLogger('onNotifieAction');
			$idLog = $logger->Log("flash info deconnecté", "info", array(), true);
			$this->addFlash("error", "Vous avez été deconnecté suite à l'inactivité de votre session.");
// LG 20200928 déac : dble emploi avec ci-dessous			$poResponse = $this->redirect('fos_user_security_login');
// LG 20200914 old            $poResponse = $this->redirectToroute('fos_user_security_login');
            $poResponse = $this->redirectToLogin();
			return true;
		}
//MG Ajout 20200908 Fin
		
		if (!$pbGranted) {
			// On ajoute une notification et on renvoie a la page de connexion
			$loUser = $this->getUser();
			if ($loUser) {
				$lcUser = " " . $loUser->getusername();
			} else {
				$lcUser = "";
			}
// LG 20200513 old			$this->addFlash("info", "Désolé" . $lcUser . ", vous ne possedez pas les droits requis pour accéder a cette page.");
			
			$this->addFlash("info", "Désolé" . $lcUser . ", vous ne possedez pas les droits requis pour accéder a cette page." . $msgSuppl);			

//			 On renvoie à la page précédente, ou à la page de connexion
			$session = $request->getSession();
			$session->set('URLDemandée', $request->getUri());
			$referer = filter_var($request->headers->get('referer'), FILTER_SANITIZE_URL);
			if ($request->getUri() == $referer)
				$referer = '';  // Redirection vers la même page
//			if (!$referer)
//				$referer = $this->generateUrl('fos_user_security_login');
// $this->addFlash("info", "Le referer est : ." . $referer . ", l'URL demandée est : " . $request->getUri());

			$poResponse = $this->redirect($referer);
			return true;
		}		
		return false;
	}

	// LG 20200914 : redirection vers la page de login
	function redirectToLogin() {
		return $this->redirectToroute('fos_user_security_login');
	}
	
	// Ajouter une trace dans le fichier D:\Luc\Dev\DevWeb\Symfony\applicationPAA_S4\var\log\trace.log
	// Devrait être déplacé vers un parent LIMOOG de cette classe
	function trace($psLogText) {
		$ldDate = new \DateTime('NOW');
		$lsLogText = "------------------------------------------------------------------------------------------" . chr(13)
				. $ldDate->format(\DateTime::ISO8601) . chr(13)
				. $psLogText . chr(13);
		file_put_contents("..\\var\log\\trace.log", $lsLogText, FILE_APPEND);
	}
	
	// Traitement d'une requête dans laquelle on dispose des informations sur la BDD à utiliser
	// Appellé par la route PAA_AutreBDD
	//  ex. http://www.paas4.localhost.net/DB/EspaceSingulier/pageAvecGrille/annees
	// doit appeller http://www.paas4.localhost.net/pageAvecGrille/annees, pour la BDD "EspaceSingulier"
	// LG 20200513
	public function autreBDDAction(Request $request, $psBDD, $ps1, $ps2, $ps3, $ps4, $ps5, $ps6, $ps7, $ps8) {

//		throw new \Exception("La gestion des bases de données non-défault a été avortée (cf documentation Pa Symfony)") ;
// $this->addFlash("info", "L'URL demandée est : " . $request->getRequestUri());
		
		// Récupérér la route sans les infos de BDD
		$UriBrute = $request->getRequestUri() ;
		$UriBase = explode("?", $UriBrute)[0] ;		// Enlever les éventuels paramètres en GET
		$laURi = explode("/", $UriBase) ;
// LG 20200702 old		$lsDBName = strtolower($laURi[2]) ;
		$lsDBName = $laURi[2] ;
		$this->setDBName($lsDBName) ;
		$Uri = "" ;
		for ($i=3; $i < count($laURi); $i++) {
			$Uri .= "/" ;
			$Uri .= $laURi[$i] ;
		}
	
		// Extraire les paramètres de la route
		$laRoute = $this->get('router')->match($Uri) ;
		$laRouteKeys = array_keys($laRoute) ;
		$laParamètres = [] ;

		// Récupérer le cotroleur, la méthode, les arguments (dans le bon ordre)
// echo $laRoute["_controller"] . "<br>" ;
		if (strpos($laRoute["_controller"], "::")) {
			// La route contient le nom de la classe controleur
			$laControllerData = explode("::", $laRoute["_controller"]) ;
			$lsAction = $laControllerData[1] ;
			$lsController = $laControllerData[0] ;
			$this->Log("autreBDDAction redirige vers " . $lsController . "->" . $lsAction . "() [URL = " . $UriBrute . "]") ;
			$loController = new $lsController() ;
			$loController->setContainer($this->container) ;
//			$loController->setDBName($lsDBName) ;
		} else if (strpos($laRoute["_controller"], ":")) {
			// La route contient le nom du service qui pilote le contrôleur
			$laControllerData = explode(":", $laRoute["_controller"]) ;
			$lsAction = $laControllerData[1] ;
			$loController = $this->container->get($laControllerData[0]);
			$lsController = get_class($loController) ;
			$this->Log("autreBDDAction redirige vers " . $lsController . "->" . $lsAction . "() [URL = " . $UriBrute . "]") ;
		} else {
			throw new \Exception("Le contrôleur trouvé pour la route n'a pas la bonne forme : " . $laRoute["_controller"]) ;
		}
		$r = new \ReflectionMethod($lsController, $lsAction);
		$laActionParam = $r->getParameters();	// La liste des noms des arguments de la méthode du controleur, dans le bon ordre
		
		$laParamètres = [] ;
		foreach ($laActionParam as $param) {
			//$param is an instance of ReflectionParameter
			if (strtolower($param->getName()) == 'request') {
				$laParamètres["request"] = $request ;
			} else if (isset($laRoute[$param->getName()])) {
				$laParamètres[$param->getName()] = $laRoute[$param->getName()] ;
			} else if (!$param->isOptional()) {
				throw new \Exception("Le paramètre non optionnel " . $param->getName() . " n'a pas été fourni") ;
			}
		}		
			
//		// Mémoriser le nom de BDD dans la session
// deac : déja fait plus haut		$request->getSession()->set('DBName', $lsDBName) ;
// $this->addFlash("info", "L'action demandée est : " . $lsAction);
		$response = call_user_func_array([$loController, $lsAction], $laParamètres);

		return $response;
	}
	
	// Ajouter aux logs
	// LG 202000701
	protected function Log(string $psMessage, string $psNiveauLog = "info", bool $pbAvecStackTrace = false) {
		$lsProjectDir = $this->get('kernel')->getProjectDir();
		$logger = new \App\PaaBundle\Component\MyLogger('PAABaseController', $lsProjectDir);
		return $logger->Log($psMessage, $psNiveauLog, array(), $pbAvecStackTrace);
	}
	// LG 20200513
	// Nom de la base de données (doit être inscrite dans applicationPAA_S4\config\packages\doctrine.yaml
//	protected $DBName = "default" ;
	public function setDBName($psDBName) {
//		$this->DBName = $psBDDName ;
		$this->get('session')->set('DBName', $psDBName) ;
	}
	public function getDBName() {
////		return $this->DBName ;
//		$DBName = $request->getSession()->get('DBName') ;
		$DBName = $this->get('session')->get('DBName', "default") ;
// $this->addFlash("info", "DBName = " . $DBName);
		return $DBName ;
	}

// LG 20200513 début
	// L'accès à Doctrine ne doit se faire que par ici car il faut indiquer la BDD
	protected function getDoctrine(): ManagerRegistry {
		throw new \LogicException('L\'accès direct à Doctrine est interdit : passer par les raccourcis ci-dessous');
	}
	protected function getDoctrine_Secure(): ManagerRegistry {
		return parent::getDoctrine() ;
	}
	protected function getDoctrineManager() {
#LG 20200630 old		$DBName = $this->getDBName() ;
#LG 20200630 old		return $this->getDoctrine_Secure()->getManager($DBName);
		return $this->getDoctrine_Secure()->getManager();
	}
	protected function getDoctrineConnection() {
#LG 20200630 old		$DBName = $this->getDBName() ;
#LG 20200630 old		return $this->getDoctrine_Secure()->getConnection($DBName);
		return $this->getDoctrine_Secure()->getConnection();
	}
	protected function getDoctrineRepository($psRepo) {
		return $this->getDoctrineManager()->getRepository($psRepo);
	}
// LG 20200513 fin
	
}
