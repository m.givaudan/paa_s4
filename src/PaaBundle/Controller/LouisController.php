<?php

namespace App\PaaBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\PaaBundle\Controller\PAABaseController;
use App\PaaBundle\Component\Paa\Paa_Constantes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;  // LG 20180622
// use Symfony\Component\HttpFoundation\JsonResponse;	// LG 20180622
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\Routing\Annotation\Route;
use App\Limoog\PartageBundle\Component\cParametresBDD;
use App\PaaBundle\Entity\actiGroupesacti;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//use App\PaaBundle\Entity\actiRealisations;
use App\PaaBundle\Entity\seances;
//MG Modification 20200218 Fin
use App\PaaBundle\Entity\actiSsgroupesacti;
use App\PaaBundle\Entity\actibases;
use App\PaaBundle\Entity\activites;
use App\PaaBundle\Entity\annees;
use App\PaaBundle\Entity\caisses;
use App\PaaBundle\Entity\caissesPersonnes;
use App\PaaBundle\Entity\compteurs;
use App\PaaBundle\Entity\conventions;
use App\PaaBundle\Entity\etablissements;
use App\PaaBundle\Entity\groupes;
use App\PaaBundle\Entity\intervenants;
use App\PaaBundle\Entity\joursferies;
use App\PaaBundle\Entity\referents;
use App\PaaBundle\Entity\salles;
use App\PaaBundle\Entity\services;
use App\PaaBundle\Entity\specialites;
use App\PaaBundle\Entity\usagers;
use App\PaaBundle\Security\PaaVoter;
use Doctrine\ORM\Query\ResultSetMapping;

class LouisController extends DefaultController {

// LG Old    public function FirstAction(){
	public function testlouisAction() {
		$name = 'Louis';
		$laOptions = array('poListeEntités' => null,
			'psNomTable' => 'aucune',
			'name' => $name,
			'salut' => 'bonjour a tous'
		);
		$this->fillOptionsPourRenderEnTeteMenu($laOptions);
		return $this->render('@Paa/Essais/louis.html.twig'
						, $laOptions
				);

//        return $this->render('@Paa/Essais/louis.html.twig');
	}

}
