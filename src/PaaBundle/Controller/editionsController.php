<?php

namespace App\PaaBundle\Controller;

use App\PaaBundle\Controller\PAABaseController;
use App\PaaBundle\Controller\DefaultController;
use App\PaaBundle\Component\Paa\Paa_Constantes;
use App\Limoog\PartageBundle\Component\Limoog_Constantes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;  // LG 20180622
// use Symfony\Component\HttpFoundation\JsonResponse;	// LG 20180622
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method; deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\Routing\Annotation\Route;
use App\Limoog\PartageBundle\Component\cParametresBDD;
use App\PaaBundle\Component\Paa\Paa_Constantes_Editions;
use App\PaaBundle\Component\Paa\CheckedOrNotEditions;
use App\PaaBundle\Entity\actiGroupesacti;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//use App\PaaBundle\Entity\actiRealisations;
use App\PaaBundle\Entity\seances;
//MG Modification 20200218 Fin
use App\PaaBundle\Entity\actiSsgroupesacti;
use App\PaaBundle\Entity\actibases;
use App\PaaBundle\Entity\activites;
use App\PaaBundle\Entity\annees;
use App\PaaBundle\Entity\caisses;
use App\PaaBundle\Entity\caissesPersonnes;
use App\PaaBundle\Entity\compteurs;
use App\PaaBundle\Entity\conventions;
use App\PaaBundle\Entity\etablissements;
use App\PaaBundle\Entity\groupes;
use App\PaaBundle\Entity\intervenants;
use App\PaaBundle\Entity\joursferies;
use App\PaaBundle\Entity\referents;
use App\PaaBundle\Entity\salles;
use App\PaaBundle\Entity\services;
use App\PaaBundle\Entity\specialites;
use App\PaaBundle\Entity\usagers;
use App\PaaBundle\Security\PaaVoter;

// LG 20190626 début
// class editionsController extends PAABaseController {
//    private function getLibelléTable(string $psNomTable) {
//        if ($psNomTable == 'annees') {
//            return "années";
//        }
//        if ($psNomTable == 'salles') {
//            return "équipements";
//        }
//        if ($psNomTable == 'etablissements') {
//            return "établissements";
//        }
//        if ($psNomTable == 'actibases') {
//            return "activités de base";
//        }
//        if ($psNomTable == 'actiGroupesacti') {
//            return "groupes d'activités de base";
//        }
//        if ($psNomTable == 'actiSsgroupesacti') {
//            return "sous-groupes d'activités de base";
//        }
//        if ($psNomTable == 'users') {
//            return "utilisateurs de l'application";
//        }
//        if ($psNomTable == 'paa_para') {
//            return "paramètre";
//        }
//
//        return $psNomTable;
//    }
//    function getConnexionBdd() {
//
//        $m = $this->getDoctrine()->getManager();
//        $loCnxn = $this->getDoctrine()->getConnection();
//
//        return $loCnxn;
//    }
class editionsController extends DefaultController {

// LG 20190626 fin

	function optionEditionsController(Request $request, $twig, $psLstRes, $laOptions) {  // Méthode de factorisation pour la déclaration des actions
		$psNomTable = "paa_para";

// LG 20200511 début
//        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
//            // On ajoute une notification et on renvoie a la page de connexion
//            $this->addFlash("info", "Vous ne possedez pas les droits requis pour accéder a cette page.");
//            $session = $request->getSession();
//            $session->set('URLDemandée', $request->getUri());
//        }
		if ($this->RedirectIfNotGranted($request, $this->get('security.authorization_checker')->isGranted('ROLE_USER'), $response))
			return $response;
// LG 20200511 fin

		if ($this->get('security.authorization_checker')->isGranted('ROLE_CONCEPTEUR')) {
			$lsLstTypesRes = "@Tous@";
		} else if ($this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
			$lsLstTypesRes = "GI";
		} else {
			$lsLstTypesRes = "G";
		}
		// Récupérer le manager Doctrine
// LG 20200513 old		$m = $this->getDoctrine()->getManager();
		$m = $this->getDoctrineManager();
// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
		$loCnxn = $this->getDoctrineConnection();
		if ($laOptions == null) {
			$laOptions = array('psLstRes' => $psLstRes,
				'psLibelléTable' => $this->getLibelléTable($psNomTable),
				'psLstTypesRes' => $lsLstTypesRes,
				'poParametre' => new cParametresBDD($loCnxn),
				'poConstantesEditions' => new Paa_Constantes_Editions(),
				'poCheckedOrNot' => new CheckedOrNotEditions($loCnxn),
				'psNode' => 'Inconnu'
			);
		}
		// Effectuer les traitements pour l'affichage
		return $this->render('@Paa/editions/' . $twig, $laOptions);
	}

	// LG 20190626 pour debug
	protected function tmp_ClearFavoris() {
		$set = <<<EOT
				[{
			"id": "5d134eed7bbd0",
			"nom": "Laine Klemp",
			"node": "PlanningIndividuels_listing_ressources_semaines",
			"json": {
				"giTypeHorairesXL": "1",
				"giTypeCouleursPlanIndividuel": "2",
				"gbImprPlanInclActivit\u00e9s": "on",
				"gbImprPlanInclAbsences": "on",
				"gbImprPlanInclAbsencesStd": "on",
				"gbImprPlanInclPr\u00e9sences": "on",
				"gbLibell\u00e9ExclNomPlanType": "off",
				"gbImprPlanningIndividuelUtiliseNomsCourts": "off",
				"gbImprPlanningIndividuelInclRemarque": "on",
				"gbPlanningsMultiplesInclD\u00e9butFin": "off",
				"gbPlanningsMultiplesInclEffectif": "off",
				"gbPlanningsMultiplesUnePageParJour": "off",
				"cboTypeCouleurs": "2",
				"gbPlanningListingInclutBilansParAB": "off",
				"gbPlanningListingInclutBilansParCommentaire": "off"
			}
		}, {
			"id": "5d134eff5079c",
			"nom": "Georgianna Buresh",
			"node": "PlanningIndividuels_graphique",
			"json": {
				"giTypeHorairesXL": "1",
				"giTypeCouleursPlanIndividuel": "2",
				"gbImprPlanInclActivit\u00e9s": "on",
				"gbImprPlanInclAbsences": "on",
				"gbImprPlanInclAbsencesStd": "on",
				"gbImprPlanInclPr\u00e9sences": "on",
				"gbLibell\u00e9ExclNomPlanType": "off",
				"gbImprPlanningIndividuelUtiliseNomsCourts": "off",
				"gbImprPlanningIndividuelInclRemarque": "on",
				"gbPlanningsMultiplesInclD\u00e9butFin": "off",
				"gbPlanningsMultiplesInclEffectif": "off",
				"gbPlanningsMultiplesUnePageParJour": "off"
			}
		}]
EOT;
//		$this->setParamètre("EditionFavoriJson", $set, "MU");
		$this->setParamètre("EditionFavoriJson", "", "MU");
	}

	// LG 20190626 déplacé de DefaultController
	public function editAction(request $request, $pvSemaine, $psLstRes) {
// $this->tmp_ClearFavoris() ;
// LG 20200511 début
//        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
//            // On ajoute une notification et on renvoie a la page de connexion
//            $this->addFlash("info", "Vous ne possedez pas les droits requis pour accéder a cette page.");
//            $session = $request->getSession();
//            $session->set('URLDemandée', $request->getUri());
//            return $this->redirectToroute('fos_user_security_login');
//        }
		if ($this->RedirectIfNotGranted($request, $this->get('security.authorization_checker')->isGranted('ROLE_USER'), $response))
			return $response;
// LG 20200511 fin

		if ($this->get('security.authorization_checker')->isGranted('ROLE_CONCEPTEUR')) {
			$lsLstTypesRes = "@Tous@";
		} else if ($this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
			$lsLstTypesRes = "GI";
		} else {
			$lsLstTypesRes = "G";
		}
		// Récupérer le manager Doctrine
// LG 20200513 old		$m = $this->getDoctrine()->getManager();
		$m = $this->getDoctrineManager();

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {  //Si un boutton est utilisé
			$all = $request->request->all();	 //Récupération des élément passé en POST

			if (isset($_POST['btnSubmit'])) {
				// On a cliqué sur le bouton "Enregistrer"
				foreach ($all as $key => $val) {
					if ($val == 'on' || $val == "off") {
						//Vérification si la valeur de retour est "on" ou "off" c'est alors un checkbox, sinon c'est un select
						$this->setParamètre($key, $val, "L");
					} elseif (substr($key, 0, 2) == "gb") {
						$this->setParamètre($key, $val, "NU");
					}
				}
			} elseif (isset($_POST['btnFavoris'])) {
				// On a cliqué sur le boutton "Mettre en Favoris"
// echo $toto ;
				$json = $this->Favoris_requestToJSON($request);
				$this->Favoris_EnregistreFavori($json);
			} elseif (isset($_POST['btnSubmitFavori'])) {  //Si le bouton de mise a jour des favoris est utilisé
// echo $tata ;
				$json = $this->Favoris_requestToJSON($request);
				$this->Favoris_EnregistreFavori($json);
			}
		}

// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
		$loCnxn = $this->getDoctrineConnection();
		$bddGet = $this->getParamètre("EditionFavoriJson", "", "MU");
		$bddGetTwig = json_decode($bddGet, true);
		if ($bddGetTwig) {
			$countBddGet = count($bddGetTwig);
		} else {
			$countBddGet = 0;			
		}
		$jsonTwig = array();

//var_dump($bddGet) ;
		for ($i = 0; $i != $countBddGet; $i++) {
			$jsonTwig[] = ['id' => $bddGetTwig[$i]["id"]
				, 'nom' => $bddGetTwig[$i]["nom"]
				, 'node' => $bddGetTwig[$i]["node"]
			];  // Récupération de tout les favoris
		}

		$laOptions = array('pdDateDébut' => date('Y-m-d', strtotime('monday this week')),
			'pdDateFin' => date('Y-m-d', strtotime('next sunday')),
			'psLstRes' => $psLstRes,
			'psLstTypesRes' => $lsLstTypesRes,
			'poCheckedOrNot' => new CheckedOrNotEditions($loCnxn),
			'psDataNodes' => $jsonTwig
		);

		//MG 20200113 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            $this->fillOptionsPourRenderEnTeteMenu($laOptions);
		$this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200113 Fin Modification
		// Effectuer les traitements pour l'affichage
		return $this->render('@Paa/Default/edit.html.twig', $laOptions);
	}

	// Renvoyer le json d'un favori
	private function Favoris_requestToJSON($request, $psName = null) {
		$all = $request->request->all();  //Récupération des éléments passé en POST
		$node = $all['node'];
// echo $node ;
// echo '<br>' ;
// echo $toto ;
		if (isset($all['idFavori'])) {
//echo $toto ;
			$id = $all['idFavori'];
		} else {
// echo $tata ;
			$id = uniqid();
		}
		if ($psName) {
			$name = $psName;
		} else if (isset($all['nomFavori'])) {
			$name = $all['nomFavori'];
		} else {
			$name = $node . "_" . $id;
		}

		unset($all["idFavori"]);
		unset($all["nomFavori"]);
		unset($all["favori"]);
		unset($all["node"]);
		unset($all["btnFavoris"]);
		unset($all["urlConfig"]);

		$loJson = array('id' => $id
			, 'nom' => $name
			, 'node' => $node
			, 'json' => $all);
		return $loJson;
	}

	// Enregistrer un favori
	private function Favoris_EnregistreFavori($favori) {
		$lsFavorisActuels = $this->getParamètre("EditionFavoriJson", "", "MU");
		if ($lsFavorisActuels) {
			// Il y a déja des favoris
			$laFavorisActuels = $lsFavorisActuels ? json_decode($lsFavorisActuels, true) : "";
			$exist = ExistsInArray($laFavorisActuels, 'id', $favori['id']);
//echo $exist;
//echo $toto ;
		} else {
			// Il n'y a pas encore de favoris
			$laFavorisActuels = array();
			$exist = null;
		}
		$laFavorisActuels[is_numeric($exist) ? $exist : count($laFavorisActuels)] = $favori;
		$lsFavorisNouveau = json_encode($laFavorisActuels);  //tableau d'un objet Json 
		$this->setParamètre("EditionFavoriJson", $lsFavorisNouveau, "MU");
	}

	public function GetFavorisAction($psId) {
// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
		$loCnxn = $this->getDoctrineConnection();

		$bdd = new cParametresBDD($loCnxn);

		$bddGet = $bdd->getParamètre("EditionFavoriJson", "", "MU"); //Récuperation des favoris

		$jsonDecode = json_decode($bddGet);
		$jsonCount = count($jsonDecode);

//echo $toto ;
//var_dump($bddGet);
//echo "<br>" ;
//var_dump($psId) ;
//echo "<br>" ;
//echo $toto ;
		$twig = "EditionNonTrouvée";
		$laOptions = array();
		for ($i = 0; $i != $jsonCount; $i++) {
			$loFavori = $jsonDecode[$i];
			if (isset($loFavori->id) && $loFavori->id == $psId) {  //Si l'id d'un des favori est égale a l'id en paramêtre
				foreach ($loFavori as $key => $value) {
					$formulaire[$key] = $value;	 // Création du tableau associatif
				}
				if (!isset($formulaire['node']) || !$formulaire['node']) {
					// Information inexistante ou vide ?
					break;
				}
				$twig = $formulaire['node'];
//$a = $loFavori->id;
//$a = $loFavori->nom;
//$a = $loFavori->node;
//$zero = 0 ;
//$a = $loFavori->$zero;
				$objCount = count($loFavori->json);
				$CheckerOrNotEdition = new CheckedOrNotEditions($loCnxn);
//                $CheckerOrNotEdition->setjsonPredefini($formulaire['json']); //Complétion de l'attribut jsonPredefini
//                $laOptions = array('psNomFav' => $formulaire['nom'],
//                    'psIdFav' => $formulaire['id'],
//                    'poParametre' => new cParametresBDD($loCnxn),
//                    'poConstantesEditions' => new Paa_Constantes_Editions(),
//                    'poCheckedOrNot' => $CheckerOrNotEdition
//                    
//                );
				$CheckerOrNotEdition->setjsonPredefini($loFavori->json); //Complétion de l'attribut jsonPredefini
				$laOptions = array('psNomFav' => $loFavori->nom,
					'psIdFav' => $loFavori->id,
					'poParametre' => $bdd/* new cParametresBDD($loCnxn) */,
					'poConstantesEditions' => new Paa_Constantes_Editions(),
					'poCheckedOrNot' => $CheckerOrNotEdition,
					'psNode' => $formulaire['node']
				);
				break;
			}
		}
//var_dump($loFavori->json);
//var_dump($loFavori->json->giTypeCouleursPlanIndividuel);
		return $this->optionEditionsController($request, $twig . '.html.twig', "5", $laOptions);
	}

//    public function GetFavorisAction($psId) {
//        $loCnxn = $this->getDoctrine()->getConnection();
//        $bdd = new cParametresBDD($loCnxn);
//        $bddGet = $bdd->getParamètre("EditionFavoriJson", "", "MU"); //Récuperation des favoris
//
//        $jsonDecode = json_decode($bddGet);
//        $jsonCount = count($jsonDecode);
//        
//// var_dump($jsonDecode);
//        for ($i = 0; $i != $jsonCount; $i++) {
//            if ($jsonDecode[$i]->id == $psId) {  //Si l'id d'un des favori est égale a l'id en paramêtre
//                foreach ($jsonDecode[$i] as $key => $value) {
//                    $formulaire[$key] = $value;     // Création du tableau associatif
//                }
//                $objCount = count($jsonDecode[$i]->json);
//                $CheckerOrNotEdition = new CheckedOrNotEditions($loCnxn);
//                $CheckerOrNotEdition->setjsonPredefini($formulaire['json']); //Complétion de l'attribu jsonPredefini
//                $laOptions = array('psNomFav' => $formulaire['nom'],
//                    'psIdFav' => $formulaire['id'],
//                    'poParametre' => new cParametresBDD($loCnxn),
//                    'poConstantesEditions' => new Paa_Constantes_Editions(),
//                    'poCheckedOrNot' => $CheckerOrNotEdition
//                    
//                );
//                break;
//            }
//        }
//        return $this->optionEditionsController($request, $formulaire['node'] . '.html.twig', "5", $laOptions);
//    }

	public function retour($twig) {
		optionEditionsController($request, 'PlanningIndividuels_axe.html.twig');
	}

	private function IndexSelect(string $value, string $model, string $tvalue) {

// LG 20200513 old		$m = $this->getDoctrine()->getManager();
		$m = $this->getDoctrineManager();
// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
		$loCnxn = $this->getDoctrineConnection();
		$poParametre = new cParametreBDD($loCnxn);

		$resultat = $poParametre->getParamètre($value, $model, $tvalue);
		$ligne = explode("\n", $resultat);

		$nbligne = 0;

		do {
			$index = explode(":", $ligne[$nbligne]);
			$nbligne++;
		} while ($nbligne <= $ligne);

		return $index;
	}

	public function PlanningIndividuels_graphique_excelAction(request $request, $psLstRes) {
		return $this->optionEditionsController($request, 'PlanningIndividuels_graphique_excel.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_graphiqueAction(request $request, $psLstRes) {
		return $this->optionEditionsController($request, 'PlanningIndividuels_graphique.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_listing_ressources_semainesAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_listing_ressources_semaines.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_listing_ressources_continuAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_listing_ressources_continu.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_listing_ressources_moisAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_listing_ressources_mois.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_listing_ressources_jourAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_listing_ressources_jour.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_axeAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_axe.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_planning_annuelAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_planning_annuel.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_listing_differenceAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_listing_difference.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_listing_proprietaireAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_listing_proprietaire.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_calendrierAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_calendrier.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_export_googleAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_export_google.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_listing_conflitAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningIndividuels_listing_conflit.html.twig', $psLstRes, null);
	}

	public function PlanningIndividuels_export_charlemagneAction() {

		// Effectuer les traitements pour l'affichage
		return $this->render('@Paa/editions/PlanningIndividuels_export_charlemagne.html.twig');
	}

	public function PlanningsMultiplesAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningsMultiples.html.twig', $psLstRes, null);
	}

	public function PlanningsParActiviteAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PlanningsParActivite.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenantsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_CalendrierannuelExcelAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_CalendrierannuelExcel.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParServicesAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParServices.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_GraphiqueannueldeuxpagesAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_Graphiqueannueldeuxpages.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_GraphiqueannuelunepageAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_Graphiqueannuelunepage.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_GraphiquehebdomadaireAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_Graphiquehebdomadaire.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_ListingdateadateAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_Listingdateadate.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_ListinghebdomadaireAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_Listinghebdomadaire.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_ListingemargementdateadateAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_Listingemargementdateadate.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenants_ListingemargementmensuelAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants_Listingemargementmensuel.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParIntervenantsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParIntervenants.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParServices_BulletindepointagemensuelexcelAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParServices_Bulletindepointagemensuelexcel.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParServices_HebdomadaireAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParServices_Hebdomadaire.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParServices_MensuelAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParServices_Mensuel.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParServices_MensuelA3Action(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParServices_MensuelA3.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParServices_ParhorairescritiquesAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParServices_Parhorairescritiques.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_ParServices_QuotidienAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_ParServices_Quotidien.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_AbsencesparintervenantsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_Absencesparintervenants.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_SituationparintervenantsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_Situationparintervenants.html.twig', $psLstRes, null);
	}

	public function PresencesIntervenants_EtatdescompteursAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesIntervenants_Etatdescompteurs.html.twig', $psLstRes, null);
	}

	public function PresencesUsagersAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_AttestationdepresenceAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Attestationdepresence.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_FeuilledemargementAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Feuilledemargement.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_GraphiqueannueldeuxpageAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Graphiqueannueldeuxpage.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_GraphiqueannuelunepageAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Graphiqueannuelunepage.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_HeuresdedepartlemercrediAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Heuresdedepartlemercredi.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_ListingabsencesparusagerAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Listingabsencesparusager.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_ListingparcommentairesdabsencesAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Listingparcommentairesdabsences.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_PlanningabsencesparsemaineAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Planningabsencesparsemaine.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_PresentssuruneplageAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Presentssuruneplage.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_RecapabsencesjournalieresAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_Recapabsencesjournalieres.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_UsagerparUsagerAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_UsagerparUsager.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_joursparjoursAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_joursparjours.html.twig', $psLstRes, null);
	}

	public function PresencesUsagers_joursparjoursSansmotifsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'PresencesUsagers_joursparjoursSansmotifs.html.twig', $psLstRes, null);
	}

	public function RecapitulatifsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_AvancéAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_Avancé.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_Avancé_ABAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_Avancé_AB.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_Avancé_GAAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_Avancé_GA.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_Avancé_SGAAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_Avancé_SGA.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_Avancé_TTAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_Avancé_TT.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_ExtractionpourstatistiquesEspaceSingulierAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_ExtractionpourstatistiquesEspaceSingulier.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_ExtractionpourstatistiquesFreymingAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_ExtractionpourstatistiquesFreyming.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_standardAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_standard.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_standard_ABAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_standard_AB.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_standard_GAAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_standard_GA.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_standard_SGAAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_standard_SGA.html.twig', $psLstRes, null);
	}

	public function Recapitulatifs_standard_TTAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Recapitulatifs_standard_TT.html.twig', $psLstRes, null);
	}

	public function RapportsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Rapports.html.twig', $psLstRes, null);
	}

	public function ListingsAction(request $request, $psLstRes) {

		return $this->optionEditionsController($request, 'Listings.html.twig', $psLstRes, null);
	}

}
