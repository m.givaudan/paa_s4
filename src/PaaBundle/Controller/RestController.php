<?php

namespace App\PaaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

//class RestController extends Controller {
class RestController extends PAABaseController {

	// LG 20180622
	// Récupération du JSON des données d'une ligne d'une table
	// Besoin de token : cf. \applicationPAA\app\config\security.yml
	/**
	 * @Security("has_role('ROLE_USER')")
	 */
	public function getJSONEntityAction(Request $request, $psNomTable, $id) {
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
		$entity = $em->getRepository("PaaBundle:" . $psNomTable)->find($id);
		$data = $this->get('serializer')->serialize($entity, 'json');

		$response = new Response($data);
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	// LG 20180622
	// Récupération du JSON des données d'une ligne d'une table
	// Besoin de token : cf. \applicationPAA\app\config\security.yml
	/**
	 * deac : géré par RedirectIfDisconnected @ Security("has_role('ROLE_USER')")
	 */
	public function getJSONEntitiesAction(Request $request, string $psNomTable, string $psParametres) {
// echo $err;
		if ($LoginResponse = $this->RedirectIfDisconnected($request, 'ROLE_USER', $request->headers->get('referer'))) {
			// L'utilisateur en cours n'est pas authéntifié, ou sa session a expiré
			// On retourne à la page appellante
			return $LoginResponse;
		}
		$lbKO = false;
		if ($psNomTable == 'intervenantsPrésents') {
// LG 20200513 début
//			$entities = $this->getDoctrine()
//					->getRepository("PaaBundle:intervenants")
//					->findAllPrésents(null, null);
			$entities = $this->getDoctrineManager()
					->getRepository("PaaBundle:intervenants")
					->findAllPrésents(null, null);
// LG 20200513 fin
// MG Ajout 20200803 Début
			} else if ($psNomTable === 'getCompetencesCommunes'){
				$entities = $this->getDoctrineManager()
						->getRepository("PaaBundle:actibases")
						->rtvCurCompetencesCommunes($psParametres, true);
// MG Ajout 20200803 Fin
			} else if (ucwords($psNomTable) == 'Ressources' || ucwords($psNomTable) == 'RessourcesIgnoreParticipantes' || ucwords($psNomTable) == 'RessourcesPourActi') {
			$lsQueryString = $request->getQueryString();
			// Calculer la liste des ressources demandée
// LG 20200513 début
//			$entities = $this->getDoctrine()
//					->getRepository("PaaBundle:ressources")
//					->findAllAvecParametres($lsQueryString, (ucwords($psNomTable) == 'RessourcesPourActi'));
			$entities = $this->getDoctrineRepository("PaaBundle:ressources")
					->findAllAvecParametres($lsQueryString, (ucwords($psNomTable) == 'RessourcesPourActi'));
// LG 20200513 fin

			$laQueryString = array();
			parse_str($lsQueryString, $laQueryString);
			if (isset($laQueryString["iActi"]) && $laQueryString["iActi"]) {
				// On a fourni une activité : on va aussi vouloir 
				//	* la liste des ressources compétentes ou participant à l'activité
				//	* la liste des ressources participant à l'activité
				$loSerializer = $this->get('serializer');
				$lsJSONRessourcesCompétentes = $loSerializer->serialize($entities, 'json');
// LG 20200513 début
//				$entities = $this->getDoctrine()
//						->getRepository("PaaBundle:ressources")
//						->rtvCurRessourcesActivite($laQueryString["iActi"]);
				$entities = $this->getDoctrineRepository("PaaBundle:ressources")
						->rtvCurRessourcesActivite($laQueryString["iActi"]);
// LG 20200513 fin
				$lsJSONRessourcesParticipantes = $loSerializer->serialize($entities, 'json');
				$lsData = '{"Compétentes": ' . $lsJSONRessourcesCompétentes
						. ', "Participantes": ' . $lsJSONRessourcesParticipantes . '}';
				$response = new Response($lsData);
				$response->headers->set('Content-Type', 'application/json');
				return $response;
			}
		} else if (ucwords($psNomTable) == 'Planning') {
			$lsFich = dirname(dirname(__FILE__)) . '/Component/Paa/planning.php';
			require_once $lsFich;
			$lsQueryString = $request->getQueryString();
// LG 20200513 old			$loConnexion = $this->getDoctrine()->getConnection();
			$loConnexion = $this->getDoctrineConnection();
			$lbKO = !Planning_RtvJSONPlanning($lsQueryString, $loConnexion, $this, true, $lsData, $lsErreur);
		} else {
// LG 20200513 old			$em = $this->getDoctrine()->getManager();
			$em = $this->getDoctrineManager();
			$entities = $em->getRepository("PaaBundle:" . $psNomTable)->findAll();
		}
		if (!isset($lsData)) {
			$lsData = $this->get('serializer')->serialize($entities, 'json');
		}

		$response = new Response($lsData);
		if ($lbKO) {
			$response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, $lsErreur);
		} else {
			$response->headers->set('Content-Type', 'application/json');
		}

		return $response;
	}

}
