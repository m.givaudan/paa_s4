<?php

namespace App\PaaBundle\Controller;

use App\PaaBundle\Controller\PAABaseController;

class LGController extends PAABaseController {

	// e.g. http://www.paa.localhost.net/app_dev.php/testLG/teste_cbcRessource
	function testAction($psPage) {
		$laOptions = [];
//		$laOptions["poController"] = $this ;
//		$laOptions["entityAnnees"] = [] ;
//		$laOptions["entityEtablissement"] = [] ;
		$psPage = $psPage ? $psPage : 'testLG';
		return $this->render('@Paa/Essais/' . $psPage . '.html.twig', $laOptions);
	}

}
