<?php

namespace App\PaaBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\PaaBundle\Controller\PAABaseController;
use App\PaaBundle\Component\Paa\Paa_Constantes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;  // LG 20180622
// use Symfony\Component\HttpFoundation\JsonResponse;	// LG 20180622
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\Routing\Annotation\Route;
use App\Limoog\PartageBundle\Component\cParametresBDD;
use App\PaaBundle\Entity\actiGroupesacti;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
use App\PaaBundle\Entity\seances;
//use App\PaaBundle\Entity\actiRealisations;
//MG Modification 20200218 Fin
use App\PaaBundle\Entity\actiSsgroupesacti;
use App\PaaBundle\Entity\actibases;
use App\PaaBundle\Entity\activites;
use App\PaaBundle\Entity\annees;
use App\PaaBundle\Entity\caisses;
use App\PaaBundle\Entity\caissesPersonnes;
use App\PaaBundle\Entity\compteurs;
use App\PaaBundle\Entity\conventions;
use App\PaaBundle\Entity\etablissements;
use App\PaaBundle\Entity\groupes;
use App\PaaBundle\Entity\intervenants;
use App\PaaBundle\Entity\intervenantsContrats;
use App\PaaBundle\Entity\joursferies;
use App\PaaBundle\Entity\referents;
use App\PaaBundle\Entity\salles;
use App\PaaBundle\Entity\services;
use App\PaaBundle\Entity\specialites;
use App\PaaBundle\Entity\usagers;
use App\PaaBundle\Component\Logs;
use App\PaaBundle\Security\PaaVoter;
use App\PaaBundle\Component\Paa\CheckedOrNotEditions;
use App\PaaBundle\Component\Paa\Paa; //MG 20200701
use App\PaaBundle\Entity\seriesseances; //MG 20200316
use Symfony\Component\Config\Definition\Exception\Exception;

//include('../Component/Paa/Paa.php');
// class DefaultController extends Controller {

class DefaultController extends PAABaseController {

    protected function PAA_Autorise(String $psNomTable, Boolean $pbListe, Boolean $pbEcriture) {
        return true;
    }

//	// Page d'accueil : demande de mdp si pas authentifié
//	// page index sinon
//    public function accueilAction(Request $request, Router $router) {
//        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
//			// Un utilisateur est déja authentifié
//			return $this->redirectToroute('paa_index');
//		} else {
//			// Pas encore authentifié
//			// Basculer vers la demande de login et rediriger vers la page par défaut
//			// (e.g. page planning pour Etudiant et Enseignant)
//			$session = $request->getSession();	
////			$URLDemandée = $request->getUri() ;
//			$URLDemandée = $this->generateUrl("paa_default") ;
//			$session->set('URLDemandée', $URLDemandée) ;
//			return $this->redirectToroute('fos_user_security_login');
//		}
//	}
    // Page par défault (e.g. page planning pour Etudiant et Enseignant)
    public function defaultAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            // Personne n'est authentifié
            $session = $request->getSession();
//MG Modification 20200207 Début
//getUri retournait un http, besoin de https pour la securité de l'appli en prod
// LG 20200210 : ce code ne répond pas du tout à la question qui était posée, mais il peut peut-être être utile.
// IL faut le tester en dev et en prod (https), et surtout le répliquer partout où son équivalent est utilisé
// A priori, getURI est censé servir à revenir sur la bonne page une foir l'authentification refaite
            if ($request->isSecure()) {
                $URLDemandée = substr($request->getUri(), 4);
                $URLDemandée = 'https' . $URLDemandée;
            } else {
                $URLDemandée = $request->getUri();
            }
//			$URLDemandée = $request->getUri();
//MG Modification 20200207 Fin
            $session->set('URLDemandée', $URLDemandée);
// LG 20200914 old            return $this->redirectToroute('fos_user_security_login');
            return $this->redirectToLogin();
        }
        $loUser = $this->getUser();
        $laRolesUser = $loUser->getRoles();
        if (count($laRolesUser) == 2 && ($laRolesUser[0] == 'ROLE_ETUDIANT' || $laRolesUser[0] == 'ROLE_ENSEIGNANT')) {
            // Page par défaut : planning
            $lsRes = $loUser->getcRes();
            $lvSemaine = Null; // par défaut
            return $this->planningAction($request, $lvSemaine, $lsRes);
        } else {
            // Page par défaut : index
//MG 20200110 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            return $this->indexAction();
            return $this->indexAction($request);
//MG 20200110 Fin Modification
        }
    }

    // Page d'index
//MG 20200110 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
    public function indexAction(Request $request) {

        // Récupérer le manager Doctrine
//        $m = $this->getDoctrine()->getManager();
        $laOptions = Array();
//MG 20200110 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//        $this->fillOptionsPourRenderEnTeteMenu($laOptions);
        $this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200110 Fin Modification
        // Effectuer les traitements pour l'affichage
        return $this->render('@Paa/Default/index.html.twig', $laOptions);
    }

    public function planningAction(request $request, $pvSemaine, $psLstRes) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            // On ajoute une notification et on renvoie a la page de connexion
            $this->addFlash("error", "Vous ne possedez pas les droits requis pour accéder a cette page.");
            $session = $request->getSession();
            $session->set('URLDemandée', $request->getUri());
// LG 20200914 old            return $this->redirectToroute('fos_user_security_login');
            return $this->redirectToLogin();
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_CONCEPTEUR')) {
            $lsLstTypesRes = "@Tous@";
        } else if ($this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
            $lsLstTypesRes = "GI";
        } else {
            $lsLstTypesRes = "G";
        }
        // Récupérer le manager Doctrine
// LG 20200513 old		$m = $this->getDoctrine()->getManager();
        $m = $this->getDoctrineManager();

        $laOptions = array('pdDateDébut' => date('Y-m-d', strtotime('monday this week')),
            'pdDateFin' => date('Y-m-d', strtotime('next sunday')),
            'psLstRes' => $psLstRes,
            'psLstTypesRes' => $lsLstTypesRes
        );
//MG 20200110 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//        $this->fillOptionsPourRenderEnTeteMenu($laOptions);
        $this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200110 Fin Modification
        // Effectuer les traitements pour l'affichage
        return $this->render('@Paa/Planning/Planning.html.twig', $laOptions);
    }

// LG 20200513 old	public function pageAvecGrilleAction($psNomTable, request $request) {
    public function pageAvecGrilleAction(request $request, $psNomTable) {
// LG 20200511 début
//		if (!$this->isGranted(PaaVoter::VOIR . $psNomTable)) {
//			// On ajoute une notification et on renvoie a la page de connexion
//			$this->addFlash("error", "Vous ne possedez pas les droits requis pour accéder a cette page.");
//			$session = $request->getSession();
//			$session->set('URLDemandée', $request->getUri());
//		}
        if ($this->RedirectIfNotGranted($request, $this->isGranted(PaaVoter::VOIR . $psNomTable), $response))
            return $response;
		
// LG 20200511 fin

        $lsTwig = $psNomTable;
        // S'inscrire à l'événement postConnect pour effectuer l'identification sous Postgres dès l'ouverture de la connexion
        // LG 20170728 déac $this->suscribeToPostConnectionEvent();
        // Récupérer le manager Doctrine
// LG 20200513 old		
//$m = $this->getDoctrine()->getManager();
        $m = $this->getDoctrineManager();

        $loListeEntités = '';
        $loListeSéances = '';
        //Début Modif MG20191129
        // Récupérer la liste des entités à utiliser pour le traitement demandé
        if ($psNomTable == 'activitesTrad') {
            //old MG 20191202
//          $loListeEntités = $m->getRepository("PaaBundle:activitesTrad")->findBy(array());
//          //MG Modification début 20200701
            //Début Modif MG20191202;
//             $loListeEntités = $m->getRepository("PaaBundle:activitesTrad")->findAllPourSemaine(-1, 'true', 'true');
            //Fin Modif MG20191202
// LG 20200509 old			$pvDateDebut = $request->get('dateDebut');
            $pvDateDebut = $request->getSession()->get('dateDebut');

//            $pvDateFin = $request->get('dateFin'); En vu de l'amélioration de findAllPourSemaine
            $numSemaineDebut = getNoSemaine($pvDateDebut);
//			var_dump($numSemaineDebut);
//            $numSemaineFin = getNoSemaine($pvDateFin);  En vu de l'amélioration de findAllPourSemaine
            $loListeEntités = $m->getRepository("PaaBundle:activitesTrad")->findAllPourSemaine($numSemaineDebut, 'true', 'true');
            //MG Modification fin 20200701
// LG 20200127 début
            $psNomTable = 'activites';
// LG 20200127 fin
// MG 20200203 Début
        } else if ($psNomTable == 'seancesTrad') {

//			$pvDateDebut = $request->get('dateDebut');
//			$pvDateFin = $request->get('dateFin');			
            $this->refreshDatesSession($request);
            $this->refreshDatesSession($request);
            $pvDateDebut = $request->getSession()->get('dateDebut');
            $pvDateFin = $request->getSession()->get('dateFin');
//			dump($pvDateDebut);
//			dump($pvDateFin);

            $loListeSéances = $m->getRepository("PaaBundle:seancesTrad")->findSeancesParDate($pvDateDebut, $pvDateFin);

//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//			$psNomTable = 'actiRealisations'
            $psNomTable = 'seances';
//MG Modification 20200218 Fin
// MG 20200203 Fin
        } else {
            $loListeEntités = $m->getRepository("PaaBundle:" . $psNomTable)->findBy(array() /* Pas de filtre ni de tri */
                    //,array('iidAnnee' => 'ASC') // On trie par date décroissante
            );
        }
        //Fin Modif MG20191129
        $laOptions = array(
            'poListeEntités' => $loListeEntités,
            'poListeSéances' => $loListeSéances,
            'psNomTable' => $psNomTable,
        );

//MG 20200110 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//        $this->fillOptionsPourRenderEnTeteMe($laOptions);
        $this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
// LG 20200127 début
//        return $this->render('@Paa/pageAvecGrille/' . $psNomTable . '.html.twig'
//                        , $laOptions
        return $this->render('@Paa/pageAvecGrille/' . $lsTwig . '.html.twig'
                        , $laOptions
// LG 20200127 fin
        );
    }

    protected function getLibelléTable(string $psNomTable) {
        if ($psNomTable == 'annees') {
            return "années";
        }
        if ($psNomTable == 'salles') {
            return "équipements";
        }
        if ($psNomTable == 'etablissements') {
            return "établissements";
        }
        if ($psNomTable == 'actibases') {
            return "activités de base";
        }
        if ($psNomTable == 'actiGroupesacti') {
            return "groupes d'activités de base";
        }
        if ($psNomTable == 'actiSsgroupesacti') {
            return "sous-groupes d'activités de base";
        }
        if ($psNomTable == 'users') {
            return "utilisateurs de l'application";
        }

        return $psNomTable;
    }

    public function pageEntityFormAction($psNomTable, request $request, $piId) {
// LG 20200511 début
//		if (!$this->isGranted(PaaVoter::VOIR . $psNomTable, $piId)) {
//			// On ajoute une notification et on renvoie a la page de connexion
//			$this->addFlash("info", "Vous ne possedez pas les droits requis pour accéder a cette page.");
//			$session = $request->getSession();
//			$session->set('URLDemandée', $request->getUri());		
//		}
        if ($this->RedirectIfNotGranted($request, $this->isGranted(PaaVoter::VOIR . $psNomTable, $piId), $response))
            return $response;
// LG 20200511 fin
        return $this->entityFormAction($request, $psNomTable, $piId, "", true);
    }

    // Déterminer si l'utilisateur a les droits d'écriture et/ou de lecture
    // Si pas droits de lecture, lance une exception
    // Sinon, renvoie true si droits d'écriture, false sinon
    // LG 20191217
    public function getDroitsEcritureLectureOrDie($psNomTable, $piId) {
        $lbADroitDEcriture = $this->isGranted(PaaVoter::MODIFIER . $psNomTable, $piId);
        if ($lbADroitDEcriture) {
            $lbADroitDeLecture = true;
        } else {
            $lbADroitDeLecture = $this->isGranted(PaaVoter::VOIR . $psNomTable, $piId);
        }
        if (!$lbADroitDeLecture) {
            throw new Exception("Désolé, vous ne disposez pas de droits suffisants pour afficher ces informations.");
        }
        return $lbADroitDEcriture;
    }

    public function entityFormAction(Request $request, $psNomTable, $piId, $psOnglet, $pbDansPage = false) {
// LG 20200511 début
////		$this->denyAccessUnlessGranted(PaaVoter::VOIR.$psNomTable, $piId, 'Désolé, vous ne disposez pas de droits de consultation sur cet élément.');
//		if (!$this->isGranted(PaaVoter::VOIR . $psNomTable, $piId)) {
//			// L'utilisateur en cours ne dispose pas des droits sur cette page
//			$loUser = $this->getUser();
//			if ($loUser) {
//				$lcUser = " " . $loUser->getusername();
//			} else {
//				$lcUser = "";
//			}
//			$lsHTML = "Désolé" . $lcUser . ", vous n'avez pas accès à cette information.";
//			return new \Symfony\Component\HttpFoundation\Response($lsHTML);
//		}
//ob_start() ;
//var_dump($this->getUser()) ;
//$res = ob_get_clean() ;
//return new response($res) ;
		
        if ($this->RedirectIfNotGranted($request, $this->isGranted(PaaVoter::VOIR . $psNomTable, $piId), $response))
            return $response;
// LG 20200511 fin
        //MG Début 20192211
        // Déterminer les droits de lecture et d'écriture
// LG 20191217 début
//		$lbADroitDEcriture = $this->isGranted(PaaVoter::MODIFIER . $psNomTable, $piId);
//		if ($lbADroitDEcriture) {
//			$lbADroitDeLecture = true;
//		} else {
//			$lbADroitDeLecture = $this->isGranted(PaaVoter::VOIR . $psNomTable, $piId);
//		}
        $lbADroitDEcriture = $this->getDroitsEcritureLectureOrDie($psNomTable, $piId);
// LG 20191217 fin
        //MG Fin 20192211
        // Reprise de cas particuliers de noms de table
        if ($psNomTable == 'intervenantsAdresse') {
            $psNomTable = 'intervenants';
//MG Suppression 20200813
//Bloque l'onglet Adresse erreur de ma part
//            $psOnglet = 'OngletAdresse';
        }

        //Cas particulier, affichage de activitesTrad, mais form de activites demandé
        if ($psNomTable == 'activitesTrad' || $psNomTable == 'activites') {
            $psNomTable = 'activites';
            if (!$psOnglet) {
                $psOnglet = 'OngletGénéral';
            }
        }

        //MG début 20191231
        //Cas particulier, affichage de seancesTrad, mais form de actiRealisation demandé
        if ($psNomTable == 'seancesTrad' || $psNomTable == 'seances') {
            $psNomTable = 'seances';
            if (!$psOnglet) {
                $psOnglet = 'OngletSéance';
            }
        }

        //MG début 20191231
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrineManager();
        $loEntité = $em->getRepository("PaaBundle:" . $psNomTable)->find($piId);

//		echo $piId . "<br><br>" ;
//		echo $toto ;
        //MG modification 20191223
        //lsLstActibase peut ne pas exister
        $lsLstInfos = '';
        //MG modification 20200512
		//$libelleActi peut ne pas exister MG 20200904
        $libelleActi = '';
        //MG modification 20200512
        $lstJoursFeries = '';
        //MG modification 20200512
// LG 20200824 old        if ($psNomTable == 'activites') {
        if ($psNomTable == 'activites' && $loEntité) {
            $laLstRessource = $loEntité->getressources();
// LG 20200507 old			$lsLstInfos = $em->getRepository("PaaBundle:activitesTrad")->findActivitesParRessources($laLstRessource);
// MG 20200806 old			Pour la serialization et rafrachissement dynamique
//            $lsLstInfos = $em->getRepository("PaaBundle:actibases")->rtvCurCompetencesCommunes($laLstRessource);
            $lsLstInfos = $em->getRepository("PaaBundle:actibases")->rtvCurCompetencesCommunes($laLstRessource, false);
            $idActivite = $loEntité->getId(); 
			$libelleActi = $em->getRepository("PaaBundle:" . $psNomTable)->rtvlibelleacti($idActivite, 400511 );//1449087 sans balise body, mais visiblement aucun effet 
        }
        if ($psNomTable == 'seances') {
// LG 20200428 début
//			$lsLstInfos = $em->getRepository("PaaBundle:seancesTrad")->findRessourcesEligible($piId);
            $lsLstInfos = $em->getRepository("PaaBundle:seancesRessources")->RtvCurRessourcesSeance_TtesInfos(
                    $piId   // $piId_Seance
                    , ''  // $psLstType_Res = ''
                    , true  // $pbListeDesUsagersDuGroupe = 'false'
                    , true  // $pbToutesRessources = 'false'
                    , 2  // $lcEvalueCompétence = 'null'
                    , 1  // $piEvalueSiDisponible = 'null'
//                                                ,       // $piId_Acti = 'null'
//                                                ,       // $piId_ActiBase = 'null'
//                                                ,       // $ptDébutSéance = 'null'
//                                                ,       // $ptFinSéance = 'null'
//                                                ,       // $pbEvalueIncompatibilitésUsagers = 'false'
//                                                ,       // $pbEvalueHeuresFaitesEtAFaireUsagers = 'false'
//                                                ,       // $pcLstResFiltre = 'null'
//                                                ,       // $pbEvalueUneSéanceParJourUsagers = 'false'
//                                                ,       // $pbEvalueHeuresFaitesEtAFaireGroupes = 'false'
//                                                ,       // $pbInclParticipationsAnnulées = 'false'
//                                                ,       // $pbLblUsagerInclutNomGroupe = 'false'
//                                                ,       // $pvInfosSuppl = 'null'
            );
// LG 20200428 fin
//			var_dump($lsLstInfos);
			
//MG Ajout 20200904 Début
            $idActivite = $loEntité->getActivité()->getId(); 
			$libelleActi = $em->getRepository("PaaBundle:activites")->rtvlibelleacti($idActivite, 5020799 ); //6069375 sans balise body, mais visiblement aucun effet
//MG Ajout 20200904 Fin
        }

// LG 20200824 déac début : accepter les Id null pour créer une nouvelle entité (salles/intervenants/usagers/groupes : dans le cas où on crée un nouveau sans être sur l'onglet "Général"
//        if ($loEntité == null) {
//            return new \Symfony\Component\HttpFoundation\Response("Enregistrement inexistant ;"
//                    , Response::HTTP_NOT_FOUND
//                    , array('content-type' => 'text/html')
//            );
//        }
// LG 20200824 déac fin
		
        if ($psNomTable == 'users') {
            $userBDD = $em->getRepository("PaaBundle:" . $psNomTable)->find($piId);
            $pwdBDD = $userBDD->getPassword();
        }
        $laFormOptions = array();
// LG 20200601 début
        $lsType = $psNomTable;
// LG 20200601 fin
        $lsSousType = "";
        if (!isset($lsTwig)) {
            $lsTwig = $psNomTable;
        }

        if ($psNomTable == 'users') {
// LG 20200915 déac (passé dans le Form)            $laLstIntervenants = $this->get('doctrine')
// LG 20200915 déac (passé dans le Form)                    ->getRepository("PaaBundle:intervenants")
// LG 20200915 déac (passé dans le Form)                    ->findAllPrésents();
// LG 20200915 déac (passé dans le Form)            $laFormOptions['aIntervenants'] = $laLstIntervenants;
            if ($this->isGranted("ROLE_ADMIN")) {
                // L'utilisateur en cours a le droit de modifier les droits
                $lsSousType = "PlusAdmin";
            }
        }

        // ajout d'une condition pour assigner le sous type dans intervenants
        if ($psNomTable == 'intervenants') {
            if ($psOnglet == 'OngletAdresse') {
                $lsSousType = "Adresse";
            }
        }
		
        if (($psNomTable == 'intervenants' || $psNomTable == 'usagers' || $psNomTable == 'groupes' || $psNomTable == 'salles' || $psNomTable == 'seances' || $psNomTable == 'activites')
// LG 20200511 old				&& ($psOnglet == 'OngletCompétences' /* || substr($psOnglet, 0, 18) == 'OngletParticipants') */) || $psOnglet == 'OngletSérie' || $psOnglet == 'OngletLibellé' || $psOnglet == 'OngletSeanceGraphique' || $psOnglet == 'OngletSeanceListe') {
                && ($psOnglet == 'OngletCompétences') || substr($psOnglet, 0, 18) == 'OngletParticipants' || $psOnglet == 'OngletSérie' || $psOnglet == 'OngletLibellé' || $psOnglet == 'OngletSeanceGraphique' || $psOnglet == 'OngletSeanceListe') {
            $lsSousType = "SansProprietes";
        }
	
// MG Test Activites Seance:Liste gérer les onglet de la séance : ne fonctionne pas
//		if ($psNomTable == 'activites' && substr($psOnglet, 0, 18) == 'OngletParticipants'){
//			echo $toto;
//		}

// MG 20200901 Début
//		if($psNomTable == "activites" && $psOnglet == 'OngletSeanceListe'){
//			$lsType = 'seances';
//		}
// MG 20200901 Fin
//MG Test

        $lsRepertoire = '';
        $lsSousTypeForForm = '';
        if (strtolower($psNomTable) == 'actissgroupesacti' || strtolower($psNomTable) == 'actigroupesacti' || strtolower($psNomTable) == 'actibases') {
            $lsRepertoire = "ActiBases\\";
            $lsSousTypeForForm = 'actiBases/';
        }

//MG Ajout 20200316 Début
//Dans le cas de l'onglet Série, on utilise le formulaire de série
// LG 20200601 old		if ($psOnglet == 'OngletSérie') {
        if (strtolower($psNomTable) == 'seances' && $psOnglet == 'OngletSérie') {
// LG 20200601 old			$lsLstSerie = '';
            $lsLstSerie = [];
// LG 20200601 début
// 			$psNomTable = 'seriesseances';
            $lsType = 'seriesseances';
            $psNomTable = 'seances';
// LG 20200601 fin
            $loSéance = $em->getRepository("PaaBundle:seances")->find($piId);
            if (!$loSéance->getIserie()) {
                $loEntité = new seriesseances();
// LG 20200601 début
                $loEntité->setDDebut($loSéance->gettDebut()->setTime(0, 0));
                $loEntité->setDFin($loSéance->gettDebut()->add(new \DateInterval('P30D'))->setTime(0, 0));
//var_dump($loEntité->getDDebut()) ;
//var_dump($loEntité->getDFin()) ;
// LG 20200601 fin
            } else {
// LG 20200601 old				$loEntité = $em->getRepository("PaaBundle:" . $psNomTable)->find($loSéance->getIserie()->getIidserie());
// LG 20200601 old				$lsLstSerie = $em->getRepository("PaaBundle:seriesseances")->findSeriesSeance($loSéance->getIserie()->getIidserie(), 'null', 'null', 'null', 'false');
                $loEntité = $loSéance->getIserie();
                $lsLstSerie = $em->getRepository("PaaBundle:seriesseances")->findSeriesSeance($loEntité->getIidserie(), 'null', 'null', 'null', 'false');
                $lstJoursFeries = $em->getRepository("PaaBundle:seriesseances")->getJoursFeries(date("Y") . '-01-01', date("Y") . '-12-31');
            }
            $lsSousType = '';
            $lsLstInfos = $lsLstSerie;
        }
//MG Ajout 20200316 Fin
// LG 20200601 old		$lsForm = 'App\PaaBundle\Form\\' . $lsRepertoire . $psNomTable . $lsSousType . 'Type';
        $lsForm = 'App\PaaBundle\Form\\' . $lsRepertoire . $lsType . $lsSousType . 'Type';
// echo $lsForm ;
// LG 20200915 début
		// Injecter Doctrine dans le form builder
		$laFormOptions['doctrine'] = $this->get('doctrine') ;
// LG 20200915 fin
        $loForm = $this->createForm($lsForm, $loEntité, $laFormOptions);

        $laOptions = array(
            'poEntité' => $loEntité,
            'poForm' => $loForm->createView(),
            'psNomTable' => $psNomTable,
            'psLibelléTable' => $this->getLibelléTable($psNomTable),
            'bAutoriseListing' => false,
            'psOngletActif' => $psOnglet,
            //MG Ajout 20200518 Début
            'lstJoursFeries' => $lstJoursFeries,
            //MG Ajout 20200518 Fin
//MG Modification 20200210 Début
//			Renommer pour réutiliser la variable
//			'lsLstActibase' => $lsLstActibase,
            'lsLstInfos' => $lsLstInfos,
//MG Modification 20200210 Fin
//MG Ajout 20200904 Début
			'libelleActi' => $libelleActi,
//MG Ajout 20200904 Fin
        );

// LG 20200601 début
		if (strtolower($psNomTable) == 'seances' && $psOnglet == 'OngletSérie') {
			$laOptions["poSéance"] = $loSéance;
		}
// LG 20200601 fin

        if (($psNomTable == 'intervenants') && $psOnglet == 'OngletAdresse') {
            $laOptions["psURLTable"] = "intervenantsAdresse";
        }
        if (($psNomTable == 'intervenants' || $psNomTable == 'usagers' || $psNomTable == 'groupes' || $psNomTable == 'salles') && $psOnglet == 'OngletCompétences') {
            //récupération de la liste des AB, GAB, SGAB depuis la bdd            
            $o = getORessources($a, $b);
            $loConnexion = $this->getDoctrineConnection();
            $resultat = $o->getActiGabSgabArthur($r, $loConnexion);

            //convertion d'un array en un objet afin que l'on puisse créer le treeview
            $loRepo = $this->get('doctrine')->getRepository("PaaBundle:ressourcesActibases");
            $res = $loRepo->remplirNoeudArthur(null, $resultat);
            //on rajoute le node 'tous' qui permet de séléctionner toutes les compétences.
            array_unshift($res->nodes, (object) array('ctype' => 'GAB',
                        'cid' => 'GAB0',
                        'text' => 'Tous',
                        'icouleur' => 16777215,
                        'iid' => 0,
                        'cid_parent' => null));
            $laOptions["psDataNodes"] = json_encode($res->nodes);

            $loListeActiIntervenants = $loRepo->getActivitésOfRessource($psNomTable, $piId, '', null);
            $laOptions["poListeActiBases"] = $loListeActiIntervenants;
            $laOptions["pbAfficheCapacité"] = ($psNomTable == 'intervenants' || $psNomTable == 'usagers' || $psNomTable == 'salles');
            $laOptions["psTitreColNom"] = "Activité de base";
        }
        //MG 20191122
        $laOptions["pbDroitEcriture"] = $lbADroitDEcriture;
        if ($pbDansPage) {
            $lsTwig = "pageEntityForm";
            $lsRep = "Default";
//MG 20200110 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            $this->fillOptionsPourRenderEnTeteMenu($laOptions);
            $this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200110 Fin Modification
        } else {
            $lsRep = "form";
        }

        $lsTwig = '@Paa/' . $lsRep . '/' . $lsSousTypeForForm . $lsTwig . '.html.twig';
        $lsResponse = $this->render($lsTwig, $laOptions);
        return $lsResponse;
    }

    // Récupération du Type de table qui est contenu dans l'id
    public function getCodeTable($piId) {
        if (substr($piId, 0, 3) == 'GAB') {
            $piId = substr($piId, 3, strlen($piId) - 3);
            $psNomTable = "actiGroupesacti";
        } elseif (substr($piId, 0, 4) == 'SGAB') {
            $piId = substr($piId, 4, strlen($piId) - 4);
            $psNomTable = "actiSsgroupesacti";
        } elseif (substr($piId, 0, 2) == 'AB') {
            $piId = substr($piId, 2, strlen($piId) - 2);
            $psNomTable = "actibases";
        } else {
            throw new exception("Cas non prévu : " . $piId);
        }
        $res = array('id' => $piId, 'nomTable' => $psNomTable);
        return $res;
    }

    // Validation des modifications d'un item préexistant
    public function updateAction(Request $request, $psNomTable, $piId) {		
        $lsRepertoire = '';
        $lsSousType = '';
        $lbFlush = true;

		//AV 04/04/2019 début
        if (strtolower($psNomTable) == 'actibasesousgroupegroupe' || strtolower($psNomTable) == 'actibases') {
            $donnesacti = $this->getCodeTable($piId);
            $piId = $donnesacti['id'];
            $lsRepertoire = "ActiBases\\";
            $psNomTable = $donnesacti['nomTable'];
            if ($piId == 0 || $piId == 'NaN') {
                $this->addAction($request, $psNomTable);
                $response = new Response(
                        '{"idEntite": 0}', Response::HTTP_OK, array('content-type' => 'text/json')
                );
                return $response;
            }
        }
        //AV 04/04/2019 fin
        if (!$request->isMethod('POST')) {
            throw new \Exception("La requête de mise à jour doit être effectuée en POST");
        }

        if (!$this->isGranted(PaaVoter::MODIFIER . $psNomTable, $piId)) {

            // L'utilisateur en cours ne dispose pas des droits sur cette page
            $loUser = $this->getUser();
            if ($loUser) {
                throw new \Exception("Vous n'avez pas les droits requis pour une modification.");
                $lcUser = " " . $loUser->getusername();
            } else {
                $lcUser = "";
            }
            $lsHTML = "Désolé" . $lcUser . ", vous n'avez pas accès à cette information.";
            return new \Symfony\Component\HttpFoundation\Response($lsHTML);
        }

        if ($psNomTable == 'intervenantsAdresse') {
            $psNomTable = 'intervenants';
            $lsSousType = 'Adresse';
        }

// LG 20200513 old		$em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrineManager();
        $loEntité = $em->getRepository("PaaBundle:" . $psNomTable)->find($piId);
        $laFormOptions = array();
        if ($psNomTable == 'users') {
            // On doit mémoriser le mot de passe actuel pour le comparer ultérieurement avec le mdp à enregistrer
            $userBDD = $em->getRepository("PaaBundle:" . $psNomTable)->find($piId);
            $pwdBDD = $userBDD->getPassword();
            $laLstIntervenants = $this->get('doctrine')
                    ->getRepository("PaaBundle:intervenants")
                    ->findAllPrésents();
            $laFormOptions['aIntervenants'] = $laLstIntervenants;

            if ($this->isGranted("ROLE_ADMIN")) {
                // L'utilisateur en cours a le droit de modifier les droits
                $lsSousType = "PlusAdmin";
            }

            // Pour éviter l'erreur "Ce formulaire ne doit pas contenir des champs supplémentaires."
            // selon https://stackoverflow.com/questions/23018907/symfony2-4-form-this-form-should-not-contain-extra-fields-error
            $laFormOptions['allow_extra_fields'] = true;
        }


//		$loForm = $this->createForm('App\PaaBundle\Form\\' . $psNomTable . 'Type', $loEntité, $laFormOptions);
        $loForm = $this->createForm('App\PaaBundle\Form\\' . $lsRepertoire . $psNomTable . $lsSousType . 'Type', $loEntité, $laFormOptions);

//echo $loEntité->getid() . "<br>" ;
// echo $psNomTable . "<br>" ;
//var_dump($laAllFormData['OngletCompétences']) ; echo "<br>" ;
//echo $updateAction ;
//MG Modification 20200408 Début
// $laAllFormData n'existe pas, remplacement par $lajAllFormData
        $lajAllFormData = $request->request->all();
//		var_dump($lajAllFormData);
// echo isset($lajAllFormData['OngletParticipants'])?"OUI":"NON" ;
// echo $toto ;
        if ($psNomTable == 'actibases' && $piId > 0 || isset($lajAllFormData['OngletCompétences'])) {
            // On enregistre une activité de base ou les compétences d'une ressource
            // Il faut enregistrer les compétences
            $loRepo = $this->get('doctrine')->getRepository("PaaBundle:ressourcesActibases");
            if ($psNomTable == 'actibases') {
                // On traite les ressources compétentes pour une actiBase
                $loRepo->CompareLesCoches('I', $piId, $lajAllFormData);
//				$loRepo->CompareLesCoches('I', $piId, $laAllFormData);
                $loRepo->CompareLesCoches('U', $piId, $lajAllFormData);
//				$loRepo->CompareLesCoches('U', $piId, $laAllFormData);
                $loRepo->CompareLesCoches('G', $piId, $lajAllFormData);
//				$loRepo->CompareLesCoches('G', $piId, $laAllFormData);
                $loRepo->CompareLesCoches('S', $piId, $lajAllFormData);
//				$loRepo->CompareLesCoches('S', $piId, $laAllFormData);
            }
//			if (isset($laAllFormData['OngletCompétences'])) {
            if (isset($lajAllFormData['OngletCompétences'])) {
                // On traite les compétences d'une ressource
                $lsType_Res = strtoupper(substr($psNomTable, 0, 1));
//				$loRepo->CompareLesCoches('C', $lsType_Res . $piId, $laAllFormData);
                $loRepo->CompareLesCoches('C', $lsType_Res . $piId, $lajAllFormData);

                // On ne doit pas poursuivre l'enregistrement sinon on crée un enregistrement vide (????)
                $lbFlush = false;
            }

//MG Modification 20200408 Fin
            // AV 11/04/2019 fin
        }
// LG 20200508 début
////MG Ajout 20200408 Début
//		else if ($psNomTable == 'seances' && $piId > 0 || isset($lajAllFormData['OngletCompétences'])) {
//			$loRepo = $this->get('doctrine')->getRepository("PaaBundle:seancesRessources");
//			if ($psNomTable == 'seances') {
//
//				$loRepo->EnregistreParticipationAvecCoche('I', $piId, $lajAllFormData);
//				$loRepo->EnregistreParticipationAvecCoche('U', $piId, $lajAllFormData);
//				$loRepo->EnregistreParticipationAvecCoche('G', $piId, $lajAllFormData);
//				$loRepo->EnregistreParticipationAvecCoche('S', $piId, $lajAllFormData);
//			}
//		}
////MG Ajout 20200408 Fin
        else if ($psNomTable == 'seances' && isset($lajAllFormData['OngletParticipants'])) {
            // On enregistre des participants de la séance
            $loRepo = $this->get('doctrine')->getRepository("PaaBundle:seancesRessources");
            if (isset($lajAllFormData['OngletParticipants_I'])) {
                // Sous-onglet Intervenants
                $loRepo->EnregistreParticipationAvecCoche('I', $piId, $lajAllFormData);
            }
            if (isset($lajAllFormData['OngletParticipants_U'])) {
                // Sous-onglet Usagers
                $loRepo->EnregistreParticipationAvecCoche('U', $piId, $lajAllFormData);
            }
            if (isset($lajAllFormData['OngletParticipants_G'])) {
                // Sous-onglet Groupes
                $loRepo->EnregistreParticipationAvecCoche('G', $piId, $lajAllFormData);
            }
            if (isset($lajAllFormData['OngletParticipants_S'])) {
                // Sous-onglet Salles
                $loRepo->EnregistreParticipationAvecCoche('S', $piId, $lajAllFormData);
            }
        }
// LG 20200508 fin

        $loHandleRequest = $loForm->handleRequest($request);
        if (!$loHandleRequest->isSubmitted()) {
            // A partir de Symfony4, le form doit être submitted
            throw new \Exception("Erreur Limoog : La requête de mise à jour n'a pas été \"Submitted\"");
        }
        if ($loHandleRequest->isValid()) {
            if ($psNomTable == 'users') {
                if ($loEntité->getUsername() != 'dftAdmin') {
                    // Pour les users, il faut encoder le mdp s'il a changé
                    // A noter que ca semble fait dans D:\Luc\Dev\DevWeb\Symfony\applicationPAA\src\PaaBundle\Controller\UserController.php
                    // qui n'est utilisé nulle part
                    $pwd = $loEntité->getPassword();
                    $encoder = $this->get("security.password_encoder");
                    $pwdEndodé = $encoder->encodePassword($loEntité, $pwd);
                    if ($pwd === null) {
                        $loEntité->setPassword($pwdBDD);
                    } else if ($pwdEndodé !== $pwdBDD) {
                        // Le password a changé, l'encoder avant de l'enregistrer
                        $loEntité->setPassword($pwdEndodé);
                    }
                } else {
                    throw new \Exception('Impossible de modifier le mdp de ' . $loEntité->getUsername());
                }
            }
			
//MG Ajout 20200824 Début
			$em = $this->getDoctrineManager();
			$repo = $em->getRepository("PaaBundle:" . $psNomTable);
			
			if(method_exists($repo, 'checkValidité')){
				$msgErreur = $repo->checkValidité($loEntité);
			} else {
				$msgErreur = false;
			}
				
			if($msgErreur){
				return new Response('{"msgErreur": "'.$msgErreur.'"}', Response::HTTP_OK, array('content-type' => 'text/json'));
			}
//MG Ajout 20200824 Début

            if ($lbFlush) {
                // Sauvegarder les changements
                $em->flush();
            }

            // Le return permet de ne pas actualiser le formulaire lors d'une sauvegarde
            if ($loEntité == NULL) {
                $response = new Response(
                        '{"idEntite": 0}', Response::HTTP_OK, array('content-type' => 'text/json')
                );
            } else {
                $response = new Response(
                        '{"idEntite": ' . $loEntité->getId() . '}', Response::HTTP_OK, array('content-type' => 'text/json')
                );
            }
            return $response;
        } else {
            // Au moins une saisie est invalide
            $lsErreurs = $this->getStringErrorsForm($loForm);
            throw new \Exception("La requête de mise à jour n'est pas valide : " . $lsErreurs);
        }
    }

    // Validation des modifications d'un nouvel item
    public function addAction(Request $request, $psNomTable) {
        if (!$request->isMethod('POST')) {
            throw new \Exception("La requête d'ajout doit être effectuée en POST");
        }
        $lsRepertoire = '';
		if (strtolower($psNomTable) == 'actibasesousgroupegroupe') {
			$psNomTable = $request->request->all()["NomEntité"] ;
		}
        if (strtolower($psNomTable) == 'actissgroupesacti' || strtolower($psNomTable) == 'actigroupesacti' || strtolower($psNomTable) == 'actibases') {
            $lsRepertoire = "ActiBases\\";
        }
		
        $this->denyAccessUnlessGranted(PaaVoter::AJOUTER . $psNomTable, null, 'Désolé, vous ne disposez pas de droits d\'ajout d\'éléments de ce type.');

        $em = $this->getDoctrineManager();
        $repo = $em->getRepository("PaaBundle:" . $psNomTable);
        $loEntité = $repo->find(0);
        $laFormOptions = array();
        if ($psNomTable == 'users') {
            $laLstIntervenants = $this->get('doctrine')
                    ->getRepository("PaaBundle:intervenants")
                    ->findAllPrésents();
            $laFormOptions['aIntervenants'] = $laLstIntervenants;

            // Pour éviter l'erreur "Ce formulaire ne doit pas contenir des champs supplémentaires."
            // selon https://stackoverflow.com/questions/23018907/symfony2-4-form-this-form-should-not-contain-extra-fields-error
            $laFormOptions['allow_extra_fields'] = true;
        }

        $loForm = $this->get('form.factory')->create('App\PaaBundle\Form' . '\\' . $lsRepertoire . $psNomTable . 'Type', $loEntité, $laFormOptions);
        $loHandleRequest = $loForm->handleRequest($request);
        if ($loHandleRequest->isValid()) {
            // Les saisies sont valides
            $loEntité = $loForm->getData();
            if ($psNomTable == 'users') {
                // Pour les users, il faut encoder le mdp
                $encoder = $this->get("security.password_encoder");
                $pwd = $loEntité->getPassword();
                $pwdEndodé = $encoder->encodePassword($loEntité, $pwd);
                $loEntité->setPassword($pwdEndodé);
            }
//MG Ajout 20200824 Début
			if(method_exists($repo, 'checkValidité')){				
				$msgErreur = $repo->checkValidité($loEntité);
			} else {
				$msgErreur = false;
			}
				
			if($msgErreur){
				return new Response('{"msgErreur": "'.$msgErreur.'"}', Response::HTTP_OK, array('content-type' => 'text/json'));
			}
//MG Ajout 20200824 Début
			
            $em->persist($loEntité);
            $em->flush();
        } else {
            // Au moins une saisie est invalide
            $lsErreurs = $this->getStringErrorsForm($loForm);
            throw new \Exception("La requête d'ajout n'est pas valide : " . $lsErreurs);
        }

        $response = new Response(
                '{"idEntite": ' . $loEntité->getId() . '}', Response::HTTP_OK, array('content-type' => 'text/json')
        );
        return $response;
    }

    public function deleteAction(Request $request, $psNomTable, $piId) {
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrineManager();
        $loEntité = $em->getRepository("PaaBundle:" . $psNomTable)->find($piId);
// MG 20200915 début
		try {
// MG 20200915 fin
			$em->remove($loEntité);
			$em->flush();
// MG 20200915 début
		} catch (Exception $ex) {
			return new Response('{"msgErreur": "Contrainte de clé étrangère"}', Response::HTTP_OK, array('content-type' => 'text/json'));
		}
// MG 20200915 fin

        $response = new Response(
                '{"suppression": "OK"}', Response::HTTP_OK, array('content-type' => 'text/json')
        );
        return $response;
    }

    // LG 20180626 
    protected function autoriseListing($psNomTable) {
        return in_array($psNomTable, array("annees", "intervenants", "salles", "usagers", "groupes", "actibases"));
    }

    public function listingTableAction(Request $request, $psNomTable, $psParametres) {
        require_once(dirname(__DIR__) . "/Resources/editions/listings.php");

        // Simple exécution en PDF pour l'instant
// LG 20201001 old        executeListing($psNomTable, "PDF", $psParametres, $this->getDoctrineConnection());
        executeListing($psNomTable, "PDF", $psParametres, $this->getDoctrineConnection());

        $response = new Response(
                'Table : ' . $psNomTable . '<br>Paramètres : ' . $psParametres, Response::HTTP_OK, array('content-type' => 'text/html')
        );

        return $response;
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function setParametreAction($psNomParametre, $pvValeur, $psType) {
        $lsRep = $this->getParamètresBDD()->setParamètre($psNomParametre, $pvValeur, $psType);
        $response = new Response($lsRep, Response::HTTP_OK, array('content-type' => 'text/html'));
        return $response;
    }

// LG 20190510 : déplacé vers PAABaseController
//    protected function getParamètresBDD() {
//        if (!isset($this->oParamètresBDD)) {
//            $loCnxn = $this->getDoctrine()->getConnection();
//            $this->oParamètresBDD = new cParametresBDD($loCnxn);
//        }
//        return $this->oParamètresBDD;
//    }

    protected function getIdAnnéeCourante() {
        // Avec utilisation d'un paramètre défini dans parameters.yml : return $this->getParameter('IdAnnéeCourante') ;
        return $this->getParamètresBDD()->getParamètre("giAnnée", "1", "NU");  // CONSTANTE eiId_EtablissementGlobal
    }

    protected function getIdEtablissementCourant() {
        // Avec utilisation d'un paramètre défini dans parameters.yml : return $this->getParameter('IdEtablissementCourant') ;
        return $this->getParamètresBDD()->getParamètre("giEtablissement", "-1", "NU");  // CONSTANTE eiId_EtablissementGlobal
    }

    protected function getDataPourEnTeteMenu($poDoctrineManager, &$paAnnees, &$paEtablissements/* , &$paSemaine */) {

        $paAnnees = $poDoctrineManager->getRepository("PaaBundle:annees")->findListe();
        $paEtablissements = $poDoctrineManager->getRepository("PaaBundle:ressources")->findAllFiltrée("E");
//        $paSemaine = $poDoctrineManager->getRepositoty();

        return;
    }

//MG 20200110 Début Modification
//Utilisation de l'object request pour acceder a la session afin de garder certains parametres entre les pages
    function fillOptionsPourRenderEnTeteMenu(&$paOptions, Request $request) {

//MG Modification 20200203 Début
//changement de nom de la fonction
//		$session = $this->getSemaines($request);
        $session = $this->refreshDatesSession($request);
//MG Modification 20200203 Fin

        $paOptions["pdDebut"] = $session->get('dateDebut');
        $paOptions["pdFin"] = $session->get('dateFin');
//MG 20200110 Fin Modification
        // Récupérer le manager Doctrine
// LG 20200513 old		$m = $this->getDoctrine()->getManager();
        $m = $this->getDoctrineManager();

        // Listes des années et des établissements, pour affichage dans la barre de menu
        $this->getDataPourEnTeteMenu($m, $entityAnnees, $entityEtablissement/* , $paSemaine */);

        // Remplir le tableau des options
        $paOptions["entityAnnees"] = $entityAnnees;

//        $paOptions["entitySemaines"] = $entitySemaines;
        $paOptions["piIdAnnee"] = $this->getIdAnnéeCourante();
        $paOptions["entityEtablissement"] = $entityEtablissement;
        $paOptions["piIdEtablissement"] = $this->getIdEtablissementCourant();
        $paOptions["poController"] = $this;
        if (isset($paOptions["psNomTable"])) {
            $paOptions["psLibelléTable"] = $this->getLibelléTable($paOptions["psNomTable"]);
            $paOptions["bAutoriseListing"] = $this->autoriseListing($paOptions["psNomTable"]);
        }
    }

    // Sur réception d'une notification (d'erreur en général)
    // e.g. "OnNotifie?Message=Erreur : Erreur de JavaScript : Uncaught TypeError: Cannot read property 'oAut' of undefined@Chr(13)@dans le fichier http://www.paa.localhost.net/Limoog/Planning/cGrd_HTML-V2/src/cMenu_Grd.js@Chr(13)@à la ligne 237@Chr(13)@+ de détails : TypeError: Cannot read property 'oAut' of undefined@Chr(13)@    at _Class.buildItemsList_Seance (http://www.paa.localhost.net/Limoog/Planning/cGrd_HTML-V2/src/cMenu_Grd.js:237:40)@Chr(13)@    at _Class.BuildItemsList (http://www.paa.localhost.net/Limoog/Planning/cGrd_HTML-V2/src/cMenu_Grd.js:198:16)@Chr(13)@    at Object.build (http://www.paa.localhost.net/Limoog/Planning/cGrd_HTML-V2/src/cMenu_Grd.js:29:41)@Chr(13)@    at Object.contextmenuTermine [as callbackPoséParbuildAvecCallback] (http://www.paa.localhost.net/jQuery.contextMenu/jquery.contextMenu.js:287:24)@Chr(13)@    at buildCallback (http://www.paa.localhost.net/Limoog/Planning/cGrd_HTML-V2/src/cMenu_Grd.js:65:13)@Chr(13)@    at XMLHttpRequest.LG_Ajax_GetText_Navigateur.xhr_object.onreadystatechange (http://www.paa.localhost.net/Limoog/Partages/src/functions_AJAX.js:264:5)"
    // Valeur de retour : JSON validant le succès de l'enegistrement de la notification
    function onNotifieAction(Request $request) {

        $lsMsg = $request->get('Message');
		$lsIdLog = $request->get('idLog');
// LG 20200702 début
//         Logs::debug($lsMsg);
		$lsMsg = str_replace("@Chr(13)@", PHP_EOL, $lsMsg) ;
		$lsProjectDir = $this->get('kernel')->getProjectDir();
		$logger = new \App\PaaBundle\Component\MyLogger('onNotifieAction', $lsProjectDir, null, $lsIdLog?:null);
		$idLog = $logger->Log($lsMsg, "info", array(), false);
// LG 20200702 fin
        $lsRep = '{"OK":"true","idLog":"' . $idLog . '"}';
        $response = new Response($lsRep);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function ongletGeneralAction(Request $request, $psNomTable, $piId) {
        throw new Exception("Fonction marquée comme obsolète");
        $em = $this->getDoctrineManager();

        $loEntité = $em->getRepository("PaaBundle:" . $psNomTable)->find($piId);
        $loForm = $this->get('form.factory')->create('App\PaaBundle\Form' . '\\' . $psNomTable . 'Type', $loEntité);
        if ($request->isMethod('POST') && $loForm->handleRequest($request)->isValid()) {
            // Inutile de persister ici, Doctrine connait déjà notre annonce
            $em->flush();
            // Ce return permet de ne pas actualiser le formulaire lors d'une sauvegarde
            return new \Symfony\Component\HttpFoundation\Response();
        }

        return $this->render('@Paa/form:' . $psNomTable . 'OngletGeneral.html.twig', array(
                    'poEntité' => $loEntité,
                    'poForm' => $loForm->createView(),
                    'psNomTable' => $psNomTable,
                    'id' => $piId
        ));
    }

    // LG 20190219
    // Actions dédiée aux essais de pages
    public function EssaiAction($psEssai, request $request) {
        $psNomTable = "annees";
        $piId = 3;

// LG 20200511 début
//		if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
//			// On ajoute une notification et on renvoie a la page de connexion
//			$this->addFlash("info", "Vous ne possedez pas les droits requis pour accéder a cette page.");
//			$session = $request->getSession();
//			$session->set('URLDemandée', $request->getUri());
//			return $this->redirectToroute('fos_user_security_login');
//		}
        if ($this->RedirectIfNotGranted($request, $this->get('security.authorization_checker')->isGranted('ROLE_USER'), $response))
            return $response;
// LG 20200511 fin

        if (!$this->isGranted(PaaVoter::VOIR . $psNomTable, $piId)) {
            // L'utilisateur en cours ne dispose pas des droits sur cette page
            $loUser = $this->getUser();
            if ($loUser) {
                $lcUser = " " . $loUser->getusername();
            } else {
                $lcUser = "";
            }
            $lsHTML = "Désolé" . $lcUser . ", vous n'avez pas accès à cette information.";
            return new \Symfony\Component\HttpFoundation\Response($lsHTML);
        }

// LG 20200513 old		$em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrineManager();

        $loEntité = $em->getRepository("PaaBundle:annees")->find(3);
        $loForm = $this->get('form.factory')->create('App\PaaBundle\Form\anneesType', $loEntité);

        $laOptions = array(
            'poEntité' => $loEntité,
            'poForm' => $loForm->createView(),
            'psNomTable' => "Essai",
            'psLibelléTable' => "Essai",
            'psOngletActif' => "Général"
        );
//MG 20200110 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//        $this->fillOptionsPourRenderEnTeteMenu($laOptions);
        $this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200110 Fin Modification
        return $this->render('@Paa/Essais/Essai.html.twig', $laOptions);
    }

//MG Ajout Début 20200110  
//Renvoi les semaines à utilisées 
//MG Modification 20200203 Début
//	public function getSemaines(Request $request) {
// Le nom de la fonction ne correspond plus a son action
    public function refreshDatesSession(Request $request) {
//MG Modification 20200203 Fin
// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
        $loCnxn = $this->getDoctrineConnection();
        $oParamètresBDD = new cParametresBDD($loCnxn);
        $loUser = $this->getUser();
// LG 20200427 début
// 		$laRolesUser = $loUser->getRoles();
        if ($loUser) {
            $laRolesUser = $loUser->getRoles();
        } else {
            $laRolesUser = [];
        }
// LG 20200427 fin
        $giNbSemAvance = (int) $oParamètresBDD->getParamètre('giNbSemAvance', '0', 'N');

        $session = $request->getSession();
        $params = $request->query;

        if ($params->get('dateDebut')) {
            $session->set('dateDebut', $params->get('dateDebut'));
        } else if (!$params->get('dateDebut') && !$session->get('dateDebut')) {
            $session->set('dateDebut', date("Y-m-d", strtotime("monday")));
        } else {
            $session->set('dateDebut', $session->get('dateDebut'));
        }
//        $numSemaineDebut = getNoSemaine($pvDateDebut);

        if ($params->get('dateFin')) {
            $session->set('dateFin', $params->get('dateFin'));
        } else if (!$params->get('dateFin') && !$session->get('dateFin')) {
            $session->set('dateFin', date("Y-m-d", strtotime("sunday")));
        } else if (!$params->get('dateFin') && !$session->get('dateFin') && $laRolesUser[0] === 'ROLE_CONCEPTEUR') {
            $session->set('dateFin', date("Y-m-d", strtotime("+" . $giNbSemAvance . "week", strtotime("sunday"))));
        } else {
            $session->set('dateFin', $session->get('dateFin'));
        }
//        $numSemaineFin = getNoSemaine($pvDateFin);
//                var_dump($session->get('dateDebut'));
//                var_dump($session->get('dateFin'));

        return $session;
    }

//MG Ajout Fin 20200110 
//MG Ajout Debut 20200113
//MG Modification 202002003 Début
// Le nom ne correspondait pas avec le nom de la route
//public function setVariableSession($psNomParametre, $pvValeur, $psType, Request $request) {
    public function setVariableDeSession($psNomParametre, $pvValeur, $psType, Request $request) {
//MG Modification 202002003 Fin
        $session = $request->getSession();

        if ($psNomParametre === 'dateDebut') {
            $session->set('dateDebut', $pvValeur);
        }
        if ($psNomParametre === 'dateFin') {
            $session->set('dateFin', $pvValeur);
        }
        $response = new Response('ok', Response::HTTP_OK, array('content-type' => 'text/html'));
        return $response;
    }

//MG Ajout Fin 20200113
//MG Ajout Debut 20200117
//MG Modification 20200225 Début
//	public function entityForActiviteAction(, $piId, Request $request) {
    public function entityForActiviteAction($psNomTable, $piId, Request $request) {
//MG Modification 20200225 Fin
//MG Modification 20200225 Début
//$em = $this->getDoctrine()->getManager();
//		$seances = $em->getRepository("PaaBundle:actiRealisations")->find($piId);
//		$idActivite = $seances->getActivité();
//		return $this->entityFormAction($request, 'activites', $idActivite, "");
//		return $this->entityFormAction($request, 'actiRealisations', $piId, "");
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//		return $this->entityFormAction($request, 'actiRealisations', $piId, "");
        //return $this->entityFormAction($request, 'seances', $piId, "");
//MG Modification 20200218 Fin
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrineManager();
        if ($psNomTable === "seancesTrad") {
            return $this->entityFormAction($request, 'seances', $piId, "");
        } else if ($psNomTable === "activitesTrad") {
            $seances = $em->getRepository("PaaBundle:seances")->find($piId);
            $idActivite = $seances->getActivité();
            return $this->entityFormAction($request, 'activites', $idActivite, "");
        }
//MG Modification 20200225 Fin
    }

//MG Ajout Fin   20200117
//MG Ajout Debut 20200403
//	public function updateParticipant($loForm, Request $request){
//		if ($loForm->isSubmitted() && $loForm->isValid()) {
//			$resform = $request->request->all();
//			var_dump($resform);
//        }
//	}
//MG Ajout Fin 20200403

	function rapportErreurClientAction(Request $request) {
        $LogId = $request->request->get('iderreur');
        $Message = $request->request->get('Message');
// Si le message n'est pas vide, il sera envoyé dans les logs
        if ($Message != "") {
            $Rapport = "Message Utilisateur en commentaire du bug [Id: " . $LogId . "] : " . PHP_EOL . chr(9) . $Message;
//            \App\PaaBundle\Component\Logs::rapportuser($Rapport);
			$lsProjectDir = $this->get('kernel')->getProjectDir();
			$logger = new \App\PaaBundle\Component\MyLogger('PAABaseController', $lsProjectDir);
			$logger->Log($Rapport, "info", array(), false);
		}

        if (isset($_POST['retourpage'])) {
            return $this->redirectToRoute($_SERVER['HTTP_REFERER']);
        }
        if (isset($_POST['accueil'])) {
            return $this->redirectToRoute('paa_index');
        }
        return new Response("Votre requete à bien été envoyée !");
	}
	
}
