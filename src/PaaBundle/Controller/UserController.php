<?php

namespace App\PaaBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\Routing\Annotation\Route;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller; Deprecated since symfony 4.2
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use App\PaaBundle\Form\Type\UserType;
use App\PaaBundle\Entity\users;

class UserController extends AbstractController {

	/**
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"user"})
	 * @Rest\Post("/users")
	 */
	public function postUsersAction(Request $request) {
		$user = new Users();
		$loForm = $this->createForm(UserType::class, $user, ['validation_groups' => ['Default', 'New']]);

		$loForm->submit($request->request->all());
		if ($loForm->isValid()) {
			$encoder = $this->get('security.password_encoder');
			// le mot de passe en clair est encodé avant la sauvegarde
			$encoded = $encoder->encodePassword($user, $user->getPlainPassword());
			$user->setPassword($encoded);

// LG 20200513 old			$em = $this->get('doctrine.orm.entity_manager');
			$em = $this->getDoctrineManager();
			$em->persist($user);
			$em->flush();
			return $user;
		} else {
			return $loForm;
		}
	}

	/**
	 * @Rest\View(serializerGroups={"user"})
	 * @Rest\Put("/users/{id}")
	 */
	public function updateUserAction(Request $request) {
		return $this->updateUser($request, true);
	}

	/**
	 * @Rest\View(serializerGroups={"user"})
	 * @Rest\Patch("/users/{id}")
	 */
	public function patchUserAction(Request $request) {
		return $this->updateUser($request, false);
	}

	private function updateUser(Request $request, $clearMissing) {
// LG 20200513 old		$user = $this->get((doctrine.orm.entity_manager')
		$user = $this->getDoctrineManager()
				->getRepository('PaaBundle:User')
				->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
		/* @var $user User */

		if (empty($user)) {
			return $this->userNotFound();
		}

		if ($clearMissing) { // Si une mise à jour complète, le mot de passe doit être validé
			$options = ['validation_groups' => ['Default', 'FullUpdate']];
		} else {
			$options = []; // Le groupe de validation par défaut de Symfony est Default
		}

		$loForm = $this->createForm(UserType::class, $user, $options);

		$loForm->submit($request->request->all(), $clearMissing);

		if ($loForm->isValid()) {
			// Si l'utilisateur veut changer son mot de passe
			if (!empty($user->getPlainPassword())) {
				$encoder = $this->get('security.password_encoder');
				$encoded = $encoder->encodePassword($user, $user->getPlainPassword());
				$user->setPassword($encoded);
			}
// LG 20200513 old			$em = $this->get('doctrine.orm.entity_manager');
			$em = $this->getDoctrineManager();
			$em->merge($user);
			$em->flush();
			return $user;
		} else {
			return $loForm;
		}
	}

	private function userNotFound() {
		return \FOS\RestBundle\View\View::create(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
	}

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

