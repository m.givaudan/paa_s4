<?php

namespace App\PaaBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\PaaBundle\Controller\PAABaseController;
use App\PaaBundle\Component\Paa\Paa_Constantes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;  // LG 20180622
// use Symfony\Component\HttpFoundation\JsonResponse;	// LG 20180622
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;deprecated since version 5.2. Use "Symfony\Component\Routing\Annotation\Route"
use Symfony\Component\Routing\Annotation\Route;
use App\Limoog\PartageBundle\Component\cParametresBDD;
use App\PaaBundle\Entity\actiGroupesacti;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//use App\PaaBundle\Entity\actiRealisations;
use App\PaaBundle\Entity\seances;
//MG Modification 20200218 Fin
use App\PaaBundle\Entity\actiSsgroupesacti;
use App\PaaBundle\Entity\actibases;
use App\PaaBundle\Entity\activites;
use App\PaaBundle\Entity\annees;
use App\PaaBundle\Entity\caisses;
use App\PaaBundle\Entity\caissesPersonnes;
use App\PaaBundle\Entity\compteurs;
use App\PaaBundle\Entity\conventions;
use App\PaaBundle\Entity\etablissements;
use App\PaaBundle\Entity\groupes;
use App\PaaBundle\Entity\intervenants;
use App\PaaBundle\Entity\joursferies;
use App\PaaBundle\Entity\referents;
use App\PaaBundle\Entity\salles;
use App\PaaBundle\Entity\services;
use App\PaaBundle\Entity\specialites;
use App\PaaBundle\Entity\usagers;
use App\PaaBundle\Security\PaaVoter;
use Doctrine\ORM\Query\ResultSetMapping;

class YohanCascheraController extends DefaultController {

// LG Old    public function FirstAction(){
	public function Teste_GetIDActivitesAction(Request $request) {

//      $psNomTable = "annees";
//      $piId = 3;
//      $Manager = $this->getDoctrine()->getManager();
//      
//      
//      
////      $loForm = $this->createForm('App\PaaBundle\Form\TestYohanType');
////      $option = array('form' => $loForm->createView(),
////                        
////            );
//      
//              $loEntité = $Manager->getRepository("PaaBundle:annees")->find(3);
//              $laOptions = array(                    'poEntité' => $loEntité,);
//      		$this->fillOptionsPourRenderEnTeteMenu($loEntité) ;
//        return $this->render('@Paa/Default/TestYohan.html.twig');
		// info Obligatoir menu 
		$psNomTable = "annees";
		$piId = 3;
		// Manager Symfony
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
		// info Obligatoir Menu
		$loEntité = $em->getRepository("PaaBundle:annees")->find(3);
		$activiter = $em->getRepository("PaaBundle:activites");
		// Mon Formulaire 
		$pLstRes = ['USAGER 1' => 'U1', 'USAGER2' => 'U2'];
		$pNomResActive = 'U2';
		$loForm = $this->createForm('App\PaaBundle\Form\IDActivitesType', null, ['LstRes' => $pLstRes, 'PnomResActiv' => $pNomResActive]);
		// CE CODE EST DUPLIQUE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Cette requête est à déplacer dans le repository de Activites : méthode "rtvCurActivitesRessource" avec les mêmes paramètres
		// libellé court : 2174 = eiFormatLibelléStdRéduit
		// libellé long : 1151 + 2^12
		$function = $em->getConnection()->prepare("SELECT * FROM paa.rtvCurActivitesRessource('I1','-1',null,null,null,null,null,null,true,2174::integer,(1151+2^12)::integer)");
		$function->execute();
		$curActivitesRessource = $function->fetchall();

		$laOptions = array(
			'poEntité' => $loEntité,
			'poForm' => $loForm->createView(),
			'paActivitésRessource' => $curActivitesRessource,
			'psOngletActif' => "",
// LG début
			'psSSOngletActif' => "",
			'pddate' => '2018-01-01',
			'piHeureDeb' => '00:00',
			'piHeureFin' => '00:00'
// LG fin			
		);

		//MG 20200113 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            $this->fillOptionsPourRenderEnTeteMenu($laOptions);
		$this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200113 Fin Modification
// LG old		return $this->render('@Paa/Default/getIDActivites.html.twig', $laOptions);
		$render = $this->render('@Paa/Essais/pageTest_GetIDActivites.html.twig', $laOptions);
		return $render;
	}

// LG old    public function PopupAction ()
	public function TestePopup_getIDActivitesAction(Request $request) {



		$psNomTable = "annees";
		$piId = 3;
		// Manager Symfony
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
		// info Obligatoir Menu
		$loEntité = $em->getRepository("PaaBundle:annees")->find(3);
		// Mon Formulaire 
		$loForm = $this->createForm('App\PaaBundle\Form\IDActivitesType');

		// CE CODE EST DUPLIQUE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Cette requête est à déplacer dans le repository de Activites : méthode "rtvCurActivitesRessource" avec les mêmes paramètres
		// libellé court : 2174 = eiFormatLibelléStdRéduit
		// libellé long : 1151 + 2^12
		// 
		// 
		// 
		// YC  Transfert de la fonction dans le Répository activites
//		$function = $em->getConnection()->prepare("SELECT * FROM paa.rtvCurActivitesRessource('I1','-1',null,null,null,null,null,null,true,2174::integer,(1151+2^12)::integer)");
//		$function->execute();
//		$curActivitesRessource = $function->fetchall();
		$activiter = $em->getRepository("PaaBundle:activites");
		$curActivitesRessource = $activiter->rtvCurActivitesRessource('I1', '-1', 'null', 'null', 'null', 'null', 'null', 'null', 'true', 2174, 9343);

//      Temporaire !!! Duré du Teste 

		$poform = $this->createForm('App\PaaBundle\Form\IDActiviteTestTempPopUpType');


		$laOptions = array(
			'poEntité' => $loEntité,
			'poForm' => $loForm->createView(),
			'paActivitésRessource' => $curActivitesRessource,
			'psOngletActif' => "",
			'psSSOngletActif' => "",
			'LoForm' => $poform->createView()
		);


		// Tentative de recuperation ;
		$loHandleRequest = $loForm->handleRequest($request);

		if ($loHandleRequest->isSubmitted()) {
			$data = $loForm->getData();
			return $this->redirectToRoute('getIDActivites');
		} else {
			
		}

		//  fin de tentative
		//MG 20200113 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            $this->fillOptionsPourRenderEnTeteMenu($laOptions);
		$this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200113 Fin Modification
// LG old		return $this->render('@Paa/Dialog\getIDActivite:getIDActivitesPoPuP.html.twig', $laOptions);
		return $this->render('@Paa/Essais/pageTestPopup_getIDActivites.html.twig', $laOptions);
	}

// LG old    public function TestFormAction()
	public function getIDActivitesAction($psLstRes, $psResActive, $pdDate, $piHeureDeb, $piHeureFin, Request $request) {
//    public function getIDActivitesAction($psLstRes, $psResActive, $pdDate, $piHeureDeb, $piHeureFin) {
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		
//MG Ajout 20200928 Début
if ($LoginResponse = $this->RedirectIfDisconnected($request, 'ROLE_USER', $request->headers->get('referer'))) {
			// L'utilisateur en cours n'est pas authentifié, ou sa session a expiré
			// On retourne à la page appellante ($request->headers->get('referer'))
			return $LoginResponse;
		}        
//MG Ajout 20200928 Fin
		
		$em = $this->getDoctrineManager();
		$pResult = $em->getRepository("PaaBundle:ressources")->FindAllList($psLstRes);

		foreach ($pResult as $res) {
			$pLstRes['' . $res['cnom_res'] . ''] = $text = str_replace(' ', '', $res['ctype_res'] . $res['iid_res']);
		}

		// récuperation des paramètre et Passage au Formulaire 
// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
		$loCnxn = $this->getDoctrineConnection();
		$bdd = new cParametresBDD($loCnxn);
		$psOngletActif = $bdd->getParamètre('psOngletActifGetIdActivites', 'OngletActivité', 'TU');
		$psSSOngletActif = $bdd->getParamètre('psSSOngletActifGetIdActivites', 'OngletGénéral', 'TU');
		$pIncactgen = $bdd->getParamètre("Incactgen", "false", "LU");
		$pIncsactsys = $bdd->getParamètre("Incsactsys", "false", "LU");
		$puniressconce = $bdd->getParamètre("uniressconce", "true", "LU");
		$lsGroupeSélectionné = $bdd->getParamètre("pgroupActi", "null", "NU");
		$lsStyleAB = $bdd->getParamètre("pparam", 1, "NU");

// echo $lsGroupeSélectionné ;
//echo $toto ;
		$RepoOGroupActi = $em->getRepository(actiGroupesacti::class);
		if ($lsGroupeSélectionné == null || $lsGroupeSélectionné == 0) {
			$loGroupeSélectionné = null;
//			echo $toto ;
		} else {
			$loGroupeSélectionné = $RepoOGroupActi->find((int) $lsGroupeSélectionné);
		}
//MG Modification 20200304 Début
		// Création du formulaire 
//        $loForm = $this->createForm('App\PaaBundle\Form\IDActivitesType', null
//										, ['LstRes'=>$pLstRes,
//											'PnomResActiv' => $psResActive,
//											'Incsactsys'=>$pIncsactsys,
//											'Incactgen'=>$pIncactgen,
//											'uniressconce'=>$puniressconce,
//											'oGroupeSélectionné'=>$loGroupeSélectionné,
//											'pparam'=>$lsStyleAB
//										]);

		$options = [
			'LstRes' => $pLstRes,
			'PnomResActiv' => $psResActive,
			'Incsactsys' => $pIncsactsys,
			'Incactgen' => $pIncactgen,
			'uniressconce' => $puniressconce,
			'oGroupeSélectionné' => $loGroupeSélectionné,
			'pparam' => $lsStyleAB
		];

		$loForm = $this->createForm('App\PaaBundle\Form\IDActivitesType', null, $options);
//MG Modification 20200304 Début        
// LG 20190718 début
//        // Inclulsion pour NpSemaine
//        $path = dirname(__DIR__) . "/Component/Paa/Paa.php";
//        require_once($path);
//        $PnoSemaine = getNoSemaine($pdDate);
//        //  liste des activités
//		$req = "SELECT * FROM paa.rtvCurActivitesRessource('$psResActive', '$PnoSemaine', null, $pIncsactsys, $pIncactgen, $puniressconce, $oGroupeSélectionné, null, true, 2174::integer, (1151+2^12)::integer)";
//		$function = $em->getConnection()->prepare($req);
//        $function->execute();
//        $curActivitesRessource = $function->fetchall();
//        $opt = array('paActivitésRessource' => $curActivitesRessource,
//            'psSSOngletActif' => $psSSOngletActif,
//            'psOngletActif' => $psOngletActif,
//            'poForm' => $loForm->createView(),
//            'psLstRes' => $psLstRes, // Variable Formulaire
//            'psResActive' => $psResActive, // Variable Formaulaire,
//            'PnomResActiv' => $psResActive,
//            'pddate' => $pdDate,
//            'piHeureDeb' => $piHeureDeb,
//            'piHeureFin' => $piHeureFin
//        );
		$opt = array('psSSOngletActif' => $psSSOngletActif,
			'psOngletActif' => $psOngletActif,
			'poForm' => $loForm->createView(),
			'psLstRes' => $psLstRes, // Variable Formulaire
			'psResActive' => $psResActive, // Variable Formaulaire,
			'PnomResActiv' => $psResActive,
			'pddate' => $pdDate,
			'piHeureDeb' => $piHeureDeb,
			'piHeureFin' => $piHeureFin,
			'psStyleAB' => $lsStyleAB
		);

		$this->ComplèteTableauPourgetIDActivites_Datatable($opt, $psLstRes, $pdDate
				, $pIncsactsys, $pIncactgen, $puniressconce, $lsGroupeSélectionné);

// LG 20190718 fin

		return $this->render('@Paa/Dialog/getIDActivite/getIDActivites.html.twig', $opt);
	}

	private function ComplèteTableauPourgetIDActivites_Datatable(array &$opt, $pRes, $pdDate
	, $Incsactsys, $Incactgen, $uniressconce, $piGroupeActi) {
		//  liste des activités
		// Inclulsion pour NpSemaine
		$path = dirname(__DIR__) . "/Component/Paa/Paa.php";
		require_once($path);
		$liSemaine = getNoSemaine($pdDate);

//		$req = "SELECT * FROM paa.rtvCurActivitesRessource('$psResActive', '$liSemaine', null, $pIncsactsys"
//				. ", $pIncactgen, $puniressconce, $piGroupeActi, null, true"
//				. ", 2174::integer, (1151+2^12)::integer)";
//		$req = "Select * 
//			From PAA.RtvCurActivitesRessource('$pRes'	/*pcLstRes*/
//						, '$liSemaine'					/*pcLstSem*/
//						, null							/*pbUsagerInclutActiGroupes*/
//						, $Incsactsys					/*pbInclActiSystème*/
//						, $Incactgen					/*pbInclActiGénériques*/
//						, $uniressconce					/*pbQuePourRessource*/
//						, $piGroupeActi					/*piGroupeActi*/
//						, null							/*tiEtablissement*/
//						, true							/*tlCalculeNbHeuresPosées*/
//						, 2174::integer					/*tiFormatLibelleCourt*/
//						, (1151+2^12)::integer			/*tiFormatLibelleLong*/
//						) ;
//			" ;
//		$function = $em->getConnection()->prepare($req);
//        $function->execute();
//        $curActivitesRessource = $function->fetchall();
// LG 20200513 old		$em = $this->getDoctrine()->getManager();
		$em = $this->getDoctrineManager();
		$repoActivités = $em->getRepository("PaaBundle:activites");
		$curActivitesRessource = $repoActivités->rtvCurActivitesRessource($pRes // $tcLstRes
				, $liSemaine	// $tcLstSemaines
				, 'null'	 // $tbUsagerInclutActiGroupes
				, $Incsactsys	// $tbInclActiSystème
				, $Incactgen	// $tbInclActiGénériques
				, $uniressconce	// $tbQuePourRessource
				, $piGroupeActi	// $tiGroupeActiBases
				, 'null'	 // $tiEtablissement
				, 'true'	 // $tbCalculeNbHeuresPosées
				, '2174::integer'   // $tiFormatLibelleCourt
				, '(1151+2^12)::integer' // $tiFormatLibelleLong
		);

		$opt['paActivitésRessource'] = $curActivitesRessource;
	}

	public function getIDActiviteDatatableAction(request $request, $pRes, $pDate, $pheuredeb
	, $pheurefin, $Incsactsys, $Incactgen, $uniressconce, $pgroupActi, $psStyleAB) {

		// Sauvegarde des paramètres
// LG 20200513 old		$loCnxn = $this->getDoctrine()->getConnection();
		$loCnxn = $this->getDoctrineConnection();
		$bdd = new cParametresBDD($loCnxn);
		$bdd->setParamètre("Incactgen", $Incactgen, 'LU');
		$bdd->setParamètre("Incsactsys", $Incsactsys, 'LU');
		$bdd->setParamètre("uniressconce", $uniressconce, 'LU');
		$bdd->setParamètre("pgroupActi", $pgroupActi, "NU");
		$bdd->setParamètre("pparam", $psStyleAB, "NU");

// LG 20190718 début
//		$path = dirname(__DIR__) . "/Component/Paa/Paa.php";
//		require_once($path);
//		$PnoSemaine = getNoSemaine($pDate);
//
//		$groupacti = ($pgroupActi=='null')?'null':(int)$pgroupActi;
//		$em = $this->getDoctrine()->getManager();
//		$req = "Select * 
//			From PAA.RtvCurActivitesRessource('$pRes'	/*pcLstRes*/
//						, '$PnoSemaine'					/*pcLstSem*/
//						, null							/*pbUsagerInclutActiGroupes*/
//						, $Incsactsys					/*pbInclActiSystème*/
//						, $Incactgen					/*pbInclActiGénériques*/
//						, $uniressconce					/*pbQuePourRessource*/
//						, $groupacti					/*piGroupeActi*/
//						, null							/*tiEtablissement*/
//						, true							/*tlCalculeNbHeuresPosées*/
//						, 2174::integer					/*tiFormatLibelleCourt*/
//						, (1151+2^12)::integer			/*tiFormatLibelleLong*/
//						) ;
//			" ;
//
//// echo $req ;
//// echo $err ;
//		$reqP = $em->getConnection()->prepare($req);
//		$reqP->execute();
//		$curActivitesRessource = $reqP->fetchall();
//		$opt = array('paActivitésRessource' => $curActivitesRessource
//						, 'psStyleAB' => $psStyleAB 
//						);
		$opt = array('psStyleAB' => $psStyleAB);
		$this->ComplèteTableauPourgetIDActivites_Datatable($opt, $pRes, $pDate
				, $Incsactsys, $Incactgen, $uniressconce, $pgroupActi);
// LG 20190718 fin


		return $this->render('@Paa/Dialog/getIDActivite/getIDActivites_Datatable.html.twig', $opt);
	}

	// Action de chargement d'un formulaire de getIdActivité
	// LG 20190328
	public function getIdActivitesFormAction(Request $request, $piId, $psOnglet) {
		$laOptions = array(
			'piIdActivite' => $piId,
			'psOngletActif' => $psOnglet,
		);
		return $this->render('@Paa/Dialog/getIDActivite/getIDActivites_SousOnglets.html.twig', $laOptions);
	}

	public function TestManyToManyAction() {

		// 260619 Yc TEste du Many TO Many Pour Chaque Ressource 

		$liActivité = 1433;
// LG 20200513 old		$em = $this->getDoctrine();
		$em = $this->getDoctrineManager();
		$activites = $em->getRepository(activites::class)->find($liActivité);
		echo("Activité N° " . $liActivité . "<br>");

//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//		$actiRealisations = $activites->getSéances();
		$seances = $activites->getSéances();
//		echo("nb séances ".count($actiRealisations) . "<br>");
		echo("nb séances " . count($seances) . "<br>");
//MG Modification 20200218 Fin

		$groupes = $activites->getgroupes();
		echo("nb groupes " . count($groupes) . "<br>");

		$intervenants = $activites->getintervenants();
		echo("nb intervenants " . count($intervenants) . "<br>");

		$usagers = $activites->getusagers();
		echo("nb usagers " . count($usagers) . "<br>");

		$salles = $activites->getsalles();
		echo("nb salles " . count($salles) . "<br>");

		return $this->render('@Paa/Essais/zZoneDeTestYc.html.twig');
	}

	public function SeanceAction(Request $request) {
		//  $poform =  $this->createForm('App\PaaBundle\Form\seanceType');
		$laOptions = array(
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//            'poEntité'=>new actiRealisations(),
			'poEntité' => new seances(),
//MG Modification 20200218 Fin
			'psOngletActif' => "OngletSeance",
			'psSSOngletActif' => "",
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//            'psNomTable'=>"actiRealisations",
			'psNomTable' => "seances",
//MG Modification 20200218 Fin
				//   'poform'=> $poform->createView()
		);
		//MG 20200113 Début Modification
//Ajout du parametre request pour fillOptionsPourRenderEnTeteMenu
//            $this->fillOptionsPourRenderEnTeteMenu($laOptions);
		$this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//MG 20200113 Fin Modification
		return $this->render('@Paa/form/seance.html.twig', $laOptions);
	}

}
