<?php

namespace App\PaaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController as BaseExceptionController;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Exception\FlattenException;
use App\PaaBundle\Component\Logs;
use Symfony\Bridge\Monolog\Logger;
use Twig\Environment;

// LG 20200513 old class ErrorController extends DefaultController {
class ErrorController extends BaseExceptionController {

    protected $debug;
    protected $twig;

    public function __construct(Environment $twig = null, bool $debug = null) {
//		$twig = $twig?:"" ;
		$debug = $debug?:false ;
        parent::__construct($twig, $debug);
    }

    //Fonction permettant de log dans la BDD un message donné en paramètre en créant une erreur
    //On peut l'appeler depuis n'importe quel controller : $this->get('logArthur')->errorAction('',null,null);
    function errorAction($errorMessage, $errorNiveau = null, $errorType = null) {
        if ($errorNiveau == null) {
            $errorNiveau = 1024;
        }
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            self::constructLog($errno, $errstr, $errfile, $errline);
        });
        if ($errorType != null) {
            trigger_error($errorMessage, $errorType);
        } else {
            trigger_error($errorMessage);
        }
        restore_error_handler();
    }

    //Fonction permettant de log dans la BDD un message donné en paramètre sans créer une erreur
    //On peut l'appeler depuis n'importe quel controller : $this->get('logArthur')->logAction('',null);
    function logAction($errorMessage, $errorNiveau = null) {
        if ($errorNiveau == null) {
            $errorNiveau = 1024;
        }
        self::constructLog($errorNiveau, $errorMessage, null, null);
    }

    //errno : niveau d'erreur
    //errstr : nom de l'erreur
    //errfile : fichier où l'erreur est apparue
    //errline : ligne du fichier où l'erreur est apparue
    function constructLog($errno, $errstr, $errfile, $errline) {
        $o = getORessources($a, $b);
// LG 20200513 old		$loConnexion = $this->getDoctrine()->getConnection();
        $loConnexion = $this->getDoctrineConnection();
        $user = $o->getUsersArthur($r, $loConnexion, $_SESSION['giUser']);
        ob_start();
        debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $data = ob_get_clean();
        $tabBacktrace = explode(']', $data);
        unset($tabBacktrace[0]);
        $tabFileLigne = explode(':', $tabBacktrace[1]);
        $data = implode(']', $tabBacktrace);
        $errfile = 'C:' . $tabFileLigne[1];
        $errline = $tabFileLigne[2];

        $content = $errstr . PHP_EOL
                . 'Niveau erreur : ' . $errno . ' : ' . $errstr . PHP_EOL
                . 'Programme concerné : ' . $errfile . ' Ligne N°' . $errline . '' . PHP_EOL . PHP_EOL
                . 'Liste des appels : ' . PHP_EOL;
        $content .= $data;
        $date = date("d/m/Y");
        $time = date("H:i:s");
        $content .= PHP_EOL . 'Le : ' . $date . ' à : ' . $time . PHP_EOL
                . 'PAA version ' . $_SESSION['gsVersionPAA'] . PHP_EOL . PHP_EOL
                . 'User : ' . $user[0]['username'] . PHP_EOL
                . 'URL demandée : ' . $_SESSION['_sf2_attributes']['URLDemandée'] . PHP_EOL
                . 'Système exploitation : ' . php_uname('s') . ' ' . php_uname('r') . ' : ' . php_uname('v') . PHP_EOL
                . 'Session windows : ' . php_uname('n') . PHP_EOL . PHP_EOL
                . 'Etat des paramètres :' . PHP_EOL . PHP_EOL;
        $n = 0;
        foreach (debug_backtrace() as $item) {
            if ($n != 0) {
                $content .= '#' . $n . ' ' . $item['class'] . $item['type'] . $item['function'] . ' called at [' . $item['file'] . ':' . $item['line'] . ']' . PHP_EOL
                        . implode(PHP_EOL, $item['args']) . PHP_EOL . PHP_EOL;
            }
            $n++;
        }
        // Décommenter pour ajouter la création d'un fichier de log en local
        //file_put_contents('testArthur.log', $content);
        $o->InsertLogArthur($r, $loConnexion, $errno, $errstr, $errfile, $_SESSION['gsVersionPAA'], $content);
    }

    function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null) {
//        $kernel = $this->container->get('kernel');
//        echo $kernel->getEnvironnement();
//        if ($kernel->getEnvironment() == 'prod') {
        if ($_ENV['APP_ENV'] === 'prod') {
            $id = $_ENV['lastErrorID'];
            $laOptions = array();
//            $templating = $this->container->get('templating');
            $ErrorCode = $exception->getStatusCode();
            $ErrorText = $exception->getMessage();

//            $this->fillOptionsPourRenderEnTeteMenu($laOptions, $request);
//            
//            return $this->render('@Paa/Default/error.html.twig', ['errorcode' => $ErrorCode, "errortext" => $ErrorText, 'laoptions' => $laOptions]);

            return new Response($this->twig->render(
                            '@Paa/Default/error.html.twig', [
                        'status_code' => $ErrorCode,
                        'status_text' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                        'exception' => $exception,
                        'logger' => $logger,
                        'currentContent' => $currentContent,
                        "errortext" => $ErrorText,
                        "id" => $id
                            ]
                    ), 200, ['Content-Type' => $request->getMimeType($request->getRequestFormat()) ?: 'text/html']);
        } else {
            return parent::showAction($request, $exception, $logger);
        }
    }

}
