<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IDActiviteTestTempPopUpType
 *
 * @author funydop
 */
class IDActiviteTestTempPopUpType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder->add('Date', DateType::class, array(
			'required' => false,
			'widget' => 'single_text',
			//'data' => date("Y-m-d",strtotime("2011-01-07"))
			'data' => new \DateTime('2018-01-01')
		))->add('HeureDeb', TimeType::class, array(
			'required' => false,
			'widget' => 'single_text',
			'data' => new \DateTime('5:20')
		))->add('HeureFin', TimeType::class, array(
			'required' => false,
			'widget' => 'single_text',
			'data' => new \DateTime('16:20')
		))->add('RessourceSelected', TextType::class, array(
			'data' => 'I1000204'
		))->add('ListeRessource', TextType::class, array(
			'data' => 'I1000204,I1000105,I1000057,I1000204'
		));
	}

	//put your code here
}
