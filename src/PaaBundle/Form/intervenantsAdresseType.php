<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

//MC 20200915 DEBUT 
//class intervenantsAdresseType extends AbstractType{
//class intervenantsAdresseType extends intervenantsSansProprietesType {
class intervenantsAdresseType extends AbaseType {
	protected $nomEntité = "intervenants" ;
//MC 20200915 FIN 
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		parent::buildForm($builder, $options);

		$builder->add('cadresse1', TextType::class, array(
					'required' => false,
				))
				->add('cadresse2', TextType::class, array(
					'required' => false,
				))
				->add('ccp', TextType::class, array(
					'required' => false,
				))
				->add('cville', TextType::class, array(
					'required' => false,
				))
				->add('ctel', TextType::class, array(
					'required' => false,
				))
				->add('cport', TextType::class, array(
					'required' => false,
				))
				->add('cmel', TextType::class, array(
					'required' => false,
				))
				->add('cnoss', TextType::class, array(
					'required' => false,
				))
				->add('dnaissance', DateTimeType::class, array(
					'widget' => 'single_text', //sert à avoir directement la date au format text dans un input
					'html5' => true, //à false on désactive la calendrier, par défaut on est à true
					'required' => false,
				))
				->add('ddate1', DateTimeType::class, array(
					'widget' => 'single_text',
					'html5' => true,
					'required' => false,
				))
				->add('ddate2', DateTimeType::class, array(
					'widget' => 'single_text',
					'html5' => true,
					'required' => false,
				))
				->add('mcommentaire', TextareaType::class, array(
					'required' => false,
				))
//                ->add('Enregistrer', SubmitType::class, array(
//                    'attr' => array('class' => 'cmdSave'),))
		;
	}

//    /**
//     * {@inheritdoc}
//     */
//    public function configureOptions(OptionsResolver $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => 'App\PaaBundle\Entity\intervenants'
//        ));
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function getBlockPrefix() {
//// LG 20190624 old        return 'fEntityForm_intervenantsAdresse';
//        return 'fEntityForm_intervenants';
//    }
}
