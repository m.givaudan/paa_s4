<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//class sallesType extends AbstractType {
class sallesType extends sallesSansProprietesType {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		parent::buildForm($builder, $options);

		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('icapacite', IntegerType::class, array(
					'required' => false,
				))
				->add('ipermis', EntityType::class, array(
					'class' => \App\PaaBundle\Entity\permis::class,
					'required' => false,
					'placeholder' => 'Aucun permis nécessaire'	// LG 20200904
					))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('ietablissement', EntityType::class, array(
					'class' => 'PaaBundle:etablissements'
					, 'choice_label' => 'cnom'))
				->add('cnoinventaire', TextType::class, array(
					'required' => false,
				))
//                ->add('Enregistrer', SubmitType::class, array(
//                    'attr' => array('class' => 'cmdSave'),
//				))
		;
	}

//
//    /**
//     * {@inheritdoc}
//     */
//    public function configureOptions(OptionsResolver $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => 'App\PaaBundle\Entity\salles'
//        ));
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function getBlockPrefix() {
//        return 'fEntityForm_salles';
//    }
}
