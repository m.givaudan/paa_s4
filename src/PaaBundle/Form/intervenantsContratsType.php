<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//MC 20200915 DEBUT
//class intervenantsContratsType extends AbstractType {
class intervenantsContratsType extends AbaseType {
     protected $nomEntité = "intervenantsContrats";
//MC 20200915 FIN 
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('ccontrat', TextType::class, array(
					'required' => false,
				))
				->add('ddebut', DateType::class, array(
					'widget' => 'single_text',
					'html5' => true,
					'required' => false,
				))
				->add('dfin', DateType::class, array(
					'widget' => 'single_text',
					'html5' => true,
					'required' => false,
				))
				->add('bnbhsem', IntegerType::class, array(
					'required' => false,
				))
				->add('bnbhmois', IntegerType::class, array(
					'required' => false,
				))
				->add('bnbhannee', IntegerType::class, array(
					'required' => false,
				))
				->add('ietablissement', EntityType::class, array(
					'class' => 'PaaBundle:etablissements',
					'choice_label' => 'cnom',
					'required' => false,
				))
				->add('ltempspartiel', CheckboxType::class, array(
					'required' => false,
				))
				->add('iintervenant', EntityType::class, array(
					'class' => 'PaaBundle:Intervenants',
					'choice_label' => 'cnom',
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\intervenantsContrats',
//			'aIntervenants' => null,
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_intervenantsContrats';
//	}
//MC 20200915 desac FIN 
}
