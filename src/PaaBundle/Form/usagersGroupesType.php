<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


//MC 20200915 DEBUT
//class usagersGroupesType extends AbstractType {
class usagersGroupesType extends AbaseType{
	
	//protected $data_class = "App\PaaBundle\Entity\usagersGroupes" ;
    protected $nomEntité = "usagersGroupes";
	
//MC 20200915 FIN
// LG 20200915 début
	private function getGroupes ($doctrine) {
		$laGroupesPrésents = $doctrine->getRepository("PaaBundle:groupes")->getAllGroupesParents() ;
		foreach ($laGroupesPrésents as $key => $value) {
			$Nom = $value->getCnom();
			$id = $value->getIidgroupe();
			$laGroupes[$Nom] = $id;
		}
		return $laGroupes ;
    }
// LG 20200915 fin

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder->add('ddebut', DateType::class, array(
                                        'widget' => 'single_text',
					'html5' => true,
					'required' => false,
				))
				->add('dfin', DateType::class, array(
                                        'widget' => 'single_text',
					'html5' => true,
					'required' => false,
				))
// LG 20200915 début
/*
				->add('igroupe', EntityType::class, array(
                                        'class' => 'PaaBundle:groupes',
                                        'choice_label' => 'cnom',
					'required' => true,
//                                    MC 20200915 DEBUT
//                                        'query_builder' => function (\App\PaaBundle\Repository\groupesRepository $er) {
//						return $er->getAllGroupesParents();
//                                        }
//                                    MC 20200915 FIN
'query_builder' => function (\App\PaaBundle\Repository\groupesRepository $er) {
						return $er->getAllGroupesParents();
                                        }					
					))
 */
				->add('igroupe', ChoiceType::class, array(
					'required' => true,
					'choices' => $this->getGroupes($options["doctrine"])
					))
// LG 20200915 fin
				->add('lparticipeauxseancesdugroupe', CheckboxType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}

//	/**
//	 * {@inheritdoc}
//	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		
//		parent::configureOptions($resolver) ;
//		
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\usagersGroupes'
//		));
//	}
//MC 20200915 desac DEBUT
	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_usagersGroupes';
//	}
//MC 20200915 desac FIN 
}
