<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//MC 20200915 DEBUT 
//class conventionsType extends AbstractType {
class conventionsType extends AbaseType {
protected $nomEntité = "conventions";

//MC 20200915 FIN
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('nhSem', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMaxSem', IntegerType::class, array(
					'required' => false,
				))
				->add('nnMaxCycl', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMoyMCy', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMaxJour', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMmaxJour', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMinSeq', IntegerType::class, array(
					'required' => false,
				))
				->add('nnMaxSeq', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMaxInterTp', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMinRepTp', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMminRepTp', IntegerType::class, array(
					'required' => false,
				))
				->add('nnMinJrsRep', IntegerType::class, array(
					'required' => false,
				))
				->add('nnMinJrsRepc', IntegerType::class, array(
					'required' => false,
				))
				->add('nnMinDimMoi', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMax2semRempl', IntegerType::class, array(
					'required' => false,
				))
				->add('nhdebnuit', IntegerType::class, array(
					'required' => false,
				))
				->add('nhfinnuit', IntegerType::class, array(
					'required' => false,
				))
				->add('lhMinRepTpType', CheckboxType::class, array(
					'required' => false,
				))
				->add('ndurnuit', IntegerType::class, array(
					'required' => false,
				))
				->add('mrhMin', TextareaType::class, array(
					'required' => false,
				))
				->add('idureepause', IntegerType::class, array(
					'required' => false,
				))
				->add('nhMaxSeq', IntegerType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\conventions'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_conventions';
//	}
//MC 20200915 desac FIN
}
