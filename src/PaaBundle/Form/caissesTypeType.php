<?php

//MC 20200903 
namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//MC 20200915 DEBUT
//class caissesTypeType extends AbstractType {
class caissesTypeType extends AbaseType {
    protected $nomEntité = "caissesType";
//	protected $nomTable = "caissestype";
//MC 20200915 FIN
    /**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
            $builder->add('cnom', TextType::class, array(
                            'required' => false,
			))
                    ->add('ctableusagers_caisses', TextType::class, array(
                        'required'=> false,
                    ))
                    ->add('cnomdft', TextType::class, array(
                        'required'=> false,
                    ))
                    ->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
                    ));
        }
//        MC 20200915 deac DEBUT
        /**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\caissesType'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_caissesType';
//	}
//        MC 20200915 deac FIN
}
