<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//LP 02/09/2020 debut
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use \App\PaaBundle\Component\Paa\Paa_Constantes;
//LP 02/09/2020 fin
//MC 20200915 DEBUT 
//class compteursType extends AbstractType {
class compteursType extends AbaseType {
    protected $nomEntité = "compteurs";

//MC 20200915 FIN 

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnomCompteur', TextType::class, array(
					'required' => false,
				))
				->add('mcommentaire', TextareaType::class, array(
					'required' => false,
				))
                        //LP 02/09/2020 debut
				->add('itype', ChoiceType::class, array(
					'choices' => array(
						'En Heures' => eiCompteurType_Heures,
						'En Points' => eiCompteurType_Points,
						'En Jours' => eiCompteurType_Jours,
					),
                                        'required' => false))
                        //LP 02/09/2020 fin
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\compteurs'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_compteurs';
//	}
//MC 20200915 desac FIN 
}
