<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//MC 20200915 DEBUT 
//class etablissementsType extends AbstractType {
class etablissementsType extends AbaseType {
protected $nomEntité = "etablissements";

//MC 20200915 FIN 
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('inbplaces', IntegerType::class, array(
					'required' => false,
				))
				->add('cnofiness', TextType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\etablissements'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_etablissements';
//	}
//MC 20200915 desac FIN 
}
