<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

//MC 20200915 DEBUT 
//class intervenantsType extends AbstractType {
//class intervenantsAdresseType extends intervenantsSansProprietesType {
class intervenantsType extends AbaseType {
	protected $nomEntité = "intervenants" ;
//MC 20200915 FIN 
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		parent::buildForm($builder, $options);

		$builder->add('cnom', TextType::class, array(
					'required' => true,
				))
				->add('cprenom', TextType::class, array(
					'required' => true,
				))
				->add('cnomcourt', TextType::class, array(
					'required' => false,
				))
				->add('cinitiales', TextType::class, array(
					'required' => false,
				))
				->add('lexterieur', CheckboxType::class, array(
					'required' => false,
				))
				->add('remplacant', CheckboxType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('ctel', TextType::class, array(
					'required' => false,
				))
				->add('cport', TextType::class, array(
					'required' => false,
				))
				->add('ispecialite', EntityType::class, array(
					'class' => 'PaaBundle:specialites',
					'choice_label' => 'cnom',
					'required' => false,
				))
				->add('iservice', EntityType::class, array(
					'class' => 'PaaBundle:services',
					'choice_label' => 'cnom',
					'required' => false,
				))
//                ->add('Enregistrer', SubmitType::class, array(
//                    'attr' => array('class' => 'cmdSave'),
//				))
		;
	}

//    /**
//     * {@inheritdoc}
//     */
//    public function configureOptions(OptionsResolver $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => 'App\PaaBundle\Entity\intervenants'
//        ));
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function getBlockPrefix() {
//        return 'fEntityForm_intervenants';
//    }
}
