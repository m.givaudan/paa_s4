<?php
//MC 20200904 

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use \App\PaaBundle\Component\Paa\Paa_Constantes;
//MC 20200915 DEBUT 
//class compteursReglesType extends AbstractType {
class compteursReglesType extends AbaseType {
    protected $nomEntité = "compteursRegles";

//MC 20200915 FIN 
    
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('iregle', ChoiceType::class, array(
                                    'choices' => array(
						'Heures travaillées' => eiCompteurRègle_Travail,
						'Présences' => eiCompteurRègle_Présence,
						'Présence de nuit' => eiCompteurRègle_WNuit,
						'Présence en jour férié' => eiCompteurRègle_WJourFérié,
						'Présence le dimanche' => eiCompteurRègle_WDimanche,
						'Absences' => eiCompteurRègle_Absence,
						'Contrat' => eiCompteurRègle_Contrat,
						'Activités' => eiCompteurRègle_Activité,
                                        //EN foxePro : "Repos inférieur à " + uNumVersDurée(m.loRecConvention.nH_min_rep_tp, .T.)
						'Repos inférieur au repos minimum' => eiCompteurRègle_DuréeMiniRepos,
                                        //EN foxePro : "Repos inférieur à " + uNumVersDurée(m.loRecConvention.nH_mmin_rep_tp, .T.)
						'Repos inférieur au repos minimum non dérogeable' => eiCompteurRègle_DuréeMiniReposNonDérogeable,						
					),
                                    'required' => false
				))
			->add('nsens', ChoiceType::class, array(
                                    'choices' => array(
                                        'Ajouter au compteur' => eiCompteurSens_Ajouter,
                                        'Retrancher au compteur' => eiCompteurSens_Retrancher,
                                    ),
                                    'required' => false,
				))
                        ->add('bvalheure', IntegerType::class, array(
					'required' => false,
				))
                        ->add('bvaljour', IntegerType::class, array(
					'required' => false,
				))
                        ->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\compteursRegles'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_compteursRegles';
//	}
//MC 20200915 desac FIN 

}
