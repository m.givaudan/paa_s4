<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
//MC 20200915 DEBUT 
//class specialitesType extends AbstractType {
class specialitesType extends AbaseType {
    protected $nomEntité = "specialites";

//MC 20200915 FIN 

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
                                ))
                        //LP 07/09/2020 debut
                                ->add('icouleur', IntegerType::class, array(
					'required' => false,
				));
                        //LP 07/09/2020 fin
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\specialites'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_specialites';
//	}
//MC 20200915 desac FIN 
}
