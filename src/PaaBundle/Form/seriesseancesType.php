<?php

namespace App\PaaBundle\Form;

use App\PaaBundle\Component\Paa\Paa_Constantes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class seriesseancesType extends AbaseType {
//MC 20200915 DEBUT 
    protected $nomEntité = "seriesseances";
//MC 20200915 FIN
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('ddebut', DateTimeType::class, array(
					'widget' => 'single_text',
					'html5' => true, //à false on désactive le calendrier, par défaut on est à true
				))
				->add('dfin', DateTimeType::class, array(
					'widget' => 'single_text',
					'html5' => true, //à false on désactive le calendrier, par défaut on est à true
				))
				->add('ifrequence', ChoiceType::class, array(
					'choices' => array(
						'Quotidienne' => Paa_Constantes::diFréquenceRépétitionSéance_Quotidien,
						'Tous les 2 jours' => Paa_Constantes::diFréquenceRépétitionSéance_TousLes2Jours,
						'Tous les 3 jours' => Paa_Constantes::diFréquenceRépétitionSéance_TousLes3Jours,
						'Une fois par semaine' => Paa_Constantes::diFréquenceRépétitionSéance_Hebdomadaire,
						'Une fois par quinzaine' => Paa_Constantes::diFréquenceRépétitionSéance_Quinzaine,
						'Les lundis et jeudis' => Paa_Constantes::diFréquenceRépétitionSéance_LundiJeudi,
						'Les mardis et vendredi' => Paa_Constantes::diFréquenceRépétitionSéance_MardiVendredi,
					),
					'required' => true,
					))
				->add('lexclutsamedi', CheckboxType::class, array(
					'required' => false,
				))
				->add('lexclutdimanche', CheckboxType::class, array(
					'required' => false,
				))
				->add('lexclutjoursferies', CheckboxType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\seriesseances'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_seriesseances';
//	}
//MC 20200915 desac FIN 
}
