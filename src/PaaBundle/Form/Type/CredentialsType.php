<?php

namespace App\PaaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CredentialsType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('login');
		$builder->add('password');
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'data_class' => 'App\PaaBundle\Entity\Credentials',
			'csrf_protection' => false
		]);
	}

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

