<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

function __construct() {
	
}

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\PaaBundle\Entity\usagers;

class IDActivitesType extends AbstractType {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder->add('Date', DateType::class, array(
			'required' => false,
			'widget' => 'single_text'
		))->add('Tdeb', TimeType::class, array(
			'required' => false,
			'widget' => 'single_text'
		))->add('Tfin', TimeType::class, array(
			'required' => false,
			'widget' => 'single_text'
		))->add('GroupeActiActi', EntityType::class, array(
			'class' => 'PaaBundle:actiGroupesacti',
			'choice_label' => 'cnom',
			'data' => $options['oGroupeSélectionné']
		))->add('StyleAffichageAB', ChoiceType::class, array(
			'choices' => array(// demmander les valeur au prof 
				'Activité de base (nom court)' => 1, // diTypeAffichageActivités_NomCourtActiBase
				'Activité de base (nom long) ' => 2, // diTypeAffichageActivités_NomActiBase
				'Sous-Groupe AB + Activité de base ' => 3, // diTypeAffichageActivités_NomCourtActiBase_NomSousGroupeActi
				'Groupe AB + Sous-Groupe AB + Activité de base' => 4, // diTypeAffichageActivités_NomCourtActiBase_NomSousGroupeActi_NomGroupeActi
			),
			'data' => (int) $options['pparam']
		))->add('intervenants', EntityType::class, array(
			'class' => 'PaaBundle:intervenants',
			'choice_label' => 'cnom'
		))->add('Ressource', EntityType::class, array(
			'class' => 'PaaBundle:usagers',
			'choice_label' => 'cnom'
		))->add('Groupes', EntityType::class, array(
			'class' => 'PaaBundle:groupes',
			'choice_label' => 'cnom'
		))->add('salles', EntityType::class, array(
			'class' => 'PaaBundle:salles',
			'choice_label' => 'cnom'
		));

		// Verification que la liste Existe Pour Crée la liste des ressource 
		if (isset($options['LstRes'])) {
			$builder->add('PlstRes', ChoiceType::class, array(
				'choices' => $options['LstRes'],
				'data' => $options['PnomResActiv'])
			);
		}

		$builder->add('IncActGener', CheckboxType::class, array(
			'required' => false,
			'data' => ($options['Incactgen'] == 'true' ? true : false)));

		$builder->add('IncLesActSyse', CheckboxType::class, array(
			'required' => false,
			'data' => ($options['Incsactsys'] == 'true' ? true : false)));
		$builder->add('UniPourLaRessConce', CheckboxType::class, array(
			'required' => false,
			'data' => ($options['uniressconce'] == 'true' ? true : false)));
		$builder->add('ApresInsertions', ChoiceType::class, array(
			'choices' => array(
				'Auncun' => 1,
				'Général' => 2,
				'Participants' => 3,
				'Série' => 4,
		)));
	}

	public function finishView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options) {
		// Pour ajouter un item qui n'est pas dans la table "Groupes d'activités"
		// Selon https://stackoverflow.com/questions/30013719/symfony2-form-type-entity-add-extra-option/30017673#30017673
		$newChoice = new ChoiceView(array(), 'null', "[Tous les groupes d'activité]");   // <- new option
//		$view->children['GroupeActiActi']->vars['choices'][] = $newChoice;					//<- adding the new option 
		// Ajouter la nouvelle option au début
		// Ca permet en plus de tricher : quand on force la valeur à "null", la combo n'est pas repositionnée
		// mais se replace sur le 1er item,il vaut donc mieux que ce soit justement celui-ci qui corresponde à la valeur "null"
		$view->children['GroupeActiActi']->vars['choices'] = Array($newChoice) + $view->children['GroupeActiActi']->vars['choices'];
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'App\PaaBundle\Entity\activites',
			'LstRes' => null,
			'PnomResActiv' => null,
			'Incsactsys' => null,
			'Incactgen' => null,
			'uniressconce' => null,
			'oGroupeSélectionné' => null,
			'pparam' => null
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'fEntityForm_activites';
	}

}
