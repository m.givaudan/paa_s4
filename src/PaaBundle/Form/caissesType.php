<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

//MC 20200915 DEBUT
//class caissesType extends AbstractType {
class caissesType extends AbaseType {
    protected $nomEntité = "caisses";
//MC 20200915 FIN
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('cadresse1', TextType::class, array(
					'required' => false,
				))
				->add('cadresse2', TextType::class, array(
					'required' => false,
				))
				->add('ccp', TextType::class, array(
					'required' => false,
				))
				->add('cville', TextType::class, array(
					'required' => false,
				))
				->add('ctel', TextType::class, array(
					'required' => false,
				))
                        //MC 20200902 DEBUT
				->add('itype', EntityType::class, array(
					'class' => 'PaaBundle:caissesType',
					'choice_label' => 'cnom',
					'required' => false,
				))
                        //MC 20200902 FIN
				->add('cemail', TextType::class, array(
					'required' => false,
				))
				->add('cfax', TextType::class, array(
					'required' => false,
				))
				->add('cweb', TextType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 deac DEBUT
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\caisses'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_caisses';
//	}

//MC 20200915 deac FIN
}
