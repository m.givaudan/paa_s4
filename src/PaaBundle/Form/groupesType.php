<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//MC 20200915 DEBUT 
//class groupesType extends AbstractType {
class groupesType extends AbaseType {
    protected $nomEntité = "groupes";
//MC 20200915 FIN
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

		parent::buildForm($builder, $options);

		$builder->add('cnom', TextType::class, array(
                    'required' => false,
                ))
                ->add('cnomcourt', TextType::class, array(
                    'required' => false,
                ))
                ->add('ieffectif', IntegerType::class, array(
                    'required' => false,
                ))
                ->add('lpresconf', CheckboxType::class, array(
                    'required' => false,
                ))
//                ->add('idebutrythme', IntegerType::class, array(
//                    'required' => false,
//                ))
                ->add('icouleur', IntegerType::class, array(
                    'required' => false,
                ))
                ->add('ietablissement', EntityType::class, array(
                    'class' => 'PaaBundle:etablissements',
                    'choice_label' => 'cnom'
                ))
                ->add('isallehabituelle', EntityType::class, array(
                    'class' => 'PaaBundle:salles',
                    'required' => false,
					'placeholder' => 'Salle habituelle de ce groupe'
                ))
                ->add('iservice', EntityType::class, array(
                    'class' => 'PaaBundle:services',
					'choice_label' => 'cnom',
                    'required' => false,
					'placeholder' => 'Service d\'appartenance de ce groupe' 
                ))
//                ->add('Enregistrer', SubmitType::class, array(
//                    'attr' => array('class' => 'cmdSave'),
//				))
				;
    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function configureOptions(OptionsResolver $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => 'App\PaaBundle\Entity\groupes'
//        ));
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function getBlockPrefix() {
//        return 'fEntityForm_groupes';
//    }
//
}
