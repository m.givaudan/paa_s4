<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//MC 20200915 DEBUT 
// class usersType extends AbstractType {
class usersType extends AbaseType {
	
//	protected $data_class = "App\PaaBundle\Entity\users" ;
        protected $nomEntité = "users";

//MC 20200915 FIN 

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

// LG 20200915 début
//		// Créer le tableau des intervenants
//		// En utilisant $laIntervenants, fourni dans $options
//		// (et qu'il FAUT SIGNALER dans $this->configureOptions)
//		$laIntervenants = array();
//		if (isset($options['aIntervenants'])) {
//			foreach ($options['aIntervenants'] as $key => $value) {
//				$Nom = $value->getCnomcourt();
//				$id = $value->getIidintervenant();
//				$laIntervenants[$Nom] = $id;
//			}
//		} else {
//			// $options['aIntervenants'] n'existe pas par exemple dans le cas de l'ajout d'un nouveau
//			// RAS dans ce cas
//		}
		// Créer le tableau des intervenants
		// En utilisant doctrine, fourni dans $options
		// (et qu'il FAUT SIGNALER dans $this->configureOptions)
		$laLstIntervenants = $options["doctrine"]
				->getRepository("PaaBundle:intervenants")
				->findAllPrésents();
		$laIntervenants = array();
		foreach ($laLstIntervenants as $key => $value) {
			$Nom = $value->getCnomcourt();
			$id = $value->getIidintervenant();
			$laIntervenants[$Nom] = $id;
		}
// LG 20200915 fin

		$builder
				->add('ctype_res', ChoiceType::class, array(
					'required' => false,
					'choices' => array(
						'Intervenants' => 'I',
						'Usagers' => 'U',
					),
				))
// LG 20190214 début
				->add('iid_res', ChoiceType::class, array(
					'required' => false,
					'choices' => $laIntervenants,
				))
				->add('username', TextType::class, array(
					'required' => false,
				))
				->add('email', EmailType::class, array(
					'required' => true,
				))
				->add('enabled', CheckboxType::class, array(
					'required' => false,
				))
				->add('lpeutchangerprofil', CheckboxType::class, array(
					'required' => false,
				))
				->add('password', PasswordType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
	
// LG 20200915 déac début
//	/**
//	 * {@inheritdoc}
//	 */
//	public function configureOptions(OptionsResolver $resolver) {
//// LG 20200915 début
////		$resolver->setDefaults(array(
////			'data_class' => 'App\PaaBundle\Entity\users',
////			'aIntervenants' => null,
////		));
//		parent::configureOptions($resolver) ;
//		$resolver->setDefaults(array(
//			'aIntervenants' => null,
//		));
//// LG 20200915 fin
//	}
// LG 20200915 déac fin
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_users';
//	}
//MC 20200915 desac FIN 
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

