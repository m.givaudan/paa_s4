<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

//MC 20200915 DEBUT 
//class anneesType extends AbstractType {
class anneesType extends AbaseType {

    protected $nomEntité = "annees";

//MC 20200915 FIN 

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('cnom', TextType::class, array(
                    'required' => false,
                ))
                ->add('ddebut', DateType::class, array(
                    'widget' => 'single_text', //sert à avoir directement la date au format text dans un input
                    'html5' => true, //à false on désactive la calendrier, par défaut on est à true
                    'required' => true,
                ))
                ->add('dfin', DateType::class, array(
                    'widget' => 'single_text',
                    'html5' => true,
                ))
                ->add('Enregistrer', SubmitType::class, array(
                    'attr' => array('class' => 'cmdSave'),
        ));
    }

//MC 20200915 desac DEBUT 
    /**
     * {@inheritdoc}
     */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\annees'
//		));
//	}

    /**
     * {@inheritdoc}
     */
//	public function getBlockPrefix() {
//		return 'fEntityForm_annees';
//	}
//MC 20200915 desac FIN 
}
