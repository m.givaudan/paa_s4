<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//MC 20200915 DEBUT 
//class servicesType extends AbstractType {
class servicesType extends AbaseType {
    protected $nomEntité = "services";
//MC 20200915 FIN
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('ietablissement', EntityType::class
						, array('class' => 'PaaBundle:etablissements'
					, 'choice_label' => 'cnom'
					, 'placeholder' => 'Sélectionnez un établissement'	// LG 20200904
							))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\services'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_services';
//	}
//MC 20200915 desac FIN 
}
