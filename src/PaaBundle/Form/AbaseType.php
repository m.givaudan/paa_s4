<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AbaseType extends AbstractType {

	protected $nomEntité = "";
	protected $nomTable = "";	// LG 20200915 : inutile sauf si différent de $nomEntité

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		// Tous les form ont un bouton 'Enregistrer'
		$builder->add('Enregistrer', SubmitType::class, array(
			'attr' => array('class' => 'cmdSave'),
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		if (!$this->nomEntité) {
			throw new \Exception('La propriété "nomEntité" de la classe ' . get_class($this) . " n'a pas été définie.");
		}
		$resolver->setDefaults(array(
					'data_class' => 'App\PaaBundle\Entity\\' . $this->nomEntité,
                    'doctrine' => null		// doctrine est fournie par le controleur 
                    ));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
// MC 20020915 old		return 'fEntityForm_' . $this->nomEntité;
// LG 20200915 olf		return 'fEntityForm_' . strtolower($this->nomEntité);
		return 'fEntityForm_' . ($this->nomTable?:$this->nomEntité);
//		return 'fEntityForm_' . $this->nomEntité;
	}

}
