<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//class usagersType extends AbstractType {
class usagersType extends usagersSansProprietesType {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('cprenom', TextType::class, array(
					'required' => false,
				))
				->add('cnomcourt', TextType::class, array(
					'required' => false,
				))
				->add('csexe', TextType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('ccivilite', ChoiceType::class, array(
					'required' => false,
					'choices' => array(
						'Monsieur' => 'Monsieur',
						'Madame' => 'Madame',
						'Mademoiselle' => 'Mademoiselle',
					),
				))
				->add('cnationalite', TextType::class, array(
					'required' => false,
				))
				->add('dnaissance', DateType::class, array(
					'widget' => 'single_text',
					'required' => false,
				))
				->add('clieunaissance', TextType::class, array(
					'required' => false,
				))
				->add('cnomjeunefille', TextType::class, array(
					'required' => false,
				))
				->add('dentree', DateType::class, array(
					'widget' => 'single_text',
					'required' => false,
				))
				->add('dsortie', DateType::class, array(
					'widget' => 'single_text',
					'required' => false,
				))
                        //MC 20200914 DEBUT
                                ->add('ireferent',EntityType::class,array(
                                        'class' => 'PaaBundle:intervenants',
					'choice_label' => 'cnom',
                                        'required' => false,
                                ))
                                ->add('ireferent2',EntityType::class,array(
                                                'class' => 'PaaBundle:intervenants',
					'choice_label' => 'cnom',
                                                'required' => false,
                                ))
                                ->add('ireferent3',EntityType::class,array(
                                    'class' => 'PaaBundle:intervenants',
					'choice_label' => 'cnom',
                                                'required' => false,
                                ))
                                ->add('ireferent4',EntityType::class,array(
                                    'class' => 'PaaBundle:intervenants',
					'choice_label' => 'cnom',
                                                'required' => false,
                                ))
                                ->add('ireferent5',EntityType::class,array(
                                    'class' => 'PaaBundle:intervenants',
					'choice_label' => 'cnom',
                                                'required' => false,
                                ))               
                                ->add('cchamplibre1', TextType::class, array(
					'required' => false,
				))
                        //MC 20200914 FIN
				->add('cchamplibre2', TextType::class, array(
					'required' => false,
				))
				->add('cchamplibre3', TextType::class, array(
					'required' => false,
				))
				->add('cchamplibre4', TextType::class, array(
					'required' => false,
				))
				->add('cchamplibre5', TextType::class, array(
					'required' => false,
				))
                        //MC 20200914 DEBUT
                                ->add('cchamplibre6', TextType::class, array(
					'required' => false,
				))->add('cchamplibre7', TextType::class, array(
					'required' => false,
				))->add('cchamplibre8', TextType::class, array(
					'required' => false,
				))->add('cchamplibre9', TextType::class, array(
					'required' => false,
				))->add('cchamplibre10', TextType::class, array(
					'required' => false,
				))
                        //MC 20200914 FIN
				->add('mcommentaire', TextareaType::class, array(
					'required' => false,
				))
// LG 20200513 : rétabli début : sinon le bouton n'apparait pas dans pageAvecGrille
				->add('Enregistrer', SubmitType::class, array(
                    'attr' => array('class' => 'cmdSave'),
				))
// LG 20200513 : rétabli fin
		;
	}

//
//    /**
//     * {@inheritdoc}
//     */
//    public function configureOptions(OptionsResolver $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => 'App\PaaBundle\Entity\usagers'
//        ));
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function getBlockPrefix() {
//        return 'fEntityForm_usagers';
//    }
}
