<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

/**
 * Classe de formulaire de saissie d'un user, Y COMPRIS la saisie des roles
 */
class usersPlusAdminType extends usersType {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);

		// Saisie des rôles
		// selon https://openclassrooms.com/forum/sujet/symfony2-ajouter-roles-depuis-le-form-register
		// http://symfony.com/doc/3.4/form/data_transformers.html
		$builder->add('roles', ChoiceType::class, array(
			'choices' => array(
				'Administrateur' => 'ROLE_ADMIN',
				'Concepteur' => 'ROLE_CONCEPTEUR',
				'Enseignant' => 'ROLE_ENSEIGNANT',
				'Etudiant' => 'ROLE_ETUDIANT',
			),
			'multiple' => true,
			'required' => false,
//							'property_path' => false,
//							'expanded' => false,
		));
// Si le ChoiceType ci-dessus est "multiple" => false
// il faut fournir à l'objet la capacité de se transformer de tableau (les données réelles) à string (nécessaires pour stocker une seule valeur)
//        $builder->get('roles')
//            ->addModelTransformer(new CallbackTransformer(
//                function ($rolesAsArray) {
//                    // transform the array to a string
//                    return implode(', ', $rolesAsArray);
//                },
//                function ($rolesAsString) {
//                    // transform the string back to an array
//                    return explode(', ', $rolesAsString);
//                }
//            )) ;
	}

}
