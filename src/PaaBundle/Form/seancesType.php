<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class seancesType extends seancesSansProprietesType {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('tdebut', DateTimeType::class, array(
					'widget' => 'single_text',
					'html5' => true, //à false on désactive le calendrier, par défaut on est à true
				))
				->add('tfin', DateTimeType::class, array(
					'widget' => 'single_text',
				))
				/* ->add('llblauto', CheckboxType::class, array(
				  'required' => false,
				  ))
				  ->add('mlibelle', TextareaType::class, array(
				  'required' => false,
				  )) */
				->add('mcommentaire', TextareaType::class, array(
					'required' => false,
				))
//MG ajout Début 20200128
				->add('linclutpause', CheckboxType::class, array(
					'required' => false,
				))
				->add('llibre1', CheckboxType::class, array(
					'required' => false,
				))
				->add('llibre2', CheckboxType::class, array(
					'required' => false,
				))
//MG ajout Début 20200128
				/*
				  //				->add('ltousplans', CheckboxType::class, array(
				  //                  'required' => false,
				  //                  ))
				  //                  ->add('tmaj', DateTimeType::class, array(
				  //                  'widget' => 'single_text',
				  //                  ))
				  //                  ->add('tconflit', DateTimeType::class, array(
				  //                  'widget' => 'single_text',
				  //                  ))
				  //                  ->add('linclutpause', CheckboxType::class, array(
				  //                  'required' => false,
				  //                  ))
				  //                  ->add('mlstdates', TextareaType::class, array(
				  //                  'required' => false,
				  //                  ))
				  //                  ->add('llibre1', CheckboxType::class, array(
				  //                  'required' => false,
				  //                  ))
				  //                  ->add('llibre2', CheckboxType::class, array(
				  //                  'required' => false,
				  //                  ))
				  //                  ->add('iacti', EntityType::class, array(
				  //                  'class' => 'PaaBundle:activites',
				  //                  'choice_label' => 'iidActivite'
				  //                  ))
				  //                  ->add('iserie', EntityType::class, array(
				  //                  'class' => 'PaaBundle:seriesseances',
				  //                  'choice_label' => 'iid_serie'
				  //                  ))
				 */
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\seances'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_seances';
//	}
//MC 20200915 desac FIN 
}
