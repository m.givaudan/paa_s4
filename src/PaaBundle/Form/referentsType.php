<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//MC 20200915 DEBUT 
//class referentsType extends AbstractType {
class referentsType extends AbaseType {

    protected $nomEntité = "referents";

//MC 20200915 FIN 
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('ctypelien', TextType::class, array(
					'required' => false,
				))
				->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('cprenom', TextType::class, array(
					'required' => false,
				))
				->add('cadresse', TextType::class, array(
					'required' => false,
				))
				->add('ccp', TextType::class, array(
					'required' => false,
				))
				->add('cville', TextType::class, array(
					'required' => false,
				))
				->add('cportable', TextType::class, array(
					'required' => false,
				))
				->add('cteldomicile', TextType::class, array(
					'required' => false,
				))
				->add('ctelbureau', TextType::class, array(
					'required' => false,
				))
				->add('cmel', TextType::class, array(
					'required' => false,
				))
				->add('ltuteur', CheckboxType::class, array(
					'required' => false,
				))
				->add('lautoriserecup', CheckboxType::class, array(
					'required' => false,
				))
				->add('lresidence', CheckboxType::class, array(
					'required' => false,
				))
				->add('lplacement', CheckboxType::class, array(
					'required' => false,
				))
				->add('ccivilite', TextType::class, array(
					'required' => false,
				))
				->add('ccommentaire', TextType::class, array(
					'required' => false,
				))
				->add('lsendbull', CheckboxType::class, array(
					'required' => false,
				))
				->add('lice', CheckboxType::class, array(
					'required' => false,
				))
				->add('cadresse2', TextType::class, array(
					'required' => false,
				))
				->add('lfactloyer', CheckboxType::class, array(
					'required' => false,
				))
				->add('ldotationglobale', CheckboxType::class, array(
					'required' => false,
				))
				->add('ctype', TextType::class, array(
					'required' => false,
				))
				->add('lfactprixjournee', CheckboxType::class, array(
					'required' => false,
				))
				->add('lfactreversion', CheckboxType::class, array(
					'required' => false,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\referents'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_referents';
//	}
//MC 20200915 desac FIN 
}
