<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

//MC 20200915 DEBUT
//class caissesPersonnesType extends AbstractType {
class caissesPersonnesType extends AbaseType {
    protected $nomEntité = "caissesPersonnes";
//MC 20200915 FIN
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder->add('ccivilite', TextType::class, array(
					'required' => false,
				))
				->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('cprenom', TextType::class, array(
					'required' => false,
				))
				->add('ctel', TextType::class, array(
					'required' => false,
				))
				->add('cport', TextType::class, array(
					'required' => false,
				))
				->add('cfax', TextType::class, array(
					'required' => false,
				))
				->add('cemail', TextType::class, array(
					'required' => false,
				))
				->add('mcommentaire', TextareaType::class, array(
					'required' => false,
				))
				->add('cfonction', TextType::class, array(
					'required' => false,
				))
// LG 20190214 test début
				->add('icaisse', EntityType::class, array(
					'class' => 'PaaBundle:Caisses',
					'choice_label' => 'cNomCourt',
					'required' => false,
				))
//				->add('icaisse_Integer', TextType::class, array(
//					'required' => false,
//				))
// LG 20190214 test fin
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 deac DEBUT
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\caissesPersonnes',
//			'aCaisses' => null,
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_caissesPersonnes';
//	}
//MC 20200915 deac FIN

}
