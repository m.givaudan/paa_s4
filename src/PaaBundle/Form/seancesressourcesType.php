<?php

namespace App\PaaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\PaaBundle\Entity\ressources;
use App\PaaBundle\Entity\seances;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class seancesressourcesType extends AbaseType {
//MC 20200915 DEBUT 
    protected $nomEntité = "seancesRessources";
//MC 20200915 FIN 
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
				->add('iid_res', EntityType::class, array(
					'required' => false,
					'class' => ressources::class,
				))
				->add('iseance', EntityType::class, array(
					'required' => false,
					'class' => seances::class,
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 20200915 desac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\seancesRessources'
//            'data_class' => 'App\PaaBundle\Entity\seances'
//            'data_class' => null
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_seancesressources';
//	}
//MC 20200915 desac FIN 
}
