<?php

namespace App\PaaBundle\Form\ActiBases;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

// LG 20200915 old class actiSsgroupesactiType extends AbstractType {
class actiSsgroupesactiType extends \App\PaaBundle\Form\ABaseType {

// LG 20200915 début
	protected $nomEntité = "actiSsgroupesacti";
	protected $nomTable = "actissgroupesacti";	// inutile sauf si différent de $nomEntité
// LG 20200915 fin

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('igroupe', EntityType::class
						, array('class' => 'PaaBundle:actiGroupesacti'
					, 'choice_label' => 'cnom'))
				->add('ldansbilan', CheckboxType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('ietablissement', EntityType::class, array(
					'class' => 'PaaBundle:etablissements',
					'choice_label' => 'cnom'
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}

// LG 20200915 déac début
//	/**
//	 * {@inheritdoc}
//	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\actiSsgroupesacti'
//		));
//	}
//
//	/**
//	 * {@inheritdoc}
//	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_actissgroupesacti';
//	}
// LG 20200915 déac fin

}
