<?php

namespace App\PaaBundle\Form\ActiBases;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class actibasesAbsenceAutreType extends actibasesBaseType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);
		$builder->add('npriorite', ChoiceType::class, array(
					'choices' => array(
						'Priorité haute' => '1',
						'Priorité moyenne' => '2',
						'Priorité basse' => '3',
					),
					'required' => false))
				->add('ietablissement', EntityType::class
						, array('class' => 'PaaBundle:etablissements'
					, 'choice_label' => 'cnom'))
				->add('ldansbilan', CheckboxType::class, array('required' => false))
				->add('lexclplanning', CheckboxType::class, array('required' => false));
	}

}
