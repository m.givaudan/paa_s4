<?php

namespace App\PaaBundle\Form\ActiBases;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class actibasesAbsenceIntervenantType extends actibasesBaseType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);
		$builder->add('iabsfix', IntegerType::class, array('required' => false))
				->add('iabsfixajout', IntegerType::class, array('required' => false))
				->add('ltravaillee', CheckboxType::class, array('required' => false))
				->add('iabstype', ChoiceType::class, array(
					'choices' => array(
						'Déterminée' => 1,
						'Fixée' => 2,
						'Proportion' => 3,
					),
					'choice_label' => function($value, $key, $index) {
						if ($value == 1) {
							return ' Déterminée par la taille de la séance';
						}
						if ($value == 2) {
							return ' Fixée';
						}
						if ($value == 3) {
							return ' Proportion du contrat hebdo.';
						}
					},
					'required' => false,
					'expanded' => true,
					'multiple' => false,
					'placeholder' => false,
		));
	}

}
