<?php

namespace App\PaaBundle\Form\ActiBases;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\PaaBundle\Component\Paa\Paa_Constantes;

class actibasesBaseType extends AbstractType {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		//$loEntity = $builder->getData();
		//$liIdActiBase = $loEntity->getIidactibase();
		//$loSousGroupe = $loEntity->getIsousgroupe();
		//$liIdSousGroupe = $loSousGroupe->getIidactisousgroupe();
		// $liIdSousGroupe = $options["piIdSousGroupe"] ;
		// Champs disponibles pour tous les types d'AB
		$builder
				->add('isousgroupe', EntityType::class
						, array('class' => 'PaaBundle:actiSsgroupesacti'
					, 'choice_label' => 'cnom'))
				->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('cnomcourt', TextType::class, array('required' => false))
				->add('ccode', TextType::class, array('required' => false))
				->add('icouleur', IntegerType::class, array('required' => false))
				->add('lsysteme', CheckboxType::class, array('required' => false))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'fEntityForm_actibases';
	}

}
