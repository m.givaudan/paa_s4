<?php

namespace App\PaaBundle\Form\ActiBases;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\PaaBundle\Component\Paa\Paa_Constantes;

class actibasesType extends actibasesBaseType {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);
		//$loEntity = $builder->getData();
		//$liIdActiBase = $loEntity->getIidactibase();
		//$loSousGroupe = $loEntity->getIsousgroupe();
		//$liIdSousGroupe = $loSousGroupe->getIidactisousgroupe();
		// $liIdSousGroupe = $options["piIdSousGroupe"] ;
		// Champs disponibles pour tous les types d'AB
		$builder
				->add('lconcinterv', CheckboxType::class, array('required' => false))
				->add('lconcusag', CheckboxType::class, array('required' => false))
				->add('lconcgroup', CheckboxType::class, array('required' => false))
				->add('lconcsalle', CheckboxType::class, array('required' => false))
				->add('lautoriseseancessimultanees', CheckboxType::class, array('required' => false))
				->add('npriorite', ChoiceType::class, array(
					'choices' => array(
						'Priorité haute' => '1',
						'Priorité moyenne' => '2',
						'Priorité basse' => '3',
					),
					'required' => false))
				->add('ietablissement', EntityType::class
						, array('class' => 'PaaBundle:etablissements'
					, 'choice_label' => 'cnom'))
				->add('ldansbilan', CheckboxType::class, array('required' => false))
				->add('lexclplanning', CheckboxType::class, array('required' => false))
				->add('linactive', CheckboxType::class, array('required' => false))
				->add('ltousintervenants', CheckboxType::class, array('required' => false))
				->add('inbusmax', IntegerType::class, array('required' => false))
				->add('ltoususagers', CheckboxType::class, array('required' => false))
				->add('ltousgroupes', CheckboxType::class, array('required' => false))
				->add('ltoutessalles', CheckboxType::class, array('required' => false));
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'App\PaaBundle\Entity\actibases'
		));
	}

}
