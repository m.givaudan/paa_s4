<?php

namespace App\PaaBundle\Form\ActiBases;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\PaaBundle\Form\AbaseType;

class actiGroupesactiType extends AbaseType {
	protected $nomEntité = "actiGroupesacti";
	protected $nomTable = "actigroupesacti";	// LG 20200915
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('cnom', TextType::class, array(
					'required' => false,
				))
				->add('nconcetab', IntegerType::class, array(
					'required' => false,
				))
				->add('nconcinterv', IntegerType::class, array(
					'required' => false,
				))
				->add('nconcusag', IntegerType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('nconcgroup', IntegerType::class, array(
					'required' => false,
				))
				->add('nconcsalle', IntegerType::class, array(
					'required' => false,
				))
				->add('ldansbilan', CheckboxType::class, array(
					'required' => false,
				))
				->add('icouleur', IntegerType::class, array(
					'required' => false,
				))
				->add('ietablissement', EntityType::class, array(
					'class' => 'PaaBundle:etablissements',
					'choice_label' => 'cnom'
				))
				->add('Enregistrer', SubmitType::class, array(
					'attr' => array('class' => 'cmdSave'),
		));
	}
//MC 2020915 deac DEBUT 
	/**
	 * {@inheritdoc}
	 */
//	public function configureOptions(OptionsResolver $resolver) {
//		$resolver->setDefaults(array(
//			'data_class' => 'App\PaaBundle\Entity\actiGroupesacti'
//		));
//	}

	/**
	 * {@inheritdoc}
	 */
//	public function getBlockPrefix() {
//		return 'fEntityForm_actigroupesacti';
//	}

//MC 2020915 deac FIN 
}
