<?php

namespace App\PaaBundle\Repository;

use \PDO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class seancesTradRepository extends EntityRepository {

	public function findSeancesParDate($ttdébut, $ttfin, $tclstseances = '', $tbforcemaj = 'false') {
		$sql = "SELECT * FROM paa.rtvcurseances_trad('" . $ttdébut . "','" . $ttfin . "', '" . $tclstseances . "'," . $tbforcemaj . ")";

		$em = $this->getEntityManager();
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$lstSéances = $req->fetchAll(PDO::FETCH_OBJ);
		return $lstSéances;
	}

//	Renvoyer un curseur contenant les ressources utilisées par une séance donnée
//			Avec informations complémentaires :
//			Liste de toutes les ressources
//			Compétence de la ressource pour cette activité de base
//			Disponibilité de la ressource pour cette séance
//			piId_Séance
//			psLstType_Res
//			(pbListeDesUsagersDuGroupe)	: True pour inclure les usagers du groupe en tant que participants
//			(cf champs Individuel et AvecGroupe)
//			(pbToutesRessources)			: True pour inclure dans le curseur les ressources ne participant pas
//			(piEvalueCompétence)			: False ou 0 	: sans évaluation de la compétence
//											: 1 		: évalue la compétence et place le résultat dans le curseur
//											: 2			: ne garde que les ressources compétentes
//			(piEvalueSiDisponible)			: False ou 0 	: sans évaluation de la disponibilité (absence de séance concurrente)
//											: 1 		: évalue la disponibilité sur toutes les ressources et place le résultat dans le curseur
//											: 2			: évalue la disponibilité uniquement pour les ressources concernées
//			(piId_Acti)					: pour ne pas avoir à la rechercher à partir de l'Id de séance
//			(ptDébutSéance)				: dans le cas de pbToutesRessources et piEvalueSiDisponible, pour ne pas avoir à la rechercher à partir de l'Id de séance pour filtrer les ressources sorties
//			(ptFinSéance)					: dans le cas de piEvalueSiDisponible, pour ne pas avoir à la rechercher à partir de l'Id de séance
//			(pbEvalueIncompatibilitésUsagers)	: True pour évaluer la compatibilité des usagers avec des usagers déja participants
//			(pbEvalueHeuresFaitesEtAFaireUsagers)		: True pour renseigner les heures prévues, prévues max, prévues min et heures faites (usagers uniquement)
//			(pcLstResFiltre)				: liste de ressources sur lesquelles évaluer les imposibilités
//			(pbEvalueUneSéanceParJourUsagers)
//			(pbEvalueHeuresFaitesEtAFaireGroupes)
//			(pbInclParticipationsAnnulées)	: True pour inclure les ressources dont la participation est annulée
//			(pbLblUsagerInclutNomGroupe)	: True pour faire apparaitre le nom du groupe dans le libellé des ressources de type Usager

	public function findRessourcesEligible($piid_séance
	, $pslsttype_res = ''
	, $pblistedesusagersdugroupe = 'false'
	, $pbtoutesressources = 'false'
	, $pievaluecompétence = 'null'
	, $pievaluesidisponible = 'null'
	, $piid_acti = 'null'
	, $piid_actibase = 'null'
	, $ptdébutséance = 'null'
	, $ptfinséance = 'null'
	, $pbevalueincompatibilitésusagers = 'false'
	, $pbevalueheuresfaitesetafaireusagers = 'false'
	, $pclstresfiltre = 'null'
	, $pbevalueuneséanceparjourusagers = 'false'
	, $pbevalueheuresfaitesetafairegroupes = 'false'
	, $pbinclparticipationsannulées = 'false'
	, $pblblusagerinclutnomgroupe = 'false'
	, $pvinfossuppl = 'null') {
		throw new Exception("Fonction obsolète");
		$sql = "SELECT * FROM paa.rtvcurressourcesseance_ttesinfos(" . $piid_séance
				. ", '" . $pslsttype_res . "'"
				. ", " . $pblistedesusagersdugroupe
				. ", " . $pbtoutesressources
				. ", " . $pievaluecompétence
				. "," . $pievaluesidisponible
				. ", " . $piid_acti
				. ", " . $piid_actibase
				. ", " . $ptdébutséance
				. ", " . $ptfinséance
				. "," . $pbevalueincompatibilitésusagers
				. ", " . $pbevalueheuresfaitesetafaireusagers
				. ", " . $pclstresfiltre
				. "," . $pbevalueuneséanceparjourusagers
				. ", " . $pbevalueheuresfaitesetafairegroupes
				. ", " . $pbinclparticipationsannulées
				. "," . $pblblusagerinclutnomgroupe
				. ", " . $pvinfossuppl
				. ")";
// echo $sql ;		
		$em = $this->getEntityManager();
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$lstRessources = $req->fetchAll(PDO::FETCH_OBJ);
		return $lstRessources;
	}

}
