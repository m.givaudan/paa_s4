<?php

namespace App\PaaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class anneesRepository extends EntityRepository {

// !!!!! Ne pas oublier l'annotation dans l'entité (\PaaBundle\Entity\intervenants.php)
// : * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\anneesRepository")     
	public function findListe() {
		$rsm = new ResultSetMapping;
		$rsm->addEntityResult('PaaBundle:annees', 'i');
		$rsm->addFieldResult('i', 'iid_annee', 'iidAnnee');
		$rsm->addFieldResult('i', 'cnom', 'cnom');
		$entityManager = $this->getEntityManager();
		$query = $entityManager->createNativeQuery("SELECT iid_annee, cnom FROM Paa.Annees i Order by 2;", $rsm);
		$rows = $query->getResult();

		return $rows;
	}

}
