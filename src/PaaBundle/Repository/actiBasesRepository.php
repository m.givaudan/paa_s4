<?php

namespace App\PaaBundle\Repository;

use \PDO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class actiBasesRepository extends PAABaseRepository {

	public function rtvCurCompetencesCommunes($psLst_Res, $pbSerializable) {
		
//MG Modification 20200806 Début
//Retourne pas le même objet que pour getRessources (voir intervenantsRepository)

		if (is_array($psLst_Res)) {
			$psLst_Res = implode(',', $psLst_Res);
		}
		if ($psLst_Res) {
			// La liste de ressources n'est pas vide : ne prendre que les activités de base pour lesquelles toutes sont compétentes
			$sql = "SELECT af.*, ab.cnom, ab.isousgroupe FROM paa.RtvCurCompetencesCommunes('" . $psLst_Res . "') af, paa.Actibases ab "
					. " WHERE af.iid_actibase = ab.iid_actibase "
					. " AND ab.isousgroupe > 0";
		} else {
			// La liste de ressources est vide : prendre toutes les activités de base
			$sql = "SELECT ab.iId_Actibase, ab.cnom, ab.isousgroupe FROM paa.Actibases ab "
					. "WHERE ab.isousgroupe > 0";
		}
		$sql .= " ORDER By ab.cNom";

		$em = $this->getEntityManager();
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$lstIdActibases = $req->fetchAll(PDO::FETCH_OBJ);
		
		//MG Modification 20200806 Début
//		return $lstIdActibases;
		//Dans le cas du rafraichissment des actibases dans la liste des activités
		if($pbSerializable){
			//Mise de l'objet sous forme de tableau pour pouvoir le serializer
			//un objet de type stdClass ne fonctionnant pas : https://github.com/FriendsOfSymfony/FOSRestBundle/issues/1016
			$arrayActibase = [];
			foreach ($lstIdActibases as $actibase){
				$object = [
					'iid_actibase' => $actibase->iid_actibase,
					//supprimer les espaces inutlie
					'cnom' => trim($actibase->cnom),
					'isousgroupe' =>$actibase->isousgroupe,
				];
				array_push($arrayActibase, $object);
			}
			return $arrayActibase;
		} else {
			return $lstIdActibases;
		}
		//MG Modification 20200806 Fin
	}
	
	//MG Ajout 20200824 Début
	public function checkValidité($loEntité){
			
		$update = ';';
		if ($loEntité->getId()){
			$update = ' AND actibases.iid_actibase != '.$loEntité->getId().';';
		}
		
		$psSQL = "SELECT COUNT(cnom) FROM paa.actibases WHERE upper(cnom) = upper('".$loEntité->getCnom()."') AND isousgroupe = ".$loEntité->getIsousgroupe()->getId();
		$psSQL2 = "SELECT COUNT(cnomcourt) FROM paa.actibases WHERE upper(cnomcourt) = upper('".$loEntité->getCnomcourt()."') AND isousgroupe = ".$loEntité->getIsousgroupe()->getId();
		$psSQL3 = "SELECT COUNT(ccode) FROM paa.actibases WHERE upper(ccode) = upper('".$loEntité->getCcode()."')";
		
		if($this->RtvSQLResult("{$psSQL}{$update}", 0)){
			$msgErreur = 'Ce nom existe déja pour ce sous-groupe.';
		} else if($this->RtvSQLResult("{$psSQL2}{$update}", 0)){
			$msgErreur = 'Ce nom court existe déja pour ce sous-groupe.'; 
		} else if($this->RtvSQLResult("{$psSQL3}{$update}", 0)){
		$msgErreur = 'Ce code existe déja pour ce sous-groupe.'; 
		} else {
			return false;
		}
		return $msgErreur;
	}
//MG Ajout 20200824 Fin
}