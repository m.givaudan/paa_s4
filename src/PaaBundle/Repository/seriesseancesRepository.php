<?php

namespace App\PaaBundle\Repository;

use \PDO;
use App\PaaBundle\Repository\PAABaseRepository;

class seriesseancesRepository extends PAABaseRepository{

//piSerieSeances (facult) : l'Id de la série
//piSeance (facult) : pour retrouver la série avec l'Id d'une des séances, si on ne connait pas l'id de série
//pdDebut (facult) : date de début souhaitée
//pdFin (facult) : date de fin souhaitée
//tlIgnoreLstRes (facult) : fournir true pour ne pas récupérer la liste des ressources de chaque séance (+ rapide)

	public function findSeriesSeance($piSerieSeances = 'null', $piSeance = 'null', $pdDebut = 'null', $pdFin = 'null', $tlIgnoreLstRes = 'true') {
		$sql = "SELECT * FROM paa.seriesseances_rtvcurserieseances(" . $piSerieSeances . ", " . $piSeance . ", " . $pdDebut . ", " . $pdFin . ",  " . $tlIgnoreLstRes . ")";
		$em = $this->getEntityManager();
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$lstSeries = $req->fetchAll(PDO::FETCH_OBJ);
		return $lstSeries;
	}
	
	//MG Ajout 20201805 Début
	public function getJoursFeries($dDate1, $dDate2){
		$sql = "Select * From paa.joursferies WHERE dDate >= '".$dDate1."' AND dDate <= '".$dDate2."'";
		$em = $this->getEntityManager();
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$lstJoursFeries = $req->fetchAll(PDO::FETCH_OBJ);
		return $lstJoursFeries;
	}
	//MG Ajout 20201805 Fin
	
	//MG 20201606 Début
//	Diffuser à toute une série les modifications faites sur une séance
	//	piSéanceModèle		: Id de la séance qui sert de modèle à la diffusion
	//	piSérie				: Id de la série à scinder
	//	(pdDate)				: Dft : toutes les dates
	//	(piPortée)				: 0 (Dft) = éclater la série (NON IMPLEMENTE)
	//							: 1 = scinder à partir de pdDate
	//							: 2 = modifier pour toutes les séances qui ont lieu le même jour de semaine que pdDate, >= pdDate
	//	(piTypeDépendance)		: fournir la valeur pour supprimer dans la série un type de dépendance
	public function serieseances_diffusesurserie($piséancemodèle, $pisérie, $pdDate, $piportée, $pitypedépendance){
		$em = $this->getEntityManager();
		
//MG Ajout 20203006 Début
//Traitement pour scinder la série en 2
		if(3 === $piportée){		
			
			$sql = "with cte as ( "
					. "INSERT INTO paa.seriesseances "
						. "(ddebut, dfin, ifrequence, lexclutsamedi, lexclutdimanche, lexclutjoursferies) "
						. "SELECT ddebut, dfin, ifrequence, lexclutsamedi, lexclutdimanche, lexclutjoursferies "
							. "FROM paa.seriesseances "
							. "WHERE iid_serie = 97 "
							. "RETURNING iid_serie "
					. "), "
				. "cte2 As (UPDATE paa.tseances "
				. "SET iSerie = (SELECT iid_serie FROM cte), "
				. "tMAJ = (SELECT paa.paadatetime()) "
				. "WHERE iSerie = ".$pisérie. " AND tDebut >= '".$pdDate."' "
				. "RETURNING iSerie) "
				. "select distinct iSerie from cte2 ;";

			$pisérie = $this->RtvSQLResult($sql);
			$piportée = 1;
		}
//MG Ajout 20203006 Fin
		$sql = "Select paa.serieseances_diffusesurserie(".$piséancemodèle.",".$pisérie.",'".$pdDate."',".$piportée.",".$pitypedépendance.");";
		
		
		$req = $em->getConnection()->prepare($sql);
		$valide = $req->execute();
		return $valide;
		
	}
	//MG 20201606 Fin

}
