<?php

// src/PaaBundle/Repository/ProductRepository.php

namespace App\PaaBundle\Repository;

use App\PaaBundle\Entity\activites;
use \PDO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class activitesRepository extends EntityRepository {

	// Renvoyer la liste des activités pour une ressource
	function rtvCurActivitesRessource($tcLstRes, $tcLstSemaines, $tbUsagerInclutActiGroupes
	, $tbInclActiSystème, $tbInclActiGénériques, $tbQuePourRessource, $tiGroupeActiBases
	, $tiEtablissement, $tbCalculeNbHeuresPosées
	, $tiFormatLibelleCourt, $tiFormatLibelleLong) {
		$em = $this->getEntityManager();
		if ($tiGroupeActiBases == 0) {
			$tiGroupeActiBases = 'null';
		}
		if ($tiGroupeActiBases != 'null') {
			$tiGroupeActiBases = (int) $tiGroupeActiBases;
		}
//var_dump($tiGroupeActiBases);
//echo $tata ;
		$sql = "SELECT Case When lestactibasesysteme Then 2 When lGenerique Then 1 Else 0 End iTri, * FROM paa.rtvCurActivitesRessource('$tcLstRes'"
				. ",'$tcLstSemaines'"
				. ",$tbUsagerInclutActiGroupes"
				. ",$tbInclActiSystème"
				. ",$tbInclActiGénériques"
				. ",$tbQuePourRessource"
				. ",$tiGroupeActiBases"
				. ",$tiEtablissement"
				. ",$tbCalculeNbHeuresPosées"
				. ",$tiFormatLibelleCourt"
				. ",$tiFormatLibelleLong"
				. ")"
				. "Order by 1 ;";
// echo $sql;
// echo $toto ;
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$laActivités = $req->fetchAll(PDO::FETCH_OBJ);
		return $laActivités;
	}
	
	public function rtvlibelleacti($piActi, $piFormat = 1151, $piTaillePolice = 8, $piSeance = null, $pddebutseances = null, $pdfinseances = null, $pslstacti_res_couleur = null){
		$sql = "SELECT * FROM paa.rtvlibelleacti(".$piActi.", ".$piFormat." )";
		
		$em = $this->getEntityManager();
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$libelleActi = $req->fetchAll(PDO::FETCH_OBJ);
		return $libelleActi;
	}

}
