<?php

namespace App\PaaBundle\Repository;

class sallesRepository extends PAABaseRepository {

//MG Ajout 20200824 Début
	public function checkValidité($poEntité){
			
		$update = ';';
		if ($poEntité->getId()){
			$update = ' AND salles.iid_salle != '.$poEntité->getId().';';
		}
		
		$lsSQL = "SELECT COUNT(cnom) FROM paa.salles WHERE upper(cnom) = upper('".$poEntité->getCnom()."')";
		if($this->RtvSQLResult("{$lsSQL}{$update}", 0)){
			$msgErreur = 'Ce nom est déjà utilisé par un autre équipement.';
		} else {
			return false;
		}
		return $msgErreur;
	}
//MG Ajout 20200824 Fin
}
