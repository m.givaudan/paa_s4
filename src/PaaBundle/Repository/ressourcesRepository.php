<?php

// src/PaaBundle/Repository/ProductRepository.php

namespace App\PaaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

$path = dirname(__DIR__) . "/Component/Paa/Paa.php";
require_once($path);

class ressourcesRepository extends EntityRepository {

	public function findAllAvecParametres($psParametres) {

		// Lecture et interprétation des paramètres
		$laParametres = array();
		parse_str($psParametres, $laParametres);
		$lsType_Res = isset($laParametres["cType_Res"]) ? $laParametres["cType_Res"] : "@Tous@";
		$lvDebut = isset($laParametres["vDebut"]) ? $laParametres["vDebut"] : -1;
		$lvFin = isset($laParametres["vFin"]) ? $laParametres["vFin"] : -1;
		$liActi = isset($laParametres["iActi"]) ? $laParametres["iActi"] : null;
		$liActiBase = isset($laParametres["iActiBase"]) ? $laParametres["iActiBase"] : null;

		return $this->findAllFiltrée($lsType_Res, $lvDebut, $lvFin, $liActiBase, $liActi);
	}

	// $psType_Res	: type de ressource à chercher (éventuellement '@Tous@')
	// $pvDebut		: date/heure/semaine de début de la période dans laquelle la ressource doit être présente
	// $pvFin		: date/heure/semaine de fin de la période dans laquelle la ressource doit être présente
	// $piActiBase	: pour ne prendre que les ressources compétentes pour cette activité de base
	// $piActi		: activité pour l'activité de base de laquelle la ressource doit être compétente
	public function findAllFiltrée($psType_Res, $pvDebut = null, $pvFin = null, $piActiBase = null, $piActi = null) {

		//$lsType_Res = $psType_Res ;
// $psType_Res = 'G' ;
		$liDebut = $pvDebut ? $pvDebut : -1;
		if (is_numeric($liDebut)) {
			($liDebut = intval($liDebut));
		} else if (isDate($liDebut)) {
			($liDebut = CTOD($liDebut));
		}
		$liFin = $pvFin ? $pvFin : -1;
		if (is_numeric($liFin)) {
			($liFin = intval($liFin));
		} else if (isDate($liFin)) {
			($liFin = CTOD($liFin));
		}

		// Création et paramétrage de l'objet chargé de la récupération des données
		$o = getORessources($a, $b);
		$o->cType_Res = $psType_Res;
		$o->iSem = $liDebut;
		$o->iSemFin = $liFin;
		$o->iActiBaseCompetence = $piActiBase ? $piActiBase : 'null';
		$o->iActiviteCompetence = $piActi ? $piActi : 'null';

		$lsSQL = $o->rtvSQLRessources("cNom, cNomCourt");
		$lsSQL = utf8_encode($lsSQL);
//		if ($pbAccepteSousGroupes) {
		if ($piActi) {
			$lsWhere = "";
		} else {
			$lsWhere = "WHERE (cType_Res != 'G' or Position('-' in cNom) = 0)"; // pour exclure les sous-groupes (bricolage : on devrait intervenir dans fonction RtvCurResources Postgres)
		}
		$lsSQL = str_replace("Order by", " As r $lsWhere Order by", $lsSQL);

		// Mapper le résultat vers une entité de type "Ressources"
		// !!!!! Ne pas oublier l'annotation dans l'entité (\PaaBundle\Entity\intervenants.php)
		// : * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\intervenantsRepository")     
		$rsm = new ResultSetMapping;
		$rsm->addEntityResult('PaaBundle:ressources', 'r');
		$rsm->addFieldResult('r', 'iid_res', 'iIdRes');
		$rsm->addFieldResult('r', 'ctype_res', 'cTypeRes');
		$rsm->addFieldResult('r', 'cnom', 'cNom');
		$rsm->addFieldResult('r', 'cnomcourt', 'cNomCourt');

		// Récupérer les données dans une collection de ressources
		$entityManager = $this->getEntityManager();
		$query = $entityManager->createNativeQuery($lsSQL, $rsm);
		$ressources = $query->getResult();

		return $ressources;
	}

	// Renvoyer la liste des ressources pour une activité
	function rtvCurRessourcesActivite($piActivité) {
		$lsSQL = "SELECT iid_res, ctype_res FROM paa.Acti_Ressources"
				. " Where iActi = $piActivité ;";
//        $em = $this->getEntityManager();
//		$req = $em->getConnection()->prepare($sql);
//      $req->execute();
		// Mapper le résultat vers une entité de type "Ressources"
		// !!!!! Ne pas oublier l'annotation dans l'entité (\PaaBundle\Entity\intervenants.php)
		// : * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\intervenantsRepository")     
		$rsm = new ResultSetMapping;
		$rsm->addEntityResult('PaaBundle:ressources', 'r');
		$rsm->addFieldResult('r', 'iid_res', 'iIdRes');
		$rsm->addFieldResult('r', 'ctype_res', 'cTypeRes');

		// Récupérer les données dans une collection de ressources
		$entityManager = $this->getEntityManager();
		$query = $entityManager->createNativeQuery($lsSQL, $rsm);
		$ressources = $query->getResult();

		return $ressources;
	}

	// ajout de Function FindAllList($liste) s'appuyer sur findAllFiltrée
	function FindAllList($lstRes) {

		//$lsType_Res = $psType_Res ;
// $psType_Res = 'G' ;
		// Création et paramétrage de l'objet chargé de la récupération des données
//		$o = getORessources($a, $b) ; // cltresforce ( Fournire liste ) 
//		$o->cLstResForcées = $lstRes ;
//		$lsSQL = $o->rtvcurlstressources($lstRes,'',1+4) ;
//             

		$function = $this->getEntityManager()->getConnection()->prepare("SELECT * FROM paa.rtvcurlstressources('$lstRes','',1+4)");
		$function->execute();
		$curRessource = $function->fetchall();

		return $curRessource;
	}

}
