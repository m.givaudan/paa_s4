<?php

namespace App\PaaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class groupesRepository extends PAABaseRepository {

//MG Ajout 20200824 Début
    public function checkValidité($loEntité) {

        $update = ';';
        if ($loEntité->getId()) {
            $update = ' AND groupes.iid_groupe != ' . $loEntité->getId() . ';';
        }

        $psSQL = "SELECT COUNT(cnom) FROM paa.groupes WHERE upper(cnom) = upper('" . $loEntité->getCnom() . "')";
        $psSQL2 = "SELECT COUNT(cnomcourt) FROM paa.groupes WHERE upper(cnomcourt) = upper('" . $loEntité->getCnomcourt() . "')";
        if ($this->RtvSQLResult("{$psSQL}{$update}", 0)) {
            $msgErreur = 'Ce nom est déjà utilisé par un groupe.';
        } else if ($this->RtvSQLResult("{$psSQL2}{$update}", 0)) {
            $msgErreur = 'Ce nom court est déjà utilisé par un groupe.';
        } else {
            return false;
        }
        return $msgErreur;
    }

//MG Ajout 20200824 Fin
//MC 20200914 DEBUT
    public function getAllGroupesParents() {

        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('PaaBundle:groupes', 'g');
        $rsm->addFieldResult('g', 'iid_groupe', 'iidGroupe');
        $rsm->addFieldResult('g', 'cnom', 'cnom');
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createNativeQuery("SELECT iid_groupe, cnom FROM Paa.groupes WHERE coalesce(igroupeparent,0) = 0  ORDER BY 2;", $rsm);
        $rows = $query->getResult();
        return $rows;
//                
//               $queryBuilder =  $this->getEntityManager()->createQueryBuilder();
//                $queryBuilder->select('iid_groupe, cnom')
//                   ->from('Groupes', 'u')
//                   ->where('coalesce(igroupeparent,0) = 0 ')
//                   ->orderBy('2', 'ASC');
//                
//		return $queryBuilder;
    }
//MC 20200914 FIN    
}
