<?php

namespace App\PaaBundle\Repository;

use App\PaaBundle\Entity\seancesRessources;
use \PDO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class seancesRessourcesRepository extends PAABaseRepository {

	// Permet de comparer les coches des seances que l'on veut enregistrer avec les coches qui sont déjà dans la bdd
	//psTypeRes : Le type de Ressource demandée ('I','U','G','E', 'C')
	//$pvIdItem : L'id de la séance concernée
	//			: ou de la ressource (Type+Id)
	//paNouvellesValeurs : Tableau contenant un tableau par checkbox cochée
	public function EnregistreParticipationAvecCoche($psTypeRes, $pvIdItem, $paNouvellesValeurs) {
//		var_dump($psTypeRes);
//		var_dump($pvIdItem);
// var_dump($paNouvellesValeurs) ;
		$lsType_Res = strtoupper($psTypeRes);
		$donneesBdd = $this->RtvCurRessourcesSeance_TtesInfos(
				$pvIdItem   // $piId_Seance
				, $psTypeRes  // $psLstType_Res = ''
				, true  // $pbListeDesUsagersDuGroupe = 'false'
				, true  // $pbToutesRessources = 'false'
				, 2  // $lcEvalueCompétence = 'null'
				, 1  // $piEvalueSiDisponible = 'null'
				, null	  // $piId_Acti = 'null'
				, null	  // $piId_ActiBase = 'null'
				, null	  // $ptDébutSéance = 'null'
				, null	  // $ptFinSéance = 'null'
				, null	  // $pbEvalueIncompatibilitésUsagers = 'false'
				, null	  // $pbEvalueHeuresFaitesEtAFaireUsagers = 'false'
				, null	  // $pcLstResFiltre = 'null'
				, null	  // $pbEvalueUneSéanceParJourUsagers = 'false'
				, null	  // $pbEvalueHeuresFaitesEtAFaireGroupes = 'false'
				, null	  // $pbInclParticipationsAnnulées = 'false'
				, null	  // $pbLblUsagerInclutNomGroupe = 'false'
				, null	  // $pvInfosSuppl = 'null'
				, true  // $pbInclInfosFlags
		);

		$lsReq = '';
		foreach ($donneesBdd as $val) {
			$lbCréeEnregistre = false;
			$lbSupprime = false;
// LG 20200508 début
//			$lsType_Res_Item = $lsType_Res;
			$lsType_Res_Item = $val['ctype_res'];
			if ($lsType_Res_Item != $lsType_Res) {
				// Cette ressource n'est pas du bon type : il faur revoir RtvCurRessourcesSeance_TtesInfos
				continue;
			}
// LG 20200508 fin
			$liId_Seance_Item = $val['iseance'];
			$liId_Res_Item = $val['iid_res'];
			$lsKey = 'Coche' . strtoupper($psTypeRes) . '_' . $val['iid_res'];
			$lParticipe = array_key_exists($lsKey, $paNouvellesValeurs);
			$lDansActi = $val['dansacti'];
// LG 20200508 début
//			$mCommentaire = $val['mcommentaire'];
			$lParticipe_BDD = $val['lparticipe'];
			$mCommentaire_BDD = $val['mcommentaire'];
			$mCommentaire = $paNouvellesValeurs['Commentaire' . strtoupper($psTypeRes) . '_' . $val['iid_res']];
			$lbAnnule = !$lParticipe;
// LG 20200508 fin
// LG 20200508 début
//			if ($lParticipe && $lDansActi) {
//				//Enregistrer les commentaires
//				$lbCréeEnregistre = true;
//			}
//
//			if ($lParticipe && !$lDansActi) {
//				//Créer ou enregistrer
//				$lbCréeEnregistre = true;
//			}
//
//			if (!$lParticipe && !$lDansActi) {
//				//Effacer la participation
//				$lbSupprime = true;
//			}
//
////			If IsNull(m.toRec.iid_seance_ressource)
////				&gsSetStepOn, "_3HL137CQB"		&& LG 20120426 
////			Endif
////			m.liSéanceRessourcePT = RtvSQLResult("Select Iseanceressourceplantype" ;
////								+ " From Séances_Ressources_Flags" ;
////								+ " Where Iseance_ressource = " + Transform(m.toRec.iid_seance_ressource) ;	&& psSQL
////							,  ;		&& piFld
////							,  ;		&& plFound
////							, 0 ;		&& pvDftValue
////							)
////			m.liSéanceRessourcePT = Nvl(m.liSéanceRessourcePT, 0)
////			If m.liSéanceRessourcePT = 0
////				* Cette séance ne provient pas des plans-types
////				* On peut la supprimer				
////				m.lbSupprime = .T.
////			Else
////				* On doit conserver la ligne
////				* pour que la recréation de semaine en tienne correctement compte
////				m.lbCréeEnregistre = .T.
////			Endif
//
//			if (!$lParticipe && $lDansActi) {
//				//Créer ou enregistrer
//				$lbCréeEnregistre = true;
//			}
//
////			if (array_key_exists($lsKey, $paNouvellesValeurs)) {
////				//si la case est cochée
////				if($val['lparticipe'] == true && $val['dansacti'] == true){
////					//Enregistrer les commentaires
////					$mCommentaire = $paNouvellesValeurs['Commentaire' . strtoupper($psTypeRes) . '_' . $val['iid_res']];
////					$enregistreSeance = true;
//////					echo $un;
////				} else if ($val['lparticipe'] == true && $val['dansacti'] == false){
////					//Créer ou enregistrer
////					$enregistreSeance = true;
//////					echo $deux;
////				}			
////			} else {
////				if($val['lparticipe'] == false && $val['dansacti'] == false){
////				//effacer la participation
////					$lsReq .= "DELETE FROM paa.seances_ressources"
////							. " WHERE ctype_res ='" . $lsType_Res_Item . "'"
////								. " AND iid_res = " . $liId_Res_Item . ""
////								. " AND iseance = " . $liId_Seance_Item . ";\n";
//////					echo $trois;
////				} else if ($val['lparticipe'] == false && $val['dansacti'] == true){
////					//Créer ou enregistrer
////					$enregistreSeance = true;
//////					echo $quatre;
////				}
////			}
			if ($lParticipe == $lParticipe_BDD && $mCommentaire == $mCommentaire_BDD) {
				// Aucun changement
			} else if ($lParticipe && $lDansActi) {
				// Enregistrer les commentaires
				$lbCréeEnregistre = true;
			} else if ($lParticipe && !$lDansActi) {
				// Créer ou enregistrer
				$lbCréeEnregistre = true;
			} else if (!$lParticipe && !$lDansActi) {
				// Effacer la participation
				if ($val["iseanceressourceplantype"]) {
					// Cette séance provient du plan-type
					// On doit conserver la ligne pour que la recréation de semaine en tienne correctement compte
					$lbCréeEnregistre = true;
				} else {
					// Cette séance ne provient pas des plans-types : on peut la supprimer
					$lbSupprime = true;
				}
			} else if (!$lParticipe && $lDansActi) {
				// Créer ou enregistrer
				$lbCréeEnregistre = true;
			}
// LG 20200508 fin

			if ($lbCréeEnregistre) {
// LG 20200508 début
//				$lsReq .= "INSERT INTO paa.seances_ressources (iseance, ctype_res, iid_res, lannule, mcommentaire)"
//						. " VALUES(" . $liId_Seance_Item . ", '" . $lsType_Res_Item . "', " . $liId_Res_Item . ", false, '" . $mCommentaire . "')"
//						. "ON CONFLICT (iseance, ctype_res, iid_res) DO UPDATE SET mcommentaire = '" . $mCommentaire . "';\n";
				$lsReq .= "INSERT INTO paa.seances_ressources (iseance, ctype_res, iid_res, lannule, mcommentaire)"
						. " VALUES (" . $liId_Seance_Item . ", '" . $lsType_Res_Item . "', " . $liId_Res_Item . ", " . ($lbAnnule ? "true" : "false") . ", '" . $mCommentaire . "')"
						. "ON CONFLICT (iseance, ctype_res, iid_res) DO UPDATE SET mcommentaire = '" . $mCommentaire . "';\n";
// LG 20200508 fin
			}

			if ($lbSupprime) {
				$lsReq .= "DELETE FROM paa.seances_ressources"
						. " WHERE ctype_res ='" . $lsType_Res_Item . "'"
						. " AND iid_res = " . $liId_Res_Item . ""
						. " AND iseance = " . $liId_Seance_Item . ";\n";
			}
		}
//var_dump($lsReq);
//echo $toto;
		if ($lsReq) {
			$entityManager = $this->getEntityManager();
			$entityManager->getConnection()->exec($lsReq);
		}
	}

	public function findAllPourSeance($psTypeRes, $piIdSeance) {
		$entityManager = $this->getEntityManager();
//        if ($psTypeRes == 'I') {
//            $nomIid = 'iId_Intervenant';
//            $nomTable = 'Intervenants';
//			$nomNom = "Evl(Res.cNomCourt, Coalesce(Res.cNom, '') || ' ' || Coalesce(Res.cPrenom, ''))" ;
//        } else if ($psTypeRes == 'G') {
//            $nomIid = 'iId_groupe';
//            $nomTable = 'groupes';
//			$nomNom = "Res.cNom" ;
//        } else if ($psTypeRes == 'S') {
//            $nomIid = 'iId_Salle';
//            $nomTable = 'Salles';
//			$nomNom = "Res.cNom" ;
//        } else if ($psTypeRes == 'U') {
//            $nomIid = 'iId_Usager';
//            $nomTable = 'usagers';
//			$nomNom = "Evl(Res.cNomCourt, Res.cNom || ' ' || Res.cPrenom)" ;
//        }
//        $reqsql = "select Res." . $nomIid . " iId_Res, " . $nomNom . " cNom"
//						. ", S.iid_seance"
//						. ", SR.lannule"
//				. " FROM PAA.seances_ressources SR"
//				. " Right Join PAA.tseances S"
//					. " On S.iId_seance = " . $piIdSeance
//				. " Right Join PAA." . $nomTable . " Res"
//					. " On SR.ctype_res = '" . $psTypeRes . "'"
//						. " And S.iid_seance = " . $piIdSeance . ""
//						. " And Res." . $nomIid . " = SR.iId_Res";
		/* LG 20200427 début
		  $reqsql = "SELECT * FROM paa.rtvcurressourcesseance_ttesinfos(".$piIdSeance.")" ;
		  $function = $entityManager->getConnection()->prepare($reqsql);
		  $function->execute();
		  $allRessourceSeance = $function->fetchAll(PDO::FETCH_ASSOC);
		  return $allRessourceSeance;
		 */
		return $this->RtvCurRessourcesSeance_TtesInfos($piIdSeance   // $piId_Seance
						, $psTypeRes   // $psLstType_Res = ''
//                                                ,      // $pbListeDesUsagersDuGroupe = 'false'
//                                                ,      // $pbToutesRessources = 'false'
//                                                ,      // $lcEvalueCompétence = 'null'
//                                                ,      // $piEvalueSiDisponible = 'null'
//                                                ,      // $piId_Acti = 'null'
//                                                ,      // $piId_ActiBase = 'null'
//                                                ,      // $ptDébutSéance = 'null'
//                                                , $ptFinSéance = 'null'
//                                                , $pbEvalueIncompatibilitésUsagers = 'false'
//                                                , $pbEvalueHeuresFaitesEtAFaireUsagers = 'false'
//                                                , $pcLstResFiltre = 'null'
//                                                , $pbEvalueUneSéanceParJourUsagers = 'false'
//                                                , $pbEvalueHeuresFaitesEtAFaireGroupes = 'false'
//                                                , $pbInclParticipationsAnnulées = 'false'
//                                                , $pbLblUsagerInclutNomGroupe = 'false'
//                                                , $pvInfosSuppl = 'null'
		);
// LG 20200427 fin
	}

	/** ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
	 * Renvoyer un curseur contenant les ressources utilisées par une séance donnée
	 * Avec informations complémentaires :
	 * Liste de toutes les ressources
	 * Compétence de la ressource pour cette activité de base
	 * Disponibilité de la ressource pour cette séance
	 * (@tcCurseur)					: pour retour du curseur créé
	 * piId_Séance
	 * psLstType_Res
	 * (pbListeDesUsagersDuGroupe)	: .T. pour inclure les usagers du groupe en tant que participants
	 * 		(cf champs Individuel et AvecGroupe)
	 * (pbToutesRessources)			: .T. pour inclure dans le curseur les ressources ne participant pas
	 * (piEvalueCompétence)			: .F. ou 0 	: sans évaluation de la compétence
	 * 								: 1 		: évalue la compétence et place le résultat dans le curseur
	 * 								: 2			: ne garde que les ressources compétentes
	 * 								: "<IdRes1>,<IdRes2>,.." 	: ne garde que les ressources compétentes, PLUS les ressources fournies en paramètre (PG uniquement)
	 * (piEvalueSiDisponible)			: .F. ou 0 	: sans évaluation de la disponibilité (absence de séance concurrente)
	 * 								: 1 		: évalue la disponibilité sur toutes les ressources et place le résultat dans le curseur
	 * 								: 2			: évalue la disponibilité uniquement pour les ressources concernées
	 * (piId_Acti)					: pour ne pas avoir à la rechercher à partir de l'Id de séance
	 * (ptDébutSéance)				: dans le cas de pbToutesRessources et piEvalueSiDisponible, pour ne pas avoir à la rechercher à partir de l'Id de séance pour filtrer les ressources sorties
	 * (ptFinSéance)					: dans le cas de piEvalueSiDisponible, pour ne pas avoir à la rechercher à partir de l'Id de séance
	 * (pbEvalueIncompatibilitésUsagers)	: .T. pour évaluer la compatibilité des usagers avec des usagers déja participants
	 * (pbEvalueHeuresFaitesEtAFaireUsagers)		: .T. pour renseigner les heures prévues, prévues max, prévues min et heures faites (usagers uniquement)
	 * (pcLstResFiltre)				: liste de ressources sur lesquelles évaluer les imposibilités
	 * (pbEvalueUneSéanceParJourUsagers)
	 * (pbEvalueHeuresFaitesEtAFaireGroupes)
	 * (pbInclParticipationsAnnulées)	: .T. pour inclure les ressources dont la participation est annulée
	 * (pbLblUsagerInclutNomGroupe)	: .T. pour faire apparaitre le nom du groupe dans le libellé des ressources de type Usager
	 * (pvInfosSuppl)					: Doit-on évaluer les séances antérieures ? (PG uniquement)
	 * 								: e.g. '[{"lInclDateDerniereSeanceU": true}, ...]' pour inclure la dernière date de séances antérieures de cette AB ?
	 * $pbInclInfosFlags			: .T. pour faire la jointure avec Séances_Ressources_Flags
	 * Liste des champs : "DansActi L" ;
	  + ", id_Séance N(10, 0)" ;
	  + ", Type_Res C(1)" ;
	  + ", id_Res N(10, 0)" ;
	  + ", Nom C(50)" ;
	  + ", Individuel L Null" ;
	  + ", AvecGroupe L Null" ;
	  + ", Commentaire C(254)" ;
	  + ", lAbsent L" ;		&& Toujours .F. : les absents (hors établissement) ne sont pas dans le curseur
	  + ", lParticipe L" ;
	  + ", lAnnule L"	;		&& .T. en cas d'annulation de la participation du fait de participation à l'acti ou d'un usager car son groupe participe
	  + ", lCompétent L Null" ;
	  + ", lIndisponible L Null Default Null" ;
	  + ", lCoche L" ;
	  + ", mLstUsagersIncompatibles M" ;
	  + ", lPlaceAuto L" ;
	  + ", Id_Séance_Ressource I"
	  LG 20200428
	 * 
	 */
	function RtvCurRessourcesSeance_TtesInfos($piId_Seance
	, $psLstType_Res = ''
	, $pbListeDesUsagersDuGroupe = false
	, $pbToutesRessources = false
	, $lcEvalueCompétence = null
	, $piEvalueSiDisponible = null
	, $piId_Acti = null
	, $piId_ActiBase = null
	, $ptDébutSéance = null
	, $ptFinSéance = null
	, $pbEvalueIncompatibilitésUsagers = false
	, $pbEvalueHeuresFaitesEtAFaireUsagers = false
	, $pcLstResFiltre = null
	, $pbEvalueUneSéanceParJourUsagers = false
	, $pbEvalueHeuresFaitesEtAFaireGroupes = false
	, $pbInclParticipationsAnnulées = false
	, $pbLblUsagerInclutNomGroupe = false
	, $pvInfosSuppl = null
	, $pbInclInfosFlags = null) {
		$sql = "Select * From PAA.RtvCurRessourcesSeance_TtesInfos("
				. $piId_Seance				 // piId_Séance
				. ", " . PG_ToLitteral(Evl($psLstType_Res, ''), 'Text')	  // psLstType_Res
				. ", " . PG_ToLitteral($pbListeDesUsagersDuGroupe, 'Boolean')	  // pbListeDesUsagersDuGroupe
				. ", " . PG_ToLitteral($pbToutesRessources, 'Boolean')	   // pbToutesRessources
				. ", " . PG_ToLitteral($lcEvalueCompétence, 'Text')	   // piEvalueCompétence
				. ", " . PG_ToLitteral(Evl($piEvalueSiDisponible, 0), 'Integer')   // piEvalueSiDisponible
				. ", " . PG_ToLitteral(Evl($piId_Acti, Null), 'Integer')	 // piId_Acti
				. ", " . PG_ToLitteral(Evl($piId_ActiBase, Null), 'Integer')	// piId_ActiBase
				. ", " . PG_ToLitteral(Evl($ptDébutSéance, Null), 'TimeStamp')	 // ptDébutSéance
				. ", " . PG_ToLitteral(Evl($ptFinSéance, Null), 'TimeStamp')	// ptFinSéance
				. ", " . PG_ToLitteral($pbEvalueIncompatibilitésUsagers, 'Boolean')   // pbEvalueIncompatibilitésUsagers
				. ", " . PG_ToLitteral($pbEvalueHeuresFaitesEtAFaireUsagers, 'Boolean')  // pbEvalueHeuresFaitesEtAFaireUsagers
				. ", " . PG_ToLitteral(Evl($pcLstResFiltre, ''), 'Text')	 // pcLstResFiltre
				. ", " . PG_ToLitteral($pbEvalueUneSéanceParJourUsagers, 'Boolean')   // pbEvalueUneSéanceParJourUsagers
				. ", " . PG_ToLitteral($pbEvalueHeuresFaitesEtAFaireGroupes, 'Boolean')  // pbEvalueHeuresFaitesEtAFaireGroupes
				. ", " . PG_ToLitteral($pbInclParticipationsAnnulées, 'Boolean')   // pbInclParticipationsAnnulées
				. ", " . PG_ToLitteral($pbLblUsagerInclutNomGroupe, 'Boolean')	 // plLblUsagerInclutNomGroupe
				. ", " . PG_ToLitteral($pvInfosSuppl, 'Text')		  // pvEvalueSéancesAntérieures
				. ") ";
// LG 20200508 début
		if ($pbInclInfosFlags) {
			// Ajouter la jointure sur la table Séances_Ressources_Flags
			$sql = "Select * From (" . $sql . ") SR"
					. " Left Join Paa.Seances_Ressources_Flags SRF"
					. " On SRF.iSeance_Ressource = SR. iId_Seance_Ressource";
		}
// LG 20200508 début
//echo $sql ;
//echo $toto ;
		$em = $this->getEntityManager();
		$reqPrep = $em->getConnection()->prepare($sql);
		$reqPrep->execute();
		$res = $reqPrep->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}

//	public function test(){
//		if(){
//			$lbCréeEnregistre = true;
//		}
//		Case Nvl(m.toRec.lParticipe, .F.) .and. Nvl(m.toRec.DansActi, .F.)
//			* Enregistrer les commentaires
//			m.lbCréeEnregistre = .T.
//			
//		Case Nvl(m.toRec.lParticipe, .F.) .and. .not. Nvl(m.toRec.DansActi, .F.)
//			* Créer ou enregistrer
//			m.lbCréeEnregistre = .T.
//			
//		Case .not. Nvl(m.toRec.lParticipe, .F.) .and. .not. Nvl(m.toRec.DansActi, .F.)
//			* Effacer la participation
//			If IsNull(m.toRec.iid_seance_ressource)
//				&gsSetStepOn, "_3HL137CQB"		&& LG 20120426 
//			Endif
//	}
}
