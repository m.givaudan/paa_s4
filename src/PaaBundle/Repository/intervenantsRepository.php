<?php

namespace App\PaaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use App\PaaBundle\Entity\intervenants;

//MG Modification 20200821 Début
//EntityRepository est appelé par PAABaseRepository
//class intervenantsRepository extends EntityRepository {
class intervenantsRepository extends PAABaseRepository {
//MG Modification 20200821 Fin

	public function findAllPrésents($pvDebut = null, $pvFin = null) {
// https://symfony.com/doc/3.4/doctrine/repository.html
// https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/reference/native-sql.html
// !!!!! Ne pas oublier l'annotation dans l'entité (\PaaBundle\Entity\intervenants.php)
// : * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\intervenantsRepository")     
		$rsm = new ResultSetMapping;
		$rsm->addEntityResult('PaaBundle:intervenants', 'i');
		$rsm->addFieldResult('i', 'iid_intervenant', 'iidIntervenant');
		$rsm->addFieldResult('i', 'cnomcourt', 'cnomcourt');
		$entityManager = $this->getEntityManager();
		$query = $entityManager->createNativeQuery("SELECT iid_intervenant, cnomcourt FROM Paa.Intervenants i Order by 2;", $rsm);
//		$query = $entityManager->createNativeQuery("SELECT iId_Intervenant, cNomCourt FROM Paa.Intervenants i Where cNomCourt Like '%a%';", $rsm);
//		$query->setParameter(1, 'a');

		$intervenants = $query->getResult();

		return $intervenants;
	}

//MG Ajout 20200821 Début
	public function checkValidité($loEntité){
		
		$update = ';';
		if ($loEntité->getId()){
			$update = ' AND intervenants.iid_intervenant != '.$loEntité->getId().';';
		}
		
		$psSQL = "SELECT COUNT(cinitiales) FROM paa.intervenants WHERE upper(cinitiales) = upper('".$loEntité->getCinitiales()."')";
		$psSQL2 = "SELECT COUNT(cnomcourt) FROM paa.intervenants WHERE upper(cnomcourt) = upper('".$loEntité->getCnomcourt()."')";
		$psSQL3 = "SELECT COUNT(cnom) FROM paa.intervenants WHERE upper(cnom|| ' '||cprenom) = upper('".$loEntité->getCnom()." ".$loEntité->getCprenom()."')";
		
		if($this->RtvSQLResult("{$psSQL}{$update}", 0)){
			$msgErreur = 'Ces initiales sont déjà utilisées par un autre intervenant';
		} else if ($this->RtvSQLResult("{$psSQL2}{$update}", 0)){
			$msgErreur = 'Ce nom court est déjà utilisé par un autre intervenant';
		} else if ($this->RtvSQLResult("{$psSQL3}{$update}", 0)){
			$msgErreur = 'Un intervenant possédant le même nom et prénom existe déjà';
		} else {
			return false;
		}
		return $msgErreur;
	}
//MG Ajout 20200821 Fin
}
