<?php

namespace App\PaaBundle\Repository;

$lsFich = dirname(dirname(__FILE__)) . '/Component/Paa/Paa.php';
require_once $lsFich;

//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//use App\PaaBundle\Entity\actiRealisations;
//MG Modification 20200218 Fin
use App\PaaBundle\Entity\seances;
use \PDO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

//class actiRealisationsRepository extends EntityRepository {
class seancesRepository extends PAABaseRepository {

	protected $oParamètresBDD = null;

	function setoParamètresBDD($poParamètres) {
		$this->oParamètresBDD = $poParamètres;
	}

	// **************************************************************************************************************
	// Renvoyer le texte des informations sur les séances concurrentes pour les ressources fournies, sur le créneau fourni
	// Valeur de retour : true si succès
	function RtvMsgInfosSeancesConcurrentes(&$pcMsg
	, $ptDébut
	, $ptFin
	, $pcLstResAChecker
	, $piId_SéanceAExclure = null
	, $psLstResDeLaSéanceAExclure = null
	, $piActiDeLaSéanceAExclure = null
	, $pbExclutAbsences = null
	) {
		$lsDébut = "'" . $ptDébut->format('Y/m/d H:i:s') . "'::timestamp";
		$lsFin = "'" . $ptFin->format('Y/m/d H:i:s') . "'::timestamp";
		$lsId_SéanceAExclure = ($piId_SéanceAExclure == null) ? 'null' : $piId_SéanceAExclure;
		$lsLstResDeLaSéanceAExclure = ($psLstResDeLaSéanceAExclure == null) ? 'null' : ("'" . $psLstResDeLaSéanceAExclure . "'");
		$liActiDeLaSéanceAExclure = ($piActiDeLaSéanceAExclure == null) ? 'null' : $piActiDeLaSéanceAExclure;
		$lbExclutAbsences = ($pbExclutAbsences == null) ? 'null' : ($pbExclutAbsences ? 'true' : 'false');
		$lsSQL = <<<SQL
			Select cLstLibellesSéancesConcurrentes 
				From Paa.RtvMsgInfosSeancesConcurrentes($lsDébut
														, $lsFin
														, '$pcLstResAChecker'
														, $lsId_SéanceAExclure
														, $lsLstResDeLaSéanceAExclure
														, $liActiDeLaSéanceAExclure
														, $lbExclutAbsences
														) ;
SQL;
		$pcMsg = $this->RtvSQLResult($lsSQL  //  psSQL 
				, "" //  pvDftValue 
				);
// $pcMsg = $lsSQL ;
		// Valeur de retour
		Return true;
	}

	// **************************************************************************************************************
	// Renvoyer la liste des participants d'une séance
	// &psLstRes			: pour retour de la liste des ressources
	// poSéance			: entité de la séance
	// Renvoie true si succès
	function RtvLstRessourcesSéance($poSéance, &$psLstRes) {
		$liId_Séance = $poSéance->getId() ? $poSéance->getId() : "null";
		$liId_Acti = $poSéance->getActivité()->getId();
		$lsSQL = <<<SQL
			Select Paa.RtvLstRessourcesSeance($liId_Séance, null, false, false, $liId_Acti) ;
SQL;
		$psLstRes = $this->RtvSQLResult($lsSQL  //  psSQL 
				, "" //  pvDftValue 
				);

		return true;
	}

	// **************************************************************************************************************
	// Définir la liste des participants d'une séance
	// poSéance				: entité de la séance
	// paLstResAInclure		: tableau des ressources à affecter à la séance (e.g. ["I1000209","G1000044","S1000075"])
	// Renvoie true si succès
	function SetLstRessourcesSéance($poSéance, $paLstResAInclure) {
		$liId_Séance = $poSéance->getId();
		$lsJSONLstRes = "";
		$lsType_Res = "";
		$liId_Res = 0;
		foreach ($paLstResAInclure as $res) {
			decomposeRessource($res, $lsType_Res, $liId_Res);
			if ($lsJSONLstRes)
				$lsJSONLstRes .= ",";
			$lsJSONLstRes .= '{"cType_Res": "' . $lsType_Res . '", "iId_Res": ' . $liId_Res . '}';
		}
		$lsJSONLstRes = '[' . $lsJSONLstRes . ']';
		$lsSQL = <<<SQL
			Select Paa.SetLstRessourcesSeance($liId_Séance, '$lsJSONLstRes') ;
SQL;

		$psLstRes = $this->RtvSQLResult($lsSQL  //  psSQL 
				, "" //  pvDftValue 
				);

		return true;
	}

	// **************************************************************************************************************
	// Construire une entité séance avec un tableau de valeurs
	// paSéances		: tableau contenant les clés iId_Seance, iActi, tDebut, tFin
	// &poEntitéSéance	: pour retour de l'entité
	// &pbNouvelleSéance	: pour retour true si c'est une nouvelle séance
	// Renvoie true si succès
	function getSéanceAvecTableau($paSéance, &$poEntitéSéance, &$pbNouvelleSéance = false) {
		$pbNouvelleSéance = false;
		$em = $this->getEntityManager();
		if (isset($paSéance["iId_Seance"]) && $paSéance["iId_Seance"]) {
			// Séance préexistante
//MG Ajout 20200603 Début
			$poEntitéSéance = $this->find($paSéance["iId_Seance"]);
			if (!isset($paSéance["iActi"])) {
				$paSéance["iActi"] = $poEntitéSéance->getActivité()->getIidactivite();
			}
			$oActi = $em->getReference('PaaBundle:activites', $paSéance["iActi"]);
			$poEntitéSéance->setActivité($oActi);
//MG Ajout 20200603 Fin
		} else if (!isset($paSéance["iActi"]) or empty($paSéance["iActi"])) {
			// Id d'activité manquant
			$psJSONSeances = '{"Erreur": "Id d\'activité manquant"}';
			return false;
		} else {
			// Nouvelle séance
			$pbNouvelleSéance = true;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//			$poEntitéSéance = new actiRealisations();
			$poEntitéSéance = new seances();
//MG Modification 20200218 Fin
			// Il faut fournir une référence à l'activité de cette séance
			// cf. https://stackoverflow.com/questions/44955296/doctrine-2-persist-entity-with-joined-tables-foreign-key-opposed-to-foreign-e

			$loRefActi = $em->getReference('PaaBundle:activites', $paSéance["iActi"]);
			// LG 20190718, autre solution ? : $activites = $em->getRepository(activites::class)->find($paSéance["iActi"]);

			$poEntitéSéance->setActivité($loRefActi);
		}
		// Appliquer les modifications à apporter à la séance
		$poEntitéSéance->setTdebut($paSéance["tDebut"]);
		$poEntitéSéance->setTfin($paSéance["tFin"]);
		return true;
	}

	// **************************************************************************************************************
	function AutorisePoseSéance($toSeance, $psType_Res, $piId_Res, $pbNewRecord, &$tcMsg) {
// Pour tests
		return true;
		if (!$pbNewRecord && $this->oParamètresBDD->lGèreAccompagnementParSéances()) {
			// Tratement de l'accompagnement par séances (en principe obsolète)
			// voir cRecSeance.AutorisePoseSéance dans proc_séance_recséance.prg
			$tcMsg = "Désolé, le cas de l'accompagnement par séance n'est pas encore géré";
			;
			return false;
		}

		$loSéanceOriginale = $this->getEntityManager()->getUnitOfWork()->getOriginalEntityData($toSeance);
//echo $toSeance->getTdebut() == $loSéanceOriginale["tdebut"] ;
//echo $toSeance->getTfin() == $loSéanceOriginale["tfin"] ;
		IF (!$pbNewRecord
				And ( $toSeance->getTdebut() != $loSéanceOriginale["tdebut"]
				Or $toSeance->getTfin() != $loSéanceOriginale["tfin"]
//						Or $toSeance->tDebutSeance != $loSéanceOriginale->tDebutSeance
//						Or $toSeance->nFinSeance != $loSéanceOriginale->tFinSeance
				)
		) {
//				And Not Autorise("ModiHorairesSéance"	// psAutoriseQuoi
//								, m.gsUser				// psUser
//								, "W" ;					// psTypeAccès
//								, $toSeance->iId_Seance ;		// pvVar1
//								, $pcType_Res ;		// pvVar2
//								, $piId_Res ;		// pvVar3
//								, This.nDebut != This.nDebutSeance ;
//									or This.nFin != This.nFinSeance ;
//									or This.oRecInitial.nDebut != This.oRecInitial.nDebutSeance ;
//									or This.oRecInitial.nFin != This.oRecInitial.nFinSeance ;	// pvVar4 : .T. si c'est pour l'ajustement d'une participation à l'intérieur d'une séance
//								)
			// Pas les droits
			$tcMsg = "Annulation par AutorisePoseSéance.";
//$tcMsg .= "<br>"
//		. $toSeance->getTdebut()->format('d/m/Y h:i:s') . "<br>"
//		. $loSéanceOriginale["tdebut"]->format('d/m/Y h:i:s') . "<br>"
//		. $toSeance->getTfin()->format('d/m/Y h:i:s') . "<br>"
//		. $loSéanceOriginale["tfin"]->format('d/m/Y h:i:s') . "<br>" ;
			return false;
		}

		$tcMsg = "Refusé par AutorisePoseSéance !!!";
		return false;
	}

	// **************************************************************************************************************
	// Déterminer si une séance utilise une ressource passée en paramètre
	// piSéance						: Id de séance
	// psType_Res					: type de ressource, ou liste de ressources
	// piId_Res						: Id de ressource, ou vide si psType_Res est une liste
	// (pbUsagIgnoreGroupe)			: si .F., on cherche aussi pour le groupe, si psType_Res = "U"
	// LG 20100614
	Function SéanceUtiliseRessource($piSéance
	, $psType_Res
	, $piId_Res = 0
	, $pbIgnoreEnAmontDeUsager = false
	, $pbChercheEnAvalDeEtablissement = false
	, $pbChercheEnAvalDeGroupe = false
	) {
		$pbIgnoreEnAmontDeUsager = $pbIgnoreEnAmontDeUsager ? 'true' : 'false';
		$pbChercheEnAvalDeEtablissement = $pbChercheEnAvalDeEtablissement ? 'true' : 'false';
		$pbChercheEnAvalDeGroupe = $pbChercheEnAvalDeGroupe ? 'true' : 'false';
		$lsSQL = <<<SQL
			Select Paa.SeanceUtiliseRessource($piSéance, '$psType_Res', $piId_Res, $pbIgnoreEnAmontDeUsager, $pbChercheEnAvalDeEtablissement, $pbChercheEnAvalDeGroupe) ;
SQL;

		$llUtilise = $this->RtvSQLResult($lsSQL //  psSQL 
				, false //  pvDftValue 
				);

		// Valeur de retour
		Return $llUtilise;
	}

	// **************************************************************************************************************
	// Déterminer si une séance utilise une ressource passée en paramètre
	// piSéance						: Id de séance
	// piId_Activite				: pour renvoi de l'information per référence
	// piId_Acti_Base				: pour renvoi de l'information per référence
	// piSousGroupeActiBase			: pour renvoi de l'information per référence
	// LG 20100614
	Function EstAbsence($piSéance
	, &$piId_Activite = null
	, &$piId_Acti_Base = null
	, &$piSousGroupeActiBase = null
	) {
		$piSéance = Nvl($piSéance, 'null');
		$piId_Activite = Nvl($piId_Activite, 'null');
		$piId_Acti_Base = Nvl($piId_Acti_Base, 'null');
		$piSousGroupeActiBase = Nvl($piSousGroupeActiBase, 'null');

		$lsSQL = <<<SQL
			Select * From Paa.EstAbsence($piId_Activite, $piId_Acti_Base, $piSéance, $piSousGroupeActiBase) ;
SQL;
		$llFound = null;
		$laRow = $this->RtvSQLFirstRow($lsSQL, $llFound);
		if ($llFound) {
// echo "Trouvé : " . $laRow["lestabsence"]?'vrai':'faux' ;
			$llEstAbsence = $laRow["lestabsence"];
			$piId_Activite = $laRow["iid_activite"];
			$piId_Acti_Base = $laRow["iid_acti_base"];
			$piSousGroupeActiBase = $laRow["isousgroupeactibase"];
		} else {
// echo "NON Trouvé" ;
			$llEstAbsence = false;
			$piId_Activite = null;
			$piId_Acti_Base = null;
			$piSousGroupeActiBase = null;
		}

		// Valeur de retour
		Return $llEstAbsence;
	}

	// **************************************************************************************************************
	// Déterminer si une séance utilise une ressource passée en paramètre
	// piSéance						: Id de séance
	// LG 20100614
	Function EstPrésence($piSéance
	, $piId_Activite = null
	, $piId_Acti_Base = null
	, $piSousGroupeActiBase = null
	) {
		$piSéance = Nvl($piSéance, 'null');
		$piId_Activite = Nvl($piId_Activite, 'null');
		$piId_Acti_Base = Nvl($piId_Acti_Base, 'null');
		$lsSQL = <<<SQL
			Select Paa.EstPresence($piId_Activite, $piId_Acti_Base, $piSéance) As lEstPresence ;
SQL;
		$llFound = null;
		$laRow = $this->RtvSQLFirstRow($lsSQL, $llFound);
		if ($llFound) {
			$llEstPresence = $laRow["lestpresence"];
		} else {
			$llEstPresence = false;
		}

		// Valeur de retour
		Return $llEstPresence;
	}

	// **************************************************************************************************************
	// Renvoyer l'Id de l'utilisateur créateur de la séance passée en paramètre
	// $piId_Séance			: id de la séance dont on veut connaitre le créateur
	// (&$psNomCréateur)	: pour retour du nom du créateur (rempli seulement si fourni non vide)
	function RtvUserCréateur($piId_Séance, &$psNomCréateur = "") {
		return $this->RtvUserCréateurPourTable("tSeances", $piId_Séance, $psNomCréateur);
	}

	// **************************************************************************************************************
	// Renvoyer le libellé d'une séance
	// dcFormatLibelle_GrilleImpr 							"Grilleimpr"
	function getLibelle($piId_Séance, $psTypeRessource, $psTypeSpécial = "Grilleimpr") {
		$psTypeRessource = "I";
		$lsSQLFormatLibelle = "Select * From Paa.RtvFormatLblSeance("
				. "'" . $psTypeRessource . "'"  //tsTypeRessource varchar
				. ", true"	   //tbFormatHTML boolean default false
				. ", '" . $psTypeSpécial . "'"  //tvTypeSpecial varchar default ''
				. ", null"	   //tdDateSeance date default Null
				. ")";
		$lsSQLFormatLibelle = "(" . $lsSQLFormatLibelle . ")::integer";
		$lsSQL = "Select Paa.RtvLibelleActi_Realisation(Null"	   // piActi integer
				. ", " . $piId_Séance	// tiReal integer
				. ", " . $lsSQLFormatLibelle  // piFormat integer Default Null
				. ", Null"	   // piTaillePolice integer default 8
				. ")";
		$llFound = false;
		$libelle = $this->RtvSQLResult($lsSQL, "Libellé introuvable", $llFound);
		return $libelle;
	}

	// **************************************************************************************************************
	// Effacer la participation d'une ressource à une séance
	// $piId_Séance			: id de la séance à supprimer
	// LG 20190610
	function EffaceSéance($piId_Séance) {
		$lsSQL = "Delete From Paa.tSeances Where iId_Seance = $piId_Séance ;";
		return $this->ExecuteSQL($lsSQL);
	}

	// **************************************************************************************************************
	// Effacer la participation d'une ressource à une séance
	// $piId_Séance			: id de la séance dont on veut enlever une ressource participante
	// $psLstRes			: liste des id des ressources pour lesquelles on doit effacer la participation
	// LG 20190610
	function AnnuleParticipationRessource($piId_Séance, $psLstRes, $psCommentaire = '') {
		$lsSQL = "Select PAA.AnnuleParticipationRessource('$psLstRes'"   // psLstRes
				. ", '$piId_Séance'"		 // psLstSéances
				. ", '$psCommentaire'"		 // psCommentaire
				. ", false"			 // pbAnul_Creat
				. ", false"			 // pbDétruitParticipation
				. ", false"			 // pbDétruitSéanceSiVide
				. ") ;";
		return $this->ExecuteSQL($lsSQL);
	}

//MG 20200309 Ajout Début
	// **************************************************************************************************************
	// Retrouver le nombre d'heure réalisé pout une activité
	// $idActivite			: id de l'a séance'activité dont on veut le nombre d'heure réalisé
	function NombreHeuresRealise($idActivite) {
		$lsSQL = "SELECT sum(tfin - tdebut) FROM paa.tseances WHERE iacti = $idActivite ;";
		return $this->ExecuteSQL($lsSQL);
	}

//MG 20200309 Ajout Fin
}
