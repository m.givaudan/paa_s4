<?php

namespace App\PaaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use \PDO;

$lsFich = dirname(dirname(__FILE__)) . '/Component/Paa/ressources.php';
require_once $lsFich;

class ressourcesActibasesRepository extends EntityRepository {

	public function findAllPourActiBase($psTypeRes, $piIdActiBase) {
		$entityManager = $this->getEntityManager();
		if ($psTypeRes == 'I') {
			$nomIid = 'iId_Intervenant';
			$nomTable = 'Intervenants';
			$nomNom = "Evl(Res.cNomCourt, Coalesce(Res.cNom, '') || ' ' || Coalesce(Res.cPrenom, ''))";
		} else if ($psTypeRes == 'G') {
			$nomIid = 'iId_groupe';
			$nomTable = 'groupes';
			$nomNom = "Res.cNom";
		} else if ($psTypeRes == 'S') {
			$nomIid = 'iId_Salle';
			$nomTable = 'Salles';
			$nomNom = "Res.cNom";
		} else if ($psTypeRes == 'U') {
			$nomIid = 'iId_Usager';
			$nomTable = 'usagers';
			$nomNom = "Evl(Res.cNomCourt, Res.cNom || ' ' || Res.cPrenom)";
		}

		$reqsql = "select Res." . $nomIid . " iId_Res, " . $nomNom . " cNom"
				. ", RAB.iId_Ressource_ActiBase NotNull As lCompetent, RAB.iCapacite"
				. ", " . $piIdActiBase . " iId_ActiBase"
				. ", SGAB.iId_ActiSousGroupe, SGAB.iGroupe As iId_ActiGroupe"
				. " FROM PAA.Ressources_ActiBases RAB"
				. " Right Join PAA.ActiBases AB"
				. " On AB.iId_ActiBase = " . $piIdActiBase
				. " Right Join PAA.Acti_SsGroupesActi SGAB"
				. " On SGAB.iId_ActiSousGroupe = AB.iSousGroupe"
				. " Right Join PAA." . $nomTable . " Res"
				. " On RAB.cType_Res = '" . $psTypeRes . "'"
				. " And RAB.iActiBase = " . $piIdActiBase . ""
				. " And Res." . $nomIid . " = RAB.iId_Res"
				. " Order by 3 Desc ;";
		$function = $entityManager->getConnection()->prepare($reqsql);
		$function->execute();
		$allAB = $function->fetchAll(PDO::FETCH_ASSOC);

		return $allAB;
	}

	public function findAllPourRessource($psType_Res, $piId_Res) {
		$entityManager = $this->getEntityManager();

		$reqsql = "select " . $piId_Res . " iId_Res, AB.cNomCourt cNom"
				. ", AB.iId_ActiBase"
				. ", RAB.iId_Ressource_ActiBase NotNull As lCompetent, RAB.iCapacite"
				. " FROM PAA.Ressources_ActiBases RAB"
				. " Right Join PAA.ActiBases AB"
				. " On RAB.cType_Res = '" . $psType_Res . "'"
				. " And RAB.iId_Res = " . $piId_Res
				. " And RAB.iActiBase = AB.iId_ACtiBase"
				. " Where AB.isousgroupe > 0"
				. " Order by 3 Desc ;";
		$function = $entityManager->getConnection()->prepare($reqsql);
		$function->execute();
		$allAB = $function->fetchAll(PDO::FETCH_ASSOC);

		return $allAB;
	}

	// Permet de récupérer toutes les activité dans lesquelles une ressource est compétente
	// psTypeRes	: le type de la ressource demandée ('I', 'U', 'intervenant', ...)
	// piIdRes		: l'id de la ressource demandé
	// psType		: le type de 'catégorie' (GAB, SGAB)
	// piIdType		: l'id de la 'catégorie' demandée (par exemple si on demande la catégorie GAB on aura l'id de ce GAB)
	public function getActivitésOfRessource($psTypeRes, $piIdRes, $psType, $piIdType) {
		if ($psTypeRes == null) {
			$psTypeRes = 'I';
		} else {
			$psTypeRes = strtoupper(substr($psTypeRes, 0, 1));
		}
		switch ($psType) {
			case 'GAB':
				$reqsql = "select RAB.iId_Res, RAB.iId_Ressource_ActiBase NotNull As lCompetent
								, AB.iid_actibase, SGAB.iId_ActiSousGroupe, SGAB.iGroupe As iId_ActiGroupe
                        FROM PAA.actibases AB
                        Left Join PAA.Ressources_ActiBases RAB 
							On RAB.iactibase = AB.iid_actibase 
								AND RAB.ctype_res = '" . $psTypeRes . "'
								And RAB.iId_Res = " . $piIdRes . "
						Left Join PAA.Acti_SsGroupesActi SGAB 
							On SGAB.iId_ActiSousGroupe = AB.iSousGroupe 
						WHERE AB.iid_actibase > 0";
				if ($piIdType) {
					$reqsql .= " AND AB.iSousGroupe IN (SELECT iId_ActiSousGroupe FROM PAA.Acti_SsGroupesActi WHERE igroupe = " . $piIdType . ")";
				}
				$reqsql .= " AND AB.iSousGroupe > 0";
				$reqsql .= "ORDER BY 5 Asc ;";
				break;
			case 'SGAB':
				$reqsql = "select *, RAB.iId_Ressource_ActiBase NotNull As lCompetent
								, AB.iid_actibase, SGAB.iId_ActiSousGroupe, SGAB.iGroupe As iId_ActiGroupe
                        FROM PAA.actibases AB
						Left Join PAA.Ressources_ActiBases RAB
							On RAB.iactibase = AB.iid_actibase 
								AND RAB.ctype_res = '" . $psTypeRes . "'
								And RAB.iId_Res = " . $piIdRes . "
						Left Join PAA.Acti_SsGroupesActi SGAB 
							On SGAB.iId_ActiSousGroupe = AB.iSousGroupe 
                        WHERE AB.iid_actibase > 0
							AND AB.isousgroupe = " . $piIdType . "
                    Order by 5 Asc ;";
				break;
			default:
				$reqsql = "select RAB.iId_Res, RAB.iId_Ressource_ActiBase NotNull As lCompetent
								, RAB.iCapacite, AB.cnom
								, AB.iid_actibase, SGAB.iId_ActiSousGroupe, SGAB.iGroupe As iId_ActiGroupe
                        FROM PAA.actibases AB
                        Left Join PAA.Ressources_ActiBases RAB 
							On RAB.iactibase = AB.iid_actibase 
								AND RAB.ctype_res = '" . $psTypeRes . "'
								And RAB.iId_Res = " . $piIdRes . "
                        Left Join PAA.Acti_SsGroupesActi  SGAB 
							On SGAB.iId_ActiSousGroupe = AB.iSousGroupe 
						WHERE AB.iid_actibase > 0
							ORDER BY 5 Asc ;";
				break;
		}


		$entityManager = $this->getEntityManager();
		$function = $entityManager->getConnection()->prepare($reqsql);
		$function->execute();
		$allRes = $function->fetchAll(PDO::FETCH_ASSOC);

		return $allRes;
	}

	// Permet de comparer les coches des AB que l'on veut enregistrer avec les coches qui sont déjà dans la bdd
	//psTypeRes : Le type de Ressource demandée ('I','U','G','E', 'C')
	//$pvIdItem : L'id de l'activité de base concernée
	//			: ou de la ressource (Type+Id)
	//paNouvellesValeurs : Tableau contenant un tableau par checkbox cochée
	public function CompareLesCoches($psTypeRes, $pvIdItem, $paNouvellesValeurs) {
		switch ($psTypeRes) {
			case 'C':
				// Compétences d'une ressource
				decomposeRessource($pvIdItem, $lsType_Res, $liId_Res);
				$donneesBdd = $this->findAllPourRessource($lsType_Res, $liId_Res);
				break;
			default:
				// Ressources compétentes pour une actiBase
				$lsType_Res = strtoupper($psTypeRes);
				$donneesBdd = $this->findAllPourActiBase($psTypeRes, $pvIdItem);
		}
		$lsReq = null;
		foreach ($donneesBdd as $val) {
			$lsType_Res_Item = $lsType_Res;
			$liId_ActiBase_Item = $val['iid_actibase'];
			switch ($psTypeRes) {
				case 'C':
					// Compétences d'une ressource
					$lsKey = 'CocheC_' . $val['iid_actibase'];
					$liId_Res_Item = $liId_Res;
					break;
				default:
					// Ressources compétentes pour une actiBase
					$lsKey = 'Coche' . strtoupper($psTypeRes) . '_' . $val['iid_res'];
					$liId_Res_Item = $val['iid_res'];
			}
			if (array_key_exists($lsKey, $paNouvellesValeurs)) {
				//si la case est cochée
				if ($val['lcompetent'] != true) {
					//si la case n'était pas cochée avant
					// ajouter un champ dans la table ressources_actibases
					$lsReq .= "INSERT INTO paa.ressources_actibases(ctype_res, iid_res, iactibase)"
							. " VALUES('" . $lsType_Res_Item . "'"
							. ", " . $liId_Res_Item . ""
							. ", " . $liId_ActiBase_Item . ""
							. ");\n";
				}
			} else {
				if ($val['lcompetent'] == true) {
					//si la case était cochée mais ne l'est plus
					//supprimer la ligne correspondant dans la table ressources_actibases
					$lsReq .= "DELETE FROM paa.ressources_actibases"
							. " WHERE ctype_res ='" . $lsType_Res_Item . "'"
							. " AND iid_res = " . $liId_Res_Item . ""
							. " AND iactibase = " . $liId_ActiBase_Item . ""
							. ";\n";
				}
			}
		}
		if ($lsReq != NULL) {
			$entityManager = $this->getEntityManager();
			$entityManager->getConnection()->exec($lsReq);
		}
	}

	// Permet de concevoir un objet nous permettant de construire un treeview
	//ligneParent : la ligne du parent (null si c'est un GAB)
	//tableauGlobal : le tableau de données contenant toutes les GAB, SGAB, AB
	public function remplirNoeudArthur($ligneParent, $tableauGlobal) {
		if ($ligneParent == null) {
			$obj = (object) array(
						'ctype' => null,
						'cid' => null,
						'text' => null,
						'icouleur' => null,
						'iid' => null,
						'cid_parent' => null
			);
		} else {
			$obj = (object) array(
						'ctype' => $ligneParent['ctype'],
						'cid' => $ligneParent['cid'],
						'cid' => $ligneParent['cid'],
						'text' => $ligneParent['cnom'],
						'icouleur' => $ligneParent['icouleur'],
						'iid' => $ligneParent['iid'],
						'cid_parent' => $ligneParent['cid_parent']
			);
		}
		foreach ($tableauGlobal as $val) {
			if ($val['cid_parent'] === $obj->cid) {
				$fils = $this->remplirNoeudArthur($val, $tableauGlobal);
				$obj->nodes[] = $fils;
			}
		}
		return $obj;
	}

}
