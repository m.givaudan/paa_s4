<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PaaBundle\Repository;

/**
 * Description of actiSsgroupesactiRepository
 *
 * @author Administrateur
 */
class actiSsgroupesactiRepository extends PAABaseRepository {
	
	//MG Ajout 20200824 Début
	public function checkValidité($loEntité){
			
		$update = ';';
		if ($loEntité->getId()){
			$update = ' AND acti_ssgroupesacti.iid_actisousgroupe != '.$loEntité->getId().';';
		}
		
		$psSQL = "SELECT COUNT(cnom) FROM paa.acti_ssgroupesacti WHERE upper(cnom) = upper('".$loEntité->getCnom()."') AND igroupe = ".$loEntité->getIgroupe()->getId();
		if($this->RtvSQLResult("{$psSQL}{$update}", 0)){
			$msgErreur = 'Ce sous-groupe existe déjà pour ce groupe.';
		} else {
			return false;
		}
		return $msgErreur;
	}
//MG Ajout 20200824 Fin
}
