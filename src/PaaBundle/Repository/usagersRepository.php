<?php

namespace App\PaaBundle\Repository;

class usagersRepository extends PAABaseRepository {

//MG Ajout 20200824 Début
	public function checkValidité($loEntité){
			
		$update = ';';
		if ($loEntité->getId()){
			$update = ' AND usagers.iid_usager != '.$loEntité->getId().';';
		}
		
		$psSQL = "SELECT COUNT(cnomcourt) FROM paa.usagers WHERE upper(cnomcourt) = upper('".$loEntité->getCnomcourt()."')";
		$psSQL2 = "SELECT COUNT(cnom) FROM paa.usagers WHERE upper(cnom|| ' '||cprenom) = upper('".$loEntité->getCnom()." ".$loEntité->getCprenom()."')";	
		if($this->RtvSQLResult("{$psSQL}{$update}", 0)){
			$msgErreur = 'Ce nom court est déjà utilisé par un usager.';
		} else if ($this->RtvSQLResult("{$psSQL2}{$update}", 0)){
			$msgErreur = 'Un usager possédant le même nom et prénom existe déjà.';
		} else {
			return false;
		}
		return $msgErreur;
	}
//MG Ajout 20200824 Fin
}
