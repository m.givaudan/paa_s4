<?php

namespace App\PaaBundle\Repository;

use \PDO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class activitesTradRepository extends EntityRepository {

//    function maj_activites_trad($piSem)
//    {
//        $em = $this->getEntityManager();
//        $sql = 'SELECT * FROM paa.maj_activites_trad('.$piSem.')';
//        
//        $req = $em->getConnection()->prepare($sql);
//        $req->execute();
//        $res = $req->fetchAll(PDO::FETCH_OBJ);
//        return $res;
//    }
//    Renvoie la liste des activités, les intervenats, les usagers, les salles et ls groupes sur la semaine fournie
//    $piSemaine                    : numéro de semaine
//    $pbmasqueactisystème          :
//    $psacti_base                  :
//    $psintervenant                :
//    $pusager                      :
//    $pssalle                      :
//    $psgroupe                     :
//    $pbexclactivitésgénériques    :
	public function findAllPourSemaine($piSemaine = 0, $pbmasqueactisystème = 'false', $pbexclactivitésgénériques = 'false', $psacti_base = "'SansFiltre'", $psintervenant = "'SansFiltre'", $pusager = "'SansFiltre'", $pssalle = "'SansFiltre'", $psgroupe = "'SansFiltre'") {
		$entityManager = $this->getEntityManager();
		$sql = 'SELECT * FROM paa.maj_activites_trad(' . $piSemaine . ')';

		$req = $entityManager->getConnection()->prepare($sql);
		$req->execute();

		$sql = 'SELECT at.*, AB.cNomCourt cActiBase FROM paa.buildfiltreactivites_rtvids(' . $piSemaine . ',' . $pbmasqueactisystème . ',' . $psacti_base . ',' . $psintervenant . ',' . $pusager . ',' . $pssalle . ',' . $psgroupe . ',' . $pbexclactivitésgénériques . ')'
				. 'as af, paa_cache.activites_trad at, Paa.ActiBases AB WHERE af.iid_activite = at.iid_activite And AB.iId_ACtiBase = at.iActiBase';
//dump($sql);
		$rsm = new ResultSetMapping;
		$rsm->addEntityResult('PaaBundle:activitesTrad', 'at1');
		$rsm->addFieldResult('at1', 'iid_activite', 'iidActivite');
		$rsm->addFieldResult('at1', 'isemaine', 'isemaine');
//        $rsm->addFieldResult('at1', 'iid_actibase', 'iactibase');
		$rsm->addFieldResult('at1', 'cactibase', 'cactibase');
		$rsm->addFieldResult('at1', 'msalles', 'msalles');
		$rsm->addFieldResult('at1', 'mintervenants', 'mintervenants');
		$rsm->addFieldResult('at1', 'mgroupes', 'mgroupes');
		$rsm->addFieldResult('at1', 'mremarque', 'mremarque');
		$rsm->addFieldResult('at1', 'mlstseances', 'mlstseances');

		$query = $entityManager->createNativeQuery($sql, $rsm);
		$activites = $query->getResult();

//        dump($activites);

		return $activites;
	}

	function findActivitesParRessources($psLst_Res) {
		// Obsolète : utiliser actiBasesRepository->rtvCurCompetencesCommunes
		echo $obsolete;

		if (is_array($psLst_Res)) {
			$psLst_Res = implode(',', $psLst_Res);
		}
		if ($psLst_Res) {
			// La liste de ressources n'est pas vide : ne prendre que les activités de base pour lesquelles toutes sont compétentes
			$sql = "SELECT af.*, ab.cnom, ab.isousgroupe FROM paa.RtvCurCompetencesCommunes('" . $psLst_Res . "') af, paa.Actibases ab "
					. "WHERE af.iid_actibase = ab.iid_actibase "
					. "AND ab.isousgroupe > 0";
		} else {
			// La liste de ressources est vide : prendre toutes les activités de base
			$sql = "SELECT ab.iId_Actibase, ab.cnom, ab.isousgroupe FROM paa.Actibases ab "
					. "WHERE ab.isousgroupe > 0";
		}

//        $sql = "SELECT af.*, ab.cnom, ab.isousgroupe FROM paa.RtvCurLstRessources('".$psLst_Res."') af, paa.Actibases ab "
//           . "WHERE af.iid_actibase = ab.iid_actibase "
//           . "AND ab.isousgroupe > 0";
//        var_dump($sql);
		$em = $this->getEntityManager();
		$req = $em->getConnection()->prepare($sql);
		$req->execute();
		$lstIdActibases = $req->fetchAll(PDO::FETCH_OBJ);
//        dump($lstIdActibases);
		return $lstIdActibases;
	}

}
