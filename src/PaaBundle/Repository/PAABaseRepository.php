<?php

namespace App\PaaBundle\Repository;

use \PDO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class PAABaseRepository extends EntityRepository {

	// **************************************************************************************************************
	// Exécuter une requête SQL LMD ou LDD
	// $psSQL			: la requête LMD/LDD
	// D'après https://php.net/manual/fr/pdo.exec.php
	// LG 20190610
	public function ExecuteSQL($psSQL) {
		$loCnxn = $this->getEntityManager()->getConnection();
		$lvResult = $loCnxn->exec($psSQL);
		if ($lvResult === false) {
			//PDO::eval() might return `false` for some statements (e.g. CREATE TABLE) even if the operation completed successfully, when using PDO_DBLIB and FreeTDS. So it is not a reliable way of testing the op status.
			//PDO::errorInfo() can be used to test the SQLSTATE error code for '00000' (success) and '01000' (success with warning).
			$err = $loCnxn->errorInfo();
			if ($err[0] === '00000' || $err[0] === '01000') {
				return true;
			}
		}
		return $lvResult;
	}

	// **************************************************************************************************************
	// Renvoyer un résultat atomique pour une requête SQL fournie
	// $psSQL			: la requête censée renvoyer un résultat atomique
	// ($pvDftValue)	: la valeur si aucun résultat n'est trouvé
	// (&$plFound)		: pour retour true si une ligne a été trouvée
	public function RtvSQLResult($psSQL, $pvDftValue = null, &$plFound = null) {
		$laRow0 = $this->RtvSQLFirstRow($psSQL, $plFound);
		if ($laRow0 !== null && sizeof($laRow0) > 0) {
			$lvResult = array_values($laRow0)[0];
		} else {
			$plFound = false;
			$lvResult = $pvDftValue;
		}
		return $lvResult;
	}

	// **************************************************************************************************************
	// Renvoyer un tableau FetchAll pour une requête SQL fournie
	public function RtvSQLFirstRow($psSQL, &$plFound = null) {
		$query = $this->getEntityManager()->getConnection()->prepare($psSQL);
		$query->execute();
		if ($query->rowCount() > 0) {
			$plFound = true;
			$laRow0 = $query->fetch(PDO::FETCH_ASSOC);
		} else {
// echo 'RtvSQLFirstRow ' . $query->rowCount() . '<br>' ;
// echo 'RtvSQLFirstRow ' . $psSQL . '<br>' ;
			$plFound = false;
			$laRow0 = null;
		}
		return $laRow0;
	}

	// **************************************************************************************************************
	// Renvoyer un tableau FetchAll pour une requête SQL fournie
	public function RtvSQLArray($psSQL) {
		$query = $this->getEntityManager()->getConnection()->prepare($psSQL);
		$query->execute();
		$laResult = $query->fetchAll(PDO::FETCH_ASSOC);
		return $laResult;
	}

	// **************************************************************************************************************
	// Renvoyer l'Id de l'utilisateur créateur de la ligne de table passée en paramètre
	// $psTable				: table dans laquelle se trouve la ligne dont on veut connaitre le créateur
	// $piId_Séance			: id de la ligne dont on veut connaitre le créateur
	// (&$psNomCréateur)	: pour retour du nom du créateur (rempli seulement si fourni non vide)
	function RtvUserCréateurPourTable($psTable, $piIdLigne, &$psNomCréateur = "") {
		if (!$piIdLigne)
			return 0;
		$lsSQL = "Select iUser"
				. " From (Select * From PAA.tLogs Where iIdLigne = " . $piIdLigne . ") Tmp"
				. " Where Upper(Alltrim(cTable)) = '" . Upper(Alltrim($psTable)) . "'"
				. " And cType = 'I'";
//		$row0 = $this->RtvSQLFirstRow($lsSQL) ;
//		if ($row0) $liUser = $row0[0] ;	// Résultat trouvé
//		else $liUser = 0 ;				// Pas de résultat trouvé
		$liUser = $this->RtvSQLResult($lsSQL, 0);

		if ($psNomCréateur) {
			// On a fourni ce paramètre non vide : on veur que sa valeur soit renseignée
			// Pas encore pris en charge
			throw new Exception("Le renseignement du nom d'user n'est pas encore pris en charge");
		}

		return $liUser;
	}
	
	public function checkValidité($poEntité){
		return '';
	}

}
