<?php

// Pour la conversion d'une phrase avec la 1ere lettre du 1er mot en majuscules
// Selon https://stackoverflow.com/questions/31145361/how-to-call-ucwords-in-twig
// http://symfony.com/doc/3.4/templating/twig_extension.html
// Mais ca n'a jamais fonctionné

namespace App\PaaBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;

class AppExtension extends AbstractExtension {

	public function getFilters() {
		return array(
			new TwigFilter('properphrase', array($this, 'properPhraseFilter')),
			new TwigFilter('versduree', array($this, 'versDurée')),
		);
	}

	public function getFunctions() {
		return array(
			new TwigFunction('asset_version', [$this, 'assetVersion'])
		);
	}

	public function properPhraseFilter($psPhrase) {
		return ucwords($psPhrase);
	}

	public function versDurée($piNum) {
		return (String) $piNum;
	}

	public function assetVersion($path) {
		$this->$path = $path;

//MG Modification 20200312 Début 
//Ajout de la prise en compte des fichiers css minifier
//		$limoog = substr($path, 0, 6);
//		$extension = substr($path, 0, -2);
//        if ($limoog === 'Limoog' && $extension === 'js' && $_ENV['APP_ENV'] === 'prod'){        
//            $path = substr($path, 0, -2);
//            $path = $path.'min.js';
//		}
//		$path = 'Limoog/Partages/css/LouisStyle.css'; //TEST
		//MG Modification 20200602 Début
		//Pour la gestion des fichiers dans dossier public avec le prefix manquant
		//https://github.com/symfony/symfony/issues/28612
		$versionStrategy = new StaticVersionStrategy("v=" . $_ENV['paa_version_interne']);
		$package = new PathPackage('', $versionStrategy);
		$dirname = pathinfo($path, PATHINFO_DIRNAME);
		$limoog = substr($dirname, 0, 6);
		if ($limoog === 'Limoog' && $_ENV['APP_ENV'] === 'prod') {
			$extension = pathinfo($path, PATHINFO_EXTENSION);
			if ($extension === 'js') {
				$basename = substr(pathinfo($path, PATHINFO_BASENAME), 0, -2);
				$path = $dirname . '/' . $basename . 'min.' . $extension;
			} else if ($extension === 'css') {
				$basename = substr(pathinfo($path, PATHINFO_BASENAME), 0, -3);
				$path = $dirname . '/' . $basename . 'min.' . $extension;
			}
			return $package->getUrl($path);
		} else if ($_ENV['baseURL']){
			$path = $_ENV['baseURL'].$package->getUrl($path);
			return $path;
		} else {
			return $package->getUrl($path);
		}
		//MG Modification 20200602 Fin
//MG Modification 20200312 Fin

		$versionStrategy = new StaticVersionStrategy("v=" . $_ENV['paa_version_interne']);
		$package = new PathPackage('', $versionStrategy);
		
                $path = 'PAA2/paa_s4/public/'.$path;
                return $package->getUrl($path);
	}

}
