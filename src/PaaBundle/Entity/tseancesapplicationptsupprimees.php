<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tseancesapplicationptsupprimees
 *
 * @ORM\Table(name="paa.tseancesapplicationptsupprimees", indexes={@ORM\Index(name="tseancesapplicationptsupprimees_spt", columns={"iseanceplantype"}), @ORM\Index(name="tseancesapplicationptsupprimees_semaine", columns={"isemaine"})})
 * @ORM\Entity
 */
class tseancesapplicationptsupprimees {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_seancesapplicationptsupprimees", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tseancesapplicationptsupprimees_iid_seancesapplicationptsupprimees_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSeancesapplicationptsupprimees;

	public function setIidseancesapplicationptsupprimees($piIdseancesapplicationptsupprimees) {
		$this->iidSeancesapplicationptsupprimees = $piIdseancesapplicationptsupprimees;
		return $this;
	}

	public function getIidseancesapplicationptsupprimees() {
		return $this->iidSeancesapplicationptsupprimees;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSeancesapplicationptsupprimees;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="isemaine", type="integer", nullable=true)
	 */
	private $isemaine = '0';

	public function setIsemaine($piSemaine) {
		$this->isemaine = $piSemaine;
		return $this;
	}

	public function getIsemaine() {
		return $this->isemaine;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseanceplantype", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseanceplantype;

	public function setIseanceplantype($piSeanceplantype) {
		$this->iseanceplantype = $piSeanceplantype;
		return $this;
	}

	public function getIseanceplantype() {
		return $this->iseanceplantype;
	}

}
