<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * formatsCouleur
 *
 * @ORM\Table(name="paa.formats_couleur", uniqueConstraints={@ORM\UniqueConstraint(name="u_formats_couleur_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class formatsCouleur {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_format", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.formats_couleur_iid_format_seq", allocationSize=1, initialValue=1)
	 */
	private $iidFormat;

	public function setIidformat($piIdformat) {
		$this->iidFormat = $piIdformat;
		return $this;
	}

	public function getIidformat() {
		return $this->iidFormat;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidFormat;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=100, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ndebut", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $ndebut = '0.0';

	public function setNdebut($pnDebut) {
		$this->ndebut = $pnDebut;
		return $this;
	}

	public function getNdebut() {
		return $this->ndebut;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nfin", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $nfin = '0.0';

	public function setNfin($pnFin) {
		$this->nfin = $pnFin;
		return $this;
	}

	public function getNfin() {
		return $this->nfin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = 'rgb(255, 255, 255)';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype", type="string", length=1, nullable=true)
	 */
	private $ctype = 'S';

	public function setCtype($pcType) {
		$this->ctype = $pcType;
		return $this;
	}

	public function getCtype() {
		return $this->ctype;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="itype", type="integer", nullable=true)
	 */
	private $itype = '-5';

	public function setItype($piType) {
		$this->itype = $piType;
		return $this;
	}

	public function getItype() {
		return $this->itype;
	}

}
