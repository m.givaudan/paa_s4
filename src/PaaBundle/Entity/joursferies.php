<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * joursferies
 *
 * @ORM\Table(name="paa.joursferies", uniqueConstraints={@ORM\UniqueConstraint(name="u_joursferies_ddate&cnom", columns={"cnom", "ddate"})}, indexes={@ORM\Index(name="joursferies_jf_date", columns={"ddate"})})
 * @ORM\Entity
 */
class joursferies {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_jourferie", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.joursferies_iid_jourferie_seq", allocationSize=1, initialValue=1)
	 */
	private $iidJourferie;

	public function setIidjourferie($piIdjourferie) {
		$this->iidJourferie = $piIdjourferie;
		return $this;
	}

	public function getIidjourferie() {
		return $this->iidJourferie;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidJourferie;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate", type="date", nullable=true)
	 */
	private $ddate;

	//private $ddate = '0001-01-01';
	public function setDdate($pdDate) {
		$this->ddate = $pdDate;
		return $this;
	}

	public function getDdate() {
		return $this->ddate;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddatefin", type="date", nullable=true)
	 */
	private $ddatefin;

	public function setDdatefin($pdDatefin) {
		$this->ddatefin = $pdDatefin;
		return $this;
	}

	public function getDdatefin() {
		return $this->ddatefin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

}
