<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * typesclasses
 *
 * @ORM\Table(name="paa.typesclasses", uniqueConstraints={@ORM\UniqueConstraint(name="u_typesclasses_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class typesclasses {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_typeclasse", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.typesclasses_iid_typeclasse_seq", allocationSize=1, initialValue=1)
	 */
	private $iidTypeclasse = 'TypesClasses';

	public function setIidtypeclasse($piIdtypeclasse) {
		$this->iidTypeclasse = $piIdtypeclasse;
		return $this;
	}

	public function getIidtypeclasse() {
		return $this->iidTypeclasse;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidTypeclasse;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icycle", type="integer", nullable=true)
	 */
	private $icycle = '0';

	public function setIcycle($piCycle) {
		$this->icycle = $piCycle;
		return $this;
	}

	public function getIcycle() {
		return $this->icycle;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

}
