<?php

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Paa_Securite.authtoken",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="auth_tokens_value_unique", columns={"value"})}
 * )
 */
class AuthToken {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $value;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	protected $createdAt;

	/* "Users" changé en "users" */

	/**
	 * @var \users
	 *
	 * @ORM\ManyToOne(targetEntity="users")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iuser", referencedColumnName="iid_user")
	 * })
	 *      */
	protected $user;

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getValue() {
		return $this->value;
	}

	public function setValue($value) {
		$this->value = $value;
	}

	public function getCreatedAt() {
		return $this->createdAt;
	}

	public function setCreatedAt(\DateTime $createdAt) {
		$this->createdAt = $createdAt;
	}

	public function getUser() {
		return $this->user;
		// return new users() ;
	}

//    public function setUser(User $user)
	public function setUser(users $user) {
		$this->user = $user;
	}

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

