<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * actiGroupesacti
 *
 * @ORM\Table(name="paa.acti_groupesacti", uniqueConstraints={@ORM\UniqueConstraint(name="u_acti_groupesacti_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="acti_groupesacti_dansbilan", columns={"ldansbilan"}), @ORM\Index(name="IDX_BB6B2746DCB91600", columns={"ietablissement"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\actiGroupesactiRepository") 
 */
class actiGroupesacti {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_actigroupe", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.acti_groupesacti_iid_actigroupe_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
	private $iidActigroupe = 'acti_groupesacti';

	public function setIidactigroupe($piIdactigroupe) {
		$this->iidActigroupe = $piIdactigroupe;
		return $this;
	}

	public function getIidactigroupe() {
		return $this->iidActigroupe;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidActigroupe;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nconcetab", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $nconcetab = '2';

	public function setNconcetab($pnConcetab) {
		$this->nconcetab = $pnConcetab;
		return $this;
	}

	public function getNconcetab() {
		return $this->nconcetab;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nconcinterv", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $nconcinterv = '2';

	public function setNconcinterv($pnConcinterv) {
		$this->nconcinterv = $pnConcinterv;
		return $this;
	}

	public function getNconcinterv() {
		return $this->nconcinterv;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nconcusag", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $nconcusag = '2';

	public function setNconcusag($pnConcusag) {
		$this->nconcusag = $pnConcusag;
		return $this;
	}

	public function getNconcusag() {
		return $this->nconcusag;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nconcgroup", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $nconcgroup = '2';

	public function setNconcgroup($pnConcgroup) {
		$this->nconcgroup = $pnConcgroup;
		return $this;
	}

	public function getNconcgroup() {
		return $this->nconcgroup;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nconcsalle", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $nconcsalle = '2';

	public function setNconcsalle($pnConcsalle) {
		$this->nconcsalle = $pnConcsalle;
		return $this;
	}

	public function getNconcsalle() {
		return $this->nconcsalle;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldansbilan", type="boolean", nullable=true)
	 */
	private $ldansbilan = false;

	public function setLdansbilan($plDansbilan) {
		$this->ldansbilan = $plDansbilan;
		return $this;
	}

	public function getLdansbilan() {
		return $this->ldansbilan;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '0';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var \etablissements
	 *
	 * @ORM\ManyToOne(targetEntity="etablissements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ietablissement", referencedColumnName="iid_etablissement")
	 * })
	 */
	private $ietablissement;

	public function setIetablissement($piEtablissement) {
		$this->ietablissement = $piEtablissement;
		return $this;
	}

	public function getIetablissement() {
		return $this->ietablissement;
	}

}
