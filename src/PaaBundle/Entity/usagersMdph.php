<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersMdph
 *
 * @ORM\Table(name="paa.usagers_mdph", indexes={@ORM\Index(name="usagers_mdph_usager", columns={"iid_usager"}), @ORM\Index(name="usagers_mdph_archiv", columns={"darchivage"}), @ORM\Index(name="usagers_mdph_uc_caisse", columns={"icaisse"}), @ORM\Index(name="usagers_mdph_uc_contact", columns={"icontact"})})
 * @ORM\Entity
 */
class usagersMdph {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_usager_caisse", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.usagers_mdph_iid_usager_caisse_seq", allocationSize=1, initialValue=1)
	 */
	private $iidUsagerCaisse;

	public function setIidusagercaisse($piIdusagercaisse) {
		$this->iidUsagerCaisse = $piIdusagercaisse;
		return $this;
	}

	public function getIidusagercaisse() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icaisse", type="integer", nullable=true)
	 */
	private $icaisse = '0';

	public function setIcaisse($piCaisse) {
		$this->icaisse = $piCaisse;
		return $this;
	}

	public function getIcaisse() {
		return $this->icaisse;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icontact", type="integer", nullable=true)
	 */
	private $icontact = '0';

	public function setIcontact($piContact) {
		$this->icontact = $piContact;
		return $this;
	}

	public function getIcontact() {
		return $this->icontact;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnodossier", type="string", length=50, nullable=true)
	 */
	private $cnodossier = '';

	public function setCnodossier($pcNodossier) {
		$this->cnodossier = $pcNodossier;
		return $this;
	}

	public function getCnodossier() {
		return $this->cnodossier;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddebut", type="date", nullable=true)
	 */
	private $ddebut;

	public function setDdebut($pdDebut) {
		$this->ddebut = $pdDebut;
		return $this;
	}

	public function getDdebut() {
		return $this->ddebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinplacement", type="date", nullable=true)
	 */
	private $dfinplacement;

	public function setDfinplacement($pdFinplacement) {
		$this->dfinplacement = $pdFinplacement;
		return $this;
	}

	public function getDfinplacement() {
		return $this->dfinplacement;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinaah", type="date", nullable=true)
	 */
	private $dfinaah;

	public function setDfinaah($pdFinaah) {
		$this->dfinaah = $pdFinaah;
		return $this;
	}

	public function getDfinaah() {
		return $this->dfinaah;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctauxinvalidite", type="string", length=50, nullable=true)
	 */
	private $ctauxinvalidite = '';

	public function setCtauxinvalidite($pcTauxinvalidite) {
		$this->ctauxinvalidite = $pcTauxinvalidite;
		return $this;
	}

	public function getCtauxinvalidite() {
		return $this->ctauxinvalidite;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfininvalidite", type="date", nullable=true)
	 */
	private $dfininvalidite;

	public function setDfininvalidite($pdFininvalidite) {
		$this->dfininvalidite = $pdFininvalidite;
		return $this;
	}

	public function getDfininvalidite() {
		return $this->dfininvalidite;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinrth", type="date", nullable=true)
	 */
	private $dfinrth;

	public function setDfinrth($pdFinrth) {
		$this->dfinrth = $pdFinrth;
		return $this;
	}

	public function getDfinrth() {
		return $this->dfinrth;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinactp", type="date", nullable=true)
	 */
	private $dfinactp;

	public function setDfinactp($pdFinactp) {
		$this->dfinactp = $pdFinactp;
		return $this;
	}

	public function getDfinactp() {
		return $this->dfinactp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clieuprofessionnel", type="string", length=50, nullable=true)
	 */
	private $clieuprofessionnel = '';

	public function setClieuprofessionnel($pcLieuprofessionnel) {
		$this->clieuprofessionnel = $pcLieuprofessionnel;
		return $this;
	}

	public function getClieuprofessionnel() {
		return $this->clieuprofessionnel;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dembauche", type="date", nullable=true)
	 */
	private $dembauche;

	public function setDembauche($pdEmbauche) {
		$this->dembauche = $pdEmbauche;
		return $this;
	}

	public function getDembauche() {
		return $this->dembauche;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinplacement_npa", type="boolean", nullable=true)
	 */
	private $lfinplacementNpa = false;

	public function setLfinplacementnpa($plFinplacementnpa) {
		$this->lfinplacementNpa = $plFinplacementnpa;
		return $this;
	}

	public function getLfinplacementnpa() {
		return $this->lfinplacementNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinaah_npa", type="boolean", nullable=true)
	 */
	private $lfinaahNpa = false;

	public function setLfinaahnpa($plFinaahnpa) {
		$this->lfinaahNpa = $plFinaahnpa;
		return $this;
	}

	public function getLfinaahnpa() {
		return $this->lfinaahNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinrth_npa", type="boolean", nullable=true)
	 */
	private $lfinrthNpa = false;

	public function setLfinrthnpa($plFinrthnpa) {
		$this->lfinrthNpa = $plFinrthnpa;
		return $this;
	}

	public function getLfinrthnpa() {
		return $this->lfinrthNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinactp_npa", type="boolean", nullable=true)
	 */
	private $lfinactpNpa = false;

	public function setLfinactpnpa($plFinactpnpa) {
		$this->lfinactpNpa = $plFinactpnpa;
		return $this;
	}

	public function getLfinactpnpa() {
		return $this->lfinactpNpa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinpriseencharge", type="date", nullable=true)
	 */
	private $dfinpriseencharge;

	public function setDfinpriseencharge($pdFinpriseencharge) {
		$this->dfinpriseencharge = $pdFinpriseencharge;
		return $this;
	}

	public function getDfinpriseencharge() {
		return $this->dfinpriseencharge;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinpriseencharge_npa", type="boolean", nullable=true)
	 */
	private $lfinpriseenchargeNpa = false;

	public function setLfinpriseenchargenpa($plFinpriseenchargenpa) {
		$this->lfinpriseenchargeNpa = $plFinpriseenchargenpa;
		return $this;
	}

	public function getLfinpriseenchargenpa() {
		return $this->lfinpriseenchargeNpa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinaeeh", type="date", nullable=true)
	 */
	private $dfinaeeh;

	public function setDfinaeeh($pdFinaeeh) {
		$this->dfinaeeh = $pdFinaeeh;
		return $this;
	}

	public function getDfinaeeh() {
		return $this->dfinaeeh;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinaeeh_npa", type="boolean", nullable=true)
	 */
	private $lfinaeehNpa = false;

	public function setLfinaeehnpa($plFinaeehnpa) {
		$this->lfinaeehNpa = $plFinaeehnpa;
		return $this;
	}

	public function getLfinaeehnpa() {
		return $this->lfinaeehNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfininvalidite_npa", type="boolean", nullable=true)
	 */
	private $lfininvaliditeNpa = false;

	public function setLfininvaliditenpa($plFininvaliditenpa) {
		$this->lfininvaliditeNpa = $plFininvaliditenpa;
		return $this;
	}

	public function getLfininvaliditenpa() {
		return $this->lfininvaliditeNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinpch_npa", type="boolean", nullable=true)
	 */
	private $lfinpchNpa = false;

	public function setLfinpchnpa($plFinpchnpa) {
		$this->lfinpchNpa = $plFinpchnpa;
		return $this;
	}

	public function getLfinpchnpa() {
		return $this->lfinpchNpa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="darchivage", type="date", nullable=true)
	 */
	private $darchivage;

	public function setDarchivage($pdArchivage) {
		$this->darchivage = $pdArchivage;
		return $this;
	}

	public function getDarchivage() {
		return $this->darchivage;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactloyer", type="boolean", nullable=true)
	 */
	private $lfactloyer = false;

	public function setLfactloyer($plFactloyer) {
		$this->lfactloyer = $plFactloyer;
		return $this;
	}

	public function getLfactloyer() {
		return $this->lfactloyer;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldotationglobale", type="boolean", nullable=true)
	 */
	private $ldotationglobale = false;

	public function setLdotationglobale($plDotationglobale) {
		$this->ldotationglobale = $plDotationglobale;
		return $this;
	}

	public function getLdotationglobale() {
		return $this->ldotationglobale;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfincartestationnement", type="date", nullable=true)
	 */
	private $dfincartestationnement;

	public function setDfincartestationnement($pdFincartestationnement) {
		$this->dfincartestationnement = $pdFincartestationnement;
		return $this;
	}

	public function getDfincartestationnement() {
		return $this->dfincartestationnement;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfincartestationnement_npa", type="boolean", nullable=true)
	 */
	private $lfincartestationnementNpa = false;

	public function setLfincartestationnementnpa($plFincartestationnementnpa) {
		$this->lfincartestationnementNpa = $plFincartestationnementnpa;
		return $this;
	}

	public function getLfincartestationnementnpa() {
		return $this->lfincartestationnementNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinactp_actif", type="boolean", nullable=true)
	 */
	private $lfinactpActif = false;

	public function setLfinactpactif($plFinactpactif) {
		$this->lfinactpActif = $plFinactpactif;
		return $this;
	}

	public function getLfinactpactif() {
		return $this->lfinactpActif;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinpch", type="date", nullable=true)
	 */
	private $dfinpch;

	public function setDfinpch($pdFinpch) {
		$this->dfinpch = $pdFinpch;
		return $this;
	}

	public function getDfinpch() {
		return $this->dfinpch;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinpch_actif", type="boolean", nullable=true)
	 */
	private $lfinpchActif = false;

	public function setLfinpchactif($plFinpchactif) {
		$this->lfinpchActif = $plFinpchactif;
		return $this;
	}

	public function getLfinpchactif() {
		return $this->lfinpchActif;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinplacement2", type="date", nullable=true)
	 */
	private $dfinplacement2;

	public function setDfinplacement2($pdFinplacement2) {
		$this->dfinplacement2 = $pdFinplacement2;
		return $this;
	}

	public function getDfinplacement2() {
		return $this->dfinplacement2;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinplacement2_npa", type="boolean", nullable=true)
	 */
	private $lfinplacement2Npa = false;

	public function setLfinplacement2npa($plFinplacement2npa) {
		$this->lfinplacement2Npa = $plFinplacement2npa;
		return $this;
	}

	public function getLfinplacement2npa() {
		return $this->lfinplacement2Npa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactprixjournee", type="boolean", nullable=true)
	 */
	private $lfactprixjournee = false;

	public function setLfactprixjournee($plFactprixjournee) {
		$this->lfactprixjournee = $plFactprixjournee;
		return $this;
	}

	public function getLfactprixjournee() {
		return $this->lfactprixjournee;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactreversion", type="boolean", nullable=true)
	 */
	private $lfactreversion = false;

	public function setLfactreversion($plFactreversion) {
		$this->lfactreversion = $plFactreversion;
		return $this;
	}

	public function getLfactreversion() {
		return $this->lfactreversion;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iid_usager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iidUsager;

	public function setIidusager($piIdusager) {
		$this->iidUsager = $piIdusager;
		return $this;
	}

	public function getIidusager() {
		return $this->iidUsager;
	}

}
