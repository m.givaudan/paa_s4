<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * duDossiersRessources
 *
 * @ORM\Table(name="paa.du_dossiers_ressources", indexes={@ORM\Index(name="du_dossiers_ressources_fk_arbo", columns={"idossier"})})
 * @ORM\Entity
 */
class duDossiersRessources {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_dossier_ressource", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.du_dossiers_ressources_iid_dossier_ressource_seq", allocationSize=1, initialValue=1)
	 */
	private $iidDossierRessource;

	public function setIiddossierressource($piIddossierressource) {
		$this->iidDossierRessource = $piIddossierressource;
		return $this;
	}

	public function getIiddossierressource() {
		return $this->iidDossierRessource;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidDossierRessource;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cres", type="string", length=50, nullable=true)
	 */
	private $cres = '';

	public function setCres($pcRes) {
		$this->cres = $pcRes;
		return $this;
	}

	public function getCres() {
		return $this->cres;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idossier", type="integer", nullable=true)
	 */
	private $idossier = '0';

	public function setIdossier($piDossier) {
		$this->idossier = $piDossier;
		return $this;
	}

	public function getIdossier() {
		return $this->idossier;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mchemin", type="text", nullable=true)
	 */
	private $mchemin = '';

	public function setMchemin($pmChemin) {
		$this->mchemin = $pmChemin;
		return $this;
	}

	public function getMchemin() {
		return $this->mchemin;
	}

}
