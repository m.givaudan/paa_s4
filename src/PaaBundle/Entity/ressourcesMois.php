<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ressourcesMois
 *
 * @ORM\Table(name="paa.ressources_mois", uniqueConstraints={@ORM\UniqueConstraint(name="u_ressources_mois_ctype_res&iid_res&iannee&imois", columns={"ctype_res", "iannee", "iid_res", "imois"})}, indexes={@ORM\Index(name="ressources_mois_id_res", columns={"iid_res"}), @ORM\Index(name="ressources_mois_annee", columns={"iannee"}), @ORM\Index(name="ressources_mois_mois", columns={"imois"})})
 * @ORM\Entity
 */
class ressourcesMois {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_mois_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iannee", type="integer", nullable=true)
	 */
	private $iannee = '0';

	public function setIannee($piAnnee) {
		$this->iannee = $piAnnee;
		return $this;
	}

	public function getIannee() {
		return $this->iannee;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="imois", type="integer", nullable=true)
	 */
	private $imois = '0';

	public function setImois($piMois) {
		$this->imois = $piMois;
		return $this;
	}

	public function getImois() {
		return $this->imois;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mhoraires", type="text", nullable=true)
	 */
	private $mhoraires;

	public function setMhoraires($pmHoraires) {
		$this->mhoraires = $pmHoraires;
		return $this;
	}

	public function getMhoraires() {
		return $this->mhoraires;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mjoursw_s1", type="text", nullable=true)
	 */
	private $mjourswS1;

	public function setMjoursws1($pmJoursws1) {
		$this->mjourswS1 = $pmJoursws1;
		return $this;
	}

	public function getMjoursws1() {
		return $this->mjourswS1;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mjoursw_s2", type="text", nullable=true)
	 */
	private $mjourswS2;

	public function setMjoursws2($pmJoursws2) {
		$this->mjourswS2 = $pmJoursws2;
		return $this;
	}

	public function getMjoursws2() {
		return $this->mjourswS2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mjoursw_s3", type="text", nullable=true)
	 */
	private $mjourswS3;

	public function setMjoursws3($pmJoursws3) {
		$this->mjourswS3 = $pmJoursws3;
		return $this;
	}

	public function getMjoursws3() {
		return $this->mjourswS3;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mjoursw_s4", type="text", nullable=true)
	 */
	private $mjourswS4;

	public function setMjoursws4($pmJoursws4) {
		$this->mjourswS4 = $pmJoursws4;
		return $this;
	}

	public function getMjoursws4() {
		return $this->mjourswS4;
	}

}
