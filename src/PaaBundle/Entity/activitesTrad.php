<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * activitesTrad
 *
 * @ORM\Table(name="paa_cache.activites_trad", uniqueConstraints={@ORM\UniqueConstraint(name="u_activites_trad_iid_activite", columns={"iid_activite"})}, indexes={@ORM\Index(name="activites_trad_isemaine", columns={"isemaine_fin"}), @ORM\Index(name="activites_trad_semfin", columns={"isemaine_fin"}), @ORM\Index(name="activites_trad_semaine", columns={"isemaine"}), @ORM\Index(name="IDX_4E765A6355B7AE9A", columns={"iactibase"})})
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\activitesTradRepository")
 */
class activitesTrad {

	/**
	 * @ORM\Column(name="iid_activite", type="integer", nullable=true)
	 * @var integer
	 * @ var \activites
	 *
	 * @ORM\Id
	 * @ ORM\GeneratedValue(strategy="NONE")
	 * @ ORM\OneToOne(targetEntity="activites")
	 * @ ORM\JoinColumns({
	 *   @ ORM\JoinColumn(name="iid_activite", referencedColumnName="iid_activite")
	 * })
	 */
	private $iidActivite;

	public function setIidactivite($piIdactivite) {
		$this->iidActivite = $piIdactivite;
		return $this;
	}

	public function getIidactivite() {
		return $this->iidActivite;
	}

	/**
	 * @ORM\Column(name="iactibase", type="integer", nullable=true)
	 * @var integer
	 * @ var \actibases
	 *
	 * @ ORM\ManyToOne(targetEntity="actibases")
	 * @ ORM\JoinColumns({
	 *   @ ORM\JoinColumn(name="iactibase", referencedColumnName="iid_actibase")
	 * })
	 */
	private $iactibase;

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cactibase", type="string", length=254, nullable=true)
	 */
	private $cactibase = '';

	public function setCactibase($pcActibase) {
		$this->cactibase = $pcActibase;
		return $this;
	}

	public function getCactibase() {
		return $this->cactibase;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="isemaine", type="integer", nullable=true)
	 */
	private $isemaine = '0';

	public function setIsemaine($piSemaine) {
		$this->isemaine = $piSemaine;
		return $this;
	}

	public function getIsemaine() {
		return $this->isemaine;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="musagers", type="text", nullable=true)
	 */
	private $musagers = '';

	public function setMusagers($pmUsagers) {
		$this->musagers = $pmUsagers;
		return $this;
	}

	public function getMusagers() {
		return $this->musagers;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="msalles", type="text", nullable=true)
	 */
	private $msalles = '';

	public function setMsalles($pmSalles) {
		$this->msalles = $pmSalles;
		return $this;
	}

	public function getMsalles() {
		return $this->msalles;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mintervenants", type="text", nullable=true)
	 */
	private $mintervenants = '';

	public function setMintervenants($pmIntervenants) {
		$this->mintervenants = $pmIntervenants;
		return $this;
	}

	public function getMintervenants() {
		return $this->mintervenants;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="metablissements", type="text", nullable=true)
	 */
	private $metablissements = '';

	public function setMetablissements($pmEtablissements) {
		$this->metablissements = $pmEtablissements;
		return $this;
	}

	public function getMetablissements() {
		return $this->metablissements;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mgroupes", type="text", nullable=true)
	 */
	private $mgroupes = '';

	public function setMgroupes($pmGroupes) {
		$this->mgroupes = $pmGroupes;
		return $this;
	}

	public function getMgroupes() {
		return $this->mgroupes;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmaj = '0001-01-01';
	private $tmaj = null;

	public function setTmaj($ptMaj) {
		$this->tmaj = $ptMaj;
		return $this;
	}

	public function getTmaj() {
		return $this->tmaj;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj_acti", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmajActi = '0001-01-01';
	private $tmajActi = null;

	public function setTmajacti($ptMajacti) {
		$this->tmajActi = $ptMajacti;
		return $this;
	}

	public function getTmajacti() {
		return $this->tmajActi;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lajour", type="boolean", nullable=true)
	 */
	private $lajour = false;

	public function setLajour($plAjour) {
		$this->lajour = $plAjour;
		return $this;
	}

	public function getLajour() {
		return $this->lajour;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="crythme", type="string", length=6, nullable=true)
	 */
	private $crythme = 'E';

	public function setCrythme($pcRythme) {
		$this->crythme = $pcRythme;
		return $this;
	}

	public function getCrythme() {
		return $this->crythme;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mremarque", type="text", nullable=true)
	 */
	private $mremarque = '';

	public function setMremarque($pmRemarque) {
		$this->mremarque = $pmRemarque;
		return $this;
	}

	public function getMremarque() {
		return $this->mremarque;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mlstseances", type="text", nullable=true)
	 */
	private $mlstseances = '';

	public function setMlstseances($pmLstseances) {
		$this->mlstseances = $pmLstseances;
		return $this;
	}

	public function getMlstseances() {
		return $this->mlstseances;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lgenerique", type="boolean", nullable=true)
	 */
	private $lgenerique = false;

	public function setLgenerique($plGenerique) {
		$this->lgenerique = $plGenerique;
		return $this;
	}

	public function getLgenerique() {
		return $this->lgenerique;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mlibelle", type="text", nullable=true)
	 */
	private $mlibelle = '';

	public function setMlibelle($pmLibelle) {
		$this->mlibelle = $pmLibelle;
		return $this;
	}

	public function getMlibelle() {
		return $this->mlibelle;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="isemaine_fin", type="integer", nullable=true)
	 */
	private $isemaineFin = '0';

	public function setIsemainefin($piSemainefin) {
		$this->isemaineFin = $piSemainefin;
		return $this;
	}

	public function getIsemainefin() {
		return $this->isemaineFin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icoulactibase", type="integer", nullable=true)
	 */
	private $icoulactibase = '0';

	public function setIcoulactibase($piCoulactibase) {
		$this->icoulactibase = $piCoulactibase;
		return $this;
	}

	public function getIcoulactibase() {
		return $this->icoulactibase;
	}

}
