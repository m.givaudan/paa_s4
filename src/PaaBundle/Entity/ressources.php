<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * groupes
 *
 * @ORM\Table(name="paa.ressources")
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\ressourcesRepository")     
 */
class ressources {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=false)
	 * @ORM\Id
	 * @ ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.ressources_iid_ressource_seq", allocationSize=1, initialValue=1)
	 * @ ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
	private $iIdRes = '';

	public function setiIdRes($piId) {
		$this->iIdRes = $piId;
		return $this;
	}

	public function getiIdRes() {
		return $this->iIdRes;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iIdRes;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="$ctype_res", type="string", length=50, nullable=true)
	 * @ORM\Id
	 */
	private $cTypeRes = '';

	public function setCTypeRes($pctype) {
		$this->cTypeRes = $pctype;
		return $this;
	}

	public function getCTypeRes() {
		return $this->cTypeRes;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cNom = '';

	public function setCNom($pcNom) {
		$this->cNom = $pcNom;
		return $this;
	}

	public function getCNom() {
		return $this->cNom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=50, nullable=true)
	 */
	private $cNomCourt = '';

	public function setCNomCourt($pcNomcourt) {
		$this->cNomCourt = $pcNomcourt;
		return $this;
	}

	public function getCNomCourt() {
		return $this->cNomCourt;
	}

}
