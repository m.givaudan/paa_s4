<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * conflits
 *
 * @ORM\Table(name="paa.conflits", indexes={@ORM\Index(name="conflits_cr_debut", columns={"tdebut"}), @ORM\Index(name="conflits_seance1", columns={"iseance1"}), @ORM\Index(name="conflits_seance2", columns={"iseance2"})})
 * @ORM\Entity
 */
class conflits {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.conflits_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid = 'Conflits';

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="icouleur", type="decimal", precision=9, scale=0, nullable=true)
	 */
	private $icouleur = '0.0';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="caction", type="string", length=1, nullable=true)
	 */
	private $caction = '';

	public function setCaction($pcAction) {
		$this->caction = $pcAction;
		return $this;
	}

	public function getCaction() {
		return $this->caction;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lignorer", type="boolean", nullable=true)
	 */
	private $lignorer = false;

	public function setLignorer($plIgnorer) {
		$this->lignorer = $plIgnorer;
		return $this;
	}

	public function getLignorer() {
		return $this->lignorer;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nduree", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $nduree = '0.0';

	public function setNduree($pnDuree) {
		$this->nduree = $pnDuree;
		return $this;
	}

	public function getNduree() {
		return $this->nduree;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebut", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tdebut = '0001-01-01';
	private $tdebut = null;

	public function setTdebut($ptDebut) {
		$this->tdebut = $ptDebut;
		return $this;
	}

	public function getTdebut() {
		return $this->tdebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tfin", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tfin = '0001-01-01';
	private $tfin = Null;

	public function setTfin($ptFin) {
		$this->tfin = $ptFin;
		return $this;
	}

	public function getTfin() {
		return $this->tfin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iacti1", type="integer", nullable=true)
	 */
	private $iacti1 = '0';

	public function setIacti1($piActi1) {
		$this->iacti1 = $piActi1;
		return $this;
	}

	public function getIacti1() {
		return $this->iacti1;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iacti2", type="integer", nullable=true)
	 */
	private $iacti2 = '0';

	public function setIacti2($piActi2) {
		$this->iacti2 = $piActi2;
		return $this;
	}

	public function getIacti2() {
		return $this->iacti2;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconflitdugroupe", type="boolean", nullable=true)
	 */
	private $lconflitdugroupe = false;

	public function setLconflitdugroupe($plConflitdugroupe) {
		$this->lconflitdugroupe = $plConflitdugroupe;
		return $this;
	}

	public function getLconflitdugroupe() {
		return $this->lconflitdugroupe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconflitindividugroupe", type="boolean", nullable=true)
	 */
	private $lconflitindividugroupe = false;

	public function setLconflitindividugroupe($plConflitindividugroupe) {
		$this->lconflitindividugroupe = $plConflitindividugroupe;
		return $this;
	}

	public function getLconflitindividugroupe() {
		return $this->lconflitindividugroupe;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance1", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseance1;

	public function setIseance1($piSeance1) {
		$this->iseance1 = $piSeance1;
		return $this;
	}

	public function getIseance1() {
		return $this->iseance1;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance2", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseance2;

	public function setIseance2($piSeance2) {
		$this->iseance2 = $piSeance2;
		return $this;
	}

	public function getIseance2() {
		return $this->iseance2;
	}

}
