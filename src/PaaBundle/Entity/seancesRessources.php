<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesRessources
 *
 * @ORM\Table(name="paa.seances_ressources", uniqueConstraints={@ORM\UniqueConstraint(name="u_seances_ressources_iseance&ctype_res&iid_res", columns={"ctype_res", "iid_res", "iseance"})}, indexes={@ORM\Index(name="seances_ressources_type_res", columns={"ctype_res"}), @ORM\Index(name="seances_ressources_sr_seance", columns={"iseance"}), @ORM\Index(name="seances_ressources_sr_annule", columns={"lannule"}), @ORM\Index(name="seances_ressources_id_res", columns={"iid_res"}), @ORM\Index(name="IDX_9F3BD0B27676EAA", columns={"iremplace"}), @ORM\Index(name="IDX_9F3BD0BCB1FB50E", columns={"iremplace_par"})})
  .* @ORM\Entity(repositoryClass="App\PaaBundle\Repository\seancesRessourcesRepository")
 */
class seancesRessources {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_seance_ressource", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.seances_ressources_iid_seance_ressource_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSeanceRessource = 'seances_ressources';

	public function setIidseanceressource($piIdseanceressource) {
		$this->iidSeanceRessource = $piIdseanceressource;
		return $this;
	}

	public function getIidseanceressource() {
		return $this->iidSeanceRessource;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSeanceRessource;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lannule", type="boolean", nullable=true)
	 */
	private $lannule = false;

	public function setLannule($plAnnule) {
		$this->lannule = $plAnnule;
		return $this;
	}

	public function getLannule() {
		return $this->lannule;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="minfos", type="text", nullable=true)
	 */
	private $minfos = '';

	public function setMinfos($pmInfos) {
		$this->minfos = $pmInfos;
		return $this;
	}

	public function getMinfos() {
		return $this->minfos;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdiffusion", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tdiffusion = '0001-01-01';
	private $tdiffusion = null;

	public function setTdiffusion($ptDiffusion) {
		$this->tdiffusion = $ptDiffusion;
		return $this;
	}

	public function getTdiffusion() {
		return $this->tdiffusion;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmaj = '0001-01-01';
	private $tmaj = null;

	public function setTmaj($ptMaj) {
		$this->tmaj = $ptMaj;
		return $this;
	}

	public function getTmaj() {
		return $this->tmaj;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mlbldiffusion", type="text", nullable=true)
	 */
	private $mlbldiffusion = '';

	public function setMlbldiffusion($pmLbldiffusion) {
		$this->mlbldiffusion = $pmLbldiffusion;
		return $this;
	}

	public function getMlbldiffusion() {
		return $this->mlbldiffusion;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lanulcreat", type="boolean", nullable=true)
	 */
	private $lanulcreat = false;

	public function setLanulcreat($plAnulcreat) {
		$this->lanulcreat = $plAnulcreat;
		return $this;
	}

	public function getLanulcreat() {
		return $this->lanulcreat;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lplaceauto", type="boolean", nullable=true)
	 */
	private $lplaceauto = false;

	public function setLplaceauto($plPlaceauto) {
		$this->lplaceauto = $plPlaceauto;
		return $this;
	}

	public function getLplaceauto() {
		return $this->lplaceauto;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dseance", type="date", nullable=true)
	 */
	private $dseance;

	public function setDseance($pdSeance) {
		$this->dseance = $pdSeance;
		return $this;
	}

	public function getDseance() {
		return $this->dseance;
	}

	/**
	 * @var \intervenants
	 *
	 * @ORM\ManyToOne(targetEntity="intervenants")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iremplace", referencedColumnName="iid_intervenant")
	 * })
	 */
	private $iremplace;

	public function setIremplace($piRemplace) {
		$this->iremplace = $piRemplace;
		return $this;
	}

	public function getIremplace() {
		return $this->iremplace;
	}

	/**
	 * @var \intervenants
	 *
	 * @ORM\ManyToOne(targetEntity="intervenants")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iremplace_par", referencedColumnName="iid_intervenant")
	 * })
	 */
	private $iremplacePar;

	public function setIremplacepar($piRemplacepar) {
		$this->iremplacePar = $piRemplacepar;
		return $this;
	}

	public function getIremplacepar() {
		return $this->iremplacePar;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseance;

	public function setIseance($piSeance) {
		$this->iseance = $piSeance;
		return $this;
	}

	public function getIseance() {
		return $this->iseance;
	}

}
