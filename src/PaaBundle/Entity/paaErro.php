<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * paaErro
 *
 * @ORM\Table(name="paa.paa_erro")
 * @ORM\Entity
 */
class paaErro {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="err_no", type="decimal", precision=5, scale=0, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 */
	private $errNo = '0.0';

	public function setErrno($peRrno) {
		$this->errNo = $peRrno;
		return $this;
	}

	public function getErrno() {
		return $this->errNo;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="err_desc", type="string", length=254, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 */
	private $errDesc = '';

	public function setErrdesc($peRrdesc) {
		$this->errDesc = $peRrdesc;
		return $this;
	}

	public function getErrdesc() {
		return $this->errDesc;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="err_prg", type="string", length=254, nullable=true)
	 */
	private $errPrg = '';

	public function setErrprg($peRrprg) {
		$this->errPrg = $peRrprg;
		return $this;
	}

	public function getErrprg() {
		return $this->errPrg;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="err_vers", type="string", length=254, nullable=true)
	 */
	private $errVers = '';

	public function setErrvers($peRrvers) {
		$this->errVers = $peRrvers;
		return $this;
	}

	public function getErrvers() {
		return $this->errVers;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="err_dat1", type="datetime", nullable=true)
	 */
	private $errDat1 = '0001-01-01';

	public function setErrdat1($peRrdat1) {
		$this->errDat1 = $peRrdat1;
		return $this;
	}

	public function getErrdat1() {
		return $this->errDat1;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="err_dat2", type="datetime", nullable=true)
	 */
	private $errDat2 = '0001-01-01';

	public function setErrdat2($peRrdat2) {
		$this->errDat2 = $peRrdat2;
		return $this;
	}

	public function getErrdat2() {
		return $this->errDat2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="err_deta", type="text", nullable=true)
	 */
	private $errDeta = '';

	public function setErrdeta($peRrdeta) {
		$this->errDeta = $peRrdeta;
		return $this;
	}

	public function getErrdeta() {
		return $this->errDeta;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="err_nb", type="decimal", precision=5, scale=0, nullable=true)
	 */
	private $errNb = '0.0';

	public function setErrnb($peRrnb) {
		$this->errNb = $peRrnb;
		return $this;
	}

	public function getErrnb() {
		return $this->errNb;
	}

}
