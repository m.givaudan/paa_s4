<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tournees
 *
 * @ORM\Table(name="paa.tournees", uniqueConstraints={@ORM\UniqueConstraint(name="u_tournees_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="tournees_ste_taxi", columns={"isociete"})})
 * @ORM\Entity
 */
class tournees {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_tournee", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tournees_iid_tournee_seq", allocationSize=1, initialValue=1)
	 */
	private $iidTournee = 'tournees';

	public function setIidtournee($piIdtournee) {
		$this->iidTournee = $piIdtournee;
		return $this;
	}

	public function getIidtournee() {
		return $this->iidTournee;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ncoutkm", type="decimal", precision=5, scale=3, nullable=true)
	 */
	private $ncoutkm = '0.0';

	public function setNcoutkm($pnCoutkm) {
		$this->ncoutkm = $pnCoutkm;
		return $this;
	}

	public function getNcoutkm() {
		return $this->ncoutkm;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lactive", type="boolean", nullable=true)
	 */
	private $lactive = true;

	public function setLactive($plActive) {
		$this->lactive = $plActive;
		return $this;
	}

	public function getLactive() {
		return $this->lactive;
	}

	/**
	 * @var \trspSocietes
	 *
	 * @ORM\ManyToOne(targetEntity="trspSocietes")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="isociete", referencedColumnName="iid_societe")
	 * })
	 */
	private $isociete;

	public function setIsociete($piSociete) {
		$this->isociete = $piSociete;
		return $this;
	}

	public function getIsociete() {
		return $this->isociete;
	}

}
