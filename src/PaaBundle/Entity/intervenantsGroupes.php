<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * intervenantsGroupes
 *
 * @ORM\Table(name="paa.intervenants_groupes", uniqueConstraints={@ORM\UniqueConstraint(name="u_intervenants_groupes_iintervenant&igroupe", columns={"igroupe", "iintervenant"})}, indexes={@ORM\Index(name="intervenants_groupes_intervenan", columns={"iintervenant"}), @ORM\Index(name="intervenants_groupes_ig_groupe", columns={"igroupe"})})
 * @ORM\Entity
 */
class intervenantsGroupes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_intervenant_groupe", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.intervenants_groupes_iid_intervenant_groupe_seq", allocationSize=1, initialValue=1)
	 */
	private $iidIntervenantGroupe = 'intervenants_groupes';

	public function setIidintervenantgroupe($piIdintervenantgroupe) {
		$this->iidIntervenantGroupe = $piIdintervenantgroupe;
		return $this;
	}

	public function getIidintervenantgroupe() {
		return $this->iidIntervenantGroupe;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidIntervenantGroupe;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \groupes
	 *
	 * @ORM\ManyToOne(targetEntity="groupes")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="igroupe", referencedColumnName="iid_groupe")
	 * })
	 */
	private $igroupe;

	public function setIgroupe($piGroupe) {
		$this->igroupe = $piGroupe;
		return $this;
	}

	public function getIgroupe() {
		return $this->igroupe;
	}

	/**
	 * @var \intervenants
	 *
	 * @ORM\ManyToOne(targetEntity="intervenants")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iintervenant", referencedColumnName="iid_intervenant")
	 * })
	 */
	private $iintervenant;

	public function setIintervenant($piIntervenant) {
		$this->iintervenant = $piIntervenant;
		return $this;
	}

	public function getIintervenant() {
		return $this->iintervenant;
	}

}
