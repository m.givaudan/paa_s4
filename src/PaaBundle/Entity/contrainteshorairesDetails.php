<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * contrainteshorairesDetails
 *
 * @ORM\Table(name="paa.contrainteshoraires_details", indexes={@ORM\Index(name="contrainteshoraires_details_ihor", columns={"icontraintehoraire"})})
 * @ORM\Entity
 */
class contrainteshorairesDetails {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.contrainteshoraires_details_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icontraintehoraire", type="integer", nullable=true)
	 */
	private $icontraintehoraire = '0';

	public function setIcontraintehoraire($piContraintehoraire) {
		$this->icontraintehoraire = $piContraintehoraire;
		return $this;
	}

	public function getIcontraintehoraire() {
		return $this->icontraintehoraire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ndebut", type="decimal", precision=6, scale=3, nullable=true)
	 */
	private $ndebut = '0.0';

	public function setNdebut($pnDebut) {
		$this->ndebut = $pnDebut;
		return $this;
	}

	public function getNdebut() {
		return $this->ndebut;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nfin", type="decimal", precision=6, scale=3, nullable=true)
	 */
	private $nfin = '0.0';

	public function setNfin($pnFin) {
		$this->nfin = $pnFin;
		return $this;
	}

	public function getNfin() {
		return $this->nfin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="inbtot", type="integer", nullable=true)
	 */
	private $inbtot;

	public function setInbtot($piNbtot) {
		$this->inbtot = $piNbtot;
		return $this;
	}

	public function getInbtot() {
		return $this->inbtot;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="inbhommes", type="integer", nullable=true)
	 */
	private $inbhommes;

	public function setInbhommes($piNbhommes) {
		$this->inbhommes = $piNbhommes;
		return $this;
	}

	public function getInbhommes() {
		return $this->inbhommes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="inbfemmes", type="integer", nullable=true)
	 */
	private $inbfemmes;

	public function setInbfemmes($piNbfemmes) {
		$this->inbfemmes = $piNbfemmes;
		return $this;
	}

	public function getInbfemmes() {
		return $this->inbfemmes;
	}

}
