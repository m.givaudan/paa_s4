<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tourneesVariantes
 *
 * @ORM\Table(name="paa.tournees_variantes", uniqueConstraints={@ORM\UniqueConstraint(name="u_tournees_variantes_itournee&nvariante", columns={"itournee", "nvariante"})}, indexes={@ORM\Index(name="tournees_variantes_tv_var", columns={"nvariante"}), @ORM\Index(name="tournees_variantes_tv_tourne", columns={"itournee"})})
 * @ORM\Entity
 */
class tourneesVariantes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_tournee_variante", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tournees_variantes_iid_tournee_variante_seq", allocationSize=1, initialValue=1)
	 */
	private $iidTourneeVariante = 'tournees_variantes';

	public function setIidtourneevariante($piIdtourneevariante) {
		$this->iidTourneeVariante = $piIdtourneevariante;
		return $this;
	}

	public function getIidtourneevariante() {
		return $this->iidTourneeVariante;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidTourneeVariante;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nvariante", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $nvariante = '0.0';

	public function setNvariante($pnVariante) {
		$this->nvariante = $pnVariante;
		return $this;
	}

	public function getNvariante() {
		return $this->nvariante;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ndistance", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $ndistance = '0.0';

	public function setNdistance($pnDistance) {
		$this->ndistance = $pnDistance;
		return $this;
	}

	public function getNdistance() {
		return $this->ndistance;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ichauffeur", type="integer", nullable=true)
	 */
	private $ichauffeur;

	public function setIchauffeur($piChauffeur) {
		$this->ichauffeur = $piChauffeur;
		return $this;
	}

	public function getIchauffeur() {
		return $this->ichauffeur;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ivehicule", type="integer", nullable=true)
	 */
	private $ivehicule;

	public function setIvehicule($piVehicule) {
		$this->ivehicule = $piVehicule;
		return $this;
	}

	public function getIvehicule() {
		return $this->ivehicule;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ntpsparcours", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $ntpsparcours = '0.0';

	public function setNtpsparcours($pnTpsparcours) {
		$this->ntpsparcours = $pnTpsparcours;
		return $this;
	}

	public function getNtpsparcours() {
		return $this->ntpsparcours;
	}

	/**
	 * @var \tournees
	 *
	 * @ORM\ManyToOne(targetEntity="tournees")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="itournee", referencedColumnName="iid_tournee")
	 * })
	 */
	private $itournee;

	public function setItournee($piTournee) {
		$this->itournee = $piTournee;
		return $this;
	}

	public function getItournee() {
		return $this->itournee;
	}

}
