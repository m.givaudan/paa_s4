<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * intervenants
 *
 * @ORM\Table(name="paa.intervenants", uniqueConstraints={@ORM\UniqueConstraint(name="u_intervenants_cinitiales", columns={"cinitiales"}), @ORM\UniqueConstraint(name="u_intervenants_cnomcourt", columns={"cnomcourt"})}, indexes={@ORM\Index(name="intervenants_nom_interv", columns={"cnom"}), @ORM\Index(name="intervenants_i_spe", columns={"ispecialite"}), @ORM\Index(name="intervenants_remplac", columns={"remplacant"}), @ORM\Index(name="intervenants_ext", columns={"lexterieur"})})
 * @ ORM\Entity // LG 20180629 old
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\intervenantsRepository")     
 */
class intervenants {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->iacti = new \Doctrine\Common\Collections\ArrayCollection();
		// AV 07/03/2019 début
		$this->listeContrats = new \Doctrine\Common\Collections\ArrayCollection();
		// AV 07/03/2019 fin

		$this->activites = new \Doctrine\Common\Collections\ArrayCollection();
	}

	public function __toString() {
		return $this->cnomcourt;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_intervenant", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.intervenants_iid_intervenant_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
	private $iidIntervenant = 0;

	public function setIidintervenant($piIdintervenant) {
		$this->iidIntervenant = $piIdintervenant;
		return $this;
	}

	public function getIidintervenant() {
		return $this->iidIntervenant;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidIntervenant;
	}

	// AV 04/03/2019 fin
	// AV 07/03/2019 début

	/**
	 * @ORM\OneToMany(targetEntity="intervenantsContrats", mappedBy="iintervenant")
	 */
	private $listeContrats;

	/**
	 * Get listeContrats
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getlisteContrats() {
		return $this->listeContrats;
	}

	// AV 07/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

//    AV 06/03/2019 début
//      @var integer
//     
//      @ORM\Column(name="ispecialite", type="integer", nullable=true)

	/**
	 * @var \specialites
	 * 
	 * @ORM\ManyToOne(targetEntity="specialites")
	 * @ORM\JoinColumn(name="ispecialite", referencedColumnName="iid_specialite")
	 * 
	 */
	private $ispecialite = '0';

	public function setIspecialite($piSpecialite) {
		$this->ispecialite = $piSpecialite;
		return $this;
	}

	public function getIspecialite() {
		return $this->ispecialite;
	}

//    AV 06/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmdp", type="string", length=10, nullable=true)
	 */
	private $cmdp = '';

	public function setCmdp($pcMdp) {
		$this->cmdp = $pcMdp;
		return $this;
	}

	public function getCmdp() {
		return $this->cmdp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cinitiales", type="string", length=10, nullable=true)
	 */
	private $cinitiales = '';

	public function setCinitiales($pcInitiales) {
		$this->cinitiales = $pcInitiales;
		return $this;
	}

	public function getCinitiales() {
		return $this->cinitiales;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="itype", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $itype = '1';

	public function setItype($piType) {
		$this->itype = $piType;
		return $this;
	}

	public function getItype() {
		return $this->itype;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse1", type="string", length=254, nullable=true)
	 */
	private $cadresse1 = '';

	public function setCadresse1($pcAdresse1) {
		$this->cadresse1 = $pcAdresse1;
		return $this;
	}

	public function getCadresse1() {
		return $this->cadresse1;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse2", type="string", length=254, nullable=true)
	 */
	private $cadresse2 = '';

	public function setCadresse2($pcAdresse2) {
		$this->cadresse2 = $pcAdresse2;
		return $this;
	}

	public function getCadresse2() {
		return $this->cadresse2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccp", type="string", length=254, nullable=true)
	 */
	private $ccp = '';

	public function setCcp($pcCp) {
		$this->ccp = $pcCp;
		return $this;
	}

	public function getCcp() {
		return $this->ccp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cville", type="string", length=254, nullable=true)
	 */
	private $cville = '';

	public function setCville($pcVille) {
		$this->cville = $pcVille;
		return $this;
	}

	public function getCville() {
		return $this->cville;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctel", type="string", length=30, nullable=true)
	 */
	private $ctel = '';

	public function setCtel($pcTel) {
		$this->ctel = $pcTel;
		return $this;
	}

	public function getCtel() {
		return $this->ctel;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cport", type="string", length=30, nullable=true)
	 */
	private $cport = '';

	public function setCport($pcPort) {
		$this->cport = $pcPort;
		return $this;
	}

	public function getCport() {
		return $this->cport;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmel", type="string", length=50, nullable=true)
	 */
	private $cmel = '';

	public function setCmel($pcMel) {
		$this->cmel = $pcMel;
		return $this;
	}

	public function getCmel() {
		return $this->cmel;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cprenom", type="string", length=50, nullable=true)
	 */
	private $cprenom = '';

	public function setCprenom($pcPrenom) {
		$this->cprenom = $pcPrenom;
		return $this;
	}

	public function getCprenom() {
		return $this->cprenom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=50, nullable=true)
	 */
	private $cnomcourt = '';

	public function setCnomcourt($pcNomcourt) {
		$this->cnomcourt = $pcNomcourt;
		return $this;
	}

	public function getCnomcourt() {
		return $this->cnomcourt;
	}

	//    AV 06/03/2019 début
//      @var integer
//     
//      @ORM\Column(name="iservice", type="integer", nullable=true)

	/**
	 * @var \services
	 * 
	 * @ORM\ManyToOne(targetEntity="services")
	 * @ORM\JoinColumn(name="iservice", referencedColumnName="iid_service")
	 * 
	 */
	private $iservice = '0';

	public function setIservice($piService) {
		$this->iservice = $piService;
		return $this;
	}

	public function getIservice() {
		return $this->iservice;
	}

	//    AV 06/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="soldehrs", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $soldehrs = '0.0';

	public function setSoldehrs($psOldehrs) {
		$this->soldehrs = $psOldehrs;
		return $this;
	}

	public function getSoldehrs() {
		return $this->soldehrs;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lexterieur", type="boolean", nullable=true)
	 */
	private $lexterieur = false;

	public function setLexterieur($plExterieur) {
		$this->lexterieur = $plExterieur;
		return $this;
	}

	public function getLexterieur() {
		return $this->lexterieur;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="remplacant", type="boolean", nullable=true)
	 */
	private $remplacant = false;

	public function setRemplacant($prEmplacant) {
		$this->remplacant = $prEmplacant;
		return $this;
	}

	public function getRemplacant() {
		return $this->remplacant;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrepertoire", type="text", nullable=true)
	 */
	private $mrepertoire = '';

	public function setMrepertoire($pmRepertoire) {
		$this->mrepertoire = $pmRepertoire;
		return $this;
	}

	public function getMrepertoire() {
		return $this->mrepertoire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="isemcycle", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $isemcycle;

	public function setIsemcycle($piSemcycle) {
		$this->isemcycle = $piSemcycle;
		return $this;
	}

	public function getIsemcycle() {
		return $this->isemcycle;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrythme", type="text", nullable=true)
	 */
	private $mrythme = '';

	public function setMrythme($pmRythme) {
		$this->mrythme = $pmRythme;
		return $this;
	}

	public function getMrythme() {
		return $this->mrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idebutrythme", type="integer", nullable=true)
	 */
	private $idebutrythme = '0';

	public function setIdebutrythme($piDebutrythme) {
		$this->idebutrythme = $piDebutrythme;
		return $this;
	}

	public function getIdebutrythme() {
		return $this->idebutrythme;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroitsr", type="integer", nullable=true)
	 */
	private $idroitsr = '1569615';

	public function setIdroitsr($piDroitsr) {
		$this->idroitsr = $piDroitsr;
		return $this;
	}

	public function getIdroitsr() {
		return $this->idroitsr;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroitsw", type="integer", nullable=true)
	 */
	private $idroitsw = '0';

	public function setIdroitsw($piDroitsw) {
		$this->idroitsw = $piDroitsw;
		return $this;
	}

	public function getIdroitsw() {
		return $this->idroitsw;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dnaissance", type="date", nullable=true)
	 */
// LG 20200811 old	private $dnaissance = '0001-01-01';
	private $dnaissance = null ;

	public function setDnaissance($pdNaissance) {
		$this->dnaissance = $pdNaissance;
		return $this;
	}

	public function getDnaissance() {
		return $this->dnaissance;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnoss", type="string", length=15, nullable=true)
	 */
	private $cnoss = '';

	public function setCnoss($pcNoss) {
		$this->cnoss = $pcNoss;
		return $this;
	}

	public function getCnoss() {
		return $this->cnoss;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate1", type="date", nullable=true)
	 */
	private $ddate1;

	public function setDdate1($pdDate1) {
		$this->ddate1 = $pdDate1;
		return $this;
	}

	public function getDdate1() {
		return $this->ddate1;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate2", type="date", nullable=true)
	 */
	private $ddate2;

	public function setDdate2($pdDate2) {
		$this->ddate2 = $pdDate2;
		return $this;
	}

	public function getDdate2() {
		return $this->ddate2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccivilite", type="string", length=20, nullable=true)
	 */
	private $ccivilite = '';

	public function setCcivilite($pcCivilite) {
		$this->ccivilite = $pcCivilite;
		return $this;
	}

	public function getCcivilite() {
		return $this->ccivilite;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomjeunefille", type="string", length=50, nullable=true)
	 */
	private $cnomjeunefille = '';

	public function setCnomjeunefille($pcNomjeunefille) {
		$this->cnomjeunefille = $pcNomjeunefille;
		return $this;
	}

	public function getCnomjeunefille() {
		return $this->cnomjeunefille;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mwinlogin", type="text", nullable=true)
	 */
	private $mwinlogin = '';

	public function setMwinlogin($pmWinlogin) {
		$this->mwinlogin = $pmWinlogin;
		return $this;
	}

	public function getMwinlogin() {
		return $this->mwinlogin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clibre1", type="string", length=50, nullable=true)
	 */
	private $clibre1 = '';

	public function setClibre1($pcLibre1) {
		$this->clibre1 = $pcLibre1;
		return $this;
	}

	public function getClibre1() {
		return $this->clibre1;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clibre2", type="string", length=50, nullable=true)
	 */
	private $clibre2 = '';

	public function setClibre2($pcLibre2) {
		$this->clibre2 = $pcLibre2;
		return $this;
	}

	public function getClibre2() {
		return $this->clibre2;
	}

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="activites", mappedBy="iintervenant")
//     */
//    private $iacti;
//    public function setActivités($piActi){
//        $this->iacti = $piActi;
//        return $this;
//    }
//    public function getActivités() {
//        return $this->iacti;
//    }

	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\activites",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_salle",nullable=false)},
	 *   inverseJoinColumns = {@ORM\JoinColumn(name="iacti", referencedColumnName="iid_intervenant")}
	 * )iid_activite
	 */
	private $activites;

	/**
	 * @return Collection|acivite[]
	 */
	public function getactivites(): \Doctrine\Common\Collections\ArrayCollection {
		return $this->activites;
	}

	public function setactivites() {
		if (!$this->activites->contains($activite)) {
			$this->activites[] = $activite;
			$activite->setintervenants($this);
		}

		return $this;
	}

}
