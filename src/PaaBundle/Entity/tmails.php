<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tmails
 *
 * @ORM\Table(name="paa.tmails", indexes={@ORM\Index(name="IDX_E8D10222CA5E9C4D", columns={"ifrom"}), @ORM\Index(name="IDX_E8D1022242E540A7", columns={"imailsource"})})
 * @ORM\Entity
 */
class tmails {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_mail", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tmails_iid_mail_seq", allocationSize=1, initialValue=1)
	 */
	private $iidMail;

	public function setIidmail($piIdmail) {
		$this->iidMail = $piIdmail;
		return $this;
	}

	public function getIidmail() {
		return $this->iidMail;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidMail;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mto", type="text", nullable=true)
	 */
	private $mto = '';

	public function setMto($pmTo) {
		$this->mto = $pmTo;
		return $this;
	}

	public function getMto() {
		return $this->mto;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mto_lstids", type="text", nullable=true)
	 */
	private $mtoLstids = '';

	public function setMtolstids($pmTolstids) {
		$this->mtoLstids = $pmTolstids;
		return $this;
	}

	public function getMtolstids() {
		return $this->mtoLstids;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcc", type="text", nullable=true)
	 */
	private $mcc = '';

	public function setMcc($pmCc) {
		$this->mcc = $pmCc;
		return $this;
	}

	public function getMcc() {
		return $this->mcc;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcc_lstids", type="text", nullable=true)
	 */
	private $mccLstids = '';

	public function setMcclstids($pmCclstids) {
		$this->mccLstids = $pmCclstids;
		return $this;
	}

	public function getMcclstids() {
		return $this->mccLstids;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mbcc", type="text", nullable=true)
	 */
	private $mbcc = '';

	public function setMbcc($pmBcc) {
		$this->mbcc = $pmBcc;
		return $this;
	}

	public function getMbcc() {
		return $this->mbcc;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mbcc_lstids", type="text", nullable=true)
	 */
	private $mbccLstids = '';

	public function setMbcclstids($pmBcclstids) {
		$this->mbccLstids = $pmBcclstids;
		return $this;
	}

	public function getMbcclstids() {
		return $this->mbccLstids;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mfiles", type="text", nullable=true)
	 */
	private $mfiles = '';

	public function setMfiles($pmFiles) {
		$this->mfiles = $pmFiles;
		return $this;
	}

	public function getMfiles() {
		return $this->mfiles;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cobject", type="string", length=254, nullable=true)
	 */
	private $cobject = '';

	public function setCobject($pcObject) {
		$this->cobject = $pcObject;
		return $this;
	}

	public function getCobject() {
		return $this->cobject;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mbody", type="text", nullable=true)
	 */
	private $mbody = '';

	public function setMbody($pmBody) {
		$this->mbody = $pmBody;
		return $this;
	}

	public function getMbody() {
		return $this->mbody;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tsent", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tsent = '0001-01-01';
	private $tsent = null;

	public function setTsent($ptSent) {
		$this->tsent = $ptSent;
		return $this;
	}

	public function getTsent() {
		return $this->tsent;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lsentbymail", type="boolean", nullable=true)
	 */
	private $lsentbymail = false;

	public function setLsentbymail($plSentbymail) {
		$this->lsentbymail = $plSentbymail;
		return $this;
	}

	public function getLsentbymail() {
		return $this->lsentbymail;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tread", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tread = '0001-01-01';
	private $tread = null;

	public function setTread($ptRead) {
		$this->tread = $ptRead;
		return $this;
	}

	public function getTread() {
		return $this->tread;
	}

	/**
	 * @var \intervenants
	 *
	 * @ORM\ManyToOne(targetEntity="intervenants")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ifrom", referencedColumnName="iid_intervenant")
	 * })
	 */
	private $ifrom;

	public function setIfrom($piFrom) {
		$this->ifrom = $piFrom;
		return $this;
	}

	public function getIfrom() {
		return $this->ifrom;
	}

	/**
	 * @var \tmails
	 *
	 * @ORM\ManyToOne(targetEntity="tmails")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="imailsource", referencedColumnName="iid_mail")
	 * })
	 */
	private $imailsource;

	public function setImailsource($piMailsource) {
		$this->imailsource = $piMailsource;
		return $this;
	}

	public function getImailsource() {
		return $this->imailsource;
	}

}
