<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * planstypes
 *
 * @ORM\Table(name="paa.planstypes", uniqueConstraints={@ORM\UniqueConstraint(name="u_planstypes_ctype_res&iid_res&iplantype", columns={"ctype_res", "iid_res", "iplantype"})}, indexes={@ORM\Index(name="planstypes_plantype", columns={"iplantype"}), @ORM\Index(name="planstypes_id_res", columns={"iid_res"})})
 * @ORM\Entity
 */
class planstypes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_pt", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.planstypes_iid_pt_seq", allocationSize=1, initialValue=1)
	 */
	private $iidPt = 'PLANSTYPES';

	public function setIidpt($piIdpt) {
		$this->iidPt = $piIdpt;
		return $this;
	}

	public function getIidpt() {
		return $this->iidPt;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidPt;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="iplantype", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $iplantype = '0.0';

	public function setIplantype($piPlantype) {
		$this->iplantype = $piPlantype;
		return $this;
	}

	public function getIplantype() {
		return $this->iplantype;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldft", type="boolean", nullable=true)
	 */
	private $ldft = false;

	public function setLdft($plDft) {
		$this->ldft = $plDft;
		return $this;
	}

	public function getLdft() {
		return $this->ldft;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="inodisp", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $inodisp = '0.0';

	public function setInodisp($piNodisp) {
		$this->inodisp = $piNodisp;
		return $this;
	}

	public function getInodisp() {
		return $this->inodisp;
	}

}
