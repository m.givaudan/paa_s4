<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * groupes
 *
 * @ORM\Table(name="paa.groupes", uniqueConstraints={@ORM\UniqueConstraint(name="u_groupes_cnomcourt", columns={"cnomcourt"}), @ORM\UniqueConstraint(name="u_groupes_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="groupes_nom_groupe", columns={"cnom"}), @ORM\Index(name="groupes_gpe_par", columns={"igroupeparent"}), @ORM\Index(name="IDX_C66BD075DCB91600", columns={"ietablissement"}), @ORM\Index(name="IDX_C66BD0758B0CB522", columns={"isallehabituelle"}), @ORM\Index(name="IDX_C66BD075D6772155", columns={"iservice"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\groupesRepository") 
 */
class groupes {

	public function __construct() {
		$this->activites = new \Doctrine\Common\Collections\ArrayCollection();
	}

	public function __toString() {
		return $this->cnom;
	}

	// <editor-fold defaultstate="collapsed" desc="Propriétés simples">
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_groupe", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.groupes_iid_groupe_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
	private $iidGroupe = NULL;

	public function setIidgroupe($piIdgroupe) {
		$this->iidGroupe = $piIdgroupe;
		return $this;
	}

	public function getIidgroupe() {
		return $this->iidGroupe;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidGroupe;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=50, nullable=true)
	 */
	private $cnomcourt = '';

	public function setCnomcourt($pcNomcourt) {
		$this->cnomcourt = $pcNomcourt;
		return $this;
	}

	public function getCnomcourt() {
		return $this->cnomcourt;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="igroupeparent", type="integer", nullable=true)
	 */
	private $igroupeparent = '0';

	public function setIgroupeparent($piGroupeparent) {
		$this->igroupeparent = $piGroupeparent;
		return $this;
	}

	public function getIgroupeparent() {
		return $this->igroupeparent;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type_groupe", type="string", length=50, nullable=true)
	 */
	private $typeGroupe = '';

	public function setTypegroupe($ptYpegroupe) {
		$this->typeGroupe = $ptYpegroupe;
		return $this;
	}

	public function getTypegroupe() {
		return $this->typeGroupe;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ieffectif", type="decimal", precision=3, scale=0, nullable=true)
	 */
	private $ieffectif;

	public function setIeffectif($piEffectif) {
		$this->ieffectif = $piEffectif;
		return $this;
	}

	public function getIeffectif() {
		return $this->ieffectif;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lpresconf", type="boolean", nullable=true)
	 */
	private $lpresconf = true;

	public function setLpresconf($plPresconf) {
		$this->lpresconf = $plPresconf;
		return $this;
	}

	public function getLpresconf() {
		return $this->lpresconf;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrepertoire", type="text", nullable=true)
	 */
	private $mrepertoire = '';

	public function setMrepertoire($pmRepertoire) {
		$this->mrepertoire = $pmRepertoire;
		return $this;
	}

	public function getMrepertoire() {
		return $this->mrepertoire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse1", type="string", length=254, nullable=true)
	 */
	private $cadresse1 = '';

	public function setCadresse1($pcAdresse1) {
		$this->cadresse1 = $pcAdresse1;
		return $this;
	}

	public function getCadresse1() {
		return $this->cadresse1;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse2", type="string", length=254, nullable=true)
	 */
	private $cadresse2 = '';

	public function setCadresse2($pcAdresse2) {
		$this->cadresse2 = $pcAdresse2;
		return $this;
	}

	public function getCadresse2() {
		return $this->cadresse2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccp", type="string", length=254, nullable=true)
	 */
	private $ccp = '';

	public function setCcp($pcCp) {
		$this->ccp = $pcCp;
		return $this;
	}

	public function getCcp() {
		return $this->ccp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cville", type="string", length=254, nullable=true)
	 */
	private $cville = '';

	public function setCville($pcVille) {
		$this->cville = $pcVille;
		return $this;
	}

	public function getCville() {
		return $this->cville;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctel", type="string", length=30, nullable=true)
	 */
	private $ctel = '';

	public function setCtel($pcTel) {
		$this->ctel = $pcTel;
		return $this;
	}

	public function getCtel() {
		return $this->ctel;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cport", type="string", length=30, nullable=true)
	 */
	private $cport = '';

	public function setCport($pcPort) {
		$this->cport = $pcPort;
		return $this;
	}

	public function getCport() {
		return $this->cport;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmel", type="string", length=50, nullable=true)
	 */
	private $cmel = '';

	public function setCmel($pcMel) {
		$this->cmel = $pcMel;
		return $this;
	}

	public function getCmel() {
		return $this->cmel;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrythme", type="text", nullable=true)
	 */
	private $mrythme = '';

	public function setMrythme($pmRythme) {
		$this->mrythme = $pmRythme;
		return $this;
	}

	public function getMrythme() {
		return $this->mrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idebutrythme", type="integer", nullable=true)
	 */
	private $idebutrythme = '0';

	public function setIdebutrythme($piDebutrythme) {
		$this->idebutrythme = $piDebutrythme;
		return $this;
	}

	public function getIdebutrythme() {
		return $this->idebutrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var \etablissements
	 *
	 * @ORM\ManyToOne(targetEntity="etablissements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ietablissement", referencedColumnName="iid_etablissement")
	 * })
	 */
	private $ietablissement;
	public function setIetablissement($piEtablissement) {
		$this->ietablissement = $piEtablissement;
		return $this;
	}
	public function getIetablissement() {
		return $this->ietablissement;
	}

	/**
	 * @var \salles
	 *
	 * @ORM\ManyToOne(targetEntity="salles")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="isallehabituelle", referencedColumnName="iid_salle")
	 * })
	 */
	private $isallehabituelle;

	public function setIsallehabituelle($piSallehabituelle) {
		$this->isallehabituelle = $piSallehabituelle;
		return $this;
	}

	public function getIsallehabituelle() {
		return $this->isallehabituelle;
	}

	/**
	 * @var \services
	 *
	 * @ORM\ManyToOne(targetEntity="services")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iservice", referencedColumnName="iid_service")
	 * })
	 */
	private $iservice;

	public function setIservice($piService) {
		$this->iservice = $piService;
		return $this;
	}

	public function getIservice() {
		return $this->iservice;
	}

	// </editor-fold>

	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\activites",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_groupe",nullable=false)},
	 *   inverseJoinColumns = {@ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")}
	 * )
	 */
	private $activites;

	/**
	 * @return Collection|acivite[]
	 */
	public function getActivites(): \Doctrine\Common\Collections\ArrayCollection {
		return $this->activites;
	}

	public function setActivites($paActivites): self {
		//MG 20200803 Utilisé lors de la création d'une activité depuis la liste des activités
//		echo $CetteMéthodeEstJamaisUtilisée;
		$this->activites[] = $paActivites;
//            $activite->setgroupe($this);
		return $this;
	}

	public function addActivité(activites $activité): self {
		if (!$this->activites->contains($activité)) {
			$this->activites[] = $activité;
			$activité->addGroupe($this);
		}
		return $this;
	}

	public function removeActivité(activites $activité): self {
		if ($this->activites->contains($activité)) {
			$this->activites->removeElement($activité);
			$activité->removeGroupe($this);
		}
		return $this;
	}
        
//        MC 20200915 DEBUT
        /**
        * @ORM\OneToMany(targetEntity="usagersGroupes", mappedBy="igroupe")
        */
        private $listeGroupesUsagers;

       /**
        * Get $listeGroupesUsagers
        *
        * @return \Doctrine\Common\Collections\Collection 
        */
        public function getlisteGroupesUsagers() {
                return $this->listeGroupesUsagers;
        }
//        MC 20200915 FIN

}
