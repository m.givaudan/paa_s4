<?php

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LG : cette annotation a été ajoutée pour réduire une exception, mais il n'y a physiquement de table derierre
 * @ORM\Entity()
 * @ ORM\Table(name="paa_pseudodata.authtoken",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="auth_tokens_value_unique", columns={"value"})}
 * )
 */
class Credentials {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="string")
	 */
	protected $login;
	protected $password;

	public function getLogin() {
		return $this->login;
	}

	public function setLogin($login) {
		$this->login = $login;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

}
