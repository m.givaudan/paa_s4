<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersRessources
 *
 * @ORM\Table(name="paa.usagers_ressources", indexes={@ORM\Index(name="usagers_ressources_archiv", columns={"darchivage"}), @ORM\Index(name="IDX_F02ED3A0329A8370", columns={"iid_usager"})})
 * @ORM\Entity
 */
class usagersRessources {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_usager_caisse", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.usagers_ressources_iid_usager_caisse_seq", allocationSize=1, initialValue=1)
	 */
	private $iidUsagerCaisse;

	public function setIidusagercaisse($piIdusagercaisse) {
		$this->iidUsagerCaisse = $piIdusagercaisse;
		return $this;
	}

	public function getIidusagercaisse() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctyperessources", type="string", length=254, nullable=true)
	 */
	private $ctyperessources = '';

	public function setCtyperessources($pcTyperessources) {
		$this->ctyperessources = $pcTyperessources;
		return $this;
	}

	public function getCtyperessources() {
		return $this->ctyperessources;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmontantressources", type="string", length=254, nullable=true)
	 */
	private $cmontantressources = '';

	public function setCmontantressources($pcMontantressources) {
		$this->cmontantressources = $pcMontantressources;
		return $this;
	}

	public function getCmontantressources() {
		return $this->cmontantressources;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mreversementcg", type="text", nullable=true)
	 */
	private $mreversementcg = '';

	public function setMreversementcg($pmReversementcg) {
		$this->mreversementcg = $pmReversementcg;
		return $this;
	}

	public function getMreversementcg() {
		return $this->mreversementcg;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="darchivage", type="date", nullable=true)
	 */
	private $darchivage;

	public function setDarchivage($pdArchivage) {
		$this->darchivage = $pdArchivage;
		return $this;
	}

	public function getDarchivage() {
		return $this->darchivage;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactloyer", type="boolean", nullable=true)
	 */
	private $lfactloyer = false;

	public function setLfactloyer($plFactloyer) {
		$this->lfactloyer = $plFactloyer;
		return $this;
	}

	public function getLfactloyer() {
		return $this->lfactloyer;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldotationglobale", type="boolean", nullable=true)
	 */
	private $ldotationglobale = false;

	public function setLdotationglobale($plDotationglobale) {
		$this->ldotationglobale = $plDotationglobale;
		return $this;
	}

	public function getLdotationglobale() {
		return $this->ldotationglobale;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactprixjournee", type="boolean", nullable=true)
	 */
	private $lfactprixjournee = false;

	public function setLfactprixjournee($plFactprixjournee) {
		$this->lfactprixjournee = $plFactprixjournee;
		return $this;
	}

	public function getLfactprixjournee() {
		return $this->lfactprixjournee;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactreversion", type="boolean", nullable=true)
	 */
	private $lfactreversion = false;

	public function setLfactreversion($plFactreversion) {
		$this->lfactreversion = $plFactreversion;
		return $this;
	}

	public function getLfactreversion() {
		return $this->lfactreversion;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iid_usager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iidUsager;

	public function setIidusager($piIdusager) {
		$this->iidUsager = $piIdusager;
		return $this;
	}

	public function getIidusager() {
		return $this->iidUsager;
	}

}
