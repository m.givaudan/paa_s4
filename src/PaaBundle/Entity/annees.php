<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * annees
 *
 * @ORM\Table(name="paa.annees", uniqueConstraints={@ORM\UniqueConstraint(name="u_annees_cnom", columns={"cnom"})})
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\anneesRepository")     
 */
class annees {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_annee", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.annees_iid_annee_seq", allocationSize=1, initialValue=1)
	 */
	private $iidAnnee = 'annees';

	public function setIidannee($piIdannee) {
		$this->iidAnnee = $piIdannee;
		return $this;
	}

	public function getIidannee() {
		return $this->iidAnnee;
	}

// LG 20180628 début
	public function getId() {
		return $this->getIidannee();
	}

// LG 20180628 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddebut", type="date", nullable=true)
	 */
	private $ddebut;

	public function setDdebut($pdDebut) {
		$this->ddebut = $pdDebut;
		return $this;
	}

	public function getDdebut() {
		return $this->ddebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfin", type="date", nullable=true)
	 */
	private $dfin;

	public function setDfin($pdFin) {
		$this->dfin = $pdFin;
		return $this;
	}

	public function getDfin() {
		return $this->dfin;
	}

}
