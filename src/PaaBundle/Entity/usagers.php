<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagers
 *
 * @ORM\Table(name="paa.usagers", uniqueConstraints={@ORM\UniqueConstraint(name="u_usagers_cnomcourt", columns={"cnomcourt"})}, indexes={@ORM\Index(name="usagers_u_service", columns={"iservice"}), @ORM\Index(name="usagers_nom_usager", columns={"cnom"}), @ORM\Index(name="usagers_etablissem", columns={"ietablissement"}), @ORM\Index(name="usagers_date_sorti", columns={"dsortie"}), @ORM\Index(name="usagers_u_prenom", columns={"cprenom"}), @ORM\Index(name="usagers_no_interne", columns={"cnointerne"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\usagersRepository") 
 */
class usagers {

	public function __toString() {
		return $this->cnomcourt;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_usager", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.usagers_iid_usager_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
// LG 20200904 old	private $iidUsager = 'usagers';
	private $iidUsager = '0';

	public function setIidusager($piIdusager) {
		$this->iidUsager = $piIdusager;
		return $this;
	}

	public function getIidusager() {
		return $this->iidUsager;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidUsager;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnointerne", type="string", length=20, nullable=true)
	 */
	private $cnointerne = '';

	public function setCnointerne($pcNointerne) {
		$this->cnointerne = $pcNointerne;
		return $this;
	}

	public function getCnointerne() {
		return $this->cnointerne;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cprenom", type="string", length=50, nullable=true)
	 */
	private $cprenom = '';

	public function setCprenom($pcPrenom) {
		$this->cprenom = $pcPrenom;
		return $this;
	}

	public function getCprenom() {
		return $this->cprenom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=50, nullable=true)
	 */
	private $cnomcourt = '';

	public function setCnomcourt($pcNomcourt) {
		$this->cnomcourt = $pcNomcourt;
		return $this;
	}

	public function getCnomcourt() {
		return $this->cnomcourt;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dnaissance", type="date", nullable=true)
	 */
// LG 20200811 old	private $dnaissance = '0001-01-01';
	private $dnaissance = null;

	public function setDnaissance($pdNaissance) {
		$this->dnaissance = $pdNaissance;
		return $this;
	}

	public function getDnaissance() {
		return $this->dnaissance;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dentree", type="date", nullable=true)
	 */
// LG 20200811 old	private $dentree = '0001-01-01';
	private $dentree = null;

	public function setDentree($pdEntree) {
		$this->dentree = $pdEntree;
		return $this;
	}

	public function getDentree() {
		return $this->dentree;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dsortie", type="date", nullable=true)
	 */
// LG 20200811 old	private $dsortie = '0001-01-01';
	private $dsortie = null;

	public function setDsortie($pdSortie) {
		$this->dsortie = $pdSortie;
		return $this;
	}

	public function getDsortie() {
		return $this->dsortie;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ietablissement", type="integer", nullable=true)
	 */
	private $ietablissement = '0';

	public function setIetablissement($piEtablissement) {
		$this->ietablissement = $piEtablissement;
		return $this;
	}

	public function getIetablissement() {
		return $this->ietablissement;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iservice", type="integer", nullable=true)
	 */
	private $iservice;

	public function setIservice($piService) {
		$this->iservice = $piService;
		return $this;
	}

	public function getIservice() {
		return $this->iservice;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrepertoire", type="text", nullable=true)
	 */
	private $mrepertoire = '';

	public function setMrepertoire($pmRepertoire) {
		$this->mrepertoire = $pmRepertoire;
		return $this;
	}

	public function getMrepertoire() {
		return $this->mrepertoire;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ireferent", type="integer", nullable=true)
	 */
	private $ireferent = '0';

	public function setIreferent($piReferent) {
		$this->ireferent = $piReferent;
		return $this;
	}

	public function getIreferent() {
		return $this->ireferent;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrythme", type="text", nullable=true)
	 */
	private $mrythme = '';

	public function setMrythme($pmRythme) {
		$this->mrythme = $pmRythme;
		return $this;
	}

	public function getMrythme() {
		return $this->mrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idebutrythme", type="integer", nullable=true)
	 */
	private $idebutrythme = '0';

	public function setIdebutrythme($piDebutrythme) {
		$this->idebutrythme = $piDebutrythme;
		return $this;
	}

	public function getIdebutrythme() {
		return $this->idebutrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="csexe", type="string", length=1, nullable=true)
	 */
	private $csexe = '';

	public function setCsexe($pcSexe) {
		$this->csexe = $pcSexe;
		return $this;
	}

	public function getCsexe() {
		return $this->csexe;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cdeficience", type="string", length=25, nullable=true)
	 */
	private $cdeficience = '';

	public function setCdeficience($pcDeficience) {
		$this->cdeficience = $pcDeficience;
		return $this;
	}

	public function getCdeficience() {
		return $this->cdeficience;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cregime", type="string", length=50, nullable=true)
	 */
	private $cregime = '';

	public function setCregime($pcRegime) {
		$this->cregime = $pcRegime;
		return $this;
	}

	public function getCregime() {
		return $this->cregime;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clieunaissance", type="string", length=50, nullable=true)
	 */
	private $clieunaissance = '';

	public function setClieunaissance($pcLieunaissance) {
		$this->clieunaissance = $pcLieunaissance;
		return $this;
	}

	public function getClieunaissance() {
		return $this->clieunaissance;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnationalite", type="string", length=50, nullable=true)
	 */
	private $cnationalite = '';

	public function setCnationalite($pcNationalite) {
		$this->cnationalite = $pcNationalite;
		return $this;
	}

	public function getCnationalite() {
		return $this->cnationalite;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrepphoto", type="text", nullable=true)
	 */
	private $mrepphoto = '';

	public function setMrepphoto($pmRepphoto) {
		$this->mrepphoto = $pmRepphoto;
		return $this;
	}

	public function getMrepphoto() {
		return $this->mrepphoto;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmotifsortie", type="string", length=100, nullable=true)
	 */
	private $cmotifsortie = '';

	public function setCmotifsortie($pcMotifsortie) {
		$this->cmotifsortie = $pcMotifsortie;
		return $this;
	}

	public function getCmotifsortie() {
		return $this->cmotifsortie;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate1", type="date", nullable=true)
	 */
// LG 20200811 old	private $ddate1 = '0001-01-01';
	private $ddate1 = null;

	public function setDdate1($pdDate1) {
		$this->ddate1 = $pdDate1;
		return $this;
	}

	public function getDdate1() {
		return $this->ddate1;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate2", type="date", nullable=true)
	 */
// LG 20200811 old	private $ddate2 = '0001-01-01';
	private $ddate2 = null;

	public function setDdate2($pdDate2) {
		$this->ddate2 = $pdDate2;
		return $this;
	}

	public function getDdate2() {
		return $this->ddate2;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate3", type="date", nullable=true)
	 */
// LG 20200811 old	private $ddate3 = '0001-01-01';
	private $ddate3 = null;

	public function setDdate3($pdDate3) {
		$this->ddate3 = $pdDate3;
		return $this;
	}

	public function getDdate3() {
		return $this->ddate3;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate4", type="date", nullable=true)
	 */
// LG 20200811 old	private $ddate4 = '0001-01-01';
	private $ddate4 = null;

	public function setDdate4($pdDate4) {
		$this->ddate4 = $pdDate4;
		return $this;
	}

	public function getDdate4() {
		return $this->ddate4;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ireferent2", type="integer", nullable=true)
	 */
	private $ireferent2 = '0';

	public function setIreferent2($piReferent2) {
		$this->ireferent2 = $piReferent2;
		return $this;
	}

	public function getIreferent2() {
		return $this->ireferent2;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ireferent3", type="integer", nullable=true)
	 */
	private $ireferent3 = '0';

	public function setIreferent3($piReferent3) {
		$this->ireferent3 = $piReferent3;
		return $this;
	}

	public function getIreferent3() {
		return $this->ireferent3;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ireferent4", type="integer", nullable=true)
	 */
	private $ireferent4 = '0';

	public function setIreferent4($piReferent4) {
		$this->ireferent4 = $piReferent4;
		return $this;
	}

	public function getIreferent4() {
		return $this->ireferent4;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ireferent5", type="integer", nullable=true)
	 */
	private $ireferent5 = '0';

	public function setIreferent5($piReferent5) {
		$this->ireferent5 = $piReferent5;
		return $this;
	}

	public function getIreferent5() {
		return $this->ireferent5;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre1", type="string", length=50, nullable=true)
	 */
	private $cchamplibre1 = '';

	public function setCchamplibre1($pcChamplibre1) {
		$this->cchamplibre1 = $pcChamplibre1;
		return $this;
	}

	public function getCchamplibre1() {
		return $this->cchamplibre1;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cchambre", type="string", length=20, nullable=true)
	 */
	private $cchambre = '';

	public function setCchambre($pcChambre) {
		$this->cchambre = $pcChambre;
		return $this;
	}

	public function getCchambre() {
		return $this->cchambre;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldate1_npa", type="boolean", nullable=true)
	 */
	private $ldate1Npa = false;

	public function setLdate1npa($plDate1npa) {
		$this->ldate1Npa = $plDate1npa;
		return $this;
	}

	public function getLdate1npa() {
		return $this->ldate1Npa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldate2_npa", type="boolean", nullable=true)
	 */
	private $ldate2Npa = false;

	public function setLdate2npa($plDate2npa) {
		$this->ldate2Npa = $plDate2npa;
		return $this;
	}

	public function getLdate2npa() {
		return $this->ldate2Npa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldate3_npa", type="boolean", nullable=true)
	 */
	private $ldate3Npa = false;

	public function setLdate3npa($plDate3npa) {
		$this->ldate3Npa = $plDate3npa;
		return $this;
	}

	public function getLdate3npa() {
		return $this->ldate3Npa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldate4_npa", type="boolean", nullable=true)
	 */
	private $ldate4Npa = false;

	public function setLdate4npa($plDate4npa) {
		$this->ldate4Npa = $plDate4npa;
		return $this;
	}

	public function getLdate4npa() {
		return $this->ldate4Npa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldate20ans_npa", type="boolean", nullable=true)
	 */
	private $ldate20ansNpa = false;

	public function setLdate20ansnpa($plDate20ansnpa) {
		$this->ldate20ansNpa = $plDate20ansnpa;
		return $this;
	}

	public function getLdate20ansnpa() {
		return $this->ldate20ansNpa;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icoulavertissement", type="integer", nullable=true)
	 */
	private $icoulavertissement = '0';

	public function setIcoulavertissement($piCoulavertissement) {
		$this->icoulavertissement = $piCoulavertissement;
		return $this;
	}

	public function getIcoulavertissement() {
		return $this->icoulavertissement;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="corigine", type="string", length=100, nullable=true)
	 */
	private $corigine = '';

	public function setCorigine($pcOrigine) {
		$this->corigine = $pcOrigine;
		return $this;
	}

	public function getCorigine() {
		return $this->corigine;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="igroupeacti", type="integer", nullable=true)
	 */
	private $igroupeacti;

	public function setIgroupeacti($piGroupeacti) {
		$this->igroupeacti = $piGroupeacti;
		return $this;
	}

	public function getIgroupeacti() {
		return $this->igroupeacti;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomjeunefille", type="string", length=50, nullable=true)
	 */
	private $cnomjeunefille = '';

	public function setCnomjeunefille($pcNomjeunefille) {
		$this->cnomjeunefille = $pcNomjeunefille;
		return $this;
	}

	public function getCnomjeunefille() {
		return $this->cnomjeunefille;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccivilite", type="string", length=10, nullable=true)
	 */
	private $ccivilite = '';

	public function setCcivilite($pcCivilite) {
		$this->ccivilite = $pcCivilite;
		return $this;
	}

	public function getCcivilite() {
		return $this->ccivilite;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre2", type="string", length=50, nullable=true)
	 */
	private $cchamplibre2 = '';

	public function setCchamplibre2($pcChamplibre2) {
		$this->cchamplibre2 = $pcChamplibre2;
		return $this;
	}

	public function getCchamplibre2() {
		return $this->cchamplibre2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre3", type="string", length=50, nullable=true)
	 */
	private $cchamplibre3 = '';

	public function setCchamplibre3($pcChamplibre3) {
		$this->cchamplibre3 = $pcChamplibre3;
		return $this;
	}

	public function getCchamplibre3() {
		return $this->cchamplibre3;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre4", type="string", length=50, nullable=true)
	 */
	private $cchamplibre4 = '';

	public function setCchamplibre4($pcChamplibre4) {
		$this->cchamplibre4 = $pcChamplibre4;
		return $this;
	}

	public function getCchamplibre4() {
		return $this->cchamplibre4;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre5", type="string", length=50, nullable=true)
	 */
	private $cchamplibre5 = '';

	public function setCchamplibre5($pcChamplibre5) {
		$this->cchamplibre5 = $pcChamplibre5;
		return $this;
	}

	public function getCchamplibre5() {
		return $this->cchamplibre5;
	}
        //MC 20200914 DEBUT
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre6", type="string", length=50, nullable=true)
	 */
	private $cchamplibre6 = '';

	public function setCchamplibre6($pcChamplibre6) {
		$this->cchamplibre6 = $pcChamplibre6;
		return $this;
	}

	public function getCchamplibre6() {
		return $this->cchamplibre6;
	}
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre7", type="string", length=50, nullable=true)
	 */
	private $cchamplibre7 = '';

	public function setCchamplibre7($pcChamplibre7) {
		$this->cchamplibre7 = $pcChamplibre7;
		return $this;
	}

	public function getCchamplibre7() {
		return $this->cchamplibre7;
	}
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre8", type="string", length=50, nullable=true)
	 */
	private $cchamplibre8 = '';

	public function setCchamplibre8($pcChamplibre8) {
		$this->cchamplibre8 = $pcChamplibre8;
		return $this;
	}

	public function getCchamplibre8() {
		return $this->cchamplibre8;
	}

        /**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre9", type="string", length=50, nullable=true)
	 */
	private $cchamplibre9 = '';

	public function setCchamplibre9($pcChamplibre9) {
		$this->cchamplibre9 = $pcChamplibre9;
		return $this;
	}

	public function getCchamplibre9() {
		return $this->cchamplibre9;
	}
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="cchamplibre10", type="string", length=50, nullable=true)
	 */
	private $cchamplibre10 = '';

	public function setCchamplibre10($pcChamplibre10) {
		$this->cchamplibre10 = $pcChamplibre10;
		return $this;
	}

	public function getCchamplibre10() {
		return $this->cchamplibre10;
	}
        //MC 20200914 FIN

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccommdate1", type="string", length=50, nullable=true)
	 */
	private $ccommdate1 = '';

	public function setCcommdate1($pcCommdate1) {
		$this->ccommdate1 = $pcCommdate1;
		return $this;
	}

	public function getCcommdate1() {
		return $this->ccommdate1;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccommdate2", type="string", length=50, nullable=true)
	 */
	private $ccommdate2 = '';

	public function setCcommdate2($pcCommdate2) {
		$this->ccommdate2 = $pcCommdate2;
		return $this;
	}

	public function getCcommdate2() {
		return $this->ccommdate2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccommdate3", type="string", length=50, nullable=true)
	 */
	private $ccommdate3 = '';

	public function setCcommdate3($pcCommdate3) {
		$this->ccommdate3 = $pcCommdate3;
		return $this;
	}

	public function getCcommdate3() {
		return $this->ccommdate3;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccommdate4", type="string", length=50, nullable=true)
	 */
	private $ccommdate4 = '';

	public function setCcommdate4($pcCommdate4) {
		$this->ccommdate4 = $pcCommdate4;
		return $this;
	}

	public function getCcommdate4() {
		return $this->ccommdate4;
	}

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="activites", mappedBy="iusager")
//     */
//    private $iacti;
//    public function setActivités($piActi){
//        $this->iacti = $piActi;
//        return $this;
//    }
//    public function getActivités() {
//        return $this->iacti;
//    }

	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\activites",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_usagers",nullable=false)},
	 *   inverseJoinColumns = {@ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")}
	 * )
	 */
	private $activites;

	/**
	 * @return Collection|acivite[]
	 */
	public function getactivites(): \Doctrine\Common\Collections\ArrayCollection {
		return $this->activites;
	}

	public function setactivites() {
		if (!$this->activites->contains($activite)) {
			$this->activites[] = $activite;
			$activite->setusagers($this);
		}

		return $this;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->activites = new \Doctrine\Common\Collections\ArrayCollection();
		$this->iacti = new \Doctrine\Common\Collections\ArrayCollection();
	}

        
        //MC 20200910 DEBUT
    /**
     * @ORM\OneToMany(targetEntity="referents", mappedBy="iusager")
     */
    private $listeReferents;

    /**
     * Get $listeReferents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getlisteReferents() {
            return $this->listeReferents;
    }
    
    /**
     * @ORM\OneToMany(targetEntity="usagersGroupes", mappedBy="iusager")
     */
    private $listeUsagersGroupes;

    /**
     * Get $listeUsagersGroupes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getlisteUsagersGroupes() {
            return $this->listeUsagersGroupes;
    }
    //MC 20200910 FIN
}
