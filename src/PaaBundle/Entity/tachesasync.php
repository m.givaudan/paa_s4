<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tachesasync
 *
 * @ORM\Table(name="paa.tachesasync", indexes={@ORM\Index(name="tachesasync_completed", columns={"completed"}), @ORM\Index(name="tachesasync_priority", columns={"priority"}), @ORM\Index(name="tachesasync_started", columns={"started"}), @ORM\Index(name="tachesasync_id", columns={"id"}), @ORM\Index(name="tachesasync_type", columns={"type"})})
 * @ORM\Entity
 */
class tachesasync {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="id", type="string", length=15, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tachesasync_id_seq", allocationSize=1, initialValue=1)
	 */
	private $id = '';

	public function setId($piD) {
		$this->id = $piD;
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=60, nullable=true)
	 */
	private $title = '';

	public function setTitle($ptItle) {
		$this->title = $ptItle;
		return $this;
	}

	public function getTitle() {
		return $this->title;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="returndata", type="text", nullable=true)
	 */
	private $returndata = '';

	public function setReturndata($prEturndata) {
		$this->returndata = $prEturndata;
		return $this;
	}

	public function getReturndata() {
		return $this->returndata;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="inputdata", type="text", nullable=true)
	 */
	private $inputdata = '';

	public function setInputdata($piNputdata) {
		$this->inputdata = $piNputdata;
		return $this;
	}

	public function getInputdata() {
		return $this->inputdata;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=15, nullable=true)
	 */
	private $type = '';

	public function setType($ptYpe) {
		$this->type = $ptYpe;
		return $this;
	}

	public function getType() {
		return $this->type;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="priority", type="integer", nullable=true)
	 */
	private $priority = '0';

	public function setPriority($ppRiority) {
		$this->priority = $ppRiority;
		return $this;
	}

	public function getPriority() {
		return $this->priority;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="submitted", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $submitted = '0001-01-01';
	private $submitted = null;

	public function setSubmitted($psUbmitted) {
		$this->submitted = $psUbmitted;
		return $this;
	}

	public function getSubmitted() {
		return $this->submitted;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="started", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $started = '0001-01-01';
	private $started = null;

	public function setStarted($psTarted) {
		$this->started = $psTarted;
		return $this;
	}

	public function getStarted() {
		return $this->started;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $updated = '0001-01-01';
	private $updated = null;

	public function setUpdated($puPdated) {
		$this->updated = $puPdated;
		return $this;
	}

	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="completed", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $completed = '0001-01-01';
	private $completed = null;

	public function setCompleted($pcOmpleted) {
		$this->completed = $pcOmpleted;
		return $this;
	}

	public function getCompleted() {
		return $this->completed;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="expire", type="integer", nullable=true)
	 */
	private $expire = '0';

	public function setExpire($peXpire) {
		$this->expire = $peXpire;
		return $this;
	}

	public function getExpire() {
		return $this->expire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="status", type="string", length=254, nullable=true)
	 */
	private $status = '';

	public function setStatus($psTatus) {
		$this->status = $psTatus;
		return $this;
	}

	public function getStatus() {
		return $this->status;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="username", type="string", length=20, nullable=true)
	 */
	private $username = '';

	public function setUsername($puSername) {
		$this->username = $puSername;
		return $this;
	}

	public function getUsername() {
		return $this->username;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="chkcounter", type="integer", nullable=true)
	 */
	private $chkcounter = '0';

	public function setChkcounter($pcHkcounter) {
		$this->chkcounter = $pcHkcounter;
		return $this;
	}

	public function getChkcounter() {
		return $this->chkcounter;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="cancelled", type="boolean", nullable=true)
	 */
	private $cancelled = false;

	public function setCancelled($pcAncelled) {
		$this->cancelled = $pcAncelled;
		return $this;
	}

	public function getCancelled() {
		return $this->cancelled;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="surserveur", type="boolean", nullable=true)
	 */
	private $surserveur = false;

	public function setSurserveur($psUrserveur) {
		$this->surserveur = $psUrserveur;
		return $this;
	}

	public function getSurserveur() {
		return $this->surserveur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="properties", type="text", nullable=true)
	 */
	private $properties = '';

	public function setProperties($ppRoperties) {
		$this->properties = $ppRoperties;
		return $this;
	}

	public function getProperties() {
		return $this->properties;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="bttencours", type="boolean", nullable=true)
	 */
	private $bttencours = false;

	public function setBttencours($pbTtencours) {
		$this->bttencours = $pbTtencours;
		return $this;
	}

	public function getBttencours() {
		return $this->bttencours;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="actif", type="boolean", nullable=true)
	 */
	private $actif = false;

	public function setActif($paCtif) {
		$this->actif = $paCtif;
		return $this;
	}

	public function getActif() {
		return $this->actif;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="pause", type="boolean", nullable=true)
	 */
	private $pause = false;

	public function setPause($ppAuse) {
		$this->pause = $ppAuse;
		return $this;
	}

	public function getPause() {
		return $this->pause;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="sidtache", type="string", length=15, nullable=true)
	 */
	private $sidtache = '';

	public function setSidtache($psIdtache) {
		$this->sidtache = $psIdtache;
		return $this;
	}

	public function getSidtache() {
		return $this->sidtache;
	}

}
