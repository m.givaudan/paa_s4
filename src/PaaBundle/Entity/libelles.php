<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * libelles
 *
 * @ORM\Table(name="paa.libelles", indexes={@ORM\Index(name="libelles_format", columns={"iformat"}), @ORM\Index(name="libelles_id", columns={"iid_objet"}), @ORM\Index(name="libelles_type", columns={"itype_objet"})})
 * @ORM\Entity
 */
class libelles {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_libelle", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.libelles_iid_libelle_seq", allocationSize=1, initialValue=1)
	 */
	private $iidLibelle;

	public function setIidlibelle($piIdlibelle) {
		$this->iidLibelle = $piIdlibelle;
		return $this;
	}

	public function getIidlibelle() {
		return $this->iidLibelle;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidLibelle;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="itype_objet", type="integer", nullable=true)
	 */
	private $itypeObjet = '0';

	public function setItypeobjet($piTypeobjet) {
		$this->itypeObjet = $piTypeobjet;
		return $this;
	}

	public function getItypeobjet() {
		return $this->itypeObjet;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_objet", type="integer", nullable=true)
	 */
	private $iidObjet = '0';

	public function setIidobjet($piIdobjet) {
		$this->iidObjet = $piIdobjet;
		return $this;
	}

	public function getIidobjet() {
		return $this->iidObjet;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iformat", type="integer", nullable=true)
	 */
	private $iformat = '0';

	public function setIformat($piFormat) {
		$this->iformat = $piFormat;
		return $this;
	}

	public function getIformat() {
		return $this->iformat;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mlibelle", type="text", nullable=true)
	 */
	private $mlibelle;

	public function setMlibelle($pmLibelle) {
		$this->mlibelle = $pmLibelle;
		return $this;
	}

	public function getMlibelle() {
		return $this->mlibelle;
	}

}
