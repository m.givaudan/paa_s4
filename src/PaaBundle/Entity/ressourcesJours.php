<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ressourcesJours
 *
 * @ORM\Table(name="paa.ressources_jours", indexes={@ORM\Index(name="ressources_jours_actibase", columns={"iactibase"}), @ORM\Index(name="ressources_jours_id_res", columns={"iid_res"}), @ORM\Index(name="ressources_jours_date", columns={"ddate"})})
 * @ORM\Entity
 */
class ressourcesJours {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_ressource_jour", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_jours_iid_ressource_jour_seq", allocationSize=1, initialValue=1)
	 */
	private $iidRessourceJour = 'RESSOURCES_JOURS';

	public function setIidressourcejour($piIdressourcejour) {
		$this->iidRessourceJour = $piIdressourcejour;
		return $this;
	}

	public function getIidressourcejour() {
		return $this->iidRessourceJour;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidRessourceJour;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate", type="date", nullable=true)
	 */
// LG 20200811 old	private $ddate = '0001-01-01';
	private $ddate = null;

	public function setDdate($pdDate) {
		$this->ddate = $pdDate;
		return $this;
	}

	public function getDdate() {
		return $this->ddate;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=2, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes;

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iactibase", type="integer", nullable=true)
	 */
	private $iactibase;

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="ndebut", type="float", precision=10, scale=0, nullable=true)
	 */
	private $ndebut = '0';

	public function setNdebut($pnDebut) {
		$this->ndebut = $pnDebut;
		return $this;
	}

	public function getNdebut() {
		return $this->ndebut;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nfin", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nfin = '24';

	public function setNfin($pnFin) {
		$this->nfin = $pnFin;
		return $this;
	}

	public function getNfin() {
		return $this->nfin;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfin", type="date", nullable=true)
	 */
	private $dfin;

	public function setDfin($pdFin) {
		$this->dfin = $pdFin;
		return $this;
	}

	public function getDfin() {
		return $this->dfin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommdemande", type="text", nullable=true)
	 */
	private $mcommdemande = '';

	public function setMcommdemande($pmCommdemande) {
		$this->mcommdemande = $pmCommdemande;
		return $this;
	}

	public function getMcommdemande() {
		return $this->mcommdemande;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommvalidation", type="text", nullable=true)
	 */
	private $mcommvalidation = '';

	public function setMcommvalidation($pmCommvalidation) {
		$this->mcommvalidation = $pmCommvalidation;
		return $this;
	}

	public function getMcommvalidation() {
		return $this->mcommvalidation;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdemande", type="datetime", nullable=true)
	 */
	private $tdemande;

	public function setTdemande($ptDemande) {
		$this->tdemande = $ptDemande;
		return $this;
	}

	public function getTdemande() {
		return $this->tdemande;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lapprouve", type="boolean", nullable=true)
	 */
	private $lapprouve = true;

	public function setLapprouve($plApprouve) {
		$this->lapprouve = $plApprouve;
		return $this;
	}

	public function getLapprouve() {
		return $this->lapprouve;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tapprouve", type="datetime", nullable=true)
	 */
	private $tapprouve;

	public function setTapprouve($ptApprouve) {
		$this->tapprouve = $ptApprouve;
		return $this;
	}

	public function getTapprouve() {
		return $this->tapprouve;
	}

}
