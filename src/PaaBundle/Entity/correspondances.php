<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * correspondances
 *
 * @ORM\Table(name="paa.correspondances", uniqueConstraints={@ORM\UniqueConstraint(name="u_correspondances_itypetable&iid_paa", columns={"iid_paa", "itypetable"})}, indexes={@ORM\Index(name="correspondances_app", columns={"itypeapplication"}), @ORM\Index(name="correspondances_id_ext", columns={"cid_externe"}), @ORM\Index(name="correspondances_table", columns={"itypetable"})})
 * @ORM\Entity
 */
class correspondances {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_corr", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.correspondances_iid_corr_seq", allocationSize=1, initialValue=1)
	 */
	private $iidCorr;

	public function setIidcorr($piIdcorr) {
		$this->iidCorr = $piIdcorr;
		return $this;
	}

	public function getIidcorr() {
		return $this->iidCorr;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidCorr;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="itypeapplication", type="integer", nullable=true)
	 */
	private $itypeapplication = '0';

	public function setItypeapplication($piTypeapplication) {
		$this->itypeapplication = $piTypeapplication;
		return $this;
	}

	public function getItypeapplication() {
		return $this->itypeapplication;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="csousbase", type="string", length=50, nullable=true)
	 */
	private $csousbase = '';

	public function setCsousbase($pcSousbase) {
		$this->csousbase = $pcSousbase;
		return $this;
	}

	public function getCsousbase() {
		return $this->csousbase;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="itypetable", type="integer", nullable=true)
	 */
	private $itypetable = '0';

	public function setItypetable($piTypetable) {
		$this->itypetable = $piTypetable;
		return $this;
	}

	public function getItypetable() {
		return $this->itypetable;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_paa", type="integer", nullable=true)
	 */
	private $iidPaa = '0';

	public function setIidpaa($piIdpaa) {
		$this->iidPaa = $piIdpaa;
		return $this;
	}

	public function getIidpaa() {
		return $this->iidPaa;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cid_externe", type="string", length=100, nullable=true)
	 */
	private $cidExterne = '';

	public function setCidexterne($pcIdexterne) {
		$this->cidExterne = $pcIdexterne;
		return $this;
	}

	public function getCidexterne() {
		return $this->cidExterne;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timport", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $timport = '0001-01-01';
	private $timport = null;

	public function setTimport($ptImport) {
		$this->timport = $ptImport;
		return $this;
	}

	public function getTimport() {
		return $this->timport;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmaj = '0001-01-01';
	private $tmaj = null;

	public function setTmaj($ptMaj) {
		$this->tmaj = $ptMaj;
		return $this;
	}

	public function getTmaj() {
		return $this->tmaj;
	}

}
