<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * cles
 *
 * @ORM\Table(name="paa.cles", uniqueConstraints={@ORM\UniqueConstraint(name="u_cles_inocle&ctable", columns={"ctable", "inocle"})})
 * @ORM\Entity
 */
class cles {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctable", type="string", length=100, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.cles_ctable_seq", allocationSize=1, initialValue=1)
	 */
	private $ctable = '';

	public function setCtable($pcTable) {
		$this->ctable = $pcTable;
		return $this;
	}

	public function getCtable() {
		return $this->ctable;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="inocle", type="integer", nullable=true)
	 */
	private $inocle = '0';

	public function setInocle($piNocle) {
		$this->inocle = $piNocle;
		return $this;
	}

	public function getInocle() {
		return $this->inocle;
	}

}
