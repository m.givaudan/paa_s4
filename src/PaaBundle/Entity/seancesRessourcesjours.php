<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesRessourcesjours
 *
 * @ORM\Table(name="paa.seances_ressourcesjours")
 * @ORM\Entity
 */
class seancesRessourcesjours {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cbidon", type="string", length=50, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.seances_ressourcesjours_cbidon_seq", allocationSize=1, initialValue=1)
	 */
	private $cbidon = '';

	public function setCbidon($pcBidon) {
		$this->cbidon = $pcBidon;
		return $this;
	}

	public function getCbidon() {
		return $this->cbidon;
	}

}
