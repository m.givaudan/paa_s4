<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tlogs
 *
 * @ORM\Table(name="paa.tlogs", indexes={@ORM\Index(name="tlogs_ctype", columns={"ctype"}), @ORM\Index(name="tlogs_iid", columns={"iid"}), @ORM\Index(name="tlogs_iidligne", columns={"iidligne"})})
 * @ORM\Entity
 */
class tlogs {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tlogs_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid = '0';

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype", type="string", length=1, nullable=true)
	 */
	private $ctype = '';

	public function setCtype($pcType) {
		$this->ctype = $pcType;
		return $this;
	}

	public function getCtype() {
		return $this->ctype;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctable", type="string", length=50, nullable=true)
	 */
	private $ctable = '';

	public function setCtable($pcTable) {
		$this->ctable = $pcTable;
		return $this;
	}

	public function getCtable() {
		return $this->ctable;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iidligne", type="integer", nullable=true)
	 */
	private $iidligne = '0';

	public function setIidligne($piIdligne) {
		$this->iidligne = $piIdligne;
		return $this;
	}

	public function getIidligne() {
		return $this->iidligne;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mvaleurs", type="text", nullable=true)
	 */
	private $mvaleurs = '';

	public function setMvaleurs($pmValeurs) {
		$this->mvaleurs = $pmValeurs;
		return $this;
	}

	public function getMvaleurs() {
		return $this->mvaleurs;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iuser", type="integer", nullable=true)
	 */
	private $iuser = '0';

	public function setIuser($piUser) {
		$this->iuser = $piUser;
		return $this;
	}

	public function getIuser() {
		return $this->iuser;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ttime", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $ttime = '0001-01-01';
	private $ttime = null;

	public function setTtime($ptTime) {
		$this->ttime = $ptTime;
		return $this;
	}

	public function getTtime() {
		return $this->ttime;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcallstack", type="text", nullable=true)
	 */
	private $mcallstack = '';

	public function setMcallstack($pmCallstack) {
		$this->mcallstack = $pmCallstack;
		return $this;
	}

	public function getMcallstack() {
		return $this->mcallstack;
	}

}
