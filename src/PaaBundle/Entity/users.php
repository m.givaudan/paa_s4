<?php

// src/PaaBundle/Entity/user.php
// LG 20171214 pour FOSUserBundle

namespace App\PaaBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Paa_Securite.Users")
 */
/* Paa_Securite.Users est finalement une table, mais pourrait être une vue créée comme suit : 
  Drop View If exists Paa_Securite.Users ;
  Create Or Replace View Paa_Securite.Users As
  SELECT t0.cInitiales AS username
  , t0.cNomCourt AS username_canonical
  , t0.cMel AS email
  , t0.cMel AS email_canonical
  , true AS enabled
  , ''::Text AS salt
  , t0.cmdp As password
  , current_timestamp + '1 day'::interval As last_login
  , ''::Text As confirmation_token
  , current_timestamp - '1 day'::interval As password_requested_at
  , 'a:0:{}'::Text As roles
  , t0.iid_intervenant
  FROM Paa.Intervenants t0
  Where Not empty(iDroitsW) Or Not empty(iDroitsR) ;
 */
class users extends BaseUser {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_user", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="Paa_Securite.users_iid_user_seq", allocationSize=1, initialValue=1)
	 */
	protected $id;

	public function setid($piId) {
		return $this->setiid_user($piId);
	}

	public function setiid_user($piId) {
		$this->id = $piId;
	}

	public function getid() {
		return $this->getiid_user();
	}

	public function getiid_user() {
		return $this->id;
	}

	public function getIidUser() {
		return $this->id;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", nullable=true)
	 */
	protected $cType_Res;

	public function setCtypeRes($ctype) {
		$this->cType_Res = $ctype;
	}

	public function getctype_res() {
		return $this->getCtypeRes();
	}

	public function getCtypeRes() {
		return $this->cType_Res;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	protected $iid_res;

	public function setIidRes($id) {
		$this->iid_res = $id;
	}

	public function getiid_res() {
		return $this->getIidRes();
	}

	public function getIidRes() {
		return $this->iid_res;
	}

	public function getcRes() {
		return $this->getctype_res() . $this->getIidRes();
	}

	protected $username;

	public function setemail($mail) {
		$this->email = $mail;
	}

	public function getemail() {
		return $this->email;
	}

	protected $password;

	public function setPassword($password) {
		parent::setPassword($password);
	}

	public function getpassword() {
		return $this->password;
	}

	/**
	 * @var string
	 */
	public $roles2;

	/**
	 * @var bool
	 * 
	 * @ORM\Column(name="lpeutchangerprofil", type="boolean")
	 */
	protected $lpeutchangerprofil;

	public function isLPeutChangerProfil() {
		return $this->lpeutchangerprofil;
	}

	public function setLPeutChangerProfil($boolean) {
		$this->lpeutchangerprofil = (bool) $boolean;
		return $this;
	}

	public function __construct() {
		parent::__construct();
		// your own logic
	}

}
