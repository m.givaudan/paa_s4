<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersAutorisations
 *
 * @ORM\Table(name="paa.usagers_autorisations", uniqueConstraints={@ORM\UniqueConstraint(name="u_usagers_autorisations_iautorisation&iusager", columns={"iautorisation", "iusager"})}, indexes={@ORM\Index(name="usagers_autorisations_usager", columns={"iusager"})})
 * @ORM\Entity
 */
class usagersAutorisations {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_usager_autorisation", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.usagers_autorisations_iid_usager_autorisation_seq", allocationSize=1, initialValue=1)
	 */
	private $iidUsagerAutorisation = 'Usagers_Autorisations';

	public function setIidusagerautorisation($piIdusagerautorisation) {
		$this->iidUsagerAutorisation = $piIdusagerautorisation;
		return $this;
	}

	public function getIidusagerautorisation() {
		return $this->iidUsagerAutorisation;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidUsagerAutorisation;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iautorisation", type="integer", nullable=true)
	 */
	private $iautorisation = '0';

	public function setIautorisation($piAutorisation) {
		$this->iautorisation = $piAutorisation;
		return $this;
	}

	public function getIautorisation() {
		return $this->iautorisation;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lautorisation", type="boolean", nullable=true)
	 */
	private $lautorisation = false;

	public function setLautorisation($plAutorisation) {
		$this->lautorisation = $plAutorisation;
		return $this;
	}

	public function getLautorisation() {
		return $this->lautorisation;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="commentaire", type="text", nullable=true)
	 */
	private $commentaire = '';

	public function setCommentaire($pcOmmentaire) {
		$this->commentaire = $pcOmmentaire;
		return $this;
	}

	public function getCommentaire() {
		return $this->commentaire;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iusager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iusager;

	public function setIusager($piUsager) {
		$this->iusager = $piUsager;
		return $this;
	}

	public function getIusager() {
		return $this->iusager;
	}

}
