<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * intervenantsPermis
 *
 * @ORM\Table(name="paa.intervenants_permis", uniqueConstraints={@ORM\UniqueConstraint(name="u_intervenants_permis_iintervenant&ipermis", columns={"iintervenant", "ipermis"})}, indexes={@ORM\Index(name="intervenants_permis_ip_permis", columns={"ipermis"}), @ORM\Index(name="intervenants_permis_intervenan", columns={"iintervenant"})})
 * @ORM\Entity
 */
class intervenantsPermis {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.intervenants_permis_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var \intervenants
	 *
	 * @ORM\ManyToOne(targetEntity="intervenants")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iintervenant", referencedColumnName="iid_intervenant")
	 * })
	 */
	private $iintervenant;

	public function setIintervenant($piIntervenant) {
		$this->iintervenant = $piIntervenant;
		return $this;
	}

	public function getIintervenant() {
		return $this->iintervenant;
	}

	/**
	 * @var \permis
	 *
	 * @ORM\ManyToOne(targetEntity="permis")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ipermis", referencedColumnName="iid_permis")
	 * })
	 */
	private $ipermis;

	public function setIpermis($piPermis) {
		$this->ipermis = $piPermis;
		return $this;
	}

	public function getIpermis() {
		return $this->ipermis;
	}

}
