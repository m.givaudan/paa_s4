<?php
// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)


namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * compteurs
 *
 * @ORM\Table(name="paa.compteurs", uniqueConstraints={@ORM\UniqueConstraint(name="u_compteurs_cnom_compteur", columns={"cnom_compteur"})})
 * @ORM\Entity
 */
class compteurs
{
    //MC 20200907 DEBUT
    /**
    * Constructor
    */
    public function __construct() {
        $this->listeRegles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    //MC 20200907 FIN
    /**
     * @var integer
     *
     * @ORM\Column(name="iid_compteur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="paa.compteurs_iid_compteur_seq", allocationSize=1, initialValue=1)
     */
    private $iidCompteur = 'compteurs';
    public function setIidcompteur($piIdcompteur){
        $this->iidCompteur = $piIdcompteur;
        return $this;
    }
    public function getIidcompteur() {
        return $this->iidCompteur;
    }
    
    // AV 04/03/2019 début
    public function getId() {
        return $this->iidCompteur;
    }
    // AV 04/03/2019 fin

    /**
     * @var string
     *
     * @ORM\Column(name="cnom_compteur", type="string", length=50, nullable=true)
     */
    private $cnomCompteur = '';
    public function setCnomcompteur($pcNomcompteur){
        $this->cnomCompteur = $pcNomcompteur;
        return $this;
    }
    public function getCnomcompteur() {
        return $this->cnomCompteur;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="mcommentaire", type="text", nullable=true)
     */
    private $mcommentaire = '';
    public function setMcommentaire($pmCommentaire){
        $this->mcommentaire = $pmCommentaire;
        return $this;
    }
    public function getMcommentaire() {
        return $this->mcommentaire;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="itype", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $itype = '1';
    public function setItype($piType){
        $this->itype = $piType;
        return $this;
    }
    public function getItype() {
        return $this->itype;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
     */
    private $ctypeRes = '';
    public function setCtyperes($pcTyperes){
        $this->ctypeRes = $pcTyperes;
        return $this;
    }
    public function getCtyperes() {
        return $this->ctypeRes;
    }
    
    //MC 20200907 DEBUT
    /**
     * @ORM\OneToMany(targetEntity="compteursRegles", mappedBy="icompteur")
     */
    private $listeRegles;

    /**
     * Get $listeRegles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getlisteRegles() {
            return $this->listeRegles;
    }
    //MC 20200907 FIN


}

