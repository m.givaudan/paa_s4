<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesRessourcesFlags
 *
 * @ORM\Table(name="paa.seances_ressources_flags", uniqueConstraints={@ORM\UniqueConstraint(name="u_seances_ressources_flags_iseance_ressource", columns={"iseance_ressource"})})
 * @ORM\Entity
 */
class seancesRessourcesFlags {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iseanceressourceplantype", type="integer", nullable=true)
	 */
	private $iseanceressourceplantype = '0';

	public function setIseanceressourceplantype($piSeanceressourceplantype) {
		$this->iseanceressourceplantype = $piSeanceressourceplantype;
		return $this;
	}

	public function getIseanceressourceplantype() {
		return $this->iseanceressourceplantype;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconf_plant", type="boolean", nullable=true)
	 */
	private $lconfPlant = false;

	public function setLconfplant($plConfplant) {
		$this->lconfPlant = $plConfplant;
		return $this;
	}

	public function getLconfplant() {
		return $this->lconfPlant;
	}

	/**
	 * @var \seancesRessources
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="seancesRessources")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance_ressource", referencedColumnName="iid_seance_ressource")
	 * })
	 */
	private $iseanceRessource;

	public function setIseanceressource($piSeanceressource) {
		$this->iseanceRessource = $piSeanceressource;
		return $this;
	}

	public function getIseanceressource() {
		return $this->iseanceRessource;
	}

}
