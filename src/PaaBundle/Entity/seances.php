<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

$lsFich = dirname(dirname(__FILE__)) . '/Component/Paa/Paa.php';
require_once $lsFich;

/**
 * seances
 *
 * @ORM\Table(name="paa.tseances", indexes={@ORM\Index(name="acti_realisations_ar_debut", columns={"tdebut"}), @ORM\Index(name="acti_realisations_date_maj", columns={"tmaj"}), @ORM\Index(name="acti_realisations_d_conflit", columns={"tconflit"}), @ORM\Index(name="acti_realisations_acti", columns={"iacti"}), @ORM\Index(name="acti_realisations_ar_fin", columns={"tfin"}), @ORM\Index(name="IDX_38BDCD10F2219B18", columns={"iserie"})})
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\seancesRepository")
 */
class seances {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_seance", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tseances_iid_seance_seq", allocationSize=1, initialValue=1)
	 */
// LG 20190925 old    private $iidSeance = 'acti_realisations';
	private $iidSeance = null;

	public function setIidseance($piIdseance) {
		$this->iidSeance = $piIdseance;
		return $this;
	}

	public function getIidseance() {
		return $this->iidSeance;
	}

	// AV 04/03/2019
	public function getId() {
		return $this->iidSeance;
	}

	/**
	 * @var \activites
	 *
	 * @ORM\ManyToOne(targetEntity="App\PaaBundle\Entity\activites", inversedBy="activites")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite",nullable=false)
	 * })
	 */
// LG 20190718 old    private $iacti;
	private $oActivité;

// LG 20190718 old    public function setIacti($pvActi){
	public function setActivité($pvActi) {
// LG 20190718 old		$this->iacti = $pvActi;
		$this->oActivité = $pvActi;
		return $this;
	}

	public function getActivité() {
// LG 20190718 old        return $this->iacti;
		return $this->oActivité;
	}

// Yohan début   
//     /**
//     * @ ORM\ManyToOne(targetEntity="App\PaaBundle\Entity\activites", inversedBy="activites")
//     * @ ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite",nullable=false)
//     */
//    private $activite; 
//
//    public function getactivite(): /* LG 20190718old ?activite*/activite
//    {
//        return $this->activite;
//    }
//
//    public function setactivite(/* LG 20190718old ?activites*/activite $activite): self
//    {
//        $this->activite = $activite;
//        return $this;
//    }
// Yohan fin	

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebut", type="datetime", nullable=true)
	 */
	private $tdebut;

	public function setTdebut($ptDebut) {
		$this->tdebut = $ptDebut;
		return $this;
	}

	public function getTdebut() {
		return $this->tdebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tfin", type="datetime", nullable=true)
	 */
	private $tfin;

	public function setTfin($ptFin) {
		$this->tfin = $ptFin;
		return $this;
	}

	public function getTfin() {
		return $this->tfin;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="llblauto", type="boolean", nullable=true)
	 */
	private $llblauto = true;

	public function setLlblauto($plLblauto) {
		$this->llblauto = $plLblauto;
		return $this;
	}

	public function getLlblauto() {
		return $this->llblauto;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltousplans", type="boolean", nullable=true)
	 */
	private $ltousplans = true;

	public function setLtousplans($plTousplans) {
		$this->ltousplans = $plTousplans;
		return $this;
	}

	public function getLtousplans() {
		return $this->ltousplans;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj", type="datetime", nullable=true)
	 */
	private $tmaj;

	public function setTmaj($ptMaj) {
		$this->tmaj = $ptMaj;
		return $this;
	}

	public function getTmaj() {
		return $this->tmaj;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tconflit", type="datetime", nullable=true)
	 */
	private $tconflit;

	public function setTconflit($ptConflit) {
		$this->tconflit = $ptConflit;
		return $this;
	}

	public function getTconflit() {
		return $this->tconflit;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="linclutpause", type="boolean", nullable=true)
	 */
	private $linclutpause = false;

	public function setLinclutpause($plInclutpause) {
		$this->linclutpause = $plInclutpause;
		return $this;
	}

	public function getLinclutpause() {
		return $this->linclutpause;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mlstdates", type="text", nullable=true)
	 */
	private $mlstdates = '';

	public function setMlstdates($pmLstdates) {
		$this->mlstdates = $pmLstdates;
		return $this;
	}

	public function getMlstdates() {
		return $this->mlstdates;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="llibre1", type="boolean", nullable=true)
	 */
	private $llibre1 = false;

	public function setLlibre1($plLibre1) {
		$this->llibre1 = $plLibre1;
		return $this;
	}

	public function getLlibre1() {
		return $this->llibre1;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="llibre2", type="boolean", nullable=true)
	 */
	private $llibre2 = false;

	public function setLlibre2($plLibre2) {
		$this->llibre2 = $plLibre2;
		return $this;
	}

	public function getLlibre2() {
		return $this->llibre2;
	}

	/**
	 * @var \seriesseances
	 *
	 * @ORM\ManyToOne(targetEntity="seriesseances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iserie", referencedColumnName="iid_serie")
	 * })
	 */
	private $iserie;

	public function setIserie($piSerie) {
		$this->iserie = $piSerie;
		return $this;
	}

	public function getIserie() {
		return $this->iserie;
	}

	public function getIcouleur() {
// LG 20190718 old        return $this->iacti->getIcouleur();
		return $this->oActivité->getIcouleur();
	}

	public function getCcouleur() {
		return "#" . color_DecToHex($this->getIcouleur());
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mlibelle", type="text", nullable=true)
	 */
	private $mlibelle = '';

	public function setMlibelle($pmLibelle) {
		$this->mlibelle = $pmLibelle;
		return $this;
	}

	public function getMlibelle() {
		// if ($this->mlibelle) 
		return $this->mlibelle;
		// else return "VoiciLeLibellé" ;
	}

}
