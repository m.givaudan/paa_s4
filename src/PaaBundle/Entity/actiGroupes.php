<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * actiGroupes
 *
 * @ORM\Table(name="paa.acti_groupes", uniqueConstraints={@ORM\UniqueConstraint(name="u_acti_groupes_iacti&igroupe", columns={"iacti", "igroupe"})}, indexes={@ORM\Index(name="acti_groupes_ag_groupe", columns={"igroupe"}), @ORM\Index(name="acti_groupes_acti", columns={"iacti"})})
 * @ORM\Entity
 */
class actiGroupes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="igroupe", type="integer", nullable=false)
	 * @ORM\Id
// LG 20200824 début : retour à l'annotation d'origine, sans quoi Symfony renvoie l'erreur "L'entité a une clé multiple mais elle est auto-incrémentée"
//	 * @ ORM\GeneratedValue(strategy="NONE")
	 * @ ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
// LG 20200811 fin
	 * @ORM\GeneratedValue(strategy="NONE")
// LG 20200824 fin
	 */
	private $igroupe = '0';

	public function setIgroupe($piGroupe) {
		$this->igroupe = $piGroupe;
		return $this;
	}

	public function getIgroupe() {
		return $this->igroupe;
	}

	/**
	 * @var \activites
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="activites")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")
	 * })
	 */
	private $iacti;

	public function setIacti($piActi) {
		$this->iacti = $piActi;
		return $this;
	}

	public function getIacti() {
		return $this->iacti;
	}

}
