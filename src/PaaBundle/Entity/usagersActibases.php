<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersActibases
 *
 * @ORM\Table(name="paa.usagers_actibases", uniqueConstraints={@ORM\UniqueConstraint(name="u_usagers_actibases_iusager&iactibase", columns={"iactibase", "iusager"})}, indexes={@ORM\Index(name="usagers_actibases_ua_periode", columns={"iperiode"}), @ORM\Index(name="usagers_actibases_nivo_acti", columns={"cnivoacti"}), @ORM\Index(name="usagers_actibases_acti_base", columns={"iactibase"}), @ORM\Index(name="usagers_actibases_ua_usager", columns={"iusager"})})
 * @ORM\Entity
 */
class usagersActibases {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnivoacti", type="string", length=1, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 */
	private $cnivoacti = 'A';

	public function setCnivoacti($pcNivoacti) {
		$this->cnivoacti = $pcNivoacti;
		return $this;
	}

	public function getCnivoacti() {
		return $this->cnivoacti;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="iperiode", type="decimal", precision=5, scale=0, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 */
	private $iperiode = '0.0';

	public function setIperiode($piPeriode) {
		$this->iperiode = $piPeriode;
		return $this;
	}

	public function getIperiode() {
		return $this->iperiode;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nprevues", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $nprevues = '0.0';

	public function setNprevues($pnPrevues) {
		$this->nprevues = $pnPrevues;
		return $this;
	}

	public function getNprevues() {
		return $this->nprevues;
	}

	/**
	 * @var \actibases
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="actibases")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iactibase", referencedColumnName="iid_actibase")
	 * })
	 */
	private $iactibase;

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iusager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iusager;

	public function setIusager($piUsager) {
		$this->iusager = $piUsager;
		return $this;
	}

	public function getIusager() {
		return $this->iusager;
	}

}
