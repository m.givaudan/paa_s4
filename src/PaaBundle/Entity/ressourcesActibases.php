<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ressourcesActibases
 *
 * @ORM\Table(name="paa.ressources_actibases", uniqueConstraints={@ORM\UniqueConstraint(name="u_ressources_actibases_ctype_res&iid_res&iactibase", columns={"ctype_res", "iactibase", "iid_res"})}, indexes={@ORM\Index(name="ressources_actibases_type_res", columns={"ctype_res"}), @ORM\Index(name="ressources_actibases_actibase", columns={"iactibase"}), @ORM\Index(name="ressources_actibases_id_res", columns={"iid_res"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\ressourcesActibasesRepository")
 */
class ressourcesActibases {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_ressource_actibase", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_actibases_iid_ressource_actibase_seq", allocationSize=1, initialValue=1)
	 */
	private $iidRessourceActibase = 'ressources_actibases';

	public function setIidressourceactibase($piIdressourceactibase) {
		$this->iidRessourceActibase = $piIdressourceactibase;
		return $this;
	}

	public function getIidressourceactibase() {
		return $this->iidRessourceActibase;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidRessourceActibase;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iactibase", type="integer", nullable=true)
	 */
	private $iactibase = '0';

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="icapacite", type="decimal", precision=5, scale=0, nullable=true)
	 */
	private $icapacite;

	public function setIcapacite($piCapacite) {
		$this->icapacite = $piCapacite;
		return $this;
	}

	public function getIcapacite() {
		return $this->icapacite;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bprescription", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bprescription;

	public function setBprescription($pbPrescription) {
		$this->bprescription = $pbPrescription;
		return $this;
	}

	public function getBprescription() {
		return $this->bprescription;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bprescriptionsemaine", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bprescriptionsemaine;

	public function setBprescriptionsemaine($pbPrescriptionsemaine) {
		$this->bprescriptionsemaine = $pbPrescriptionsemaine;
		return $this;
	}

	public function getBprescriptionsemaine() {
		return $this->bprescriptionsemaine;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iacticorr", type="integer", nullable=true)
	 */
	private $iacticorr;

	public function setIacticorr($piActicorr) {
		$this->iacticorr = $piActicorr;
		return $this;
	}

	public function getIacticorr() {
		return $this->iacticorr;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mformulemin", type="text", nullable=true)
	 */
	private $mformulemin;

	public function setMformulemin($pmFormulemin) {
		$this->mformulemin = $pmFormulemin;
		return $this;
	}

	public function getMformulemin() {
		return $this->mformulemin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mformulemax", type="text", nullable=true)
	 */
	private $mformulemax;

	public function setMformulemax($pmFormulemax) {
		$this->mformulemax = $pmFormulemax;
		return $this;
	}

	public function getMformulemax() {
		return $this->mformulemax;
	}

}
