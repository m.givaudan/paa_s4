<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * duDocuments
 *
 * @ORM\Table(name="paa.du_documents", indexes={@ORM\Index(name="du_documents_fk_creat", columns={"icreateur"}), @ORM\Index(name="du_documents_date", columns={"ddate"})})
 * @ORM\Entity
 */
class duDocuments {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_document", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.du_documents_iid_document_seq", allocationSize=1, initialValue=1)
	 */
	private $iidDocument;

	public function setIiddocument($piIddocument) {
		$this->iidDocument = $piIddocument;
		return $this;
	}

	public function getIiddocument() {
		return $this->iidDocument;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidDocument;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="itype", type="integer", nullable=true)
	 */
	private $itype = '1';

	public function setItype($piType) {
		$this->itype = $piType;
		return $this;
	}

	public function getItype() {
		return $this->itype;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icreateur", type="integer", nullable=true)
	 */
	private $icreateur = '0';

	public function setIcreateur($piCreateur) {
		$this->icreateur = $piCreateur;
		return $this;
	}

	public function getIcreateur() {
		return $this->icreateur;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate", type="date", nullable=true)
	 */
	private $ddate = 'date()';

	public function setDdate($pdDate) {
		$this->ddate = $pdDate;
		return $this;
	}

	public function getDdate() {
		return $this->ddate;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clibelle", type="string", length=254, nullable=true)
	 */
	private $clibelle = '';

	public function setClibelle($pcLibelle) {
		$this->clibelle = $pcLibelle;
		return $this;
	}

	public function getClibelle() {
		return $this->clibelle;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="linactif", type="boolean", nullable=true)
	 */
	private $linactif = false;

	public function setLinactif($plInactif) {
		$this->linactif = $plInactif;
		return $this;
	}

	public function getLinactif() {
		return $this->linactif;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mresume", type="text", nullable=true)
	 */
	private $mresume = '';

	public function setMresume($pmResume) {
		$this->mresume = $pmResume;
		return $this;
	}

	public function getMresume() {
		return $this->mresume;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mchemin", type="text", nullable=true)
	 */
	private $mchemin = '';

	public function setMchemin($pmChemin) {
		$this->mchemin = $pmChemin;
		return $this;
	}

	public function getMchemin() {
		return $this->mchemin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iforecolor", type="integer", nullable=true)
	 */
	private $iforecolor = '0';

	public function setIforecolor($piForecolor) {
		$this->iforecolor = $piForecolor;
		return $this;
	}

	public function getIforecolor() {
		return $this->iforecolor;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ibackcolor", type="integer", nullable=true)
	 */
	private $ibackcolor = 'rgb(255, 255, 255)';

	public function setIbackcolor($piBackcolor) {
		$this->ibackcolor = $piBackcolor;
		return $this;
	}

	public function getIbackcolor() {
		return $this->ibackcolor;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mfichthumbnail", type="text", nullable=true)
	 */
	private $mfichthumbnail = '';

	public function setMfichthumbnail($pmFichthumbnail) {
		$this->mfichthumbnail = $pmFichthumbnail;
		return $this;
	}

	public function getMfichthumbnail() {
		return $this->mfichthumbnail;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mfichpdf", type="text", nullable=true)
	 */
	private $mfichpdf = '';

	public function setMfichpdf($pmFichpdf) {
		$this->mfichpdf = $pmFichpdf;
		return $this;
	}

	public function getMfichpdf() {
		return $this->mfichpdf;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="inbpagespdf", type="integer", nullable=true)
	 */
	private $inbpagespdf = '0';

	public function setInbpagespdf($piNbpagespdf) {
		$this->inbpagespdf = $piNbpagespdf;
		return $this;
	}

	public function getInbpagespdf() {
		return $this->inbpagespdf;
	}

}
