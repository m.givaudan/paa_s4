<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesTrad
 *
 * @ORM\Table(name="paa_cache.seances_trad", uniqueConstraints={@ORM\UniqueConstraint(name="u_seances_trad_iid_seance", columns={"iid_seance"})}, indexes={@ORM\Index(name="seances_trad_id_acti", columns={"iacti"}), @ORM\Index(name="seances_trad_id_seance", columns={"iid_seance"}), @ORM\Index(name="seances_trad_st_debut", columns={"tdebut"}), @ORM\Index(name="seances_trad_st_fin", columns={"tfin"}), @ORM\Index(name="IDX_7D8F846655B7AE9A", columns={"iactibase"})})
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\seancesTradRepository")
 */
class seancesTrad {

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebut", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tdebut = '0001-01-01';
	private $tdebut = null;

	public function setTdebut($ptDebut) {
		$this->tdebut = $ptDebut;
		return $this;
	}

	public function getTdebut() {
		return $this->tdebut;
	}

	/**
	 * @var \DateTime
	 * @ORM\Column(name="tfin", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tfin = '0001-01-01';
	private $tfin = null;

	public function setTfin($ptFin) {
		$this->tfin = $ptFin;
		return $this;
	}

	public function getTfin() {
		return $this->tfin;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltousplans", type="boolean", nullable=true)
	 */
	private $ltousplans = false;

	public function setLtousplans($plTousplans) {
		$this->ltousplans = $plTousplans;
		return $this;
	}

	public function getLtousplans() {
		return $this->ltousplans;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="ndebut", type="float", precision=10, scale=0, nullable=true)
	 */
	private $ndebut = '0.0';

	public function setNdebut($pnDebut) {
		$this->ndebut = $pnDebut;
		return $this;
	}

	public function getNdebut() {
		return $this->ndebut;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nfin", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nfin = '0.0';

	public function setNfin($pnFin) {
		$this->nfin = $pnFin;
		return $this;
	}

	public function getNfin() {
		return $this->nfin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cactibase", type="string", length=50, nullable=true)
	 */
	private $cactibase = '';

	public function setCactibase($pcActibase) {
		$this->cactibase = $pcActibase;
		return $this;
	}

	public function getCactibase() {
		return $this->cactibase;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icoulactibase", type="integer", nullable=true)
	 */
	private $icoulactibase = '0';

	public function setIcoulactibase($piCoulactibase) {
		$this->icoulactibase = $piCoulactibase;
		return $this;
	}

	public function getIcoulactibase() {
		return $this->icoulactibase;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="musagers", type="text", nullable=true)
	 */
	private $musagers = '';

	public function setMusagers($pmUsagers) {
		$this->musagers = $pmUsagers;
		return $this;
	}

	public function getMusagers() {
		return $this->musagers;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="msalles", type="text", nullable=true)
	 */
	private $msalles = '';

	public function setMsalles($pmSalles) {
		$this->msalles = $pmSalles;
		return $this;
	}

	public function getMsalles() {
		return $this->msalles;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mintervenants", type="text", nullable=true)
	 */
	private $mintervenants = '';

	public function setMintervenants($pmIntervenants) {
		$this->mintervenants = $pmIntervenants;
		return $this;
	}

	public function getMintervenants() {
		return $this->mintervenants;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="metablissements", type="text", nullable=true)
	 */
	private $metablissements = '';

	public function setMetablissements($pmEtablissements) {
		$this->metablissements = $pmEtablissements;
		return $this;
	}

	public function getMetablissements() {
		return $this->metablissements;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mgroupes", type="text", nullable=true)
	 */
	private $mgroupes = '';

	public function setMgroupes($pmGroupes) {
		$this->mgroupes = $pmGroupes;
		return $this;
	}

	public function getMgroupes() {
		return $this->mgroupes;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mlibelle", type="text", nullable=true)
	 */
	private $mlibelle = '';

	public function setMlibelle($pmLibelle) {
		$this->mlibelle = $pmLibelle;
		return $this;
	}

	public function getMlibelle() {
		return $this->mlibelle;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmaj = '0001-01-01';
	private $tmaj = null;

	public function setTmaj($ptMaj) {
		$this->tmaj = $ptMaj;
		return $this;
	}

	public function getTmaj() {
		return $this->tmaj;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj_seance", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmajSeance = '0001-01-01';
	private $tmajSeance = null;

	public function setTmajseance($ptMajseance) {
		$this->tmajSeance = $ptMajseance;
		return $this;
	}

	public function getTmajseance() {
		return $this->tmajSeance;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lajour", type="boolean", nullable=true)
	 */
	private $lajour = false;

	public function setLajour($plAjour) {
		$this->lajour = $plAjour;
		return $this;
	}

	public function getLajour() {
		return $this->lajour;
	}

	/**
	 * @var \activites
	 *
	 * @ORM\ManyToOne(targetEntity="activites")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")
	 * })
	 */
	private $iacti;

	public function setIacti($piActi) {
		$this->iacti = $piActi;
		return $this;
	}

	public function getIacti() {
		return $this->iacti;
	}

	/**
	 * @var \actibases
	 *
	 * @ORM\ManyToOne(targetEntity="actibases")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iactibase", referencedColumnName="iid_actibase")
	 * })
	 */
	private $iactibase;

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iid_seance", referencedColumnName="iid_seance")
	 * })
	 */
	private $iidSeance;

	public function setIidseance($piIdseance) {
		$this->iidSeance = $piIdseance;
		return $this;
	}

	public function getIidseance() {
		return $this->iidSeance;
	}

}
