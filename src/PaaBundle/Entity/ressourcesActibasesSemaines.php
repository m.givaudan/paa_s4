<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ressourcesActibasesSemaines
 *
 * @ORM\Table(name="paa.ressources_actibases_semaines", uniqueConstraints={@ORM\UniqueConstraint(name="u_ressources_actibases_semaines_ctype_res&iid_res&iactibase&ise", columns={"ctype_res", "iactibase", "iid_res", "isem"})}, indexes={@ORM\Index(name="ressources_actibases_semaines_iid_res", columns={"iid_res"}), @ORM\Index(name="ressources_actibases_semaines_iactibase", columns={"iactibase"}), @ORM\Index(name="ressources_actibases_semaines_isem", columns={"isem"})})
 * @ORM\Entity
 */
class ressourcesActibasesSemaines {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_actibases_semaines_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="isem", type="integer", nullable=true)
	 */
	private $isem = '0';

	public function setIsem($piSem) {
		$this->isem = $piSem;
		return $this;
	}

	public function getIsem() {
		return $this->isem;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bprevues", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bprevues;

	public function setBprevues($pbPrevues) {
		$this->bprevues = $pbPrevues;
		return $this;
	}

	public function getBprevues() {
		return $this->bprevues;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj_prev", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmajPrev = '0001-01-01';
	private $tmajPrev = null;

	public function setTmajprev($ptMajprev) {
		$this->tmajPrev = $ptMajprev;
		return $this;
	}

	public function getTmajprev() {
		return $this->tmajPrev;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bprevuesglobales", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bprevuesglobales;

	public function setBprevuesglobales($pbPrevuesglobales) {
		$this->bprevuesglobales = $pbPrevuesglobales;
		return $this;
	}

	public function getBprevuesglobales() {
		return $this->bprevuesglobales;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bfaites", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bfaites;

	public function setBfaites($pbFaites) {
		$this->bfaites = $pbFaites;
		return $this;
	}

	public function getBfaites() {
		return $this->bfaites;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bfaitesglobales", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bfaitesglobales;

	public function setBfaitesglobales($pbFaitesglobales) {
		$this->bfaitesglobales = $pbFaitesglobales;
		return $this;
	}

	public function getBfaitesglobales() {
		return $this->bfaitesglobales;
	}

	/**
	 * @var \actibases
	 *
	 * @ORM\ManyToOne(targetEntity="actibases")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iactibase", referencedColumnName="iid_actibase")
	 * })
	 */
	private $iactibase;

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

}
