<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * paaPara
 *
 * @ORM\Table(name="paa.paa_para", uniqueConstraints={@ORM\UniqueConstraint(name="unique_paa_para_iid_parametre", columns={"iid_parametre"})}, indexes={@ORM\Index(name="paa_para_iidmachine", columns={"iidmachine"}), @ORM\Index(name="paa_para_iiduser", columns={"iiduser"})})
 * @ORM\Entity
 */
class paaPara {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.paa_para_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="variable", type="string", length=100, nullable=true)
	 */
	private $variable = '';

	public function setVariable($pvAriable) {
		$this->variable = $pvAriable;
		return $this;
	}

	public function getVariable() {
		return $this->variable;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="varcar", type="string", length=254, nullable=true)
	 */
	private $varcar = '';

	public function setVarcar($pvArcar) {
		$this->varcar = $pvArcar;
		return $this;
	}

	public function getVarcar() {
		return $this->varcar;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="varnum", type="decimal", precision=20, scale=5, nullable=true)
	 */
	private $varnum = '0.0';

	public function setVarnum($pvArnum) {
		$this->varnum = $pvArnum;
		return $this;
	}

	public function getVarnum() {
		return $this->varnum;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="vardate", type="date", nullable=true)
	 */
// LG 20200811 old	private $vardate = '0001-01-01';
	private $vardate = null;

	public function setVardate($pvArdate) {
		$this->vardate = $pvArdate;
		return $this;
	}

	public function getVardate() {
		return $this->vardate;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="varbool", type="boolean", nullable=true)
	 */
	private $varbool = false;

	public function setVarbool($pvArbool) {
		$this->varbool = $pvArbool;
		return $this;
	}

	public function getVarbool() {
		return $this->varbool;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="var_memo", type="text", nullable=true)
	 */
	private $varMemo = '';

	public function setVarmemo($pvArmemo) {
		$this->varMemo = $pvArmemo;
		return $this;
	}

	public function getVarmemo() {
		return $this->varMemo;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="vartype", type="string", length=1, nullable=true)
	 */
	private $vartype = '';

	public function setVartype($pvArtype) {
		$this->vartype = $pvArtype;
		return $this;
	}

	public function getVartype() {
		return $this->vartype;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iiduser", type="integer", nullable=true)
	 */
	private $iiduser = '0';

	public function setIiduser($piIduser) {
		$this->iiduser = $piIduser;
		return $this;
	}

	public function getIiduser() {
		return $this->iiduser;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iidmachine", type="integer", nullable=true)
	 */
	private $iidmachine = '0';

	public function setIidmachine($piIdmachine) {
		$this->iidmachine = $piIdmachine;
		return $this;
	}

	public function getIidmachine() {
		return $this->iidmachine;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_parametre", type="integer", nullable=false)
	 */
	private $iidParametre;

	public function setIidparametre($piIdparametre) {
		$this->iidParametre = $piIdparametre;
		return $this;
	}

	public function getIidparametre() {
		return $this->iidParametre;
	}

}
