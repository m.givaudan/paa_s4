<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesRessourcesPlages
 *
 * @ORM\Table(name="paa.seances_ressources_plages", indexes={@ORM\Index(name="seances_ressources_plages_sr", columns={"iseance_ressource"})})
 * @ORM\Entity
 */
class seancesRessourcesPlages {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_seance_ressource_plage", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.seances_ressources_plages_iid_seance_ressource_plage_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSeanceRessourcePlage;

	public function setIidseanceressourceplage($piIdseanceressourceplage) {
		$this->iidSeanceRessourcePlage = $piIdseanceressourceplage;
		return $this;
	}

	public function getIidseanceressourceplage() {
		return $this->iidSeanceRessourcePlage;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSeanceRessourcePlage;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebut", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tdebut = '0001-01-01';
	private $tdebut = null;

	public function setTdebut($ptDebut) {
		$this->tdebut = $ptDebut;
		return $this;
	}

	public function getTdebut() {
		return $this->tdebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tfin", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tfin = '0001-01-01';
	private $tfin = null;

	public function setTfin($ptFin) {
		$this->tfin = $ptFin;
		return $this;
	}

	public function getTfin() {
		return $this->tfin;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebutseance", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tdebutseance = '0001-01-01';
	private $tdebutseance = null;

	public function setTdebutseance($ptDebutseance) {
		$this->tdebutseance = $ptDebutseance;
		return $this;
	}

	public function getTdebutseance() {
		return $this->tdebutseance;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tfinseance", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tfinseance = '0001-01-01';
	private $tfinseance = null;

	public function setTfinseance($ptFinseance) {
		$this->tfinseance = $ptFinseance;
		return $this;
	}

	public function getTfinseance() {
		return $this->tfinseance;
	}

	/**
	 * @var \seancesRessources
	 *
	 * @ORM\ManyToOne(targetEntity="seancesRessources")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance_ressource", referencedColumnName="iid_seance_ressource")
	 * })
	 */
	private $iseanceRessource;

	public function setIseanceressource($piSeanceressource) {
		$this->iseanceRessource = $piSeanceressource;
		return $this;
	}

	public function getIseanceressource() {
		return $this->iseanceRessource;
	}

}
