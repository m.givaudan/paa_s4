<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersGroupes
 *
 * @ORM\Table(name="paa.usagers_groupes", indexes={@ORM\Index(name="usagers_groupes_tusager", columns={"iusager"}), @ORM\Index(name="usagers_groupes_tgroupe", columns={"igroupe"}), @ORM\Index(name="usagers_groupes_tdebut", columns={"ddebut"})})
 * @ORM\Entity
 */
class usagersGroupes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_usager_groupe", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.usagers_groupes_iid_usager_groupe_seq", allocationSize=1, initialValue=1)
	 */
	private $iidUsagerGroupe = 'usagers_groupes';

	public function setIidusagergroupe($piIdusagergroupe) {
		$this->iidUsagerGroupe = $piIdusagergroupe;
		return $this;
	}

	public function getIidusagergroupe() {
		return $this->iidUsagerGroupe;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidUsagerGroupe;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddebut", type="date", nullable=true)
	 */
	private $ddebut;

	public function setDdebut($pdDebut) {
		$this->ddebut = $pdDebut;
		return $this;
	}

	public function getDdebut() {
		return $this->ddebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfin", type="date", nullable=true)
	 */
	private $dfin;

	public function setDfin($pdFin) {
		$this->dfin = $pdFin;
		return $this;
	}

	public function getDfin() {
		return $this->dfin;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bhrsobjectif", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bhrsobjectif = '0.0';

	public function setBhrsobjectif($pbHrsobjectif) {
		$this->bhrsobjectif = $pbHrsobjectif;
		return $this;
	}

	public function getBhrsobjectif() {
		return $this->bhrsobjectif;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bhrsfaitesavant", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bhrsfaitesavant = '0.0';

	public function setBhrsfaitesavant($pbHrsfaitesavant) {
		$this->bhrsfaitesavant = $pbHrsfaitesavant;
		return $this;
	}

	public function getBhrsfaitesavant() {
		return $this->bhrsfaitesavant;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lparticipeauxseancesdugroupe", type="boolean", nullable=true)
	 */
	private $lparticipeauxseancesdugroupe = true;

	public function setLparticipeauxseancesdugroupe($plParticipeauxseancesdugroupe) {
		$this->lparticipeauxseancesdugroupe = $plParticipeauxseancesdugroupe;
		return $this;
	}

	public function getLparticipeauxseancesdugroupe() {
		return $this->lparticipeauxseancesdugroupe;
	}

        /**
	 * @var \groupes
	 *
	 *   @ORM\ManyToOne(targetEntity="groupes")
	 * 
	 *   @ORM\JoinColumn(name="igroupe", referencedColumnName="iid_groupe")
	 * 
	 */
	private $igroupe;

	public function setIgroupe($piGroupe) {
		$this->igroupe = $piGroupe;
		return $this;
	}

	public function getIgroupe() {
		return $this->igroupe;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers", inversedBy="listeUsagersGroupes")
	 * 
	 * @ORM\JoinColumn(name="iusager", referencedColumnName="iid_usager")
	 * 
	 */
	private $iusager;

	public function setIusager($piUsager) {
		$this->iusager = $piUsager;
		return $this;
	}

	public function getIusager() {
		return $this->iusager;
	}

}
