<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesPresabs
 *
 * @ORM\Table(name="paa.seances_presabs", indexes={@ORM\Index(name="seances_presabs_service", columns={"iservice"}), @ORM\Index(name="seances_presabs_seance", columns={"iseance"})})
 * @ORM\Entity
 */
class seancesPresabs {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_seance_presabs", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.seances_presabs_iid_seance_presabs_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSeancePresabs;

	public function setIidseancepresabs($piIdseancepresabs) {
		$this->iidSeancePresabs = $piIdseancepresabs;
		return $this;
	}

	public function getIidseancepresabs() {
		return $this->iidSeancePresabs;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSeancePresabs;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseance;

	public function setIseance($piSeance) {
		$this->iseance = $piSeance;
		return $this;
	}

	public function getIseance() {
		return $this->iseance;
	}

	/**
	 * @var \services
	 *
	 * @ORM\ManyToOne(targetEntity="services")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iservice", referencedColumnName="iid_service")
	 * })
	 */
	private $iservice;

	public function setIservice($piService) {
		$this->iservice = $piService;
		return $this;
	}

	public function getIservice() {
		return $this->iservice;
	}

}
