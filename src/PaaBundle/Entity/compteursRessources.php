<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * compteursRessources
 *
 * @ORM\Table(name="paa.compteurs_ressources", uniqueConstraints={@ORM\UniqueConstraint(name="u_compteurs_ressources_ctype_res&iid_res&icompteur&ddate", columns={"ctype_res", "ddate", "icompteur", "iid_res"})}, indexes={@ORM\Index(name="compteurs_ressources_idate", columns={"ddate"}), @ORM\Index(name="compteurs_ressources_id_res", columns={"iid_res"}), @ORM\Index(name="compteurs_ressources_cr_compt", columns={"icompteur"})})
 * @ORM\Entity
 */
class compteursRessources {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_compteur_ressource", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.compteurs_ressources_iid_compteur_ressource_seq", allocationSize=1, initialValue=1)
	 */
	private $iidCompteurRessource = 'compteurs_ressources';

	public function setIidcompteurressource($piIdcompteurressource) {
		$this->iidCompteurRessource = $piIdcompteurressource;
		return $this;
	}

	public function getIidcompteurressource() {
		return $this->iidCompteurRessource;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidCompteurRessource;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddate", type="date", nullable=true)
	 */
	private $ddate = 'date()';

	public function setDdate($pdDate) {
		$this->ddate = $pdDate;
		return $this;
	}

	public function getDdate() {
		return $this->ddate;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bvaleur", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bvaleur = '0.0';

	public function setBvaleur($pbValeur) {
		$this->bvaleur = $pbValeur;
		return $this;
	}

	public function getBvaleur() {
		return $this->bvaleur;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="loffset", type="boolean", nullable=true)
	 */
	private $loffset = false;

	public function setLoffset($plOffset) {
		$this->loffset = $plOffset;
		return $this;
	}

	public function getLoffset() {
		return $this->loffset;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \compteurs
	 *
	 * @ORM\ManyToOne(targetEntity="compteurs")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="icompteur", referencedColumnName="iid_compteur")
	 * })
	 */
	private $icompteur;

	public function setIcompteur($piCompteur) {
		$this->icompteur = $piCompteur;
		return $this;
	}

	public function getIcompteur() {
		return $this->icompteur;
	}

}
