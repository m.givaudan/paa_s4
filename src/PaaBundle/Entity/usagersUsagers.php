<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersUsagers
 *
 * @ORM\Table(name="paa.usagers_usagers", indexes={@ORM\Index(name="usagers_usagers_usager2", columns={"iusager2"}), @ORM\Index(name="usagers_usagers_usager1", columns={"iusager1"})})
 * @ORM\Entity
 */
class usagersUsagers {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.usagers_usagers_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="itypelien", type="integer", nullable=true)
	 */
	private $itypelien = '1';

	public function setItypelien($piTypelien) {
		$this->itypelien = $piTypelien;
		return $this;
	}

	public function getItypelien() {
		return $this->itypelien;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iusager1", referencedColumnName="iid_usager")
	 * })
	 */
	private $iusager1;

	public function setIusager1($piUsager1) {
		$this->iusager1 = $piUsager1;
		return $this;
	}

	public function getIusager1() {
		return $this->iusager1;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iusager2", referencedColumnName="iid_usager")
	 * })
	 */
	private $iusager2;

	public function setIusager2($piUsager2) {
		$this->iusager2 = $piUsager2;
		return $this;
	}

	public function getIusager2() {
		return $this->iusager2;
	}

}
