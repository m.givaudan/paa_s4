<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * referents
 *
 * @ORM\Table(name="paa.referents", indexes={@ORM\Index(name="referents_r_usager", columns={"iusager"})})
 * @ORM\Entity
 */
class referents {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_referent", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.referents_iid_referent_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
	private $iidReferent = 'referents';

	public function setIidreferent($piIdreferent) {
		$this->iidReferent = $piIdreferent;
		return $this;
	}

	public function getIidreferent() {
		return $this->iidReferent;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidReferent;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctypelien", type="string", length=50, nullable=true)
	 */
	private $ctypelien = '';

	public function setCtypelien($pcTypelien) {
		$this->ctypelien = $pcTypelien;
		return $this;
	}

	public function getCtypelien() {
		return $this->ctypelien;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cprenom", type="string", length=50, nullable=true)
	 */
	private $cprenom = '';

	public function setCprenom($pcPrenom) {
		$this->cprenom = $pcPrenom;
		return $this;
	}

	public function getCprenom() {
		return $this->cprenom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse", type="string", length=254, nullable=true)
	 */
	private $cadresse = '';

	public function setCadresse($pcAdresse) {
		$this->cadresse = $pcAdresse;
		return $this;
	}

	public function getCadresse() {
		return $this->cadresse;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccp", type="string", length=10, nullable=true)
	 */
	private $ccp = '';

	public function setCcp($pcCp) {
		$this->ccp = $pcCp;
		return $this;
	}

	public function getCcp() {
		return $this->ccp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cville", type="string", length=50, nullable=true)
	 */
	private $cville = '';

	public function setCville($pcVille) {
		$this->cville = $pcVille;
		return $this;
	}

	public function getCville() {
		return $this->cville;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cportable", type="string", length=20, nullable=true)
	 */
	private $cportable = '';

	public function setCportable($pcPortable) {
		$this->cportable = $pcPortable;
		return $this;
	}

	public function getCportable() {
		return $this->cportable;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cteldomicile", type="string", length=20, nullable=true)
	 */
	private $cteldomicile = '';

	public function setCteldomicile($pcTeldomicile) {
		$this->cteldomicile = $pcTeldomicile;
		return $this;
	}

	public function getCteldomicile() {
		return $this->cteldomicile;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctelbureau", type="string", length=20, nullable=true)
	 */
	private $ctelbureau = '';

	public function setCtelbureau($pcTelbureau) {
		$this->ctelbureau = $pcTelbureau;
		return $this;
	}

	public function getCtelbureau() {
		return $this->ctelbureau;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmel", type="string", length=50, nullable=true)
	 */
	private $cmel = '';

	public function setCmel($pcMel) {
		$this->cmel = $pcMel;
		return $this;
	}

	public function getCmel() {
		return $this->cmel;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltuteur", type="boolean", nullable=true)
	 */
	private $ltuteur = false;

	public function setLtuteur($plTuteur) {
		$this->ltuteur = $plTuteur;
		return $this;
	}

	public function getLtuteur() {
		return $this->ltuteur;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lautoriserecup", type="boolean", nullable=true)
	 */
	private $lautoriserecup = false;

	public function setLautoriserecup($plAutoriserecup) {
		$this->lautoriserecup = $plAutoriserecup;
		return $this;
	}

	public function getLautoriserecup() {
		return $this->lautoriserecup;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lresidence", type="boolean", nullable=true)
	 */
	private $lresidence = false;

	public function setLresidence($plResidence) {
		$this->lresidence = $plResidence;
		return $this;
	}

	public function getLresidence() {
		return $this->lresidence;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lplacement", type="boolean", nullable=true)
	 */
	private $lplacement = false;

	public function setLplacement($plPlacement) {
		$this->lplacement = $plPlacement;
		return $this;
	}

	public function getLplacement() {
		return $this->lplacement;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccivilite", type="string", length=10, nullable=true)
	 */
	private $ccivilite = '';

	public function setCcivilite($pcCivilite) {
		$this->ccivilite = $pcCivilite;
		return $this;
	}

	public function getCcivilite() {
		return $this->ccivilite;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccommentaire", type="text", nullable=true)
	 */
	private $ccommentaire = '';

	public function setCcommentaire($pcCommentaire) {
		$this->ccommentaire = $pcCommentaire;
		return $this;
	}

	public function getCcommentaire() {
		return $this->ccommentaire;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lsendbull", type="boolean", nullable=true)
	 */
	private $lsendbull = false;

	public function setLsendbull($plSendbull) {
		$this->lsendbull = $plSendbull;
		return $this;
	}

	public function getLsendbull() {
		return $this->lsendbull;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lice", type="boolean", nullable=true)
	 */
	private $lice = false;

	public function setLice($plIce) {
		$this->lice = $plIce;
		return $this;
	}

	public function getLice() {
		return $this->lice;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clienpublipostage", type="string", length=20, nullable=true)
	 */
	private $clienpublipostage = '';

	public function setClienpublipostage($pcLienpublipostage) {
		$this->clienpublipostage = $pcLienpublipostage;
		return $this;
	}

	public function getClienpublipostage() {
		return $this->clienpublipostage;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse2", type="string", length=254, nullable=true)
	 */
	private $cadresse2 = '';

	public function setCadresse2($pcAdresse2) {
		$this->cadresse2 = $pcAdresse2;
		return $this;
	}

	public function getCadresse2() {
		return $this->cadresse2;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactloyer", type="boolean", nullable=true)
	 */
	private $lfactloyer = false;

	public function setLfactloyer($plFactloyer) {
		$this->lfactloyer = $plFactloyer;
		return $this;
	}

	public function getLfactloyer() {
		return $this->lfactloyer;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldotationglobale", type="boolean", nullable=true)
	 */
	private $ldotationglobale = false;

	public function setLdotationglobale($plDotationglobale) {
		$this->ldotationglobale = $plDotationglobale;
		return $this;
	}

	public function getLdotationglobale() {
		return $this->ldotationglobale;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype", type="string", length=25, nullable=true)
	 */
	private $ctype = '';

	public function setCtype($pcType) {
		$this->ctype = $pcType;
		return $this;
	}

	public function getCtype() {
		return $this->ctype;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactprixjournee", type="boolean", nullable=true)
	 */
	private $lfactprixjournee = false;

	public function setLfactprixjournee($plFactprixjournee) {
		$this->lfactprixjournee = $plFactprixjournee;
		return $this;
	}

	public function getLfactprixjournee() {
		return $this->lfactprixjournee;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactreversion", type="boolean", nullable=true)
	 */
	private $lfactreversion = false;

	public function setLfactreversion($plFactreversion) {
		$this->lfactreversion = $plFactreversion;
		return $this;
	}

	public function getLfactreversion() {
		return $this->lfactreversion;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iusager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iusager;

	public function setIusager($piUsager) {
		$this->iusager = $piUsager;
		return $this;
	}

	public function getIusager() {
		return $this->iusager;
	}

}
