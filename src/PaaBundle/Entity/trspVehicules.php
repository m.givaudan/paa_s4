<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * trspVehicules
 *
 * @ORM\Table(name="paa.trsp_vehicules", uniqueConstraints={@ORM\UniqueConstraint(name="u_trsp_vehicules_cimmatriculation", columns={"cimmatriculation"})}, indexes={@ORM\Index(name="trsp_vehicules_tv_marque", columns={"cmarque"}), @ORM\Index(name="trsp_vehicules_tv_societe", columns={"isociete"})})
 * @ORM\Entity
 */
class trspVehicules {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_vehicule", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.trsp_vehicules_iid_vehicule_seq", allocationSize=1, initialValue=1)
	 */
	private $iidVehicule = 'trsp_vehicules';

	public function setIidvehicule($piIdvehicule) {
		$this->iidVehicule = $piIdvehicule;
		return $this;
	}

	public function getIidvehicule() {
		return $this->iidVehicule;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidVehicule;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmodele", type="string", length=50, nullable=true)
	 */
	private $cmodele = '';

	public function setCmodele($pcModele) {
		$this->cmodele = $pcModele;
		return $this;
	}

	public function getCmodele() {
		return $this->cmodele;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmarque", type="string", length=50, nullable=true)
	 */
	private $cmarque = '';

	public function setCmarque($pcMarque) {
		$this->cmarque = $pcMarque;
		return $this;
	}

	public function getCmarque() {
		return $this->cmarque;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="iannee", type="decimal", precision=4, scale=0, nullable=true)
	 */
	private $iannee = '0.0';

	public function setIannee($piAnnee) {
		$this->iannee = $piAnnee;
		return $this;
	}

	public function getIannee() {
		return $this->iannee;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cimmatriculation", type="string", length=15, nullable=true)
	 */
	private $cimmatriculation = '';

	public function setCimmatriculation($pcImmatriculation) {
		$this->cimmatriculation = $pcImmatriculation;
		return $this;
	}

	public function getCimmatriculation() {
		return $this->cimmatriculation;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="inbplaces", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $inbplaces = '0.0';

	public function setInbplaces($piNbplaces) {
		$this->inbplaces = $piNbplaces;
		return $this;
	}

	public function getInbplaces() {
		return $this->inbplaces;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \trspSocietes
	 *
	 * @ORM\ManyToOne(targetEntity="trspSocietes")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="isociete", referencedColumnName="iid_societe")
	 * })
	 */
	private $isociete;

	public function setIsociete($piSociete) {
		$this->isociete = $piSociete;
		return $this;
	}

	public function getIsociete() {
		return $this->isociete;
	}

}
