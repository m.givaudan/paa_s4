<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * gpesIntervActib
 *
 * @ORM\Table(name="paa.gpes_interv_actib", indexes={@ORM\Index(name="gpes_interv_actib_acti_base", columns={"iactibase"}), @ORM\Index(name="gpes_interv_actib_resp", columns={"lresp"}), @ORM\Index(name="gpes_interv_actib_intervenan", columns={"iintervenant"}), @ORM\Index(name="gpes_interv_actib_gia_annee", columns={"iannee"}), @ORM\Index(name="gpes_interv_actib_gia_groupe", columns={"igroupe"})})
 * @ORM\Entity
 */
class gpesIntervActib {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.gpes_interv_actib_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bh_i_simple", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bhISimple = '0.0';

	public function setBhisimple($pbHisimple) {
		$this->bhISimple = $pbHisimple;
		return $this;
	}

	public function getBhisimple() {
		return $this->bhISimple;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bh_i_rgrpt", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bhIRgrpt = '0.0';

	public function setBhirgrpt($pbHirgrpt) {
		$this->bhIRgrpt = $pbHirgrpt;
		return $this;
	}

	public function getBhirgrpt() {
		return $this->bhIRgrpt;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bh_i_pluri", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bhIPluri = '0.0';

	public function setBhipluri($pbHipluri) {
		$this->bhIPluri = $pbHipluri;
		return $this;
	}

	public function getBhipluri() {
		return $this->bhIPluri;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bh_i_rgplur", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bhIRgplur = '0.0';

	public function setBhirgplur($pbHirgplur) {
		$this->bhIRgplur = $pbHirgplur;
		return $this;
	}

	public function getBhirgplur() {
		return $this->bhIRgplur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bh_i_tot", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bhITot = '0.0';

	public function setBhitot($pbHitot) {
		$this->bhITot = $pbHitot;
		return $this;
	}

	public function getBhitot() {
		return $this->bhITot;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lresp", type="boolean", nullable=true)
	 */
	private $lresp = false;

	public function setLresp($plResp) {
		$this->lresp = $plResp;
		return $this;
	}

	public function getLresp() {
		return $this->lresp;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lcoche", type="boolean", nullable=true)
	 */
	private $lcoche = false;

	public function setLcoche($plCoche) {
		$this->lcoche = $plCoche;
		return $this;
	}

	public function getLcoche() {
		return $this->lcoche;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iannee", type="integer", nullable=true)
	 */
	private $iannee = '0';

	public function setIannee($piAnnee) {
		$this->iannee = $piAnnee;
		return $this;
	}

	public function getIannee() {
		return $this->iannee;
	}

	/**
	 * @var \actibases
	 *
	 * @ORM\ManyToOne(targetEntity="actibases")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iactibase", referencedColumnName="iid_actibase")
	 * })
	 */
	private $iactibase;

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

	/**
	 * @var \groupes
	 *
	 * @ORM\ManyToOne(targetEntity="groupes")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="igroupe", referencedColumnName="iid_groupe")
	 * })
	 */
	private $igroupe;

	public function setIgroupe($piGroupe) {
		$this->igroupe = $piGroupe;
		return $this;
	}

	public function getIgroupe() {
		return $this->igroupe;
	}

	/**
	 * @var \intervenants
	 *
	 * @ORM\ManyToOne(targetEntity="intervenants")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iintervenant", referencedColumnName="iid_intervenant")
	 * })
	 */
	private $iintervenant;

	public function setIintervenant($piIntervenant) {
		$this->iintervenant = $piIntervenant;
		return $this;
	}

	public function getIintervenant() {
		return $this->iintervenant;
	}

}
