<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * trspChauffeurs
 *
 * @ORM\Table(name="paa.trsp_chauffeurs", indexes={@ORM\Index(name="trsp_chauffeurs_tc_societe", columns={"isociete"}), @ORM\Index(name="trsp_chauffeurs_tc_nom", columns={"cnom"})})
 * @ORM\Entity
 */
class trspChauffeurs {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_chauffeur", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.trsp_chauffeurs_iid_chauffeur_seq", allocationSize=1, initialValue=1)
	 */
	private $iidChauffeur = 'trsp_chauffeurs';

	public function setIidchauffeur($piIdchauffeur) {
		$this->iidChauffeur = $piIdchauffeur;
		return $this;
	}

	public function getIidchauffeur() {
		return $this->iidChauffeur;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidChauffeur;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cprenom", type="string", length=50, nullable=true)
	 */
	private $cprenom = '';

	public function setCprenom($pcPrenom) {
		$this->cprenom = $pcPrenom;
		return $this;
	}

	public function getCprenom() {
		return $this->cprenom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cportable", type="string", length=20, nullable=true)
	 */
	private $cportable = '';

	public function setCportable($pcPortable) {
		$this->cportable = $pcPortable;
		return $this;
	}

	public function getCportable() {
		return $this->cportable;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mvehiculeshabituels", type="text", nullable=true)
	 */
	private $mvehiculeshabituels = '';

	public function setMvehiculeshabituels($pmVehiculeshabituels) {
		$this->mvehiculeshabituels = $pmVehiculeshabituels;
		return $this;
	}

	public function getMvehiculeshabituels() {
		return $this->mvehiculeshabituels;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \trspSocietes
	 *
	 * @ORM\ManyToOne(targetEntity="trspSocietes")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="isociete", referencedColumnName="iid_societe")
	 * })
	 */
	private $isociete;

	public function setIsociete($piSociete) {
		$this->isociete = $piSociete;
		return $this;
	}

	public function getIsociete() {
		return $this->isociete;
	}

}
