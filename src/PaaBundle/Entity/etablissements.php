<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * etablissements
 *
 * @ORM\Table(name="paa.etablissements", uniqueConstraints={@ORM\UniqueConstraint(name="u_etablissements_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="etablissements_nofiness", columns={"cnofiness"}), @ORM\Index(name="etablissements_nom_etabli", columns={"cnom"})})
 * @ORM\Entity
 * @ ORM\Entity(repositoryClass="App\PaaBundle\Repository\etablissementsRepository")     
 */
class etablissements {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_etablissement", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 202000811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.etablissements_iid_etablissement_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
	private $iidEtablissement = 'etablissements';

	public function setIidetablissement($piIdetablissement) {
		$this->iidEtablissement = $piIdetablissement;
		return $this;
	}

	public function getIidetablissement() {
		return $this->iidEtablissement;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidEtablissement;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="inbplaces", type="decimal", precision=4, scale=0, nullable=true)
	 */
	private $inbplaces = '0';

	public function setInbplaces($piNbplaces) {
		$this->inbplaces = $piNbplaces;
		return $this;
	}

	public function getInbplaces() {
		return $this->inbplaces;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnofiness", type="string", length=10, nullable=true)
	 */
	private $cnofiness = '';

	public function setCnofiness($pcNofiness) {
		$this->cnofiness = $pcNofiness;
		return $this;
	}

	public function getCnofiness() {
		return $this->cnofiness;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrepertoire", type="text", nullable=true)
	 */
	private $mrepertoire = '';

	public function setMrepertoire($pmRepertoire) {
		$this->mrepertoire = $pmRepertoire;
		return $this;
	}

	public function getMrepertoire() {
		return $this->mrepertoire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrythme", type="text", nullable=true)
	 */
	private $mrythme = '';

	public function setMrythme($pmRythme) {
		$this->mrythme = $pmRythme;
		return $this;
	}

	public function getMrythme() {
		return $this->mrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idebutrythme", type="integer", nullable=true)
	 */
	private $idebutrythme = '0';

	public function setIdebutrythme($piDebutrythme) {
		$this->idebutrythme = $piDebutrythme;
		return $this;
	}

	public function getIdebutrythme() {
		return $this->idebutrythme;
	}

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="activites", mappedBy="ietablissement")
//     */
//    private $iacti;
//    public function setActivités($piActi){
//        $this->iacti = $piActi;
//        return $this;
//    }
//    public function getActivités() {
//        return $this->iacti;
//    }

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->iacti = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
// LG 20200904 début
	public function __toString() {
		return $this->cnom;
	}
// LG 20200904 fin

}
