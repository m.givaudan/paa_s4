<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * conventions
 *
 * @ORM\Table(name="paa.conventions", uniqueConstraints={@ORM\UniqueConstraint(name="u_conventions_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class conventions {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_convention", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.conventions_iid_convention_seq", allocationSize=1, initialValue=1)
	 */
	private $iidConvention = 'conventions';

	public function setIidconvention($piIdconvention) {
		$this->iidConvention = $piIdconvention;
		return $this;
	}

	public function getIidconvention() {
		return $this->iidConvention;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidConvention;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_sem", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhSem = '0.0';

	public function setNhsem($pnHsem) {
		$this->nhSem = $pnHsem;
		return $this;
	}

	public function getNhsem() {
		return $this->nhSem;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_max_sem", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMaxSem = '0.0';

	public function setNhmaxsem($pnHmaxsem) {
		$this->nhMaxSem = $pnHmaxsem;
		return $this;
	}

	public function getNhmaxsem() {
		return $this->nhMaxSem;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nn_max_cycl", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $nnMaxCycl = '0.0';

	public function setNnmaxcycl($pnNmaxcycl) {
		$this->nnMaxCycl = $pnNmaxcycl;
		return $this;
	}

	public function getNnmaxcycl() {
		return $this->nnMaxCycl;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_moy_m_cy", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMoyMCy = '0.0';

	public function setNhmoymcy($pnHmoymcy) {
		$this->nhMoyMCy = $pnHmoymcy;
		return $this;
	}

	public function getNhmoymcy() {
		return $this->nhMoyMCy;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_max_jour", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMaxJour = '0.0';

	public function setNhmaxjour($pnHmaxjour) {
		$this->nhMaxJour = $pnHmaxjour;
		return $this;
	}

	public function getNhmaxjour() {
		return $this->nhMaxJour;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_mmax_jour", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMmaxJour = '0.0';

	public function setNhmmaxjour($pnHmmaxjour) {
		$this->nhMmaxJour = $pnHmmaxjour;
		return $this;
	}

	public function getNhmmaxjour() {
		return $this->nhMmaxJour;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_min_seq", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMinSeq = '0.0';

	public function setNhminseq($pnHminseq) {
		$this->nhMinSeq = $pnHminseq;
		return $this;
	}

	public function getNhminseq() {
		return $this->nhMinSeq;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nn_max_seq", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $nnMaxSeq = '0.0';

	public function setNnmaxseq($pnNmaxseq) {
		$this->nnMaxSeq = $pnNmaxseq;
		return $this;
	}

	public function getNnmaxseq() {
		return $this->nnMaxSeq;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_max_inter_tp", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMaxInterTp = '0.0';

	public function setNhmaxintertp($pnHmaxintertp) {
		$this->nhMaxInterTp = $pnHmaxintertp;
		return $this;
	}

	public function getNhmaxintertp() {
		return $this->nhMaxInterTp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_min_rep_tp", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMinRepTp = '0.0';

	public function setNhminreptp($pnHminreptp) {
		$this->nhMinRepTp = $pnHminreptp;
		return $this;
	}

	public function getNhminreptp() {
		return $this->nhMinRepTp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_mmin_rep_tp", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMminRepTp = '0.0';

	public function setNhmminreptp($pnHmminreptp) {
		$this->nhMminRepTp = $pnHmminreptp;
		return $this;
	}

	public function getNhmminreptp() {
		return $this->nhMminRepTp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nn_min_jrs_rep", type="decimal", precision=3, scale=1, nullable=true)
	 */
	private $nnMinJrsRep = '0.0';

	public function setNnminjrsrep($pnNminjrsrep) {
		$this->nnMinJrsRep = $pnNminjrsrep;
		return $this;
	}

	public function getNnminjrsrep() {
		return $this->nnMinJrsRep;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nn_min_jrs_repc", type="decimal", precision=3, scale=1, nullable=true)
	 */
	private $nnMinJrsRepc = '0.0';

	public function setNnminjrsrepc($pnNminjrsrepc) {
		$this->nnMinJrsRepc = $pnNminjrsrepc;
		return $this;
	}

	public function getNnminjrsrepc() {
		return $this->nnMinJrsRepc;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nn_min_dim_moi", type="decimal", precision=3, scale=1, nullable=true)
	 */
	private $nnMinDimMoi = '0.0';

	public function setNnmindimmoi($pnNmindimmoi) {
		$this->nnMinDimMoi = $pnNmindimmoi;
		return $this;
	}

	public function getNnmindimmoi() {
		return $this->nnMinDimMoi;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_max_2sem_rempl", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMax2semRempl = '0.0';

	public function setNhmax2semrempl($pnHmax2semrempl) {
		$this->nhMax2semRempl = $pnHmax2semrempl;
		return $this;
	}

	public function getNhmax2semrempl() {
		return $this->nhMax2semRempl;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nhdebnuit", type="decimal", precision=6, scale=3, nullable=true)
	 */
	private $nhdebnuit = '0.0';

	public function setNhdebnuit($pnHdebnuit) {
		$this->nhdebnuit = $pnHdebnuit;
		return $this;
	}

	public function getNhdebnuit() {
		return $this->nhdebnuit;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nhfinnuit", type="decimal", precision=6, scale=3, nullable=true)
	 */
	private $nhfinnuit = '0.0';

	public function setNhfinnuit($pnHfinnuit) {
		$this->nhfinnuit = $pnHfinnuit;
		return $this;
	}

	public function getNhfinnuit() {
		return $this->nhfinnuit;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lh_min_rep_tp_type", type="boolean", nullable=true)
	 */
	private $lhMinRepTpType = false;

	public function setLhminreptptype($plHminreptptype) {
		$this->lhMinRepTpType = $plHminreptptype;
		return $this;
	}

	public function getLhminreptptype() {
		return $this->lhMinRepTpType;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ndurnuit", type="decimal", precision=6, scale=3, nullable=true)
	 */
	private $ndurnuit = '0.0';

	public function setNdurnuit($pnDurnuit) {
		$this->ndurnuit = $pnDurnuit;
		return $this;
	}

	public function getNdurnuit() {
		return $this->ndurnuit;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrh_min", type="text", nullable=true)
	 */
	private $mrhMin = '24+24+12;24+12-24-12+11;24+24-12+11;24+12-24';

	public function setMrhmin($pmRhmin) {
		$this->mrhMin = $pmRhmin;
		return $this;
	}

	public function getMrhmin() {
		return $this->mrhMin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idureepause", type="integer", nullable=true)
	 */
	private $idureepause = '20';

	public function setIdureepause($piDureepause) {
		$this->idureepause = $piDureepause;
		return $this;
	}

	public function getIdureepause() {
		return $this->idureepause;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nh_max_seq", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $nhMaxSeq = '6';

	public function setNhmaxseq($pnHmaxseq) {
		$this->nhMaxSeq = $pnHmaxseq;
		return $this;
	}

	public function getNhmaxseq() {
		return $this->nhMaxSeq;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lcompteenjours", type="boolean", nullable=true)
	 */
	private $lcompteenjours = false;

	public function setLcompteenjours($plCompteenjours) {
		$this->lcompteenjours = $plCompteenjours;
		return $this;
	}

	public function getLcompteenjours() {
		return $this->lcompteenjours;
	}

}
