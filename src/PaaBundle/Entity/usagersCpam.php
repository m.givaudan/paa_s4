<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersCpam
 *
 * @ORM\Table(name="paa.usagers_cpam", indexes={@ORM\Index(name="usagers_cpam_usager", columns={"iid_usager"}), @ORM\Index(name="usagers_cpam_uc_contact", columns={"icontact"}), @ORM\Index(name="usagers_cpam_uc_caisse", columns={"icaisse"}), @ORM\Index(name="usagers_cpam_archiv", columns={"darchivage"})})
 * @ORM\Entity
 */
class usagersCpam {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_usager_caisse", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.usagers_cpam_iid_usager_caisse_seq", allocationSize=1, initialValue=1)
	 */
	private $iidUsagerCaisse;

	public function setIidusagercaisse($piIdusagercaisse) {
		$this->iidUsagerCaisse = $piIdusagercaisse;
		return $this;
	}

	public function getIidusagercaisse() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icaisse", type="integer", nullable=true)
	 */
	private $icaisse = '0';

	public function setIcaisse($piCaisse) {
		$this->icaisse = $piCaisse;
		return $this;
	}

	public function getIcaisse() {
		return $this->icaisse;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icontact", type="integer", nullable=true)
	 */
	private $icontact = '0';

	public function setIcontact($piContact) {
		$this->icontact = $piContact;
		return $this;
	}

	public function getIcontact() {
		return $this->icontact;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnodossier", type="string", length=50, nullable=true)
	 */
	private $cnodossier = '';

	public function setCnodossier($pcNodossier) {
		$this->cnodossier = $pcNodossier;
		return $this;
	}

	public function getCnodossier() {
		return $this->cnodossier;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddebut", type="date", nullable=true)
	 */
	private $ddebut;

	public function setDdebut($pdDebut) {
		$this->ddebut = $pdDebut;
		return $this;
	}

	public function getDdebut() {
		return $this->ddebut;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccategorie", type="string", length=50, nullable=true)
	 */
	private $ccategorie = '';

	public function setCcategorie($pcCategorie) {
		$this->ccategorie = $pcCategorie;
		return $this;
	}

	public function getCcategorie() {
		return $this->ccategorie;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfincentpourcent", type="date", nullable=true)
	 */
	private $dfincentpourcent;

	public function setDfincentpourcent($pdFincentpourcent) {
		$this->dfincentpourcent = $pdFincentpourcent;
		return $this;
	}

	public function getDfincentpourcent() {
		return $this->dfincentpourcent;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfincmu", type="date", nullable=true)
	 */
	private $dfincmu;

	public function setDfincmu($pdFincmu) {
		$this->dfincmu = $pdFincmu;
		return $this;
	}

	public function getDfincmu() {
		return $this->dfincmu;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfintransport", type="date", nullable=true)
	 */
	private $dfintransport;

	public function setDfintransport($pdFintransport) {
		$this->dfintransport = $pdFintransport;
		return $this;
	}

	public function getDfintransport() {
		return $this->dfintransport;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomassure", type="string", length=50, nullable=true)
	 */
	private $cnomassure = '';

	public function setCnomassure($pcNomassure) {
		$this->cnomassure = $pcNomassure;
		return $this;
	}

	public function getCnomassure() {
		return $this->cnomassure;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cciviliteassure", type="string", length=20, nullable=true)
	 */
	private $cciviliteassure = '';

	public function setCciviliteassure($pcCiviliteassure) {
		$this->cciviliteassure = $pcCiviliteassure;
		return $this;
	}

	public function getCciviliteassure() {
		return $this->cciviliteassure;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnoaffiliation", type="string", length=50, nullable=true)
	 */
	private $cnoaffiliation = '';

	public function setCnoaffiliation($pcNoaffiliation) {
		$this->cnoaffiliation = $pcNoaffiliation;
		return $this;
	}

	public function getCnoaffiliation() {
		return $this->cnoaffiliation;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfincentpourcent_npa", type="boolean", nullable=true)
	 */
	private $lfincentpourcentNpa = false;

	public function setLfincentpourcentnpa($plFincentpourcentnpa) {
		$this->lfincentpourcentNpa = $plFincentpourcentnpa;
		return $this;
	}

	public function getLfincentpourcentnpa() {
		return $this->lfincentpourcentNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfincmu_npa", type="boolean", nullable=true)
	 */
	private $lfincmuNpa = false;

	public function setLfincmunpa($plFincmunpa) {
		$this->lfincmuNpa = $plFincmunpa;
		return $this;
	}

	public function getLfincmunpa() {
		return $this->lfincmuNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfintransport_npa", type="boolean", nullable=true)
	 */
	private $lfintransportNpa = false;

	public function setLfintransportnpa($plFintransportnpa) {
		$this->lfintransportNpa = $plFintransportnpa;
		return $this;
	}

	public function getLfintransportnpa() {
		return $this->lfintransportNpa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="darchivage", type="date", nullable=true)
	 */
	private $darchivage;

	public function setDarchivage($pdArchivage) {
		$this->darchivage = $pdArchivage;
		return $this;
	}

	public function getDarchivage() {
		return $this->darchivage;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactloyer", type="boolean", nullable=true)
	 */
	private $lfactloyer = false;

	public function setLfactloyer($plFactloyer) {
		$this->lfactloyer = $plFactloyer;
		return $this;
	}

	public function getLfactloyer() {
		return $this->lfactloyer;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldotationglobale", type="boolean", nullable=true)
	 */
	private $ldotationglobale = false;

	public function setLdotationglobale($plDotationglobale) {
		$this->ldotationglobale = $plDotationglobale;
		return $this;
	}

	public function getLdotationglobale() {
		return $this->ldotationglobale;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iregime", type="integer", nullable=true)
	 */
	private $iregime = '0';

	public function setIregime($piRegime) {
		$this->iregime = $piRegime;
		return $this;
	}

	public function getIregime() {
		return $this->iregime;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfinacs", type="date", nullable=true)
	 */
	private $dfinacs;

	public function setDfinacs($pdFinacs) {
		$this->dfinacs = $pdFinacs;
		return $this;
	}

	public function getDfinacs() {
		return $this->dfinacs;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinacs_npa", type="boolean", nullable=true)
	 */
	private $lfinacsNpa = false;

	public function setLfinacsnpa($plFinacsnpa) {
		$this->lfinacsNpa = $plFinacsnpa;
		return $this;
	}

	public function getLfinacsnpa() {
		return $this->lfinacsNpa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfincmuc", type="date", nullable=true)
	 */
	private $dfincmuc;

	public function setDfincmuc($pdFincmuc) {
		$this->dfincmuc = $pdFincmuc;
		return $this;
	}

	public function getDfincmuc() {
		return $this->dfincmuc;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfincmuc_npa", type="boolean", nullable=true)
	 */
	private $lfincmucNpa = false;

	public function setLfincmucnpa($plFincmucnpa) {
		$this->lfincmucNpa = $plFincmucnpa;
		return $this;
	}

	public function getLfincmucnpa() {
		return $this->lfincmucNpa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfincmuc_actif", type="boolean", nullable=true)
	 */
	private $lfincmucActif = false;

	public function setLfincmucactif($plFincmucactif) {
		$this->lfincmucActif = $plFincmucactif;
		return $this;
	}

	public function getLfincmucactif() {
		return $this->lfincmucActif;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfincmu_actif", type="boolean", nullable=true)
	 */
	private $lfincmuActif = false;

	public function setLfincmuactif($plFincmuactif) {
		$this->lfincmuActif = $plFincmuactif;
		return $this;
	}

	public function getLfincmuactif() {
		return $this->lfincmuActif;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfinacs_actif", type="boolean", nullable=true)
	 */
	private $lfinacsActif = false;

	public function setLfinacsactif($plFinacsactif) {
		$this->lfinacsActif = $plFinacsactif;
		return $this;
	}

	public function getLfinacsactif() {
		return $this->lfinacsActif;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactprixjournee", type="boolean", nullable=true)
	 */
	private $lfactprixjournee = false;

	public function setLfactprixjournee($plFactprixjournee) {
		$this->lfactprixjournee = $plFactprixjournee;
		return $this;
	}

	public function getLfactprixjournee() {
		return $this->lfactprixjournee;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactreversion", type="boolean", nullable=true)
	 */
	private $lfactreversion = false;

	public function setLfactreversion($plFactreversion) {
		$this->lfactreversion = $plFactreversion;
		return $this;
	}

	public function getLfactreversion() {
		return $this->lfactreversion;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iid_usager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iidUsager;

	public function setIidusager($piIdusager) {
		$this->iidUsager = $piIdusager;
		return $this;
	}

	public function getIidusager() {
		return $this->iidUsager;
	}

}
