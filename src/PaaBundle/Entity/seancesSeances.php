<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesSeances
 *
 * @ORM\Table(name="paa.seances_seances", uniqueConstraints={@ORM\UniqueConstraint(name="u_seances_seances_iseance1&iseance2", columns={"iseance1", "iseance2"})}, indexes={@ORM\Index(name="seances_seances_seance1", columns={"iseance1"}), @ORM\Index(name="seances_seances_seance2", columns={"iseance2"})})
 * @ORM\Entity
 */
class seancesSeances {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_seance_seance", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.seances_seances_iid_seance_seance_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSeanceSeance = 'SEANCES_SEANCES';

	public function setIidseanceseance($piIdseanceseance) {
		$this->iidSeanceSeance = $piIdseanceseance;
		return $this;
	}

	public function getIidseanceseance() {
		return $this->iidSeanceSeance;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSeanceSeance;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ityperelation", type="integer", nullable=true)
	 */
	private $ityperelation = '0';

	public function setItyperelation($piTyperelation) {
		$this->ityperelation = $piTyperelation;
		return $this;
	}

	public function getItyperelation() {
		return $this->ityperelation;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance1", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseance1;

	public function setIseance1($piSeance1) {
		$this->iseance1 = $piSeance1;
		return $this;
	}

	public function getIseance1() {
		return $this->iseance1;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance2", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseance2;

	public function setIseance2($piSeance2) {
		$this->iseance2 = $piSeance2;
		return $this;
	}

	public function getIseance2() {
		return $this->iseance2;
	}

}
