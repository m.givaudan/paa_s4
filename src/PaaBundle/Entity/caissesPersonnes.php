<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * caissesPersonnes
 *
 * @ORM\Table(name="paa.caisses_personnes", indexes={@ORM\Index(name="caisses_personnes_cp_caisse", columns={"icaisse"})})
 * @ORM\Entity
 */
class caissesPersonnes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_caissespersonnes", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.caisses_personnes_iid_caissespersonnes_seq", allocationSize=1, initialValue=1)
	 */
	private $iidCaissespersonnes;

	public function setIidcaissespersonnes($piIdcaissespersonnes) {
		$this->iidCaissespersonnes = $piIdcaissespersonnes;
		return $this;
	}

	public function getIidcaissespersonnes() {
		return $this->iidCaissespersonnes;
	}

	public function getId() {
		return $this->iidCaissespersonnes;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccivilite", type="string", length=10, nullable=true)
	 */
	private $ccivilite = '';

	public function setCcivilite($pcCivilite) {
		$this->ccivilite = $pcCivilite;
		return $this;
	}

	public function getCcivilite() {
		return $this->ccivilite;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cprenom", type="string", length=50, nullable=true)
	 */
	private $cprenom = '';

	public function setCprenom($pcPrenom) {
		$this->cprenom = $pcPrenom;
		return $this;
	}

	public function getCprenom() {
		return $this->cprenom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctel", type="string", length=50, nullable=true)
	 */
	private $ctel = '';

	public function setCtel($pcTel) {
		$this->ctel = $pcTel;
		return $this;
	}

	public function getCtel() {
		return $this->ctel;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cport", type="string", length=50, nullable=true)
	 */
	private $cport = '';

	public function setCport($pcPort) {
		$this->cport = $pcPort;
		return $this;
	}

	public function getCport() {
		return $this->cport;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cfax", type="string", length=50, nullable=true)
	 */
	private $cfax = '';

	public function setCfax($pcFax) {
		$this->cfax = $pcFax;
		return $this;
	}

	public function getCfax() {
		return $this->cfax;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cemail", type="string", length=50, nullable=true)
	 */
	private $cemail = '';

	public function setCemail($pcEmail) {
		$this->cemail = $pcEmail;
		return $this;
	}

	public function getCemail() {
		return $this->cemail;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cfonction", type="string", length=25, nullable=true)
	 */
	private $cfonction = '';

	public function setCfonction($pcFonction) {
		$this->cfonction = $pcFonction;
		return $this;
	}

	public function getCfonction() {
		return $this->cfonction;
	}

//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="icaisse", referencedColumnName="iid_caisses")
	/**
	 * @var \caisses
	 *
	 * @ORM\ManyToOne(targetEntity="caisses", inversedBy="listeContacts")
	 * @ORM\JoinColumn(name="icaisse", referencedColumnName="iid_caisses")
	 * })
	 */
	private $icaisse;
	public function setIcaisse($piCaisse) {
		$this->icaisse = $piCaisse;
		return $this;
	}

	public function getIcaisse() {
		return $this->icaisse;
	}
//// LG 20200929 test début
//	/**
//	 * @var \icaisse_Integer
//	 *
//	 * @ORM\Column(name="icaisse", type="integer", length=25, nullable=true)
//	 * })
//	 */
//	private $icaisse_Integer;
//	public function setIcaisseInteger($piCaisse) {
//		$this->icaisse_Integer = $piCaisse;
//		return $this;
//	}
//
//	public function getIcaisseInteger() {
//		return $this->icaisse_Integer;
//	}
//// LG 20200929 test fin

}
