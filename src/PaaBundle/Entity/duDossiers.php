<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * duDossiers
 *
 * @ORM\Table(name="paa.du_dossiers", indexes={@ORM\Index(name="du_dossiers_fk_parent", columns={"iparent"})})
 * @ORM\Entity
 */
class duDossiers {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_dossier", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.du_dossiers_iid_dossier_seq", allocationSize=1, initialValue=1)
	 */
	private $iidDossier;

	public function setIiddossier($piIddossier) {
		$this->iidDossier = $piIddossier;
		return $this;
	}

	public function getIiddossier() {
		return $this->iidDossier;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidDossier;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iparent", type="integer", nullable=true)
	 */
	private $iparent = '0';

	public function setIparent($piParent) {
		$this->iparent = $piParent;
		return $this;
	}

	public function getIparent() {
		return $this->iparent;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clibelle", type="string", length=50, nullable=true)
	 */
	private $clibelle = '';

	public function setClibelle($pcLibelle) {
		$this->clibelle = $pcLibelle;
		return $this;
	}

	public function getClibelle() {
		return $this->clibelle;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iforecolor", type="integer", nullable=true)
	 */
	private $iforecolor = '0';

	public function setIforecolor($piForecolor) {
		$this->iforecolor = $piForecolor;
		return $this;
	}

	public function getIforecolor() {
		return $this->iforecolor;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ibackcolor", type="integer", nullable=true)
	 */
	private $ibackcolor = '0';

	public function setIbackcolor($piBackcolor) {
		$this->ibackcolor = $piBackcolor;
		return $this;
	}

	public function getIbackcolor() {
		return $this->ibackcolor;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfontbold", type="boolean", nullable=true)
	 */
	private $lfontbold = false;

	public function setLfontbold($plFontbold) {
		$this->lfontbold = $plFontbold;
		return $this;
	}

	public function getLfontbold() {
		return $this->lfontbold;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfontitalic", type="boolean", nullable=true)
	 */
	private $lfontitalic = false;

	public function setLfontitalic($plFontitalic) {
		$this->lfontitalic = $plFontitalic;
		return $this;
	}

	public function getLfontitalic() {
		return $this->lfontitalic;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mimage", type="text", nullable=true)
	 */
	private $mimage = '';

	public function setMimage($pmImage) {
		$this->mimage = $pmImage;
		return $this;
	}

	public function getMimage() {
		return $this->mimage;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mimageselected", type="text", nullable=true)
	 */
	private $mimageselected = '';

	public function setMimageselected($pmImageselected) {
		$this->mimageselected = $pmImageselected;
		return $this;
	}

	public function getMimageselected() {
		return $this->mimageselected;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mchemindft", type="text", nullable=true)
	 */
	private $mchemindft = '';

	public function setMchemindft($pmChemindft) {
		$this->mchemindft = $pmChemindft;
		return $this;
	}

	public function getMchemindft() {
		return $this->mchemindft;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idureeavantarchivage", type="integer", nullable=true)
	 */
	private $idureeavantarchivage = '0';

	public function setIdureeavantarchivage($piDureeavantarchivage) {
		$this->idureeavantarchivage = $piDureeavantarchivage;
		return $this;
	}

	public function getIdureeavantarchivage() {
		return $this->idureeavantarchivage;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldroitsduparent", type="boolean", nullable=true)
	 */
	private $ldroitsduparent = true;

	public function setLdroitsduparent($plDroitsduparent) {
		$this->ldroitsduparent = $plDroitsduparent;
		return $this;
	}

	public function getLdroitsduparent() {
		return $this->ldroitsduparent;
	}

}
