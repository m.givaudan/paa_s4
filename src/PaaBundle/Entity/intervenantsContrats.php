<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * intervenantsContrats
 *
 * @ORM\Table(name="paa.intervenants_contrats", indexes={@ORM\Index(name="intervenants_contrats_intervenant", columns={"iintervenant"}), @ORM\Index(name="intervenants_contrats_ic_debut", columns={"ddebut"}), @ORM\Index(name="intervenants_contrats_tp_par", columns={"ltempspartiel"})})
 * @ORM\Entity
 */
class intervenantsContrats {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_intervenantscontrats", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.intervenants_contrats_iid_intervenantscontrats_seq", allocationSize=1, initialValue=1)
	 */
	private $iidIntervenantscontrats = 'intervenants_contrats';

	public function setIidintervenantscontrats($piIdintervenantscontrats) {
		$this->iidIntervenantscontrats = $piIdintervenantscontrats;
		return $this;
	}

	public function getIidintervenantscontrats() {
		return $this->iidIntervenantscontrats;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidIntervenantscontrats;
	}

	// AV 04/03/2019 fin
	// AV 07/03/2019 début
	public function __construct() {
		$this->doldDebut = null;
		$this->oldFin = null;
		$this->dfin = null;
		$dateNow = new \DateTime('now');
		$this->ddebut = $dateNow->format('Y-m-d');
	}

	// AV 07/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccontrat", type="string", length=254, nullable=true)
	 */
	private $ccontrat = '';

	public function setCcontrat($pcContrat) {
		$this->ccontrat = $pcContrat;
		return $this;
	}

	public function getCcontrat() {
		return $this->ccontrat;
	}

	// AV 07/03/2019 début
	// /**
	// * @var \DateTime
	// * @ORM\Column(name="ddebut", type="date", nullable=true)
	// */

	/**
	 * @var \Date
	 *
	 * @ORM\Column(name="ddebut", type="date", nullable=true)
	 */
	// AV 07/03/2019 fin
	private $ddebut;

	public function setDdebut($pdDebut) {
		$this->ddebut = $pdDebut;
		return $this;
	}

	public function getDdebut() {
		return $this->ddebut;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bnb_h_sem", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $bnbHSem = '35';

	public function setBnbhsem($pbNbhsem) {
		$this->bnbHSem = $pbNbhsem;
		return $this;
	}

	public function getBnbhsem() {
		return $this->bnbHSem;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bnb_h_mois", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $bnbHMois = '140';

	public function setBnbhmois($pbNbhmois) {
		$this->bnbHMois = $pbNbhmois;
		return $this;
	}

	public function getBnbhmois() {
		return $this->bnbHMois;
	}

	// AV 07/03/2019 début
	// /**
	// * @var \DateTime
	// * @ORM\Column(name="dfin", type="date", nullable=true)
	// */
	/**
	 * @var \Date
	 *
	 * @ORM\Column(name="dfin", type="date", nullable=true)
	 */
	// AV 07/03/2019 fin
	private $dfin;

	public function setDfin($pdFin) {
		$this->dfin = $pdFin;
		return $this;
	}

	public function getDfin() {
		return $this->dfin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="bnb_h_annee", type="decimal", precision=7, scale=2, nullable=true)
	 */
	private $bnbHAnnee = '0.0';

	public function setBnbhannee($pbNbhannee) {
		$this->bnbHAnnee = $pbNbhannee;
		return $this;
	}

	public function getBnbhannee() {
		return $this->bnbHAnnee;
	}

	// AV 07/03/2019 début
	// /**
	// * @var \DateTime
	// * @ORM\Column(name="dold_debut", type="date", nullable=true)
	// */
	// private $doldDebut = '0001-01-01';

	/**
	 * @var \Date
	 *
	 * @ORM\Column(name="dold_debut", type="date", nullable=true)
	 */
	private $doldDebut = null;

	// AV 07/03/2019 fin
	public function setDolddebut($pdOlddebut) {
		$this->doldDebut = $pdOlddebut;
		return $this;
	}

	public function getDolddebut() {
		return $this->doldDebut;
	}

	// AV 07/03/2019 début
	// /**
	// * @var \DateTime
	// * @ORM\Column(name="old_fin", type="date", nullable=true)
	// */
	// private $oldFin = '0001-01-01';
	/**
	 * @var \Date
	 *
	 * @ORM\Column(name="old_fin", type="date", nullable=true)
	 */
	private $oldFin = null;

	// AV 07/03/2019 fin
	public function setOldfin($poLdfin) {
		$this->oldFin = $poLdfin;
		return $this;
	}

	public function getOldfin() {
		return $this->oldFin;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltempspartiel", type="boolean", nullable=true)
	 */
	private $ltempspartiel = false;

	public function setLtempspartiel($plTempspartiel) {
		$this->ltempspartiel = $plTempspartiel;
		return $this;
	}

	public function getLtempspartiel() {
		return $this->ltempspartiel;
	}

//    AV 07/03/2019 début
//      @var integer
//     
//      @ORM\Column(name="ietablissement", type="integer", nullable=true)
//     

	/**
	 * @var \etablissements
	 *
	 * @ORM\ManyToOne(targetEntity="etablissements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ietablissement", referencedColumnName="iid_etablissement")
	 * })
	 */
	private $ietablissement;

	public function setIetablissement($piEtablissement) {
		$this->ietablissement = $piEtablissement;
		return $this;
	}

	public function getIetablissement() {
		return $this->ietablissement;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iconvention", type="integer", nullable=true)
	 */
	// AV 07/03/2019 début
	// private $iconvention = 'giId_Convention';
	private $iconvention = '1';

	// AV 07/03/2019 fin
	public function setIconvention($piConvention) {
		$this->iconvention = $piConvention;
		return $this;
	}

	public function getIconvention() {
		return $this->iconvention;
	}

	// AV 07/03/2019 début
	// /**
	//* @ORM\ManyToOne(targetEntity="intervenants")
	// * @ORM\JoinColumns({
	// *   @ORM\JoinColumn(name="iintervenant", referencedColumnName="iid_intervenant")
	// * })
	// */
	/**
	 * @var \intervenants
	 *
	 * @ORM\ManyToOne(targetEntity="intervenants", inversedBy="listeContrats")
	 * @ORM\JoinColumn(name="iintervenant", referencedColumnName="iid_intervenant")
	 * 
	 */
	// AV 07/03/2019 fin
	private $iintervenant;

	public function setIintervenant($piIntervenant) {
		$this->iintervenant = $piIntervenant;
		return $this;
	}

	public function getIintervenant() {
		return $this->iintervenant;
	}

}
