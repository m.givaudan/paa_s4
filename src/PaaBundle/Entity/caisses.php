<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * caisses
 *
 * @ORM\Table(name="paa.caisses", uniqueConstraints={@ORM\UniqueConstraint(name="u_caisses_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class caisses {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_caisses", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.caisses_iid_caisses_seq", allocationSize=1, initialValue=1)
	 */
	private $iidCaisses /* = 'CAISSES' */;

	public function setIidcaisses($piIdcaisses) {
		$this->iidCaisses = $piIdcaisses;
		return $this;
	}

	public function getIidcaisses() {
		return $this->iidCaisses;
	}

	public function getiId_Caisses() {
		return $this->iidCaisses;
	}

	public function getId() {
		return $this->iidCaisses;
	}

// LG 20190215 début	
	public function __construct() {
		$this->listeContacts = new ArrayCollection();
	}

	/**
	 * @ORM\OneToMany(targetEntity="caissesPersonnes", mappedBy="icaisse")
	 */
	private $listeContacts;

	/**
	 * Get listeContacts
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getlisteContacts() {
		return $this->listeContacts;
	}

// LG 20190215 fin	

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=200, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=50, nullable=true)
	 */
	private $cnomcourt = '';

	public function setCnomcourt($pcNomcourt) {
		$this->cnomcourt = $pcNomcourt;
		return $this;
	}

	public function getCnomcourt() {
		if (($this->cnomcourt ?? "") == "")
			return $this->cnomcourt;
		else
			return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse1", type="string", length=254, nullable=true)
	 */
	private $cadresse1 = '';

	public function setCadresse1($pcAdresse1) {
		$this->cadresse1 = $pcAdresse1;
		return $this;
	}

	public function getCadresse1() {
		return $this->cadresse1;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse2", type="string", length=254, nullable=true)
	 */
	private $cadresse2 = '';

	public function setCadresse2($pcAdresse2) {
		$this->cadresse2 = $pcAdresse2;
		return $this;
	}

	public function getCadresse2() {
		return $this->cadresse2;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccp", type="string", length=254, nullable=true)
	 */
	private $ccp = '';

	public function setCcp($pcCp) {
		$this->ccp = $pcCp;
		return $this;
	}

	public function getCcp() {
		return $this->ccp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cville", type="string", length=254, nullable=true)
	 */
	private $cville = '';

	public function setCville($pcVille) {
		$this->cville = $pcVille;
		return $this;
	}

	public function getCville() {
		return $this->cville;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctel", type="string", length=100, nullable=true)
	 */
	private $ctel = '';

	public function setCtel($pcTel) {
		$this->ctel = $pcTel;
		return $this;
	}

	public function getCtel() {
		return $this->ctel;
	}
//MC 20200903 DEBUT
//      @var integer
//      @ORM\Column(name="itype", type="integer", nullable=true)
	
        /**
	 * @var \caissestype
	 * 
	 * @ORM\ManyToOne(targetEntity="caissesType")
	 * @ORM\JoinColumn(name="itype", referencedColumnName="iid_typecaisses")
	 * 
	 */
	private $itype = '0';
//MC 20200903 FIN
	public function setItype($piType) {
		$this->itype = $piType;
		return $this;
	}

	public function getItype() {
		return $this->itype;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cemail", type="string", length=100, nullable=true)
	 */
	private $cemail = '';

	public function setCemail($pcEmail) {
		$this->cemail = $pcEmail;
		return $this;
	}

	public function getCemail() {
		return $this->cemail;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cfax", type="string", length=100, nullable=true)
	 */
	private $cfax = '';

	public function setCfax($pcFax) {
		$this->cfax = $pcFax;
		return $this;
	}

	public function getCfax() {
		return $this->cfax;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cweb", type="string", length=100, nullable=true)
	 */
	private $cweb = '';

	public function setCweb($pcWeb) {
		$this->cweb = $pcWeb;
		return $this;
	}

	public function getCweb() {
		return $this->cweb;
	}

}
