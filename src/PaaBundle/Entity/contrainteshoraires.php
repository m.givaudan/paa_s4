<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * contrainteshoraires
 *
 * @ORM\Table(name="paa.contrainteshoraires", uniqueConstraints={@ORM\UniqueConstraint(name="u_contrainteshoraires_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class contrainteshoraires {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.contrainteshoraires_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=254, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

}
