<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * services
 *
 * @ORM\Table(name="paa.services", uniqueConstraints={@ORM\UniqueConstraint(name="u_services_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="services_s_nom", columns={"cnom"})})
 * @ORM\Entity
 */
class services {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_service", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.services_iid_service_seq", allocationSize=1, initialValue=1)
	 */
	private $iidService = 'services';

	public function setIidservice($piIdservice) {
		$this->iidService = $piIdservice;
		return $this;
	}

	public function getIidservice() {
		return $this->iidService;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidService;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=100, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrepertoire", type="text", nullable=true)
	 */
	private $mrepertoire = '';

	public function setMrepertoire($pmRepertoire) {
		$this->mrepertoire = $pmRepertoire;
		return $this;
	}

	public function getMrepertoire() {
		return $this->mrepertoire;
	}

// LG 20200811 début
	/**
	 * @ var integer
	 *
	 * @ ORM\Column(name="ietablissement", type="integer", nullable=true)
	 */
	/**
	 * @var \etablissements
	 *
	 * @ORM\ManyToOne(targetEntity="etablissements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ietablissement", referencedColumnName="iid_etablissement")
	 * })
	 */
// LG 20200811 fin
	private $ietablissement;

	public function setIetablissement($piEtablissement) {
		$this->ietablissement = $piEtablissement;
		return $this;
	}

	public function getIetablissement() {
		return $this->ietablissement;
	}

}
