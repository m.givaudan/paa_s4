<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usagersAidesociale
 *
 * @ORM\Table(name="paa.usagers_aidesociale", indexes={@ORM\Index(name="usagers_aidesociale_uc_contact", columns={"icontact"}), @ORM\Index(name="usagers_aidesociale_uc_caisse", columns={"icaisse"}), @ORM\Index(name="usagers_aidesociale_usager", columns={"iid_usager"}), @ORM\Index(name="usagers_aidesociale_archiv", columns={"darchivage"})})
 * @ORM\Entity
 */
class usagersAidesociale {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_usager_caisse", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.usagers_aidesociale_iid_usager_caisse_seq", allocationSize=1, initialValue=1)
	 */
	private $iidUsagerCaisse;

	public function setIidusagercaisse($piIdusagercaisse) {
		$this->iidUsagerCaisse = $piIdusagercaisse;
		return $this;
	}

	public function getIidusagercaisse() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidUsagerCaisse;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icaisse", type="integer", nullable=true)
	 */
	private $icaisse = '0';

	public function setIcaisse($piCaisse) {
		$this->icaisse = $piCaisse;
		return $this;
	}

	public function getIcaisse() {
		return $this->icaisse;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icontact", type="integer", nullable=true)
	 */
	private $icontact = '0';

	public function setIcontact($piContact) {
		$this->icontact = $piContact;
		return $this;
	}

	public function getIcontact() {
		return $this->icontact;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnodossier", type="string", length=50, nullable=true)
	 */
	private $cnodossier = '';

	public function setCnodossier($pcNodossier) {
		$this->cnodossier = $pcNodossier;
		return $this;
	}

	public function getCnodossier() {
		return $this->cnodossier;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddebut", type="date", nullable=true)
	 */
	private $ddebut;

	public function setDdebut($pdDebut) {
		$this->ddebut = $pdDebut;
		return $this;
	}

	public function getDdebut() {
		return $this->ddebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfin", type="date", nullable=true)
	 */
	private $dfin;

	public function setDfin($pdFin) {
		$this->dfin = $pdFin;
		return $this;
	}

	public function getDfin() {
		return $this->dfin;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfin_npa", type="boolean", nullable=true)
	 */
	private $lfinNpa = false;

	public function setLfinnpa($plFinnpa) {
		$this->lfinNpa = $plFinnpa;
		return $this;
	}

	public function getLfinnpa() {
		return $this->lfinNpa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="darchivage", type="date", nullable=true)
	 */
	private $darchivage;

	public function setDarchivage($pdArchivage) {
		$this->darchivage = $pdArchivage;
		return $this;
	}

	public function getDarchivage() {
		return $this->darchivage;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactloyer", type="boolean", nullable=true)
	 */
	private $lfactloyer = false;

	public function setLfactloyer($plFactloyer) {
		$this->lfactloyer = $plFactloyer;
		return $this;
	}

	public function getLfactloyer() {
		return $this->lfactloyer;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldotationglobale", type="boolean", nullable=true)
	 */
	private $ldotationglobale = false;

	public function setLdotationglobale($plDotationglobale) {
		$this->ldotationglobale = $plDotationglobale;
		return $this;
	}

	public function getLdotationglobale() {
		return $this->ldotationglobale;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mdomicilesecours", type="text", nullable=true)
	 */
	private $mdomicilesecours = '';

	public function setMdomicilesecours($pmDomicilesecours) {
		$this->mdomicilesecours = $pmDomicilesecours;
		return $this;
	}

	public function getMdomicilesecours() {
		return $this->mdomicilesecours;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactprixjournee", type="boolean", nullable=true)
	 */
	private $lfactprixjournee = false;

	public function setLfactprixjournee($plFactprixjournee) {
		$this->lfactprixjournee = $plFactprixjournee;
		return $this;
	}

	public function getLfactprixjournee() {
		return $this->lfactprixjournee;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lfactreversion", type="boolean", nullable=true)
	 */
	private $lfactreversion = false;

	public function setLfactreversion($plFactreversion) {
		$this->lfactreversion = $plFactreversion;
		return $this;
	}

	public function getLfactreversion() {
		return $this->lfactreversion;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iid_usager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iidUsager;

	public function setIidusager($piIdusager) {
		$this->iidUsager = $piIdusager;
		return $this;
	}

	public function getIidusager() {
		return $this->iidUsager;
	}

}
