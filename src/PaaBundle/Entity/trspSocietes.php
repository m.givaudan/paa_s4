<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * trspSocietes
 *
 * @ORM\Table(name="paa.trsp_societes", uniqueConstraints={@ORM\UniqueConstraint(name="u_trsp_societes_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="trsp_societes_ts_nom", columns={"cnom"})})
 * @ORM\Entity
 */
class trspSocietes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_societe", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.trsp_societes_iid_societe_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSociete = 'trsp_societes';

	public function setIidsociete($piIdsociete) {
		$this->iidSociete = $piIdsociete;
		return $this;
	}

	public function getIidsociete() {
		return $this->iidSociete;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSociete;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cadresse", type="string", length=50, nullable=true)
	 */
	private $cadresse = '';

	public function setCadresse($pcAdresse) {
		$this->cadresse = $pcAdresse;
		return $this;
	}

	public function getCadresse() {
		return $this->cadresse;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccp", type="string", length=20, nullable=true)
	 */
	private $ccp = '';

	public function setCcp($pcCp) {
		$this->ccp = $pcCp;
		return $this;
	}

	public function getCcp() {
		return $this->ccp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cville", type="string", length=50, nullable=true)
	 */
	private $cville = '';

	public function setCville($pcVille) {
		$this->cville = $pcVille;
		return $this;
	}

	public function getCville() {
		return $this->cville;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctel", type="string", length=20, nullable=true)
	 */
	private $ctel = '';

	public function setCtel($pcTel) {
		$this->ctel = $pcTel;
		return $this;
	}

	public function getCtel() {
		return $this->ctel;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cfax", type="string", length=20, nullable=true)
	 */
	private $cfax = '';

	public function setCfax($pcFax) {
		$this->cfax = $pcFax;
		return $this;
	}

	public function getCfax() {
		return $this->cfax;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cportable", type="string", length=20, nullable=true)
	 */
	private $cportable = '';

	public function setCportable($pcPortable) {
		$this->cportable = $pcPortable;
		return $this;
	}

	public function getCportable() {
		return $this->cportable;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmel", type="string", length=50, nullable=true)
	 */
	private $cmel = '';

	public function setCmel($pcMel) {
		$this->cmel = $pcMel;
		return $this;
	}

	public function getCmel() {
		return $this->cmel;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomresp", type="string", length=50, nullable=true)
	 */
	private $cnomresp = '';

	public function setCnomresp($pcNomresp) {
		$this->cnomresp = $pcNomresp;
		return $this;
	}

	public function getCnomresp() {
		return $this->cnomresp;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ncoutkm", type="decimal", precision=5, scale=3, nullable=true)
	 */
	private $ncoutkm = '0.0';

	public function setNcoutkm($pnCoutkm) {
		$this->ncoutkm = $pnCoutkm;
		return $this;
	}

	public function getNcoutkm() {
		return $this->ncoutkm;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=50, nullable=true)
	 */
	private $cnomcourt = '';

	public function setCnomcourt($pcNomcourt) {
		$this->cnomcourt = $pcNomcourt;
		return $this;
	}

	public function getCnomcourt() {
		return $this->cnomcourt;
	}

}
