<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * actibases
 *
 * @ORM\Table(name="paa.actibases", uniqueConstraints={@ORM\UniqueConstraint(name="u_actibases_cnomcourt", columns={"cnomcourt", "isousgroupe"}), @ORM\UniqueConstraint(name="u_actibases_cnom", columns={"cnom", "isousgroupe"})}, indexes={@ORM\Index(name="actibases_ab_travail", columns={"ltravaillee"}), @ORM\Index(name="actibases_abs_type", columns={"iabstype"}), @ORM\Index(name="actibases_exclpla", columns={"lexclplanning"}), @ORM\Index(name="actibases_as_sys", columns={"lsysteme"}), @ORM\Index(name="actibases_sousgroupe", columns={"isousgroupe"}), @ORM\Index(name="IDX_34025C9DCB91600", columns={"ietablissement"})})
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\actiBasesRepository")
 */
class actibases {

	public function __toString() {
		return $this->cnomcourt;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_actibase", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ ORM\SequenceGenerator(sequenceName="paa.actibases_iid_actibase_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
	private $iidActibase = 'ActiBases';

	public function setIidactibase($piIdactibase) {
		$this->iidActibase = $piIdactibase;
		return $this;
	}

	public function getIidactibase() {
		return $this->iidActibase;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidActibase;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=100, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="inbusmax", type="decimal", precision=3, scale=0, nullable=true)
	 */
	private $inbusmax = '0.0';

	public function setInbusmax($piNbusmax) {
		$this->inbusmax = $piNbusmax;
		return $this;
	}

	public function getInbusmax() {
		return $this->inbusmax;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lsysteme", type="boolean", nullable=true)
	 */
	private $lsysteme = false;

	public function setLsysteme($plSysteme) {
		$this->lsysteme = $plSysteme;
		return $this;
	}

	public function getLsysteme() {
		return $this->lsysteme;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconcetab", type="boolean", nullable=true)
	 */
	private $lconcetab = false;

	public function setLconcetab($plConcetab) {
		$this->lconcetab = $plConcetab;
		return $this;
	}

	public function getLconcetab() {
		return $this->lconcetab;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconcinterv", type="boolean", nullable=true)
	 */
	private $lconcinterv = false;

	public function setLconcinterv($plConcinterv) {
		$this->lconcinterv = $plConcinterv;
		return $this;
	}

	public function getLconcinterv() {
		return $this->lconcinterv;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconcusag", type="boolean", nullable=true)
	 */
	private $lconcusag = false;

	public function setLconcusag($plConcusag) {
		$this->lconcusag = $plConcusag;
		return $this;
	}

	public function getLconcusag() {
		return $this->lconcusag;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconcgroup", type="boolean", nullable=true)
	 */
	private $lconcgroup = false;

	public function setLconcgroup($plConcgroup) {
		$this->lconcgroup = $plConcgroup;
		return $this;
	}

	public function getLconcgroup() {
		return $this->lconcgroup;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconcsalle", type="boolean", nullable=true)
	 */
	private $lconcsalle = false;

	public function setLconcsalle($plConcsalle) {
		$this->lconcsalle = $plConcsalle;
		return $this;
	}

	public function getLconcsalle() {
		return $this->lconcsalle;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '0';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ldansbilan", type="boolean", nullable=true)
	 */
	private $ldansbilan = false;

	public function setLdansbilan($plDansbilan) {
		$this->ldansbilan = $plDansbilan;
		return $this;
	}

	public function getLdansbilan() {
		return $this->ldansbilan;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="npriorite", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $npriorite = '2';

	public function setNpriorite($pnPriorite) {
		$this->npriorite = $pnPriorite;
		return $this;
	}

	public function getNpriorite() {
		return $this->npriorite;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=100, nullable=true)
	 */
	private $cnomcourt = '';

	public function setCnomcourt($pcNomcourt) {
		$this->cnomcourt = $pcNomcourt;
		return $this;
	}

	public function getCnomcourt() {
		return $this->cnomcourt;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconcmatiere", type="boolean", nullable=true)
	 */
	private $lconcmatiere = false;

	public function setLconcmatiere($plConcmatiere) {
		$this->lconcmatiere = $plConcmatiere;
		return $this;
	}

	public function getLconcmatiere() {
		return $this->lconcmatiere;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lintervindisp", type="boolean", nullable=true)
	 */
	private $lintervindisp = false;

	public function setLintervindisp($plIntervindisp) {
		$this->lintervindisp = $plIntervindisp;
		return $this;
	}

	public function getLintervindisp() {
		return $this->lintervindisp;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lsalleindisp", type="boolean", nullable=true)
	 */
	private $lsalleindisp = false;

	public function setLsalleindisp($plSalleindisp) {
		$this->lsalleindisp = $plSalleindisp;
		return $this;
	}

	public function getLsalleindisp() {
		return $this->lsalleindisp;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lusagindisp", type="boolean", nullable=true)
	 */
	private $lusagindisp = false;

	public function setLusagindisp($plUsagindisp) {
		$this->lusagindisp = $plUsagindisp;
		return $this;
	}

	public function getLusagindisp() {
		return $this->lusagindisp;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lgroupindisp", type="boolean", nullable=true)
	 */
	private $lgroupindisp = false;

	public function setLgroupindisp($plGroupindisp) {
		$this->lgroupindisp = $plGroupindisp;
		return $this;
	}

	public function getLgroupindisp() {
		return $this->lgroupindisp;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lexclplanning", type="boolean", nullable=true)
	 */
	private $lexclplanning = false;

	public function setLexclplanning($plExclplanning) {
		$this->lexclplanning = $plExclplanning;
		return $this;
	}

	public function getLexclplanning() {
		return $this->lexclplanning;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nnbusmin", type="decimal", precision=3, scale=0, nullable=true)
	 */
	private $nnbusmin = '0.0';

	public function setNnbusmin($pnNbusmin) {
		$this->nnbusmin = $pnNbusmin;
		return $this;
	}

	public function getNnbusmin() {
		return $this->nnbusmin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="iabstype", type="decimal", precision=1, scale=0, nullable=true)
	 */
	private $iabstype = '1';

	public function setIabstype($piAbstype) {
		$this->iabstype = $piAbstype;
		return $this;
	}

	public function getIabstype() {
		return $this->iabstype;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="iabsfix", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $iabsfix = '0.0';

	public function setIabsfix($piAbsfix) {
		$this->iabsfix = $piAbsfix;
		return $this;
	}

	public function getIabsfix() {
		return $this->iabsfix;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccode", type="string", length=10, nullable=true)
	 */
	private $ccode = '';

	public function setCcode($pcCode) {
		$this->ccode = $pcCode;
		return $this;
	}

	public function getCcode() {
		return $this->ccode;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltravaillee", type="boolean", nullable=true)
	 */
	private $ltravaillee = false;

	public function setLtravaillee($plTravaillee) {
		$this->ltravaillee = $plTravaillee;
		return $this;
	}

	public function getLtravaillee() {
		return $this->ltravaillee;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ljustifiee", type="boolean", nullable=true)
	 */
	private $ljustifiee = false;

	public function setLjustifiee($plJustifiee) {
		$this->ljustifiee = $plJustifiee;
		return $this;
	}

	public function getLjustifiee() {
		return $this->ljustifiee;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltoususagers", type="boolean", nullable=true)
	 */
	private $ltoususagers = false;

	public function setLtoususagers($plToususagers) {
		$this->ltoususagers = $plToususagers;
		return $this;
	}

	public function getLtoususagers() {
		return $this->ltoususagers;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltousintervenants", type="boolean", nullable=true)
	 */
	private $ltousintervenants = false;

	public function setLtousintervenants($plTousintervenants) {
		$this->ltousintervenants = $plTousintervenants;
		return $this;
	}

	public function getLtousintervenants() {
		return $this->ltousintervenants;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltousgroupes", type="boolean", nullable=true)
	 */
	private $ltousgroupes = false;

	public function setLtousgroupes($plTousgroupes) {
		$this->ltousgroupes = $plTousgroupes;
		return $this;
	}

	public function getLtousgroupes() {
		return $this->ltousgroupes;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="ltoutessalles", type="boolean", nullable=true)
	 */
	private $ltoutessalles = false;

	public function setLtoutessalles($plToutessalles) {
		$this->ltoutessalles = $plToutessalles;
		return $this;
	}

	public function getLtoutessalles() {
		return $this->ltoutessalles;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="linactive", type="boolean", nullable=true)
	 */
	private $linactive = false;

	public function setLinactive($plInactive) {
		$this->linactive = $plInactive;
		return $this;
	}

	public function getLinactive() {
		return $this->linactive;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="iabsfixajout", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $iabsfixajout = '0';

	public function setIabsfixajout($piAbsfixajout) {
		$this->iabsfixajout = $piAbsfixajout;
		return $this;
	}

	public function getIabsfixajout() {
		return $this->iabsfixajout;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lautoriseseancessimultanees", type="boolean", nullable=true)
	 */
	private $lautoriseseancessimultanees = false;

	public function setLautoriseseancessimultanees($plAutoriseseancessimultanees) {
		$this->lautoriseseancessimultanees = $plAutoriseseancessimultanees;
		return $this;
	}

	public function getLautoriseseancessimultanees() {
		return $this->lautoriseseancessimultanees;
	}

	/**
	 * @var \etablissements
	 *
	 * @ORM\ManyToOne(targetEntity="etablissements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ietablissement", referencedColumnName="iid_etablissement")
	 * })
	 */
	private $ietablissement;

	public function setIetablissement($piEtablissement) {
		$this->ietablissement = $piEtablissement;
		return $this;
	}

	public function getIetablissement() {
		return $this->ietablissement;
	}

	/**
	 * @var \actiSsgroupesacti
	 *
	 * @ORM\ManyToOne(targetEntity="actiSsgroupesacti")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="isousgroupe", referencedColumnName="iid_actisousgroupe")
	 * })
	 */
	private $isousgroupe;

	public function setIsousgroupe($piSousgroupe) {
		$this->isousgroupe = $piSousgroupe;
		return $this;
	}

	public function getIsousgroupe() {
		return $this->isousgroupe;
	}

}
