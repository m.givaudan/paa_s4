<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * duDocumentsRessources
 *
 * @ORM\Table(name="paa.du_documents_ressources", indexes={@ORM\Index(name="du_documents_ressources_fk_arbo", columns={"idossier"}), @ORM\Index(name="du_documents_ressources_fk_doc", columns={"idocument"})})
 * @ORM\Entity
 */
class duDocumentsRessources {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_document_ressource", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.du_documents_ressources_iid_document_ressource_seq", allocationSize=1, initialValue=1)
	 */
	private $iidDocumentRessource;

	public function setIiddocumentressource($piIddocumentressource) {
		$this->iidDocumentRessource = $piIddocumentressource;
		return $this;
	}

	public function getIiddocumentressource() {
		return $this->iidDocumentRessource;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidDocumentRessource;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cres", type="string", length=50, nullable=true)
	 */
	private $cres = '';

	public function setCres($pcRes) {
		$this->cres = $pcRes;
		return $this;
	}

	public function getCres() {
		return $this->cres;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idocument", type="integer", nullable=true)
	 */
	private $idocument = '0';

	public function setIdocument($piDocument) {
		$this->idocument = $piDocument;
		return $this;
	}

	public function getIdocument() {
		return $this->idocument;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idossier", type="integer", nullable=true)
	 */
	private $idossier = '0';

	public function setIdossier($piDossier) {
		$this->idossier = $piDossier;
		return $this;
	}

	public function getIdossier() {
		return $this->idossier;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="limpliqueres", type="boolean", nullable=true)
	 */
	private $limpliqueres = false;

	public function setLimpliqueres($plImpliqueres) {
		$this->limpliqueres = $plImpliqueres;
		return $this;
	}

	public function getLimpliqueres() {
		return $this->limpliqueres;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroitsplus", type="integer", nullable=true)
	 */
	private $idroitsplus = '0';

	public function setIdroitsplus($piDroitsplus) {
		$this->idroitsplus = $piDroitsplus;
		return $this;
	}

	public function getIdroitsplus() {
		return $this->idroitsplus;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroitsmoins", type="integer", nullable=true)
	 */
	private $idroitsmoins = '0';

	public function setIdroitsmoins($piDroitsmoins) {
		$this->idroitsmoins = $piDroitsmoins;
		return $this;
	}

	public function getIdroitsmoins() {
		return $this->idroitsmoins;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clibelle", type="string", length=50, nullable=true)
	 */
	private $clibelle = '';

	public function setClibelle($pcLibelle) {
		$this->clibelle = $pcLibelle;
		return $this;
	}

	public function getClibelle() {
		return $this->clibelle;
	}

}
