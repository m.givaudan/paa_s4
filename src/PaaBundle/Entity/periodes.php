<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * periodes
 *
 * @ORM\Table(name="paa.periodes", uniqueConstraints={@ORM\UniqueConstraint(name="u_periodes_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="periodes_p_debut", columns={"tdebut"})})
 * @ORM\Entity
 */
class periodes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_periode", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.periodes_iid_periode_seq", allocationSize=1, initialValue=1)
	 */
	private $iidPeriode = 'periodes';

	public function setIidperiode($piIdperiode) {
		$this->iidPeriode = $piIdperiode;
		return $this;
	}

	public function getIidperiode() {
		return $this->iidPeriode;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidPeriode;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebut", type="date", nullable=true)
	 */
// LG 20200811 old	private $tdebut = '0001-01-01';
	private $tdebut = null;

	public function setTdebut($ptDebut) {
		$this->tdebut = $ptDebut;
		return $this;
	}

	public function getTdebut() {
		return $this->tdebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tfin", type="date", nullable=true)
	 */
// LG 20200811 old	private $tfin = '0001-01-01';
	private $tfin = null;

	public function setTfin($ptFin) {
		$this->tfin = $ptFin;
		return $this;
	}

	public function getTfin() {
		return $this->tfin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

}
