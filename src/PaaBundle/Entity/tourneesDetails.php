<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tourneesDetails
 *
 * @ORM\Table(name="paa.tournees_details", indexes={@ORM\Index(name="tournees_details_td_usager", columns={"iusager"}), @ORM\Index(name="tournees_details_td_horaire", columns={"nhoraire"}), @ORM\Index(name="tournees_details_td_var", columns={"nvariante"}), @ORM\Index(name="tournees_details_td_tournee", columns={"itournee"})})
 * @ORM\Entity
 */
class tourneesDetails {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_tournee_detail", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tournees_details_iid_tournee_detail_seq", allocationSize=1, initialValue=1)
	 */
	private $iidTourneeDetail = 'tournees_details';

	public function setIidtourneedetail($piIdtourneedetail) {
		$this->iidTourneeDetail = $piIdtourneedetail;
		return $this;
	}

	public function getIidtourneedetail() {
		return $this->iidTourneeDetail;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidTourneeDetail;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nvariante", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $nvariante = '0.0';

	public function setNvariante($pnVariante) {
		$this->nvariante = $pnVariante;
		return $this;
	}

	public function getNvariante() {
		return $this->nvariante;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nrang", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $nrang = '0.0';

	public function setNrang($pnRang) {
		$this->nrang = $pnRang;
		return $this;
	}

	public function getNrang() {
		return $this->nrang;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clieudepose", type="string", length=50, nullable=true)
	 */
	private $clieudepose = '';

	public function setClieudepose($pcLieudepose) {
		$this->clieudepose = $pcLieudepose;
		return $this;
	}

	public function getClieudepose() {
		return $this->clieudepose;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nhoraire", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $nhoraire = '0.0';

	public function setNhoraire($pnHoraire) {
		$this->nhoraire = $pnHoraire;
		return $this;
	}

	public function getNhoraire() {
		return $this->nhoraire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ndistance", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $ndistance = '0.0';

	public function setNdistance($pnDistance) {
		$this->ndistance = $pnDistance;
		return $this;
	}

	public function getNdistance() {
		return $this->ndistance;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ncoutkm", type="decimal", precision=5, scale=3, nullable=true)
	 */
	private $ncoutkm = '0.0';

	public function setNcoutkm($pnCoutkm) {
		$this->ncoutkm = $pnCoutkm;
		return $this;
	}

	public function getNcoutkm() {
		return $this->ncoutkm;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ireferentdepose", type="integer", nullable=true)
	 */
	private $ireferentdepose = '0';

	public function setIreferentdepose($piReferentdepose) {
		$this->ireferentdepose = $piReferentdepose;
		return $this;
	}

	public function getIreferentdepose() {
		return $this->ireferentdepose;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cligne", type="string", length=50, nullable=true)
	 */
	private $cligne = '';

	public function setCligne($pcLigne) {
		$this->cligne = $pcLigne;
		return $this;
	}

	public function getCligne() {
		return $this->cligne;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nhoraireplanb", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $nhoraireplanb = '0.0';

	public function setNhoraireplanb($pnHoraireplanb) {
		$this->nhoraireplanb = $pnHoraireplanb;
		return $this;
	}

	public function getNhoraireplanb() {
		return $this->nhoraireplanb;
	}

	/**
	 * @var \tournees
	 *
	 * @ORM\ManyToOne(targetEntity="tournees")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="itournee", referencedColumnName="iid_tournee")
	 * })
	 */
	private $itournee;

	public function setItournee($piTournee) {
		$this->itournee = $piTournee;
		return $this;
	}

	public function getItournee() {
		return $this->itournee;
	}

	/**
	 * @var \usagers
	 *
	 * @ORM\ManyToOne(targetEntity="usagers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iusager", referencedColumnName="iid_usager")
	 * })
	 */
	private $iusager;

	public function setIusager($piUsager) {
		$this->iusager = $piUsager;
		return $this;
	}

	public function getIusager() {
		return $this->iusager;
	}

}
