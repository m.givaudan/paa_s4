<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * permis
 *
 * @ORM\Table(name="paa.permis", uniqueConstraints={@ORM\UniqueConstraint(name="u_permis_cnom", columns={"cnom"}), @ORM\UniqueConstraint(name="u_permis_cnomcourt", columns={"cnomcourt"})})
 * @ORM\Entity
 */
class permis {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_permis", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.permis_iid_permis_seq", allocationSize=1, initialValue=1)
	 */
	private $iidPermis = 'permis';

	public function setIidpermis($piIdpermis) {
		$this->iidPermis = $piIdpermis;
		return $this;
	}

	public function getIidpermis() {
		return $this->iidPermis;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidPermis;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=254, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomcourt", type="string", length=50, nullable=true)
	 */
	private $cnomcourt = '';

	public function setCnomcourt($pcNomcourt) {
		$this->cnomcourt = $pcNomcourt;
		return $this;
	}

	public function getCnomcourt() {
		return $this->cnomcourt;
	}
	
	public function __toString() {
// LG 20200904 début
////MG Modification 20200817 Début
////		return $this->cnomcourt;
//		return $this->cnom;
////MG Modification 20200817 Fin
		return $this->cnomcourt . ' : ' . $this->cnom ;
// LG 20200904 fin
	}

}
