<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * etiquettes
 *
 * @ORM\Table(name="paa.etiquettes", uniqueConstraints={@ORM\UniqueConstraint(name="u_etiquettes_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class etiquettes {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_etiquette", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.etiquettes_iid_etiquette_seq", allocationSize=1, initialValue=1)
	 */
	private $iidEtiquette;

	public function setIidetiquette($piIdetiquette) {
		$this->iidEtiquette = $piIdetiquette;
		return $this;
	}

	public function getIidetiquette() {
		return $this->iidEtiquette;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidEtiquette;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="itype", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $itype = '1';

	public function setItype($piType) {
		$this->itype = $piType;
		return $this;
	}

	public function getItype() {
		return $this->itype;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mchemin", type="text", nullable=true)
	 */
	private $mchemin = '';

	public function setMchemin($pmChemin) {
		$this->mchemin = $pmChemin;
		return $this;
	}

	public function getMchemin() {
		return $this->mchemin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctagname", type="text", nullable=true)
	 */
	private $ctagname = '';

	public function setCtagname($pcTagname) {
		$this->ctagname = $pcTagname;
		return $this;
	}

	public function getCtagname() {
		return $this->ctagname;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctagexpr", type="text", nullable=true)
	 */
	private $ctagexpr = '';

	public function setCtagexpr($pcTagexpr) {
		$this->ctagexpr = $pcTagexpr;
		return $this;
	}

	public function getCtagexpr() {
		return $this->ctagexpr;
	}

}
