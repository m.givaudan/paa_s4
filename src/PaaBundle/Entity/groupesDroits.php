<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * groupesDroits
 *
 * @ORM\Table(name="paa.groupes_droits", uniqueConstraints={@ORM\UniqueConstraint(name="u_groupes_droits_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class groupesDroits {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_groupe_droit", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.groupes_droits_iid_groupe_droit_seq", allocationSize=1, initialValue=1)
	 */
	private $iidGroupeDroit = 'groupes_droits';

	public function setIidgroupedroit($piIdgroupedroit) {
		$this->iidGroupeDroit = $piIdgroupedroit;
		return $this;
	}

	public function getIidgroupedroit() {
		return $this->iidGroupeDroit;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidGroupeDroit;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=100, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroitsr", type="integer", nullable=true)
	 */
	private $idroitsr = '0';

	public function setIdroitsr($piDroitsr) {
		$this->idroitsr = $piDroitsr;
		return $this;
	}

	public function getIdroitsr() {
		return $this->idroitsr;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroitsw", type="integer", nullable=true)
	 */
	private $idroitsw = '0';

	public function setIdroitsw($piDroitsw) {
		$this->idroitsw = $piDroitsw;
		return $this;
	}

	public function getIdroitsw() {
		return $this->idroitsw;
	}

}
