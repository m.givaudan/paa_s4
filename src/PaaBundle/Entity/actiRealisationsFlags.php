<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * actiRealisationsFlags
 *
 * @ORM\Table(name="paa.tseances_flags", uniqueConstraints={@ORM\UniqueConstraint(name="u_acti_realisations_flags_iseance", columns={"iseance"})}, indexes={@ORM\Index(name="acti_realisations_flags_real_plant", columns={"iseanceplantype"}), @ORM\Index(name="acti_realisations_flags_conf_plant", columns={"lconfplantype"})})
 * @ORM\Entity
 */
class actiRealisationsFlags {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iseance", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.tseances_flags_iseance_seq", allocationSize=1, initialValue=1)
	 */
	private $iseance = '0';

	public function setIseance($piSeance) {
		$this->iseance = $piSeance;
		return $this;
	}

	public function getIseance() {
		return $this->iseance;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iseance;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iseanceplantype", type="integer", nullable=true)
	 */
	private $iseanceplantype = '0';

	public function setIseanceplantype($piSeanceplantype) {
		$this->iseanceplantype = $piSeanceplantype;
		return $this;
	}

	public function getIseanceplantype() {
		return $this->iseanceplantype;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconfplantype", type="boolean", nullable=true)
	 */
	private $lconfplantype = false;

	public function setLconfplantype($plConfplantype) {
		$this->lconfplantype = $plConfplantype;
		return $this;
	}

	public function getLconfplantype() {
		return $this->lconfplantype;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lpasconfplantgere", type="boolean", nullable=true)
	 */
	private $lpasconfplantgere = false;

	public function setLpasconfplantgere($plPasconfplantgere) {
		$this->lpasconfplantgere = $plPasconfplantgere;
		return $this;
	}

	public function getLpasconfplantgere() {
		return $this->lpasconfplantgere;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lmodifsemaine", type="boolean", nullable=true)
	 */
	private $lmodifsemaine = false;

	public function setLmodifsemaine($plModifsemaine) {
		$this->lmodifsemaine = $plModifsemaine;
		return $this;
	}

	public function getLmodifsemaine() {
		return $this->lmodifsemaine;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lmodifplantype", type="boolean", nullable=true)
	 */
	private $lmodifplantype = false;

	public function setLmodifplantype($plModifplantype) {
		$this->lmodifplantype = $plModifplantype;
		return $this;
	}

	public function getLmodifplantype() {
		return $this->lmodifplantype;
	}

}
