<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * duDroits
 *
 * @ORM\Table(name="paa.du_droits", indexes={@ORM\Index(name="du_droits_id_res", columns={"iid_res"}), @ORM\Index(name="du_droits_fk_arbo", columns={"idossier"})})
 * @ORM\Entity
 */
class duDroits {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_droit", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.du_droits_iid_droit_seq", allocationSize=1, initialValue=1)
	 */
	private $iidDroit;

	public function setIiddroit($piIddroit) {
		$this->iidDroit = $piIddroit;
		return $this;
	}

	public function getIiddroit() {
		return $this->iidDroit;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidDroit;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=2, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idossier", type="integer", nullable=true)
	 */
	private $idossier = '0';

	public function setIdossier($piDossier) {
		$this->idossier = $piDossier;
		return $this;
	}

	public function getIdossier() {
		return $this->idossier;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroits", type="integer", nullable=true)
	 */
	private $idroits = '0';

	public function setIdroits($piDroits) {
		$this->idroits = $piDroits;
		return $this;
	}

	public function getIdroits() {
		return $this->idroits;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idroits_usr", type="integer", nullable=true)
	 */
	private $idroitsUsr = '0';

	public function setIdroitsusr($piDroitsusr) {
		$this->idroitsUsr = $piDroitsusr;
		return $this;
	}

	public function getIdroitsusr() {
		return $this->idroitsUsr;
	}

}
