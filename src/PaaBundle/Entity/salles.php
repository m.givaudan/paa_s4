<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * salles
 *
 * @ORM\Table(name="paa.salles", uniqueConstraints={@ORM\UniqueConstraint(name="u_salles_cnom", columns={"cnom"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\sallesRepository") 
 */
class salles {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->activites = new \Doctrine\Common\Collections\ArrayCollection();
		$this->iacti = new \Doctrine\Common\Collections\ArrayCollection();
	}

	public function __toString() {
		return $this->cnom;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_salle", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
// LG 20200811 old	 * @ORM\SequenceGenerator(sequenceName="paa.salles_iid_salle_seq", allocationSize=1, initialValue=1)
	 * @ORM\SequenceGenerator(sequenceName="paa.ressources_seq", allocationSize=1, initialValue=1)
	 */
// LG 20200904 old	private $iidSalle = 'salles';
	private $iidSalle = 0 ;

	public function setIidsalle($piIdsalle) {
		$this->iidSalle = $piIdsalle;
		return $this;
	}

	public function getIidsalle() {
		return $this->iidSalle;
	}

	public function getId() {
		return $this->getIidsalle();
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="icapacite", type="decimal", precision=3, scale=0, nullable=true)
	 */
	private $icapacite = '0.0';

	public function setIcapacite($piCapacite) {
		$this->icapacite = $piCapacite;
		return $this;
	}

	public function getIcapacite() {
		return $this->icapacite;
	}

// LG 20200904 début
//	/**
//	 * @ var string
//	 *
//	 * @ ORM\Column(name="ipermis", type="decimal", precision=10, scale=0, nullable=true)
//	 */
	/**
	 * @var \permis
	 *
	 * @ORM\ManyToOne(targetEntity="permis")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ipermis", referencedColumnName="iid_permis")
	 * })
	 */
// LG 20200904 fin
	private $ipermis = '0';

	public function setIpermis($piPermis) {
		$this->ipermis = $piPermis;
		return $this;
	}

	public function getIpermis() {
		return $this->ipermis;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrepertoire", type="text", nullable=true)
	 */
	private $mrepertoire = '';

	public function setMrepertoire($pmRepertoire) {
		$this->mrepertoire = $pmRepertoire;
		return $this;
	}

	public function getMrepertoire() {
		return $this->mrepertoire;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mrythme", type="text", nullable=true)
	 */
	private $mrythme = '';

	public function setMrythme($pmRythme) {
		$this->mrythme = $pmRythme;
		return $this;
	}

	public function getMrythme() {
		return $this->mrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="idebutrythme", type="integer", nullable=true)
	 */
	private $idebutrythme = '0';

	public function setIdebutrythme($piDebutrythme) {
		$this->idebutrythme = $piDebutrythme;
		return $this;
	}

	public function getIdebutrythme() {
		return $this->idebutrythme;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

// LG 20200811 début
//	/**
//	 * @ var integer
//	 *
//	 * @ ORM\Column(name="ietablissement", type="integer", nullable=true)
//	 */
	/**
	 * @var \etablissements
	 *
	 * @ORM\ManyToOne(targetEntity="etablissements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="ietablissement", referencedColumnName="iid_etablissement")
	 * })
	 */
// LG 20200811 fin

	private $ietablissement /* = '-1' */;

	public function setIetablissement($piEtablissement) {
		$this->ietablissement = $piEtablissement;
		return $this;
	}

	public function getIetablissement() {
		return $this->ietablissement;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnoinventaire", type="string", length=50, nullable=true)
	 */
	private $cnoinventaire = '';

	public function setCnoinventaire($pcNoinventaire) {
		$this->cnoinventaire = $pcNoinventaire;
		return $this;
	}

	public function getCnoinventaire() {
		return $this->cnoinventaire;
	}

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="activites", mappedBy="isalle")
//     */
//    private $iacti;
//    public function setActivités($piActi){
//        $this->iacti = $piActi;
//        return $this;
//    }
//    public function getActivités() {
//        return $this->iacti;
//    }

	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\activites",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_salle",nullable=false)},
	 *   inverseJoinColumns = {@ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")}
	 * )
	 */
	private $activites;

	/**
	 * @return Collection|acivite[]
	 */
	public function getActivites(): \Doctrine\Common\Collections\ArrayCollection {
		return $this->activites;
	}

	public function setActivites($paActivite) {
		$this->activites[] = $paActivite;
//            $activite->setsalles($this);
		return $this;
	}

}
