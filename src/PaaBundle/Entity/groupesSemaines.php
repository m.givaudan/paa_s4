<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * groupesSemaines
 *
 * @ORM\Table(name="paa.groupes_semaines", uniqueConstraints={@ORM\UniqueConstraint(name="u_groupes_semaines_isemaine&igroupe", columns={"igroupe", "isemaine"})}, indexes={@ORM\Index(name="groupes_semaines_gs_usager", columns={"igroupe"}), @ORM\Index(name="groupes_semaines_gs_semaine", columns={"isemaine"})})
 * @ORM\Entity
 */
class groupesSemaines {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_ressource_semaine", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.groupes_semaines_iid_ressource_semaine_seq", allocationSize=1, initialValue=1)
	 */
	private $iidRessourceSemaine = 'GROUPES_SEMAINES';

	public function setIidressourcesemaine($piIdressourcesemaine) {
		$this->iidRessourceSemaine = $piIdressourcesemaine;
		return $this;
	}

	public function getIidressourcesemaine() {
		return $this->iidRessourceSemaine;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidRessourceSemaine;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="isemaine", type="integer", nullable=true)
	 */
	private $isemaine = '0';

	public function setIsemaine($piSemaine) {
		$this->isemaine = $piSemaine;
		return $this;
	}

	public function getIsemaine() {
		return $this->isemaine;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="abs_std", type="decimal", precision=5, scale=2, nullable=true)
	 */
	private $absStd;

	public function setAbsstd($paBsstd) {
		$this->absStd = $paBsstd;
		return $this;
	}

	public function getAbsstd() {
		return $this->absStd;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpres_lu", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresLu;

	public function setTotpreslu($ptOtpreslu) {
		$this->totpresLu = $ptOtpreslu;
		return $this;
	}

	public function getTotpreslu() {
		return $this->totpresLu;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpres_ma", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresMa;

	public function setTotpresma($ptOtpresma) {
		$this->totpresMa = $ptOtpresma;
		return $this;
	}

	public function getTotpresma() {
		return $this->totpresMa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpres_me", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresMe;

	public function setTotpresme($ptOtpresme) {
		$this->totpresMe = $ptOtpresme;
		return $this;
	}

	public function getTotpresme() {
		return $this->totpresMe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpres_je", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresJe;

	public function setTotpresje($ptOtpresje) {
		$this->totpresJe = $ptOtpresje;
		return $this;
	}

	public function getTotpresje() {
		return $this->totpresJe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpres_ve", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresVe;

	public function setTotpresve($ptOtpresve) {
		$this->totpresVe = $ptOtpresve;
		return $this;
	}

	public function getTotpresve() {
		return $this->totpresVe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpres_sa", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresSa;

	public function setTotpressa($ptOtpressa) {
		$this->totpresSa = $ptOtpressa;
		return $this;
	}

	public function getTotpressa() {
		return $this->totpresSa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpres_di", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresDi;

	public function setTotpresdi($ptOtpresdi) {
		$this->totpresDi = $ptOtpresdi;
		return $this;
	}

	public function getTotpresdi() {
		return $this->totpresDi;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confaj_lu", type="boolean", nullable=true)
	 */
	private $confajLu = false;

	public function setConfajlu($pcOnfajlu) {
		$this->confajLu = $pcOnfajlu;
		return $this;
	}

	public function getConfajlu() {
		return $this->confajLu;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confaj_ma", type="boolean", nullable=true)
	 */
	private $confajMa = false;

	public function setConfajma($pcOnfajma) {
		$this->confajMa = $pcOnfajma;
		return $this;
	}

	public function getConfajma() {
		return $this->confajMa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confaj_me", type="boolean", nullable=true)
	 */
	private $confajMe = false;

	public function setConfajme($pcOnfajme) {
		$this->confajMe = $pcOnfajme;
		return $this;
	}

	public function getConfajme() {
		return $this->confajMe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confaj_je", type="boolean", nullable=true)
	 */
	private $confajJe = false;

	public function setConfajje($pcOnfajje) {
		$this->confajJe = $pcOnfajje;
		return $this;
	}

	public function getConfajje() {
		return $this->confajJe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confaj_ve", type="boolean", nullable=true)
	 */
	private $confajVe = false;

	public function setConfajve($pcOnfajve) {
		$this->confajVe = $pcOnfajve;
		return $this;
	}

	public function getConfajve() {
		return $this->confajVe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confaj_sa", type="boolean", nullable=true)
	 */
	private $confajSa = false;

	public function setConfajsa($pcOnfajsa) {
		$this->confajSa = $pcOnfajsa;
		return $this;
	}

	public function getConfajsa() {
		return $this->confajSa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confaj_di", type="boolean", nullable=true)
	 */
	private $confajDi = false;

	public function setConfajdi($pcOnfajdi) {
		$this->confajDi = $pcOnfajdi;
		return $this;
	}

	public function getConfajdi() {
		return $this->confajDi;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatrav_lu", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravLu;

	public function setTotatravlu($ptOtatravlu) {
		$this->totatravLu = $ptOtatravlu;
		return $this;
	}

	public function getTotatravlu() {
		return $this->totatravLu;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatrav_ma", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravMa;

	public function setTotatravma($ptOtatravma) {
		$this->totatravMa = $ptOtatravma;
		return $this;
	}

	public function getTotatravma() {
		return $this->totatravMa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatrav_me", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravMe;

	public function setTotatravme($ptOtatravme) {
		$this->totatravMe = $ptOtatravme;
		return $this;
	}

	public function getTotatravme() {
		return $this->totatravMe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatrav_je", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravJe;

	public function setTotatravje($ptOtatravje) {
		$this->totatravJe = $ptOtatravje;
		return $this;
	}

	public function getTotatravje() {
		return $this->totatravJe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatrav_ve", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravVe;

	public function setTotatravve($ptOtatravve) {
		$this->totatravVe = $ptOtatravve;
		return $this;
	}

	public function getTotatravve() {
		return $this->totatravVe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatrav_sa", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravSa;

	public function setTotatravsa($ptOtatravsa) {
		$this->totatravSa = $ptOtatravsa;
		return $this;
	}

	public function getTotatravsa() {
		return $this->totatravSa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatrav_di", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravDi;

	public function setTotatravdi($ptOtatravdi) {
		$this->totatravDi = $ptOtatravdi;
		return $this;
	}

	public function getTotatravdi() {
		return $this->totatravDi;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajpres_lu", type="datetime", nullable=true)
	 */
	private $tmajpresLu;

	public function setTmajpreslu($ptMajpreslu) {
		$this->tmajpresLu = $ptMajpreslu;
		return $this;
	}

	public function getTmajpreslu() {
		return $this->tmajpresLu;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajpres_ma", type="datetime", nullable=true)
	 */
	private $tmajpresMa;

	public function setTmajpresma($ptMajpresma) {
		$this->tmajpresMa = $ptMajpresma;
		return $this;
	}

	public function getTmajpresma() {
		return $this->tmajpresMa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajpres_me", type="datetime", nullable=true)
	 */
	private $tmajpresMe;

	public function setTmajpresme($ptMajpresme) {
		$this->tmajpresMe = $ptMajpresme;
		return $this;
	}

	public function getTmajpresme() {
		return $this->tmajpresMe;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajpres_je", type="datetime", nullable=true)
	 */
	private $tmajpresJe;

	public function setTmajpresje($ptMajpresje) {
		$this->tmajpresJe = $ptMajpresje;
		return $this;
	}

	public function getTmajpresje() {
		return $this->tmajpresJe;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajpres_ve", type="datetime", nullable=true)
	 */
	private $tmajpresVe;

	public function setTmajpresve($ptMajpresve) {
		$this->tmajpresVe = $ptMajpresve;
		return $this;
	}

	public function getTmajpresve() {
		return $this->tmajpresVe;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajpres_sa", type="datetime", nullable=true)
	 */
	private $tmajpresSa;

	public function setTmajpressa($ptMajpressa) {
		$this->tmajpresSa = $ptMajpressa;
		return $this;
	}

	public function getTmajpressa() {
		return $this->tmajpresSa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajpres_di", type="datetime", nullable=true)
	 */
	private $tmajpresDi;

	public function setTmajpresdi($ptMajpresdi) {
		$this->tmajpresDi = $ptMajpresdi;
		return $this;
	}

	public function getTmajpresdi() {
		return $this->tmajpresDi;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajconf_lu", type="datetime", nullable=true)
	 */
	private $tmajconfLu;

	public function setTmajconflu($ptMajconflu) {
		$this->tmajconfLu = $ptMajconflu;
		return $this;
	}

	public function getTmajconflu() {
		return $this->tmajconfLu;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajconf_ma", type="datetime", nullable=true)
	 */
	private $tmajconfMa;

	public function setTmajconfma($ptMajconfma) {
		$this->tmajconfMa = $ptMajconfma;
		return $this;
	}

	public function getTmajconfma() {
		return $this->tmajconfMa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajconf_me", type="datetime", nullable=true)
	 */
	private $tmajconfMe;

	public function setTmajconfme($ptMajconfme) {
		$this->tmajconfMe = $ptMajconfme;
		return $this;
	}

	public function getTmajconfme() {
		return $this->tmajconfMe;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajconf_je", type="datetime", nullable=true)
	 */
	private $tmajconfJe;

	public function setTmajconfje($ptMajconfje) {
		$this->tmajconfJe = $ptMajconfje;
		return $this;
	}

	public function getTmajconfje() {
		return $this->tmajconfJe;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajconf_ve", type="datetime", nullable=true)
	 */
	private $tmajconfVe;

	public function setTmajconfve($ptMajconfve) {
		$this->tmajconfVe = $ptMajconfve;
		return $this;
	}

	public function getTmajconfve() {
		return $this->tmajconfVe;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajconf_sa", type="datetime", nullable=true)
	 */
	private $tmajconfSa;

	public function setTmajconfsa($ptMajconfsa) {
		$this->tmajconfSa = $ptMajconfsa;
		return $this;
	}

	public function getTmajconfsa() {
		return $this->tmajconfSa;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmajconf_di", type="datetime", nullable=true)
	 */
	private $tmajconfDi;

	public function setTmajconfdi($ptMajconfdi) {
		$this->tmajconfDi = $ptMajconfdi;
		return $this;
	}

	public function getTmajconfdi() {
		return $this->tmajconfDi;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences", type="text", nullable=true)
	 */
	private $mabsences;

	public function setMabsences($pmAbsences) {
		$this->mabsences = $pmAbsences;
		return $this;
	}

	public function getMabsences() {
		return $this->mabsences;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="abs_lu", type="boolean", nullable=true)
	 */
	private $absLu;

	public function setAbslu($paBslu) {
		$this->absLu = $paBslu;
		return $this;
	}

	public function getAbslu() {
		return $this->absLu;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="abs_ma", type="boolean", nullable=true)
	 */
	private $absMa;

	public function setAbsma($paBsma) {
		$this->absMa = $paBsma;
		return $this;
	}

	public function getAbsma() {
		return $this->absMa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="abs_me", type="boolean", nullable=true)
	 */
	private $absMe;

	public function setAbsme($paBsme) {
		$this->absMe = $paBsme;
		return $this;
	}

	public function getAbsme() {
		return $this->absMe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="abs_je", type="boolean", nullable=true)
	 */
	private $absJe;

	public function setAbsje($paBsje) {
		$this->absJe = $paBsje;
		return $this;
	}

	public function getAbsje() {
		return $this->absJe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="abs_ve", type="boolean", nullable=true)
	 */
	private $absVe;

	public function setAbsve($paBsve) {
		$this->absVe = $paBsve;
		return $this;
	}

	public function getAbsve() {
		return $this->absVe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="abs_sa", type="boolean", nullable=true)
	 */
	private $absSa;

	public function setAbssa($paBssa) {
		$this->absSa = $paBssa;
		return $this;
	}

	public function getAbssa() {
		return $this->absSa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="abs_di", type="boolean", nullable=true)
	 */
	private $absDi;

	public function setAbsdi($paBsdi) {
		$this->absDi = $paBsdi;
		return $this;
	}

	public function getAbsdi() {
		return $this->absDi;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="labsmodified", type="boolean", nullable=true)
	 */
	private $labsmodified;

	public function setLabsmodified($plAbsmodified) {
		$this->labsmodified = $plAbsmodified;
		return $this;
	}

	public function getLabsmodified() {
		return $this->labsmodified;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="absaj_ferm_lu", type="boolean", nullable=true)
	 */
	private $absajFermLu = false;

	public function setAbsajfermlu($paBsajfermlu) {
		$this->absajFermLu = $paBsajfermlu;
		return $this;
	}

	public function getAbsajfermlu() {
		return $this->absajFermLu;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="absaj_ferm_ma", type="boolean", nullable=true)
	 */
	private $absajFermMa = false;

	public function setAbsajfermma($paBsajfermma) {
		$this->absajFermMa = $paBsajfermma;
		return $this;
	}

	public function getAbsajfermma() {
		return $this->absajFermMa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="absaj_ferm_me", type="boolean", nullable=true)
	 */
	private $absajFermMe = false;

	public function setAbsajfermme($paBsajfermme) {
		$this->absajFermMe = $paBsajfermme;
		return $this;
	}

	public function getAbsajfermme() {
		return $this->absajFermMe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="absaj_ferm_je", type="boolean", nullable=true)
	 */
	private $absajFermJe = false;

	public function setAbsajfermje($paBsajfermje) {
		$this->absajFermJe = $paBsajfermje;
		return $this;
	}

	public function getAbsajfermje() {
		return $this->absajFermJe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="absaj_ferm_ve", type="boolean", nullable=true)
	 */
	private $absajFermVe = false;

	public function setAbsajfermve($paBsajfermve) {
		$this->absajFermVe = $paBsajfermve;
		return $this;
	}

	public function getAbsajfermve() {
		return $this->absajFermVe;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="absaj_ferm_sa", type="boolean", nullable=true)
	 */
	private $absajFermSa = false;

	public function setAbsajfermsa($paBsajfermsa) {
		$this->absajFermSa = $paBsajfermsa;
		return $this;
	}

	public function getAbsajfermsa() {
		return $this->absajFermSa;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="absaj_ferm_di", type="boolean", nullable=true)
	 */
	private $absajFermDi = false;

	public function setAbsajfermdi($paBsajfermdi) {
		$this->absajFermDi = $paBsajfermdi;
		return $this;
	}

	public function getAbsajfermdi() {
		return $this->absajFermDi;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnomsemaine", type="string", length=50, nullable=true)
	 */
	private $cnomsemaine;

	public function setCnomsemaine($pcNomsemaine) {
		$this->cnomsemaine = $pcNomsemaine;
		return $this;
	}

	public function getCnomsemaine() {
		return $this->cnomsemaine;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpresnuit_lu", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresnuitLu;

	public function setTotpresnuitlu($ptOtpresnuitlu) {
		$this->totpresnuitLu = $ptOtpresnuitlu;
		return $this;
	}

	public function getTotpresnuitlu() {
		return $this->totpresnuitLu;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpresnuit_ma", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresnuitMa;

	public function setTotpresnuitma($ptOtpresnuitma) {
		$this->totpresnuitMa = $ptOtpresnuitma;
		return $this;
	}

	public function getTotpresnuitma() {
		return $this->totpresnuitMa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpresnuit_me", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresnuitMe;

	public function setTotpresnuitme($ptOtpresnuitme) {
		$this->totpresnuitMe = $ptOtpresnuitme;
		return $this;
	}

	public function getTotpresnuitme() {
		return $this->totpresnuitMe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpresnuit_je", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresnuitJe;

	public function setTotpresnuitje($ptOtpresnuitje) {
		$this->totpresnuitJe = $ptOtpresnuitje;
		return $this;
	}

	public function getTotpresnuitje() {
		return $this->totpresnuitJe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpresnuit_ve", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresnuitVe;

	public function setTotpresnuitve($ptOtpresnuitve) {
		$this->totpresnuitVe = $ptOtpresnuitve;
		return $this;
	}

	public function getTotpresnuitve() {
		return $this->totpresnuitVe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpresnuit_sa", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresnuitSa;

	public function setTotpresnuitsa($ptOtpresnuitsa) {
		$this->totpresnuitSa = $ptOtpresnuitsa;
		return $this;
	}

	public function getTotpresnuitsa() {
		return $this->totpresnuitSa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totpresnuit_di", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totpresnuitDi;

	public function setTotpresnuitdi($ptOtpresnuitdi) {
		$this->totpresnuitDi = $ptOtpresnuitdi;
		return $this;
	}

	public function getTotpresnuitdi() {
		return $this->totpresnuitDi;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatravnuit_lu", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravnuitLu;

	public function setTotatravnuitlu($ptOtatravnuitlu) {
		$this->totatravnuitLu = $ptOtatravnuitlu;
		return $this;
	}

	public function getTotatravnuitlu() {
		return $this->totatravnuitLu;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatravnuit_ma", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravnuitMa;

	public function setTotatravnuitma($ptOtatravnuitma) {
		$this->totatravnuitMa = $ptOtatravnuitma;
		return $this;
	}

	public function getTotatravnuitma() {
		return $this->totatravnuitMa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatravnuit_me", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravnuitMe;

	public function setTotatravnuitme($ptOtatravnuitme) {
		$this->totatravnuitMe = $ptOtatravnuitme;
		return $this;
	}

	public function getTotatravnuitme() {
		return $this->totatravnuitMe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatravnuit_je", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravnuitJe;

	public function setTotatravnuitje($ptOtatravnuitje) {
		$this->totatravnuitJe = $ptOtatravnuitje;
		return $this;
	}

	public function getTotatravnuitje() {
		return $this->totatravnuitJe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatravnuit_ve", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravnuitVe;

	public function setTotatravnuitve($ptOtatravnuitve) {
		$this->totatravnuitVe = $ptOtatravnuitve;
		return $this;
	}

	public function getTotatravnuitve() {
		return $this->totatravnuitVe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatravnuit_sa", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravnuitSa;

	public function setTotatravnuitsa($ptOtatravnuitsa) {
		$this->totatravnuitSa = $ptOtatravnuitsa;
		return $this;
	}

	public function getTotatravnuitsa() {
		return $this->totatravnuitSa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="totatravnuit_di", type="float", precision=10, scale=0, nullable=true)
	 */
	private $totatravnuitDi;

	public function setTotatravnuitdi($ptOtatravnuitdi) {
		$this->totatravnuitDi = $ptOtatravnuitdi;
		return $this;
	}

	public function getTotatravnuitdi() {
		return $this->totatravnuitDi;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences_lu", type="text", nullable=true)
	 */
	private $mabsencesLu;

	public function setMabsenceslu($pmAbsenceslu) {
		$this->mabsencesLu = $pmAbsenceslu;
		return $this;
	}

	public function getMabsenceslu() {
		return $this->mabsencesLu;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences_ma", type="text", nullable=true)
	 */
	private $mabsencesMa;

	public function setMabsencesma($pmAbsencesma) {
		$this->mabsencesMa = $pmAbsencesma;
		return $this;
	}

	public function getMabsencesma() {
		return $this->mabsencesMa;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences_me", type="text", nullable=true)
	 */
	private $mabsencesMe;

	public function setMabsencesme($pmAbsencesme) {
		$this->mabsencesMe = $pmAbsencesme;
		return $this;
	}

	public function getMabsencesme() {
		return $this->mabsencesMe;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences_je", type="text", nullable=true)
	 */
	private $mabsencesJe;

	public function setMabsencesje($pmAbsencesje) {
		$this->mabsencesJe = $pmAbsencesje;
		return $this;
	}

	public function getMabsencesje() {
		return $this->mabsencesJe;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences_ve", type="text", nullable=true)
	 */
	private $mabsencesVe;

	public function setMabsencesve($pmAbsencesve) {
		$this->mabsencesVe = $pmAbsencesve;
		return $this;
	}

	public function getMabsencesve() {
		return $this->mabsencesVe;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences_sa", type="text", nullable=true)
	 */
	private $mabsencesSa;

	public function setMabsencessa($pmAbsencessa) {
		$this->mabsencesSa = $pmAbsencessa;
		return $this;
	}

	public function getMabsencessa() {
		return $this->mabsencesSa;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mabsences_di", type="text", nullable=true)
	 */
	private $mabsencesDi;

	public function setMabsencesdi($pmAbsencesdi) {
		$this->mabsencesDi = $pmAbsencesdi;
		return $this;
	}

	public function getMabsencesdi() {
		return $this->mabsencesDi;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nbseancespres_lu", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nbseancespresLu;

	public function setNbseancespreslu($pnBseancespreslu) {
		$this->nbseancespresLu = $pnBseancespreslu;
		return $this;
	}

	public function getNbseancespreslu() {
		return $this->nbseancespresLu;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nbseancespres_ma", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nbseancespresMa;

	public function setNbseancespresma($pnBseancespresma) {
		$this->nbseancespresMa = $pnBseancespresma;
		return $this;
	}

	public function getNbseancespresma() {
		return $this->nbseancespresMa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nbseancespres_me", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nbseancespresMe;

	public function setNbseancespresme($pnBseancespresme) {
		$this->nbseancespresMe = $pnBseancespresme;
		return $this;
	}

	public function getNbseancespresme() {
		return $this->nbseancespresMe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nbseancespres_je", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nbseancespresJe;

	public function setNbseancespresje($pnBseancespresje) {
		$this->nbseancespresJe = $pnBseancespresje;
		return $this;
	}

	public function getNbseancespresje() {
		return $this->nbseancespresJe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nbseancespres_ve", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nbseancespresVe;

	public function setNbseancespresve($pnBseancespresve) {
		$this->nbseancespresVe = $pnBseancespresve;
		return $this;
	}

	public function getNbseancespresve() {
		return $this->nbseancespresVe;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nbseancespres_sa", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nbseancespresSa;

	public function setNbseancespressa($pnBseancespressa) {
		$this->nbseancespresSa = $pnBseancespressa;
		return $this;
	}

	public function getNbseancespressa() {
		return $this->nbseancespresSa;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nbseancespres_di", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nbseancespresDi;

	public function setNbseancespresdi($pnBseancespresdi) {
		$this->nbseancespresDi = $pnBseancespresdi;
		return $this;
	}

	public function getNbseancespresdi() {
		return $this->nbseancespresDi;
	}

	/**
	 * @var \groupes
	 *
	 * @ORM\ManyToOne(targetEntity="groupes")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="igroupe", referencedColumnName="iid_groupe")
	 * })
	 */
	private $igroupe;

	public function setIgroupe($piGroupe) {
		$this->igroupe = $piGroupe;
		return $this;
	}

	public function getIgroupe() {
		return $this->igroupe;
	}

}
