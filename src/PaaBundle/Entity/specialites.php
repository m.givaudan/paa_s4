<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * specialites
 *
 * @ORM\Table(name="paa.specialites", uniqueConstraints={@ORM\UniqueConstraint(name="u_specialites_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="specialites_nom_specia", columns={"cnom"})})
 * @ORM\Entity
 */
class specialites {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_specialite", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.specialites_iid_specialite_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSpecialite = 'Specialites';

	public function setIidspecialite($piIdspecialite) {
		$this->iidSpecialite = $piIdspecialite;
		return $this;
	}

	public function getIidspecialite() {
		return $this->iidSpecialite;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSpecialite;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=100, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}
        
        // LP 03/09/2020 debut
        
        /**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */        
        private $icouleur = '16777215';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}
        
        // LP 03/09/2020

}
