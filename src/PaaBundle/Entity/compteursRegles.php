<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * compteursRegles
 *
 * @ORM\Table(name="paa.compteurs_regles", indexes={@ORM\Index(name="IDX_82B16ECF3EE9E7ED", columns={"icompteur"})})
 * @ORM\Entity
 */
class compteursRegles {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_compteur_regle", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.compteurs_regles_iid_compteur_regle_seq", allocationSize=1, initialValue=1)
	 */
	private $iidCompteurRegle = 'compteurs_règles';

	public function setIidcompteurregle($piIdcompteurregle) {
		$this->iidCompteurRegle = $piIdcompteurregle;
		return $this;
	}

	public function getIidcompteurregle() {
		return $this->iidCompteurRegle;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidCompteurRegle;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iregle", type="integer", nullable=true)
	 */
	private $iregle = '4';

	public function setIregle($piRegle) {
		$this->iregle = $piRegle;
		return $this;
	}

	public function getIregle() {
		return $this->iregle;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nsens", type="decimal", precision=2, scale=0, nullable=true)
	 */
	private $nsens = '1';

	public function setNsens($pnSens) {
		$this->nsens = $pnSens;
		return $this;
	}

	public function getNsens() {
		return $this->nsens;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bvaljour", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bvaljour = '0.0';

	public function setBvaljour($pbValjour) {
		$this->bvaljour = $pbValjour;
		return $this;
	}

	public function getBvaljour() {
		return $this->bvaljour;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="bvalheure", type="float", precision=10, scale=0, nullable=true)
	 */
	private $bvalheure = '1';

	public function setBvalheure($pbValheure) {
		$this->bvalheure = $pbValheure;
		return $this;
	}

	public function getBvalheure() {
		return $this->bvalheure;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="minfossuppl", type="text", nullable=true)
	 */
	private $minfossuppl = '';

	public function setMinfossuppl($pmInfossuppl) {
		$this->minfossuppl = $pmInfossuppl;
		return $this;
	}

	public function getMinfossuppl() {
		return $this->minfossuppl;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \compteurs
	 *
	 * @ORM\ManyToOne(targetEntity="compteurs")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="icompteur", referencedColumnName="iid_compteur")
	 * })
	 */
	private $icompteur;

	public function setIcompteur($piCompteur) {
		$this->icompteur = $piCompteur;
		return $this;
	}

	public function getIcompteur() {
		return $this->icompteur;
	}

}
