<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * activitesFlags
 *
 * @ORM\Table(name="paa.activites_flags", uniqueConstraints={@ORM\UniqueConstraint(name="u_activites_flags_iacti", columns={"iacti"})}, indexes={@ORM\Index(name="activites_flags_acti_plant", columns={"iactiplantype"}), @ORM\Index(name="activites_flags_conf_plant", columns={"lconfplantype"})})
 * @ORM\Entity
 */
class activitesFlags {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iactiplantype", type="integer", nullable=true)
	 */
	private $iactiplantype = '0';

	public function setIactiplantype($piActiplantype) {
		$this->iactiplantype = $piActiplantype;
		return $this;
	}

	public function getIactiplantype() {
		return $this->iactiplantype;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lconfplantype", type="boolean", nullable=true)
	 */
	private $lconfplantype = false;

	public function setLconfplantype($plConfplantype) {
		$this->lconfplantype = $plConfplantype;
		return $this;
	}

	public function getLconfplantype() {
		return $this->lconfplantype;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tmaj = '0001-01-01';
	private $tmaj = null;

	public function setTmaj($ptMaj) {
		$this->tmaj = $ptMaj;
		return $this;
	}

	public function getTmaj() {
		return $this->tmaj;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lmodifsemaine", type="boolean", nullable=true)
	 */
	private $lmodifsemaine = false;

	public function setLmodifsemaine($plModifsemaine) {
		$this->lmodifsemaine = $plModifsemaine;
		return $this;
	}

	public function getLmodifsemaine() {
		return $this->lmodifsemaine;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lmodifplantype", type="boolean", nullable=true)
	 */
	private $lmodifplantype = false;

	public function setLmodifplantype($plModifplantype) {
		$this->lmodifplantype = $plModifplantype;
		return $this;
	}

	public function getLmodifplantype() {
		return $this->lmodifplantype;
	}

	/**
	 * @var \activites
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="activites")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")
	 * })
	 */
	private $iacti;

	public function setIacti($piActi) {
		$this->iacti = $piActi;
		return $this;
	}

	public function getIacti() {
		return $this->iacti;
	}

}
