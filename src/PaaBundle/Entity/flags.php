<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * flags
 *
 * @ORM\Table(name="paa.flags", uniqueConstraints={@ORM\UniqueConstraint(name="u_flags_iid_table&iid_res", columns={"iid_res", "iid_table"})}, indexes={@ORM\Index(name="flags_ttable", columns={"iid_table"}), @ORM\Index(name="flags_tidres", columns={"iid_res"})})
 * @ORM\Entity
 */
class flags {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.flags_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="iid_table", type="decimal", precision=3, scale=0, nullable=true)
	 */
	private $iidTable = '0.0';

	public function setIidtable($piIdtable) {
		$this->iidTable = $piIdtable;
		return $this;
	}

	public function getIidtable() {
		return $this->iidTable;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmodif", type="datetime", nullable=true)
	 */
	private $tmodif = 'paa.paadatetime()';

	public function setTmodif($ptModif) {
		$this->tmodif = $ptModif;
		return $this;
	}

	public function getTmodif() {
		return $this->tmodif;
	}

}
