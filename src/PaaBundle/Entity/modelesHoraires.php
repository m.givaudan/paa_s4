<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * modelesHoraires
 *
 * @ORM\Table(name="paa.modeles_horaires", uniqueConstraints={@ORM\UniqueConstraint(name="u_modeles_horaires_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class modelesHoraires {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_modele", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.modeles_horaires_iid_modele_seq", allocationSize=1, initialValue=1)
	 */
	private $iidModele = 'Modeles_Horaires';

	public function setIidmodele($piIdmodele) {
		$this->iidModele = $piIdmodele;
		return $this;
	}

	public function getIidmodele() {
		return $this->iidModele;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidModele;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=50, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mhoraires", type="text", nullable=true)
	 */
	private $mhoraires = '';

	public function setMhoraires($pmHoraires) {
		$this->mhoraires = $pmHoraires;
		return $this;
	}

	public function getMhoraires() {
		return $this->mhoraires;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur = 'rgb(255, 255, 255)';

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		return $this->icouleur;
	}

}
