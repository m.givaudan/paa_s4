<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
//MG 20200309 Ajout Début
use App\PaaBundle\Repository\seancesRepository;
//MG 20200309 Ajout Fin
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumns;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * activites
 *
 * @ORM\Table(name="paa.activites", indexes={@ORM\Index(name="activites_mati_corr", columns={"imaticorr"}), @ORM\Index(name="activites_isemaine", columns={"isemaine_fin"}), @ORM\Index(name="activites_a_sem", columns={"isemaine"}), @ORM\Index(name="activites_semfin", columns={"isemaine_fin"}), @ORM\Index(name="activites_acti_base", columns={"iactibase"})})
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\activitesRepository")     
 */
class activites {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->groupes = new ArrayCollection();
		$this->usagers = new ArrayCollection();
		$this->intervenants = new ArrayCollection();
		$this->salles = new ArrayCollection();
		$this->séances = new ArrayCollection();
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_activite", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.activites_iid_activite_seq", allocationSize=1, initialValue=1)
	 */
	private $iidActivite = 'ACTIVITÉS';

	// -------------------------------------------------------------------
	// <editor-fold defaultstate="collapsed" desc="Propriétés simples">
	public function setIidactivite($piIdactivite) {
		$this->iidActivite = $piIdactivite;
		return $this;
	}

	public function getIidactivite() {
		return $this->iidActivite;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidActivite;
	}

	// AV 04/03/2019 fin

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="isemaine", type="integer", nullable=true)
	 */
	private $isemaine = '0';

	public function setIsemaine($piSemaine) {
		$this->isemaine = $piSemaine;
		return $this;
	}

	public function getIsemaine() {
		return $this->isemaine;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ndureedft", type="decimal", precision=6, scale=2, nullable=true)
	 */
	private $ndureedft = '0.5';

	public function setNdureedft($pnDureedft) {
		$this->ndureedft = $pnDureedft;
		return $this;
	}

	public function getNdureedft() {
		return $this->ndureedft;
	}

	/**
	 * @var float
	 *
	 * @ORM\Column(name="nprevusem", type="float", precision=10, scale=0, nullable=true)
	 */
	private $nprevusem;

	public function setNprevusem($pnPrevusem) {
		$this->nprevusem = $pnPrevusem;
		return $this;
	}

	public function getNprevusem() {
		return $this->nprevusem;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mremarque", type="string", length=100, nullable=true)
	 */
	private $mremarque = '';

	public function setMremarque($pmRemarque) {
		$this->mremarque = $pmRemarque;
		return $this;
	}

	public function getMremarque() {
// LG 20200510 old		return $this->mremarque;
		return trim($this->mremarque);
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="string", length=100, nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tmaj", type="datetime", nullable=true)
	 */
	private $tmaj;

	public function setTmaj($ptMaj) {
		$this->tmaj = $ptMaj;
		return $this;
	}

	public function getTmaj() {
		return $this->tmaj;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="crythme", type="string", length=10, nullable=true)
	 */
	private $crythme = 'E';

	public function setCrythme($pcRythme) {
		$this->crythme = $pcRythme;
		return $this;
	}

	public function getCrythme() {
		return $this->crythme;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mraznoseance", type="text", nullable=true)
	 */
	private $mraznoseance = '';

	public function setMraznoseance($pmRaznoseance) {
		$this->mraznoseance = $pmRaznoseance;
		return $this;
	}

	public function getMraznoseance() {
		return $this->mraznoseance;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lestabsence", type="boolean", nullable=true)
	 */
	private $lestabsence;

	public function setLestabsence($plEstabsence) {
		$this->lestabsence = $plEstabsence;
		return $this;
	}

	public function getLestabsence() {
		return $this->lestabsence;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lestpresence", type="boolean", nullable=true)
	 */
	private $lestpresence;

	public function setLestpresence($plEstpresence) {
		$this->lestpresence = $plEstpresence;
		return $this;
	}

	public function getLestpresence() {
		return $this->lestpresence;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="imaticorr", type="integer", nullable=true)
	 */
	private $imaticorr;

	public function setImaticorr($piMaticorr) {
		$this->imaticorr = $piMaticorr;
		return $this;
	}

	public function getImaticorr() {
		return $this->imaticorr;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lforceusagers", type="boolean", nullable=true)
	 */
	private $lforceusagers = false;

	public function setLforceusagers($plForceusagers) {
		$this->lforceusagers = $plForceusagers;
		return $this;
	}

	public function getLforceusagers() {
		return $this->lforceusagers;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lusagersttesseances", type="boolean", nullable=true)
	 */
	private $lusagersttesseances = false;

	public function setLusagersttesseances($plUsagersttesseances) {
		$this->lusagersttesseances = $plUsagersttesseances;
		return $this;
	}

	public function getLusagersttesseances() {
		return $this->lusagersttesseances;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lgenerique", type="boolean", nullable=true)
	 */
	private $lgenerique = false;

	public function setLgenerique($plGenerique) {
		$this->lgenerique = $plGenerique;
		return $this;
	}

	public function getLgenerique() {
		return $this->lgenerique;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="isemaine_fin", type="integer", nullable=true)
	 */
	private $isemaineFin = '0';

	public function setIsemainefin($piSemainefin) {
		$this->isemaineFin = $piSemainefin;
		return $this;
	}

	public function getIsemainefin() {
		return $this->isemaineFin;
	}

	/**
	 * @var \actibases
	 *
	 * @ORM\ManyToOne(targetEntity="actibases")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iactibase", referencedColumnName="iid_actibase")
	 * })
	 */
	private $iactibase;

	public function setIactibase($piActibase) {
		$this->iactibase = $piActibase;
		return $this;
	}

	public function getIactibase() {
		return $this->iactibase;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="icouleur", type="integer", nullable=true)
	 */
	private $icouleur;

	public function setIcouleur($piCouleur) {
		$this->icouleur = $piCouleur;
		return $this;
	}

	public function getIcouleur() {
		if ($this->icouleur !== NULL)
			return $this->icouleur;
		else
			return $this->getIcouleurActiBase();
	}

	public function getIcouleurActiBase() {
		return $this->iactibase->getIcouleur();
	}

	// </editor-fold>
	// -------------------------------------------------------------------
	// <editor-fold defaultstate="collapsed" desc="Collection des séances">
	/**
	 * @ORM\OneToMany(targetEntity="App\PaaBundle\Entity\seances", mappedBy="oActivité", orphanRemoval=true)
	 * @ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite", nullable=false)
	 */
	private $séances;

	/**
	 * @return Collection|seances[]
	 */
	public function getSéances(): Collection {
		if ($this->séances) {
			return $this->séances;
		} else {
			return new ArrayCollection([]);
		}
	}

//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//    public function addSéances(actiRealisation $séance): self
	public function addSéances(seances $séance): self {
//MG Modification 20200218 Fin 
		echo $CetteMéthodeEstJamaisUtilisée;
		if (!$this->séances->contains($séance)) {
			$this->séances[] = $séance;
			$séance->setactivites($this);
		}
		return $this;
	}

	// </editor-fold>
	// -------------------------------------------------------------------
	// <editor-fold defaultstate="collapsed" desc="Collection des groupes">
	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\groupes",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite",nullable=false)},
	 * inverseJoinColumns = {@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_groupe")}
	 * )
	 * LG 20200508 : cette relation devrait impliquer de poser la valeur 'G' dans cType_Res de Acti_Ressources, mais Symfony ne le permet pas -> un trigger PG corrige ça à l'insert et à l'update
	 */
	private $groupes;

	/**
	 * @return Collection|groupes[]
	 */
	public function getGroupes(): Collection {
//		if ($this->groupes) {
// echo $toto ;
		return $this->groupes;
//		} else {
//			return new ArrayCollection([]);
//		}
	}

	public function setGroupes() {
		//MG 20200803 Utilisé lors de la création d'une activité depuis la liste des activités
		echo $CetteMéthodeEstJamaisUtilisée;
		if (!$this->groupes->contains($groupe)) {
			$this->groupes[] = $groupe;
//			$groupe->setactivites($this);
		}
		return $this;
	}

	public function addGroupe(/* App\PaaBundle\Entity\salles LG 2019-06-28 Type Invalide?  */ $Groupe) {
		//MG 20200803 Utilisé lors de la création d'une activité depuis la liste des activités
//		echo $CetteMéthodeEstJamaisUtilisée;
		if (!$this->groupes || !$this->groupes->contains($Groupe)) {
			$this->groupes[] = $Groupe;
			//MG Modification 20200803
			//Double passage lors de la création d'une activite dans la liste d'activite , 
//			$Groupe->setactivites($this);
		}
		return $this;
	}

	public function removeGroupe(/* App\PaaBundle\Entity\salles LG 2019-06-28 Type Invalide ?   */$Groupe) {
		//MG 20200803 Utilisé lors de la création d'une activité depuis la liste des activités
//		echo $CetteMéthodeEstJamaisUtilisée;
		if ($this->groupes && $this->groupes->contains($Groupe)) {
			$this->groupes->remove($Groupe);
		}
		return $this;
	}

	// </editor-fold>
	// -------------------------------------------------------------------
	// <editor-fold defaultstate="collapsed" desc="Collection des usagers">
	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\usagers",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite",nullable=false)},
	 * inverseJoinColumns = {@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_usager")}
	 * )
	 * LG 20200508 : cette relation devrait impliquer de poser la valeur 'U' dans cType_Res de Acti_Ressources, mais Symfony ne le permet pas -> un trigger PG corrige ça à l'insert et à l'update
	 */
	private $usagers;

	/**
	 * @return Collection|usagers[]
	 */
	public function getUsagers(): Collection {
		if ($this->usagers) {
			return $this->usagers;
		} else {
			return new ArrayCollection([]);
		}
	}

	public function setUsagers() {
		echo $CetteMéthodeEstJamaisUtilisée;
		if (!$this->usagers || !$this->usagers->contains($usagers)) {
			$this->usagers[] = $usagers;
			$usagers->setactivites($this);
		}
		return $this;
	}

	public function addUsager(/* App\PaaBundle\Entity\salles LG 2019-06-28 Type Invalide?  */ $usager) {
		echo $CetteMéthodeEstJamaisUtilisée;
		if (!$this->usagers || !$this->usagers->contains($usager)) {
			$this->usagers[] = $usager;
			$usager->setactivites($this);
		}
		return $this;
	}

	public function removeUsager(/* App\PaaBundle\Entity\salles LG 2019-06-28 Type Invalide ?   */$usager) {
		echo $CetteMéthodeEstJamaisUtilisée;
		if ($this->usagers && $this->usagers->contains($usager)) {
			// $this->usagers[] = $usager;
		}
		return $this;
	}

	// </editor-fold>
	// -------------------------------------------------------------------
	// <editor-fold defaultstate="collapsed" desc="Collection des intervenants">
	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\intervenants",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite",nullable=false)},
	 * inverseJoinColumns = {@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_intervenant")}
	 * )
	 * LG 20200508 : cette relation devrait impliquer de poser la valeur 'I' dans cType_Res de Acti_Ressources, mais Symfony ne le permet pas -> un trigger PG corrige ça à l'insert et à l'update
	 */
	private $intervenants;

	/**
	 * @return Collection|intervenants[]
	 */
	public function getintervenants(): Collection {
		if ($this->intervenants) {
			return $this->intervenants;
		} else {
			return new ArrayCollection([]);
		}
	}

	public function setintervenants($po) {
//              if (!$this->intervenants->contains($intervenants)) {
//            $this->intervenants[] = $intervenants;
//            $intervenants->setactivites($this);
//        }
// echo $CetteMéthodeEstJamaisUtilisée ;
		$this->intervenants = $po;

		return $this;
	}

	// </editor-fold>
	// -------------------------------------------------------------------
	// <editor-fold defaultstate="collapsed" desc="Collection des salles">
	/**
	 * @ORM\ManyToMany(targetEntity="App\PaaBundle\Entity\salles",cascade={"persist"})
	 * @ORM\JoinTable(name="paa.acti_ressources",
	 * joinColumns={@ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite",nullable=false)},
	 * inverseJoinColumns = {@ORM\JoinColumn(name="iid_res", referencedColumnName="iid_salle")}
	 * )
	 * LG 20200508 : cette relation devrait impliquer de poser la valeur 'S' dans cType_Res de Acti_Ressources, mais Symfony ne le permet pas -> un trigger PG corrige ça à l'insert et à l'update
	 */
	private $salles;

	/**
	 * @return Collection|salles[]
	 */
	public function getSalles(): Collection {
		if ($this->salles) {
			return $this->salles;
		} else {
			return new ArrayCollection([]);
		}
	}

	public function setSalles() {
		echo $CetteMéthodeEstJamaisUtilisée;
		if (!$this->salles || !$this->salles->contains($salles)) {
			$this->salles[] = $salles;
			$salles->setactivites($this);
		}
		return $this;
	}

	public function addSalle(/* App\PaaBundle\Entity\salles LG 2019-06-28 Type Invalide?  */ $salle) {
//		echo $CetteMéthodeEstJamaisUtilisée;
		if (!$this->salles || !$this->salles->contains($salle)) {
			$this->salles[] = $salle;
			$salle->setactivites($this);
		}
		return $this;
	}

	public function removeSalle(/* App\PaaBundle\Entity\salles LG 2019-06-28 Type Invalide ?   */$salle) {
		echo $CetteMéthodeEstJamaisUtilisée;
		if ($this->salles && $this->salles->contains($salle)) {
			// $this->salles[] = $salle;
		}
		return $this;
	}

	// </editor-fold>

	public function getressources() {
		$mesId = [];
		$mintervenants = $this->getintervenants();
		$mgroupes = $this->getgroupes();
		$musagers = $this->getusagers();
		$msalles = $this->getsalles();
		foreach ($mintervenants as $value) {
			$mesId[] = 'I' . $value->getId();
		}
		foreach ($mgroupes as $value) {
			$mesId[] = 'G' . $value->getId();
		}
		foreach ($musagers as $value) {
			$mesId[] = 'U' . $value->getId();
		}
		foreach ($msalles as $value) {
			$mesId[] = 'S' . $value->getId();
		}
		return $mesId;
	}

	private $objectif;

	public function getObjectif() {
		return $this->objectif;
	}

	public function setObjectif($objectif) {
		$this->objectif = $objectif;
	}

	private $realise;

	public function getrealise() {
//MG 20200309 Modification Début
		return $this->realise;
//            return 11;
//            $loRepo = new seancesRepository();
//			$heureRealise = $loRepo->NombreHeuresRealise($this->id);
//			$em = $this->getDoctrine()->getManager();
//			$heureRealise = $em->getRepository("PaaBundle:seances")->NombreHeuresRealise($this->id);
//			return $heureRealise;
//MG 20200309 Modification Début
	}

	public function setrealise($realise) {
		$this->realise = $realise;
	}

	private $remarque;

	public function getremarque() {
		return $this->remarque;
	}

	public function setremarque($remarque) {
		$this->remarque = $remarque;
	}

	private $validite;

	public function getvalidite() {
		return $this->validite;
	}

	public function setvalidite($validite) {
		$this->validite = $validite;
	}

	private $heureDebut;

	public function getheureDebut() {
		return $this->heureDebut;
	}

	public function setheureDebut($heureDebut) {
		$this->heureDebut = $heureDebut;
	}

	private $heureFin;

	public function getheureFin() {
		return $this->heureFin;
	}

	public function setheureFin($heureFin) {
		$this->heureFin = $heureFin;
	}

}
