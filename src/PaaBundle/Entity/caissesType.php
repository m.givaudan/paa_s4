<?php
//MC 20200903 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\PaaBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * caissesType
 *
 * @ORM\Table(name="paa.ttypecaisses", uniqueConstraints={@ORM\UniqueConstraint(name="u_caissesType_cnom", columns={"cnom"})})
 * @ORM\Entity
 */
class caissesType {
    
    /**
    * @var integer
    *
    * @ORM\Column(name="iid_typecaisses", type="integer", nullable=false)
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="SEQUENCE")
    * @ORM\SequenceGenerator(sequenceName="paa.typecaisses_seq", allocationSize=1, initialValue=17)
    */
    private $iid_typecaisses /* = 'TYPECAISSES' */;

    public function setIid_typecaisses($piIdtypecaisses) {
       $this->iid_typecaisses = $piIdtypecaisses;
       return $this;
    }

    public function getIid_typecaisses() {
        return $this->iid_typecaisses;
    }
    
    public function getId() {
		return $this->iid_typecaisses;
	}
        
    /**
    * @var string
    *
    * @ORM\Column(name="cnom", type="string", length=200, nullable=true)
    */
    private $cnom = '';

    public function setCnom($pcNom) {
           $this->cnom = $pcNom;
           return $this;
    }

    public function getCnom() {
           return $this->cnom;
    }
    
    /**
    * @var string
    *
    * @ORM\Column(name="ctableusagers_caisses", type="string", length=200, nullable=true)
    */
    private $ctableusagers_caisses = '';

    public function setCtableusagers_caisses($pcTableusagers_caisses) {
           $this->ctableusagers_caisses = $pcTableusagers_caisses;
           return $this;
    }
    
    public function setCtableusagersCaisses($pcTableusagers_caisses){
        $this->ctableusagers_caisses = $pcTableusagers_caisses;
        return $this;
    }

    public function getCtableusagers_caisses() {
           return $this->ctableusagers_caisses;
    }
    public function getCtableusagersCaisses(){
        return $this->ctableusagers_caisses;
    }


    /**
    * @var string
    *
    * @ORM\Column(name="cnomdft", type="string", length=200, nullable=true)
    */
    private $cnomdft = '';

    public function setCnomdft($pcNomdft) {
           $this->cnomdft = $pcNomdft;
           return $this;
    }

    public function getCnomdft() {
           return $this->cnomdft;
    }

	
}
