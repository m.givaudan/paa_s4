<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * regimescpam
 *
 * @ORM\Table(name="paa.regimescpam", uniqueConstraints={@ORM\UniqueConstraint(name="u_regimescpam_ccode", columns={"ccode"}), @ORM\UniqueConstraint(name="u_regimescpam_cnom", columns={"cnom"})}, indexes={@ORM\Index(name="regimescpam_code", columns={"ccode"})})
 * @ORM\Entity
 */
class regimescpam {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_regime", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.regimescpam_iid_regime_seq", allocationSize=1, initialValue=1)
	 */
	private $iidRegime = 'REGIMESCPAM';

	public function setIidregime($piIdregime) {
		$this->iidRegime = $piIdregime;
		return $this;
	}

	public function getIidregime() {
		return $this->iidRegime;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidRegime;
	}

	// AV 04/03/2019 fin

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ccode", type="string", length=5, nullable=true)
	 */
	private $ccode = '';

	public function setCcode($pcCode) {
		$this->ccode = $pcCode;
		return $this;
	}

	public function getCcode() {
		return $this->ccode;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cnom", type="string", length=100, nullable=true)
	 */
	private $cnom = '';

	public function setCnom($pcNom) {
		$this->cnom = $pcNom;
		return $this;
	}

	public function getCnom() {
		return $this->cnom;
	}

}
