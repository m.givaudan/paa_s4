<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * actiInterdictions
 *
 * @ORM\Table(name="paa.acti_interdictions", indexes={@ORM\Index(name="acti_interdictions_acti", columns={"iacti"}), @ORM\Index(name="acti_interdictions_ai_fin", columns={"tfin"}), @ORM\Index(name="acti_interdictions_ai_debut", columns={"tdebut"})})
 * @ORM\Entity
 */
class actiInterdictions {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_interdiction", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.acti_interdictions_iid_interdiction_seq", allocationSize=1, initialValue=1)
	 */
	private $iidInterdiction = 'acti_interdictions';

	public function setIidinterdiction($piIdinterdiction) {
		$this->iidInterdiction = $piIdinterdiction;
		return $this;
	}

	public function getIidinterdiction() {
		return $this->iidInterdiction;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidInterdiction;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebut", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tdebut = '0001-01-01';
	private $tdebut = null;

	public function setTdebut($ptDebut) {
		$this->tdebut = $ptDebut;
		return $this;
	}

	public function getTdebut() {
		return $this->tdebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tfin", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tfin = '0001-01-01';
	private $tfin = null;

	public function setTfin($ptFin) {
		$this->tfin = $ptFin;
		return $this;
	}

	public function getTfin() {
		return $this->tfin;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="clibelle", type="string", length=100, nullable=true)
	 */
	private $clibelle;

	public function setClibelle($pcLibelle) {
		$this->clibelle = $pcLibelle;
		return $this;
	}

	public function getClibelle() {
		return $this->clibelle;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mcommentaire", type="text", nullable=true)
	 */
	private $mcommentaire = '';

	public function setMcommentaire($pmCommentaire) {
		$this->mcommentaire = $pmCommentaire;
		return $this;
	}

	public function getMcommentaire() {
		return $this->mcommentaire;
	}

	/**
	 * @var \activites
	 *
	 * @ORM\ManyToOne(targetEntity="activites")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")
	 * })
	 */
	private $iacti;

	public function setIacti($piActi) {
		$this->iacti = $piActi;
		return $this;
	}

	public function getIacti() {
		return $this->iacti;
	}

}
