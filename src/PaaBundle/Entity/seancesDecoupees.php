<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seancesDecoupees
 *
 * @ORM\Table(name="paa.seances_decoupees", indexes={@ORM\Index(name="seances_decoupees_id_res", columns={"iid_res"}), @ORM\Index(name="seances_decoupees_type_res", columns={"ctype_res"}), @ORM\Index(name="seances_decoupees_id_seance", columns={"iseance"}), @ORM\Index(name="seances_decoupees_sd_debut", columns={"tdebut"}), @ORM\Index(name="IDX_20E2EDBFE4366230", columns={"iacti"})})
 * @ORM\Entity
 */
class seancesDecoupees {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.seances_decoupees_iid_seq", allocationSize=1, initialValue=1)
	 */
	private $iid;

	public function setIid($piId) {
		$this->iid = $piId;
		return $this;
	}

	public function getIid() {
		return $this->iid;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ctype_res", type="string", length=1, nullable=true)
	 */
	private $ctypeRes = '';

	public function setCtyperes($pcTyperes) {
		$this->ctypeRes = $pcTyperes;
		return $this;
	}

	public function getCtyperes() {
		return $this->ctypeRes;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_res", type="integer", nullable=true)
	 */
	private $iidRes = '0';

	public function setIidres($piIdres) {
		$this->iidRes = $piIdres;
		return $this;
	}

	public function getIidres() {
		return $this->iidRes;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tdebut", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tdebut = '0001-01-01';
	private $tdebut = null;

	public function setTdebut($ptDebut) {
		$this->tdebut = $ptDebut;
		return $this;
	}

	public function getTdebut() {
		return $this->tdebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="tfin", type="datetime", nullable=true)
	 */
// LG 20200811 old	private $tfin = '0001-01-01';
	private $tfin = null;

	public function setTfin($ptFin) {
		$this->tfin = $ptFin;
		return $this;
	}

	public function getTfin() {
		return $this->tfin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="inbconflits", type="integer", nullable=true)
	 */
	private $inbconflits = '0';

	public function setInbconflits($piNbconflits) {
		$this->inbconflits = $piNbconflits;
		return $this;
	}

	public function getInbconflits() {
		return $this->inbconflits;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="npriorite", type="decimal", precision=8, scale=3, nullable=true)
	 */
	private $npriorite = '0.0';

	public function setNpriorite($pnPriorite) {
		$this->npriorite = $pnPriorite;
		return $this;
	}

	public function getNpriorite() {
		return $this->npriorite;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lestabsence", type="boolean", nullable=true)
	 */
	private $lestabsence = false;

	public function setLestabsence($plEstabsence) {
		$this->lestabsence = $plEstabsence;
		return $this;
	}

	public function getLestabsence() {
		return $this->lestabsence;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lestgroupe", type="boolean", nullable=true)
	 */
	private $lestgroupe = false;

	public function setLestgroupe($plEstgroupe) {
		$this->lestgroupe = $plEstgroupe;
		return $this;
	}

	public function getLestgroupe() {
		return $this->lestgroupe;
	}

	/**
	 * @var \activites
	 *
	 * @ORM\ManyToOne(targetEntity="activites")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iacti", referencedColumnName="iid_activite")
	 * })
	 */
	private $iacti;

	public function setIacti($piActi) {
		$this->iacti = $piActi;
		return $this;
	}

	public function getIacti() {
		return $this->iacti;
	}

	/**
	 * @var \seances
	 *
	 * @ORM\ManyToOne(targetEntity="seances")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="iseance", referencedColumnName="iid_seance")
	 * })
	 */
	private $iseance;

	public function setIseance($piSeance) {
		$this->iseance = $piSeance;
		return $this;
	}

	public function getIseance() {
		return $this->iseance;
	}

}
