<?php

// Fichier genere par Doctrine et repris par CorrigeDoctrine.prg
// (fichier c:\luc\projets vb et foxpro\paa45 sp�cifiques\progs\aa.PRG)

namespace App\PaaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * seriesseances
 * 
 * @ORM\Table(name="paa.seriesseances")
 * @ORM\Entity(repositoryClass="App\PaaBundle\Repository\seriesseancesRepository"))
 */
class seriesseances {

	public function __construct() {
// LG 20200601 déac 		$this->ddebut = 'date()';
// LG 20200601 déac		$this->dfin = 'gomonth(date(), 12)';
	}
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="iid_serie", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="paa.seriesseances_iid_serie_seq", allocationSize=1, initialValue=1)
	 */
	private $iidSerie;

	public function setIidserie($piIdserie) {
		$this->iidSerie = $piIdserie;
		return $this;
	}

	public function getIidserie() {
		return $this->iidSerie;
	}

	// AV 04/03/2019 début
	public function getId() {
		return $this->iidSerie;
	}

	// AV 04/03/2019 fin

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="ddebut", type="date", nullable=true)
	 */
// LG 20200601 old
//	private $ddebut = 'date()';
	private $ddebut = null;

	public function setDdebut($pdDebut) {
		$this->ddebut = $pdDebut;
		return $this;
	}

	public function getDdebut() {
		return $this->ddebut;
	}

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dfin", type="date", nullable=true)
	 */
//	private $dfin = 'gomonth(date(), 12)';
	private $dfin = null;

	public function setDfin($pdFin) {
		$this->dfin = $pdFin;
		return $this;
	}

	public function getDfin() {
		return $this->dfin;
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="ifrequence", type="integer", nullable=false)
	 */
	private $ifrequence = '1';

	public function setIfrequence($piFrequence) {
		$this->ifrequence = $piFrequence;
		return $this;
	}

	public function getIfrequence() {
		return $this->ifrequence;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lexclutsamedi", type="boolean", nullable=true)
	 */
	private $lexclutsamedi = true;

	public function setLexclutsamedi($plExclutsamedi) {
		$this->lexclutsamedi = $plExclutsamedi;
		return $this;
	}

	public function getLexclutsamedi() {
		return $this->lexclutsamedi;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lexclutdimanche", type="boolean", nullable=true)
	 */
	private $lexclutdimanche = true;

	public function setLexclutdimanche($plExclutdimanche) {
		$this->lexclutdimanche = $plExclutdimanche;
		return $this;
	}

	public function getLexclutdimanche() {
		return $this->lexclutdimanche;
	}

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="lexclutjoursferies", type="boolean", nullable=true)
	 */
	private $lexclutjoursferies = true;

	public function setLexclutjoursferies($plExclutjoursferies) {
		$this->lexclutjoursferies = $plExclutjoursferies;
		return $this;
	}

	public function getLexclutjoursferies() {
		return $this->lexclutjoursferies;
	}

}
