<?php

namespace App\PaaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PaaBundle extends Bundle {
// LG 20200516 début
// Selon https://symfony.com/doc/current/bundles/FOSUserBundle/overriding_controllers.html
// Dire que le parent de notre bundle est FOS_User permet de surcharger des classes de celui-ci
	public function getParent() {
		return 'FOSUserBundle';
	}
// LG 20200516 fin
}
