<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\PaaBundle\Security;

use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;

use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;

class UserManager extends BaseUserManager {
	// Inspiré de https://matthiasnoback.nl/2014/05/inject-the-manager-registry-instead-of-the-entity-manager/
	public function __construct(PasswordUpdaterInterface $passwordUpdater
			, CanonicalFieldsUpdater $canonicalFieldsUpdater
			, ManagerRegistry $managerRegistry
			, Session $session
			, RequestStack $requestStack
			, $class
			) {

// LG 20200804 début : pour minify.js (????)
		if (!method_exists($requestStack, "getCurrentRequest")) {
			// Etonnant !!!
				// Pourquoi minify passe-t-il par là ?
				// Pourquoi cette méthode n'existe-t-elle pas dans ce contexte ?
			echo "requestStack n'a pas de méthode getCurrentRequest()" ;
		} else 
// LG 20200804 fin
		if (!$requestStack->getCurrentRequest()) {
//MG Modification 20201307 Début
			// Bloque le minify.bat, pourquoi ?
			echo "requestStack->getCurrentRequest() est NULL !" ;
//MG Modification 20201307 Fin
			$lsDBName = null ;
		} else {
			try {
				if (!$requestStack->getCurrentRequest()) {
					echo "$requestStack->getCurrentRequest() est NULL !" ;
					$lsDBName = null ;
				} else {
					// Algo identique à celui de RouterDecorator, à factoriser
					$pathInfo = $requestStack->getCurrentRequest()->getPathInfo() ;
					if (strpos($pathInfo, "/DB/") === false) {
						// Pas de mention de DB dans cette URL
						$lsDBName = 'default' ;
					} else {
						$UriBase = explode("?", $pathInfo)[0] ;		// Enlever les éventuels paramètres en GET
						$laURi = explode("/", $UriBase) ;
						$lsDBName = strtolower($laURi[2]) ;
					}
					$session->set('DBName', $lsDBName) ;
				}
				
			} catch (\Exception $e) {
				
			}
		}
#LG 20200630 old $om = $managerRegistry->getManager($lsDBName);
		$om = $managerRegistry->getManager();
		
		parent::__construct($passwordUpdater, $canonicalFieldsUpdater, $om, $class);
	}
}
