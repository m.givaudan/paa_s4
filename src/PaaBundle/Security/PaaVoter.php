<?php

// Classe prenant la décision d'accorder l'accès aux ressources de PAA
// Via <ControllerObject>->get('security.authorization_checker')->isGranted('modiUsager', <idUsager>)
// https://symfony.com/doc/3.4/components/security/authorization.html
// https://symfony.com/doc/3.4/security/voters.html#security-voters-change-strategy

namespace App\PaaBundle\Security;

use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use App\PaaBundle\Entity\users;
use App\PaaBundle\Component\Paa\Paa_Constantes_Securite;

class PaaVoter extends Voter {

	// these strings are just invented: you can use anything
	const MENU = 'menu';
	const VOIR = 'voir';
	const MODIFIER = 'modi';
	const AJOUTER = 'ajou';
	const MENU_DONNEESDEBASE = "DonnéesDeBase";
	const MENU_AUTRESLISTES = "AutresListes";
	const MENU_SEMAINEREELLE = "SemaineRéelle";
	const MENU_SEMAINETYPE = "SemaineType";
	const MENU_EDITIONS = "Editions";
	const MENU_OUTILS = "Outils";
	const MENU_USERS = "Users";

	/**
	 * @var AccessDecisionManagerInterface
	 */
	private $decisionManager;
	private $entityManager;

	/**
	 * PaaVoter constructor.
	 *
	 * @param AccessDecisionManagerInterface $decisionManager
	 */
	public function __construct(AccessDecisionManagerInterface $decisionManager, EntityManagerInterface $em) {
		$this->decisionManager = $decisionManager;
		$this->entityManager = $em;
	}

	protected function supports($attribute, $item = 0) {
		// if the attribute isn't one we support, return false
		if (!in_array(substr($attribute, 0, 4), array(self::MENU, self::VOIR, self::MODIFIER, self::AJOUTER))) {
			return false;
		}

//        // only vote on Post objects inside this voter
//        if (!$item instanceof Post) {
//            return false;
//        }

		return true;
	}

	protected function voteOnAttribute($attribute, $item = 0, TokenInterface $token) {
		$user = $token->getUser();

		if (!$user instanceof users) {
			// the user must be logged in; if not, deny access
			return false;
		}

//        // you know $subject is a Post object, thanks to supports
//        /** @var Post $post */
//        $post = $subject;

		$lsTypeAccès = substr($attribute, 0, 4);  // self::VOIR ou self::MODIFIER
		$lsTypeRessource = substr($attribute, 4);  // "Usager", "Intervenant", ...

		if ($lsTypeAccès == self::MENU) {
			return $this->voteOnMenu($item, $user, $token);
		} else {
			return $this->voteOnDataAccess($lsTypeAccès, $lsTypeRessource, $item, $user, $token);
		}

		throw new \LogicException('This code should not be reached!');
	}

	// Déterminer si l'user en cours a droit au menu demandé
	private function voteOnMenu($psMenu, $poUser, TokenInterface $token) {
		switch ($psMenu) {
			case self::MENU_USERS:
				return $this->decisionManager->decide($token, array('ROLE_ADMIN'));

			case self::MENU_DONNEESDEBASE:
				return $this->decisionManager->decide($token, array('ROLE_ADMIN'));
//MC 20200902 DEBUT
                        case self::MENU_AUTRESLISTES:
                                return $this->decisionManager->decide($token, array('ROLE_ADMIN'));
                            
                        case self::MENU_EDITIONS:
                                return $this->decisionManager->decide($token, array('ROLE_ADMIN'));
//MC 20200902 FIN
			case self::MENU_OUTILS:
				return $this->decisionManager->decide($token, array('ROLE_ADMIN'));

			case self::MENU_SEMAINETYPE:
				return $this->decisionManager->decide($token, array('ROLE_CONCEPTEUR'));

			case self::MENU_SEMAINEREELLE:
				return $this->decisionManager->decide($token, array('ROLE_ENSEIGNANT')) || $this->decisionManager->decide($token, array('ROLE_ETUDIANT'));

			default:
				throw new \LogicException('Cas de menu non prévu : psMenu = $psMenu');
		}
	}

	private function getDroitsCurrentUser($poUser) {
		if ($poUser->getctype_res() == 'I') {
			// Intervenant : voir s'il a les droits
			$liIntervenant = $poUser->getiid_res();
// $liIntervenant = 923456789 ;
// $loIntervenant = $this->entityManager->getRepository("PaaBundle:intervenants")->find($liIntervenant)
			if ($liIntervenant == null) {
				// Cet user n'a pas d'intervenant
//					echo $Err1 ;
			} else if (($loIntervenant = $this->entityManager->getRepository("PaaBundle:intervenants")->find($liIntervenant)) == null) {
				
			} else if ($loIntervenant == null) {
				// Cet intervenant n'a pas été retrouvé
//					echo $Err2 ;
			} else {
				return $loIntervenant->getIdroitsw();
			}
		}
		return 0;
	}

	private function voteOnDataAccess($psTypeAccès, $psTypeRessource, $item, $poUser, TokenInterface $token) {

		// Simplification abusive à corriger par la suite
		switch ($psTypeAccès) {
			case self::VOIR:
// LG 20190830 old                return $this->decisionManager->decide($token, array('ROLE_USER'));
				return $this->decisionManager->decide($token, array('ROLE_ENSEIGNANT'));
			case self::MODIFIER:
			//return true;
			case self::AJOUTER:
			//return true;
		}


// LG 20190830 début

		if (!$this->voteOnDataAccess(self::VOIR, $psTypeRessource, $item, $poUser, $token)) {
			// L'utilisateur en cours n'a même pas le droit de voir
			return false;
		}
// LG 20190830 fin
		$lbAutorise = $this->decisionManager->decide($token, array('ROLE_ADMIN'));

/// LG 20190607 début
		if ($lbAutorise)
			return true;

		// ---------------------------------------------------------------------------------------
		// Récursivité sur une liste d'items
		if (Vartype($item) == "C") {
			// On a fourni une liste d'items
			// Rappeller cette fonction pour chaque item
			$laItems = explode(",", $item);

			foreach ($laItems as $lsItem) {
				$liItem = intval($lsItem);
				if (!$this->voteOnDataAccess($psTypeAccès, $psTypeRessource, $liItem, $poUser, $token)) {
					// Pas les droits sur cet item, pas la peine d'examiner les autres
					return false;
				}
			}
			// Si on arrive là, c'est que les droits sont OK
			return true;
		}
		// Fin de récursivité sur une liste d'items
		// ---------------------------------------------------------------------------------------
// LG 20190607 fin
//echo $err ;
// return false ;
// Pour test
		$lbAutorise = false;
		if (!$lbAutorise && $psTypeRessource == 'users') {
			// L'utilisateur actuel n'a pas les droits, mais peut-être s'agit-il de sa propre fiche ?
			$loUser = $token->getUser();
			if ($loUser) {
				$lbAutorise = (strval($loUser->getid()) === strval($item));
				if ($lbAutorise) {
					$peutchangerprofil = $loUser->isLPeutChangerProfil();
					$lbAutorise = false;
				}
			}
		} else if (false) {
			// Pas d'accès en écriture pour les étudiants
		} else {
			// On doit se baser sur les droits de l'user courant
			$loUser = $token->getUser();
			$liId_User = $loUser->getid();
			$liDroits = $this->getDroitsCurrentUser($loUser);
// deac pour tests
			if ($liDroits == Paa_Constantes_Securite::eiDroitsW_PredefAdministrateur)
				return true;
			if ($liDroits == Paa_Constantes_Securite::eiDroitsW_PredefAucun)
				return false;

			// Cas de figure traités selon le modèle de sécurité.prg, fonction "Autorise"
			// Transformation de certains cas
			if (in_array($psTypeRessource, ['PoseSéance', 'EnlèveSéance', 'ModifieSéance', 'ModiHorairesSéance', 'SesPropresParticipations'])) {
				// Il faut tester si c'est une séance d'absence ou non
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//				$loSéancesRepository = $this->entityManager->getRepository("PaaBundle:actiRealisations") ;
				$loSéancesRepository = $this->entityManager->getRepository("PaaBundle:seances");
//MG Modification 20200218 Fin
				$liActiBase = null;
				If (Upper($psTypeRessource) === Upper("PoseSéance")) {
// echo $err1 ;
					// On passe l'Id d'activité
					// Déterminer si c'est une absence et en profiter pour récupérer l'activité de base
					$liActi = $item;
					$liSeance = null;
					$lbEstAbsence = $loSéancesRepository->EstAbsence($liSeance, $liActi, $liActiBase);
				} Else {
					// On passe l'Id de séance
					// Déterminer si c'est une absence et en profiter pour récupérer l'activité de base
					$liActi = null;
					$liSeance = $item;
					$lbEstAbsence = $loSéancesRepository->EstAbsence($liSeance, $liActi, $liActiBase);
				}
				If ($lbEstAbsence) { // C'est une absence
					$psTypeRessource .= "Absence";
				} else {
					$lbEstPrésence = $loSéancesRepository->EstPrésence(null, null, $liActiBase);
					If ($lbEstPrésence) { // C'est une présence
						$psTypeRessource .= "Présence";
					}
				}
			}
			// Modifier une séance
			if (in_array($psTypeRessource, ['EnlèveSéance', 'ModiHorairesSéance', 'ChangeActivitéDeSéance', 'SesPropresParticipations'])) {
				// désactivé car absent de Sécurité.PRG if ($liDroits & Paa_Constantes_Securite::eiDroits_Séances > 0) return true ;
// echo $err2 ;
				if ($liDroits & Paa_Constantes_Securite::eiDroits_ModifSéanceActiCrééeParAutre > 0)
					return true;
//MG Modification 20200218 Début
//Table actiRealisations renommer en seances
//				$loSéancesRepository = $this->entityManager->getRepository("PaaBundle:actiRealisations") ;
				$loSéancesRepository = $this->entityManager->getRepository("PaaBundle:seances");
//MG Modification 20200218 Fin
				if ($loSéancesRepository->RtvUserCréateur($item) == $liId_User) {
					// L'utilisateur en cours est le créateur de la séance
					// Il peut la modifier
					return true;
				}
				If ($liDroits & Paa_Constantes_Securite::eiDroits_SéancesQuiConcernentIntervenant > 0
						And $item
						And $loSéancesRepository->SéanceUtiliseRessource($item, "I", $liId_User)
						And ! $loSéancesRepository->EstAbsence($item)) {
					// Droits sur ses propres séances et cette séance le concerne
					return true;
				}
// LG : Obsolète ? En tout cas, je n'ai pas fini de traduire ce code depuis VFP
//				If ($liDroits & Paa_Constantes_Securite::eiDroits_SesPropresParticipations > 0
//										And $item
//										And Evl($pvVar2, "") = "I"
//										And Evl($pvVar3, 0) = $liId_User
//										And $loSéancesRepository->SéanceUtiliseRessource($item, "I", $liId_User)
//										And ($pvVar4	//&&  pvVar4 : .T. si c'est pour l'ajustement d'une participation à l'intérieur d'une séance 
//											Or $loSéancesRepository->EstAbsence($item) or $loSéancesRepository->EstPrésence($item)
//											Or $loSéancesRepository->RtvUserCréateur("tSeances", $item) = $liId_User)) {
//					return true ;
//				}
			} else if (in_array($psTypeRessource, ['GetId_Activité', 'PoseSéance'])) {
				If ($liDroits & Paa_Constantes_Securite::eiDroits_Séances > 0) {
					return true;
				} Else If ($liDroits & Paa_Constantes_Securite::eiDroits_SesPropresParticipations > 0
						And $item
						And $loSéancesRepository->SéanceUtiliseRessource($item, "I", $liId_User)) {
					// Droits sur ses propres participations et cette séance le concerne
					return true;
				}
			} else {
				// Cas non prévu
			}
		}

		return $lbAutorise;
	}

}
