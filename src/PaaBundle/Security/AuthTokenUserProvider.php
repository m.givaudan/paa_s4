<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuthTokenUserProvider
 *
 * @author Haris
 */

namespace App\PaaBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;

class AuthTokenUserProvider implements UserProviderInterface {

	protected $authTokenRepository;
	protected $userRepository;

	public function __construct(EntityRepository $authTokenRepository, EntityRepository $userRepository) {
		$this->authTokenRepository = $authTokenRepository;
		$this->userRepository = $userRepository;
	}

	public function getAuthToken($authTokenHeader) {
		return $this->authTokenRepository->findOneByValue($authTokenHeader);
	}

	public function loadUserByUsername($email) {
		return $this->userRepository->findByEmail($email);
	}

	public function refreshUser(UserInterface $user) {
		// Le systéme d'authentification est stateless, on ne doit donc jamais appeler la méthode refreshUser
		throw new UnsupportedUserException();
	}

	public function supportsClass($class) {
		return 'App\PaaBundle\Entity\users' === $class;
	}

}
