<?php

namespace App\Limoog\PartageBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller; Deprecated since symfony 4.2
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController {

	public function indexAction() {
		return $this->render('LimoogPartageBundle:Default:index.html.twig');
	}

}
