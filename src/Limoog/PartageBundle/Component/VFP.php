<?php

// Fonctions "VFP-like"

require_once(dirname(__FILE__) . '/xTools.php');

function Vartype($pvVal) {
	$lcType = gettype($pvVal);
	if ($lcType == "boolean")
		return "L";
	if ($lcType == "integer")
		return "N";
	if ($lcType == "double")
		return "N";
	if ($lcType == "string") {
		if (isDate($pvVal))
			return "D";
		if (isDateTime($pvVal))
			return "T";
		return "C";
	}
	if ($lcType == "array")
		return "A";
	if ($lcType == "object")
		return "O";
	if ($lcType == "resource")
		return "X";
	if ($lcType == "NULL")
		return "X";
	if ($lcType == "unknown type")
		return "X";
	return "X";
}

function Iif($lExpression, $eExpression1, $eExpression2) {
	return $lExpression ? $eExpression1 : $eExpression2;
}

function Strtran($cSearched, $cSearchFor, $cReplacement) {
	return str_replace($cSearchFor, $cReplacement, $cSearched);
}

function EVL($eExpression1, $eExpression2) {
	return (empty($eExpression1) && !is_null($eExpression1)) ? $eExpression2 : $eExpression1;
}

function ENVL($eExpression1, $eExpression2) {
	return (is_null($eExpression1) || empty($eExpression1)) ? $eExpression2 : $eExpression1;
}

function NVL($eExpression1, $eExpression2) {
	return is_null($eExpression1) ? $eExpression2 : $eExpression1;
}

// ************************************************************************************
// Fonctions date
function Year($pvDate) {
	if (IsNull($pvDate))
		return null;
	$pvDate = Strtran($pvDate, "/", "-"); // S'assurer des délimiteurs de date
//	$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	if (Vartype($pvDate) == "D")
		$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	else if (Vartype($pvDate) == "T")
		$lvDate = DateTime::createFromFormat("Y-m-d G:i:s", $pvDate);
	else
		$lvDate = null;
	return $lvDate ? Int($lvDate->format("Y")) : null;
}

function Month($pvDate) {
	if (IsNull($pvDate))
		return null;
	$pvDate = Strtran($pvDate, "/", "-"); // S'assurer des délimiteurs de date
//	$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	if (Vartype($pvDate) == "D")
		$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	else if (Vartype($pvDate) == "T")
		$lvDate = DateTime::createFromFormat("Y-m-d G:i:s", $pvDate);
	else
		$lvDate = null;
	return $lvDate ? Int($lvDate->format("m")) : null;
}

function Day($pvDate) {
	if (IsNull($pvDate))
		return null;
	$pvDate = Strtran($pvDate, "/", "-"); // S'assurer des délimiteurs de date
//	$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	if (Vartype($pvDate) == "D")
		$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	else if (Vartype($pvDate) == "T")
		$lvDate = DateTime::createFromFormat("Y-m-d G:i:s", $pvDate);
	else
		$lvDate = null;
	return $lvDate ? Int($lvDate->format("d")) : null;
}

function Hour($pt) {
	if (IsNull($pt))
		return null;
	if (Vartype($pt) == "T")
		return Int(substr($pt, 11, 2));
	else
		LG_die(__Function__ . " : le paramètre recu n'est pas de type Time : " . $pt);
}

function Minute($pt) {
	if (IsNull($pt))
		return null;
	if (Vartype($pt) == "T")
		return Int(substr($pt, 14, 2));
	else
		LG_die(__Function__ . " : le paramètre recu n'est pas de type Time : " . $pt);
}

function Sec($pt) {
	if (IsNull($pt))
		return null;
	if (Vartype($pt) == "T")
		return Int(substr($pt, 17, 2));
	else
		LG_die(__Function__ . " : le paramètre recu n'est pas de type Time : " . $pt);
}

function CTOT($pt) {
	if (IsNull($pt))
		return null;
	if (Vartype($pt) == "T")
		return $pt;
	else
		LG_die(__Function__ . " : le paramètre recu n'est pas de type Time : " . $pt);
}

function CTOD($pt) {
	if (IsNull($pt))
		return null;
	if (Vartype($pt) == "T")
		return TTOD($pt);
	if (Vartype($pt) == "D")
		return $pt;
	else
		LG_die(__Function__ . " : le paramètre recu n'est pas de type Date ou Time : " . $pt);
}

function TTOD($pt) {
	if (IsNull($pt))
		return null;
	return substr($pt, 0, 10);
}

function DTOC($pd, $_1 = 0) {
	if (IsNull($pd))
		return null;
	if (Vartype($pd) == "T")
		$pd = TTOD($pd);
	if (Vartype($pd) == "D") {
//		if ($_1 == 1) return date_format(date_create($pd), 'Ymd') ;
//		return date_format(date_create($pd), 'Y/m/d') ;
		$pd = date_create($pd);
	}
	if (Vartype($pd) == "O" And get_class($pd) == "DateTime") {
		if ($_1 == 1)
			return date_format($pd, 'Ymd');
		return date_format($pd, 'Y/m/d');
	} else
		LG_die(__Function__ . " : le paramètre recu n'est pas de type Date ou Time : " . $pd);
}

function TTOC($pd, $_1 = 0) {
	if (IsNull($pd))
		return null;
	if (Vartype($pd) == "T") {
//		if ($_1 == 1) return date_format(date_create($pd), 'Ymd His') ;
//		return date_format(date_create($pd), 'Y/m/d H:i:s') ;
		$pd = date_create($pd);
	}
	if (Vartype($pd) == "O" And get_class($pd) == "DateTime") {
		if ($_1 == 1)
			return date_format($pd, 'Ymd His');
		return date_format($pd, 'Y/m/d H:i:s');
	} else
		LG_die(__Function__ . " : le paramètre recu n'est pas de type Date ou Time : " . $pd);
}

function CDOW($pvDate) {
	if (IsNull($pvDate))
		return null;
	$pvDate = Strtran($pvDate, "/", "-"); // S'assurer des délimiteurs de date
	$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	$liDay = DOW($pvDate);
	if ($liDay == 1)
		return "Lundi";
	if ($liDay == 2)
		return "Mardi";
	if ($liDay == 3)
		return "Mercredi";
	if ($liDay == 4)
		return "Jeudi";
	if ($liDay == 5)
		return "Vendredi";
	if ($liDay == 6)
		return "Samedi";
	if ($liDay == 7)
		return "Dimanche";
	return Null;
}

function DOW($pvDate, $pnFirstDayOfWeek = null) {
	$pvDate = Strtran($pvDate, "/", "-"); // S'assurer des délimiteurs de date
	$lvDate = DateTime::createFromFormat("Y-m-d", $pvDate);
	if (!$lvDate)
		return null;
	$liDay = $lvDate->format("w");
//	$liDay = $liDay + ($pnFirstDayOfWeek - 2) ;
	$liDay += 1;
	$pnFirstDayOfWeek = ENVl($pnFirstDayOfWeek, 2);
//	$pnFirstDayOfWeek = $pnFirstDayOfWeek%7 ;
	if ($pnFirstDayOfWeek == 1) {
		// Jour1 = dimanche : comme dans PHP
		// ras
	} else if ($pnFirstDayOfWeek == 2) {
// echo $pnFirstDayOfWeek . "<br>" ;
		$liDay = $liDay - 1;
	} else if ($pnFirstDayOfWeek == 3) {
		// Jour1 = mardi	
		$liDay = $liDay - 2;
	} else if ($pnFirstDayOfWeek == 4) {
		// Jour1 = merdredi	
		$liDay = $liDay - 3;
	} else if ($pnFirstDayOfWeek == 5) {
		// Jour1 = jeudi	
		$liDay = $liDay - 4;
	} else if ($pnFirstDayOfWeek == 6) {
		// Jour1 = vendredi	
		$liDay = $liDay - 5;
	} else if ($pnFirstDayOfWeek == 7) {
		// Jour1 = samedi	
		$liDay = $liDay - 6;
	} else {
		$liDay = Null;
		lg_Die("Cas non prévu");
	}
	if ($liDay <= 0)
		$liDay += 7;
	return $liDay;
}

function IsNull($v) {
	return is_null($v);
}

function Int($pnNb) {
	if (IsNull($pnNb))
		return null;
	return intval($pnNb);
}

function Val($pnNb) {
	if (IsNull($pnNb))
		return null;
	return floatval($pnNb);
}

// ************************************************************************************
// Fonctions caractère
function Dollar($cContenu, $cContenant) {
	return strpos($cContenant, $cContenu) !== false;
}

function Upper($v) {
	if (IsNull($v))
		return null;
	return strtoupper($v);
}

function Left($v, $piLen) {
	if (IsNull($v))
		return null;
	if (IsNull($piLen))
		return null;
	return substr($v, 0, $piLen);
}

function Right($v, $piLen) {
	if (IsNull($v))
		return null;
	if (IsNull($piLen))
		return null;
	if ($piLen == 0)
		return '';
	return substr($v, strlen($v) - $piLen, strlen($v));
}

function Alltrim($s) {
	if (IsNull($s))
		return null;
	return Trim($s);
}

function Len($s) {
	if (IsNull($s))
		return null;
	return strLen($s);
}

function At($cSearchExpression, $cExpressionSearched, $nOccurrence = 0) {
	if (IsNull($cSearchExpression))
		return null;
	if (IsNull($cExpressionSearched))
		return null;
	$pos = strpos($cExpressionSearched, $cSearchExpression, $nOccurrence);
	if ($pos === False)
		return 0;
	else
		return $pos + 1;
}

function Atc($cSearchExpression, $cExpressionSearched, $nOccurrence = 0) {
	return At(Upper($cSearchExpression), Upper($cExpressionSearched), $nOccurrence);
}
