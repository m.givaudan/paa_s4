<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Limoog\PartageBundle\Component;

class cParametresBDD {

	private $oCnxn = null;

	function __construct($loCnxn) {
		$this->oCnxn = $loCnxn;
	}

	public function getParamètre($psNomParamètre, $pvValeurDft, $psType) {
//		if (!isset($_SESSION[$psNomParamètre])) {
		// Il faut déterminer maintenant la valeur
		$_SESSION[$psNomParamètre] = $this->getParamètreBDD($psNomParamètre, $pvValeurDft, $psType);
//		}
		return $_SESSION[$psNomParamètre];
	}

	public function setParamètre($psNomParamètre, $pvValeur, $psType) {
		$_SESSION[$psNomParamètre] = $pvValeur;
		return $this->setParamètreBDD($psNomParamètre, $pvValeur, $psType);
	}

	public function rtvIdMachineByName($psNomMachine) {
		$liMachine = $this->oCnxn->rtvSQLResult("Select iIdMachine From paa.Paa_Para Where Variable = 'lstMachines' And Trim(Varcar) = '$psNomMachine'");
		return $liMachine;
	}

	protected function getParamètreBDD($psNomParamètre, $pvValeurDft, $psType) {
// LG 20190510 début
		if (isset($this->$psNomParamètre)) {
			// Ce paramètre a déja été initialisé : utiliser sa valeur mémorisée
			return $this->$psNomParamètre;
		}
// LG 20190510 début
		$lsSQL = "Select Partage.Paa_Para_GetParametre( :NomPara , :ValeurDft, :Type) As vval";
		$loRes = $this->oCnxn->fetchAll($lsSQL, Array('NomPara' => $psNomParamètre, 'ValeurDft' => $pvValeurDft, 'Type' => $psType));
		if ($loRes) {
			$lvRetu = $loRes[0]["vval"];
		} else {
			$lvRetu = $pvValeurDft;
		}
// LG 20190510 début
		// Mémoriser pour une éventuelle utilisation ultérieure
		$this->$psNomParamètre = $lvRetu;
// LG 20190510 début
		return $lvRetu;
	}

	protected function setParamètreBDD($psNomParamètre, $pvValeur, $psType) {
// LG 20190510 début
		if (isset($this->$psNomParamètre)) {
			// Ce paramètre a déja été initialisé : libérer sa valeur mémorisée
			unset($this->$psNomParamètre);
		}
// LG 20190510 début
		$lsSQL = "Select Partage.Paa_Para_SetParametre( :NomPara , :Valeur, :Type) As vval";
// LG 20190718 début
		if ($pvValeur === 'null' && strpos($psType, "N")) {
			$pvValeur = (int) null;
		}
// LG 20190718 fin
		$loRes = $this->oCnxn->fetchAll($lsSQL, Array('NomPara' => $psNomParamètre, 'Valeur' => $pvValeur, 'Type' => $psType));
		if ($loRes) {
			$lvRetu = $loRes[0]["vval"];
		} else {
			$lvRetu = "Echec";
		}
		return $lvRetu;
	}

}
