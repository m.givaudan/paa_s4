<?php

// Fonctions outils pour JSON
//$lcRep = dirname(dirname(__FILE__)) ;
//require_once($lcRep . '/Partages/VFP.php');
require_once('VFP.php');

// echo JSON_ToLitteral('"') ;
// Convertir une variable en litt�ral compatible avec JSON
function JSON_ToLitteral($pvVal, $psType = null) {

	$lcType = gettype($pvVal);

	if (!EmptyOrNull($psType)) {
		
	} else if (IsNull($pvVal))
		return 'null';
	else if ($lcType == "integer")
		$psType = "integer";
	else if ($lcType == "double")
		$psType = "double";
	else if ($lcType == "boolean")
		$psType = "boolean";
	else if ($lcType == "date")
		$psType = "date";
	else if ($lcType == "timstamp")
		$psType = "timestamp";
	else
		$psType = "string";

	if ($lcType == "boolean" && !IsNull($pvVal))
		$pvVal = $pvVal ? "true" : "false";
	if (in_Array(strToLower($psType), array("string", "date", "timestamp")) && !IsNull($pvVal)) {
		// Chaine de caract�res ou date ou datetime
		$pvVal = '"'
				. strTran($pvVal, '"', '\\' . '"') /* strTran($pvVal, '"', '@') */
				. '"';
//		$pvVal = json_encode($pvVal) ;	
	} else if (!IsNull($pvVal)) {
		$pvVal .= '';
	}
	if (IsNull($pvVal))
		$pvVal = 'null';

	return $pvVal;
}
