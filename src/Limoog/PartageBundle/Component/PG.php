<?php

// Fonctions outils pour Postgres
// $lcRep = dirname(dirname(__FILE__)) ;
// require_once($lcRep . '/Partages/VFP.php');
require_once('VFP.php');

// echo PG_ToLitteral(new DateTime()) ;

function PG_ToLitteral($pvVal, $psType = null) {

	$lcType = gettype($pvVal);

	if (!EmptyOrNull($psType)) {
		
	} else if (IsNull($pvVal))
		return 'null';
	else if ($lcType == "integer")
		$psType = "Integer";
	else if ($lcType == "double")
		$psType = "Numeric";
	else if ($lcType == "boolean")
		$psType = "Boolean";
	else if ($lcType == "date")
		$psType = "Date";
	else if ($lcType == "timstamp")
		$psType = "Timestamp";
	else
		$psType = "Text";

// echo EmptyOrNull($psType) . ", " . $pvVal . " : " . $lcType . ", " . $psType . "<br>" ;

	if ($lcType == "boolean" && !IsNull($pvVal))
		$pvVal = $pvVal ? "True" : "False";
	if (in_Array(strToUpper($psType), array("DATE", "TIMESTAMP", "TEXT", "VARCHAR")) && !IsNull($pvVal)) {
		$pvVal = "'"
				. $pvVal
				. "'";
	}
	if (IsNull($pvVal))
		$pvVal = 'null';

	/* 	if ($lcType == "integer") return "N" ;
	  if ($lcType == "double") return "N" ;
	  if ($lcType == "array") return "A" ;
	  if ($lcType == "object") return "O" ;
	  if ($lcType == "resource") return "X" ;
	  if ($lcType == "NULL") return "X" ;
	  if ($lcType == "unknown type") return "X" ;
	 */
	return $pvVal . "::" . $psType;
}
