<?php

require_once('VFP.php');

// ************************************************************************************************
// Termine l'exécution en cas d'erreur
function LG_die($pcMsg) {

	$pcMsg = ENvl($pcMsg, "Erreur inconnue");
	echo __Function__ . " : " . $pcMsg . "<BR>";
	debug_print_backtrace();
//							. debug_backtrace() 
	echo "<BR>";

	$pcMsg = "lg_Die : " . $pcMsg;

	// die("Erreur d'exécution dans " . $pcFile . " ligne " . $piLine . " : " . $pcMsg . " Arrêt du script.") ;
	throw new Exception($pcMsg);
}

// ************************************************************************************************
// Renvoie True si la chaine passée correspond à une date
// http://stackoverflow.com/questions/19271381/correctly-determine-if-date-string-is-a-valid-date-in-that-format
function isDate($date) {
	$date = Strtran($date, "/", "-"); // S'assurer des délimiteurs de date
	$d = DateTime::createFromFormat('Y-m-d', $date);
	return $d && $d->format('Y-m-d') === $date;
}

// ************************************************************************************************
// Renvoie True si la chaine passée correspond à une dateHeure
// http://stackoverflow.com/questions/19396735/how-can-i-check-if-a-variable-type-is-datetime
function isDateTime($date) {
	$date = Strtran($date, "/", "-"); // S'assurer des délimiteurs de date
	$format = 'Y-m-d H:i:s';
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}

// ************************************************************************************************
// Ajoute un nb de jours à une date
function dateAdd($date, $piNbJours) {
	$date = Strtran($date, "/", "-"); // S'assurer des délimiteurs de date
	$d = DateTime::createFromFormat('Y-m-d', $date);
	$d = date_add($d, date_interval_create_from_date_string($piNbJours . ' days'));
	try {
		$d = $d->format('Y/m/d');
	} catch (Exception $e) {
		$d = Null;
	}
	return $d;
}

// ************************************************************************************************
// Instancie une nouvelle variable globale si elle n'est pas déja instanciée
function getOrReUseGlobalObject(&$poObject, $pcGlobalName, $pcClassName, &$psErr, array $paParams = array()) {
	$lbKO = False;
	if (isset($GLOBALS[$pcGlobalName]) && $GLOBALS[$pcGlobalName] && is_a($GLOBALS[$pcGlobalName], $pcClassName)) {
		// Connexion globale déja définie : la réutiliser
		$poObject = $GLOBALS[$pcGlobalName];
		$psErr = "";
	} else {
		// Instancier l'objet selon le nob de paramètres fournis pour lui
		if (count($paParams) === 0) {
			$poObject = new $pcClassName();
		} else if (count($paParams) === 1) {
			$poObject = new $pcClassName($paParams[0]);
		}
		if (!is_a($poObject, $pcClassName)) {
			$psErr = "Echec de la création de l'objet " . $pcClassName . ".";
			$lbKO = True;
		} else {
			$GLOBALS[$pcGlobalName] = $poObject;
		}
	}

	return !$lbKO;
}

// ************************************************************************************************
function EmptyOrNull($eExpression1) {
	return Empty_LG($eExpression1);
}

function Empty_LG($eExpression1) {
	return (is_null($eExpression1) || empty($eExpression1));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoie la date de début de semaine par rapport à une date passée en paramètre
function dateDébutSemaine($pvDate) {

	$lbVersTime = false;
	if (Empty_LG($pvDate)) {
//		If ($lbVersTime) {
//			$pvDate = uNumDateVersTime($pvDate, 0) ;
//		}
//		Return $pvDate ;
		lg_Die(__Function . " date fournie vide");
	} else if (Vartype($pvDate) == "C") {
		// Convertir en date
		$pvDate = CTOD($pvDate);
	} else if (Vartype($pvDate) == "D") {
		// RAS
	} else if (Vartype($pvDate) == "T") {
		// Convertir en date, et renvoyer l'heure 00:00:00
		$pvDate = Ttod($pvDate);
		$lbVersTime = True;
	} else {
		lg_Die(__Function . " type de paramètre non prévu");
	}

	$pvDate = dateAdd($pvDate, -1 * (DOW($pvDate, 2) - 1));

	If ($lbVersTime) {
		$pvDate = uNumDateVersTime($pvDate, 0);
	}

	Return $pvDate;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Convertir une date + heure numérique vers le format Time
// pvDate			: Date, ou DateTime, ou chaine au format "^YYYY-MM-DD"
// piHeure			: Heure (format N ou T)
// piTypeArrondi		: Dft = 7 (Arrondi à la minute)
function uNumDateVersTime($pvDate, $piHeure, $piTypeArrondi = 7) {

	If (Vartype($piHeure) == "T") {
		// On fournit un DateTime : le convertir en Num
		$piHeure = uHeureVersNum($piHeure);
	}
// LG 20190411 début
	$pvDate = substr($pvDate, 0, 10);
// LG 20190411 début

	$lsTime = $pvDate . " ";
	IF (estMinuit($piHeure)) {
		// Protection contre le fait que 24 heures donnent une date invalide
		$lsTime = $lsTime . "23:59:59";
	} Else {
		$lsTime = $lsTime . uNumVersHeure($piHeure // piNum
						, True  // pbSecondes
						, True  // pbReturnString
						, True  // pbStringTypeChaineHeure
						, False  // pbAutoriseSup24hrs
						, False  // pbTronqueCaractèresAuxHeures
						, False  // pbAffLblMin
						, Iif(Empty($piTypeArrondi), 7, $piTypeArrondi)  // piTypeArrondi
				);
	}

	Return CTOT($lsTime);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Renvoie True si l'heure passée est minuit
// LG 20060223
Function estMinuit($pvHeure) {

	$liHeure = false;

	if (Vartype($pvHeure) == "T") {
		$liHeure = uHeureVersNum($pvHeure);
	} else if (Vartype($pvHeure) == "N") {
		$liHeure = $pvHeure;
	} else {
		lg_Die(__FUNCTION__ . " : paramètre fourni inattendu : " . $pvHeure . " (type : " . Vartype($pvHeure) . ")");
	}

	Return $liHeure >= 23.999;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// Convertir une durée de type décimal en nombre de type heure/minute/secondes
// piNum 					: valeur à convertir
//(pbSecondes)				: si on doit aussi renvoyer les secondes
//(pbReturnString) 			: si True, renvoie une chaine de caractères
//(pbStringTypeChaineHeure)	: si True, la valeur string s'écrit "10:10" et non pas "10h10"
//(pbAutoriseSup24hrs)		: si True, on accepte une durée supérieure à 24 heures
//(pbTronqueCaractèresAuxHeures)	: si True, "01h34" s'écrit "1h34"
//(pbAffLblMin)				: si True, "01h34" s'écrit "01h34mn"
//(piTypeArrondi)			: Dft : 1 (aucun arrondi)
Function uNumVersHeure($piNum
, $pbSecondes = False
, $pbReturnStringInutilisé = False
, $pbStringTypeChaineHeure = False
, $pbAutoriseSup24hrs = False
, $pbTronqueCaractèresAuxHeures = False
, $pbAffLblMin = False
, $piTypeArrondi = 1
) {

//	Local liHeures,liSecondes, liMinutes, lvDurée, lsSigne, liNbCarHeures, lbReturn 

	$piNum = Nvl($piNum, 0);
	IF (VarType($piNum) == "C") {
//		$piNum = Val($piNum) ;
		$piNum = (float) $piNum;
	}
	IF (VarType($piNum) == "T") {
		$piNum = Hour($piNum) + Minute($piNum) / 60 + Sec($piNum) / 60 / 60;
	}

	If (Vartype($piNum) !== "L") {
		$lsSigne = Iif($piNum < 0, "-", "");
		$piNum = Abs($piNum);
	}

	$liHeures = INT($piNum); //Nb d'heures
//	IF ($pbAutoriseSup24hrs && !$pbReturnStringInutilisé) {
//		lg_Die(__Function__ . " : deux options incompatibles") ;
//	}

	IF (!$pbAutoriseSup24hrs && $liHeures > 24) {
//		DO WHILE $liHeures > 24 
//			&gsSetStepOn, "_4M50OPD79"
//			wWAIT("Une durée supérieure à 24 heures a été rencontrée dans la fonction uNumVersHeure : ce cas n'est pas géré", 'wind Nowait')
//
//			$liHeures = $liHeures - 24
//		Enddo
		lg_Die(__Function__ . " : durée supérieure à 24 heures (" . $piNum . ")");
	}

	$liMinutes = $piNum - INT($piNum);   //Minutes décimales
	$liMinutes = $liMinutes * 60;	//Minutes en base 60, incluant les secondes

	If ($pbSecondes && ($piTypeArrondi !== 7)) {
		// On souhaite calculer le Nb de secondes
		$liSecondes = $liMinutes - INT($liMinutes); //Secondes en fraction de minute
		$liSecondes = Round($liSecondes * 60);   //Secondes en base 60
		$liMinutes = INT($liMinutes);	 //Minutes entières
	} Else {
		// On ne souhaite pas calculer le Nb de secondes
		// Ou on arrondit à la minute
		$liSecondes = 0;
		$liMinutes = Round($liMinutes, 0);	//Minutes entières	
	}

	// Corrections
	If ($liSecondes >= 59.9) {
		$liMinutes = $liMinutes + 1;
		$liSecondes = 0;
	}
	If ($liMinutes >= 59.9) {
		$liHeures = $liHeures + 1;
		$liMinutes = 0;
	}

	$lsSepa = $pbStringTypeChaineHeure ? ":" : "h";
	$lvDurée = $liHeures . $lsSepa . Iif($liMinutes < 10, "0", "") . $liMinutes;
	If ($liHeures < 10 && !$pbTronqueCaractèresAuxHeures) {
		$lvDurée = "0" . $lvDurée;
	}

	If ($pbSecondes) {
		$lvDurée = $lvDurée . ":" . Iif($liSecondes < 10, "0", "") . $liSecondes;
	}
	$lvDurée = $lsSigne . $lvDurée;

//	IF ($pbReturnStringInutilisé) {
	IF (!$pbStringTypeChaineHeure) {
//			$lvDurée = Strtran($lvDurée, ":", "h") ;
		IF ($pbAffLblMin) {
			$lvDurée = $lvDurée . "mn";
		}
		If ($pbTronqueCaractèresAuxHeures && Left($lvDurée, 1) == "0") {
			If (Len(aGaucheDe($lvDurée, "h")) == 2) {
				// Si la valeur est de type 0xhxx on enlève le 1er caractère 
				$lvDurée = Right($lvDurée, Len($lvDurée) - 1);
			}
		}
	}
//	} Else {
//		If ($lsSigne !== "") {
//			lg_Die(__Function__ . " : un signe non vide a été rencontré") ;
//		}
//		$lvDurée = CTOT($lvDurée) ;
//	}

	Return $lvDurée;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// Convertir une durée de type heure/minute/secondes en nombre de type décimal (heures décimales)
// pvDurée 					: Durée ou heure de la journée à convertir
//					Formats	: T, C HH:MM:SS, C HHhMM, C HH.MinutesDécimales
//(pbAutoriseSup24hrs)		: si True, on accepte une durée supérieure à 24 heures
//(pbFormatSansSéparateur)	: dans ce cas, l'heure est passé sous forme "hhmmss" sans les séparateurs ":"
//(@piMinutes)				: pour un retour du nb de minutes
//(@piSecondes)				: pour un retour du nb de secondes
Function uHeureVersNum($pvDurée, $pbAutoriseSup24hrsINUTILISE = False, $pbFormatSansSéparateur = False
, &$piMinutes = 0, &$piSecondes = 0) {

	If (IsNull($pvDurée)) {
		Return Null;
	}

// MG début
	if (Vartype($pvDurée) == "O" And get_class($pvDurée) == "DateTime") {
		$pvDurée = $pvDurée->format('H:i:s');
	}
// MG fin

	IF (Vartype($pvDurée) == "T") {
		Return uTimeVersNum($pvDurée);
	}

//	Local liHeures, liSecondes, liMinutes, lvDurée, liNégatif

	If (Vartype($pvDurée) == "N") {
		// La valeur est déja numérique
		Return Round($pvDurée, 6);
	}

	IF (Vartype($pvDurée) == "C" && $pbFormatSansSéparateur) {
		IF (Dollar(":", $pvDurée)) {
			LG_die(__Function__ . " : format du paramètre incorrect");
		} Else {
//			Local lsHeures, lsMinutes, lsSecondes
			$lsHeures = Left($pvDurée, 2);
			$lsMinutes = SUBSTR($pvDurée, 2, 2);
			$lsSecondes = SUBSTR($pvDurée, 4, 2);
			$pvDurée = $lsHeures . ":" . $lsMinutes;
			IF ($lsSecondes !== "") {
				$pvDurée = $pvDurée . ":" . $lsSecondes;
			}
		}
	}

	While (Dollar("h", $pvDurée)) {
		$pvDurée = Strtran($pvDurée, "h", ":");
	}
	If (Dollar("¼", $pvDurée)) {
		$pvDurée = Strtran($pvDurée, "¼", ".25");
	}
	If (Dollar("½", $pvDurée)) {
		$pvDurée = Strtran($pvDurée, "½", ".5");
	}
	If (Dollar("¾", $pvDurée)) {
		$pvDurée = Strtran($pvDurée, "¾", ".75");
	}
	If (Dollar(".", $pvDurée)) {
		// Durée passée sous forme décimale
		Return Val($pvDurée);
	}

	$liNégatif = Iif(Left(Alltrim($pvDurée), 1) == "-", -1, 1);

	// Nb d'heures
	$liHeures = Val(AGaucheDe($pvDurée, ":"));
	$pvDurée = ADroiteDe($pvDurée, ":");

	// Nb de minutes
	$liMinutes = Val(AGaucheDe($pvDurée, ":"));
	$pvDurée = ADroiteDe($pvDurée, ":");
	If ($liMinutes > 60) {
		LG_die(__Function__ . " : Un nb de minutes supérieur à 60 a été rencontrée dans la fonction uHeureVersNum : ce cas n'est pas géré", 'wind Nowait');
	}

	// Nb de minutes
	$liSecondes = Val(AGaucheDe($pvDurée, ":"));
	If ($liSecondes > 60) {
		LG_die(__Function__ . " : Un nb de secondes supérieur à 60 a été rencontrée dans la fonction uHeureVersNum : ce cas n'est pas géré", 'wind Nowait');
	}

//	Local liNum
	$liNum = $liNégatif * uHeuresMinutesSecondesVersNum($liHeures, $liMinutes, $liSecondes);
	// On a fourni non vide pour effectuer le calcul
	$piMinutes = $liHeures * 60 + $liMinutes + $liSecondes / 60;
	// On a fourni non vide pour effectuer le calcul
	$piSecondes = $liHeures * 60 * 60 + $liMinutes * 60 + $liSecondes;

	Return $liNum;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// Valeur de retour		:
// LG 20150530
Function uHeuresMinutesSecondesVersNum($piHeures, $piMinutes, $piSecondes) {

	$liNum = Round($piHeures + $piMinutes * 100 / 60 / 100 + $piSecondes * 100 / 60 / 60 / 100, 6);

	If ($liNum > 23.999 && $liNum < 24.001) {
		$liNum = 24.000000;
	}

	// Valeur de retour
	Return $liNum;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Convertir une durée de type dateTime en nombre de type décimal
// pvDurée
// TF 20071217
Function uTimeVersNum($pvDurée) {

	If (Vartype($pvDurée) == "N") {
		// La valeur est déja numérique
		Return Round($pvDurée, 6);
	}

//	Return Round(Hour($pvDurée) + Minute($pvDurée) * 100/60/100 + Sec($pvDurée) * 100/60/60/100, 6)
	Return uHeuresMinutesSecondesVersNum(Hour($pvDurée), Minute($pvDurée), Sec($pvDurée));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// psChaineComplete
// psChaineCherchee
//(pbIgnoreCasse)
Function ADroiteDe($psChaineComplete, $psChaineCherchee, $pbIgnoreCasse = False) {

	// Vérification des paramètres
	IF (IsNull($psChaineComplete))
		Return "";
	IF (vartype($psChaineComplete) !== "C" || vartype($psChaineCherchee) !== "C") {
		LG_die(__Function__ . " : Paramètres invalides");
	}

	if ($psChaineCherchee == "") {
		Return Right($psChaineComplete, Len($psChaineComplete) - 1);
	} else if (At(Upper($psChaineCherchee), Upper($psChaineComplete)) == 0) {
		Return "";
	} else if ($pbIgnoreCasse) {
		$lsCh = Right($psChaineComplete, Len($psChaineComplete) - Atc(Upper($psChaineCherchee), Upper($psChaineComplete)) - (len($psChaineCherchee) - 1));
	} else {
		$lsCh = Right($psChaineComplete, Len($psChaineComplete) - At(Upper($psChaineCherchee), Upper($psChaineComplete)) - (len($psChaineCherchee) - 1));
	}

	RETURN $lsCh;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// psChaineComplete
// psChaineCherchee
Function AGaucheDe($psChaineComplete, $psChaineCherchee, $pbIgnoreCasse = False) {

	// Vérification des paramètres
	IF (IsNull($psChaineComplete))
		Return "";
	IF (vartype($psChaineComplete) !== "C" || vartype($psChaineCherchee) !== "C") {
		LG_die(__Function__ . " : Paramètres invalides");
	}

	if ($psChaineCherchee == "") {
		Return Left($psChaineComplete, 1);
	} else if (At(Upper($psChaineCherchee), Upper($psChaineComplete)) == 0) {
		RETURN $psChaineComplete;
	} else if ($pbIgnoreCasse) {
		$lsCh = Left($psChaineComplete, Atc(Upper($psChaineCherchee), Upper($psChaineComplete)) - 1);
	} else {
		$lsCh = Left($psChaineComplete, At(Upper($psChaineCherchee), Upper($psChaineComplete)) - 1);
	}

	RETURN $lsCh;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// Convertir une couleur décimale en couleur hexadécimale
Function color_DecToHex($psDec) {

	$lsHex = substr("000000" . dechex($psDec), -6);
	$lsHex = substr($lsHex, 4, 2) . substr($lsHex, 2, 2) . substr($lsHex, 0, 2);
	$lsHex = upper($lsHex);
	RETURN $lsHex;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// Convertir un tableau en objet
// Selon https://stackoverflow.com/questions/19272011/how-to-convert-an-array-into-an-object-using-stdclass
function convertToObject($array) {
	$object = new stdClass();
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			$value = convertToObject($value);
		}
		$object->$key = $value;
	}
	return $object;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// LG 20190626
function ExistsInArray(array $array, $key, $val) {

	$nbCase = count($array);
	for ($i = 0; $i != $nbCase; $i++) {
		if ($array[$i][$key] == $val) {
			return $i;
		}
	}
	return null;
}
