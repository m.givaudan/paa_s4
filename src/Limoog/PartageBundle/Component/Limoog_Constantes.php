<?php

namespace App\PaaBundle\Component\Paa;

define("eiSemaineSuivie", 0);
define("eiSemaineInexistante", -100);

class Paa_Constantes {

	const eiActiSsGpeAbsenceIntervenant = -2;
	const eiActiSsGpeAbsenceSalle = -3;
	const eiActiSsGpeAbsenceEtablissement = -4;
	const eiActiSsGpePrésence = -5;
	const eiActiSsGpeAbsenceUsager = -6;
	const eiActiSsGpeAbsenceGroupe = -7;
	const eiActiSsGpeInexistante = -8;
	const eiActiSsGpeMatiere = -9;
	const eiActiSsGpeTournée = -11;
	const eiTypeArrondiAucun = 1;
	const eiTypeArrondiHeure = 2;
	const eiTypeArrondiDemiHeure = 3;
	const eiTypeArrondiQuartHeure = 4;
	const eiTypeArrondiCinqMinutes = 5;
	const eiTypeArrondiMinute = 6;
	const eiTypeArrondiDemiJournée = 7;
	const eiTypeArrondiQuartDeJournée = 8;

}
