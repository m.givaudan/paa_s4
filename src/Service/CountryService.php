<?php
declare(strict_types=1);
 
namespace App\Service;
 
use App\Entity\Country;
use App\Exception\CountryException;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
 
class CountryService implements CountryServiceInterface
{
    public function getOneById(int $id)
    {
        $country = 'I am not a Country object';
        if (!$country instanceof Country) {
            throw new CountryException(
                sprintf('Country "%d" was not found.', $id),
                Response::HTTP_NOT_FOUND
            );
        }
 
        return $country;
    }
 
    public function updateOneById(int $id)
    {
        try {
            $this->internalException();
        } catch (RuntimeException $e) {
            throw new CountryException(
                sprintf('Country "%d" state was changed before.', $id),
                Response::HTTP_CONFLICT,
                $e
            );
        }
    }
 
    private function internalException()
    {
        throw new RuntimeException('An internal error was encountered.');
    }
}