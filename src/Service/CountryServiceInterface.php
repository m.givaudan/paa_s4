<?php
declare(strict_types=1);
 
namespace App\Service;
 
interface CountryServiceInterface
{
    public function getOneById(int $id);
 
    public function updateOneById(int $id);
}
