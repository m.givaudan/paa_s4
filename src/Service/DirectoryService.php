<?php
 
namespace App\Service;


class DirectoryService
{    
   function __construct() {
       return;
   }
   
   public function getDirContents($dir, &$results = array()){
    $files = scandir($dir);

    foreach($files as $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            $path_extension = pathinfo($path, PATHINFO_EXTENSION);
//MG Modification 20200312 Début
//Ajout des fichiers css
//            if($path_extension === 'js'){
//                $results[] = $path;
//            }
            if($path_extension === 'js' || $path_extension === 'css'){
                $results[] = $path;
            }
//MG Modification 20200312 Début
        } else if($value != "." && $value != "..") {
            $this->getDirContents($path, $results);
        }
    }

    return $results;
    }
}