<?php
// Classe décorateur du routeur de Symfony, pour lui imposer un autre générateur d'URL
// selon https://stackoverflow.com/questions/55468435/symfony-4-2-how-to-decorate-the-urlgenerator
// et https://stackoverflow.com/questions/58268697/symfony-4-i-decorated-urlgeneratorinterface-but-its-not-used-it-uses-compile
// Voir aussi \applicationPAA_S4\config\services.yaml

namespace App\Service;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;

class RouterDecorator implements RouterInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * MyRouter constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }
	
//	public function getGenerator() {
//		if (null !== $this->generator) {
//			return $this->generator;
//		}
//
//		$this->generator = new \App\PaaBundle\Component\UrlGenerator2($this->getRouteCollection(), $this->context, $this->logger, $this->defaultLocale) ;
//		
//		return $this->generator;
//	}
		
    /**
     * @inheritdoc
     */
    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $url = $this->router->generate($name, $parameters, $referenceType);

/* ATTENTION, la méthode generate n’est pas encore fiabilisée
 * , en particulier elle peut encore changer des URLs émises par Symfony pour son propre usage.
 * Il faut recenser ces URLS par l'utilisation d'une trace.
 * Ou alors s'assurer que toutes les routes de PAA soient préfixées par PAA_ et s'appuyer sur $name pour faire le test
 * 
 * ATTENTION aussi à la génération d'URLs absolues : ce cas n' est pas encore géré
 */

		// Selon https://stackoverflow.com/questions/46021326/symfony-3-3-custom-router
		$context = $this->getContext() ;
//echo $context->baseUrl ;
//echo $context->pathInfo ;
//echo $context->method ;
//echo $context->host ;
//echo $context->scheme ;
//echo $context->httpPort ;
//echo $context->httpsPort ;
//echo $context->queryString ;
//echo $context->parameters ;
		if (substr($name, 0, 1) == "_") {
			// nom système de Symfony : RAS
//		} else if ($name == "fos_user_security_logout" || $url == "/logout") {
//			// Ce nom est système pour FOS_USER_BUNDLE : ne pas le changer
		} else if (strpos($context->getPathInfo(), "/DB/") === FALSE) {
			// Cette URL ne contient pas de référence à une base de données
		} else {
			if (!(strpos($url, "www") === false) || !(strpos($url, "http") === false)) {
				// URL absolue
				throw new \Exception("RouterDecorator.generate : le cas des URLs absolues n'est pas encore géré") ;
			}
			$laPathItems = explode("/", $context->getPathInfo()) ;
			$laUrlItems = explode("/", $url) ;
			if ($laPathItems[1] === "DB" && $laUrlItems[1] !== "DB") {
				$url = "/DB/" . $laPathItems[2] . $url ;
			}
		}
		
		return $url ;
    }

    /**
     * @inheritdoc
     */
    public function setContext(RequestContext $context)
    {
        $this->router->setContext($context);
    }

    /**
     * @inheritdoc
     */
    public function getContext()
    {
        return $this->router->getContext();
    }

    /**
     * @inheritdoc
     */
    public function getRouteCollection()
    {
        return $this->router->getRouteCollection();
    }

    /**
     * @inheritdoc
     */
    public function match($pathinfo)
    {
        return $this->router->match($pathinfo);
    }
}