<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\DirectoryService;
//v1
use MatthiasMullie\Minify;
//v2
//use JShrink\Minifier;
//v3
use Patchwork\JSqueeze;


class MinifyJSCommand extends Command
{
    // the name of the command (the part after "bin/console")
//MG 20200312 Modification Début
//Minify plus uniquement le JS
//protected static $defaultName = 'app:minify-js';
	protected static $defaultName = 'app:minify';
//MG 20200312 Modification Fin
    

    protected function configure()
    {
        $this
//MG 20200312 Modification Début
//Minify plus uniquement le JS
//                ->setDescription('Minify all js script, save them in the same place as the none min');
                ->setDescription('Minify all js and css files , save them in the same place as the none min');
//MG 20200312 Modification Fin
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    { 
// v1
//        $directory = new DirectoryService();
//        $myPath = 'D:/Limoog/applicationPAA_S4/public/Limoog';
//        $content = $directory->getDirContents($myPath);
//        foreach ($content as $value) {
//            $fileContents = file_get_contents($value);
//            $minifier  = new Minify\JS($fileContents);
//            $minifiedContents = $minifier->minify();
//            $path_dirname = pathinfo($value, PATHINFO_DIRNAME);
//            $path_basename = pathinfo($value, PATHINFO_BASENAME);
//            $path_extension = pathinfo($value, PATHINFO_EXTENSION);
//            $path_basename = substr($path_basename, 0, -3);            
//            $nomFichier = $path_dirname.'\\'.$path_basename.'.min.'.$path_extension;
//            $res = file_put_contents($nomFichier, $minifiedContents);
//            $output->writeln($nomFichier);
//        }
		
//v2
//		$directory = new DirectoryService();
//        $myPath = 'D:/Limoog/applicationPAA_S4/public/Limoog';
//        $content = $directory->getDirContents($myPath);
//        foreach ($content as $value) {
//            $fileContents = file_get_contents($value);
//            $minifier  = new Minifier();
//            $minifiedContents = $minifier->minify($fileContents);
//            $path_dirname = pathinfo($value, PATHINFO_DIRNAME);
//            $old_path_basename = pathinfo($value, PATHINFO_BASENAME);
//            $path_extension = pathinfo($value, PATHINFO_EXTENSION);
//            $path_basename = substr($old_path_basename, 0, -3);
//			
//            $nomFichier = $path_dirname.'\\'.$path_basename.'.min.'.$path_extension;
//			$oldFile = $path_dirname.'\\'.$path_basename.'.'.$path_extension;
//			if(strpos($oldFile, 'min') != null){
//				unlink($oldFile);
//				file_put_contents($oldFile, $minifiedContents);
//			} else {
//				file_put_contents($nomFichier, $minifiedContents);
//			}       
//            $output->writeln($nomFichier);
//        }
//		
//v3
//		$directory = new DirectoryService();
//        $myPath = 'D:/Limoog/applicationPAA_S4/public/Limoog';
////        $myPath = 'D:\Limoog\testMinify\UnDos'; //TEST
//        $content = $directory->getDirContents($myPath);
//        foreach ($content as $value) {
//            $fileContents = file_get_contents($value);
//            $minifier  = new JSqueeze();
//            $minifiedContents = $minifier->squeeze($fileContents, true, true);
//            $path_dirname = pathinfo($value, PATHINFO_DIRNAME);
//            $old_path_basename = pathinfo($value, PATHINFO_BASENAME);
//            $path_extension = pathinfo($value, PATHINFO_EXTENSION);
//            $path_basename = substr($old_path_basename, 0, -3);
//			
//            $nomFichier = $path_dirname.'\\'.$path_basename.'.min.'.$path_extension;
//			$oldFile = $path_dirname.'\\'.$path_basename.'.'.$path_extension;
//			if(strpos($oldFile, 'min') != null){
//				unlink($oldFile);
//				file_put_contents($oldFile, $minifiedContents);
//			} else {
//				file_put_contents($nomFichier, $minifiedContents);
//			}       
//            $output->writeln($nomFichier);
//        }
		
		//v4 ajout du css
		$directory = new DirectoryService();
        $myPath = 'D:/Limoog/applicationPAA_S4/public/Limoog';
//        $myPath = 'D:\Limoog\testMinify\UnDos'; //TEST
        $content = $directory->getDirContents($myPath);
        foreach ($content as $value) {
			$path_dirname = pathinfo($value, PATHINFO_DIRNAME);
            $old_path_basename = pathinfo($value, PATHINFO_BASENAME);
            $path_extension = pathinfo($value, PATHINFO_EXTENSION);
            $path_basename = substr($old_path_basename, 0, -3);
            $fileContents = file_get_contents($value);
			if($path_extension === 'js'){
				$minifier  = new JSqueeze();
				$minifiedContents = $minifier->squeeze($fileContents, true, true);
				$nomFichier = $path_dirname.'\\'.$path_basename.'.min.'.$path_extension;
				$oldFile = $path_dirname.'\\'.$path_basename.'.'.$path_extension;
			} else if($path_extension === 'css'){
				$minifier = new Minify\CSS($fileContents);
				$minifiedContents = $minifier->minify();
				//le '.' avant le min est compris dans le basename pour les fichiers css
				$nomFichier = $path_dirname.'\\'.$path_basename.'min.'.$path_extension;
				$oldFile = $path_dirname.'\\'.$path_basename.$path_extension;
			}
			
			if(strpos($oldFile, 'min') != null){
				unlink($oldFile);
				file_put_contents($oldFile, $minifiedContents);
			} else {
				file_put_contents($nomFichier, $minifiedContents);
			}       
            $output->writeln($nomFichier);
        }
    }
}