/* 
 * LG 20180627
 * Procédures javascript pour afficher les form Symfony dans des onglets
 */
window.entityFormOnglets_Défini = true;	// Pour vérification dans les scripts qui utilisent ce qui est défini ici

function newEntityFormOnglets(psNomTable) {
    var loEntityFormOnglets = {};
    var thisObject = loEntityFormOnglets;
    loEntityFormOnglets.nomTable = psNomTable;

    // Bricolage pour détecter les autres objets de gestion de entityForm
    if (window.entityFormGrille) {
        thisObject.oEntityFormGrille = window.entityFormGrille;
        thisObject.oEntityForm = window.entityFormGrille.oEntityForm;
// LG 20200824 début
        thisObject.oEntityForm.oEntityFormOnglets = thisObject ;
// LG 20200824 fin
    } else if (window.entityForm) {
        thisObject.oEntityForm = window.entityForm;
// LG 20200824 début
        thisObject.oEntityForm.oEntityFormOnglets = thisObject ;
// LG 20200824 fin
    } else if (window.entityFormPopup) {
        thisObject.oEntityForm = window.entityFormPopup;
// LG 20200824 début
        thisObject.oEntityForm.oEntityFormOnglets = thisObject ;
// LG 20200824 fin
    } else {
        var lsMsg = "La fenêtre n'a ni entityFormGrille ni entityForm.";
        console.warn(lsMsg);
    }

    // Valider la demande de changement d'onglet actif
// LG 20200824 début
//    loEntityFormOnglets.ChangeOnglet = function (psOngletDemandé) {
//        var liIdEntité = $("#fEntityForm_" + thisObject.nomTable + "_IdentifiantEntité").text();
    loEntityFormOnglets.ChangeOnglet = function (psOngletDemandé, piIdEntité, pfCallBack) {
        var liIdEntité ;
        if (typeof piIdEntité === 'undefined') {
            liIdEntité = $("#fEntityForm_" + thisObject.nomTable + "_IdentifiantEntité").text();           
        } else {
            liIdEntité = piIdEntité ;
        }
// LG 202008024 old : pour les nouveaux, pas besoin de demander à la grille        if (thisObject.oEntityFormGrille) {
        if (thisObject.oEntityFormGrille && liIdEntité) {
            thisObject.oEntityFormGrille.DoLoadData(liIdEntité, true, psOngletDemandé);
        } else if (thisObject.oEntityForm) {
// LG 202008024 old            thisObject.oEntityForm.DoLoadData(liIdEntité, psOngletDemandé);
            thisObject.oEntityForm.DoLoadData(liIdEntité, psOngletDemandé, pfCallBack);
        } else {
            console.warn("entityForm n'est pas défini");
        }
    };

    // Annuler la demande de changement d'onglet actif
    loEntityFormOnglets.AnnuleChangeOnglet = function () {
//		var lsOngletARestaurer = $("#formulaire_OngletActif_" + thisObject.nomTable).val() ;
    };

    // Déterminer si on est positionné sur l'onglet général
    // LG 20200824
    loEntityFormOnglets.estOngletGénéral = function () {
        return $("#formulaire_OngletGénéral").hasClass("show") ;
    }

    // Initialiser la capture du clic sur les onglets
    $(".boutonOnglet").on("click", function (e) {
        var lsOngletDemandé = $(this).attr('id');
        var lsOngletActuel = $("#formulaire_OngletActif_" + thisObject.nomTable).val();
        if (lsOngletDemandé === lsOngletActuel)
            return;

        // Essayer de déselectionner l'onglet actuellement actif, avec enregistrement de l'onglet actif si il a été modifié
        var loEntityForm;
        loEntityForm = thisObject.oEntityForm;
        if (loEntityForm) {
            loEntityForm.EnregistreSiNecessaire(
                    loEntityFormOnglets.ChangeOnglet			// callbackSiSuccès
                    , lsOngletDemandé							// callbackData
                    , loEntityFormOnglets.AnnuleChangeOnglet	// callbackSiEchec
                    );
        } else {
            console.warn("entityForm n'est pas défini");
        }

        // 2 lignes pour ne pas que l'onglet qu'on quitte soit masqué
        e.preventDefault();
        return false;
    });

}

// Activer un onglet de pageframe
// Inspiré de https://stackoverflow.com/questions/17830590/bootstrap-tab-activation-with-jquery  
function SelectTabAvecId(pvTabId) {
    $('.nav-tabs li ').removeClass('active');
    $('.nav-tabs li ').removeClass('show'); // LG 20200728 bootstrap 4
    if (typeof pvTabId === "number") {
        // On a fourni le N° du tab à activer
        $('.nav-tabs li').eq(pvTabId).addClass('active'); 
        $('.nav-tabs li').eq(pvTabId).addClass('show');         // LG 20200728 bootstrap 4
    } else if ($('.nav-tabs li > #' + pvTabId)) {
        // On a fourni l'id d'un onglet
        $('.nav-tabs li > #' + pvTabId).parent().addClass('active');
        $('.nav-tabs li > #' + pvTabId).parent().addClass('show'); // LG 20200728 bootstrap 4
    } else {
        // Erreur
        console.warn("SelectTabAvecId : pvTabId n'a pas un type acceptable : " + typeof pvTabId);
    }
    private_ShowInitialTabContent();
    return;

    function private_FindActiveDiv() {
        var DivName = $('.nav-tabs .active a').attr('href');
        return DivName;
    }
    function private_RemoveFocusNonActive() {
        $('.nav-tabs  a').not('.active').blur();
        //to >  remove  :hover :focus;
    }
    function private_ShowInitialTabContent() {
        private_RemoveFocusNonActive();
        var DivName = private_FindActiveDiv();
        if (DivName)
        {
            $(DivName).parent().children().removeClass('active in');
            $(DivName).addClass('active in');
        }
    }
}

