// Scripts spécifiques à la gestion des pages de formulaire avec treeView

// entityForm.js est censé être connu
if (!window.entityForm_Défini) {
    console.warn("entityForm.js n'a pas été exécuté !");
}

// Créer un objet entityFormTreeview
function newEntityFormTreeview(psNomTable, psLibelléTable) {
    var loEntityFormTreeview = {};
    var thisObject = loEntityFormTreeview;
    loEntityFormTreeview.nomClasse = "entityFormGrille";
    loEntityFormTreeview.nomTable = psNomTable;
    loEntityFormTreeview.libelléTable = psLibelléTable;
    loEntityFormTreeview.oEntityForm = newEntityForm(loEntityFormTreeview, psNomTable, psLibelléTable);
    loEntityFormTreeview.oEntityFormBoutons = entityFormBoutons(loEntityFormTreeview, psNomTable);
    loEntityFormTreeview.oEntityForm.oEntityFormBoutons = loEntityFormTreeview.oEntityFormBoutons;

    loEntityFormTreeview.onInitComplete = function (poLGDataTable) {
// LG 20200812 déac        // Placer les boutons
// LG 20200812 déac        thisObject.PlaceBoutons();

        // Configurer la poignée de redimensionnement entre la DataTable et le form
        //LGTreeview_initGrilleResize("entityFormGrille_DataTable");
    }

    // Fonction callback de sélection d'une nouvelle ligne, en cas d'échec ou d'annulation
    loEntityFormTreeview.CancelChangeSelectedNode = function () {
        // Marquer l'action de sélection comme terminée
        thisObject.oTreeview.bIsSelectingNode = false;
    }

    // Fonction callback de sélection d'une nouvelle ligne
    loEntityFormTreeview.ChangeSelectedNode = function (paData) {
        // Sélectionner la ligne demandée
        var loNodeASelectionner = paData[0];
        thisObject.oSelectedNode = loNodeASelectionner;
        var lsIdASelectionner = loNodeASelectionner.cid;
        // Charger l'enregistrement
// LG 20200811 début
//        if (lsIdASelectionner) {
//            thisObject.DoLoadData(lsIdASelectionner, true);
//        }
        if (!lsIdASelectionner) {
            // Id à sélectionner vide : RAS
            thisObject.CancelChangeSelectedNode();
        } else if (lsIdASelectionner === loNodeASelectionner.ctype) {
            // Id à sélectionner ne contient que le type, mais pas d'Id numérique : RAS
            thisObject.CancelChangeSelectedNode();
        } else {
            // Charger l'enregistrement demandé
            thisObject.DoLoadData(lsIdASelectionner, true);
        }
// LG 20200811 fin
    };

    // Lancer le chargement de l'enregistrement demandé
    // psId					: Identifiant de l'entité à charger
    // (pbMêmeSiEnAttente)	: fournir true pour charger même si la fenêtre est déja en attente
    // (psOnglet)			: nom de l'onglet éventuel à charger
    // (pbPourNouveau)      : true si le chargement est celui d'une nouvelle entité, vierge
    // (poDataPourNouveau)  : pour fournir les données à charger en remplacement des valeurs par défaut dans le cas d'une nouvelle entité, vierge
    // Valeur de retour		: true si succès
    loEntityFormTreeview.DoLoadData = function (psId, pbMêmeSiEnAttente, psOnglet, pbPourNouveau, poDataPourNouveau) {
        if (!pbMêmeSiEnAttente && thisObject.EstEnAttente())
            return false;
        if (!psId) {
            console.warn("L'Id à charger est vide");
            thisObject.LoadCancelled();
            return false;
        }

// LG 20200813 début
        if (pbPourNouveau) {
            thisObject.bLoadDataPourNouveau = true;
        } else {
            thisObject.bLoadDataPourNouveau = false;
        }
        thisObject.oDataPourNouveau = poDataPourNouveau;
// LG 20200813 fin

        // Patientez
        thisObject.Patientez("Chargement en cours");

        // Le chargement est sous la responsabilité de thisObject.oEntityForm
        thisObject.oEntityForm.idDivEntityForm = 'formulaire_DataTable_' + thisObject.nomTable;
        return thisObject.oEntityForm.DoLoadData(psId, psOnglet, thisObject.LoadComplete, thisObject.LoadCancelled);
    };

    // En cas d'abandon de la demande de chargement
    loEntityFormTreeview.LoadCancelled = function () {
        thisObject.PatientezFin();
        // Marquer l'action de sélection comme terminée
        thisObject.oTreeview.bIsSelectingNode = false;
    };

    // Fonction appellée par JQUery quand loDivFormulaire.load se termine
    loEntityFormTreeview.LoadComplete = function (responseText, textStatus, req) {
        if (textStatus === "error") {
            // Erreur de chargement
            // Afficher le texte de la réponse
            var loDivFormulaire = $('#' + thisObject.oEntityForm.idDivEntityForm);
            loDivFormulaire.html(responseText);
            thisObject.PatientezFin();
            // Marquer l'action de sélection comme terminée
            thisObject.oTreeview.bIsSelectingNode = false;
            return;
        }

        // Initialiser les événements de formulaire
        thisObject.oEntityForm.FormReady();

        // Mettre en cache les valeurs qui viennent d'être chargées
        thisObject.oEntityForm.MetEnCacheValeursChamps();	// Enregistrer les valeurs des champs

        // La search bar et les boutons nouveau et dupliquer sont placés dans le formulaire
        var lsNomEntité;
        lsNomEntité = thisObject.oEntityForm.RtvNomEntité();
        thisObject.oEntityFormBoutons.PlaceBoutonEnregistre(lsNomEntité);
        thisObject.oEntityFormBoutons.AfficheBoutons(true);

// LG 20200813 old        thisObject.oEntityForm.SetChangementsEnAttente(false);
        thisObject.oEntityForm.SetChangementsEnAttente(thisObject.bLoadDataPourNouveau);
        thisObject.oEntityFormBoutons.ResetNouveau();
// LG 20200813 début
        if (thisObject.bLoadDataPourNouveau) {
            thisObject.oEntityForm.RemplitChampsPourNouveauOuDupicata(false, thisObject.oDataPourNouveau);
        }
//        $("#fEntityForm_URLTable").text($("#NomEntité").text()) ;     // Redéfinir l'URL de table pour que l'enregistrement se fasse correctement
// LG 20200813 fin
        thisObject.PatientezFin();

        // Marquer l'action de sélection comme terminée
        thisObject.oTreeview.bIsSelectingNode = false;

    };

// LG 20200812 début
//    loEntityFormTreeview.PlaceBoutons = function () {
//        // on vérifie si les boutons ne sont pas déjà présents
//        if ($('#ctnCmdNouvelleActiviteBase_actiBaseSousGroupeGroupe').length === 0) {
//            //bouton nouveau groupe
//            //id = ctnCmdNouveauGroupe_actiBaseSousGroupeGroupe
//            var NouveauGroupe = "<div class=\"form-group grp-bt-spe\" id=\"ctnCmdNouveauGroupe_actiBaseSousGroupeGroupe\">\
//									<button onclick=\"addElement('GAB')\" id=\"bNouveauGroupe_actiBaseSousGroupeGroupe\" class = \"btn-block btn\" style=\"white-space: inherit ! important;\">\
//										Nouveau groupe\
//										<span class=\"pull-right\">\
//											<span class=\"glyphicon glyphicon-file\">\
//											</span>\
//										</span>\
//									</button>\
//								</div>";
//            //bouton nouveau sous-groupe
//            //id = ctnCmdNouveauSousGroupe_actiBaseSousGroupeGroupe
//            var NouveauSousGroupe = "<div class=\"form-group grp-btn-spe\" id=\"ctnCmdNouveauSousGroupe_actiBaseSousGroupeGroupe\">\
//										<button onclick=\"addElement('SGAB')\" id=\"bNouveauSousGroupe_actiBaseSousGroupeGroupe\" class = \"btn-block btn\" style=\"white-space: inherit ! important;\">\
//											Nouveau sous-groupe\
//											<span class=\"pull-right\">\
//												<span class=\"glyphicon glyphicon-file\">\
//												</span>\
//											</span>\
//										</button>\
//									</div>";
//            //bouton nouvelle activité de base
//            //id = ctnCmdNouvelleActiviteBase_actiBaseSousGroupeGroupe
//            var NouvelleActiviteBase = "<div class=\"form-group grp-btn-spe\" id=\"ctnCmdNouvelleActiviteBase_actiBaseSousGroupeGroupe\">\
//											<button onclick=\"addElement('AB')\" id=\"bNouvelleActiviteBase_actiBaseSousGroupeGroupe\" class = \"btn-block btn\" style=\"white-space: inherit ! important;\">\
//												Nouvelle activité de base\
//												<span class=\"pull-right\">\
//													<span class=\"glyphicon glyphicon-file\">\
//													</span>\
//												</span>\
//											</button>\
//										</div>";
//// LG 20200812 old            $('#ctnBoutons_actiBaseSousGroupeGroupe').prepend(NouveauGroupe, NouveauSousGroupe, NouvelleActiviteBase);
//            $('#ctnBoutons_Boutons').prepend(NouveauGroupe, NouveauSousGroupe, NouvelleActiviteBase);
//            $('#ctnCmdNouveau_actiBaseSousGroupeGroupe').hide();
//
//// LG 20200812 début
//    // Gestionnaire du clic sur nouveau groupe
//    $('#bNouveauGroupe_actiBaseSousGroupeGroupe').click(thisObject.addElement('GAB'));
//
//    // Gestionnaire du clic sur nouveau sous-groupe
//    $('#bNouveauSousGroupe_actiBaseSousGroupeGroupe').click(thisObject.addElement('SGAB'));
//
//    // Gestionnaire du clic sur nouvelle activité de base
//    $('#bNouvelleActiviteBase_actiBaseSousGroupeGroupe').click(thisObject.addElement('AB'));
//
//// LG 20200812 fin
//        }
//    }
// LG 20200812 fin

    // Met l'environnement en stand-by et affiche un message d'attente
    loEntityFormTreeview.Patientez = function (psMsgText, poEltParent) {
        patientez(psMsgText, poEltParent);
        thisObject.bPatientezEnCours = true;
    }

    // Termine le stand-by et masque le message d'attente
    loEntityFormTreeview.PatientezFin = function () {
        thisObject.bPatientezEnCours = false;
        patientezFin();
    }

    loEntityFormTreeview.EstEnAttente = function () {
        var lbEnCours = thisObject.bPatientezEnCours ? true : false;
        return lbEnCours;
    }

    loEntityFormTreeview.BeforeFormSubmit = function (idDuFormulaire) {
        if (thisObject.oTreeview) {
            // Sauvegarder les données du formulaire pour remise à jour de la ligne active une fois l'enregistrement terminé
            thisObject.oTreeview.MetEnCacheValeursColonnes(idDuFormulaire);
        }
    }

    return loEntityFormTreeview;
}
