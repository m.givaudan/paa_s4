// Scripts spécifiques à la gestion des pages de formulaire avec grille

// entityForm.js est censé être connu
if (!window.entityForm_Défini) {
    console.warn("entityForm.js n'a pas été exécuté !");
}

// window.entityFormGrille_oDataTable = null ;
// window.dataTable_oSelectedRow = null ;
// window.dataTable_oSelectedRowDiv = null ;
// window.dataTable_oSelectedRow_AvantNouveau = null ;
// window.dataTable_aColonnes = null ;

// Créer un objet entityForm
function newEntityFormGrille(psNomTable, psLibelléTable/*LG 20190307 deac, psSavePath*/) {
    var loEntityFormGrille = {};
    var thisObject = loEntityFormGrille;
    loEntityFormGrille.nomClasse = "entityFormGrille";
    loEntityFormGrille.nomTable = psNomTable;
    loEntityFormGrille.libelléTable = psLibelléTable;
    loEntityFormGrille.oEntityForm = newEntityForm(loEntityFormGrille, psNomTable, psLibelléTable);
    loEntityFormGrille.oEntityFormBoutons = entityFormBoutons(loEntityFormGrille, psNomTable);
    loEntityFormGrille.oEntityForm.oEntityFormBoutons = loEntityFormGrille.oEntityFormBoutons;

    loEntityFormGrille.onLGDataTableInitComplete = function (poLGDataTable) {
        // Placer les boutons
        thisObject.PlaceBoutons();

        // Configurer la poignée de redimensionnement entre la DataTable et le form
        LGDataTable_initGrilleResize("entityFormGrille_DataTable");
    }

    // Fonction callback de sélection d'une nouvelle ligne, en cas d'échec ou d'annulation
    loEntityFormGrille.CancelChangeSelectedRow = function () {
        // Marquer l'action de sélection comme terminée
        thisObject.oDataTable.bIsSelectingRow = false;
    }

    // Fonction callback de sélection d'une nouvelle ligne
    // paData : Array(poRow, liId)
    // paData[0] = poRow	: la ligne à sélectionner
    loEntityFormGrille.ChangeSelectedRow = function (paData) {
        // Déselectionner la ligne courante
// LG 20190408 début
//		var loDivRowActuellementSelectionnée;
//		if (thisObject.oDataTable) {
//			loDivRowActuellementSelectionnée = thisObject.oDataTable.RtvSelectedRowDiv();
//		}
//		if (loDivRowActuellementSelectionnée) {
//			loDivRowActuellementSelectionnée.removeClass('selected');
//		}
//
//		// Sélectionner la ligne demandée
//		var loRowASelectionner = paData[0];
//		// LG 20190220 deac	var loDivRowASelectionner = $(loRowASelectionner.node());
//		thisObject.oDataTable.oSelectedRow = loRowASelectionner;
//		// LG 20190220 deac	window.entityFormGrille_oDataTable.oSelectedRowDiv() = loDivRowASelectionner;
//		// LG 20190220 old	loDivRowASelectionner.addClass('selected');
//		thisObject.oDataTable.oSelectedRowDiv().addClass('selected');
        var loRowASelectionner = thisObject.oDataTable.ChangeSelectedRow(paData);
// LG 20190408 fin

        // Charger l'enregistrement
        if (loRowASelectionner.data() && loRowASelectionner.data().length > 0) {
            var liIdASelectionner = loRowASelectionner.data()[0];
            thisObject.DoLoadData(liIdASelectionner, true);
        }
    };

    // Renvoie l'Id de l'entité qui est affichée dans la ligne sélectionnée de la dateTable
    loEntityFormGrille.RtvIdSelectedRow = function () {
        return thisObject.oDataTable.RtvIdSelectedRow();
    }

    // Lancer le chargement de l'enregistrement demandé
    // piId					: Identifiant de l'entité à charger
    // (pbMêmeSiEnAttente)	: fournir true pour charger même si la fenêtre est déja en attente
    // (psOnglet)			: nom de l'onglet éventuel à charger
    // Valeur de retour		: true si succès
    loEntityFormGrille.DoLoadData = function (piId, pbMêmeSiEnAttente, psOnglet) {
        if (!pbMêmeSiEnAttente && thisObject.EstEnAttente())
            return false;
        if (!piId) {
            console.warn("L'Id à charger est vide");
            thisObject.LoadCancelled();
            return false;
        }

        // Patientez
        thisObject.Patientez("Chargement en cours");

        // Le chargement est sous la responsabilité de thisObject.oEntityForm
        thisObject.oEntityForm.idDivEntityForm = 'formulaire_DataTable_' + thisObject.nomTable;
        return thisObject.oEntityForm.DoLoadData(piId, psOnglet, thisObject.LoadComplete, thisObject.LoadCancelled);
    };

    // En cas d'abandon de la demande de chargement
    loEntityFormGrille.LoadCancelled = function () {
        thisObject.PatientezFin();
        // Marquer l'action de sélection comme terminée
        thisObject.oDataTable.bIsSelectingRow = false;
    };

    // Fonction appellée par JQUery quand loDivFormulaire.load se termine
    loEntityFormGrille.LoadComplete = function (responseText, textStatus, req) {
        if (textStatus === "error") {
            // Erreur de chargement
            // Afficher le texte de la réponse
//			var lsNomTable = $('#NomTable').text();
//			var loDivFormulaire = $('#formulaire_DataTable_' + lsNomTable);
            var loDivFormulaire = $('#formulaire_DataTable_' + thisObject.nomTable);
            /*						// en ne gardant que le body, selon https://stackoverflow.com/questions/3628374/how-to-extract-body-contents-using-regexp
             // Pas opérationnel : la réponse est illisible faute des styles Symfony
             var strVal = responseText ;
             var pattern = /<body[^>]*>((.|[\n\r])*)<\/body>/im
             var array_matches = pattern.exec(strVal);
             loDivFormulaire.html(array_matches[1]) ;
             */
            loDivFormulaire.html(responseText);
            thisObject.PatientezFin();
            // Marquer l'action de sélection comme terminée
            thisObject.oDataTable.bIsSelectingRow = false;
            return;
        }

        // Marquer la sélection sur le dataTable
        thisObject.oDataTable.oSelectedRowDiv().addClass('selected');

        // Mettre en cache les valeurs qui viennent d'être chargées
        //	if (entityForm_MetEnCacheValeursChamps) entityForm_MetEnCacheValeursChamps();	// Enregistrer les valeurs des champs
        thisObject.oEntityForm.MetEnCacheValeursChamps();	// Enregistrer les valeurs des champs

        // La search bar et les boutons nouveau et dupliquer sont placés dans le formulaire
        thisObject.oEntityFormBoutons.PlaceBoutonEnregistre();
        thisObject.oEntityFormBoutons.AfficheBoutons(true);

        var liIdLigne = thisObject.oEntityForm.GetIdentifiantEntité();
        thisObject.oEntityForm.SetChangementsEnAttente(false);
        thisObject.oEntityFormBoutons.ResetNouveau();
        thisObject.PatientezFin();

        // Initialiser les événements de formulaire
//LG 20190702 old		thisObject.oEntityForm.DocumentReady();
        thisObject.oEntityForm.FormReady();

        // Marquer l'action de sélection comme terminée
        thisObject.oDataTable.bIsSelectingRow = false;

    };

    loEntityFormGrille.PlaceBoutons = function () {

        var lsIdDataTable = thisObject.oDataTable.sIdDataTable;

        if (document.getElementById(lsIdDataTable + 'Infos')) {
            var loElt = document.getElementById(lsIdDataTable + '_info');
            document.getElementById(lsIdDataTable + 'Infos').appendChild(loElt);
        }

        // Rattachement de la zone de texte de "Filtrer"
        if (document.getElementById(lsIdDataTable + '_filter')) {
            // Il y a une dataGrid : déplacer la zone de recherche dans la zone des boutons
            $('#' + lsIdDataTable + '_info').css('display', 'initial');
            $('#' + lsIdDataTable + '_info').css('visibility', 'visible');
            $('#' + lsIdDataTable + '_filter > *').css('visibility', 'visible');
            $('#' + lsIdDataTable + '_filter > * > *').css("width", "100%");
            $('#' + lsIdDataTable + '_filter > * > *').addClass("form-control");
            $('#' + lsIdDataTable + '_filter > *').addClass("input-group");
            $('#' + lsIdDataTable + '_filter > *').css("margin-bottom", "0");		//J'ajoute un margin bottom nul car bootstrap en met un de 5px, ce qui décale le reste
            $('#' + lsIdDataTable + '_filter > *').children().each(function () {
                var $cmd = $(this);
                if ($cmd.hasClass("input-group-addon"))
                    $cmd.remove();
            });
            var lsAppend = "<div class=\"input-group-addon\"><i class=\"glyphicon glyphicon-filter\"></i></div>";
            $('#' + lsIdDataTable + '_filter > *').append(lsAppend);
// LG 20190718 début
//			if($('#ctnCmdSearch_' + thisObject.nomTable).find("#" + lsIdDataTable + '_filter')) {
            // Si l'objet est déja dans le groupe : l'enlever avant de le réinsérer
            $('#ctnCmdSearch_' + thisObject.nomTable).find("#" + lsIdDataTable + '_filter').remove();
//			}
// LG 20190718 fin
            document.getElementById('ctnCmdSearch_' + thisObject.nomTable).appendChild(document.getElementById(lsIdDataTable + '_filter'));
        } else {
            // Il n'y a pas de dataGrid : masquer la zone de recherche
            $('#ctnCmdSearch').css('display', 'none');
            $('#ctnCmdSearch_' + lsIdDataTable).css('display', 'none');
        }
    }

    // Met l'environnement en stand-by et affiche un message d'attente
    loEntityFormGrille.Patientez = function (psMsgText, poEltParent) {
        patientez(psMsgText, poEltParent);
        thisObject.bPatientezEnCours = true;
    }

    // Termine le stand-by et masque le message d'attente
    loEntityFormGrille.PatientezFin = function () {
        thisObject.bPatientezEnCours = false;
        patientezFin();
    }

    loEntityFormGrille.EstEnAttente = function () {
        var lbEnCours = thisObject.bPatientezEnCours ? true : false;
        return lbEnCours;
    }

    loEntityFormGrille.BeforeFormSubmit = function (idDuFormulaire) {
        if (thisObject.oDataTable) {
            // Sauvegarder les données du formulaire pour remise à jour de la ligne active une fois l'enregistrement terminé
            thisObject.oDataTable.MetEnCacheValeursColonnes(idDuFormulaire);
        }
    }

    return loEntityFormGrille;
}
