/* 
 * LG 20180627
 * Procédures javascript pour les form Symfony
 */
window.entityForm_Défini = true;	// Pour vérification dans les scripts qui utilisent ce qui est défini ici

// Créer un objet entityForm
// poEntityFormGrille : l'objet dans la propriété entityForm duquel on va stocker l'objet généré
//			et qui sera le parent du entityForm généré (accessible par la propriété "oEntityFormGrille")
function newEntityForm(poEntityFormGrille, psNomTable, psLibelléTable) {
    var loEntityForm = {};
    var thisObject = loEntityForm;
    loEntityForm.nomClasse = "entityForm";
    loEntityForm.nomTable = psNomTable;
    loEntityForm.libelléTable = psLibelléTable;
    loEntityForm.oEntityFormGrille = poEntityFormGrille;

//	if (!window.entityForm_aValeursChamps) window.entityForm_aValeursChamps = new Array();
    loEntityForm.IdNouvelleEntité = 0;
    loEntityForm.aValeursChamps = new Array();

    // Déclencher une action du formulaire, uniquement si les données sont enregistrées (ou après confirmation de l'utilisateur)
    // callbackSiSuccès		: la fonction action à appeller si tout est OK
    // callbackData			: le tableau contenant les données à fournir à la fonction callback
    // callbackSiEchec		: la fonction action à appeller en cas d'annulation
    loEntityForm.EnregistreSiNecessaire = function (callbackSiSuccès, callbackData, callbackSiEchec) {
        if (!thisObject.RtvChangementsEnAttente()) {
            // Pas de changements en attente : appeller directement le callback
            callbackSiSuccès(callbackData);
            return;
        }

        // Le formulaire comporte des modifications non engeristrées
        var dialog = $('<p>Voulez-vous enregistrer les modifications&nbsp;?</p>').dialog({
            // Selon http://api.jqueryui.com/dialog/
// MG Modif 20200915 Début
// Le background du modal est disabled
			modal:true,
// MG Modif 20200915 Fin
            title: "Les données ont été modifiées",
            resizeable: false,
            closeText: "Annuler",
            buttons: {
                "Oui": function () {
                    dialog.dialog('close');
                    // Sauvegarder puis appeller la fonction callback si succès
                    thisObject.submit(callbackSiSuccès, callbackData);
                },
                "Non": function () {
                    dialog.dialog('close');
                    // Appeller la fonction callback sans sauvegarder
                    if (callbackSiSuccès)
                        callbackSiSuccès(callbackData);
                },
                "Annuler": function () {
                    dialog.dialog('close');
                    if (callbackSiEchec)
                        callbackSiEchec();
                }
            }

        });
    }

    // Fonction callback pour la création d'une nouvelle entité dans le cas où on était pas sur l'onglet général
    // LG 20200824
    loEntityForm.DoNouveauOuDupliqueCallbackQuandPasOngletGénéral = function (responseText, textStatus, jqXHR) {
        var lbDuplique = window.bDuplique ;
        delete window.bDuplique ;
        if (jqXHR.status === 200) {
            // Succès
            loEntityForm.DoNouveauOuDuplique(Array(lbDuplique, true)) ;
        }
    }

    loEntityForm.DoNouveauOuDuplique = function (pbDuplique) {
        if (thisObject.oEntityFormGrille.EstEnAttente())
            return false;
        var lbSansChekChangementsEnAttente = false;
    
        if (pbDuplique instanceof Array) {
            lbSansChekChangementsEnAttente = pbDuplique[1];
            pbDuplique = pbDuplique[0];
        }
        if (!lbSansChekChangementsEnAttente && thisObject.RtvChangementsEnAttente()) {
            //		alert("Enregistrez les modifications au préalable") ;
            thisObject.EnregistreSiNecessaire(
                    thisObject.DoNouveauOuDuplique
                    , Array(pbDuplique, true));
            return;
        }

// LG 20200824 déac début
//        if (pbDuplique instanceof Array) {
//            pbDuplique = pbDuplique[0];
//        }
// LG 20200824 déac fin

// LG 20200824 début
        if (thisObject.oEntityFormOnglets && !thisObject.oEntityFormOnglets.estOngletGénéral()) {
            // On n'est pas sur l'onglet général : il faut charger une entité vide sur l'onglet général, puis mener les actions habutuelles pour un nouveau
            window.bDuplique = pbDuplique ;
            thisObject.oEntityFormOnglets.ChangeOnglet("OngletGénéral", 0, loEntityForm.DoNouveauOuDupliqueCallbackQuandPasOngletGénéral) ;
            return ;
        }
// LG 20200824 fin

        if (pbDuplique) {
            // Mettre en cache les données actuelles
//			if (!entityForm_MetEnCacheValeursChamps) {
//				console.warn("entityForm_MetEnCacheValeursChamps n'existe pas") ;
//				alert("Désolé, l'action n'a pas été possible") ;
//				return false ;
//			}
            thisObject.MetEnCacheValeursChamps();
        }

        // Demande de chargement du nouveau formulaire avec copie des données en cache
        thisObject.RemplitChampsPourNouveauOuDupicata(pbDuplique);

        if (thisObject.oEntityFormGrille.oDataTable) {
            // Contexte de form avec dataTable
            // Ajouter la nouvelle ligne dans le dataTable
            thisObject.oEntityFormGrille.oDataTable.DoAjouteNouvelleLigne(pbDuplique);
        }

        thisObject.SetIdentifiantEntité(thisObject.IdNouvelleEntité);

        // Initialiser les événements de formulaire
        thisObject.SetChangementsEnAttente(true);
        thisObject.oEntityFormBoutons.ResetNouveau();
        thisObject.FormReady();
		
//MG Ajout 20200810 Début
		//Le champ est required a true sur le form Symfony mais pas pris en compte
		$('#fEntityForm_activites_iactibase').attr('required', 'required');
//Le multiselect gardait les ressources de la séance sur laquelle on a fait "nouveau"
//Mais garder cette fonctionnalité pour la dupliquation
		if(!pbDuplique && $('#cbcRessourcesActi').length > 0){
			viderRessources();
			ajax_FillcbcRessources($("#cbcRessourcesActi")[0], "", {'noActi':true,
                'actibase': $('#fEntityForm_activites_iactibase').val()
			});
		}
//MG Ajout 20200810 Fin
		
    };

    // Lancer la sauvegarde de l'enregistrement demandé
    // callback			: la fonction action à appeller
    // callbackData		: le tableau contenant les données à fournir à la fonction callback
    loEntityForm.submit = function (callback, callbackData) {
        if (callback)
            thisObject.onSave_Callback = callback;
        if (callbackData)
            thisObject.onSave_CallbackData = callbackData;
        // Le submite sera traité par "private_DoSave"
        thisObject.RtvDivFormulaire().submit();
        return true;
    };

    loEntityForm.Patientez = function (psTexte, poEltParent) {
        if (thisObject.oEntityFormGrille)
            thisObject.oEntityFormGrille.Patientez(psTexte, poEltParent);
    };
    loEntityForm.PatientezFin = function () {
        if (thisObject.oEntityFormGrille)
            return thisObject.oEntityFormGrille.PatientezFin();
    };

    // Effacer l'entité courante
    loEntityForm.DoDelete = function (event, pbMuet) {
        if (thisObject.oEntityFormGrille.EstEnAttente())
            return false;

        if (!pbMuet) {
            // On doit demander confirmation
            var dialog = $('<p>Confirmez-vous la suppression de cet élément ?</p>').dialog({
                // Selon http://api.jqueryui.com/dialog/
// MG Modif 20200915 Début
// Le background du modal est disabled
				modal:true,
// MG Modif 20200915 Fin
                title: "Supprimer",
                resizeable: false,
                closeText: "Annuler",
                buttons: {
                    "Ok": function () {
                        dialog.dialog('close');
                        // Sauvegarder puis appeller la fonction callback si succès
                        thisObject.DoDelete(event, true);
                    },
                    "Annuler": function () {
                        dialog.dialog('close');
                    }
                }
            });
            return;
        }

        thisObject.oEntityFormGrille.Patientez("Suppression en cours");
        var lsURL = getBaseURL() + 'delete/' + thisObject.nomTable + "/" + thisObject.GetIdentifiantEntité();
        $.post(
                lsURL,
                $(this).serialize(),
                function (data) {
                    $("#formulaire").html(data);
                })
                .done(function (msg) {
                    var liId;
                    if (thisObject.oEntityFormGrille.oDataTable) {
                        // On est dans une page avec grille
                        liId = thisObject.oEntityFormGrille.oDataTable.DoDeleteRow();
                    } 
// LG 20200811 début
                    else if (thisObject.oEntityFormGrille.oTreeview) {
                        var loNode = thisObject.oEntityFormGrille.oTreeview.DoDeleteNode();
                        liId = loNode.cid ;
                    }
// LG 20200811 fin
                    else {
                        throw new exception("Cas non prévu : delete hors contexte d'une grille");
                    }
                    thisObject.oEntityFormGrille.DoLoadData(liId, true);
                })
                .fail(function (xhr, status, error) {
                    thisObject.MontrerFormulaire_error(xhr.responseText, thisObject.nomTable);
                })
                .always(function () {
                    thisObject.oEntityFormGrille.PatientezFin();
                });
        return false;
    }

    loEntityForm.MontrerFormulaire_error = function (psMsgErr, psNomTable) {
//MG Modification 20200824 Début
//MG Ajout 20200915 Début
		var width, height;
		if(psNomTable === 'salles'){
			psMsgErr = 'Cet équipement est utilisé par un groupe, suppression impossible';
			var width = 800;
			var height = 200;
		} else {
			var width = 800;
			var height = 800;
		}
//MG Ajout 20200915 Fin
		var lsHTMLDialogue = "<div id='msgErreur' title='Erreur formulaire'>"
												+ "<p>"+psMsgErr+"</p>"
												+ "</div>";
		var dialog = $(lsHTMLDialogue).dialog({
// MG Modif 20200915 Début
// Le background du modal est disabled
			modal:true,
// MG Modif 20200915 Fin 
			width: width, 
			height: height, 
			close: function (e, o) {
			   $('#msgErreur').dialog('close');
			},
			buttons: {
				"OK": function(event, ui) {
					dialog.dialog('destroy');
					}   
		}});
		$("#fFormulaireError").html(lsHTMLDialogue).css({
//            "display": "block",
//            "border-style": "solid",
//            "border-color": "red"
        });
//        $('#bCacherFormulairError').css('display', 'block');
//        $("#fFormulaireError").html(psMsgErr).css({
//            "display": "block",
//            "border-style": "solid",
//            "border-color": "red"
//        });
//MG Modification 20200824 Fin
    }

    loEntityForm.CacherFormulaireError = function () {
        $('#fFormulaireError').css('display', 'none');
        $('#bCacherFormulairError').css('display', 'none');
    }

    // Renvoyer l'Id de la ligne actuellement éditée
    loEntityForm.RtvDivIdentifiantEntité = function () {
// LG 20190702 old		var lsId = "fEntityForm_" + thisObject.RtvNomTable() + "_IdentifiantEntité" ;
        var lsId = thisObject.RtvNomEntityForm() + "_IdentifiantEntité";
        var loDiv = $("#" + lsId);
        return loDiv;
    }
    loEntityForm.GetIdentifiantEntité = function () {
        var liIdLigne = thisObject.RtvDivIdentifiantEntité().text();
// LG 20200813 début
//        liIdLigne = parseInt(liIdLigne, 10);
        if (!liIdLigne) {
            liIdLigne = 0 ;
        } else {
            liIdLigne = parseInt(liIdLigne, 10);
        }
// LG 20200813 fin
        
        return liIdLigne;
    }
    loEntityForm.SetIdentifiantEntité = function (piIdEntité) {
// LG 20190702 old		$("#fEntityForm_" + thisObject.RtvNomTable() + "_IdentifiantEntité").text(piIdEntité) ;
        thisObject.RtvDivIdentifiantEntité().text(piIdEntité);
    }

    //AV 02/04/2019 début
    //ajout d'une fonction permettant de récupérer le type d'activitée pour le treeview
    loEntityForm.getCodeTable = function () {
        if ($("#CodeTable")) {
// LG 20200720 début : pour le cas du TreeView des activités de base
//            var codeTable = parseInt($("#CodeTable").text());
//            if (isNaN(codeTable))
//                codeTable = 0;
            var codeTable = $("#CodeTable").text();
// LG 20200720 fin
            return codeTable;
        } else {
            return "";
        }
    };
    //AV 02/04/2019 Fin

    // Renvoyer le nom réel de la table actuellement traitée
    // dans certains cas RtvNomTable renvoie un nom générique (e.g. treeView des activités de base)
    loEntityForm.RtvNomEntité = function () {
        if ($("#NomEntité")) {
            var nomEntité = $("#NomEntité").text();
            return nomEntité;
        } else
            return thisObject.nomTable;
    }

    loEntityForm.RtvNomEntityForm = function (pbLowerCase) {
        return "fEntityForm_" + thisObject.RtvNomTable();
// LG 20200915 test        return "fEntityForm_" + thisObject.RtvNomTable(true);
    }
    // Renvoyer le nom de la table actuellement traitée
    loEntityForm.RtvNomTable = function (pbLowerCase) {
        // On devrait aussi pouvoir utiliser thisObject.nomTable ;
        var lsNomTable = thisObject.nomTable;
        if (pbLowerCase) {
            lsNomTable = lsNomTable.toLowerCase();
        }
        return lsNomTable;
    }
    loEntityForm.RtvLblTable = function (pbLowerCase) {
        var lsLblTable = thisObject.libelléTable;
        if (pbLowerCase) {
            lsLblTable = lsLblTable.toLowerCase();
        }
        return lsLblTable;
    };

    loEntityForm.NomTableToNomEntité = function (psNomTable) {
        var lsNomEntité = psNomTable;
        if (lsNomEntité.slice(-1) === 's') {
            // enlever le "s" à la fin
            lsNomEntité = lsNomEntité.substring(0, lsNomEntité.length - 1);
        }
        return lsNomEntité;
    };
//
//	loEntityForm.NomTableToNomEntité = function(psNomTable){
//		var lsNomEntité = psNomTable ;
//		if (lsNomEntité.slice(-1) === 's') {
//			// enlever le "s" à la fin
//			lsNomEntité = lsNomEntité.substring(0, lsNomEntité.length - 1) ;
//		}
//		return lsNomEntité ;
//	};

    loEntityForm.RtvValeurChamp = function (psNomChamp) {
// LG 20190702 old		var lsIdWidget = "#fEntityForm_" + thisObject.RtvNomTable() + "_" + psNomChamp ;
        var lsIdWidget = "#" + thisObject.RtvNomEntityForm() + "_" + psNomChamp;
        var loWidget = $(lsIdWidget);
        // LG 20180703 début
        //	// En cas de clé étrangére dans un datatable, ce if check si le tag du formulaire est un select
        //	if (loWidget.prop("tagName") === "SELECT") return loWidget.text();
        //	else return loWidget.val();
        return loWidget.val();
        // LG 20180703 fin
    };

    // Renvoie le sélecteur CSS pour obtenir tous les éléments qui sont des champs de formulaire
    loEntityForm.getSélecteurChamps = function () {
        // Classe "form-control" : utilisée dans tous les formulaire de l'application
        // Classe "EFControl" (Entity Form Control) : utilisée seulement pour les controles de formulaires qui n'ont pas besoin d'être formatés par form-control
         var lsSelecteur = '[id^="' + thisObject.RtvNomEntityForm() + '_"][class~=form-control],[class~=EFControl]' ;
         return lsSelecteur ;
    };

    loEntityForm.MetEnCacheValeursChamps = function () {
        // Pour chaque controle du formulaire
        var i = 0;

// LG 20190702 old		$('[id^="fEntityForm_' + thisObject.RtvNomTable() + '_"][class=form-control],[class=EFControl]').each(function (){
        $(thisObject.getSélecteurChamps()).each(function () {

            //si c'est une checkbox ont stocke true ou false et non la val 
            if ($(this).is(":checkbox")) {
                if ($(this).attr('checked')) {
                    //si la checkbox est cochée
                    thisObject.aValeursChamps[i] = true;
                } else {
                    //si la checkbox n'est pas cochée
                    thisObject.aValeursChamps[i] = false;
                }
            } else {
                thisObject.aValeursChamps[i] = $(this).val();
            }
            i++;
        });
    };

    // Revenir aux données enregistrées
    loEntityForm.DoAnnuler = function () {
        if (thisObject.oEntityFormGrille) {		// LG 20191118
            if (thisObject.oEntityFormGrille.EstEnAttente())
                return false;
            thisObject.oEntityFormGrille.Patientez("Annulation en cours");
        }										// LG 20191118
        // Restaurer les valeurs des champs
        thisObject.RemplitChampsAvecCache();
        thisObject.SetChangementsEnAttente(false);

        if (thisObject.oEntityFormGrille) {		// LG 20191118
            thisObject.oEntityFormGrille.PatientezFin();
        }										// LG 20191118

        if (isNaN(thisObject.GetIdentifiantEntité())) {
            var node = $('#entityFormTreeview_Treeview').treeview('getSelected');
            $('#entityFormTreeview_Treeview').treeview('removeNode', [node]);
            console.log("");
        }
    }

    loEntityForm.AfterNouveau = function (pbDuplique) {
        console.warn("Obsolete");
        thisObject.RemplitChampsPourNouveauOuDupicata(pbDuplique);
    }

// LG 20200813 début
//    loEntityForm.RemplitChampsPourNouveauOuDupicata = function (pbDuplique) {
//        if (pbDuplique) {
//            return thisObject.RemplitChampsPourDuplicatas();
//        } else {
//            return thisObject.RemplitChampsPourNouveau();
//        }
//    }
    //(pbDuplique)  : .T. pour créer un duplicata
    //(poData)      : objet contenant les valeurs par défaut
    //              ex. poData.pcNom impose un nom à la place du nom par défaut
    loEntityForm.RemplitChampsPourNouveauOuDupicata = function (pbDuplique, poData) {
        if (pbDuplique) {
            return thisObject.RemplitChampsPourDuplicatas(poData);
        } else {
            return thisObject.RemplitChampsPourNouveau(poData);
        }
    }
// LG 20200813 fin

// LG 20200813 old    loEntityForm.RemplitChampsPourNouveau = function () {
    //(poData)      : objet contenant les valeurs par défaut
    //              ex. poData.pcNom impose un nom à la place du nom par défaut
    loEntityForm.RemplitChampsPourNouveau = function (poData) {
        // Passer tous les champs définis dans le formulaire
        var lsNomEntityForm = thisObject.RtvNomEntityForm();
// LG 20200810 old        var lsSelector = '[id^="' + lsNomEntityForm + '_"][class=form-control]';
// LG 20200811 old        var lsSelector = '[id^="' + lsNomEntityForm + '_"]';
        var lsSelector = thisObject.getSélecteurChamps() ;
        
        $(lsSelector).each(function () {
// LG 20200810 déac début
//// LG 20200810 début
//            if ($(this).hasClass("form-control")) {
//                // C'est un form-control
//// LG 20200810 fin
// LG 20200810 déac fin
                var lsVal = "";
                var lsChamp = $(this)[0].id;
                lsChamp = lsChamp.replace(lsNomEntityForm + "_", "");
// LG 20200813 début
//                lsVal = thisObject.RtvValeurPourChampDeNouvelleLigne(false, lsChamp, "");
//				$(this).val(lsVal);
                if (poData && poData[lsChamp]) {
                    // Cette propriété est fournie dans poData : l'utiliser
                    lsVal = poData[lsChamp] ;
                } else {
                    // Prendre la valeur par défaut
                    lsVal = thisObject.RtvValeurPourChampDeNouvelleLigne(false, lsChamp, "");
                }
                
                if ($(this).is(":checkbox")) {
                    if (lsVal === true) {
                        $(this).prop("checked", true);
                    } else {
                        $(this).prop("checked", false);
                    }
                } else {
                    $(this).val(lsVal);
                }
// LG 20200813 fin
                
// LG 20200810 début
            }
// LG 20200810 fin
        );

        // Mettre -1 à l'Id
// LG 20190702 old $("#fEntityForm_" + lsNomTable + "_IdentifiantEntité").text(thisObject.IdNouvelleEntité) ;
        thisObject.SetIdentifiantEntité(thisObject.IdNouvelleEntité);
    }

// LG 20200813 début
//    loEntityForm.RemplitChampsPourDuplicatas = function () {
//        return thisObject.RemplitChampsAvecCache(true);
    //(poData)      : objet contenant les valeurs par défaut
    //              ex. poData.pcNom impose un nom à la place du nom par défaut
    loEntityForm.RemplitChampsPourDuplicatas = function (poData) {
// LG 20200813 fin
        return thisObject.RemplitChampsAvecCache(true, poData);
    }

// LG 20200813 début
//    loEntityForm.RemplitChampsAvecCache = function (pbPourDuplicata) {
    //(poData)      : objet contenant les valeurs par défaut
    //              ex. poData.pcNom impose un nom à la place du nom par défaut
    loEntityForm.RemplitChampsAvecCache = function (pbPourDuplicata, poData) {
// LG 20200813 fin
        // Lors du chargement du formulaire si le form a été dupliqué on éxécute ce code
        if (thisObject.aValeursChamps.length > 0) {
            var i = 0;
            $(thisObject.getSélecteurChamps()).each(function () {
                var lsVal = thisObject.aValeursChamps[i];
                var lsChamp = $(this)[0].id;
                if (pbPourDuplicata) {
                    lsChamp = lsChamp.replace(thisObject.RtvNomEntityForm() + "_", "");
// LG 20200813 début
//                    lsVal = thisObject.RtvValeurPourChampDeNouvelleLigne(true, lsChamp, lsVal);
                    if (poData && poData[lsChamp]) {
                        // Cette propriété est fournie dans poData : l'utiliser
                        lsVal = poData[lsChamp] ;
                    } else {
                        // Prendre la valeur par défaut
                        lsVal = thisObject.RtvValeurPourChampDeNouvelleLigne(true, lsChamp, lsVal);
                    }
// LG 20200813 fin
                }
                if ($(this).is(":checkbox")) {
                    if (lsVal === true) {
                        $(this).prop("checked", true);
                    } else {
                        $(this).prop("checked", false);
                    }
                } else {
                    $(this).val(lsVal);
                }
                i++;
            });
            //AV fin 02/05/2019
        }
    };

    loEntityForm.RtvValeurPourChampDeNouvelleLigne = function (pbDuplique, psChamp, psValeurSource) {
        var lsVal = "";
        psChamp = psChamp.toLowerCase();
//MG Modification 20200814 Début

//        if (psChamp == 'cnom' || psChamp == 'cnomcourt' || psChamp == 'username') {
        if (psChamp == 'cnom' || psChamp == 'cnomcourt' || psChamp == 'username' || psChamp == 'cinitiales') {
            // Champ nom ou nom court
//             if (pbDuplique) {
//                lsVal = "Copie de " + psValeurSource;
//            } else {
             if (pbDuplique && psChamp == 'cinitiales') {
                lsVal = "C" + psValeurSource;
			 } else if (pbDuplique){
				lsVal = "Copie de " + psValeurSource;
            } else {
//MG Modification 20200814 Fin
                var lsEntité = thisObject.NomTableToNomEntité(thisObject.RtvLblTable(true));
                if (["a", "à", "e", "é", "è", "i", "o", "u", "y"].includes(lsEntité.substring(0, 1))) {
                    lsVal = "Nouvel " + lsEntité;
                } else {
                    lsVal = "Nouveau " + lsEntité;
                }
            }
//MG Modification 20200811 Début
//Pour avoir "cette semaine" comme validite de base
		} else if (psChamp == 'validite') {
			lsVal = "1";
//MG Modification 20200811 Fin
        } else if (pbDuplique) {
            // Duplication
            lsVal = psValeurSource;
        } else {
            // Ni nom court ni duplication
            var lsLGDataRole = $("#" + thisObject.RtvNomEntityForm() + "_" + psChamp).attr("LGDataRole");
            if (lsLGDataRole) {
                // On a défini le LGDataRole de ce champ
                var laRole = lsLGDataRole.split(".");
                if (laRole[0] === "FK") {
                    // Ce champ sert de cé primaire
                    // Il est sous la forme "FK.<nomTable>.<ColonneRéférencée>" (e.g. "FK.Caisses.iId_Caisse")
                    lsVal = $("#fEntityForm_" + laRole[1].toLowerCase() + "_IdentifiantEntité").text();
                }
            }
        }

        return lsVal;
    }

    // Renvoie .T. si l'entité affichée est une ligne en cours d'insertion
    loEntityForm.EstNouvelleEntité = function () {
        var liId = thisObject.GetIdentifiantEntité();
        return (liId == thisObject.IdNouvelleEntité);
    }

    loEntityForm.BeforeFormSubmit = function (idDuFormulaire) {
        if (thisObject.oEntityFormGrille) {
            // Contexte de formulaire avec grille
            // Sauvegarder les données du formulaire pour remise à jour de la ligne active une fois l'enregistrement terminé
//			thisObject.oEntityFormGrille.oDataTable.MetEnCacheValeursColonnes(idDuFormulaire);
            thisObject.oEntityFormGrille.BeforeFormSubmit(idDuFormulaire);
        }
    }

    // Déterminer si le formulaire a des changements non enregistrés
    loEntityForm.RtvChangementsEnAttente = function (piId) {
        if (thisObject.bModifié)
            return true;
        else
            return false;
    }

    // Spécifier si le formulaire a des changements non enregistrés
    loEntityForm.SetChangementsEnAttente = function (pbChangementsEnAttente) {
        thisObject.bModifié = pbChangementsEnAttente;
// LG 20200907 début
        if (pbChangementsEnAttente) {
            window.onbeforeunload = function (e) {
                e = e || window.event;
//                        msg = msg || '';
                var msg = 'Souhaitez-vous ignorer les modifications ?';     // Je n'ai pas l'impression que cette variable soit utilisée
                // For IE and Firefox
                if (e) {
                    e.returnValue = msg;
                }
                // For Chrome and Safari
                return msg;
            };
        } else {
            window.onbeforeunload = null ;
        }
// LG 20200907 fin
        if (thisObject.oEntityFormBoutons) {
            thisObject.oEntityFormBoutons.setChangementsEnAttente(pbChangementsEnAttente);
        }
    }

    // Retrouver l'objet JQuery formulaire
    // LG 20190701
    loEntityForm.RtvDivFormulaire = function (pbChercheNomTable) {
// LG 20190702 début
//		var loForm = $('#fEntityForm_' + thisObject.RtvNomTable()) ;
//		if (loForm.length > 0 && !loForm[0].checkValidity) {
//			// Ce n'est pas un form : chercher dans les descendants
//			loForm = loForm.find("form") ;
//			if (loForm.length == 1) {
//				// On a trouvé le form
//				// Changer le nom de la table avec celui du form
//				thisObject.nomTable = loForm[0].id.replace("fEntityForm_", "") ;
//			}
//		}
        // Récupérer le form qui a été chargé dans la div de cet élément
        var loElt = $('#' + thisObject.idDivEntityForm);
// LG 20191118 Début
//		var loForm = loElt.find("form") ;
        if (loElt.length > 0) {
            // idDivEntityForm est défini : prendre le form qu'il contient
            var loForm = loElt.find("form");
        } else {
            // idDivEntityForm n'est pas défini : la page ne devrait contenir qu'un seul form
            var loForm = $("form");
            if (loForm.length > 1) {
            }
        }
// LG 20191118 Fin
// LG 20190702 fin
        if (loForm.length !== 1) {
            if (pbChercheNomTable && !loElt.hasClass("entityForm_FormFictif")) {
                console.log("Objet formulaire introuvable ou multiple");
// LG 20190905 deac
            }
            loForm = null;
        } else if (pbChercheNomTable) {
            // Changer le nom de la table avec celui du form
            thisObject.nomTable = loForm[0].id.replace("fEntityForm_", "");
        }
        return loForm;
    }

    // Lancer le chargement de l'enregistrement demandé
    // piIdEntité			: Identifiant de l'entité à charger
    // (psOnglet)			: nom de l'onglet éventuel à charger
    // (callbackSiSuccès)	: pointeur vers la fonction callback à appeller si DoLoadData réussit
    // (callbackSiEchec)	: pointeur vers la fonction callback à appeller si DoLoadData échoue
    // Valeur de retour		: true si succès
    loEntityForm.DoLoadData = function (piIdEntité, psOnglet, callbackSiSuccès, callbackSiEchec) {
        if (!psOnglet && $("#formulaire_OngletActif_" + thisObject.nomTable))
            psOnglet = $("#formulaire_OngletActif_" + thisObject.nomTable).val();
        if (!psOnglet && !thisObject.nomTable) {
//	thisObject.nomTable = $("#formulaire_OngletActif_intervenants").val();
        }

        if (thisObject.oEntityFormBoutons)
            thisObject.oEntityFormBoutons.AfficheBoutons(false);
        var loDivFormulaire = $('#' + this.idDivEntityForm);
        if (loDivFormulaire.length === 0) {
            // Pas de formulaire
            console.warn("Il n'y a aucun formulaire à charger");
            if (callbackSiEchec)
                callbackSiEchec();
            return false;
        }

// LG 20200916 début
        if (window.entityFormBaseGrille_DocumentReady) {
            // Il faut effacer l'éventuelle fonction window.entityFormBaseGrille_DocumentReady qui aurait pu être déclarée par un entityFormBaseGrille.html.twig
            window.entityFormBaseGrille_DocumentReady = null ;
        }
// LG 20200916 fin

        // Chargement
// LG 20200914 déac        if ($('#baseURL')) {
        var lsURLTable = ($('#fEntityForm_URLTable').length > 0) ? $('#fEntityForm_URLTable').text() : thisObject.nomTable;
        var lsURL = getBaseURL() + 'form/' + lsURLTable + '/' + piIdEntité;
// LG 20200914 déac        } else {
// LG 20200914 déac            console.warn("Obsolète");
// LG 20200914 déac            var lsURL = '../form/' + thisObject.nomTable + '/' + piIdEntité;
// LG 20200914 déac        }
        if (psOnglet)
            lsURL += "/" + psOnglet;
        loDivFormulaire.html("");
        // voir https://stackoverflow.com/questions/753300/how-to-catch-error-in-jquerys-load-method
        loDivFormulaire.load(lsURL, private_OnLoadComplete);
        function private_OnLoadComplete(responseText, textStatus, jqXHR) {
            if (callbackSiSuccès)
                callbackSiSuccès(responseText, textStatus, jqXHR);
            thisObject.OnLoadComplete();
        }
        ;

        // Valeur de retour
        return true;
    };

    // Fonction appellée quand le load de l'objet est complet
    // A surcharger si besoin
    loEntityForm.OnLoadComplete = function (responseText, textStatus, jqXHR) {
        if (document.getElementById("fEntityForm_actigroupesacti_cnom") && isNaN(thisObject.GetIdentifiantEntité())) {
// Est-ce utile ?
debugger ;
            document.getElementById("fEntityForm_actigroupesacti_cnom").value = 'Nouveau groupe d\'activité de base';
            $('#fEntityForm_actiBaseSousGroupeGroupe_Enregistrer').removeClass('disabled').addClass('enabled');
            $('#bAnnuler_actiBaseSousGroupeGroupe').removeClass('disabled').addClass('enabled');

        } else if (document.getElementById("fEntityForm_actissgroupesacti_cnom") && isNaN(thisObject.GetIdentifiantEntité())) {
// Est-ce utile ?
// Et la div idGroupe, ajoutée dynamiquement, est-elle utile ?
debugger ;
            document.getElementById("fEntityForm_actissgroupesacti_cnom").value = 'Nouveau sous-groupe d\'activité de base';
            $('#fEntityForm_actiBaseSousGroupeGroupe_Enregistrer').removeClass('disabled').addClass('enabled');
            $('#bAnnuler_actiBaseSousGroupeGroupe').removeClass('disabled').addClass('enabled');
            document.getElementById("fEntityForm_actissgroupesacti_igroupe").value = $('#idGroupe').text();

        } else if (document.getElementById("fEntityForm_actibases_cnom") && isNaN(thisObject.GetIdentifiantEntité())) {
// Est-ce utile ?
// Et la div idGroupe, ajoutée dynamiquement, est-elle utile ?
debugger ;
            document.getElementById("fEntityForm_actibases_cnom").value = 'Nouvelle activité de base';
            $('#fEntityForm_actiBaseSousGroupeGroupe_Enregistrer').removeClass('disabled').addClass('enabled');
            $('#bAnnuler_actiBaseSousGroupeGroupe').removeClass('disabled').addClass('enabled');
            document.getElementById("fEntityForm_actibases_isousgroupe").value = $('#idGroupe').text();
        }
    };

//LG 20190702 old	loEntityForm.DocumentReady = function(pbFormulaireSeul) {
    loEntityForm.FormReady = function (pbFormulaireSeul) {

// LG 20190701 début
//		var lsNomTable = $('#fEntityForm_nomTable').text() ;
//		var lsLibelléTable = $('#fEntityForm_libelléTable').text() ;
//		if (this.nomTable && lsNomTable !== this.nomTable) {
//			// Anomalie
//			console.warn("Incohérence de nom de table") ;
//		}
//		if (this.libelléTable && lsLibelléTable !== this.libelléTable) {
//			// Anomalie
//			console.warn("Incohérence de libellé de table") ;
//		}
//		this.nomTable = lsNomTable ;
//		this.libelléTable = lsLibelléTable ;
        if (!this.nomTable) {
            var lsNomTable = $('#fEntityForm_nomTable').text();
            if (this.nomTable && lsNomTable !== this.nomTable) {
                // Anomalie
                console.warn("Incohérence de nom de table");
            }
            this.nomTable = lsNomTable;
        }

        if (!this.libelléTable) {
            var lsLibelléTable = $('#fEntityForm_libelléTable').text();
            if (this.libelléTable && lsLibelléTable !== this.libelléTable) {
                // Anomalie
                console.warn("Incohérence de libellé de table");
            }
            this.libelléTable = lsLibelléTable;
        }

        this.RtvDivFormulaire(true);		// Corrige éventuellement le nom de la table pour être en accord avec le formulaire
// LG 20190701 fin

        //fonctionne avec le chemin relatif. Aucun probème avec le datatable, mais ne fonctionne pas sur la page form
        if (isNaN(thisObject.GetIdentifiantEntité())) {
            // Pas de ligne active, ou nouvelle ligne
            $('#bSupprimer').css('visibility', 'hidden');
        } else {
            $('#bSupprimer').css('visibility', 'inherit');
        }

        $('#bCacherFormulairError').click(function () {
            thisObject.CacherFormulaireError();
        });

        // Gestionnaire d'événement submit : bouton "Enregistrer"
// LG 20190701 début
//		$('#fEntityForm_' + thisObject.RtvNomTable()).off("submit");
//		$('#fEntityForm_' + thisObject.RtvNomTable()).submit(private_DoSave);
        if (thisObject.RtvDivFormulaire()) {
            thisObject.RtvDivFormulaire().off("submit");
            thisObject.RtvDivFormulaire().submit(private_DoSave);
        }
// LG 20190701 fin

// LG 20190702 début
        // Inscrire l'objet EntityForm dans le tableau window.aEntityForms
        if (!window.aEntityForms) {
            // L'objet window ne contient pas encore le tableau des entityForm
            // L'ajouter maintenant, ainsi que la fonction RtvEntityForm
            window.aEntityForms = [];
            window.RtvEntityForm = function (psNomTable) {
                var loEntityForm = null;
                for (i = 0; i < window.aEntityForms.length; i++) {
                    if (window.aEntityForms[i].nomTable == psNomTable) {
                        loEntityForm = window.aEntityForms[i];
                        break;
                    }
                }
                return loEntityForm;
            }
        }
        window.aEntityForms[window.aEntityForms.length] = thisObject;
// LG 20190702 fin

        // Lancer la sauvegarde de l'enregistrement demandé
        function private_DoSave(event) {
            if (thisObject.oEntityFormGrille && thisObject.oEntityFormGrille.EstEnAttente())
                return false;
            // Vérifier la validité (HTML5 : attributs "requested")
            // Selon https://stackoverflow.com/questions/11866910/how-to-force-a-html5-form-validation-without-submitting-it-via-jquery
            if (!thisObject.RtvDivFormulaire()[0].checkValidity()) {
                // Les saisies sont invalides : demander au browser d'afficher les pbs qu'il a trouvé
                thisObject.RtvDivFormulaire()[0].reportValidity();
                return false;
            }

            thisObject.Patientez("Enregistrement en cours");
            thisObject.CacherFormulaireError();
// LG 20200810 old            var liIdEntité = thisObject.getCodeTable() + thisObject.GetIdentifiantEntité();
            var liIdEntité = thisObject.GetIdentifiantEntité();
            thisObject.BeforeFormSubmit(liIdEntité);
            var lsURL;
            var lsURLTable = ($('#fEntityForm_URLTable').length > 0) ? $('#fEntityForm_URLTable').text() : thisObject.RtvNomTable();
            if (liIdEntité === null || liIdEntité === 0) {
                // On est sur une nouvelle ligne
                lsURL = getBaseURL() + 'add/' + lsURLTable;
            } else {
                // On modifie une ligne déja existante
// LG 20200810 old                lsURL = getBaseURL() + 'update/' + lsURLTable + '/' + liIdEntité;
                lsURL = getBaseURL() + 'update/' + lsURLTable + '/' + thisObject.getCodeTable() + liIdEntité;
            }

            $.post(
                    lsURL,
                    $(this).serialize(),
                    function (data) {
                        // NK 20170619 - DEBUT
                        // Mise en commentaire de la ligne suivante
                        //$("#formulaire_detail").html(data);
                        // NK 20170619 - FIN
                    })
                    .done(function (msg) {
 // MG Ajout 20200821 Début
						if(msg.msgErreur){
							var lsHTMLDialogue = "<div id='msgErreur' title='Erreur formulaire'>"
												+ "<p>"+msg.msgErreur+"</p>"
												+ "</div>";
							$('body').append('<div id="blockingBackground" class="ui-widget-overlay ui-front" style="z-index: 100;"></div>');
							var dialog = $(lsHTMLDialogue).dialog({
// MG Modif 20200915 Début
// Le background du modal est disabled
								modal:true,
// MG Modif 20200915 Fin
								width: 800, 
								height: 295, 
								close: function (e, o) {
								   $('#msgErreur').dialog('close');
								},
								buttons: {
									"OK": function(event, ui) {
										dialog.dialog('destroy');
										}   
							}});
						}
						
						if(msg.idEntite){
 // MG Ajout 20200821 Fin					
							// Succès de l'enregistrement
							if (lsNomTable !== 'actibases' && lsNomTable !== 'actiSsgroupesacti' && lsNomTable !== 'actiGroupesacti') {
								liIdEntité = msg.idEntite;
							}
							thisObject.SetIdentifiantEntité(liIdEntité);
							thisObject.SetChangementsEnAttente(false);
							if (thisObject.oEntityFormGrille)
								thisObject.oEntityFormBoutons.ResetNouveau();

							// Enregistrer les valeurs des champs pour restaurer si annulation
							thisObject.MetEnCacheValeursChamps();

							// Lancer l'événement "dataTable_OnFormSaved"

							// AV 03/04/2019 début
							//if (thisObject.oEntityFormGrille) thisObject.oEntityFormGrille.oDataTable.OnFormSaved(liIdEntité) ;
							if (thisObject.oEntityFormGrille) {
								if (thisObject.oEntityFormGrille.oDataTable) {
									thisObject.oEntityFormGrille.oDataTable.OnFormSaved(liIdEntité);
								} else if (thisObject.oEntityFormGrille.oTreeview) {
									thisObject.oEntityFormGrille.oTreeview.OnFormSaved(liIdEntité);
								}
							}
							// AV 03/04/2019 fin

							if (thisObject.onSave_Callback && typeof thisObject.onSave_Callback === "function") {
								// On a fourni une fonction callback : l'appeller maintenant
								thisObject.onSave_Callback(thisObject.onSave_CallbackData);
							}
							thisObject.onSave_Callback = null;
							thisObject.onSave_CallbackData = null;
						}
                    })
                    .fail(function (xhr, status, error) {
                        thisObject.MontrerFormulaire_error(xhr.responseText);
                    })
                    .always(function () {
                        thisObject.onSave_Callback = null;
                        thisObject.onSave_CallbackData = null;
                        thisObject.PatientezFin();
                    });

            ;
            return false;
        };
		
        $('#fEntityForm_' + thisObject.RtvNomTable() + '_Enregistrer').unbind('click');
        $('#fEntityForm_' + thisObject.RtvNomTable() + '_Enregistrer').on('click', function () {
            if (!$('#fEntityForm_' + thisObject.RtvNomTable() + '_Enregistrer').hasClass("disabled")) {
                // Des modifications ont été apportées : le bouton est enabled

                // Valider le formulaire : demander l'enregistrement
                thisObject.RtvDivFormulaire().submit();

                // Le false permet de désactiver complétement la fonctionnalité du
                // bouton car bootstrap le fait seulement visuellement
                return false;
            }
        });

        // Evenement quand l'utilisateur change une valeur dans le formulaire
        $('input, select, textarea').on('keyup change', function (event) {
            if (!document.getElementById('fEntityForm_' + thisObject.RtvNomTable())
                    || !document.getElementById('fEntityForm_' + thisObject.RtvNomTable()).contains(event.target)) {
                // L'élément modifié ne fait pas partie du formulaire
                return;
            }
            thisObject.SetChangementsEnAttente(true);
        });

        // Grille éventuelle du form
        if (typeof entityFormBaseGrille_DocumentReady === "function") {
            entityFormBaseGrille_DocumentReady(thisObject.nomTable);
        }

        // Gestion des boutons
        if (pbFormulaireSeul) {
            // Lancement dans le cas d'un formulaire sans grille : il faut gérer les boutons
            window.entityForm_FormulaireSeul = true;
            thisObject.FormulaireSeul = true;
            thisObject.MetEnCacheValeursChamps();

            // Afficher les boutons
            thisObject.oEntityFormBoutons.PlaceBoutonEnregistre();
            thisObject.oEntityFormBoutons.AfficheBoutonNouveau(false);
            thisObject.oEntityFormBoutons.AfficheBoutonDupliquer(false);
            thisObject.oEntityFormBoutons.AfficheBoutonSupprimer(false);
// LG 20191118 début
//			thisObject.oEntityFormBoutons.AfficheBoutons(true) ;
            // Afficher les boutons à droite de l'écran, les uns à côté des autres
            thisObject.oEntityFormBoutons.AfficheBoutons(true, "BoutonsFormulaireSeul", "col-lg-6");
            thisObject.oEntityFormBoutons.oEntityForm = thisObject;
// LG 20191118 fin

            // L'enregistrement vient d'être chargé, il n'y a pas eu de modifications
            thisObject.SetChangementsEnAttente(false);

        }
    };

    // LG 20201001
    loEntityForm.DoListing = function () {
        if (thisObject.oEntityFormGrille.EstEnAttente())
            return false;
    
        if (thisObject.RtvChangementsEnAttente()) {
            //		alert("Enregistrez les modifications au préalable") ;
            thisObject.EnregistreSiNecessaire(thisObject.DoListing);
            return;
        }
        
        return entityForm_DoListing1(thisObject.nomTable) ;
        
    }

    return loEntityForm;
}

// LG 20201001 début
//function entityForm_DoListing1() {
//    var lsURL = getBaseURL() + 'listing/' + thisObject.nomTable;
function entityForm_DoListing1(psNomTable) {
    var lsURL = getBaseURL() + 'listing/' + psNomTable;
// LG 20201001 fin
    $.post(
            lsURL,
            $(this).serialize(),
            function (data) {
                $("#formulaire").html(data);
            })
            .done(function (msg) {
                debugger ;
                alert("OK");
            })
            .fail(function (xhr, status, error) {
                var lsMsg = "Désolé, une erreur a empêché l'édition de ce listing : ";
                var lsMsg2 = error ? error : "Erreur inconnue"
                alert(lsMsg + lsMsg2);
            });
    return false;
}

function entityForm_DoListing2() {
// Pour savoir laquelle des versions de $('#bListing').click est effectivement appellée
    // Solution provisoire
    // Ca serait mieux de le faire Ajax-like, selon https://stackoverflow.com/questions/4545311/download-a-file-by-jquery-ajax
    var lsURL = getBaseURL() + 'listing/' + thisObject.nomTable;

    /*
     var lsURL = '../listing/{{ psNomTable }}' ;
     $.post(
     lsURL,
     $(this).serialize(),
     function (data) {
     $("#formulaire").html(data);
     })
     .done(function (msg) {
     alert("OK");
     })
     .fail(function (xhr, status, error) {
     alert("KO");
     });
     return false;
     */
}
