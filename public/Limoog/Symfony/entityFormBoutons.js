//Scripts correspondant à la portion de page de saisie qui contient les boutons de gestion :
//Enregistrer, Annuler, Nouveau, Dupliquer, Rechercher, ...

// window.entityForm_bModifié = false ;
// entityForm.js est censé être connu
if (!window.entityForm_Défini) {
    console.warn("entityForm.js n'a pas été exécuté !");
}

// Créer un objet entityFormBoutons
// poEntityFormGrille : l'objet dans la propriété entityFormBoutons duquel on va stocker l'objet généré
//			et qui sera le parent du entityFormBoutons généré (accessible par la propriété "oEntityFormGrille")
function entityFormBoutons(poEntityFormGrille, psNomTable) {
    var loEntityFormBoutons = {};
    var thisObject = loEntityFormBoutons;
    loEntityFormBoutons.nomClasse = "entityFormBoutons";
    loEntityFormBoutons.nomTable = psNomTable;
    loEntityFormBoutons.oEntityFormGrille = poEntityFormGrille;

//	loEntityFormBoutons.RtvNomTable = function(pbLowerCase) {
//		// Doit être la même chose que thisObject.nomTable, sauf pour la casse ;
//		return thisObject.oEntityFormGrille.oEntityForm.RtvNomTable(pbLowerCase) ;		
//	}

    loEntityFormBoutons.DoAnnuler = function (event) {
        if (thisObject.oEntityFormGrille.oEntityForm.FormulaireSeul) {
//			thisObject.oEntityForm.DoLoadData(thisObject.oEntityForm.GetIdentifiantEntité()) ;
            thisObject.oEntityFormGrille.oEntityForm.DoAnnuler();
            return;
        }
        if (thisObject.oEntityFormGrille.EstEnAttente())
            return false;
        if (thisObject.oEntityFormGrille.oEntityForm.EstNouvelleEntité()) {
            // Nouvelle ligne en cours
            var liId;
//			if (dataTable_DoDeleteRow) {
//				// On est dans une page avec grille : effacer la ligne du dataTable
//				liId = dataTable_DoDeleteRow() ;
//			}
            if (thisObject.oEntityFormGrille.oDataTable) {
                // On est dans une page avec grille : effacer la ligne du dataTable
                liId = thisObject.oEntityFormGrille.oDataTable.DoDeleteRow();
            }
// LG 20200811 début
            else if (thisObject.oEntityFormGrille.oTreeview) {
                var loNode = thisObject.oEntityFormGrille.oTreeview.DoDeleteNode();
                liId = loNode.cid ;
            }
            else {
                throw new exception("Cas non prévu : delete hors contexte d'une grille");
            }
// LG 20200811 fin

            thisObject.ResetNouveau();

            // Recharger la ligne nouvellement sélectionnée
            thisObject.oEntityFormGrille.DoLoadData(liId);
        } else {
            // Ligne pré-existante : annuler les changements dans le formulaire
            if (thisObject.oEntityFormGrille.oEntityForm.DoAnnuler)
                thisObject.oEntityFormGrille.oEntityForm.DoAnnuler();
        }
        return true;
    }

    loEntityFormBoutons.DoNouveau = function () {
        thisObject.oEntityFormGrille.oEntityForm.DoNouveauOuDuplique(false);
    }

    loEntityFormBoutons.DoDuplique = function () {
	// LG 20200813 début
    if (thisObject.oEntityFormGrille.oTreeview) {
        // Cas particulier des activités de base : pas encore au point
        console.error("Non opérationnel dans le cas des activités de base") ;
        alert("Désolé, cette fonctionnalité n'est pas encore disponible") ;
        return ;
	// LG 20200813 fin
// MG Modification 20200821 Début
// Fonction non disponible
     } else if (thisObject.oEntityFormGrille.nomTable === "intervenants" ){
		console.error("Non opérationnel dans le cas des intervenants") ;
        alert("Désolé, cette fonctionnalité n'est pas encore disponible") ;
        return ;
	 }
// MG Modification 20200821 Fin 

        thisObject.oEntityFormGrille.oEntityForm.DoNouveauOuDuplique(true);
    }

    loEntityFormBoutons.DoDelete = function () {
        thisObject.oEntityFormGrille.oEntityForm.DoDelete();
    }

// LG 20201001
    loEntityFormBoutons.DoListing = function (event) {
         if (thisObject.oEntityFormGrille.EstEnAttente())
            return false;
        if (thisObject.oEntityFormGrille.oEntityForm.DoListing)
            thisObject.oEntityFormGrille.oEntityForm.DoListing();
        return true;
    }

// LG 20190408 old	loEntityFormBoutons.PlaceBoutonEnregistre = function() {
    loEntityFormBoutons.PlaceBoutonEnregistre = function (psNomTable) {
        // Le bouton enregistrer est particulier car il est généré dans le formulaire et lui est nécessaire pour fonctionner
        $('#ctnCmdEnregistrer_' + thisObject.nomTable).children().each(function () {
            var $cmd = $(this);
            $cmd.remove();
        })

        var lsNomTable = psNomTable ? psNomTable.toLowerCase() : thisObject.nomTable;
        var lsBouton = 'fEntityForm_' + lsNomTable + '_Enregistrer';
// LG 20200915 test        var lsBouton = 'fEntityForm_' + lsNomTable.toLowerCase() + '_Enregistrer';
        var loBouton = document.getElementById(lsBouton);
// LG 20190409 début
        if (loBouton && thisObject.nomTable !== lsNomTable) {
            // Noms de tables différents (ex. écran des actibases)
            // Renommer le bouton pour lui donner le nom général
            lsBouton = 'fEntityForm_' + thisObject.nomTable + '_Enregistrer';
// LG 20200915 test            lsBouton = 'fEntityForm_' + thisObject.nomTable.toLowerCase() + '_Enregistrer';
            loBouton.id = lsBouton;

            // Renommer aussi le formulaire
//			document.getElementById('fEntityForm_' + lsNomTable).id = 'fEntityForm_' + thisObject.nomTable ; ;
        }
        if (loBouton) {
            var loEmplacement = document.getElementById('ctnCmdEnregistrer_' + thisObject.nomTable);
            loEmplacement.appendChild(loBouton);
            if (loBouton.childNodes.length == 1) {
                $("#" + lsBouton).append('<span class="pull-right"><span class="glyphicon glyphicon-save"></span></span>');
            }
        }
    }

    // Déterminer si le formulaire a des changements non enregistrés
//    loEntityFormBoutons.ResetNouveau = function () {
//        if (loEntityFormBoutons.oEntityFormGrille.oEntityForm.EstNouvelleEntité()) {
//            $("#bNouveau_" + thisObject.nomTable).addClass("disabled");
//            $("#bDupliquer_" + thisObject.nomTable).addClass("disabled");
//            $("#bSupprimer_" + thisObject.nomTable).addClass("disabled");
//// LG 20200813 début            
//            $('#fEntityForm_' + thisObject.nomTable + '_Enregistrer').prop( "disabled", true );
//            $("#bAnnuler_" + thisObject.nomTable).prop( "disabled", true );
//            $("#bSupprimer_" + thisObject.nomTable).prop( "disabled", true );
//// LG 20200813 fin            
//        } else {
//            $("#bNouveau_" + thisObject.nomTable).removeClass("disabled");
//            $("#bDupliquer_" + thisObject.nomTable).removeClass("disabled");
//            $("#bSupprimer_" + thisObject.nomTable).removeClass("disabled");
//// LG 20200813 début            
//            $('#fEntityForm_' + thisObject.nomTable + '_Enregistrer').prop( "disabled", false );
//            $("#bAnnuler_" + thisObject.nomTable).prop( "disabled", false );
//            $("#bSupprimer_" + thisObject.nomTable).prop( "disabled", false );
//// LG 20200813 fin            
//        }
//    }
    loEntityFormBoutons.setBoutonDisabled = function (psIdBouton, pbDisabled) {
// debugger ;
        var loBouton = $("#" + psIdBouton) ;
        if (pbDisabled) {
            loBouton.addClass("disabled");
        } else {
            loBouton.removeClass("disabled");
        }
        loBouton.prop( "disabled", pbDisabled );
    }
    loEntityFormBoutons.ResetNouveau = function () {
        var lbDisabled = (loEntityFormBoutons.oEntityFormGrille.oEntityForm.EstNouvelleEntité())?true:false ;
        loEntityFormBoutons.setBoutonDisabled("bNouveau_" + thisObject.nomTable, lbDisabled) ;
        loEntityFormBoutons.setBoutonDisabled("bDupliquer_" + thisObject.nomTable, lbDisabled) ;
        loEntityFormBoutons.setBoutonDisabled("bSupprimer_" + thisObject.nomTable, lbDisabled) ;
// LG 20200928 déac        loEntityFormBoutons.setBoutonDisabled("bSupprimer_" + thisObject.nomTable, lbDisabled) ;
        loEntityFormBoutons.setBoutonDisabled("bNouveauGroupe_actiBaseSousGroupeGroupe", lbDisabled) ;
        loEntityFormBoutons.setBoutonDisabled("bNouveauSousGroupe_actiBaseSousGroupeGroupe", lbDisabled) ;
        loEntityFormBoutons.setBoutonDisabled("bNouvelleActiviteBase_actiBaseSousGroupeGroupe", lbDisabled) ;
        
        loEntityFormBoutons.setBoutonDisabled('fEntityForm_' + thisObject.nomTable + '_Enregistrer', !lbDisabled) ;
// LG 20200915 test        loEntityFormBoutons.setBoutonDisabled('fEntityForm_' + thisObject.nomTable.toLowerCase() + '_Enregistrer', !lbDisabled) ;
        loEntityFormBoutons.setBoutonDisabled("bAnnuler_" + thisObject.nomTable, !lbDisabled) ;
    }

    // Spécifier si le formulaire a des changements non enregistrés
//    loEntityFormBoutons.setChangementsEnAttente = function (pbChangementsEnAttente) {
//        if (pbChangementsEnAttente) {
//            $('#fEntityForm_' + thisObject.nomTable + '_Enregistrer').removeClass("disabled");
//            $("#bAnnuler_" + thisObject.nomTable).removeClass("disabled");
//// LG 20200812 début            
//            $('#fEntityForm_' + thisObject.nomTable + '_Enregistrer').prop( "disabled", false );
//            $("#bAnnuler_" + thisObject.nomTable).prop( "disabled", false );
//// LG 20200812 fin            
//        } else {
//            $('#fEntityForm_' + thisObject.nomTable + '_Enregistrer').addClass("disabled");
//            $("#bAnnuler_" + thisObject.nomTable).addClass("disabled");
//// LG 20200812 début            
//            $('#fEntityForm_' + thisObject.nomTable + '_Enregistrer').prop( "disabled", true );
//            $("#bAnnuler_" + thisObject.nomTable).prop( "disabled", true );
//// LG 20200812 fin            
//        }
//    }
    loEntityFormBoutons.setChangementsEnAttente = function (pbChangementsEnAttente) {
        var lbDisabled = pbChangementsEnAttente?false:true ;
        loEntityFormBoutons.setBoutonDisabled('fEntityForm_' + thisObject.nomTable + '_Enregistrer', lbDisabled) ;
// LG 20200915 test        loEntityFormBoutons.setBoutonDisabled('fEntityForm_' + thisObject.nomTable.toLowerCase() + '_Enregistrer', lbDisabled) ;
        loEntityFormBoutons.setBoutonDisabled("bAnnuler_" + thisObject.nomTable, lbDisabled) ;
// LG 20200915 test        loEntityFormBoutons.setBoutonDisabled("bAnnuler_" + thisObject.nomTable, lbDisabled) ;
    }

// LG 20191118 old	loEntityFormBoutons.AfficheBoutons = function(pbVisible){
//		thisObject.AfficheBouton('ctnBoutons_' + thisObject.nomTable, pbVisible) ;
    loEntityFormBoutons.AfficheBoutons = function (pbVisible, psCSSClass, psCSSButtonClass) {
        var lsBoutons = 'ctnBoutons_' + thisObject.nomTable;
        thisObject.AfficheBouton(lsBoutons, pbVisible);
        if (psCSSClass) {
            //MG 20192511
            //Modifier pour la mise en page des boutons sur la page Mon profil
//			$('#' + lsBoutons).addClass(psCSSClass) ;
            $('#' + lsBoutons + '_wrapper').addClass(psCSSClass);
        }
        if (psCSSButtonClass) {
            $('#' + lsBoutons).children().addClass(psCSSButtonClass);
        }
    }
    loEntityFormBoutons.AfficheBoutonNouveau = function (pbVisible) {
        thisObject.AfficheBouton('bNouveau_' + thisObject.nomTable, pbVisible);
    }
    loEntityFormBoutons.AfficheBoutonDupliquer = function (pbVisible) {
        thisObject.AfficheBouton('bDupliquer_' + thisObject.nomTable, pbVisible);
    }
    loEntityFormBoutons.AfficheBoutonSupprimer = function (pbVisible) {
        thisObject.AfficheBouton('bSupprimer_' + thisObject.nomTable, pbVisible);
    }
    loEntityFormBoutons.AfficheBouton = function (psIdBouton, pbVisible) {
        var lsDisplay = pbVisible ? 'block' : 'none';
        $('#' + psIdBouton).css('display', lsDisplay);
    }

//}

// LG 20190222 deac $(document).ready(function () {
//
//	// Rattachement de la zone de texte de "Filtrer"
//	if (document.getElementById('entityFormGrille_DataTable_filter')) {
//		// Il y a une dataGrid : déplacer la zone de recherche dans la zone des boutons
//		document.getElementById('ctnCmdSearch').appendChild(document.getElementById('entityFormGrille_DataTable_filter'));
//	} else {
//		// Il n'y a pas de dataGrid : masquer la zone de recherche
//		$('#ctnCmdSearch').css('display', 'none') ;
//	}

    // Renommer les boutons pour les personnaliser pour pour la table qu'ils gèrent
    $('#ctnBoutons').prop("id", "ctnBoutons_" + psNomTable);
    $('#ctnCmdSearch').prop("id", "ctnCmdSearch_" + psNomTable);
    $('#ctnCmdEnregistrer').prop("id", "ctnCmdEnregistrer_" + psNomTable);
    $('#ctnCmdAnnuler').prop("id", "ctnCmdAnnuler_" + psNomTable);
    $('#bAnnuler').prop("id", "bAnnuler_" + psNomTable);
    $('#ctnCmdNouveau').prop("id", "ctnCmdNouveau_" + psNomTable);
    $('#bNouveau').prop("id", "bNouveau_" + psNomTable);
    $('#ctnCmdDupliquer').prop("id", "ctnCmdDupliquer_" + psNomTable);
    $('#bDupliquer').prop("id", "bDupliquer_" + psNomTable);
    $('#ctnCmdSupprimer').prop("id", "ctnCmdSupprimer_" + psNomTable);
    $('#bSupprimer').prop("id", "bSupprimer_" + psNomTable);
    $('#ctnCmdListing').prop("id", "ctnCmdListing_" + psNomTable);
    $('#bListing').prop("id", "bListing_" + psNomTable);

    // Gestionnaire du clic sur Enregistrer
    // RAS : c'est géré par le Form généré par entityForm.js

    // Gestionnaire du clic sur Annuler
    $('#bAnnuler_' + psNomTable).click(loEntityFormBoutons.DoAnnuler);

    // Gestionnaire du clic sur Nouveau
    $('#bNouveau_' + psNomTable).click(loEntityFormBoutons.DoNouveau);

    // Gestionnaire du clic sur Dupliquer
    $('#bDupliquer_' + psNomTable).click(loEntityFormBoutons.DoDuplique);

    // Gestionnaire du clic sur supprimer
    $('#bSupprimer_' + psNomTable).click(loEntityFormBoutons.DoDelete);

    // Gestionnaire du clic sur Listing
// LG 20201001 début
//    $('#bListing_' + psNomTable).click(entityForm_DoListing1);
//    $('#bListing_' + psNomTable).click(entityForm_DoListing2);
    $('#bListing_' + psNomTable).click(loEntityFormBoutons.DoListing);
// LG 20201001 fin

    return loEntityFormBoutons;
}	// fin de définition de entityFormBoutons
