/* 
 * LG 20180627
 * Procédures javascript pour afficher les form Symfony sous forme de fenêtre popup
 */
window.entityFormPopup_Défini = true;	// Pour vérification dans les scripts qui utilisent ce qui est défini ici

function newEntityFormPopup() {
    var loEntityFormPopup = {};
    var thisObject = loEntityFormPopup;

    var loEntityForm = newEntityForm();
    loEntityFormPopup.oEntityForm = loEntityForm;
    window.entityFormPopup = loEntityForm;

    // Fonction appellée par entityForm quand le chargement du formulaire dans la fenêtre dialog sera fini
    loEntityForm.OnLoadComplete = function () {

        // Initialiser le entityForm
//LG 20190702 old		thisObject.oEntityForm.DocumentReady() ;
        thisObject.oEntityForm.FormReady();

        // Masquer le bouton Enregistrer natif
        $('#fEntityForm_' + thisObject.oEntityForm.nomTable + '_Enregistrer').unbind('click');
        $('#fEntityForm_' + thisObject.oEntityForm.nomTable + '_Enregistrer').css("display", "none");
    };

    // Affichage du formulaire sous forme de boite de dialogue, selon
    // Selon https://jqueryui.com/dialog/#modal-form
    // doc : https://api.jqueryui.com/dialog/
    // http://jsfiddle.net/GF7KF/4/

//MG Modification 20200131 Début
//	loEntityFormPopup.popUpEntité = function(psTable, piIdEntité) {
    loEntityFormPopup.popUpEntité = function (psTable, piIdEntité, callback) {
//MG Modification 20200131 Fin
//MG Modification 20200127 Début
        var title;


//MG Modification 20202502 Début
//		if(psTable === "seancesTrad"){
////          Il n'existe pas de nom de table, il s'agit de la popup de modification du seance sur le planning
//			lsURL = getBaseURL() + 'entityForActivite/' + psTable + '/' + piIdEntité;
//			title = "Modification de la séance";
//MG Ajout 20200225 Début
//		} else if(psTable === "activitesTrad") {
//			lsURL = getBaseURL() + 'entityForActivite/' + psTable + '/' + piIdEntité;
//			title = "Modification de l'activité";
//MG Ajout 20200225 Fin
//		} else {
//          Création d'un formulaire popup
//			var lsURL = getBaseURL() + 'form/' + psTable + "/" + piIdEntité;
//			title = thisObject.oEntityForm.libelléTable;
//		}

        if (psTable) {
            var lsURL = getBaseURL() + 'entityForActivite/' + psTable + '/' + piIdEntité ;
            if (psTable === 'seancesTrad') {
                title = "Modification de la séance";
            } else if (psTable === 'activitesTrad') {
                title = "Modification de l'activité";
            }
        } else {
            var lsURL = getBaseURL() + 'form/' + psTable + "/" + piIdEntité ;
            title = thisObject.oEntityForm.libelléTable;
        }
//MG Modification 20202502 Fin
//MG Modification 20200127 Fin
        if (!$("#dialog-form").length) {
            // Il faut créer la div de base
//			$("body").append('<div id="dialog-form" title="Saisie dans un popup" style="height:auto; display:none;"></div>');
            $("body").append('<div id="dialog-form" style="display:none;"></div>');
        }
        $("#dialog-form").load(lsURL, private_OuvreDialog);
        function private_OuvreDialog() {
            // Initialiser l'objet entityForm
            thisObject.oEntityForm.idDivEntityForm = 'dialog-form';
            thisObject.oEntityForm.OnLoadComplete();
//MG Modification 20200922 Début			
			if (estHtml_DemandeLogin(dialog.html())){
				var lsHTMLDialogue = "<div id='loginPopup' title='Déconnexion suite à une inactivité'>"
                                            + "<p>" + dialog.html() + "</p>"
                                            + "</div>";
				$(lsHTMLDialogue).dialog({
					modal:true,
					width: 400, 
					height: 600});
				
				$('.ui-dialog-titlebar-close').hide();
				$('form').append('<input type="text" id="authentificationParPopup" name="_authentificationParPopup" value="_authentificationParPopup" disabled="" hidden="">');
				$('#authentificationParPopup').val('_authentificationParPopup');
				$('.cadreIntérieur > form').submit(function (event) {
					event.preventDefault();
					lsURL = getBaseURL() + 'login_check';
					$.post(
							lsURL,
							$(this).serialize(),
							)
							.done(function (msg) {
								if (msg) {
//MG Modification 20200928 Début
//										if (msg.includes("fEntityForm_seances")) {
										if (!estHtml_DemandeLogin(msg)) {
//MG Modification 20200928 Fin
										$('#loginPopup').dialog("close");
										$('#loginPopup').remove();
										isLogout = false;
										$('#dialog-form').html(msg);
										$('#dialog-form').dialog("open");
									} else {
										if (typeof estModeDev === "function" && estModeDev()) {
											if (typeof LG_Ajax_Notifie === "function") {
												LG_Ajax_Notifie("Echec de l'authentification, la page reçue était : " + msg);
											}
										}
										$("#msgDuLayout").text("Echec de l'authentification");
									}

								}
							})
							.fail(function (xhr, status, error) {
								if (typeof LG_Ajax_Notifie === "function") {
									LG_Ajax_Notifie("La vérification du login a échoué : " + error);
								}
								alert('Echec de la vérification du login : ' + error);
							});
				});															
			} else {
				// Afficher le form dans la boite de dialogue
				dialog.dialog("open");
			}
//MG Modification 20200922 Fin	
        }

        var dialogOptions = {height: "auto", width: "auto"};
        var dialog = $("#dialog-form").dialog({
//MG Modification 20200127 Début
//		  title: thisObject.oEntityForm.libelléTable,
            title: title,
//MG Modification 20200127 Fin
            autoOpen: false,
            height: dialogOptions.height, //400
            width: dialogOptions.width, //350
            modal: true,
            buttons: {
                Enregistrer: function () {
                    dialog.find("form").submit();
//MG Ajout 20200310 Début
//Quitte le dialog form quand on enregistre
                    private_CloseDialog();
//MG Ajout 20200310 Fin
                },
                Annuler: function () {
                    dialog.dialog("close");
                }
            },
            beforeClose: function (e, ui) {
                if (!dialog.bCloseMêmeSiModifié) {
                    thisObject.oEntityForm.EnregistreSiNecessaire(private_CloseDialog		// callbackSiSuccès
                            , null		// callbackData
                            , null		// callbackSiEchec
                            );
                    e.preventDefault();
                }
                dialog.bCloseMêmeSiModifié = false;

            },
            close: function (e, o) {
                // Vider tout pour restaurer les options qui ont pu être modifiées à l'utilisation	
                callback();
                $(this).dialog('destroy');
            }
        });

        function private_CloseDialog() {
            dialog.bCloseMêmeSiModifié = true;
            dialog.dialog("close");
        }

    };

    return loEntityFormPopup;
}
