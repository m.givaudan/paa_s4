window.oPaa = {};

window.oPaa.onChangeEtablissement = function () {
    return ajaxSetParametre("giEtablissement", $("#selectEtablissement").val(), "NU");
};

window.oPaa.onChangeAnnee = function () {
    return ajaxSetParametre("giAnnée", $("#selectPeriodeTravail").val(), "NU");
};

window.oPaa.onChangeDates = function () {
    if (document.readyState !== 'complete') { //On vérifié que le document soit complètement chargé
        return;
    }
    var monOrigin = new URL(window.location.origin).toString(); //Récupération du chemin d'origine de l'url
    var monPath = window.location.pathname.toString().substr(1); //Récupération du chemin de fichier de l'url et on supprime le premier '/'

    var monUrl = monOrigin + monPath; //Reformation de l'url sans paramètres
    var dateDebut = window.oPaa.getDateDebut('YYYY-MM-DD');
    var dateFin = window.oPaa.getDateFin('YYYY-MM-DD');

    var urlParams = new URLSearchParams(window.location.search); // Création d'un objet URLSearchParams avec les paramètres de mon url actuelle

    if (monPath === 'planning') { // Si on est sur le planning, on empeche la redirection pour garder le fonctionnement de base
        setVariableDeSession('dateDebut', dateDebut, "NU"); //Modifie la variable de session, pour evité de faire recharger la page planning
        setVariableDeSession('dateFin', dateFin, "NU"); //Modifie la variable de session, pour evité de faire recharger la page planning
        return;
    }

    if (/[?&]dateDebut=/.test(location.search)) { //Si un paramètre dateDebut existe deja on le supprime
        urlParams.delete('dateDebut');
    }
    if (/[?&]dateFin=/.test(location.search)) { //Si un paramètre dateFin existe deja on le supprime
        urlParams.delete('dateFin');
    }
    urlParams.append('dateDebut', dateDebut); //On ajoute notre parametre dateDebut
    urlParams.append('dateFin', dateFin); //On ajoute notre parametre dateDebut

    url = monUrl + '?' + urlParams; //Création de l'url avec le même chemin et des paramètres différents

    window.location = url; //Redirection vers la nouvelle url
};

window.oPaa.getIdEtablissement = function () {
    return $('#selectEtablissement').val();
};

window.oPaa.getIdAnnee = function () {
    return $('#selectPeriodeTravail').val();
};

//MG Modification 20200203 Début
window.oPaa.getDateAvecJour = function (piJour, piSemaine, pbAccepteSemaineInvalide = false) {
    var ldDateTest = new Date('01/01/1951');
    var semaineMilli = piSemaine * (7 * 24 * 60 * 60 * 1000);
    var jourMilli = (piJour - 1) * (24 * 60 * 60 * 1000);
    var dateEnMilli = ldDateTest.getTime() + semaineMilli + jourMilli;
    var newDate = new Date(dateEnMilli);
    return newDate;
};
//MG Modification 20200203 Fin

window.oPaa.getNoSemaine = function (pdDate, tbSansErreurSiDateInvalide) {
// LG 20200511 début
//    var ldDate = Date.parse(pdDate); 
//    var ldDateTest = Date.parse('01 Jan 1951');
//    var liSem = Math.trunc((ldDate - ldDateTest) / (7 * 24 * 60 * 60 * 1000)) ;

    var ldDate = moment(pdDate);

    // Cas particulier : semaine reconductible
    if (ldDate.year() === 1900) {
        return 0;
    }
    var liSem;
    // Cas Particulier : semaines types
    if (ldDate.year() < 1850) {
        // Brouillon de semaine-type
        liSem = -100 + (ldDate.year() - 1800);		// -100 = eiSemaineInexistante
        var ldDateDebut = window.oPaa.GetDateAvecJour(1, liSem);

        if (!(tvDate >= ldDateDebut && tvDate <= ldDateDebut + 6)) {
            if (!tbSansErreurSiDateInvalide) {
                throw 'paa.GetNoSemaine, Cas non prévu N°1 : tvDate = ' + pdDate + ', liSem = ' + liSem;
            }
            liSem = -100;
        }
        return liSem;
    } else if (ldDate.year() < 1950) {
        // Semaine type
        liSem = -1 * (ldDate.year() - 1900);
        ldDateDebut = window.oPaa.GetDateAvecJour(1, liSem);
        if (!(tvDate >= ldDateDebut && tvDate <= ldDateDebut + 6)) {
            if (!tbSansErreurSiDateInvalide) {
                throw 'paa.GetNoSemaine, Cas non prévu N°2 : tvDate = ' + tvDate + ', liSem = ' + liSem;
            }
            liSem = -100;
        }
        return liSem;
    } else {
        // Nothing
    }

    // Cas général
    var ldDateTest = moment([1951, 0, 01]);
    var liSem = Math.trunc(ldDate.diff(ldDateTest, "days") / 7) + 1;
// LG 20200511 fin
    return liSem;
};

window.oPaa.getDate = function (pdDate, formatDate) {
    //On utilise en paramètre le paramètre de la fonction format de moment.js (formatDate)
    var momentObj = moment(pdDate);
    var momentString = momentObj.format(formatDate);
    return momentString;
};

window.oPaa.getDateDebut = function (formatDate) {
    return window.oPaa.getDate($('#cbo2Dates').data('daterangepicker').startDate, formatDate);
};

window.oPaa.getSemaineDebut = function () {
    return window.oPaa.getNoSemaine($('#cbo2Dates').data('daterangepicker').startDate);
};

window.oPaa.getDateDebutAnnee = function (formatDate) {
    return window.oPaa.getDate('2019-09-16', formatDate);
};

window.oPaa.getSemaineDebutAnnee = function () {
    return window.oPaa.getNoSemaine('16 Sep 2019');
};

window.oPaa.getDateFin = function (formatDate) {
    return window.oPaa.getDate($('#cbo2Dates').data('daterangepicker').endDate, formatDate);
};

window.oPaa.getSemaineFin = function () {
    return window.oPaa.getNoSemaine($('#cbo2Dates').data('daterangepicker').endDate);
};

window.oPaa.getDateFinAnnee = function (formatDate) {
    return window.oPaa.getDate('2020-09-20', formatDate);
};

window.oPaa.getSemaineFinAnnee = function () {
    return window.oPaa.getNoSemaine('20 Sep 2020');
};

//MG Ajout 20200518 Début
window.oPaa.getDatesBetween = function (startDate, stopDate, formatDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push(moment(currentDate).format(formatDate));
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
};
//MG Ajout 20200518 Fin