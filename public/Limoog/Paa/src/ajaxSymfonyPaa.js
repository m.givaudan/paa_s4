
// Enregistrer un paramètre en AJAX
function ajaxSetParametre(psNomParamètre, pvValeur, psType, callbackSuccess, callbackError) {
    var lsURL = getBaseURL() + "setParametre/" + psNomParamètre + "/" + pvValeur + "/" + psType;
    $.ajax({
        type: 'POST',
        url: lsURL,
//				dataType : 'json',
        success: function (data) {
            if (callbackSuccess) {
                callbackSuccess("Enregistrement OK : " + data);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // Display error
            if (callbackError) {
                callbackError("Enregistrement KO : " + errorThrown);
            }
        }
    });
}

//MG Ajout Début 20200113
function setVariableDeSession(psNomParamètre, pvValeur, psType, callbackSuccess, callbackError) {
// LG 20200720: double slash enlevé     var lsURL = getBaseURL() + "/setVariableDeSession/" + psNomParamètre + "/" + pvValeur + "/" + psType ;
    var lsURL = getBaseURL() + "setVariableDeSession/" + psNomParamètre + "/" + pvValeur + "/" + psType ;
    $.ajax({
        type: 'POST',
        url: lsURL,
        dataType: 'json',
        success: function (data) {
            if (callbackSuccess) {
                callbackSuccess("Enregistrement OK : " + data);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // Display error
            if (callbackError) {
                callbackError("Enregistrement KO : " + errorThrown);
            }
        }
    });
}
//MG Ajout Fin 20200113
