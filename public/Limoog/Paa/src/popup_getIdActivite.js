/* 
 * YC 2019/05/04
 * Procédures javascript pour afficher les form Symfony sous forme de fenêtre popup
 */
window.Popup_Défini = true;	// Pour vérification dans les scripts qui utilisent ce qui est défini ici

function newPopup_getIdActivite(psURL, psTitre, pfOnOk) {
    var loPopup = {};
    var thisObject = loPopup;

    // Affichage de boite de dialogue, selon
    // Selon https://jqueryui.com/dialog/#modal-form
    // doc : https://api.jqueryui.com/dialog/
    // http://jsfiddle.net/GF7KF/4/
    loPopup.popup = function (callback) {
        if (!$("#dialog-form").length) {
            // Il faut créer la div de base
            $("body").append('<div id="dialog-form" style="display:none;"></div>');
        }

        $("#dialog-form").load(psURL, private_OuvreDialog);
        function private_OuvreDialog(response, status, xhr) {
            if (xhr.status === 0) {
                alert("Erreur lors du chargement");
                return;
            }
            if (xhr.status === 500) {
                $("#dialog-form").html(response);
            }
//MG Modification 20200928 Début
			if (estHtml_DemandeLogin(dialog.html())){
				var lsHTMLDialogue = "<div id='loginPopup' title='Déconnexion suite à une inactivité'>"
											+ "<p>" + dialog.html() + "</p>"
											+ "</div>";
				$(lsHTMLDialogue).dialog({
					modal:true,
					width: 400, 
					height: 600});

				$('.ui-dialog-titlebar-close').hide();
				$('form').append('<input type="text" id="authentificationParPopup" name="_authentificationParPopup" value="_authentificationParPopup" disabled="" hidden="">');
				$('#authentificationParPopup').val('_authentificationParPopup');
				$('.cadreIntérieur > form').submit(function (event) {
					event.preventDefault();
					lsURL = getBaseURL() + 'login_check';
					$.post(
							lsURL,
							$(this).serialize(),
							)
							.done(function (msg) {
								if (msg) {
										if (!estHtml_DemandeLogin(msg)) {
										$('#loginPopup').dialog("close");
										$('#loginPopup').remove();
										isLogout = false;
										$('#dialog-form').html(msg);
										$('#dialog-form').dialog("open");
									} else {
										if (typeof estModeDev === "function" && estModeDev()) {
											if (typeof LG_Ajax_Notifie === "function") {
												LG_Ajax_Notifie("Echec de l'authentification, la page reçue était : " + msg);
											}
										}
										$("#msgDuLayout").text("Echec de l'authentification");
									}

								}
							})
							.fail(function (xhr, status, error) {
								if (typeof LG_Ajax_Notifie === "function") {
									LG_Ajax_Notifie("La vérification du login a échoué : " + error);
								}
								alert('Echec de la vérification du login : ' + error);
							});
				});															
			} else {
				// Afficher le form dans la boite de dialogue
				dialog.dialog("open");
			}
//MG Modification 20200928 Fin

            // Initialiser l'objet entityForm

            // Afficher le form dans la boite de dialogue
//            dialog.dialog("open");

            $('#getIDActiviteDataTable tbody').on('dblclick', 'tr', function () {
                // Double-clic sur une ligne de la liste es activités : poser la séance
                private_PoserSéance();
            });

        }

//        var dialogOptions = {height: "auto", width: "80%"};
        var dialogOptions = {height: "auto", width: "850px"};
        var dialog = $("#dialog-form").dialog({
            title: psTitre,
            autoOpen: false,
            height: dialogOptions.height, //400
            width: dialogOptions.width, //350
            modal: true,
            buttons: {
                "Poser la séance":
                        /*function () {
                         // Récupération des valeurs du formulaire 
                         var PFormGetActivité = new Object();
                         //console.log($('#date'));
                         PFormGetActivité.date = $('#widgetDébutFin_Date').val();
                         PFormGetActivité.Tdeb = $('#widgetDébutFin_Début').val();
                         PFormGetActivité.Tfin = $('#widgetDébutFin_Fin').val();
                         PFormGetActivité.PlstRes = $('#res')[0].children[0].value;
                         PFormGetActivité.groupeActi = $('#OptionListe_GroupeActi_value')[0].children[0].value;
                         PFormGetActivité.InnactiGen = document.getElementById("fEntityForm_activites_IncActGener").checked
                         PFormGetActivité.UniRessConce = document.getElementById("fEntityForm_activites_UniPourLaRessConce").checked
                         PFormGetActivité.IncActSys = document.getElementById("fEntityForm_activites_IncLesActSyse").checked
                         PFormGetActivité.param = $('#OptionListe_Param_value')[0].children[0].value;
                         PFormGetActivité.iActi = window.entityFormGrille.RtvIdSelectedRow();
                         
                         // definition des UL qui contien les onglet 
                         var SousOngletGetActivites = $('#SSongletActifGetActivites');
                         var OngletGetActivites = $('#ongletActifGetActivites');
                         
                         // Sous-onglet actif
                         for(i = 0 ; i < SousOngletGetActivites[0].children.length;i++){
                         // recuperation du sous onglet Actif 
                         if(SousOngletGetActivites[0].children[i].className == "active"){
                         PFormGetActivité.SSOngletActif = SousOngletGetActivites[0].children[i].children[0].id;
                         }
                         }
                         
                         // Onglet actif
                         for(i = 0 ; i < OngletGetActivites[0].children.length;i++) {
                         if(OngletGetActivites[0].children[i].className == "active"){
                         PFormGetActivité.OngletActif = OngletGetActivites[0].children[i].children[0].id;
                         }
                         }
                         
                         // Appeller de la fonction donnée en paramétre
                         callback(PFormGetActivité);
                         dialog.dialog("close");
                         
                         }*/private_PoserSéance,
                annuler: function () {
                    dialog.dialog("close");
                }
//                autre: function(){}
            },
            beforeClose: function (e, ui) {

            },
            close: function (e, o) {
                // Vider tout pour restaurer les options qui ont pu être modifiées à l'utilisation
                callback();
                dialog.dialog('destroy');
            }
        });

        function private_CloseDialog() {
            dialog.dialog("close");
        }

        // Gestionnaire de l'action lors du clic sur le bouton "Poser la séance"
        function private_PoserSéance() {
            // Récupération des valeurs du formulaire 
            var PFormGetActivité = new Object();
            //console.log($('#date'));
            PFormGetActivité.date = $('#widgetDébutFin_Date').val();
            PFormGetActivité.Tdeb = $('#widgetDébutFin_Début').val();
            PFormGetActivité.Tfin = $('#widgetDébutFin_Fin').val();
            PFormGetActivité.PlstRes = $('#res')[0].children[0].value;
            PFormGetActivité.groupeActi = $('#OptionListe_GroupeActi_value')[0].children[0].value;
            PFormGetActivité.InnactiGen = document.getElementById("fEntityForm_activites_IncActGener").checked;
            PFormGetActivité.UniRessConce = document.getElementById("fEntityForm_activites_UniPourLaRessConce").checked;
            PFormGetActivité.IncActSys = document.getElementById("fEntityForm_activites_IncLesActSyse").checked;
            PFormGetActivité.param = $('#OptionListe_Param_value')[0].children[0].value;
            PFormGetActivité.iActi = window.entityFormGrille.RtvIdSelectedRow();
            PFormGetActivité.aLstResAInclure = $("#checkListRessourcesPourLaSéance").val();

            // definition des UL qui contien les onglet 
            var SousOngletGetActivites = $('#SSongletActifGetActivites');
            var OngletGetActivites = $('#ongletActifGetActivites');

            // Sous-onglet actif
            PFormGetActivité.SSOngletActif = "" ;
            for (i = 0; i < SousOngletGetActivites[0].children.length; i++) {
                // recuperation du sous onglet Actif 
                if ($(SousOngletGetActivites[0].children[i].children[0]).hasClass("active")) {
                    PFormGetActivité.SSOngletActif = SousOngletGetActivites[0].children[i].children[0].id;
                }
            }

            // Onglet actif
            PFormGetActivité.OngletActif = "" ;
            for (i = 0; i < OngletGetActivites[0].children.length; i++) {
                if ($(OngletGetActivites[0].children[i].children[0]).hasClass("active")) {
                    PFormGetActivité.OngletActif = OngletGetActivites[0].children[i].children[0].id;
                }
            }

            // Appeller de la fonction donnée en paramétre
            callback(PFormGetActivité);
            dialog.dialog("close");

        }
    };

    return loPopup;
}

