// Ressources.js
// LG 20170117

// LG 20190417 essai
//var lsBasePath = "../../../" ;
//includeJS(lsBasePath + '/Limoog/Partages/widgets/LGMultiselect/LGMultiselect.js') ;

// Retrouver le nom d'un type de ressource
function rtvNomTypeRessource(pcTypeRes, pbAvecPronom) {
//	if (pcTypeRes == "I") return "Intervenant" ;
//	else if (pcTypeRes == "U") return "Usager" ;
//	else if (pcTypeRes == "G") return "Groupe" ;
//	else if (pcTypeRes == "S") return "Equipement" ;
//	else if (pcTypeRes == "E") return "Etablissement" ;
//	else return pcTypeRes ;
    var lsNom, lsPronom
    if (pcTypeRes == "I") {
        lsNom = "Intervenant";
        lsPronom = "un";
    } else if (pcTypeRes == "U") {
        lsNom = "Usager";
        lsPronom = "un";
    } else if (pcTypeRes == "G") {
        lsNom = "Groupe";
        lsPronom = "un";
    } else if (pcTypeRes == "S") {
        lsNom = "Equipement";
        lsPronom = "un";
    } else if (pcTypeRes == "E") {
        lsNom = "Etablissement";
        lsPronom = "un";
    } else if (pcTypeRes == "@Tous@") {
        lsNom = "Ressource";
        lsPronom = "une";
    } else {
        lsNom = pcTypeRes;
    }

    if (pbAvecPronom)
        return lsPronom + " " + lsNom.toLowerCase();
    else
        return lsNom;
}

// -----------------------------------------------------------
// Remplissage d'une cbcRessource avec un jeu de données data
// select	: selecteur jQuery pour la cbc à remplir
// data		: objet contenant le tableau des données (cTypeRes, iIdRes, cNomCourt)
//			: ou objet contenant deux sous-objets : 
//				* "Compétentes" = le tableau des données
//				* "Participantes" = le tableau des sélectionnées (cTypeRes, iIdRes)
// paVal	: le tableau des valeurs à affecter à la cbc une fois qu'elle sera remplie
function fillCbcRessource(select, data, paVal) {
    // Remplir
    var lcTypeResCbc = select.dataset.typeRes;
    if (!lcTypeResCbc)
        lcTypeResCbc = "@Tous@";
    var llTtesRessources = (lcTypeResCbc == "@Tous@");
    var llTtesRessources = (lcTypeResCbc.length > 1);
    var lcTypeRes_Old = lcTypeResCbc;
    var lc = '';
    var lcId;
    var laVal = [];
	
    if (data.Participantes) {
        // Les données reçues contiennent la liste à afficher et la liste à sélectionner
//MG Modifié 20191209
        if (!paVal)
            paVal = [];
        for (var i = 0; i < data.Participantes.length; i++) {
            var res = data.Participantes[i];
            var lsType_Res, liId_Res;
            if (res.cType_Res)
                lsType_Res = res.cType_Res;

// Si la ressource n'a pas de typeRes, elle n'etait pas coché
            if (res.cTypeRes)
                lsType_Res = /*""*/ res.cTypeRes;
            if (res.iId_Res)
                liId_Res = res.iId_Res;
            if (res.iIdRes)
                liId_Res = res.iIdRes;
            paVal.push(lsType_Res + liId_Res);
        }
    } else {
//MG Modification 20200813 Début
//Dans le général, pour garder les ressources selectionnées lors d'un changement d'acti de base
		paVal = $('#cbcRessourcesActi').val();
//MG Modification 20200813 Fin
	}
	
    if (data.Compétentes) {
        data = data.Compétentes;
    }
    for (var i = 0; i < data.length; i++) {
        if (data[i].cNomCourt === "Erreur de chargement de la liste") {
            lc += '<optgroup label="Erreur de chargement de la liste">';
            break;
        }
        if (llTtesRessources && data[i].cTypeRes !== lcTypeRes_Old) {
            // On change de type de ressource
            lc += '<optgroup label="' + rtvNomTypeRessource(data[i].cTypeRes) + 's">';
            lcTypeRes_Old = data[i].cTypeRes;
        }
// MG Modifié 20191209
// Si la ressource n'a pas de typeRes, elle n'est pas coché
        lcId = data[i].cTypeRes + data[i].iIdRes;
        lc += '<option value="' + lcId + '">'
                + data[i].cNomCourt + '</option>';
        if (paVal && paVal.find(function (pcId) {
//                    if (lcId == "I1000107" || pcId == "I1000107") {
//                    }
            var lb = (pcId == lcId);
            return lb;
        })) {
            laVal.push(lcId);
        }
    }
    $(select).html(lc);

    // Reconstruire la cbc
    if (lc) {
        // Il y a des items
// LG 20190408 old		$(select).multiselect('setOptions', {nonSelectedText: 'Sélectionnez une ressource'});
// LG 20190926 début
//		$(select).multiselect('setOptions', {nonSelectedText: 'Sélectionnez ' + rtvNomTypeRessource(select.dataset.typeRes, true)});
        $(select).RessourceWidget();
// LG 20190926 fin
        $(select).val(laVal);
    } else {
        $(select).multiselect('setOptions', {nonSelectedText: 'Chargement de la liste ...'});
    }
    $(select).multiselect('rebuild');
    // Test de la méthode SortByCheck $(select).multiselect('sortByCheck') ;
}

// poCbc		: la cbc à remplir
// paVal		: le tableau des valeurs à affecter à la cbc une fois qu'elle sera remplie
//(paOptions)	: objet d'options de remplissage (propriétés : typeRes, debut, fin, acti, actibase)
function ajax_FillcbcRessources(poCbc, paVal, poOptions) {
    if (!poCbc) {
        // poCbc non fourni
        console.log("poCbc non fourni");
        return;
    }
	
	var liActi;
    if (poOptions) {
        // On a fourni un tableau d'options
        if (poOptions.hasOwnProperty('typeRes'))
            poCbc.dataset.typeRes = poOptions.typeRes;
        if (poOptions.hasOwnProperty('debut'))
            poCbc.dataset.debut = poOptions.debut;
        if (poOptions.hasOwnProperty('fin'))
            poCbc.dataset.fin = poOptions.fin;
        if (poOptions.hasOwnProperty('actibase'))
            poCbc.dataset.actibase = poOptions.actibase;
        if (poOptions.hasOwnProperty('acti'))
            poCbc.dataset.acti = poOptions.acti;
		//MG Modification 20200806 Début
		if(poOptions.hasOwnProperty('noActi')){
			liActi = null;
		} else {
			var liActi = poCbc.dataset.acti;			// activité pour ne garder que les ressources compétentes
		} 
	//	var liActi = poCbc.dataset.acti;
		//MG Modification 20200806 Fin
    }
	
    var lcTypeRes = poCbc.dataset.typeRes;			// cf propriété HTML data-typeRes définie dans la balise de l'élément SELECT
    var lcDebut = poCbc.dataset.debut;				// cf propriété HTML data-debut définie dans la balise de l'élément SELECT
    var lcFin = poCbc.dataset.fin;
    var liActiBase = poCbc.dataset.actibase;		// activité pour ne garder que les ressources compétentes
    var lsURL = getBaseURL();
    lsURL = lsURL + "getRessources";
    $.ajax({
        type: 'GET',
        // NB : la gestion de l'autorisation Cross-Origin est faite dans ressources.php
        url: lsURL,
        dataType: 'json',
        data: {
            cType_Res: lcTypeRes
            , vDebut: lcDebut
            , vFin: lcFin
            , iActiBase: liActiBase
            , iActi: liActi
        },
        success: function (data) {
            // Remplir la cbc
            fillCbcRessource(poCbc, data, paVal);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // Display error
            data = [{iIdRes: 0, cTypeRes: "", cNomCourt: "Erreur de chargement de la liste"}];
            fillCbcRessource(poCbc, data);
            return false;
        }

    });
}
//MG Ajout 20200803 Début
function ajax_FillActibase(actiBaseSelect, listeRessources){
	listeRessources = listeRessources.toString();
	
	var lsURL = getBaseURL();
	lsURL = lsURL + "getCompetencesCommunes/"+listeRessources;
	
	$.ajax({
		type: 'GET',
		url: lsURL,
		dataType: 'json',
		success: function (data) {
            // Remplir l'actibase
            fillActibase(actiBaseSelect, data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
			data = [{iid_actibase: 0, cnom: "Erreur de chargement de la liste", isousgroupe: 0}];
			fillActibase(actiBaseSelect, data);
			return false;
        }
		
	});
}

// data		: objet contenant le tableau des données
function fillActibase (select, data){
	var options = "";
	var selectedOption = $(select).find(":selected");
	var iid_seanceSelected = selectedOption.attr("value");
	var cnomSelected = selectedOption.text();
	if(iid_seanceSelected === undefined ){
		options += "<option value='' selected disabled>Choisissez une activité de base</options>";
	} else if (selectedOption  && iid_seanceSelected){
		options += "<option value="+iid_seanceSelected+" selected='selected'>"+cnomSelected+"</option>";
	}
	for (var i=0; i<data.length; i++){
		if(data[i].iid_actibase != iid_seanceSelected){
			options += "<option value="+data[i].iid_actibase+">"+data[i].cnom+"</option>";
		}
	}
//	if(!options.includes(iid_seanceSelected)){
//		options += "<option value="+iid_seanceSelected+" selected='selected'>"+cnomSelected+"</option>";
//	}
	$(select).html(options);
}
//MG Ajout 20200803 Fin

//Modif Début MG 20191203
function initcombo() {
    if (!$().RessourceWidget) {
        // La méthode RessourceWidget n'est pas disponible
        if ($("[data-role=cbcRessource]").length || $("[data-role=checkListRessource]").length) {
            // Il y a des combocheck ressource
//			throw "Il y a des combocheck ressource mais la méthode RessourceWidget n'est pas disponible" ;
            console.error("Il y a des combocheck ressource mais la méthode RessourceWidget n'est pas disponible");
        }
        // La méthode RessourceWidget n'est pas utile ici
        return;
    }
    $("[data-role=cbcRessource]").RessourceWidget("comboCheck");
    $("[data-role=checkListRessource]").RessourceWidget("checkList");
}
;
//Modif Fin MG 20191203

// Classe cbcRessources, basée sur Bootstrap Multiselect (http://davidstutz.github.io/bootstrap-multiselect/)
// Doit inclure les fichiers correspondants
!function ($) {

    // -----------------------------------------------------------
    // Commandes de définition d'une combocheck Ressources
    // HTML : cbcRessources.html.twig (D:\Luc\Dev\DevWeb\Symfony\applicationPAA\src\PaaBundle\Resources\views\Widgets\)
    /*
     <select id="cbcRes_Toutes"
     multiple="multiple"
     data-role="cbcRessource"
     data-type-res="@Tous@"
     data-debut="2015-05-01"
     data-fin="2015-05-01"
     style="height: 34px;		// Force la hauteur pour que la dimension de la cbc soit correcte avant chargement
     width: 250px;"		// Largeur forcée de la cbc
     >
     Le contenu de cette combo est changé PAR AJAX, de part son data-role
     </select>
     */
//    $.fn.cbcRessource = function() {
    // psType : "comboCheck" (Dft) ou "checkList"
    $.fn.RessourceWidget = function (psType) {
// LG 20190926 début
//        if (!psType) psType = "comboCheck" ;
        if (psType) {
        } else if ($(this).attr("data-role")) {
            psType = $(this).attr("data-role");
        } else {
            psType = "comboCheck";
        }
// LG 20190926 fin
        return this.each(function (o) {
// LG 20190529 début
            if ($(this).attr("data-isMultiSelectInitialized")) {
                // Ce ultiselect est déja initialisé : ne pas le retraiter
                // sinon il est ajouté une seconde fois à l'écran
                return;
            }
// LG 20190529 fin

            var thisCbc = this;
            var lcTypeRes = thisCbc.dataset.typeRes;	// cf propriété HTML data-typeRes définie dans la balise de l'élément SELECT
//			var lcDebut = thisCbc.dataset.debut ;		// cf propriété HTML data-debut définie dans la balise de l'élément SELECT
//			var lcFin = thisCbc.dataset.fin ;
            var lcWidth = thisCbc.style.width;
            var lbDisableFiltering = (thisCbc.dataset.disableFiltering && thisCbc.dataset.disableFiltering.toLowerCase()) === "true";

            // Définir les options
// LG 20190926 old			var lsNonSelectedText = 'Chargement de la liste ...' ;
            var lsNonSelectedText = 'Sélectionnez ' + rtvNomTypeRessource(lcTypeRes, true);
            var laOptions = {enableCollapsibleOptGroups: (lcTypeRes.length > 1)/*(lcTypeRes == "@Tous@")*/
                , enableClickableOptGroups: false
                , enableFiltering: !lbDisableFiltering
                , enableCaseInsensitiveFiltering: !lbDisableFiltering
                , maxHeight: 300
                , numberDisplayed: 5
                , buttonWidth: lcWidth
                , buttonClass: 'btn bg-white btn-default LG-leftAlign'
                , nonSelectedText: lsNonSelectedText
                , onChange: function (option, checked, select) {
                    this.lModifié = true;
                    var event = jQuery.Event("onChangecbcRessource");
                    event.source = $(thisCbc)[0].id;
                    $("body").trigger(event);
                }
                , onDropdownShown: function (event) {
                    this.cOldValue = this.$select.val();
                    this.lModifié = false;
                }
                , onDropdownHide: function (event) {
                    // Ne semble pas fonctionner
                    if (!this.lModifié) {
                    }
                    // Sudhakar R, http://stackoverflow.com/questions/1773069/using-jquery-to-compare-two-arrays-of-javascript-objects
                    else if ($(this.$select.val()).not(this.cOldValue).length === 0 && $(this.cOldValue).not(this.$select.val()).length === 0) {
                    } else {
                        // Il y a eu modification
                        var event = jQuery.Event("onValideChangecbcRessource");
                        event.source = $(thisCbc)[0].id;
                        event.aLstRes = this.$select.val();
                        $("body").trigger(event);
                    }
                }
//									, onInitialized: function(select, container) {
//									}
            };

            // Créer la comboCheck
// LG 20190926 début
//			if (psType === "comboCheck") $(thisCbc).LGMutiselectCombo(laOptions) ;
//			if (psType === "checkList") $(thisCbc).LGMutiselectList(laOptions) ;
            if (psType.substring(0, 10) === "comboCheck")
                $(thisCbc).LGMutiselectCombo(laOptions);
            if (psType.substring(0, 9) === "checkList")
                $(thisCbc).LGMutiselectList(laOptions);
// LG 20190926 fin

//			$lsURL = dirname(window.location.href) + "/getRessources" ;
//			$.ajax({
//				type : 'GET',
//				// NB : la gestion de l'autorisation Cross-Origin est faite dans ressources.php
//				// url : 'http://www.limoogWebService.localhost.net/PAA/ressources.php?' ,
//				//url : '/getRessources' ,
//				url: $lsURL ,
//				dataType : 'json',
//				data: {
//					cType_Res: lcTypeRes
//					, vDebut: lcDebut
//					, vFin: lcFin
//				},
//				success : function(data){
//					// Remplir la cbc
//					fillCbcRessource(thisCbc, data) ;
//				},
//				error : function(XMLHttpRequest, textStatus, errorThrown) {
//					// Display error
//					data = [{iidRes: 0, ctypeRes: "", cnomcourt: "Erreur de chargement de la liste"}]
//					fillCbcRessource(thisCbc, data) ;
//					return false;
//				}
//
//			});
// LG 20190418 old			var laVal = $("[data-role=cbcRessource]").val() ;
            if (!thisCbc.dataset.noload) {
                var laVal = $(thisCbc).val();
                ajax_FillcbcRessources(thisCbc, laVal);
            }
// LG 20190529 début : 
            // Marquer ce multiselect comme initialisé
            $(this).attr("data-isMultiSelectInitialized", true);
// LG 20190529 fin
        });

    };

    //MG ajout Début 20191203
    $(function () {
        initcombo();
    });
    //MG ajout Fin 20191203
}(window.jQuery);
