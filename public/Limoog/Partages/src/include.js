// ---------------------------------------------------
// Inclure facilement des fichiers .js et/ou css
// VOIR AUSSI http://sametmax.com/include-require-import-en-javascript

// A noter : le script ci-dessous est à placer avant l'include de ce INCLUDE.JS
// <script type="text/javascript" src="../../Partages/src/include.js"></script>
// Créer la variable window.cPathJavascript
// Qui permet à include.js de connaitre le chemin des scripts
/*
 !function () {
 var e = p(p(p(window.location.href))) ;
 window.cPathJavascript = e ;
 function p(e){
 e = e.split('/') ;           //break the string into an array
 e.pop() ;                    //remove its last element
 return e.join('/') ;
 }
 }();
 */

function isIncluded(fileName) {
    if (!window.includedFiles)
        window.includedFiles = [];
    if (window.includedFiles.indexOf(fileName) > -1)
        return true;
    else {
        window.includedFiles.push(fileName);
        return false;
    }
}

// fileName				: le chemin du fichier JS à inclure
//(callbackWhenReady)	: fonction callback à exécuter quand le JS a été chargé
function includeJS(fileName, callbackWhenReady) {
    fileName = include_absolutePath(fileName);
    if (isIncluded(fileName))
        return;
// LG 20190409 début
// 	document.write("<script type='text/javascript' src='" + fileName + "'></script>");
    if (!callbackWhenReady) {
        // Version originale éprouvée
        document.write("<script type='text/javascript' src='" + fileName + "'></script>");
    } else {
        // Une fonction callback a été fournie
        // Selon https://stackoverflow.com/questions/14521108/dynamically-load-js-inside-js
        var script = document.createElement('script');
        script.onload = callbackWhenReady;
        script.src = fileName;
        document.head.appendChild(script);
    }
// LG 20190409 fin
}

// ---------------------------------------------------
// Inclure facilement des fichiers .css
function includeCSS(fileName) {
    fileName = include_absolutePath(fileName);
    if (isIncluded(fileName))
        return;
    document.write('<link rel="stylesheet" href="' + fileName + '">');
}

function include_absolutePath(href) {
    if (window.cPathJavascript) {
        // Le chemin des javascripts a été fourni
// Modifié pour Symfony début
//		href = window.cPathJavascript + "/Partages/" + href ;
//		var link = document.createElement("a");
//		link.href = href;
//		return (link.protocol+"//"+link.host+link.pathname+link.search+link.hash);
        return window.cPathJavascript + href;
// Modifié pour Symfony fin
    } else {
        return  href;
    }
}

function includeHTML(psFich, psIdDiv, callback) {
    if (!document.getElementById(psIdDiv)) {
        console.log("includeHTML : " + psIdDiv + " n'est pas l'Id d'un élément existant.");
        return;
    }
    window.jQuery.ajax({
        type: 'GET',
        url: psFich,
        dataType: 'text',
        success: function (data) {
            document.getElementById(psIdDiv).innerHTML = data;
            if (callback)
                callback(psIdDiv, true, data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // Display error
            document.getElementById(psIdDiv).innerHTML = "erreur dans la récupération de " + psFich + " : " + errorThrown;
            if (callback)
                callback(psIdDiv, false, errorThrown);
            return false;
        }
    });
}
