/* 
 * Classe de base pour la gestion des Data
 */

var cDataBase = Class.extend({
    cClassName: "cDataBase",
    oInitial: null, // objet contenant les données initialement chargées
    oActuel: null, // objet contenant les données actuelles, incluant les modifications par l'utilisateur
    lModifié: false, // devriendra true quand les données seront modifiées
    charge: function (psJson) {
        if (!psJson) {
            // Json vide
            return;
        }
        if (LG_Ajax_GetText_EstErreur(psJson)) {
            this.oInitial = null;
            this.oActuel = null;
            this.cLastErreur = "Impossible de charger les données : " + psJson;
            return false;
        } else {
            // Charger les données avec JSON
            this.oInitial = JSON.parse(psJson);
            this.oActuel = JSON.parse(psJson);

            this.lModifié = false;

            return true;
        }
    },
    setModifie: function (pbModifie) {
        window.oData.lModifie = pbModifie;

// Temporaire en attendant la nouvelle version de l'exe à Freyming
        var lsNomDiv = "LG_Ajax_DivEchangesWebBrowser_lModifié";
        if (document.getElementById(lsNomDiv)) {
            document.getElementById(lsNomDiv).innerHTML = pbModifie;
        }

    },
    enregistre: function () {
        var lbResult = LG_Ajax_GetText('enregistre');
        if (lbResult) {
            window.oData.oInitial = window.oData.oActuel;
            window.oData.lModifie = false;
        }
        return lbResult;
    },

    cLastErreur: "", // dernière erreur ayant eu lieu
    setLastErreur: function (pcErreur) {
        if (!pcErreur)
            return;
        this.cLastErreur = pcErreur;
        this.oActuel.Erreurs += pcErreur;
    },

    getModeExecution: function () {
        return this.oActuel.nModeExecution;
    },
    setModeExecution: function (piModeExecution) {
        this.oActuel.nModeExecution = piModeExecution;
    },

})

