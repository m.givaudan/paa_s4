/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//-----------------------------------------------------------------------------------------
// Convertir une valeur en littéral VFP
function VFP_ToLitteral(pvValeur) {
    if (typeof pvValeur == "undefined") {
        console.log("VFP_ToLitteral : pvValeur = undefined");
        return "null";
    }
    if (pvValeur === null)
        return "null";
    if (typeof pvValeur == "string" && isDate(pvValeur)) {
        // Date
        var ldDate = new Date(pvValeur);

        return '{^' + ldDate.getFullYear()
                + "/" + (getMonth(ldDate, true))
                + "/" + padLeft(ldDate.getDate(), 2, '0') + '}';
    }
    if ((typeof pvValeur == "string" && isDateTime(pvValeur))
            || (pvValeur.getMonth)) {
        // DateTime
        var ldDate = new Date(pvValeur);
        return '{^' + ldDate.getFullYear() + "/" + (getMonth(ldDate, true)) + "/" + padLeft(ldDate.getDate(), 2, '0')
                + " " + padLeft(ldDate.getHours(), 2, '0') + ":" + padLeft(ldDate.getMinutes(), 2, '0') + ":" + padLeft(ldDate.getSeconds(), 2, '0')
                + '}';
    } else if (typeof pvValeur == "string")
        return '"' + pvValeur + '"';
    else if (typeof pvValeur == "number")
        return pvValeur;
    else if (typeof pvValeur == "boolean" && pvValeur)
        return ".T.";
    else if (typeof pvValeur == "boolean" && !pvValeur)
        return ".F.";
    else if (false)
        return "Bidon";
    else
        return '"' + pvValeur + " (" + typeof pvValeur + ")" + '"';
}
