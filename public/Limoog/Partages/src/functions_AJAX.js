
// LG 20180724 old function LG_Ajax_Notifie(psMessage, psTypeMsg) {
function LG_Ajax_Notifie(psMessage, psTypeMsg, callback, psIdLog) {
// LG 20151016 : fonctionne parfaitement bien en cas standard, 
// mais quand le thread VFP est à 100% de CPU, l'événement WebBrowser BeforeNavigate2 ne se déclanche pas
// (de même que d'autres événements comme NewWindow2 et NewWindow3)
// A ce moment, il vaut mieux passer par un timer coté VFP, qui va lire un flag dans la page

    psTypeMsg = psTypeMsg ? psTypeMsg : "Message";
    // Enlever les sauts de ligne
    psMessage = psTypeMsg + " : " + psMessage.replace(/(\r\n|\n|\r)/gm, "@Chr(13)@");
// LG 20200914 début : fournir les données directement sous forme d'objet
//    var lsData = '[{"nom":"Message", "valeur":"' + psMessage + '"}' ;
//    if (psIdLog) {
//         lsData += ', {"nom":"idLog", "valeur":"' + psIdLog + '"}' ;
//    }
//    lsData += ']' ;
//    LG_Ajax_GetText("OnNotifie", lsData, "", "", callback);
    
    var loData = [] ;
    loData.push({"nom": "Message", "valeur": psMessage}) ;
    if (psIdLog) {
         loData.push({"nom": "idLog", "valeur": psIdLog}) ;
    }
    LG_Ajax_GetText("OnNotifie", loData, "", "", callback);
// LG 20200914 fin

// lsURL = "about::blank" ;
// var oPopUp = window.open(lsURL,"nom_popup","menubar=no, status=no, scrollbars=no, menubar=no, width=200, height=100");
// oPopUp.close() ;

    return true;
}
function LG_Ajax_Notifie_callback(psResponse) {
    // Retour de la notification asynchrone d'ure erreur : RAS
}

//-----------------------------------------------------------------------------------------
// Récupérer le texte d'un fichier
// psStemURL	: nom de fichier distant contenant le texte à récupérer, ou chargé de sa génération
//(psParaJSON)	: paramètres à fournir à ce fichier (format JSON)
//(psPathURL)	: partie chemin de l'URL du fichier distant
//(psExtURL)	: partie extension de fichier du fichier distant
// Exemple : LG_Ajax_GetText('GetData', '[{"nom":"p1","valeur":"para1"},{"nom":"p2","valeur":"para2"},{"nom":"p3","valeur":3}]', 'http://www.limoog.net/tmp/', 'json') ;
function LG_Ajax_GetText(psStemURL, psParaJSON, psPathURL, psExtURL, callback) {

// alert("LG_Ajax_GetText : " + window.lWebBrowserVFPControl) ;
// return "" ;

    if (window.lWebBrowserVFPControl) {
        // Mode fonctionnement webbrowser au sein d'une application cliente lourde
        // NB : le webBrowser ne permet pas l'exécution de Microsoft.XMLHTTP
        // car ces requêtes ne sont pas interceptées par BeforeNavigate2
        // -> On doit simuler une vraie navigation, avec une URL que VFP interprétera comme une demande AJAX
        // et il remplira la balise LG_Ajax_DivEchangesWebBrowser avec le texte du résultat de l'appel

        // NB : le flag "lWebBrowserVFPControl" est posé à .T. par le webBrowser au chargement de la page
        // lors de l'événement DownloadComplete

        return LG_Ajax_GetText_WebBrowser(psStemURL, psParaJSON, psPathURL, psExtURL, callback);
    } else {
        // Cas d'une navigation avec un client léger
// alert("LG_Ajax_GetText_Navigateur") ;
        return LG_Ajax_GetText_Navigateur(psStemURL, psParaJSON, psPathURL, psExtURL, callback);
    }
}

//-----------------------------------------------------------------------------------------
// Récupérer le nom de la div qui sert d'échanges d'informations avec le client webbrowser
// (si elle n'existe pas, cette div est créée)
// psNomData		: nom de la donnée que doit contenir cette div
// (psVisibility)	: visibilité ("hidden : Dft, "visible")
function LG_Ajax_GetNomDivEWB(psNomData, psVisibility) {

// alert("LG_Ajax_GetNomDivEWB : " + psNomData) ;

    // Ne pas modifier, pour rester compatible avec fonction VFP AJAX_GetNomDivEchangesWebBrowser()
    var lsNomDiv = "LG_Ajax_DivEchangesWebBrowser_" + psNomData;

    // Créer la div fictive si elle n'existe pas encore
    if (!document) {
        // Le document n'existe pas encore
        lsNomDiv = "";
    } else if (!document.body) {
        // Le body n'existe pas encore
        lsNomDiv = "";
    } else if (!(document.getElementById(lsNomDiv))) {
        // La div de destination du texte n'existe pas
        // La créer maintenant
        var loDiv = document.createElement("div");
        loDiv.id = lsNomDiv;
        loDiv.style.visibility = "hidden";
        document.body.appendChild(loDiv);
    }
    if (lsNomDiv) {
        if (!psVisibility) {
            psVisibility = "hidden";
        }
        document.getElementById(lsNomDiv).style.visibility = psVisibility;
    }

    return lsNomDiv;
}

//-----------------------------------------------------------------------------------------
// Récupérer la valeur de la constante qui signifie "La fonction LG_Ajax_GetText n'a rien renvoyé"
// Pour gestion du bug WebBrowser où BeforeNavigate2 n'est pas appellé
function LG_Ajax_GetText_RtvCst_NR() {
    return 	"@LG_Ajax_GetText_NonRempli@";
}

//-----------------------------------------------------------------------------------------
// Récupérer le texte d'un fichier, dans un contexte webbrowser au sein d'une application cliente lourde
// psStemURL	: nom de fichier distant contenant le texte à récupérer, ou chargé de sa génération
//(psParaJSON)	: paramètres à fournir à ce fichier (format JSON)
//(psPathURL)	: partie chemin de l'URL du fichier distant
//(psExtURL)	: partie extension de fichier du fichier distant
// Exemple : LG_Ajax_GetText('GetData', '[{"nom":"p1","valeur":"para1"},{"nom":"p2","valeur":"para2"},{"nom":"p3","valeur":3}]', 'http://www.limoog.net/tmp/', 'json') ;
function LG_Ajax_GetText_WebBrowser(psStemURL, psParaJSON, psPathURL, psExtURL, callback) {

    // Créer la div fictive LG_Ajax_DivEchangesWebBrowser pour réception du texte
    /*
     if (!(document.getElementById("LG_Ajax_GetText_Div"))) {
     // La div de destination du texte n'existe pas
     // La créer maintenant
     var loDiv = document.createElement("div");
     loDiv.id = "LG_Ajax_GetText_Div";
     loDiv.style.visibility = "hidden" ;
     document.body.appendChild(loDiv);
     }
     */
    var lsNomDiv = LG_Ajax_GetNomDivEWB("GetText");

    // Remplir LG_Ajax_DivEchangesWebBrowser avec une erreur pour détecter les erreurs de navigation
//    document.getElementById("LG_Ajax_GetText_Div").innerHTML = "Erreur, pas de données récupérées par le webBrowser";
    document.getElementById(lsNomDiv).innerHTML = "Erreur, pas de données récupérées par le webBrowser";

    // Générer la demande à faire
    var lsURL = 'vfps://Ajax//' + psStemURL + "(";
    if (false) {
        // Pour tests
        psParaJSON = '[{"nom":"p1","valeur":"para1"},{"nom":"p2","valeur":true},{"nom":"p3","valeur":3}]';
        var now = new Date();
        //alert(now.toLocaleString()) ;
// LG 20160930 old        now = now.getFullYear() + "/" + (now.getMonth() + 1)
        now = now.getFullYear() + "/" + (getMonth(now))
                + "/" + now.getDate()
                + " " + now.getHours()
                + ":" + now.getMinutes()
                + ":" + now.getSeconds();
        //alert(now) ;
        psParaJSON = '[{"nom":"p1","valeur":"para1"},{"nom":"p2","valeur":"' + now + '"},{"nom":"p3","valeur":3}]';
        //alert(psParaJSON) ;
    }

    var loJSON = null;
    if (!psParaJSON) {
    } else if (typeof psParaJSON === 'string' || psParaJSON instanceof String) {
        // Des paramètres ont été fournis sous forme de json string
        loJSON = JSON.parse(psParaJSON);
    } else if (typeof psParaJSON === 'object') {
        // Des paramètres ont été fournis sous forme d'objet
        loJSON = psParaJSON;
    } else {
        console.log("erreur dans les paramères");
    }

    if (loJSON) {
        var loItem, lsValeur;
        //   for (liItem in loJSON) {
        for (var liItem = 0; liItem < loJSON.length; liItem++) {
            loItem = loJSON[liItem];
            lsURL += VFP_ToLitteral(loItem.valeur);
            if (liItem < loJSON.length - 1)
                lsURL += ',';
            //alert(loItem.nom + " = " + loItem.valeur) ;
        }
    }
    lsURL += ")";

    // Demander le remplissage de la div : le webbrowser doit remplir LG_Ajax_DivEchangesWebBrowser
    var lsTexteNonRempli = LG_Ajax_GetText_RtvCst_NR();
    document.getElementById(lsNomDiv).innerHTML = lsTexteNonRempli;
//	window.navigate(lsURL) ;
    location.assign(lsURL);

    // Récupérer puis vider le contenu de la div
    lsText = document.getElementById(lsNomDiv).innerHTML;
    document.getElementById(lsNomDiv).innerHTML = "";

    if (callback) {
        callback(lsText);
        return " [Non renseigné car processus asynchrone : renvoi de la réponse à la fonction callkack]";
    } else
        return lsText;
}

//-----------------------------------------------------------------------------------------
// Récupérer le texte d'un fichier, dans un contexte webbrowser au sein d'un navigateur (client léger)
// psStemURL	: nom de fichier distant contenant le texte à récupérer, ou chargé de sa génération
//(psParaJSON)	: paramètres à fournir à ce fichier (format JSON)
//(psPathURL)	: partie chemin de l'URL du fichier distant
//(psExtURL)	: partie extension de fichier du fichier distant
//(callback)	: nom de la fonction callback éventuelle
// Exemple : LG_Ajax_GetText('GetData', '[{"nom":"p1","valeur":"para1"},{"nom":"p2","valeur":"para2"},{"nom":"p3","valeur":3}]', 'http://www.limoog.net/tmp/', 'json') ;
function LG_Ajax_GetText_Navigateur(psStemURL, psParaJSON, psPathURL, psExtURL, callback) {

    // Générer la chaine de l'URL
    if (psPathURL) {
        var lsURL = (psPathURL || '') + psStemURL + (psExtURL ? '.' + psExtURL : '') ;
    } else {
        var lsURL = getBaseURL() + psStemURL + (psExtURL ? '.' + psExtURL : '') ;
    }

    /*
     // LG 20190408 début
     if (jQuery && callback) {
     return LG_Ajax_GetText_JQuery(lsURL, psParaJSON, callback) ;
     }
     
     // -------------------------------------------------------------------
     // A partir de là, c'est obsolète
     console.warn("Obsolète") ;
     // LG 20190408 début
     */

    // Ajouter les paramètres en Get
    var loJSON = null;
    if (!psParaJSON) {
    } else if (typeof psParaJSON === 'string' || psParaJSON instanceof String) {
        // Des paramètres ont été fournis sous forme de json string
        loJSON = JSON.parse(psParaJSON);
    } else if (typeof psParaJSON === 'object') {
        // Des paramètres ont été fournis sous forme d'objet
        loJSON = psParaJSON;
    } else {
        console.log("erreur dans les paramères");
    }
    if (loJSON) {
        // Il y a des paramètres
        lsURL += "?";
        var loItem, lsValeur;
        for (var liItem = 0; liItem < loJSON.length; liItem++) {
            loItem = loJSON[liItem];
            lsURL += loItem.nom + "=" + loItem.valeur;
            if (liItem < loJSON.length - 1)
                lsURL += '&';
        }
    }

    // Préparer l'appel
    var xhr_object = null;
    var lsText = "";
    var lsErr = "";
    if (window.XMLHttpRequest) {
        // Firefox ou IE récent
        xhr_object = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        // Internet Explorer  
        xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        // XMLHttpRequest non supporté par le navigateur   
        alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest, le chargement de cette page sera incomplet...");
        lsText = "Chargement impossible : le navigateur ne supporte pas les objets XMLHTTPRequest";
    }

    var llSynchrone = true;
    if (callback) {
        llSynchrone = false;
        xhr_object.onreadystatechange = function () {
            if (xhr_object.readyState == 4 && (xhr_object.status == 200 || xhr_object.status == 0)) {
                callback(xhr_object.responseText);
            } else if (xhr_object.readyState == 4) {
                callback("Erreur : " + xhr_object.status);
            }
        };
    }

    // Effectuer l'appel
    try {
        // Synchrone ou asynchrone selon qu'on a fourni ou non une fonction callback
        xhr_object.open("GET", lsURL, !llSynchrone);
        xhr_object.send(null);
    } catch (err) {
        lsErr = ", erreur de lecture du fichier : "
                + err.message;
    }

    if (!llSynchrone) {
        // Mode asynchrone : RAS
        lsText = " [Non renseigné car processus asynchrone : renvoi de la réponse à la fonction callkack]";
// LG 20180724 old	} else if (xhr_object.readyState == 4 && lsErr == "") {
    } else if (xhr_object.readyState == 4 && lsErr == "" && (xhr_object.status == 200 || xhr_object.status == 0)) {
        lsText = xhr_object.responseText;
    } else {
        lsText = "Chargement impossible de " + (lsURL || "Null") + lsErr + " (Mode navigateur)";
    }

    return lsText;

}
/*
 //-----------------------------------------------------------------------------------------
 // Récupérer le texte d'un fichier, dans un contexte webbrowser au sein d'un navigateur (client léger)
 // psURL		: URL du fichier distant
 //(psParaJSON)	: paramètres à fournir à ce fichier (format JSON)
 //(callback)	: nom de la fonction callback éventuelle
 // Exemple : LG_Ajax_GetText('GetData', '[{"nom":"p1","valeur":"para1"},{"nom":"p2","valeur":"para2"},{"nom":"p3","valeur":3}]', 'http://www.limoog.net/tmp/', 'json') ;
 function LG_Ajax_GetText_JQuery(psURL, psParaJSON, callback) {
 
 psParaJSON = JSON.stringify(psParaJSON) ;
 $.ajax({
 type: "POST",
 url: psURL,
 data: psParaJSON,
 contentType: "application/json; charset=utf-8",
 dataType: "text",
 success: function (data, status, xhr) {
 callback(xhr.responseText) ;
 },
 error: function (xhr, status, error) {
 callback("Erreur : " + status) ;
 },
 complete: function (xhr, status) {
 //				callback() ;
 },
 });
 
 var lsText = " [Non renseigné car processus asynchrone : renvoi de la réponse à la fonction callkack]" ;
 return lsText;
 
 }
 */

//-----------------------------------------------------------------------------------------
// Déterminer si le texte renvoyé par LG_Ajax_GetText correspond &agrave; une erreur
// Renvoie 1 en cas d'erreur de lecture du fichier
function LG_Ajax_GetText_EstErreur(psText) {
//alert(psText.match("Erreur404")) ;
    if (typeof psText == 'undefined') {
        return true;
    }
//MG Ajout 20200305 Début
//Dans le cas ou psText est un objet
    else if (typeof psText == 'object') {
        psText = JSON.stringify(psText);
        return true;
    }
//MG Ajout 20200305 Fin


// alert(psText) ;
    if (psText.substring(0, 21) == "Chargement impossible") {
        return true;
    }						// Erreur de LG_Ajax_GetText
    if (psText.match("Erreur, pas de données récupérées par le webBrowser")) {
        return true;
    }	// Erreur du webBrowser : rien n'a été récupéré
    if (psText.match("Erreur lors de l'exécution de")) {
        return true;
    }							// Erreur du webBrowser : l'exécution du script a échoué
    if (psText.match("302 Found")) {
        return true;
    }												// Erreur 
    if (psText.match("404 Not Found")) {
        return true;
    }											// Page non trouvée
    if (psText.match("Erreur404")) {
        return true;
    }											// Page non trouvée
    if (psText.match("Erreur : ")) {
        return true;
    }											// Page non trouvée
    if (psText.match("entry not found")) {
        return true;
    }											// Page non trouvée
    return false;
}
