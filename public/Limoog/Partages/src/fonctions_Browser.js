﻿// Fonctions de détection de navigateur
// Et de compatibilité internavigateurs
// LG 20160930

//-----------------------------------------------------------------------------------------
// Renvoie true si on est sous Internet Explorer
        function isIE() {
            return navigator.appName == 'Microsoft Internet Explorer';
        }

//-----------------------------------------------------------------------------------------
// Renvoie true si on est dans un contexte WebBrowser (hébergé par VFP par exemple)
function isWebBrowser() {
    // On suppose que dans ce cas, on est IE 
    // et que le protocole est "File" (en général, pour tester correctement en mode non webbrowser, on utilise un serveur web et on est donc en protocole HTTP)
    return (isIE() && (location.protocol == "file:"));
}

//-----------------------------------------------------------------------------------------
// Renvoie true si on est sous Internet Explorer
function isIEInférieurA(piVersion) {
    if (!isIE())
        return false;
    else {
        if (getRealInternetExplorerVersion() < piVersion)
            return true;
        else
            return false;
    }
}

//-----------------------------------------------------------------------------------------
// Renvoie true si on est sous Chrome
function isChrome() {
    return Prototype.Browser.WebKit;
}

//-----------------------------------------------------------------------------------------
/*
 * Les deux prochaines fonctions permettent d'identifier le navigateur. 
 * Idéalement, ces fonctions ne devraient pas être utilisées : il est plus correct d'utiliser 
 * la détection d'objet.
 * Voir http://www.misfu.com/static/Javascript/support.html pour plus d'infos.
 */
/**
 * getInternetExplorerVersion, Retourne le numéro de version d'IE,
 * d'après l'user-agent, SANS chercher à savoir si on est dans un mode émulé
 * ou retourne -1 si on utilise un autre navigateur.
 * Cette fonction provient de http://msdn.microsoft.com/en-us/library/ms537509
 *
 * @return {number}  rv - le numéro de version d'internet explorer, ou -1 pour les autres navigateurs
 */
function getInternetExplorerVersion() {

    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

//-----------------------------------------------------------------------------------------
/**
 * getRealInternetExplorerVersion, Retourne le numéro de version d'IE,
 * ou retourne -1 si on utilise un autre navigateur.
 * Cette fonction provient de http://msdn.microsoft.com/en-us/library/ms537509
 *
 * @return {number}  rv - le numéro de version d'internet explorer, ou -1 pour les autres navigateurs
 */
function getRealInternetExplorerVersion()
{
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
        var ua = navigator.userAgent;
        var re = new RegExp("Trident/([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

//-----------------------------------------------------------------------------------------
// Gestion des cas où l'objet console n'existe pas
if (!console) {
    // L'objet console n'existe pas
    // Le créer
    var console = {log: function (psLog) {
            popupWindow("console : " + psLog, true)
        }};
}

/*
 //-----------------------------------------------------------------------------------------
 // Incorporer la capacité forEach même sous IE8
 // cf. http://stackoverflow.com/questions/16186930/jquery-foreach-not-working-in-ie8
 if (typeof Array.prototype.forEach != 'function') {
 Array.prototype.forEach = function(callback){
 for (var i = 0; i < this.length; i++){
 callback.apply(this, [this[i], i, this]);
 }
 };
 }
 */
/**
 * getElementsByName, modifié pour être compatible avec Internet Explorer
 *
 * @param {String} tag - type d'élément HTML. Ex : DIV, TABLE
 * @param {String} name - nom de l'élément HTML à retourner
 * @return {Element HTML} arr - renvoit l'élément HTML de type tag et de nom name
 *
 * Utilisée dans semaine.js, dans la fonction AfficherSemaine()
 */
function getElementsByName_iefix(tag, name)
{
    var elem = document.getElementsByTagName(tag);
    var arr = new Array();
    for (i = 0, iarr = 0; i < elem.length; i++)
    {
        att = elem[i].getAttribute("name");
        if (att == name)
        {
            arr[iarr] = elem[i];
            iarr++;
        }
    }
    return arr;
}

//-----------------------------------------------------------------------------------------
// La variable BrowserDetect nous renvoi plusieurs paramètres qui sont :
// Le nom du navigateur web du client 
// La version du navigateur web du client 
// Le nom du système d'exploitation du client
var BrowserDetect = {
    init: function ()
    {
        this.browser = this.searchString(this.dataBrowser) || "An test browser";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data)
    {
        for (var i = 0; i < data.length; i++)
        {
            var dataString = data[i].string;
            var dataProp = data[i].lcProp;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString)
            {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            } else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString)
    {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1)
            return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {
            string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            lcProp: window.opera,
            identity: "Opera",
            versionSearch: "Version"
        },
        {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        },
        {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino"
        },
        {// for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        },
        {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        {// for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }],
    dataOS: [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
            string: navigator.userAgent,
            subString: "iPhone",
            identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }]
};
BrowserDetect.init();

// ************************************************************************
// fonction bind sur les fonctions, pour les ie <= 8
// et quand Prototype n'est pas actif
// http://stackoverflow.com/questions/30837332/function-bind-not-supported-in-ie8-make-it-available-specifically-for-ie8-bro
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5
            // internal IsCallable function
            throw new TypeError('Function.prototype.bind - what     is     trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function () {},
                fBound = function () {
                    return fToBind.apply(this instanceof fNOP && oThis
                            ? this
                            : oThis,
                            aArgs.concat(Array.prototype.slice.call(arguments)));
                };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}

if (!document.getElementsByClassName) {
    document.getElementsByClassName = function (cn) {
        cn = cn.replace(/ *$/, '');

        if (document.querySelectorAll) // Returns NodeList here
            return document.querySelectorAll((' ' + cn).replace(/ +/g, '.'));

        cn = cn.replace(/^ */, '');

        var classes = cn.split(/ +/), clength = classes.length;
        var els = document.getElementsByTagName('*'), elength = els.length;
        var results = [];
        var i, j, match;

        for (i = 0; i < elength; i++) {
            match = true;
            for (j = clength; j--; )
                if (!RegExp(' ' + classes[j] + ' ').test(' ' + els[i].className + ' '))
                    match = false;
            if (match)
                results.push(els[i]);
        }

        // Returns Array here
        return results;
    }
}

// Quel bouton de la souris est appuyé ?
// Pour comatibilité IE<=8 avec W3C
function eventButton(e) {
    var button;
    if (e.which == null)
        // IE case
        button = (e.button < 2) ? 0 :
                ((e.button == 4) ? 1 : 2);
    else
        // All others
        button = e.button;
    return button;
}
