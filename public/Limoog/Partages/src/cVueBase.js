/* 
 * Classe de vue d'affichage des plannings
 */

// Classe selon John Resig
var cVueBase = Class.extend({
    cClassName: "cVueBase",

    charge: function (json) {
    },

    montreErreur: function (psErrMsg) {
        document.getElementById("Erreurs").innerHTML += psErrMsg + "<BR>";
        console.log(psErrMsg);
    },

    clearErreur: function () {
        document.getElementById("Erreurs").innerHTML = "";
    },

    montreEnregistrer: function (pbAffiche) {
        if (pbAffiche) {
            //document.getElementById("Planning").innerHTML = "<button id='Enregistrer' onclick='window_oControleur_montreEnregistrer()' >Enregistrer</button>" + document.getElementById("Planning").innerHTML;
            document.getElementById('Enregistrer').className = 'visible';
        }

    },

});

