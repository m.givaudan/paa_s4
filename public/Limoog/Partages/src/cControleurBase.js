/* 
 * Classe de base de controleur : méthodes communes à tous les contrôleurs
 */

function window_oControleur_montreEnregistrer() {
    if (!window.oControleur)
        return false;
    if (!window.oControleur.montreEnregistrer)
        return false;
    else
        return window.oControleur.montreEnregistrer(true);
}

window.getPlanningoCst = function () {
    return window.cst;
}
window.getPlanningoData = function () {
    return window.oData;
}
window.getPlanningoVue = function () {
    return window.oVue;
}
window.getPlanningoControleur = function () {
    return window.oControleur;
}

// ****************************************************************************************
// Classe de controleur générique pour le planning
// ****************************************************************************************
var cControleurBase = Class.extend({
    cClassName: "cControleurBase",
    cNomFichJSONACharger: "<Changé par programme>",
//	toto: function(){},

    // Lancer le chargement du planning
    // Après avoir récupéré les informations à tracer
    demandeChargement: function (psNomFichJSON) {
        /*
         // Créer l'objet Data si nécessaire
         if (!window.oData) window.oData = new cData();
         
         // Créer l'objet Vue si nécessaire
         if (!window.oVue) window.oVue = new cVue();	
         */
        this.créeSousObjetsSiNécessaire();

        // Demander les données (asynchrone ou non)
        if (psNomFichJSON)
            this.cNomFichJSONACharger = psNomFichJSON;
        var lsJSON = LG_Ajax_GetText(psNomFichJSON, null, "", "JSON", this.onJSON);
    },

    créeSousObjetsSiNécessaire: function () {
        // Créer l'objet Data si nécessaire
        if (!window.oData)
            window.oData = new cData();

        // Créer l'objet Vue si nécessaire
        if (!window.oVue)
            window.oVue = new cVue();
    },

    // Sur réception du contenu JSON de chargement
    onJSON: function (psJSON) {
        // Etude du cas où l'événement "BeforeNavigate2" du webbrowser n'a pas été appellé
        // Bug du webbrowser qui arrive par exemple quand le processus VFP est "surchargé"
        var lsTexteNonRempli = LG_Ajax_GetText_RtvCst_NR();
        if (psJSON == lsTexteNonRempli) {
            // Le texte n'a pas pu être lu instantanément
            // L'événement "BeforeNavigate2" du webbrowser n'a sans doutes pas été appellé

            // Préparer la div de réecption du JSON
            var lsNomDiv = LG_Ajax_GetNomDivEWB(this.cNomFichJSONACharger);
            document.getElementById(lsNomDiv).innerHTML = "";

            // Demander le chargement en asynchrone : VFP attend dans un timer que window.getPlanningoControleur().lDemandeChargement devienne true
            window.getPlanningoControleur().lDemandeChargement = true;

            // Lancer le timer qui va attendre qu'il y aie des données
            window.getPlanningoControleur().timerCheckPrésenceJSON = window.setInterval(window.getPlanningoControleur().checkPrésenceJSON, 50);
        } else {
            // Le texte a pu être lu instantanément
            // Appeller directement la méthode de chargement
            window.getPlanningoControleur().charge(psJSON);
        }

    },

    checkPrésenceJSON: function () {
        var lsNomDiv = LG_Ajax_GetNomDivEWB(this.cNomFichJSONACharger);
        if (document.getElementById(lsNomDiv).innerHTML) {
            // La div a été remplie
            // Arrêter le timer
            window.clearInterval(window.getPlanningoControleur().timerCheckPrésenceData);

            // Récupérer le contenu
            var lsJSON = document.getElementById(lsNomDiv).innerHTML;
            document.getElementById(lsNomDiv).innerHTML = "";

            // Charger le planning
            window.getPlanningoControleur().charge(lsJSON);
        }
    },

    getModeExecution: function () {
        var liMode = window.getPlanningoData().getModeExecution();
        if (liMode == window.getPlanningoCst().ModeExecution_Inconnu) {
//			liMode = window.getPlanningoCst().ModeExecution_ExecSymfony ;
            if (typeof window.oConfig !== "undefined")
                liMode = window.oConfig.iModeExecution
        }
        return liMode;
    },

    setModeExecution: function (piModeExecution) {
        return window.getPlanningoData().setModeExecution(piModeExecution);
    },

    // -------------------------------------------------------------
    // Vérifier que le mode d'exécution est cohérent
    // LG 20160726
    checkModeExecution: function () {
        // Etude de la copatibilité du type d'exécution demandée et du navigateur
        // (actuellement : le mode test VFP local n'est possible qu'avec IE7 (celui du controle webbrowser)
        var liIEVersion = getInternetExplorerVersion();
        var liModeExecution = this.getModeExecution();
        var llKO = false;
        var lcErreur = "";
        if (liModeExecution == window.getPlanningoCst().ModeExecution_TestNavigateur) {
            // RAS

        } else if (liModeExecution == window.getPlanningoCst().ModeExecution_VFPNet) {
            // Mode test VFP au travers d'un navigateur
            lcErreur = "Mode window.getPlanningoCst().ModeExecution_VFPNet : non pris en charge actuellement";
            window.getPlanningoData().setLastErreur(lcErreur);
            console.log(lcErreur);
            llKO = true;

        } else if (liModeExecution == window.getPlanningoCst().ModeExecution_ExecNavigateur
                || liModeExecution == window.getPlanningoCst().ModeExecution_ExecSymfony) {
            // Mode exécution réelle au travers d'un navigateur
//			lcErreur = "Mode window.getPlanningoCst().ModeExecution_VFPNet : non pris en charge actuellement" ;
//			window.getPlanningoData().setLastErreur(lcErreur) ;
//			console.log(lcErreur);
//			llKO = true ;

        } else if (liIEVersion < 1 || liIEVersion > 8) {
            // Mode test VFP en local (au travers de l'appli PAA) non pris en charge
            // Repasser en mode test navigateur
            lcErreur = "Mode window.getPlanningoCst().ModeExecution_VFPLocal : non pris en charge actuellement pour ce navigateur."
                    + " Mode changé en window.getPlanningoCst().ModeExecution_TestNavigateur";
            window.getPlanningoData().setLastErreur(lcErreur);
            console.log(lcErreur);
            liModeExecution = window.getPlanningoCst().ModeExecution_TestNavigateur;

        } else {
//			lcErreur = "Mode window.getPlanningoCst().ModeExecution_VFPLocal pris en charge actuellement pour ce navigateur : IE" + liIEVersion, "green" ;
//			window.getPlanningoData().setLastErreur(lcErreur) ;
//			console.log(lcErreur );
        }

        // Positionner le mode d'exécution
        this.setModeExecution(liModeExecution);

        return !llKO;

    },

    // -------------------------------------------------------------
    // Lancer le chargement du planning avec un texte JSON connu
    charge: function (psJson) {
        var lbKO = false;

        if (!this.beforeCharge(psJson)) {
            // Annulation
            lbKO = true;
        } else if (!window.getPlanningoData().charge(psJson)) {
            // Echec du chargement
            lbKO = true;
            window.getPlanningoVue().montreErreur(window.getPlanningoData().cLastErreur);

        } else if (!this.checkModeExecution()) {
            // Mode d'exécution incorrect ou incompatible aver le navigateur
            lbKO = true;
            window.getPlanningoVue().montreErreur(window.getPlanningoData().cLastErreur);

        } else if (!window.getPlanningoVue().charge(window.getPlanningoData().oActuel)) {
            // Echec du chargement de la vue
            lbKO = true;

        } else if (!this.afterCharge(psJson)) {
            // Annulation
            lbKO = true;

        } else {
            // Succès du chargement des données et de la vue
            if (window.oPlanning)
                window.oPlanning.jsonChargé = psJson;
        }

        //window.getPlanningoControleur().SetModifié(false);
        return !lbKO;
    },

    beforeCharge: function (psJson) {
        return true;
    },

    afterCharge: function (psJson) {
        // C'est le moment pour mettre en place les menus contextuels
        return true;
    },

    setModifie: function (pbModifie, pbSansRechargerNiAvertir) {
        window.getPlanningoData().setModifie(pbModifie);

        if (pbModifie) {
            // window.getPlanningoVue().montreEnregistrer(pbModifie);
            if (!pbSansRechargerNiAvertir) {
// LG 20151016 : LG_Ajax_Notifie fonctionne parfaitement bien en cas standard, 
// mais quand le thread VFP est à 100% de CPU, l'événement WebBrowser BeforeNavigate2 ne se déclanche pas
// (de même que d'autres événements comme NewWindow2 et NewWindow3)
// Et ca aboutit à une fenêtre d'erreur 404 sur le webbrowser
                // LG_Ajax_Notifie('Modifié');
                // On se contente donc de poser une propriété dans window.getPlanningoData()
                // Que VFP sera chargé de lire en continu
                window.getPlanningoData().lModifié = true;
// LG 20151016 fin
                window.getPlanningoVue().charge(window.getPlanningoData().oActuel);
            }
        }

    },

    enregistre: function () {
        var lbEnregistrer = window.getPlanningoData().enregistre();
        if (lbEnregistrer) {
            window.getPlanningoControleur().setModifie(false, true);
        }
        return lbEnregistrer;
    },

    accepteFerme: function () {
        if (window.getPlanningoData().lModifie) {
            alert("Les données ont été modifiées, souhaitez-vous les enregistrer, Oui/Non/Annuler");
            //TODO

            if (lsReponse == "Oui") {
                return window.getPlanningoData().enregistre();
            } else if (lsReponse == "Non") {
                return true;
            } else if (lsReponse == "Annuler") {
                return false;
            }
        }
    },

    montreEnregistrer: function () {
        if (!window.getPlanningoVue())
            return false;
        if (!window.getPlanningoVue().montreEnregistrer)
            return false;
        else
            return window.getPlanningoVue().montreEnregistrer(true);
    },
});

