// Fonctions utiles pour la gestion des couleurs

function CouleurRGBToHex(couleur) {

    var Reste1 = couleur % (256 * 256);
    var blue = (couleur - Reste1) / (256 * 256);
    var Reste2 = Reste1 % 256;

    var green = (Reste1 - Reste2) / 256;

    var red = Reste2;

    var redH = red.toString(16);
    var greenH = green.toString(16);
    var blueH = blue.toString(16);
    var val = '#' + redH.padStart(2, '0')
            + greenH.padStart(2, '0')
            + blueH.padStart(2, '0');

    if (val == '#000000') {
        val = '#ffffff';
    }

    return val;
}

function RGB(r, g, b) {
    return r + g * 256 + b * 256 * 256;
}

function getSpectrumPalette() {
    return [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
            "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ];
}

function LGSpectrum(psIdSpectrumElement, psIdInputElement, pfOnChangeCallBack) {
    var liCouleur = parseInt($(psIdInputElement).val());
    var lsCouleur = CouleurRGBToHex($(psIdInputElement).val());
    var lsPalette = getSpectrumPalette();
    $(psIdSpectrumElement).spectrum({
        color: lsCouleur,
        showInput: true,
        //className: "full-spectrum",
        showInitial: true,
        showPalette: true,
        //showSelectionPalette: true,
        maxSelectionSize: 10,
        preferredFormat: "hex",
        localStorageKey: "spectrum.limoog",
        cancelText: "annuler",
        chooseText: "choisir",
        palette: lsPalette,
        change: pfOnChangeCallBack
    });
}

// Classe cbcRessources, basée sur Bootstrap Multiselect (http://davidstutz.github.io/bootstrap-multiselect/)
// Doit inclure les fichiers correspondants
!function ($) {

    // -----------------------------------------------------------
    // Commandes de définition d'une cbo de sélection de couleurs avec Spectrum
    // HTML : 
    /*
     <select id="cboCouleur"
     data-role="cboCouleur"
     >
     Le contenu de cette combo est changé PAR AJAX, de part son data-role
     </select>
     */
    $.fn.cboCouleur = function () {
        return this.each(function (o) {
            var thisCbcSource = this;
            var liCouleur = parseInt(thisCbcSource.value);
            var lsCouleur = CouleurRGBToHex(thisCbcSource.value);
            var lsPalette = getSpectrumPalette();
            // Ajouter l'input pour le stockage de la valeur
            // <input type='text' id="icouleur_spectrum"/>
            var loInput = document.createElement("input");
            loInput.type = "text";
            thisCbcSource.parentNode.appendChild(loInput);
            // Cacher si ce n'est déja fait
            thisCbcSource.type = "hidden";

            var thisCbc = loInput;
            $(thisCbc).spectrum({
                color: lsCouleur,
                showInput: true,
                //className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                //showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                localStorageKey: "spectrum.limoog",
                cancelText: "annuler",
                chooseText: "choisir",
                palette: lsPalette,
                change: function (color) {
//					var event = jQuery.Event("onChangecboCouleur");
//					event.source = $(thisCbc)[0].id ;
//					$("body").trigger(event);
                    var colorRGB = color.toRgb();
                    var colorI = RGB(colorRGB.r, colorRGB.g, colorRGB.b)
                    thisCbcSource.value = colorI;
                }
            });
        });
    }

    $(function () {
        $("[data-role=cboCouleur]").cboCouleur();
    });

}(window.jQuery);

