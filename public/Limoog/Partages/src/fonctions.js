/*
 * Ce fichier contient les onerrors communes du projet POO_Planning 
 */
// Source : http://www.quirksmode.org/js/detect.html
// Permet de detecter le navigateur :
// Browser name:     BrowserDetect.browser
// Browser version:  BrowserDetect.version
// OS name:          BrowserDetect.OS

// AC 20121126 
// La variable BrowserDetect nous renvoi plusieurs paramétres qui sont :
// Le nom du navigateur web du client 
// La version du navigateur web du client 
// Le nom du systéme d'exploitation du client


//-----------------------------------------------------------------------------------------
// Fonctions Caractère
//-----------------------------------------------------------------------------------------

// Selon http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};
String.prototype.dollar = function (it) {
    return it.indexOf(this) != -1;
};

// Selon Ben Rowe dans http://stackoverflow.com/questions/2308134/trim-in-javascript-not-working-in-ie
// trim n'est pas implanté dans IE
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

// Selon https://stackoverflow.com/questions/490508/left-function-in-javascript-or-jquery
String.prototype.left = function (n) {
    return this.substring(0, n);
}
String.prototype.right = function (n) {
    return this.substring(n, this.length - n);
}

// Selon JP Richardson, http://stackoverflow.com/questions/8141718/javascript-need-to-do-a-right-trim
//String.prototype.rtrim = function(s) { 
//    return this.replace(new RegExp(s + "*$"), ''); 
//};
rTrim = function (txt, s) {
    var txtCorr = txt;
    while (true) {
        txtCorr = txt.replace(new RegExp(s + "*$"), '');
        if (txtCorr == txt) {
            // Pas de changement : il n'y a pas eu de correction
            break;
        } else {
            txt = txtCorr;
        }
    }
    return txt;

};

// Nettoyer le HTML pour éliminer les caractères invisibles de la fin (espaces et <br>)
function rTrimHTML(txt) {
    // Etre certain que les br sont en minuscule
    var txtCorr = txt.replace(/<br>/mgi, '<br>');
    while (true) {
        txtCorr = rTrim(txtCorr, " ");
        txtCorr = rTrim(txtCorr, "<br>");
        if (txtCorr == txt) {
            // Pas de changement : il n'y a pas eu de correction
            break;
        } else {
            txt = txtCorr;
        }
    }
    return txt;
}

// LG 20161125
// Réduire le texte de la DIV jusqu'à ce qu'il ne déborde plus
// adapté de Aram Kocharyan, http://stackoverflow.com/questions/16056591/font-scaling-based-on-width-of-container
// Voir C:\Luc\Projets VB et FoxPro\JavaScript\tests\Ajuste.html
// Utilisation : $(".déborde").ajusterTaillePolice();
(function ($) {

    // Réduire la taille de police pour tenir dans l'élément HTML parent
    // Calcul APPROXIMATIF basé sur la proportionalité des tailles de caractères
    // PAS ENCORE de prise en charge des sauts de ligne dans le texte
    // PAS DE PRISE EN CHARGE des changements de police dans le texte
    $.fn.ajusterTaillePolice_Simple = function (options) {
        options = $.extend({
            minFontSize: 8
            , decrementBy: 1
            , allowResetLineBreak: true				// Accepte-t-on d'enlever les sauts de ligne pour un meilleur ajustement ?
            , minFontSizeBeforeLineBreakReset: 10	// Si on accepte d'enlever les sauts de ligne, taille mini de police avant d'essayer
            , parentBoxClass: ''					// Si fourni, le nom de la classe CSS de la div qui est la box dans laquelle on doit ajuster le texte
                    // Si non fourni, on prend le premier parent
        }, options);

        // Calcul de la taille d'un texte
        function textDim(psText, piFontSise) {
            var dim = {};
            var carWidthPerPixelFont = 251 / 50 / 10;			// en Arial 50, le texte "S1 : FP M2" occupe 251px de large
            var carHeightPerPixelFont = 58 / 50 / 10;			// en Arial 50, le texte "S1 : FP M2" occupe 58px de haut
            var lsText = psText.trim();
            dim.width = carWidthPerPixelFont * lsText.length * piFontSise;
            dim.height = carHeightPerPixelFont * lsText.length * piFontSise;
            return dim;
        }

        var result = this.each(function () {
//nbItems++ ;
            var $this = $(this);
            if (options.parentBoxClass) {
                // On indique la classe CSS de la div qui fixe les dimensions
                var $parent = $this.parents("." + options.parentBoxClass);
            } else {
                // Pas d'indication : prendre le premier parent
                var $parent = $this.parent();
            }

            var ok = true;
            var parentWidth = $parent.width();
            var parentHeight = $parent.height();
            var parentSurf = parentWidth * parentHeight;
            var txt = $this.html();
            var nbLignes;

            var minFontSize = (options.allowResetLineBreak) ? options.minFontSizeBeforeLineBreakReset : options.minFontSize;
            var currentFontSize = parseInt($this.css('font-size').replace('px', ''));
            if (isNaN(currentFontSize)) {
                // Pas de taille de police ?
                ok = false;
            }
            while (ok) {
                var dim = textDim(txt, currentFontSize);
                nbLignes = Math.ceil(dim.width / parentWidth);
                heightTot = nbLignes * dim.height;
                if (heightTot <= parentHeight) {
                    // Cette taille de police convient
                    break;
                } else if (currentFontSize <= minFontSize) {
                    // On arrive à la taille de police minimale
                    break;
                } else {
                    currentFontSize -= options.decrementBy;
                }
            }
            $this.css('font-size', currentFontSize + 'px');


        });

        return result;
    }

    // --------------------------------------------------------------------
    // Réduire la taille de police pour tenir dans l'élément HTML parent
    // Calcul EXACT basé sur la mesure de ce que devient l'élément quand on change réellement sa taille de police
    $.fn.ajusterTaillePolice_Exact = function (options) {
        options = $.extend({
            minFontSize: 8
            , decrementBy: 1
            , allowResetLineBreak: true				// Accepte-t-on d'enlever les sauts de ligne pour un meilleur ajustement ?
            , minFontSizeBeforeLineBreakReset: 10		// Si on accepte d'enlever les sauts de ligne, taille mini de police avant d'essayer
            , parentBoxClass: ''						// Si fourni, le nom de la classe CSS de la div qui est la box dans laquelle on doit ajuster le texte
                    // Si non fourni, on prend le premier parent
        }, options);

//var date = new Date();
//var nbItems = 0 ;
//var nbIterations = 0 ;	

        function getTextHeight(em, width) {
            var $em = $(em);
            var oldPosition = $em.css('position');
            $em.css('position', 'absolute');
            if (width) {
                var oldWidth = $em[0].style.width;
                $em.css('width', width);
            }
            var dim = {width: $em.width(), height: $em.height()};
            if (width)
                $em.css('width', oldWidth);
            $em.css('position', oldPosition);
            return dim;
        }

        // Essayer avec la taille de police demandée, et renvoyer le niveau d'ajustement du texte
        // hauteur réelle / hauteur max
        function checkFontSize(em, width, height, fontSize) {
            // Fixer la taille de police et voir la taille du résultat
            em.css('font-size', fontSize + 'px');
            var dim = getTextHeight(em, width);

            return dim.height / height;
        }

        // Affectuer l'ajustement de taille et renvoyer true si l'ajustement a fonctionné
        function ajuste(em, width, height, fontSizeMax, fontSizeMin) {
//nbIterations++ ;
            var resu = checkFontSize(em, width, height, fontSizeMax)
            if (resu < 1) {
                // Tel quel, le texte tient
                return true;
            }

            // Si on arrive là, c'est que la taille max est trop grande
//nbIterations++ ;
            resu = checkFontSize(em, width, height, fontSizeMin)
            if (resu > 1) {
                // Le texte ne tient pas, même avec la dimension minimale
                return false;
            }

            // Si on arrive là, c'est que la bonne taille se trouve entre le min et le max
            var currentFontSize = fontSizeMax;
            var prevFontSize;
            while (true) {
//nbIterations++ ;
                if (currentFontSize <= fontSizeMin
                        || prevFontSize && prevFontSize == currentFontSize) {
                    return false;

                } else if (checkFontSize(em, width, height, currentFontSize) < 1) {
                    // Le texte tient maintenant
                    return true;
                }
                prevFontSize = currentFontSize;
                currentFontSize -= options.decrementBy;
            }

        }

        var result = this.each(function () {
//nbItems++ ;
            var $this = $(this);
            if (options.parentBoxClass) {
                // On indique la classe CSS de la div qui fixe les dimensions
                var $parent = $this.parents("." + options.parentBoxClass);
            } else {
                // Pas d'indication : prendre le premier parent
                var $parent = $this.parent();
            }
            var currentFontSize = parseInt($this.css('font-size').replace('px', ''));
            var initialFontSize = currentFontSize;
            var minFontSize = (options.allowResetLineBreak) ? options.minFontSizeBeforeLineBreakReset : options.minFontSize;
            var parentWidth = $parent.width();
            var parentHeight = $parent.height();
            if (!parentWidth)
                parentWidth = parseInt($parent.context.style.width);
            if (!parentHeight)
                parentHeight = parseInt($parent.context.style.height);
            var ok = false;
            if (isNaN(currentFontSize)) {
                // Pas de taille de police ?
                ok = false;

            } else if (ajuste($this, parentWidth, parentHeight, currentFontSize, minFontSize)) {
                // Tel quel, le texte tient
                ok = true;

            } else if (options.allowResetLineBreak) {
                // Le texte ne tient pas, même avec la dimension minimale
                // Enlever les sauts de ligne maintenant et reprendre avec la police initiale
                var txt = $this[0].innerHTML.replace(/<br>/g, "");
                if ($this[0].innerHTML !== txt) {
                    // Il y a des sauts de ligne
                    // Reprendre tout le calcul avec la taille oiginale
                    currentFontSize = initialFontSize;
                    $this[0].innerHTML = txt;
                }
                minFontSize = options.minFontSize;
                if (ajuste($this, parentWidth, parentHeight, currentFontSize, minFontSize)) {
                    ok = true;
                }
            }

        });

//var ms = (new Date()) - date ;
//alert("Nb items : " + nbItems 
//		+ "\n, nb itérations : " + nbIterations 
//		+ "\n, moy : " + nbIterations/nbItems
//		+ "\n, dutée : " + ms + "ms"
//		+ "\n, dutée moy : " + ms/nbItems + "ms/item"
//		) ;
//	return result ;

    };

})(jQuery);

//-----------------------------------------------------------------------------------------
// Fonctions Date
//-----------------------------------------------------------------------------------------

// Créer une nouvelle date avec un string (conversion directe non fonctionnelle sous ie8)
// psDate ! "YYYY/MM/DD"
function CTOD(psDate) {
    if ("/".dollar(psDate))
        var laDate = psDate.split("/");
    if ("-".dollar(psDate))
        var laDate = psDate.split("-");
    if (" ".dollar(psDate))
        var laDate = psDate.split(" ");
    var ldDate = new Date(laDate[0], laDate[1], laDate[2]);
    return ldDate;
}

// Déterminer si une variable est une date
// Voir aussi isDate() dans prototype.js
function isDate(pvValeur) {
    var ldDate = new Date(pvValeur);
    return (ldDate !== "Invalid Date" && !isNaN(ldDate) && (ldDate.getHours() + ldDate.getMinutes() + ldDate.getSeconds()) == 0) ? true : false;
}

// Déterminer si une variable est une dateHeure
function isDateTime(pvValeur) {
    var ldDate = new Date(pvValeur);
    return (ldDate !== "Invalid Date" && !isNaN(ldDate) && (ldDate.getHours() + ldDate.getMinutes() + ldDate.getSeconds()) > 0) ? true : false;
}
// Selon http://stackoverflow.com/questions/563406/add-days-to-javascript-date
Date.prototype.ajouteJours = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}
Date.prototype.estInferieureA = function (pdDate) {
    if (!isDate(pdDate))
        return false;
    if (this.getFullYear() < pdDate.getFullYear())
        return true;
    if (this.getFullYear() > pdDate.getFullYear())
        return false;
    if (this.getMonth() < pdDate.getMonth())
        return true;
    if (this.getMonth() > pdDate.getMonth())
        return false;
    if (this.getDate() < pdDate.getDate())
        return true;
    if (this.getDate() > pdDate.getDate())
        return false;
    return true;
}
Date.prototype.estMemeDate = function (pdDate) {
    if (!isDate(pdDate))
        return false;
    if (this.getDate() !== pdDate.getDate())
        return false;
    if (this.getMonth() !== pdDate.getMonth())
        return false;
    if (this.getFullYear() !== pdDate.getFullYear())
        return false;
    return true;
}

// Selon http://stackoverflow.com/questions/542938/how-do-i-get-the-number-of-days-between-two-dates-in-javascript
function dateDiff(date1, date2, interval) {
    var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    date1 = new Date(date1);
    date2 = new Date(date2);
    var timediff = date2 - date1;
    if (isNaN(timediff))
        return NaN;
    if (!interval)
        interval = "days";
    switch (interval) {
        case "years":
            return date2.getFullYear() - date1.getFullYear();
        case "months":
            return (
                    (date2.getFullYear() * 12 + date2.getMonth())
                    -
                    (date1.getFullYear() * 12 + date1.getMonth())
                    );
        case "weeks"  :
            return Math.floor(timediff / week);
        case "days"   :
            return Math.floor(timediff / day);
        case "hours"  :
            return Math.floor(timediff / hour);
        case "minutes":
            return Math.floor(timediff / minute);
        case "seconds":
            return Math.floor(timediff / second);
        default:
            return undefined;
    }
}
// Renvoyer la dateTime actuelle
function now() {
    var now = new Date();
    return now.getTime();
}

// Récupérer le mois d'une date
// Dans certains cas, c'est base 0, dans d'autres, c'est base 1 ...
function getMonth(pdDate, pbSurDeuxChiffres) {
    var ldDate = new Date(2000, 00, 01);
    var liIncrément = 1 - ldDate.getMonth();
    var liMonth = pdDate.getMonth() + liIncrément;
    if (pbSurDeuxChiffres) {
        return (liMonth > 9) ? liMonth.toString() : ("0" + liMonth.toString());
    } else
        return liMonth;
}

// Convertir date en caractère, à la française
// pdDate 		: date à convertir
//(plTriable)	: non vide pour date YYYYMMDD
function DTOC(pdDate, plTriable) {
    var mm = (getMonth(pdDate)) + ''; // getMonth() is zero-based
    mm = ((mm[1]) ? '' : '0') + mm;
    var dd = pdDate.getDate() + '';
    dd = (dd[1] ? '' : '0') + dd;
    var yyyy = pdDate.getFullYear();
    if (plTriable)
        return yyyy + mm + dd;
    else
        return dd + "/" + mm + "/" + yyyy;
}

// Nom du jour de la semaine
// pdDate 		: date à convertir
function CDOW(pdDate) {
    var n = pdDate.getDay();
    switch (n) {
        case 1:
            return "lundi";
        case 2:
            return "mardi";
        case 3:
            return "mercredi";
        case 4:
            return "jeudi";
        case 5:
            return "vendredi";
        case 6:
            return "samedi";
        case 0:
            return "dimanche";
    }
    console.log("Jour inconnu");
    return "Jour inconnu";
}

//MG Ajout 20200204 Début
// retourne un numero de jour de la semaine
//pdDate                        : date
//nFirstDayOfWeek               : spécifié le premier jour de la semaine
function DOW(pdDate, nFirstDayOfWeek) {
    nFirstDayOfWeek2 = nFirstDayOfWeek - 1;
    var n = pdDate.getDay() + 1;
    if (nFirstDayOfWeek > 1) {
        n = n - nFirstDayOfWeek2;
        if (n <= 0) {
            n = 7 - Math.abs(n);
        }
        if (n > 7) {
            n = n - nFirstDayOfWeek2;
        }
    } else if (nFirstDayOfWeek === 0) {
        return;
    }
    return n;
}
//MG Ajout 20200204 Fin

// Nom du mois
// pdDate 		: date à convertir, ou N° de mois
function CMONTH(pdDate) {
    var n = isNaN(pdDate) ? getMonth(pdDate) : pdDate;
    switch (n) {
        case 1:
            return "janvier";
        case 2:
            return "février";
        case 3:
            return "mars";
        case 4:
            return "avril";
        case 5:
            return "mai";
        case 6:
            return "juin";
        case 7:
            return "juillet";
        case 8:
            return "août";
        case 9:
            return "septembre";
        case 10:
            return "octobre";
        case 11:
            return "novembre";
        case 12:
            return "décembre";
    }
    console.log("Mois inconnu");
    return "Mois inconnu";
}

// Renvoyer la date de la fin du mois passé en paramètre
function dateFinDeMois(pdDate) {
    var ldDate = new Date(pdDate.getTime());
    var liMois = getMonth(pdDate);
    while (liMois == getMonth(pdDate.ajouteJours(1)))
        pdDate = pdDate.ajouteJours(1);
    return pdDate;
}

/**
 *arrayRemove
 * Supprime un élement d'un tableau, et réhausse les élements suivant, pour éviter une case vide. 
 * @param {Array} Le tableau ciblé
 * @param {indice} L'indice de l'élement à supprimer
 * Exemple : arrayRemove(this.aTableau,i);
 */
function arrayRemove() {
    if (arguments[1] > 0) {
        var _temp = arguments[0].splice(0, arguments[1]);
        arguments[0].shift();
        while (_temp.length > 0) {
            arguments[0].unshift(_temp.pop());
        }
    } else {
        arguments[0].shift();
    }
}
/**
 *pausecomp
 * Arrète l'exécution du programme pendant une durée.
 * @param {Number} millis - Le nombre de millisecondes 
 * Le processeur tourne à fond !!!!!
 */
function pausecomp(millis) {
    cosole.log("pausecomp : Le processeur tourne à fond !!!!!");
    var date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    } while (curDate - date < millis);
}

// Essaye de mettre le contenu du ficher (url) et de le mettre dans une frame
// retourne le contenu de la frame (= le contenu du fichier url)
function getFileContent(id_iframe, url)
{
    var retour = "";
    // url = 'tmp/nomDeFichierBidonPourTest.htm';
    document.getElementById('frameGetTextFichier').src = url;
    //res://ieframe.dll/unknownprotocol.htm#vfps://acquitte('tmp/MAJGrille7.html')
    try {
        retour = window.frameGetTextFichier.document.body.innerHTML;
    } catch (e) {
        myAlert('Erreur getFileContent : ' + e.message);
    }
    return retour;
}

//0 =>position en X de la barre de scroll
//1 =>position en Y de la barre de scroll 
function getScrollPosition() {
    return Array(
            (document.documentElement && document.documentElement.scrollLeft) || window.pageXOffset || self.pageXOffset || document.body.scrollLeft
            , (document.documentElement && document.documentElement.scrollTop) || window.pageYOffset || self.pageYOffset || document.body.scrollTop
            );
}

// --------------------------------------------------------------------------------------------------------------------
// Fonction qui permet d'afficher une div d'avertissement si Google Chrome Frame n'est pas utilisé
// AC 20121203 Début
function popupGoogleFrameUninstalled() {
    var text = "L'application n'a pas été chargée car le plugin Google Chrome Frame n'est pas installé sur votre navigateur.<br>"
            + "Pour installer Google Chrome Frame, veuillez suivre la procédure décrite <a href='http://www.google.com/chromeframe'>ici</a>.";
    popupWindow(text, true);
}

// --------------------------------------------------------------------------------------------------------------------
// Cette fonction sera appelé dans seance.js plus précisement au début du drag, une div apparaitra avec un texte à l'interieur
// avec la possibilité d'ignorer la div pour le cas de GFC et dans le cas des positions elle disparaitra automatiquement.
// AC 20121207
function popupWindow(cTexte, tlIgnore)
{
    var maDiv;
    if (cTexte) {
        // Un texte non vide a été fourni
        if (!document.getElementById('popup')) {
            // Le cadre n'existe pas encore : le créer
            maDiv = document.createElement("div");
            document.body.appendChild(maDiv);
        } else {
            // Le cadre existe déja : le réutiliser
            maDiv = document.getElementById('popup');
            maDiv.style.visibility = "visible";
        }
    } else {
        // Pas de texte : cacher le cadre
        if (!document.getElementById('popup')) {
            return;
        } else {
            maDiv = document.getElementById('popup');
            maDiv.style.visibility = "hidden";
            return;
        }
    }

    maDiv.id = 'popup';
    maDiv.className = 'divPopup';

    if (tlIgnore) {
//        var lcIgnore = "<br><div Style = 'text-align: right;'><a href='#' onClick='popupWindow()' >Fermer</a></Div>";
        var lcIgnore = "<br><div><a href='#' onClick='popupWindow()' >Fermer</a></Div>";
        cTexte += lcIgnore;
    }
    maDiv.innerHTML = cTexte;
}

// Cloner l'objet passé en paramètre
// http://stackoverflow.com/questions/728360/most-elegant-way-to-clone-a-javascript-object
function clone(obj, piStopAt, piLevel) {
    // Gestion de l'arrèt
    if (!piStopAt)
        piStopAt = 1000;
    if (!piLevel)
        piLevel = 1;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj)
        return obj;
    if (piLevel > piStopAt)
        return null;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i], piStopAt, piLevel + 1);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr))
                copy[attr] = clone(obj[attr], piStopAt, piLevel + 1);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

function decimalToHexa(iValeurDecimal) {
    var iHexa = iValeurDecimal.toString(16);
    var iTaille = iHexa.length;
    for (var i = 0; i < (6 - iTaille); i++) {
        iHexa += "0";
    }
    return iHexa;
}

// LG 20201001
// Inspiré de https://stackoverflow.com/questions/11866781/how-do-i-convert-an-integer-to-a-javascript-color
function rgbVFPToColor(num) {
    num >>>= 0;
    var r = num & 0xFF,
        g = (num & 0xFF00) >>> 8,
        b = (num & 0xFF0000) >>> 16;
    return "rgb(" + [r, g, b].join(",") + ")";
}


// Déterminer si un la variable fournie est un tableau
// LG 20150827 selon http://stackoverflow.com/questions/4775722/check-if-object-is-array
function isArray(pv)
{
    if (pv.constructor === Array)
        return true;
    else
        return false;
}

// Comparer 2 objets et renvoyer un objet contenant les différences
function compareObjects(po1, po2) {
    var loDiff = [];
    var lvp1, lvp2;
    var lvId = null;
    if (po1)
        lvId = po1["iId"];

    // 1er parcours : comparaison des valeurs des propriétés communes aux deux objets
    // et liste des propriétés de po1 qui ne sont pas dans po2
    for (var lcProp in po1) {

        if (typeof po2[lcProp] == 'undefined') {
            // Propriété inexistante dans l'objet 2
            lvp1 = po1[lcProp];
            loDiff[loDiff.length] = {_prop: lcProp, _Diff: {iId: lvId, o1: lvp1, o2: null}};
//alert("1" + JSON.stringify(loDiff)) ;
        }
    }

    // 2eme parcours : liste des propriétés de po2 qui ne sont pas dans po1
    for (var lcProp in po2) {
        if (lcProp.toUpperCase().match('TOJSON')) {
            // Se produit dans les cas IE6 : cette méthode est ajoutée et entraine un bug dans la ligne en-dessous
            // ("null est Null ou n'est pas un objet")
            /*alert("continue") ;*/
            continue;
        }
        if (typeof po1[lcProp] == 'undefined') {
            lvp2 = po2[lcProp];
            loDiff[loDiff.length] = {_prop: lcProp, _Diff: {iId: lvId, o1: null, o2: lvp2}};
//alert("2" + JSON.stringify(loDiff)) ;
        }

    }

    // 1er parcours : comparaison des valeurs des propriétés communes aux deux objets
    // et liste des propriétés de po1 qui ne sont pas dans po2
// var lcCas ;
    for (var lcProp in po1) {

        lvp1 = po1[lcProp];
        if (lvp1 == po2[lcProp])
        {
            // L'objet 2 a cette propriété, et la valeur est la même
            // RAS
// lcCas = "Un" ;
        }
// LG 20160307 old		else if (typeof lvp1 == 'object') {
        else if (typeof lvp1 == 'object' && lvp1 != null) {
            // Propriété de type objet
            var loObjDiff = compareObjects(lvp1, po2[lcProp]);
// lcCas = "Deux" ;
            if (loObjDiff.length > 0) {
                // Les deux items sont différents
                // loDiff[loDiff.length] = loObjDiff ;
                loDiff[loDiff.length] = {_prop: lcProp, _Diff: loObjDiff};
//alert("3" + JSON.stringify(loDiff)) ;
// lcCas = "Trois" ;
            }
        } else {
            // les valeurs sont différentes
            loDiff[loDiff.length] = {_prop: lcProp, _Diff: {iId: lvId, o1: lvp1, o2: po2[lcProp]}};
// lcCas = "Quatre" ;
//alert("4" + JSON.stringify(loDiff)) ;
        }

        /*
         if (lcProp != "iGroupe") {var ll = 1 ;}
         else if (lvId != 3 && lvId != 37) {var ll = 2 ;}
         else if (lvp1 == null && po2[lcProp] == null) {
         alert(lcProp + " : Les deux sont NULL, " + lvId + " " + lcCas) ;
         }
         else if (lvp1 == null || po2[lcProp] == null) {
         alert(lcProp + " : L'un des deux est NULL, " + lvId + " " + lcCas) ;
         }
         else {
         alert(lcProp + " : Aucun NULL, " + lvId + " " + lcCas) ;
         }
         */

    }

// console.log(loDiff);
// alert(JSON.stringify(loDiff)) ;
    return loDiff;
}

// D'après http://stackoverflow.com/questions/5366849/convert-1-to-0001-in-javascript
// nr	: chaine à compléter
// n	: nb de caractères à la fin
// str	: chaine de remplacement (dft = ' ')
function padLeft(nr, n, str) {
    if (nr.length > n)
        return nr;
// LG 20190411 : dft val = ' ' et plus '0'	return Array(n-String(nr).length+1).join(str||'0')+nr;
    return Array(n - String(nr).length + 1).join(str || ' ') + nr;
}

/*
 // http://stackoverflow.com/questions/30236912/javascript-convert-unicode-octal-bytes-to-text
 function encode(str) {
 return decToOctBytes(charsToBytes(str.split(''))).join(' ');
 }
 
 function decode(octBytes) {
 return bytesToChars(octToDecBytes(octBytes.split(' '))).join('');
 }
 
 function charsToBytes(chars) {
 return chars.map(function(char) {
 return char.charCodeAt(0);
 });
 }
 
 function bytesToChars(bytes) {
 return bytes.map(function(byte) {
 return String.fromCharCode(parseInt(byte, 10));
 });
 }
 
 function decToOctBytes(decBytes) {
 return decBytes.map(function(dec) {
 return ('000' + dec.toString(8)).substr(-3);
 });
 }
 
 function octToDecBytes(octBytes) {
 return octBytes.map(function(oct) {
 return parseInt(oct, 8);
 });
 }
 */

var sEncoding_Conv1 = " &nbsp; \340&agrave;à\346&aelig;æ\347&ccedil;ç\350&egrave;è\351&eacute;é\352&ecirc;ê\353&euml;ë\371&ugrave;ù";
var sEncoding_Conv2 = "\300&Agrave;À\310&Egrave;È\311&Eacute;É\312&Ecirc;Ê\324&Ocirc;Ô\306&AElig;Æ\307&Ccedil;Ç\313&Euml;Ë";
var sEncoding_Conv3 = "\242&cent;¢\245&yen;¥\251&copy;©\260&deg;°\265&micro;µ\274&frac14;¼\275&frac12;½\241&iexcl;¡\253&laquo;«\301&Aacute;Á\302&Acirc;Â\303&Atilde;Ã\304&Auml;Ä\305&Aring;Å\314&Igrave;Ì\315&Iacute;Í\316&Icirc;Î\317&Iuml;Ï\320&ETH;Ð\321&Ntilde;Ñ\322&Ograve;Ò\323&Oacute;Ó\325&Otilde;Õ\326&Ouml;Ö\330&Oslash;Ø\331&Ugrave;Ù\332&Uacute;Ú\333&Ucirc;Û\334&Uuml;Ü\335&Yacute;Ý\336&THORN;Þ\337&szlig;ß\243&pound;£\247&sect;§\256&reg;®\254&not;¬\266&para;¶\276&frac34;¾\277&iquest;¿\273&raquo;»\341&aacute;á\342&acirc;â\343&atilde;ã\344&auml;ä\345&aring;å\354&igrave;ì\355&iacute;í\356&icirc;î\357&iuml;ï\360&eth;ð\361&ntilde;ñ\362&ograve;ò\363&oacute;ó\364&ocirc;ô\365&otilde;õ\366&ouml;ö\370&oslash;ø\372&uacute;ú\373&ucirc;û\374&uuml;ü\375&yacute;ý\376&thorn;þ\377&yuml;ÿ";

// Enlever les codes &eacute;, ... du texte fourni
function texte_RmvCodes(psStr, piNiveau) {
    return private_texte_ChangeCodes(psStr, piNiveau, 1)
}
// Encoder le texte fourni avec &eacute;, ...
function texte_EnCode(psStr, piNiveau) {
    return private_texte_ChangeCodes(psStr, piNiveau, 2)
}
// Enlever les codes &eacute;, ... du texte fourni
// psStr		: chaine à convertir
// piNiveau		: niveau (vide ou 0 : que minuscules accentuées en francais; 1 : + majuscules accentuées en français ; 2: tous caractères
// piSens		: 1 = "&eacute;" -> "é"
//				: 2 = "é" -> "&eacute;"
function private_texte_ChangeCodes(psStr, piNiveau, piSens) {
    var lsConv = sEncoding_Conv1;					// Convertir les lettres françaises usuelles
    if (piNiveau > 0)
        lsConv += sEncoding_Conv1;	// Convertir aussi les lettres majuscules accentuées
    if (piNiveau > 1)
        lsConv += sEncoding_Conv2;	// Convertir tous les caractères bizarres

    var laConv = lsConv.split("");
    var laCar, lsCar
    var liSource = 1;
    var liDest = 0;
    if (piSens == 2) {
        liSource = 2;
        var liDest = 1;
    }
    for (var liCar in laConv) {
        laCar = laConv[liCar].split("")
        lsCar = laCar[liSource];
        if (lsCar) {
            psStr = psStr.replace(new RegExp(lsCar, 'g'), laCar[liDest]);
        }
    }
    return psStr;
}

//-----------------------------------------------------------------------------------------
// Trouver tous les objets DOM qui se trouvent au point (X,Y), de la classe demandée
// X					: abscisse
// Y 					: ordonnée
//(className)			: si on souhaite se limiter aux objets de cette classe
//(classNameForStop)	: nom de classe à partir de laquelle on arrête de chercher
//(profondeurMax)		: profondeur max. autorisée (Dft : 10)
//						: semble particulièrement important sous ie <=8 dans le cas de css :before
// Selon http://stackoverflow.com/questions/8813051/determine-which-element-the-mouse-pointer-is-on-top-of-in-javascript
function allElementsFromPoint(x, y, className, classNameForStop, profondeurMax) {

    // Vérifier qu'on n'est pas déja en train de faire ce traitement
    if (window.allElementsFromPointProcessing)
        return [];
    window.allElementsFromPointProcessing = true;
    if (!profondeurMax)
        profondeurMax = 10;
// return [] ;

    try {
        var element, elements = [];
        var old_visibility = [];
        var liProfondeur = 0;
        while (true) {
            liProfondeur++;
            element = document.elementFromPoint(x, y);
            if (!element || element === document.documentElement) {
                break;
            }
            if (liProfondeur > profondeurMax)
                break;
// popupWindow("allElementsFromPoint : " + element.id) ;
            elements.push(element);
            old_visibility.push(element.style.visibility);
            element.style.visibility = 'hidden'; // Temporarily hide the element (without changing the layout)
            if (classNameForStop && element.className == classNameForStop) {
                // On arrive à la profondeur max souhaitée
                break;
            }
        }
        for (var k = elements.length - 1; k >= 0; k--) {
            elements[k].style.visibility = old_visibility[k];
            if (className && elements[k].className !== className) {
                // Cet élément n'est pas de la classe demandée : l'enlever
                elements.splice(k, 1);
            }
        }
    } catch (e) {
        console.log("Erreur dans allElementsFromPoint : " + e);
    }

    // Restaurer l'environnement
    elements.reverse();
    window.allElementsFromPointProcessing = false;

    return elements;
}











/**
 * myAlert, affiche les informations de débuggage dans la div debug
 * @param {String} param - Chaine de caractères à afficher
 * Cette fonction ne doit pas être utiliser si la page ne contient pas la div debug
 */
function myAlert(pMsg, pCouleur)
{
    if (window.activeDebuggage)
    {
        if (pMsg == null) {
            pMsg = 'MyAlert : pMsg = null';
        }
        if (pCouleur == null) {
            pCouleur = "black";
        }
        document.getElementById('debug').innerHTML = '<FONT color ="' + pCouleur + '">'
                + pMsg + '</FONT>' + '<br>' + document.getElementById('debug').innerHTML;
    } else
    {
        return;
    }
}

/**
 * clearDebug, vide les informations de débuggage dans la div debug
 */
function clearDebug() {
    if (window.activeDebuggage)
        document.getElementById('debug').innerHTML = '';
}

// Logger le code obsolète
function obsolete(psLog) {
    var lsLog = 'Obsolète : ' + (psLog ? psLog : 'pas de log');
    console.log(lsLog);
}

// AC 20121030 Début
// Cette fonction permet de gérer les erreurs liées au JavaScript, elle nous renvoie:
// - Un message d'erreur
// - Le numéro de la ligne
// - Le fichier concerné
window.onerror = function (message, url, linenumber, columnNo, error) {

    var lcErr = "";
    if (!error) {/*alert("!error") ;*/
    } // RAS
    else if (!error.stack) {/*alert("!error.stack") ;*/
    } // RAS
    else {
        // L'erreur décrite dans stack est plus riche
        lcErr = error.stack;
    }
// LG 20180724 début
//  lcErr = null ;
    lcErr = "Erreur de JavaScript : " + message
            + "\ndans le fichier " + url
            + "\nà la ligne " + linenumber
            + "\nPile des appels javascript : \n" + lcErr;// LG 20180724 fin

//	var lcStack = stacktrace() ;
//	lcErr+= "<br>" + lcStack ;

    // Afficher l'erreur
    if (document.getElementById("Erreurs")) {
        document.getElementById("Erreurs").innerHTML = "<B>Oups, une erreur s'est produite : </B><br><br>"
                + lcErr;
    }

    // Informer le serveur
    if (typeof LG_Ajax_Notifie === "function") {
        LG_Ajax_Notifie(lcErr, "Erreur", window.onNotifie_callback);
    } else {
        console.error("La fonction LG_Ajax_Notifie n'est pas présente dans l'application, l'erreur n'a pas pu être envoyée au serveur.");
    }

    if (typeof isWebBrowser === "function" && isWebBrowser()) {
        // L'appli tourne dans un controle webBrowser, hébergé par un formulaire VFP par exemple
        // Annuler le message standard
        // permet de neutraliser le message standard de IE ou autre
        return true;
    } else {
        // On est en mode test non webBrowser : afficher l'erreur
// LG 20200916 début
//// HB old
////        if (estModeDev()) {
////            alert(lcErr);
////        } else {
////MG Modification 20200812 Début
////Remise en place du callstack pour les erreurs JavaScript
////            var lsHTMLDialogue = "<div id='msgErreur' title='Une erreur est survenue'>"
////                                    + "<p>Dites-nous ce que vous avez fait pour obtenir cette erreur. Nous la réparerons le plus rapidement possible.</p>"
////                                    + "<p>Désolé pour la gène occasionnée.</p>"
////                                    + "<textarea id='msgErreur_commentaireUtilisateur' style='width: 100%;height:110px;'></textarea>"
////                                    + "<input id='msgErreur_idLog' type='hidden'>"
////                                    + "</div>";
//			var lsHTMLDialogue = "<div id='msgErreur' title='Une erreur est survenue'>"
//								+ "<p>Désolé pour la gène occasionnée.</p>"
//								+ lcErr
//								+ "<textarea id='msgErreur_commentaireUtilisateur' style='width: 100%;height:110px;'></textarea>"
//								+ "<input id='msgErreur_idLog' type='hidden'>"
//								+ "</div>";
////MG Modification 20200812 Fin
        $("#patientez").remove()        // Masquer la popup d'attente, si elle est visible
        if (estModeDev()) {
            var lsHTMLDialogue = "<div id='msgErreur' title='Une erreur est survenue'>"
                    + lcErr.replace(/(?:\r\n|\r|\n)/g, "<br>")
                    + "</div>";
        } else {
            var lsHTMLDialogue = "<div id='msgErreur' title='Une erreur est survenue'>"
                    + "<p>Nous sommes désolés pour la gène occasionnée, mais vous pouvez nous dire ce que vous avez fait pour obtenir cette erreur. Nous la réparerons le plus rapidement possible.</p>"
                    + "<textarea id='msgErreur_commentaireUtilisateur' style='width: 100%;height:110px;'></textarea>"
                    + "<input id='msgErreur_idLog' type='hidden'>"
                    + "</div>";
        }
// LG 20200916 fin

// LG 20200916 old            var dialog = $(lsHTMLDialogue).dialog({width: 800, height: 295, close: function (e, o) {
        var dialog = $(lsHTMLDialogue).dialog({width: 800, height: 295, modal: true, close: function (e, o) {
                $('#msgErreur').dialog('close');
            },
            buttons: {
                "OK": function (event, ui) {
                    if ($("#msgErreur_commentaireUtilisateur").val()) {
                        var lsMsg = $("#msgErreur_commentaireUtilisateur").val();
                        if (typeof LG_Ajax_Notifie === "function") {
                            LG_Ajax_Notifie(lsMsg, "Commentaire utilisateur en réponse au bug javascript", null, $("#msgErreur_idLog").val());
                        }
                    }
                    dialog.dialog('destroy');
                }
            }});
//        }
    }
}

// LG 20180724
// Callback quand l'enregistrement de l'erreur est fini
window.onNotifie_callback = function (psResponseText) {
    // Affecter l'identifiant du log à l'input caché de la boite de dialogue d'affichage de l'erreur
    if ($("#msgErreur_idLog").length > 0) {
        // La boite de dialogie existe
        var loResponse = JSON.parse(psResponseText);
        if (loResponse.idLog) {
            var lcIdLog = loResponse.idLog;
            $("#msgErreur_idLog").val(lcIdLog);
        }
    }
}

// La fonction myError utilise la gestion d'erreur en JavaScript (widow.onerror), elle permet simplement d'économiser du code
function myError(poErreur, psSource) {
    myAlert((poErreur.message ? "Erreur catchée" : "myError") + (psSource ? " dans " + psSource : "") + " : " + poErreur, "red");
    return true;
}

// Ajouter/enlever un listener d'événements sur un élément DOM
function addListener(element, type, callback, capture) {
// LG 20190829 : obsolète, utiliser $(...).on()
    if (element.addEventListener) {
        element.addEventListener(type, callback, capture);
    } else {
        element.attachEvent("on" + type, callback);
    }
}
function removeListener(element, type, callback, capture) {
//LG 20190829 : obsolète, utiliser $(...).off()
    if (element.removeEventListener) {
        element.removeEventListener(type, callback, capture);
    } else {
        element.detachEvent("on" + type, callback);
    }
}

// Renvoie true si on est dans un contexte Prototype
function isPrototype() {
    return (window.Prototype);
}

// Quel bouton était eppuyé lors d'un événement ?
function eventButtons(e) {
    if (isPrototype())
        return e.buttons;
    else
        return e.which;
}

// AC 20121126
// Cette fonction sera utile pour Foxpro lorsque l'utilisateur fera un action pendant qu'il maintient une touche (shift,ctrl et alt)
// assuming you have overflow: hidden and white-space: nowrap
function eventKeyCode(e) {
    var liKeyCode = 0; // l'Identifiant de l'ensemble des touches pressés
    if (e.altKey)
        liKeyCode = liKeyCode + 4;
    if (e.ctrlKey)
        liKeyCode = liKeyCode + 2;
    if (e.shiftKey)
        liKeyCode = liKeyCode + 1;
    return liKeyCode;
}

// LG 20170201
// Astuce fournie par danorton (http://stackoverflow.com/questions/3485365/how-can-i-force-webkit-to-redraw-repaint-to-propagate-style-changes/3485654#3485654)
// pour que l'apparence soit correctement rafraichie (sinon la place des scrollbars pose pb)
function forceRefreshElement(psSelecteur) {
    var sel = $(psSelecteur)[0];
    if (!sel) {
        console.log("forceRefreshElement : élément " + psSelecteur + " non trouvé.");
        return false;
    }
    sel.style.display = 'none';
    sel.offsetHeight; // no need to store this anywhere, the reference is enough
    sel.style.display = '';
    return true;
}

// http://stackoverflow.com/questions/3710204/how-to-check-if-a-string-is-a-valid-json-string-in-javascript-without-using-try
function tryParseJSON(jsonString) {
    try {
        var o = JSON.parse(jsonString);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns null, and typeof null === "object", 
        // so we must check for that, too. Thankfully, null is falsey, so this suffices:
        if (o && typeof o === "object") {
            return o;
        }
    } catch (e) {
    }

    return false;
}
;

// Retrouver le nom du répertoire parent de l'item fourni
function dirname(e) {
    e = e.split('/');           //break the string into an array
    e.pop();                    //remove its last element
    return e.join('/');
}

// Afficher un message d'attente
// psMsg		: le message à afficher (si vide, msg par défaut : "Traitement en cours")
// psDivParent	: l'élément HTML à l'intérieur duquel centrer la fenêtre de message
function patientez(psMsgText, poEltParent) {
    poEltParent = poEltParent ? poEltParent : $("body");
    /*if ($("#patientez"))*/ $("#patientez").remove();
    psMsgText = psMsgText ? psMsgText : 'Traitement en cours';
//    var lsHTML = '<div id="patientez" style="background-color: lightgray;top: 50%;position: absolute;left: 50%;height: 200px;width: 50%;z-index: 9999;margin: 0;transform: translate(-50%, -50%);border: 3px solid darkgrey;border-radius: 5px;padding: 10px;">'
//            + '<img src="' + getBaseURL() + '../Images/loader3.gif" style="" />'
//            + psMsgText + ', merci de bien vouloir patienter ...'
//            + '</div>'
    var lsHTML = '<div id="patientez" style="background-color: lightgray;top: 50%;position: absolute;left: 50%;height: 200px;width: 50%;z-index: 9999;margin: 0;transform: translate(-50%, -50%);border: 3px solid darkgrey;border-radius: 5px;padding: 10px;">'
// LG 20200930 old            + '<img src="/Images/loader3.gif" style="" />'
            + '<img src="' + getBaseURL(true) + 'Images/loader3.gif" style="" />'
            + psMsgText + ', merci de bien vouloir patienter ...'
            + '</div>' ;
    $(poEltParent).append(lsHTML);
}
function patientezFin() {
    $("#patientez").remove();
}

// LG 20190510
// Recharger et réexécuter tous les scripts de la page
// Problème : la console Chrome (+ FireFox) n'est pas rechargée et on voit toujours l'ancienne version du code
function reloadAllJSScripts() {
    const links = document.getElementsByTagName('script');
    Array.from(links)
            .filter(link => link.src)
            .forEach(link => {
//      const url = new URL(link.src, location.href);
//      url.searchParams.set('forceReload', Date.now());
//      link.src = url.href;
//		var lsSrc = link.src ;		// ne convient pas car URL absolue alors que dans la balise on a pu la mettre relative
                var lsSrc = link.outerHTML; //<script type="text/javascript" src="vendor/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
                lsSrc = lsSrc.split('src="')[1].split('"')[0];
                reloadJS(lsSrc);
            });
}
function reloadJS(psScript) {
//	psScript = $('script[src$="' + psScript + '"]').attr("src");
    $('script[src="' + psScript + '"]').remove();
    psScript = psScript.split('?')[0];
    const url = new URL(psScript, location.href);
    url.searchParams.set('forceReload', Date.now());
    psScript = url.href;
    $('<script>').attr('src', psScript).appendTo('head');
}

// Récupérer l'URL de base du site
// LG 20200512
// LG 20201001 début
//function getBaseURL() {
// (pbPublic)   : true pour obtenir le chemin du répertoire Public
//              : false pour obtenir un chemin valide pour les pages Symfony
function getBaseURL(pbPublic) {
//    if (window.baseURL) {
//        // window.baseURL est posé par menu.html.twig
//        var lsURL = window.baseURL;
//    } else if (window.location.toString().includes('index.php')) {
//        //Pour l'utilisation sans vhost
//        var lsLocation = window.location.toString();
//        var lsURL = lsLocation.substring(0, lsLocation.indexOf("index.php")) + "index.php/";
    if (window.location.toString().includes('index.php')) {
        //Pour l'utilisation sans vhost
        var lsLocation = window.location.toString();
        var lsURL = lsLocation.substring(0, lsLocation.indexOf("index.php")) + "index.php/";
    } else if (window.baseURL) {
        // window.baseURL est posé par menu.html.twig
        var lsURL = window.baseURL;
    // LG 20201001 fin
    } else {
        console.error("baseURL n'est pas défini");
        var lsURL = new URL(window.location.origin).toString();
    }
// LG 20201001 début
    if (lsURL.indexOf("index.php") > 0 && pbPublic) {
        lsURL = lsURL.replace("index.php", "") ;
    }
//    lsURL = lsURL.replaceAll("//", "/") ;
// LG 20201001 fin

    return lsURL;
}

// Déterminer si on est en mode développement
// LG 20200701
function estModeDev() {
    if ($("#modeDevProd").length > 0) {
        var lsMode = $("#modeDevProd").html();
        return lsMode == "dev";
    } else {
        console.error("modeDevProd n'est pas défini");
        return true;
    }
}
