// Surcharge de DataTables pour ajouter les spécificités Limoog
// LG 20190220

// Initialiser une DataTable à la sauce Limoog
// psIdElement		: l'id de l'élément HTML qui contient le tableau HTML qui sert de base de la grille
// psLibelléEntité	: nom de l'entité de BDD traitée dans la DataTable
// poDataTableOwner : l'objet dans la propriété oDataTable duquel on va stocker l'objet LGDataTable généré
//						et qui sera le parent du DataTable généré
// pfOnInitComplete	: fonction à exécuter quand l'initialisation de la LGDataTable sera complète
//  tout Cette Fonction est Reprise De la Fonction LGDataTAbe_Activite 
function YCDataTable_Activite(PoNomTable, poDataTableOwner) {
    // Définir le DataTable à la sauce Limoog
// CODE JAMAIS EXECUTE
	
	jQuery.fn.extend({
        FunctionListActi: function (paOptions) {
            // Créer lA dataTable
            var laOptionsDft = {

                "initComplete": function (settings, json) {
                    // S'exécute lorsque l'initialisation du dataTable est complete
                    alert("DataTable.initComplete : il faut surcharger cette fonction");
                },

            }
            // Préparer les options définitives
            var laOptions;
            if (paOptions) {
                // On a fourni des options : les fusionner avec celles par défaut
                laOptions = $.extend(true, {}, laOptionsDft, paOptions);
            } else {
                laOptions = laOptionsDft;
            }
            // création de la datatable
            return $(this).DataTable(laOptions);
            // Cet objet retourné semble être le même que celui obtenu dans le InitComplete par "new $.fn.dataTable.Api(settings)"
        }
    });
    var lsLstColonnes = document.querySelectorAll('Titres th'); // Préparer la liste des colonnes de la grille (plus tard la même commande en trouve 2 fois trop ???)
    $('#' + PoNomTable/*entityFormGrille_DataTable*/).css('visibility', 'visible');

    function onDataTableInitComplete(settings, json)
    {
         
        var firstLine;
        firstLine = settings.aiDisplay[0];
         var loDataTable = new $.fn.dataTable.Api(settings);		// Récupérer l'objet dataTable (le même que celui renvoyé par $('#entityFormGrille_DataTable').LGDataTable ?
        var thisObject = loDataTable;
        // Pas Besoin de sa 
	if (poDataTableOwner) {
            poDataTableOwner.oDataTable = loDataTable ;
            loDataTable.parent = poDataTableOwner ;
        }

        loDataTable.sIdDataTable = PoNomTable ;
        loDataTable.bIsSelectingRow = false ;
	loDataTable.oSelectedRow = null ;
        loDataTable.oSelectedRow_AvantNouveau = null ;
        loDataTable.oSelectedRowDiv = function () {
            return $(thisObject.oSelectedRow.node());
        };
        loDataTable.aColonnes = lsLstColonnes ;
   	loDataTable.DoRowSelect = function(poRow) {
			if (thisObject.bIsSelectingRow) {
				// On est déja en train de sélectionner une ligne
				return true ;
			}
			var loDivRowASelectionner = $(poRow.node());
			if (loDivRowASelectionner.hasClass('selected')) {
				// La ligne souhaitée est déja sélectionnée : RAS
				thisObject.oSelectedRow = poRow ;	
				return true ;
                                
			}
			thisObject.bIsSelectingRow = true ;

		 
			return true;
		} ;

        loDataTable.RtvIdSelectedRow = function(){
			var liId ;
			if (thisObject.oSelectedRow) liId = thisObject.oSelectedRow.data()[0] ;
			else liId = null ;
			return liId ;
		}

		// Pour ne pas perdre cette manière de récupérer laligne sélectionnée
		loDataTable.RtvSelectedRow = function() {
			var loSelectedRowDiv = thisObject.RtvSelectedRowDiv() ;
			var loSelectedRow = null ;
			if (loSelectedRowDiv) {
				loSelectedRow = thisObject.row(loSelectedRowDiv) ;
			}
			return loSelectedRow ;
		};

		// Pour ne pas perdre cette manière de récupérer laligne sélectionnée
		loDataTable.RtvSelectedRowDiv = function() {
			var loSelectedRowDiv = thisObject.$('tr.selected');
			return loSelectedRowDiv ;
		};
                		loDataTable.DoRowSelect(loDataTable.row(firstLine));

         		//loDataTable.DoRowSelect(loDataTable.row(firstLine));
                $('#' + PoNomTable /*entityFormGrille_DataTable*/ + ' tbody').on('click', 'tr', function () {
			// Sélectionner la ligne deméndée, si c'est possible
			//loDataTable.DoRowSelect(thisObject.parent.oDataTable.row(this)) ;
                console.log("FunctionOnclick")
                console.log(thisObject);
                });
		
         
    }

    var laOptions = {"initComplete": onDataTableInitComplete};
    $('#' + PoNomTable).FunctionListActi(laOptions);

}