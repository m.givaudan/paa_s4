/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * code extrait de http://ejohn.org/blog/simple-javascript-inheritance/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
 
  // The base _Class implementation (does nothing)
  this._Class = function(){};
 
  // Create a new _Class that inherits from this class
  _Class.extend = function(prop) {
	var _super = this.prototype;
   
	// Instantiate a base class (but only create the instance,
	// don't run the init constructor)
	initializing = true;
	var prototype = new this();
	initializing = false;
   
	// Copy the properties over onto the new prototype
	for (var name in prop) {
	  // Check if we're overwriting an existing function
	  prototype[name] = typeof prop[name] == "function" &&
		typeof _super[name] == "function" && fnTest.test(prop[name]) ?
		(function(name, fn){
		  return function() {
			var tmp = this._super;
		   
			// Add a new ._super() method that is the same method
			// but on the super-class
			this._super = _super[name];
		   
			// The method only need to be bound temporarily, so we
			// remove it when we're done executing
			var ret = fn.apply(this, arguments);        
			this._super = tmp;
		   
			return ret;
		  };
		})(name, prop[name]) :
		prop[name];
	}
   
	// The dummy class constructor
	function _Class() {
	  // All construction is actually done in the init method
	  if ( !initializing && this.init )
		this.init.apply(this, arguments);
	}
   
	// Populate our constructed prototype object
	_Class.prototype = prototype;
   
	// Enforce the constructor to be what we expect
	_Class.prototype.constructor = _Class;
 
	// And make this class extendable
	_Class.extend = arguments.callee;
   
	return _Class;
  };
})();

// Classe selon John Resig
var Class = _Class.extend({
    cClassName: "ClassAuSensDeJohnResig",
	toString: function (){return "[Instance de " + this.cClassName + "]"}
});

// Classe selon John Resig
var Exemple = Class.extend({
    cClassName: "Classe Exemple",		// Propri�t� surcharg�e
	toString: function (){return "Exemple de " + this._super()}		// M�thode surcharg�e
});

