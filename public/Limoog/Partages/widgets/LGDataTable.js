// Surcharge de DataTables pour ajouter les spécificités Limoog
// LG 20190220

// Initialiser une DataTable à la sauce Limoog
// psIdElement		: l'id de l'élément HTML qui contient le tableau HTML qui sert de base de la grille
// psLibelléEntité	: nom de l'entité de BDD traitée dans la DataTable
// poDataTableOwner : l'objet dans la propriété oDataTable duquel on va stocker l'objet LGDataTable généré
//						et qui sera le parent du DataTable généré
// pfOnInitComplete	: fonction à exécuter quand l'initialisation de la LGDataTable sera complète
function LGDataTable_initGrille(psIdElement, psLibelléEntité, poDataTableOwner, pfOnInitComplete) {

    // Définir le DataTable à la sauce Limoog
    jQuery.fn.extend({
        LGDataTable: function (psLibelléEntité, paOptions) {
            // Options par défaut pour LGDataTable
//MG 20200330 Modification Début
            var infoEmpty = "Aucun " + psLibelléEntité + " à afficher";
            if (psLibelléEntité === "Série") {
                infoEmpty = '';
            } else if (psLibelléEntité === "salle"){
				psLibelléEntité = "équipement";
			}
//MG 20200330 Modification Fin
            var laOptionsDft = {
                //"bSort": false,
                "aoColumnDefs": [
                    {"bSortable": false, "aTargets": [0]}
                ],
                "language": {// lang-fr
                    processing: "Traitement en cours...",
                    search: "",
                    searchPlaceholder: "Filtrer",
                    lengthMenu: "Afficher _MENU_ " + psLibelléEntité,
                    info: "Affichage des " + psLibelléEntité + "s _START_ &agrave; _END_ sur _TOTAL_",
//MG 20200330 Modification Début
//Tableau rempli apres le chargement en JS pour les séries, le message est forcement affiché
//De plus, le tableau ne doit pas être vide dans séances séries
//					infoEmpty: "Aucun " + psLibelléEntité + " à afficher",
                    infoEmpty: infoEmpty,
//MG 20200330 Modification Fin
                    infoFiltered: "(filtr&eacute;, _MAX_ " + psLibelléEntité + "s au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                "initComplete": function (settings, json) {
                    // S'exécute lorsque l'initialisation du dataTable est complete
                    alert("DataTable.initComplete : il faut surcharger cette fonction");
                },
                "order": [[1, "asc"]], // Ordre croissant de la deuxieme colonne - A MODIFIER
                "scrollY": 180, // Scrollbar Vertical
                "scrollX": true, // Scrollbar horizontal
                "scrollCollapse": false, // true : Allow the table to reduce in height when a limited number of rows are shown
                "paging": false, // Ne pas paginer
                "pageResize": true,
                //		 columnDefs: [
                //		 {visible: false, targets: 0}
                //		 ]
            }

            // Préparer les options définitives
            var laOptions;
            if (paOptions) {
                // On a fourni des options : les fusionner avec celles par défaut
                laOptions = $.extend(true, {}, laOptionsDft, paOptions);
            } else {
                laOptions = laOptionsDft;
            }

            // Créer lA dataTable
            return $(this).DataTable(laOptions);	// Cet objet retourné semble être le même que celui obtenu dans le InitComplete par "new $.fn.dataTable.Api(settings)"
        }
    });
// LG 20190220 fin

// LG 20190220 old	window.dataTable_aColonnes = document.querySelectorAll('#entityFormGrille_DataTable_titres th'); // get all children within container
    var lsLstColonnes = document.querySelectorAll('#' + psIdElement + 'Titres th'); // Préparer la liste des colonnes de la grille (plus tard la même commande en trouve 2 fois trop ???)
    $('#' + psIdElement).css('visibility', 'visible');

    // Déclenchée quand la DataTable est completement initialisée
    // voir https://datatables.net/reference/option/initComplete
    function onDataTableInitComplete(settings, json) {
        // S'exécute lorsque l'initialisation du dataTable est complete
        var firstLine;
        firstLine = settings.aiDisplay[0]; // Récupération de la première ligne du datatable
        var loDataTable = new $.fn.dataTable.Api(settings);		// Récupérer l'objet dataTable (le même que celui renvoyé par $('#entityFormGrille_DataTable').LGDataTable ?
        var thisObject = loDataTable;
        loDataTable.nomClasse = "LGDataTable";

        // Affecter l'objet global "dataTable_oDataTable"
        poDataTableOwner.oDataTable = loDataTable;
        loDataTable.parent = poDataTableOwner;

        // Compléter l'objet DataTable pour lui ajouter les propriétés et méthodes nécessaires
        loDataTable.sIdDataTable = psIdElement;
        loDataTable.bIsSelectingRow = false;
        loDataTable.oSelectedRow = null;
        loDataTable.oSelectedRow_AvantNouveau = null;
        loDataTable.oSelectedRowDiv = function () {
            return $(thisObject.oSelectedRow.node());
        };
        loDataTable.aColonnes = lsLstColonnes;

        // Sélectionner la ligne demandée
        loDataTable.DoRowSelect = function (poRow) {
            if (thisObject.bIsSelectingRow) {
                // On est déja en train de sélectionner une ligne
                return true;
            }
            var loDivRowASelectionner = $(poRow.node());
            if (loDivRowASelectionner.hasClass('selected')) {
                // La ligne souhaitée est déja sélectionnée : RAS
                thisObject.oSelectedRow = poRow;
                return true;
            }
            thisObject.bIsSelectingRow = true;

            // Essayer de déselectionner la ligne actuellement active, avec enregistrement de la ligne active si elle a été modifiée
            if (thisObject.parent.oEntityForm) {
                thisObject.parent.oEntityForm.EnregistreSiNecessaire(
                        thisObject.parent.ChangeSelectedRow
                        , Array(poRow)
                        , thisObject.parent.CancelChangeSelectedRow);
            } else {
                thisObject.ChangeSelectedRow(Array(poRow));
                thisObject.bIsSelectingRow = false;
            }
            return true;
        };

        // Fonction callback de sélection d'une nouvelle ligne
        // paData : Array(poRow, liId)
        // paData[0] = poRow	: la ligne à sélectionner
        loDataTable.ChangeSelectedRow = function (paData) {
            var loDivRowActuellementSelectionnée;
            loDivRowActuellementSelectionnée = thisObject.RtvSelectedRowDiv();
            if (loDivRowActuellementSelectionnée) {
                loDivRowActuellementSelectionnée.removeClass('selected');
            }

            // Sélectionner la ligne demandée
            var loRowASelectionner = paData[0];
            thisObject.oSelectedRow = loRowASelectionner;
            thisObject.oSelectedRowDiv().addClass('selected');

            return loRowASelectionner;
        }

        // Ajouter une nouvelle ligne à la grille
        // pbDuplique : true pour dupliquer la ligne actuelle
        loDataTable.DoAjouteNouvelleLigne = function (pbDuplique) {
            var loNouvelleLigne = /*dataTable_*/thisObject.RtvTableauNouvelleLigne(pbDuplique);
            var rowNode = thisObject.row.add(loNouvelleLigne);
            rowNode = rowNode.draw(false);
            $(rowNode.node()).children()[0].setAttribute("style", "display: none");	// La première colonne est masquée
            $(rowNode.node()).children()[0].setAttribute("name", "0");
// LG 20200508 old            var liNouveauNoLigne = thisObject.rows()[0].length;
            var liNouveauNoLigne = thisObject.rows()[0].length - 1;
            var laColonnes = thisObject.aColonnes;
            for (i = 0; i < laColonnes.length; i++) {
                var lcIdColonne = laColonnes[i].id;
                $(rowNode.node()).children()[i].setAttribute("id", "row_" + liNouveauNoLigne + "_" + lcIdColonne);
            }
            thisObject.oSelectedRow_AvantNouveau = thisObject.oSelectedRow;
            thisObject.oSelectedRowDiv().removeClass('selected');
            thisObject.oSelectedRow = rowNode;
            thisObject.oSelectedRowDiv().addClass("selected");

        }

        // Effacer la ligne actuellement sélectionnée
        // Valeur de retour : Id de l'entité présene sur la ligne qui devient sélectionnée
        loDataTable.DoDeleteRow = function () {
            //	var lbEstNouvelleLigne = dataTable_EstNouvelleLigne() ;
            var lbEstNouvelleLigne = thisObject.EstNouvelleLigne();
            if (lbEstNouvelleLigne) {
                // On était sur une nouvelle ligne : recharger la ligne mémorisée
                thisObject.oSelectedRowDiv().remove().draw;
                thisObject.oSelectedRow.remove();
                thisObject.oSelectedRow = thisObject.oSelectedRow_AvantNouveau;
            } else {
                // On était sur une ligne réelle : charger la ligne précédente s'il y en a une
                var liRowActuelle = thisObject.oSelectedRow.index();

                // Trouver le N° d'ordre de la ligne actuelle (les lignes sont triées et il faut tenir compte de ce tri)
                var laNumérosRows = thisObject.rows()[0];
                var liNumeroRowActuelle = 0;
                for (i = 0; i < laNumérosRows.length; i++) {
                    if (laNumérosRows[i] == liRowActuelle) {
                        // On a trouvé la ligne actuellement sélectonnée
                        liNumeroRowActuelle = i;
                        break;
                    }
                }

                thisObject.oSelectedRowDiv().remove().draw;
                thisObject.oSelectedRow.remove();
                laNumérosRowsNouv = thisObject.rows()[0];

                var liRowAActiver = -1;
                if (liNumeroRowActuelle > 0) {
                    liRowAActiver = laNumérosRowsNouv[liNumeroRowActuelle - 1];
                } else if (laNumérosRows.length > 1) {
                    liRowAActiver = laNumérosRowsNouv[0];
                }

                if (liRowAActiver >= 0) {
                    var loRowAActiver = thisObject.row(liRowAActiver);
                    thisObject.oSelectedRow = loRowAActiver;
                } else {
                    thisObject.oSelectedRow = null;
                }
            }

            var liId;
            if (thisObject.oSelectedRow) {
                // LG 20190220 deac		window.dataTable_oSelectedRowDiv = $(window.entityFormGrille.oDataTable.oSelectedRow.node()) ;
                thisObject.oSelectedRowDiv().addClass("selected");
                liId = thisObject.oSelectedRow.data()[0];
            } else {
                liId = 0;
                // LG 20190220 deac		window.dataTable_oSelectedRowDiv = null ;			
            }

            return liId;
        };

        // Mettre en cache le contenu des colonnes du dataTable pour la ligne courante
        loDataTable.MetEnCacheValeursColonnes = function (piIdLigne) {
            var laValeursChamps = new Array();
            laValeursChamps[0] = piIdLigne;
            if (typeof thisObject.parent.oEntityForm.RtvValeurChamp === "function") {
                // La fonction  existe : on a bien un formulaire actif
                // Pour toutes les colonnes de la datatable
                var laColonnes = thisObject.aColonnes;
                for (var i = 1; i < laColonnes.length; i++) {
                    laValeursChamps[i] = thisObject.parent.oEntityForm.RtvValeurChamp(laColonnes[i].id);
                }
            }

            /*window.dataTable_*/thisObject.aValeursChamps = laValeursChamps;
        };

        // Restaurer les valeurs de la ligne sélectionnée dans le datatable avec les valeurs précédemment mises en cache
        loDataTable.RestoreValeursColonnesAvecCache = function (piIdEntité) {
//			debugger;
            //	if (!window.dataTable_aValeursChamps) {
            if (!thisObject.aValeursChamps) {
                console.log("LGDataTable.aValeursChamps n'est pas renseigné");
                return false;
            } else {
                //		var laValeursChamps = window.dataTable_aValeursChamps ;
                var laValeursChamps = thisObject.aValeursChamps;
            }

            var lcIdColonne = thisObject.aColonnes[0].id;

// LG 20200508 début : le sélecteur n'était pas adapté			
////MG Modification 20200303 Début
//// L'élément td n'a pas encore d'attribut 'name', loElt retourne donc undefined
//// lcName plus utilisé
////			var lcName = laValeursChamps[0] ;
////			var lcSelecteur = "td[id$=_" + lcIdColonne + "][name=" + lcName + "]" ;
//			var lcSelecteur = "td[id$=_" + lcIdColonne + "]" ;
//			var loElt = $(lcSelecteur).eq(0) ;
            var lcSelecteur = "td[id$=_" + lcIdColonne + "]";
// LG 20200907 old            var loLigneSelectionnée = $("#entityFormGrille_DataTable tr.selected");
            var loLigneSelectionnée = $("#" + psIdElement + " tr.selected");
            var loElt = loLigneSelectionnée.find(lcSelecteur);
            if (piIdEntité && !thisObject.aValeursChamps[0]) {
                //L'id de la nouvelle entité n'est pas encore dans le tableau
                thisObject.aValeursChamps[0] = piIdEntité;
            }
// LG 20200508 fin

            if (piIdEntité) {
                loElt.attr('name', piIdEntité);
                loElt.text(piIdEntité);
            }
//MG Modification 20200303 Fin

            var lcAttr = loElt.attr('id');
            var noDelaLigneQuiSeraModifiée = lcAttr.split("_")[1];
// LG 20200508 début
            var laRowData = $("#" + thisObject.sIdDataTable).DataTable().data()[noDelaLigneQuiSeraModifiée];
// LG 20200508 fin
            // Pour chaque colonne
            var laColonnes = thisObject.aColonnes;
            for (var i = 0; i < laColonnes.length; i++) {
                if (laValeursChamps[i] !== undefined) {
                    if (laColonnes[i].id.charAt(0) === "d") {
                        // Cas des dates
                        var aSplit = laValeursChamps[i].split("-");
                        laValeursChamps[i] = aSplit[2] + "/" + aSplit[1] + "/" + aSplit[0];
                    }

                    if (laColonnes[i].id.charAt(0) === "t") {
                        // Cas des DateHeures
                        var aSplit = laValeursChamps[i].substring(0, 10);
                        aSplit = aSplit.split("-");
                        laValeursChamps[i] = aSplit[2] + "/" + aSplit[1] + "/" + aSplit[0];
                    }

                    var destinationChangementLigne = "#row_" + noDelaLigneQuiSeraModifiée + "_" + laColonnes[i].id;
                    destinationChangementLigne = "#" + thisObject.sIdDataTable + " > tbody > .selected > " + destinationChangementLigne;
//					debugger;
                    // Si la donnée est une date - formater
                    laValeursChamps[i] = String(laValeursChamps[i]).replace("<", "&lt;");
// LG 20200508 début
//                    $(destinationChangementLigne).html(laValeursChamps[i]);
                    if (typeof thisObject.traduitValeurChamp === "function") {
                        // La surcharge de cet objet dispose de la méthode de traduction
                        var lsValeurChamp = thisObject.traduitValeurChamp(laColonnes[i].id, laValeursChamps[i]);
                    } else {
                        var lsValeurChamp = laValeursChamps[i];
                    }
                    $(destinationChangementLigne).html(lsValeurChamp);
                    laRowData[i] = lsValeurChamp;
// LG 20200508 fin

                }
            }
            //MG Modification 20200303 Début
            //Déplacé plus haut
//			if (piIdEntité) {
//				debugger;
//				var lcName = piIdEntité ;
//				loElt.attr('name', piIdEntité) ;
//				loElt.text(piIdEntité) ;
//			}
            //MG Modification 20200303 Fin	

            return true;
        };

        // Déclenchée par le formulaire quand l'enregistrement est fini
        loDataTable.OnFormSaved = function (piIdEntité) {
            // Mettre à jour la ligne courante du dataGrid
            //	dataTable_RestoreValeursColonnesAvecCache(piIdEntité);
            thisObject.RestoreValeursColonnesAvecCache(piIdEntité);
        }

        // Renvoie .T. si la ligne sélectionnée de la dateTable est une ligne en cours d'insertion
        loDataTable.EstNouvelleLigne = function () {
            console.warn("Obsolète");
            //	var liId = dataTable_RtvIdSelectedRow() ;
            var liId = thisObject.RtvIdSelectedRow();
            return (liId == thisObject.parent.oEntityForm.IdNouvelleEntité);
        }

        // Renvoie l'Id de l'entité qui est affichée dans la ligne sélectionnée de la dateTable
        loDataTable.RtvIdSelectedRow = function () {
            var liId;
            if (thisObject.oSelectedRow)
                liId = thisObject.oSelectedRow.data()[0];
            else
                liId = null;
            return liId;
        }

        // Pour ne pas perdre cette manière de récupérer laligne sélectionnée
        loDataTable.RtvSelectedRow = function () {
            var loSelectedRowDiv = thisObject.RtvSelectedRowDiv();
            var loSelectedRow = null;
            if (loSelectedRowDiv) {
                loSelectedRow = thisObject.row(loSelectedRowDiv);
            }
            return loSelectedRow;
        };

        // Pour ne pas perdre cette manière de récupérer laligne sélectionnée
        loDataTable.RtvSelectedRowDiv = function () {
            var loSelectedRowDiv = thisObject.$('tr.selected');
            return loSelectedRowDiv;
        };

        // Renvoyer un tableau contenant une valeur pour chaque colonne, pour une ligne nouvelle
        loDataTable.RtvTableauNouvelleLigne = function (pbDuplicata) {
            var laValeursColonnes = [];
            laValeursColonnes[0] = thisObject.parent.oEntityForm.IdNouvelleEntité;
            var lsVal, lsChamp;

            // Pour toutes les colonnes de la datatable
            var laColonnes = thisObject.aColonnes;
            for (var i = 1; i < laColonnes.length; i++) {
                lsChamp = laColonnes[i].id.toLowerCase();
                lsVal = thisObject.parent.oEntityForm.RtvValeurPourChampDeNouvelleLigne(pbDuplicata, lsChamp, thisObject.oSelectedRowDiv().children()[i].innerHTML);
                laValeursColonnes[i] = lsVal;
            }
            return laValeursColonnes;
        };

        // Sélectionner la première ligne du dataTable
        loDataTable.DoRowSelect(loDataTable.row(firstLine));

        // Activer le gestionnaire de l'événement clic sur le dataTable
        $('#' + psIdElement + ' tbody').on('click', 'tr', function () {
            // Sélectionner la ligne deméndée, si c'est possible
            loDataTable.DoRowSelect(thisObject.row(this));
        });

        if (pfOnInitComplete) {
            // Appeller la fonction callback de "InitComplete"
            pfOnInitComplete(loDataTable);
        }
    }

    // Créer l'objet DataGrid
    var laOptions = {"initComplete": onDataTableInitComplete};
    $('#' + psIdElement).LGDataTable(psLibelléEntité, laOptions);
}

// Configurer la poignée de redimensionnement entre la DataTable et le form
// psIdElement : l'id de l'élément HTML qui contient le tableau HTML qui sert de base de la grille
function LGDataTable_initGrilleResize(psIdElement) {
    // Gestion du redimensionnement de la dataTable
    // selon https://coderexample.com/datatable-page-resize/ ????
    $('#resize_handle').on('mousedown', function (e) {
        var mouseStartY = e.pageY;
        var loScrollBody = $('#' + psIdElement).closest('.dataTables_scrollBody');
        var resizeStartHeight = loScrollBody.height();
        $(document)
                // Activation de l'écoute des mouvements de souris
                .on('mousemove.dateTableResize', function (e) {
                    var height = resizeStartHeight
                            + (e.pageY - mouseStartY)
                            ;
                    if (height < 50) {
                        height = 50;
                    }

                    // Selon https://stackoverflow.com/questions/7634066/how-to-dynamically-change-jquery-datatables-height
                    loScrollBody.css('height', height + 'px');

                })
                // Désactivation de l'écoute des mouvements de souris
                .on('mouseup.dateTableResize', function (e) {
                    $(document).off('mousemove.dateTableResize mouseup.dateTableResize');
                });
        return false;
    });

}
