// cboDate.js
// Définition de la dropDown de saisie de UNE date
// Selon http://www.daterangepicker.com/
// LG 20170118

// CE FICHIER DOIT ETRE MODIFIE PAR Nicolas Duchassin

!function ($) {

    // -----------------------------------------------------------
    // Commandes de définition d'une zone de sélection de 2 dates
    // HTML : 
    /*
     <select data-role="cbo2Dates"
     style="height: 34px;		// Force la hauteur pour que la dimension de la cbc soit correcte avant chargement
     width: 250px;"		// Largeur forcée de la zone de saisie
     >
     Le contenu de cette combo est changé PAR AJAX, de part son data-role
     </select>
     */

    /*    var DateRangePicker.prototype.updateElement = function() {
     if (this.element.is('input') && !this.singleDatePicker && this.autoUpdateInput) {
     this.element.val(this.startDate.format(this.locale.format) + 'toto' + this.endDate.format(this.locale.format));
     this.element.trigger('change');
     } else if (this.element.is('input') && this.autoUpdateInput) {
     this.element.val(this.startDate.format(this.locale.format));
     this.element.trigger('change');
     }
     } ;
     */

    $.fn.cboDate = function () {
        return this.each(function () {
            var thisCbo = this;

            // Définir les options (cf. http://www.daterangepicker.com/)
            $(thisCbo).daterangepicker({
                showWeekNumbers: true	// Show localized week numbers at the start of each week on the calendars
                , opens: "left"
                , showDropdowns: true	// Show year and month select boxes above calendars to jump to a specific month and year
                , linkedCalendars: false
                , startDate: moment().isoWeekday(1).format("DD/MM/YYYY")
                , endDate: moment().isoWeekday(7).format("DD/MM/YYYY")
                , minDate: "01/01/1900"
                , minShownDate: "01/01/1950"
                , dateLimit: {
                    days: 365
                }

                , ranges: {
                    "Ce jour": [
                        moment().format("DD/MM/YYYY"),
                        moment().format("DD/MM/YYYY")
                    ],
                    "Hier": [
                        moment().subtract(1, 'days').format("DD/MM/YYYY"),
                        moment().subtract(1, 'days').format("DD/MM/YYYY")
                    ],
                    "Demain": [
                        moment().add(1, 'days').format("DD/MM/YYYY"),
                        moment().add(1, 'days').format("DD/MM/YYYY")
                    ],
                }
                , locale: {
                    applyLabel: 'Appliquer'
                    , cancelLabel: 'Annuler'
                    , format: "DD/MM/YYYY"
                    , separator: " - "
                    , fromLabel: "Du"
                    , toLabel: "Au"
                    , customRangeLabel: "Personnalisé"
                    , weekLabel: "S"
                    , daysOfWeek: [
                        "Di"
                                , "Lu"
                                , "Ma"
                                , "Me"
                                , "Je"
                                , "Ve"
                                , "Sa"
                    ]
                    , monthNames: [
                        "Janvier"
                                , "Février"
                                , "Mars"
                                , "Avril"
                                , "Mai"
                                , "Juin"
                                , "Juillet"
                                , "Août"
                                , "Septembre"
                                , "Octobre"
                                , "Novembre"
                                , "Decembre"
                    ]
                    , "firstDay": 1
                }
            });

            $(thisCbo).css("visibility", "visible");
        });

    }

    $(function () {
        $("[data-role=cboDate]").cboDate();
    });

}(window.jQuery);
