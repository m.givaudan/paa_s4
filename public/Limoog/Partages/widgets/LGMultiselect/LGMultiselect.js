// LG 20190417
//var lsBasePath = "../../../" ;
//// includeJS(lsBasePath + 'vendor/jQuery-3.3.1/jquery-3.3.1.min.js') ;
//includeJS(lsBasePath + 'vendor/bootstrap/3.3.7/js/bootstrap.min.js') ;
//includeCSS(lsBasePath + 'vendor/bootstrap/3.3.7/css/bootstrap.min.css') ;
//// Bootstrap multiselect, http://davidstutz.de/bootstrap-multiselect/index.html#getting-started
//includeJS(lsBasePath + 'vendor/bootstrap/multiselect/js/bootstrap-multiselect.js') ;
//includeCSS(lsBasePath + 'vendor/bootstrap/multiselect/css/bootstrap-multiselect.css') ;

!function ($) {
	
    $.fn.LGMutiselectCombo = function(paOptions) {
        return this.each(function(o) {
			var thisCbc = this ;
			
			// Faire appel à la méthode native (Bootstrap Multiselect (http://davidstutz.github.io/bootstrap-multiselect/))
			var loCbc = $(thisCbc).multiselect(paOptions) ;
			
			// Ajouter à la cbc une méthode sortByCheck
			$(this).data('multiselect').sortByCheck = sortByCheck ;
			function sortByCheck() {
// Trier les éléments (selon https://stackoverflow.com/questions/3050830/reorder-list-elements-jquery)
				var ul = this.$ul ;//$("ul");
				var li = ul.children("li");

				li.detach().sort(private_SortByCheck);
				ul.append(li);
				function private_SortByCheck(a, b) {
					return $(a).text().toUpperCase().localeCompare($(b).text().toUpperCase());
				}
			}
			
			// Tester la méthode sortByCheck
			// $(this).multiselect('sortByCheck') ;
		});
	};
	
    $.fn.LGMutiselectList = function(paOptions) {
        return this.each(function(o) {
			var thisCbc = this ;
			
			// Faire appel à la méthode standard de multiselect
			$(thisCbc).LGMutiselectCombo(paOptions) ;
			
			// La classe LGMultiselectList suffit à transformer une comboCheck en listCheck
			$(thisCbc.parentNode).addClass("LGMultiselectList");
			
			// Mais il faut quand même gérer la hauteur
			thisCbc.parentNode.childNodes[1].style.height = thisCbc.style.height ;
		});
	};
	
}(window.jQuery);