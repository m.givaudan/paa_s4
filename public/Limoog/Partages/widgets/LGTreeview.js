// Surcharge de Treeview pour ajouter les spécificités Limoog
// Base : https://github.com/jonmiles/bootstrap-treeview
// LG 20190220

// Initialiser un Treeview à la sauce Limoog
// psIdElement		: l'id de l'élément HTML qui contient le tableau HTML qui sert de base de la grille
// psLibelléEntité	: nom de l'entité de BDD traitée dans la DataTable
// poDataTableOwner : l'objet dans la propriété oDataTable duquel on va stocker l'objet LGDataTable généré
//						et qui sera le parent du DataTable généré
// pfOnInitComplete	: fonction à exécuter quand l'initialisation de la LGDataTable sera complète

// LG 20200805 : déplacé depuis l'intérieur de LGTreeview_initTree début
// Définir le DataTable à la sauce Limoog
jQuery.fn.extend({
    LGTreeview: function (paOptions) {
        // Options par défaut pour LGDataTable
// LG 20200805 début : Bootstrap 4 ne contient plus GlyphIcon, il faut utiliser Font Awesome
//        var laOptionsDft = {
//            levels: 1
//        };
        var laOptionsDft = {
            levels: 1,
            expandIcon: 'fas fa-plus',
            collapseIcon: 'fas fa-minus',
            emptyIcon: 'fas',
        };
// LG 20200805 fin


        // Préparer les options définitives
        var laOptions;
        if (paOptions) {
            // On a fourni des options : les fusionner avec celles par défaut
            laOptions = $.extend(true, {}, laOptionsDft, paOptions);
        } else {
            laOptions = laOptionsDft;
        }

        // Créer le treeview
        return $(this).treeview(laOptions);
    }
});
// LG 20200805 : déplacé depuis l'intérieur de LGTreeview_initTree fin

function LGTreeview_initTree(psIdElement, psDataNodes, poDataTableOwner, pfOnInitComplete) {

    // Créer l'objet TreeView
// LG 20200805 début
//// LG 20200720 début : Bootstrap 4 ne contient plus GlyphIcon, il faut utiliser Font Awesome
////     var laOptions = {"data": psDataNodes};
//    var laOptions = {"data": psDataNodes,
//        expandIcon: 'fas fa-plus',
//        collapseIcon: 'fas fa-minus',
//        emptyIcon: 'fas',
//    };
//// LG 20200720 fin
    var laOptions = {"data": psDataNodes};
// LG 20200805 fin

    var loTreeview = $('#' + psIdElement).LGTreeview(laOptions);
    var thisObject = loTreeview;
    if (pfOnInitComplete)
// LG 20200812 old        pfOnInitComplete();
        pfOnInitComplete(thisObject);

    // Affecter l'objet global "dataTable_oDataTable"
    if (poDataTableOwner)
        poDataTableOwner.oTreeview = loTreeview;
    loTreeview.parent = poDataTableOwner;

    // Compléter l'objet TreeView pour lui ajouter les propriétés et méthodes nécessaires
    loTreeview.sIdTreeview = psIdElement;
    loTreeview.bIsSelectingNode = false;
    loTreeview.oSelectedNode = null;
    loTreeview.oSelectedNode_AvantNouveau = null;

    // Définir les méthodes
    loTreeview.on("nodeSelected", function (event, node) {
        loTreeview.nodeSelected(event, node);
    });
    loTreeview.nodeSelected = function (event, node) {
        if (thisObject.bIsSelectingNode) {
            // On est déja en train de sélectionner une ligne
            return true;
        }
        thisObject.bIsSelectingNode = true;

        // Essayer de déselectionner la ligne actuellement active, avec enregistrement de la ligne active si elle a été modifiée
        if (thisObject.parent) {
            thisObject.parent.oEntityForm.EnregistreSiNecessaire(
                    thisObject.parent.ChangeSelectedNode
                    , Array(node)
                    , thisObject.parent.CancelChangeSelectedNode);
        }
        return true;
    };
    loTreeview.OnFormSaved = function (piIdEntité) {
// LG 20200813 début
        var treeView = $("#" + thisObject.sIdTreeview).treeview(true) ;
        var node = treeView.getSelected()[0] ;
        if (node.cid === node.ctype) {
            // C'est un nouvel item : fournir l'Id au TreeView
            node.cid = node.ctype + piIdEntité ;
        }
// LG 20200813 fin
        
        // Mettre à jour le treeview (RestoreValeursTreeviewAvecCache)
        thisObject.RestoreValeursTreeviewAvecCache(piIdEntité);
    };
    // Mettre en cache le contenu du treeview pour l'élément courant
    loTreeview.MetEnCacheValeursColonnes = function (piId) {
        //tout stocker dans un tableau ou un objet
        var nom = thisObject.parent.oEntityForm.RtvValeurChamp('cnom');
        thisObject.aValeursChamps = nom;
    };

    // Restaurer les valeurs de l'élement sélectionnée dans le treeview avec les valeurs précédemment mises en cache
    loTreeview.RestoreValeursTreeviewAvecCache = function (piIdEntité) {
        if (!thisObject.aValeursChamps) {
            console.log("LGTreeview.aValeursChamps n'est pas renseigné");
            return false;
        } else {
            var laValeursChamps = thisObject.aValeursChamps;
        }
        //modification du texte directement dans le treeview
        var tree = $('#entityFormTreeview_Treeview').treeview('getSelected');
        tree[0].text = laValeursChamps;
    };

// LG 20200811
    // Récupérer l'objet JSON hierarchisé contenant tous les noeuds du TreeView
    loTreeview.getData = function(pvIdElementTreeView){
        var nodes, currentChild ;
        var data = [] ;
        // On a fourni l'id de l'élément TreeView
        var treeViewObject = $('#' + pvIdElementTreeView).treeview(true) ;
        var li = 0 ;
        var li2 = 0 ;
        var node ;
        while (true) {
            node = treeViewObject.getNode(li);
            if (!node) {
                break ;
            }
            if (!node.cid_parent) {
                data[li2] = node ;
                li2 ++ ;
            }
            li++ ;
        }
        return data ;
    };
    
    // Effacer le noeud actuellement sélectionné
    // Valeur de retour : Id de l'entité présene sur le noeud qui devient sélectionné
    loTreeview.DoDeleteNode = function () {

        // Récupérer les données actuelles
        var treeData = thisObject.getData(loTreeview.sIdTreeview) ;
        
        // Trouver le noeud et celui à sélectionner après suppression
        var selectedNode = $('#' + loTreeview.sIdTreeview).treeview('getSelected') ;
        selectedNode = selectedNode[0] ;
        var idASelectionner = selectedNode.nodeId - 1 ;
        if (idASelectionner < 0) {
            idASelectionner = 0 ;
        }
        
        // Effacer le noeud
        var parent = $('#' + loTreeview.sIdTreeview).treeview('getParent', selectedNode.nodeId) ;
        if (parent && !(parent[0] && parent[0].innerHTML)) {
            // On a trouvé un parent, et ce n'est pas l'élément TreeView
            // Il faut l'effacer de son parent
            var index = parent.nodes.map(function(e) { return e.nodeId; }).indexOf(selectedNode.nodeId);
            parent.nodes.splice(index) ;
        } else {
            // On n'a pas trouvé le noeud parent
            // Il faut l'effacer de la racine
            var index = treeData.map(function(e) { return e.nodeId; }).indexOf(selectedNode.nodeId);
            treeData.splice(index) ;
            
            // Rafraichir la TreeView
            $('#' + loTreeview.sIdTreeview).treeview('remove');
            LGTreeview_initTree(loTreeview.sIdTreeview, treeData, window.entityFormTreeview, window.entityFormTreeview.onInitComplete);
        }

        // reveal la nouvelle node
        var loNoeudASelectionner = $('#' + loTreeview.sIdTreeview).treeview('getNode', idASelectionner) ;
        $('#entityFormTreeview_Treeview').treeview('revealNode', [idASelectionner, {silent: true}]);
        $('#entityFormTreeview_Treeview').treeview('toggleNodeSelected', [idASelectionner, {silent: true}]);

        return loNoeudASelectionner ;
    };

    loTreeview.AddNode = function (treeview, selectedNode, TypeAjout, EntityFormTreeview) {

// LG 20200812 OBSOLETE
console.log("Obsolète") ;
debugger ;

//        $('body').append('<div id="isNewNode" ></div>');
//        $('body').append('<div id="idGroupe" ></div>');
//        var tree = JSON.parse(treeview);
//        var index = 0;
//        var newNode;
//        //gérer l'exception: si on sélectionne une AB
//
//        if (TypeAjout === 'GAB') {
//
//            newNode = {ctype: 'GAB', cid: 'GAB', text: 'Nouveau groupe d\'activités de base', icouleur: 255, iid: null, cid_parent: null, nodes: null};
//            tree[tree.length] = newNode;
//
//        } else if (TypeAjout === 'SGAB' && selectedNode[0].ctype === 'GAB') {
//
//            tree.forEach(function (element) {
//                if (element.cid === selectedNode[0].cid) {
//                    newNode = {ctype: 'SGAB', cid: 'SGAB', text: 'Nouveau sous-groupe d\'activités de base', icouleur: 255, iid: 21, cid_parent: selectedNode[0].cid, nodes: null};
//                    if (typeof element.nodes !== 'undefined') {
//                        tree[index].nodes[element.nodes.length] = newNode;
//                    } else {
//                        tree[index].nodes = [];
//                        tree[index].nodes[0] = newNode;
//                    }
//                } else
//                    index++;
//            });
//
//        } else if (TypeAjout === 'SGAB' && selectedNode[0].ctype === 'SGAB') {
//
//            tree.forEach(function (element) {
//                if (element.cid === selectedNode[0].cid_parent) {
//                    newNode = {ctype: 'SGAB', cid: 'SGAB', text: 'Nouveau sous-groupe d\'activités de base', icouleur: 255, iid: 21, cid_parent: selectedNode[0].cid_parent, nodes: null};
//                    if (element.nodes !== 'undefined') {
//                        tree[index].nodes[element.nodes.length] = newNode;
//                    } else {
//
//                        tree[index].nodes[0] = newNode;
//                    }
//                } else
//                    index++;
//            });
//
//        } else if (TypeAjout === 'AB' && selectedNode[0].ctype === 'SGAB') {
//
//            tree.forEach(function (element) {
//                if (element.cid === selectedNode[0].cid_parent) {
//                    var GABSelected = element.nodes;
//                    var indexSGAB = 0;
//                    GABSelected.forEach(function (cursor) {
//                        if (cursor.cid === selectedNode[0].cid) {
//                            newNode = {ctype: 'AB', cid: 'AB', text: 'Nouvelle activité de base', icouleur: 255, iid: 21, cid_parent: selectedNode[0].cid};
//                            tree[index].nodes[indexSGAB].nodes[cursor.nodes.length] = newNode;
//                        }
//                        indexSGAB++;
//                    });
//                }
//                index++;
//            });
//
//        } else if (TypeAjout === 'AB' && selectedNode[0].ctype === 'AB') {
//            var NodeParente;
//
//            tree.forEach(function (element) {
//                if (element.nodes !== undefined) {
//                    element.nodes.forEach(function (elements) {
//                        if (elements.cid === selectedNode[0].cid_parent) {
//                            //node parent
//                            NodeParente = elements;
//                        }
//                    });
//                }
//            });
//
//            tree.forEach(function (element) {
//                if (element.cid === NodeParente.cid_parent) {
//                    var GABSelected = element.nodes;
//                    var indexSGAB = 0;
//                    GABSelected.forEach(function (cursor) {
//                        if (cursor.cid === NodeParente.cid) {
//                            newNode = {ctype: 'AB', cid: 'AB', text: 'Nouvelle activité de base', icouleur: 255, iid: 21, cid_parent: NodeParente.cid};
//                            tree[index].nodes[indexSGAB].nodes[cursor.nodes.length] = newNode;
//                        }
//                        indexSGAB++;
//                    });
//                }
//                index++;
//            });
//
//        } else {
//            if (selectedNode === null) {
//                //exception 1: il n'y a pas de node sélectionnée alors que l'on veut ajouter un SGAB ou une AB
//                console.warn('Attention il faut sélectionner une node');
//            } else {
//                //exception : on a séléctionner une node GAB pour ajouter une AB
//                console.warn('Attention il faut sélectionner un SGAB ou une AB pour ajouter une AB');
//            }
//            return;
//        }
//
//        // Remove le tree
//        $('#entityFormTreeview_Treeview').treeview('remove');
//        // Création du tree
//        LGTreeview_initTree('entityFormTreeview_Treeview', tree, window.entityFormTreeview, window.entityFormTreeview.onInitComplete);
//
//        // reveal la nouvelle node
//        var pattern = newNode.text;
//        var results = $('#entityFormTreeview_Treeview').treeview('search', [pattern]);
//        $('#entityFormTreeview_Treeview').treeview('clearSearch');
//        $('#entityFormTreeview_Treeview').treeview('revealNode', [results[0], {silent: true}]);
//        $('#entityFormTreeview_Treeview').treeview('toggleNodeSelected', [results[0]]);
//
//        if (TypeAjout === 'SGAB') {
//            $('#idGroupe').text(newNode.cid_parent.substr(3));
//        } else if (TypeAjout === 'AB') {
//            $('#idGroupe').text(newNode.cid_parent.substr(4));
//        }
//        
//        EntityFormTreeview.oEntityForm.RemplitChampsPourNouveauOuDupicata(false);
    };

    return loTreeview;
}