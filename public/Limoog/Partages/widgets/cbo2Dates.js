// cbo2Dates.js
// Définition de la dropDown de saisie de 2 dates
// Selon http://www.daterangepicker.com/
// LG 20170118

!function ($) {

    // -----------------------------------------------------------
    // Commandes de définition d'une zone de sélection de 2 dates
    // HTML : 
    /*
     <select data-role="cbo2Dates"
     style="height: 34px;		// Force la hauteur pour que la dimension de la cbc soit correcte avant chargement
     width: 250px;"		// Largeur forcée de la zone de saisie
     >
     Le contenu de cette combo est changé PAR AJAX, de part son data-role
     </select>
     */

    /*    var DateRangePicker.prototype.updateElement = function() {
     if (this.element.is('input') && !this.singleDatePicker && this.autoUpdateInput) {
     this.element.val(this.startDate.format(this.locale.format) + 'toto' + this.endDate.format(this.locale.format));
     this.element.trigger('change');
     } else if (this.element.is('input') && this.autoUpdateInput) {
     this.element.val(this.startDate.format(this.locale.format));
     this.element.trigger('change');
     }
     } ;
     */
    $.fn.cbo2Dates = function () {
//MG Modification 20200217 Début
        return this.each(function () {
            cbo2DatesLoad(this);
            //Code déplacé dans la fonction cbo2DatesLoad
        });
//MG Modification 20200217 Fin

    };

    $(function () {
        $("[data-role=cbo2Dates]").cbo2Dates();
    });
}(window.jQuery);
//MG 20192310 début
//MG Modification Début 20200217
// Création de la fonction cbo2DatesLoad pour réutilisé le cbo2Dates après chargement d'une page (appel à la fonction)
function cbo2DatesLoad(thisCbo, ouverture = "left") {
//	var thisCbo = this ;
// MG Ajout Début 20200106
// MG Modif Début 20200113
//Utilisation de l'attribut data-dateDebut/Fin pour définir les valeurs des dates du cbo2Dates
//    console.log(thisCbo);
    var startDate;
    var endDate;
    var format = "DD/MM/YYYY";
    if (!$(thisCbo).attr('data-datedebut')) {
        startDate = moment().isoWeekday(1).format(format);
    } else {
        startDate = window.oPaa.getDate($(thisCbo).attr('data-datedebut'), format);
    }
    if (!$(thisCbo).attr('data-datefin')) {
        endDate = moment().isoWeekday(7).format(format);
    } else {
        endDate = window.oPaa.getDate($(thisCbo).attr('data-datefin'), format);
    }
//                        alert(startDate);
//                        alert(endDate);

//                        var urlParams = new URLSearchParams(window.location.search);
//			if (/[?&]dateDebut=/.test(location.search)){
//                            startDate = window.oPaa.getDate(urlParams.get('dateDebut'), "DD/MM/YYYY");  
//                        }
//                        if (/[?&]dateFin=/.test(location.search)){
//                            endDate = window.oPaa.getDate(urlParams.get('dateFin'), "DD/MM/YYYY");
//                        }
// MG Modif Fin 20200113                      
// MG Ajout Fin 20200106
    // Définir les options (cf. http://www.daterangepicker.com/ )
    $(thisCbo).daterangepicker({
        showWeekNumbers: true	// Show localized week numbers at the start of each week on the calendars
// MG Modification 20200217 Début
// Choisir le sens de l'ouverture, par défaut gauche
//				, opens: "left"
// MG Modification 20200217 Fin
        , opens: ouverture
        , showDropdowns: true	// Show year and month select boxes above calendars to jump to a specific month and year
        , linkedCalendars: false
// MG Modification Début 20200106
//				, startDate: moment().isoWeekday(1).format("DD/MM/YYYY")
//                                , endDate: moment().isoWeekday(7).format("DD/MM/YYYY")
        , startDate: startDate
        , endDate: endDate
// MG Modification Fin 20200106
        , minDate: "01/01/1900"
        , minShownDate: "01/01/1950"
// MG Modification Début 20200107
// Suppression de la date limite pour pouvoir utilisé la validité 'cette année' qui peut dépasser 365 jours.
//				, dateLimit: {
//					days: 365
//				}
//MG Modification Fin 20200107
        , ranges: {
            "Ce jour": [
                moment().format("DD/MM/YYYY"),
                moment().format("DD/MM/YYYY")
            ],
            "Hier": [
                moment().subtract(1, 'days').format("DD/MM/YYYY"),
                moment().subtract(1, 'days').format("DD/MM/YYYY")
            ],
            "Demain": [
                moment().add(1, 'days').format("DD/MM/YYYY"),
                moment().add(1, 'days').format("DD/MM/YYYY")
            ],
            "Cette semaine": [
                moment().isoWeekday(1).format("DD/MM/YYYY"),
                moment().isoWeekday(7).format("DD/MM/YYYY")
            ],
            "Ce mois": [
                moment().startOf('month').format("DD/MM/YYYY"),
                moment().endOf('month').format("DD/MM/YYYY")
            ],
            "Semaine type": [
                moment("1901/01/28", "YYYY/MM/DD").format("DD/MM/YYYY"),
                moment("1901/02/03", "YYYY/MM/DD").format("DD/MM/YYYY")
            ]
        }
        , locale: {
            applyLabel: 'Appliquer'
            , cancelLabel: 'Annuler'
            , format: "DD/MM/YYYY"
            , separator: " - "
            , fromLabel: "Du"
            , toLabel: "Au"
            , customRangeLabel: "Personnalisé"
            , weekLabel: "S"
            , daysOfWeek: [
                "Di"
                        , "Lu"
                        , "Ma"
                        , "Me"
                        , "Je"
                        , "Ve"
                        , "Sa"
            ]
            , monthNames: [
                "Janvier"
                        , "Février"
                        , "Mars"
                        , "Avril"
                        , "Mai"
                        , "Juin"
                        , "Juillet"
                        , "Août"
                        , "Septembre"
                        , "Octobre"
                        , "Novembre"
                        , "Decembre"
            ]
            , "firstDay": 1
        }
    },
// selon https://stackoverflow.com/questions/44553261/jquery-daterangepicker-onchange-event
// fonctionne, mais inutile : on utilise plutôt $('#cbo2Dates').on('apply.daterangepicker', ...
//			  function(){
//				  alert("Changé") ;
//				  if(onChangeCallback) {onChangeCallback();}
//			  }
            );

    $(thisCbo).css("visibility", "visible");
}
//MG Modification Fin 20200217

function dateSuivante(jsDate1, jsDate2, x, id, picker) {
    if (x === 30) {
        // Plus de 25 jours, on ajoute 1 mois aux 2 dates
        jsNouvelleDate1 = moment(jsDate1, 'DD/MM/YYYY').add('months', 1);
        jsNouvelleDate2 = moment(jsDate2, 'DD/MM/YYYY').add('months', 1).endOf('month'); //endOf() nous donne le dernier jour du mois (Sinon probeleme avec mois a 30j et février)
    } else {
        //Selon le nombre de jour de différence , on ajoute le nombre x de jours a nos 2 dates
        jsNouvelleDate1 = moment(jsDate1, 'DD/MM/YYYY').add('days', x);
        jsNouvelleDate2 = moment(jsDate2, 'DD/MM/YYYY').add('days', x);
    }
    //Affecte nos nouvelles dates comme dates de départ et fin 
    jsNewDate1 = picker.setStartDate(jsNouvelleDate1);
    jsNewDate2 = picker.setEndDate(jsNouvelleDate2);
    //affichage de nos dates dans le dateRangepicker
    id.val(jsNouvelleDate1.format('DD/MM/YYYY') + ' - ' + jsNouvelleDate2.format('DD/MM/YYYY'));
}
function datePrecedente(jsDate1, jsDate2, x, id, picker) {
    if (x === 30) {
        // Plus de 25 jours, on retire 1 mois aux 2 dates
        jsNouvelleDate1 = moment(jsDate1, 'DD/MM/YYYY').subtract('months', 1);
        jsNouvelleDate2 = moment(jsDate2, 'DD/MM/YYYY').subtract('months', 1).endOf('month');
    } else {
        //Selon le nombre de jour de différence , on retire le nombre x de jours a nos 2 dates
        jsNouvelleDate1 = moment(jsDate1, 'DD/MM/YYYY').subtract('days', x);
        jsNouvelleDate2 = moment(jsDate2, 'DD/MM/YYYY').subtract('days', x);
    }
    //Affecte nos nouvelles dates comme dates de départ et fin
    jsNewDate1 = picker.setStartDate(jsNouvelleDate1);
    jsNewDate2 = picker.setEndDate(jsNouvelleDate2);
    //affichage de nos dates dans le dateRangepicker
    id.val(jsNouvelleDate1.format('DD/MM/YYYY') + ' - ' + jsNouvelleDate2.format('DD/MM/YYYY'));
}
//MG 20192310 fin