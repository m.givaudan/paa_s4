//MG 20192110 début
!function ($) {
    $.fn.SpeedWidget = function () {
//MG Modification 20200217 Début
        return this.each(function () {
            speedWidgetLoad(this);
            //Code déplacé dans la fonction speedWidgetLoad
        });
//MG Modification 20200217 Fin
    };
    //MG20192210 fin
    $(function () {
        //On apllique a tous les objets de notre page possédant l'attribut data-speed=true la fonction SpeedWidget
        $("[data-speed=true]").SpeedWidget();
    });
}(window.jQuery);
//MG 20192110 fin

//MG Modification Début 20200217
// Création de la fonction speedWidgetLoad pour réutilisé le speedWidget après chargement d'une page (appel à la fonction)
function speedWidgetLoad(thisObj) {
    if ($(thisObj).attr('data-speed') === 'done') { //Si notre objet possède deja l'attribut data-speed = 'done' alors on agit pas dessus
        return;
    }
    if (isMultiSelect(thisObj.id)) {
        var thisCbc = thisObj;
        //Vérifier que notre objet soit un multiselect
        /*Création des boutons autour du multiselect*/
        $('ul.navbar-form span.multiselect-native-select')
                .after('<button id="' + thisObj.id + 'suivante" class="btn mr-2 btn-default btn-xs btn-speedWidget d-flex align-items-center justify-content-center"><img class="next" src="../../Images/next-r.png"></img></button>');
        $('ul.navbar-form span.multiselect-native-select')
                .before('<button id="' + thisObj.id + 'precedente" class="btn w-0 btn-default btn-xs btn-speedWidget d-flex align-items-center justify-content-center"><img class="next" src="../../Images/next-l.png"></img></button>');
        $('#' + thisObj.id + 'suivante').on('click', ressourcesuivante_click);
        $('#' + thisObj.id + 'precedente').on('click', ressourceprecedente_click);
        function ressourcesuivante_click() {
            var SelectNouvellesValues = [];
            var size = 0;
            var jsRessourceSuivanteActive = [];
            var i = 0;
            var tabindex = [];
            var jsAnciennesValues = [];
            var jsNouvellesValues = [];
            var jsLastIndex;
            var o = $(".multiselect-container li.active").eq(0).index(); //Récupérer l'index du combocheck deja coché
            jsRessource = $("#cbcRessources");
            if (o === -1) {
                //Si aucune ressource est preselectionnée, on selectionne la première ressource
                jsRessource.multiselect('select',
                        $(".multiselect-container li").eq(2).find('input').val()); //Coché la première ressource
            } else if ($(".multiselect-container li").hasClass('active') === true) {
                if ($(".multiselect-container li").last().index() === o) {
                    //Si l'objet est le dernier, le suivant sera le premier
                    o = 1;
                }
                if ($(".multiselect-container li.active").eq(0).next().hasClass('multiselect-item multiselect-group') === true) {
                    //Si l'objet suivant est un titre, on passe a l'option d'après
                    o += 1;
                }
                //Au moins un combocheck est selectionné, on vérifie si le suivant est aussi coché , si oui , return true, sinon false
                $(".active input").each(function () {
                    jsRessourceSuivanteActive[size] = $(".multiselect-container li.active").eq(size).next().hasClass('active');
                    size += 1;
                });
                jsRessourceSuivanteActive.pop(); // Suppresion de la dernière valeur du tableau (non significative, toujours false)
                nbrCaseCoche = size;
                if (nbrCaseCoche === 1) {
                    //Pour une case cochée
                    $(".active input").each(function () {
                        //Pour chaque case cochée (même dans le cas de 1 case, obliger d'utiliser le each)
                        SelectNouvellesValues = [$(".multiselect-container li").eq(o + 1).find('input').val()]; //Enregistre la value l'option suivante pour charger le planning (tableau obligatoire)
                        jsRessource.multiselect('deselect', $(this).val());//Décoché la case actuellement cochée
                        jsRessource.multiselect('select',
                                $(".multiselect-container li").eq(o + 1).find('input').val()); //Coché la suivante                                
                    });
                } else if (nbrCaseCoche > 1 && jsRessourceSuivanteActive.includes(false) === true) {
                    //Pour plus d'une case cochée et si au moins une des cases suivant le bloc n'est pas coché (sauf la dernière)
                    $(".active input").each(function () {
                        //Pour chaque case cochée                                   
                        tabindex[i] = $(".multiselect-container li.active").eq(i).index(); //Sauvegarde dans un tableau la position de chaque case cochées
                        if ($(".multiselect-container li").last().index() === tabindex[i]) {
                            //Si l'objet est le dernier, le suivant sera le premier
                            tabindex[i] = 1;
                        }
                        if ($(".multiselect-container li.active").eq(i).next().hasClass('multiselect-item multiselect-group') === true) {
                            //Si l'objet suivant est un titre, on passe a l'option d'après
                            tabindex[i] += 1;
                        }
                        jsAnciennesValues[i] = $(this).val(); //Valeurs des cases cochées actuelles
                        jsNouvellesValues[i] = $(".multiselect-container li").eq(tabindex[i] + 1).find('input').val(); //Valeurs des cases qui vont être cochées
                        i += 1;
                    });
                    for (x = 0; x < jsAnciennesValues.length; x++) {
                        //Deselectionner toutes les cases actuellement cochées
                        jsRessource.multiselect('deselect', jsAnciennesValues[x]);
                    }
                    for (x = 0; x < jsNouvellesValues.length; x++) {
                        //Selectionner toutes les cases suivantes
                        jsRessource.multiselect('select', jsNouvellesValues[x]);
                        SelectNouvellesValues [x] = jsNouvellesValues[x];//Enregistre les values des options suivantes pour charger le planning
                    }
                } else if (nbrCaseCoche > 1 && jsRessourceSuivanteActive.includes(false) === false) {
                    //Pour plus d'une case cochée et si elles forment un bloc compact
                    $(".active input").each(function () {
                        //Pour chaque case cochée
                        tabindex[i] = $(".multiselect-container li.active").eq(i).index(); //Sauvegarde dans un tableau la position de chaque case cochées
                        if ($(".multiselect-container li").last().index() === tabindex[i]) {
                            //Si l'objet est le dernier, le suivant sera le premier
                            tabindex[i] = 1;
                        }
                        if ($(".multiselect-container li.active").eq(i).next().hasClass('multiselect-item multiselect-group') === true) {
                            //Si l'objet suivant est un titre, on passe a l'option d'après
                            //Pour le bloc par bloc, on ne vérifie pas le suivant du suivant, (peut poser des problemes en fin de liste)
                            tabindex[i] += 1;
                        }
                        jsAnciennesValues[i] = $(this).val();//Valeurs des cases cochées actuelles
                        jsNouvellesValues[i] = $(".multiselect-container li").eq(tabindex[i] + 1).find('input').val();//Valeurs des cases qui vont être cochées                                     
                        i += 1;
                    });
                    jsLastIndex = tabindex.slice(-1); //On retire la dernière index
                    for (x = 0; x < jsAnciennesValues.length; x++) {
                        //Deselectionner toutes les cases actuellement cochées du bloc
                        jsRessource.multiselect('deselect', jsAnciennesValues[x]);
                    }
                    for (x = 0; x < jsNouvellesValues.length; x++) {
                        //Selectionner le même nombre de cases pour le bloc suivant
                        jsRessource.multiselect('select', $(".multiselect-container li").eq(jsLastIndex[0] + x + 1).find('input').val());
                        SelectNouvellesValues[x] = $(".multiselect-container li").eq(jsLastIndex[0] + x + 1).find('input').val();//Enregistre les values des options suivantes pour charger le planning
                    }
                }
            }
            var event = jQuery.Event("onValideChangecbcRessource"); //Création de l'événement
            event.source = thisCbc.id; // = cbcRessources
            event.aLstRes = SelectNouvellesValues; //Tableau des values des options suivantes
            $("body").trigger(event); //Déclenchement de l'event pour recharger le tableau
        }
        ;
        function ressourceprecedente_click() {
            var SelectNouvellesValues = [];
            var size = 0;
            var jsRessourcePrecedenteActive = [];
            var i = 0;
            var tabindex = [];
            var jsAnciennesValues = [];
            var jsNouvellesValues = [];
            var jsFirstIndex;
            var o = $(".multiselect-container li.active").eq(0).index();//Récupérer l'index du combocheck deja coché
            jsRessource = $("#cbcRessources");
            if (o === -1) {
                //Si aucune ressource est preselectionnée, on selectionne la derniere ressource
                jsRessource.multiselect('select',
                        $(".multiselect-container li").last().find('input').val()); //Coché la dernière ressource
            } else if ($(".multiselect-container li").hasClass('active') === true) {
                if ($(".multiselect-container li.active").eq(0).prev().hasClass('multiselect-item multiselect-group') === true) {
                    //Si l'objet prédédent est un titre, on passe a l'option d'avant
                    o -= 1;
                }
                if (o === 1) {
                    //Si l'objet est le premier, le suivant sera le dernier
                    o = $(".multiselect-container li").last().index() + 1;
                }
                //Au moins un combocheck est selectionné, on vérifie si le précédent est aussi coché , si oui , return true, sinon false
                $(".active input").each(function () {
                    jsRessourcePrecedenteActive[size] = $(".multiselect-container li.active").eq(size).prev().hasClass('active');
                    size += 1;
                });
                jsRessourcePrecedenteActive.shift();// Suppresion de la première valeur du tableau (non significative, toujours false)
                nbrCaseCoche = size;
                if (nbrCaseCoche === 1) {
                    //Pour une case cochée
                    $(".active input").each(function () {
                        //Pour chaque case cochée (même dans le cas de 1 case, obliger d'utiliser le each)
                        SelectNouvellesValues = [$(".multiselect-container li").eq(o - 1).find('input').val()]; //Enregistre la value l'option précédente pour charger le planning (tableau obligatoire)
                        jsRessource.multiselect('deselect', $(this).val());//Décoché la case actuellement cochée        
                        jsRessource.multiselect('select',
                                $(".multiselect-container li").eq(o - 1).find('input').val());//Coché la précédente
                    });
                } else if (nbrCaseCoche > 1 && jsRessourcePrecedenteActive.includes(false) === true) {
                    //Pour plus d'une case cochée et si au moins une des cases précédent le bloc n'est pas coché (sauf la première)
                    $(".active input").each(function () {
                        //Pour chaque case cochée
                        tabindex[i] = $(".multiselect-container li.active").eq(i).index();//Sauvegarde dans un tableau la position de chaque case cochées
                        if ($(".multiselect-container li.active").eq(i).prev().hasClass('multiselect-item multiselect-group') === true) {
                            //Si l'objet précédent est un titre, on passe a l'option d'avant
                            tabindex[i] -= 1;
                        }
                        if (tabindex[i] === 1) {
                            //Si l'objet est le premier, le suivant sera le dernier
                            tabindex[i] = $(".multiselect-container li").last().index() + 1;
                        }
                        jsAnciennesValues[i] = $(this).val();//Valeurs des cases cochées actuelles
                        jsNouvellesValues[i] = $(".multiselect-container li").eq(tabindex[i] - 1).find('input').val();//Valeurs des cases qui vont être cochées
                        i += 1;
                    });
                    for (x = 0; x < jsAnciennesValues.length; x++) {
                        //Deselectionner toutes les cases actuellement cochées
                        jsRessource.multiselect('deselect', jsAnciennesValues[x]);
                    }
                    for (x = 0; x < jsNouvellesValues.length; x++) {
                        //Selectionner toutes les cases précédentes
                        jsRessource.multiselect('select', jsNouvellesValues[x]);
                        SelectNouvellesValues [x] = jsNouvellesValues[x];//Enregistre les values des options précédentes pour charger le planning
                    }
                } else if (nbrCaseCoche > 1 && jsRessourcePrecedenteActive.includes(false) === false) {
                    //Pour plus d'une case cochée et si elles forment un bloc compact
                    $(".active input").each(function () {
                        //Pour chaque case cochée
                        tabindex[i] = $(".multiselect-container li.active").eq(i).index();//Sauvegarde dans un tableau la position de chaque case cochées
                        if ($(".multiselect-container li.active").eq(i).prev().hasClass('multiselect-item multiselect-group') === true) {
                            //Si l'objet précédent est un titre, on passe a l'option d'avant
                            //Pour le bloc par bloc, on ne vérifie pas le précédent du précédent, (peut poser des problemes en fin de liste)
                            tabindex[i] -= 1;
                        }
                        if (tabindex[i] === 1) {
                            //Si l'objet est le premier, le suivant sera le dernier
                            tabindex[i] = $(".multiselect-container li").last().index() + 1;
                        }
                        jsAnciennesValues[i] = $(this).val();//Valeurs des cases cochées actuelles
                        jsNouvellesValues[i] = $(".multiselect-container li").eq(tabindex[i] - 1).find('input').val();//Valeurs des cases qui vont être cochées
                        i += 1;
                    });
                    jsFirstIndex = tabindex.slice(0); //On retire la première index
                    for (x = 0; x < jsAnciennesValues.length; x++) {
                        //Deselectionner toutes les cases actuellement cochées du bloc
                        jsRessource.multiselect('deselect', jsAnciennesValues[x]);
                    }
                    for (x = 0; x < jsNouvellesValues.length; x++) {
                        //Selectionner le même nombre de case pour le bloc précédent
                        jsRessource.multiselect('select', $(".multiselect-container li").eq(jsFirstIndex[0] - x - 1).find('input').val());
                        SelectNouvellesValues[x] = $(".multiselect-container li").eq(jsFirstIndex[0] - x - 1).find('input').val();//Enregistre les values des options précédentes pour charger le planning
                    }
                }
            }
            var event = jQuery.Event("onValideChangecbcRessource"); //Création de l'événement
            event.source = thisCbc.id; // = cbcRessources
            event.aLstRes = SelectNouvellesValues; //Tableau des values des options suivantes
            $("body").trigger(event); //Déclenchement de l'event pour recharger le tableau
        }
        ;
        $(thisObj).attr("data-speed", 'done');
    }
    //MG20192210 début
    if (isSelect(thisObj.id)) {
        //Vérifier que notre objet soit un select
        /*Création des boutons autour du select*/
        var thisSelect = thisObj;
//MG 20200224 Modification Début
//                $(thisObj).after('<button id="'+thisObj.id+'droite'+'" class="btn btn-default btn-xs btn-speedWidget col"><span class="glyphicon glyphicon-triangle-right"></span></button>');
//                $(thisObj).before('<button id="'+thisObj.id+'gauche'+'" class="btn btn-default btn-xs btn-speedWidget col"><span class="glyphicon glyphicon-triangle-left"></span></button>');
        $(thisObj).after('<button id="' + thisObj.id + 'droite' + '" class="btn mr-2 btn-default btn-xs btn-speedWidget d-flex align-items-center justify-content-center"><img class="next" src="../../Images/next-r.png"></img></button>');
        $(thisObj).before('<button id="' + thisObj.id + 'gauche' + '" class="btn btn-default btn-xs btn-speedWidget d-flex align-items-center justify-content-center"><img class="next" src="../../Images/next-l.png"></img></button>');
//MG 20200224 Modification Fin
        $('#' + thisObj.id + 'droite').on('click', prochaineoption_click);
        $('#' + thisObj.id + 'gauche').on('click', precedenteoption_click);
        function prochaineoption_click() {
            var jsOptionSelected;
            $('#' + thisSelect.id + ' option').each(function () {
                //Pour chaque option de mon select
                if (this.selected || $(this).attr('selected')) {
                    //Si l'option est selected ou possède l'attribut, on la mémorise pour slectionner la suivante
                    jsOptionSelected = $(this);
                }
                //On retire tous les attributs selected a notre option
                this.selected = false;
                $(this).attr('selected', false);
                $(this).removeAttr('selected');
            });
            if (jsOptionSelected.next().length > 0) {
                //Il existe une option apres, on la selectionne
                $('#' + thisSelect.id).val(jsOptionSelected.next().val());
            } else {
                //Il n'existe pas d'option apres, on selectionne la première option
                $('#' + thisSelect.id + ' option:first').attr('selected', 'selected');
                $('#' + thisSelect.id + ' option:first').selected = true;
            }
        }
        ;
        function precedenteoption_click() {
            var jsOptionSelected;
            $('#' + thisSelect.id + ' option').each(function () {
                //Pour chaque option de mon select
                if (this.selected || $(this).attr('selected')) {
                    //Si l'option est selected ou possède l'attribut
                    jsOptionSelected = $(this);
                }
                //On retire tous les attributs selected a notre option
                this.selected = false;
                $(this).attr('selected', false);
                $(this).removeAttr('selected');
            });
            if (jsOptionSelected.prev().length > 0) {
                //Il existe une option avant, on la selectionne
                $('#' + thisSelect.id).val(jsOptionSelected.prev().val());
            } else {
                //Il n'existe pas d'option avant, on selectionne la dernière option
                $('#' + thisSelect.id + ' option:last').attr('selected', 'selected');
                $('#' + thisSelect.id + ' option:last').selected = true;
            }
        }
        ;
        $(thisObj).attr("data-speed", 'done');
    }
    //MG 20192310 début
    if (isCbo2Dates(thisObj.id)) {
        //Vérifier que notre objet soit un cbo2Dates
        /*Création des boutons autour du cbo2Dates*/
        $(thisObj).after('<button id="' + thisObj.id + 'prochaine" class="btn btn-default btn-xs btn-speedWidget d-flex align-items-center justify-content-center"><img class="next" src="../../Images/next-r.png"></img></button>');
        $(thisObj).before('<button id="' + thisObj.id + 'precedente" class="btn btn-default btn-xs btn-speedWidget d-flex align-items-center justify-content-center"><img class="next" src="../../Images/next-l.png"></img></button>');
        $('#' + thisObj.id + 'prochaine').on('click', semaineprochaine_click);
        $('#' + thisObj.id + 'precedente').on('click', semaineprecedente_click);
        var picker = $('#' + thisObj.id).data('daterangepicker'); //picker, mon objet pour controler le dateRangePicker
        var id = $('#' + thisObj.id);//id de mon objet cbo2Dates
//MG Modification 20200218 Début
//Ajout de l'event pour eviter l'envoi lorsqu'on est dans un formulaire
//                function semaineprochaine_click(){
        function semaineprochaine_click(e) {
            e.preventDefault();
//MG Modification 20200218 Fin
            var jsDate1 = picker.startDate; //Premiere date (de départ)
            var jsDate2 = picker.endDate;   //Deuxième date (de fin)
            var diff = jsDate2.diff(jsDate1, 'days');//Nombre de jours de différence entre les 2 dates
            /*Selon le nombre de jours de différences, nous allons agir différemment*/
            if (diff <= 2) {
                //De 1 à 2 jours de différences, +1 jour
                x = 1;
            } else if (diff > 2 && diff < 13) {
                //De 3 à 12 jours de différences, +7 jours (1 semaine)
                x = 7;
            } else if (diff >= 13 && diff < 20) {
                //De 13 à 19 jours de différences, +14 jours (2 semaines)
                x = 14;
            } else if (diff >= 20 && diff < 26) {
                //De 20 à 25 jours de différences, +21 jours (3 semaines)
                x = 21;
            } else {
                // Plus de 25 jours, + 1 mois 
                x = 30;
            }
            dateSuivante(jsDate1, jsDate2, x, id, picker);
            $('#cbo2Dates').trigger('apply.daterangepicker', picker); //Déclenchement de l'event pour mettre a jour les dates du planning (objet picker requis)
        }
        ;
//MG Modification 20200218 Début
//Ajout de l'event pour eviter l'envoi lorsqu'on est dans un formulaire
//                function semaineprecedente_click(){
        function semaineprecedente_click(e) {
            e.preventDefault();
//MG Modification 20200218 Fin
            var jsDate1 = picker.startDate; //Premiere date (de départ)
            var jsDate2 = picker.endDate;   //Deuxième date (de fin)
            var diff = jsDate2.diff(jsDate1, 'days');//Nombre de jours de différence entre les 2 dates
            /*Selon le nombre de jours de différences, nous allons agir différemment*/
            if (diff <= 2) {
                //De 1 à 2 jours de différences, -1 jour
                x = 1;
            } else if (diff > 2 && diff < 13) {
                //De 3 à 12 jours de différences, -7 jours (1 semaine)
                x = 7;
            } else if (diff >= 13 && diff < 20) {
                //De 13 à 19 jours de différences, -14 jours (2 semaines)
                x = 14;
            } else if (diff >= 20 && diff < 26) {
                //De 20 à 25 jours de différences, -21 jours (3 semaines)
                x = 21;
            } else {
                // Plus de 25 jours, - 1 mois 
                x = 30;
            }
            datePrecedente(jsDate1, jsDate2, x, id, picker);
            $('#cbo2Dates').trigger('apply.daterangepicker', picker); //Déclenchement de l'event pour mettre a jour les dates du planning (objet picker requis)
        }
        ;
        $(thisObj).attr("data-speed", 'done');
    }
    //MG 20192310 fin
    //l'atribut data-speed de notre objet prend la valeur done une fois traiter, pour ne pas le traiter plusieurs fois  
}
;
//MG Modification Fin 20200217

function isMultiSelect(id) {
    //Permet de verifier si l'objet est un multiselect, par son id
    if ($('span.multiselect-native-select').children().filter("#" + id).length >= 1) {
        return true;
    }
}
;
function isSelect(id) {
    //Permet de verifier si l'objet est un select, par son id
    if ($('select.selectpicker').filter("#" + id).length >= 1) {
        return true;
    }
}
;
function isCbo2Dates(id) {
    //Permet de verifier si l'objet est un cbo2Dates, par son id
    if ($('input.cbo2Dates').filter("#" + id).length >= 1) {
        return true;
    }
}
;
//MG20192210 fin
