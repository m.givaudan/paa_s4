/**###############################################
 *      CLASSE cLegende
 * 		Basée sur cJour
  ###############################################*/

var cLegende = cJour.extend({

	lEstLegende: true,
	init: function (aParajournee){
		var lvReturn = this._super(aParajournee);
		this.aPara = aParajournee ;
		return lvReturn ;
	},
	
	rtvHTML: function () {
		
		var lsBorder ;
		if (this.sPosition == "droite" && this.vertical) lsBorder = 'border-left-width: ' ;
		if (this.sPosition == "gauche" && this.vertical) lsBorder = 'border-right-width: ' ;
		lsBorder += window.oPlanning.oCst.DimBordureEchelles + 'px; ' ;
		var lsClass = this.aPara.sClass + ' colléEnHaut' ;
		if (this.sPosition == "gauche") lsClass += '  colléAGaucheDuCalendrier' ;
		
		var lsHTML = ' <div '
				+ 'id= "legende' + this.sIdJour.toProperCase() + '" '
				+ 'class= "' + lsClass + '" '
				+ 'style= "'
					+ 'text-align: center; '
					+ lsBorder/*'border-left-width: ' + window.oPlanning.oCst.DimBordureEchelles + 'px; '*/
					+ (this.aPara.backgroundColor?"background-color: " + this.aPara.backgroundColor + "; " : "")
					+ (this.aPara.bottom ? 'bottom: ' + this.aPara.bottom + '; ' : '')
					+ (this.aPara.top ? 'top: ' + this.aPara.top + '; ' : '')
					+ (this.aPara.height ? 'height: ' + this.aPara.height + '; ' : '')
					+ (this.aPara.width ? 'width: ' + this.aPara.width  + '; ' : '')
					+ (this.aPara.left ? 'left: ' + this.aPara.left + '; ' : '')
					+ (this.aPara.right ? 'right: ' + this.aPara.right + '; ' : '')
					+ (this.aPara.zIndex ? 'z-index: ' + this.aPara.zIndex + '; ' : '')
					+ '" '
				+ '>'

				lsHTML = lsHTML + this.rtvHTMLEnTêteRessources() ;
				lsHTML = lsHTML + this.rtvHTMLTranches(/*piDimension*/) ;
				lsHTML = lsHTML + '</div>' ;
		
		return lsHTML ;
		
	},

	// rtvHTMLTranches
	rtvHTMLTranches: function () {
	
//		var border = window.oPlanning.oCst.DimBordureTranches;
		var loTrancheJour, liTaille ;

		// Passer toutes les tranches pour les ajouter à la journée
		var loObjTranchePrec = false;
/*		var liIncrementPos = window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource ;
		if (this.lVertical) 
			var currentPos = window.oPlanning.oCst.HauteurBouton + liIncrementPos * 2 + window.oPlanning.oCst.HauteurBouton + 3 ;
		else
			var currentPos = window.oPlanning.oCst.LargeurBouton + liIncrementPos * 2 + window.oPlanning.oStructure.LargeurBoutonRessource + 3 ;
*/		var currentPos = window.oPlanning.oStructure.getDimEnTeteJour() ;
			
		var lsHTML = "";
		for (var j = 0; j < window.oPlanning.oStructure.aTranchesJours.length; j++) {
			loTrancheJour = window.oPlanning.oStructure.aTranchesJours[j] ;
			
			// Ne traiter que les tranches du 1er jour
			if (j >= window.oPlanning.oStructure.getNbTranches()) break;

			if (loObjTranchePrec) {
				// Ce n'est pas la première tranche : ajouter l'interTranche
				if (loTrancheJour.iDebut > loObjTranchePrec.iFin) {
					// Cette tranche a une dimension non nulle
					lsHTML += loTrancheJour.rtvHTMLInterTranche(currentPos);
					currentPos = currentPos + window.oPlanning.oStructure.tailleHorsLimite + 2 ;
				}
			}
			loObjTranchePrec = loTrancheJour ;

			liTaille = loTrancheJour.iTaille ;
			lsHTML += loTrancheJour.rtvHTML(currentPos										// piPosition
											, liTaille										// piDimensionTranche
											, false											// piDimensionJour
											, this.getIdDiv() + loTrancheJour.getIdDiv()	// psIdDiv
											, true											// pbSansRessources
											, this.aPara.bAfficheTraitsLégende				// pbAvecTraitsLégende
											, this.aPara.bAfficheHeureLégende				// pbAvecHeureLégende
											, this.sIdJour									// psIdJour
											) ;
			
			currentPos = currentPos + liTaille ;
		}

		return lsHTML ;
	}
	
});
