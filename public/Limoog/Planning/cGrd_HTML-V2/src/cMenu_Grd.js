﻿// http://swisnl.github.io/jQuery-contextMenu/demo.html
// http://swisnl.github.io/jQuery-contextMenu/docs.html

function resetContextMenu() {
	$.contextMenu( 'destroy' ) ;
	initContextMenu() ;
}

function initContextMenu() {
	// Objet global de définition du menu clic droit sur séances et boutons
	window.oPlanning.oMenuSéance = new cMenuSéance();
	
	/**************************************************
	 * Context-Menu
	 **************************************************/
	$.contextMenu({
		selector: '.context-menu-seance', 
		
		// ---------------------------------------------------------------------------------------
		// Construire le menu par la méthode build permet de le créer dynamiquement à chaque clic
		build: function($trigger, e) {
			// this callback is executed every time the menu is to be shown
			// its results are destroyed every time the menu is hidden
			// e is the original contextmenu event, containing e.pageX && e.pageY (amongst other data)
			return {
				// callback général quand un item est cliqué
				callback: function(key, options) {return window.oPlanning.oMenuSéance.onItemClick(key, options)},
				// Liste des items et sous-items du menu : construite dynamiquement à chaque instanciation du menu
				items: window.oPlanning.oMenuSéance.BuildItemsList(e.currentTarget)
			};
		},

		// ---------------------------------------------------------------------------------------
		// Construire le menu par la méthode build permet de le créer dynamiquement à chaque clic
		// Avec callback, qui permet de demander au serveur les informations complémentaires sur le menu à générer
		// Clic droit -> buildAvecCallBack -> demande au serveur et appel de buildCallBack une fois que la réponse est là -> construction du menu à l'aide des infos renvoyées par le serveur
		buildAvecCallback: function($trigger, e, $this, callback) {

// LG 20180724 début
			if (!window.oPlanning.oStructure.getReadWrite()) {
				// Lecture seule
				return ;
			}
// LG 20180724 fin
			
			// Récupérer les infos de la séance/bouton cliqué
			var loData = window.oPlanning.oMenuSéance.getContextInfos($trigger, e) ;
			loData.$this = $this ;
			loData.callbackPoséParbuildAvecCallback = callback ;
			
			// Demander au serveur les autorisations de modifications
			var lsJSON = [{nom: "sIdSeance", "valeur": loData.sIdSeance}
							, {nom: "sIdRessource", "valeur": loData.sIdRessource}
							, {nom: "dDate", "valeur": loData.dDate}
							, {nom: "iDebut", "valeur": loData.iDebut}
							, {nom: "iFin", "valeur": loData.iFin}
							] ;
			LG_Ajax_GetText("onPopupMenu", lsJSON, "", "JSON", this.buildCallback);
		},
		
		// ---------------------------------------------------------------------------------------
		// Construction du menu en utilisant les données JSON renvoyées en callback par le serveur via AJAX
		// psJSON	: vide si échec de la fonction AJAX -> annulation de l'affichage du menu déroulant
		buildCallback: function(psJSON) {
			// buildAvecCallback a été appellée : ceci est le callback avec le JSON demandé au serveur pour les autorisations de modifications
			if (psJSON) {
				// Il y a réponse JSON
				var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
				loInfos.sJsonCalculé = psJSON ;
				
				// Appeller la fonction callback de construction du menu, fournie par l'appellant de buildAvecCallback
				loInfos.callbackPoséParbuildAvecCallback(loInfos.$trigger, loInfos.e, loInfos.$this) ;
			}
		},
		
		// ---------------------------------------------------------------------------------------
	   events: {
			show: function(opt) {
				// this is the trigger element
				var $this = this;
				window.oPlanning.oMenuSéance.prepareData(opt, $.contextMenu, $this) ;
			}, 
			hide: function(opt) {
				// this is the trigger element
				var $this = this;
				// export states to data store
				// : renseigne la structure $this.data avec les données saisies dans la fenêtre de menu
				var loThisData = $this.data() ;
				$.contextMenu.getInputValues(opt, loThisData);
				window.oPlanning.oMenuSéance.onFermeMenu(opt, $this, loThisData);
			}
		}
	});
}

// ---------------------------------------------------------------------------------
// Classe pour le menu clic droit
var cMenuSéance = Class.extend({
	cClassName: "cMenuSéance",
	init: function(){},
	
	// Juste avant l'affichage : préparer les données à inclure dans le popup
    prepareData: function(opt, poContextMenu, poThis) {
		// Lire la liste des données incluses dans le popup
		loThisData = poThis.data() ;
		poContextMenu.getInputValues(opt, loThisData);
        
		// Faire des modifs dans les propriétés de loThisData ici si nécessaire
		
		// Ecrire les données modifiées
        poContextMenu.setInputValues(opt, loThisData);
		
		// Conserver l'état initial pour savoir plus tard s'il y a eu des changements
        this.oOldData = JSON.parse(JSON.stringify(loThisData)) ;
    },
	
	// Récupérer les infos de la séance/bouton cliqué
	// $trigger, e : fournir non null pour reset
	getContextInfos: function ($trigger, e){
		if ($trigger) this.oContextInfos = null ;
		if (this.oContextInfos) {
			// Les infos ont déja été récupérées
			return this.oContextInfos ;
		}
		
		var loInfos = {} ;
		loInfos.e = e ;
		loInfos.trigger = $trigger ;
		loDiv = e.currentTarget ;
        var lsType ;
		if (loDiv.className.match("contenuSéance")) {
			// Objet séance : on ne peut cliquer que sur le libellé
			var loDiv = loDiv.parentNode.parentNode.parentNode.parentNode ;
			lsType = "séance" ;
		} else if (loDiv.className.match("boutonJour")) {
			// Bouton de jour
			lsType = "boutonJour" ;
		} else if (loDiv.className.match("boutonRessource")) {
			// Objet séance
			lsType = "boutonRessource" ;
		}
		if (!lsType) {
			console.log(this.cClassName + ".getTargetData : classe non reconnue : " + loDiv.className) ;
			return "" ;
		}
		loInfos.oDiv = loDiv ;
		loInfos.sType = lsType ;
		loInfos.oSeance = null ;
		loInfos.sIdSeance = "" ;
		loInfos.dDate = null ;
		loInfos.sIdDivRessource = "" ;
		loInfos.sType_Res = "" ;
		loInfos.iId_Res = 0 ;
		loInfos.iDebut = 0 ;
		loInfos.iFin = 0 ;
		if (lsType == "séance") {
			// Séance
			loInfos.oSeance = window.oPlanning.oStructure.getSeanceAvecDiv(loDiv) ;
			loInfos.sIdDivRessource = loInfos.oSeance.getIdDivRessource() ;
//			loInfos.sIdSeance = loInfos.oSeance.sIdSeance ;		// Attention : c'est l'Id dans cCurSéances
			loInfos.sIdSeance = window.oPlanning.oStructure.getLstSeancesSelectionnees() ;
			loInfos.dDate = loInfos.oSeance.dDate ;
			loInfos.iDebut = loInfos.oSeance.iDebut ;
			loInfos.iFin = loInfos.oSeance.iFin ;
		} else if (lsType == "boutonRessource") {
			loInfos.sIdDivRessource = loDiv.id.substr(3, loDiv.id.length - 3) ;
		} else {
			loInfos.sIdDivRessource = "" ;
		}
		if (loInfos.sIdDivRessource) {
			// On dispose d'une ressource
			loInfos.oRessource = window.oPlanning.oStructure.getRessourceAvecDiv(loInfos.sIdDivRessource) ;
			loInfos.sIdRessource = loInfos.oRessource.sIdRessource ;
			var loRes = decomposeRessource({sId_Res:loInfos.oRessource.sIdRessource}) ;
			loInfos.sType_Res = loRes.sType_Res ;
			loInfos.iId_Res = loRes.iId_Res ;
			loInfos.dDate = loInfos.oRessource.dDate ;
		}
		loInfos.bEstBoutonSemaine = (lsType == "boutonSemaine") ;
		loInfos.bEstBoutonJour = (lsType == "boutonJour") ;
		loInfos.bEstBoutonRessource = (lsType == "boutonRessource") ;
		loInfos.bEstBouton = (loInfos.bEstBoutonSemaine || loInfos.bEstBoutonJour || loInfos.bEstBoutonRessource) ;
		loInfos.bEstSéance = (lsType == "séance") ;
		loInfos.bEstSéanceVierge = (loInfos.bEstSéance && loInfos.oSeance.estSéanceVierge()) ;
		loInfos.bEstMultiSélection = (loInfos.bEstSéance && window.oPlanning.oStructure.aSeancesSelectionnee.length > 1) ;
		
		this.oContextInfos = loInfos ;
		
		return this.oContextInfos ;	
	},
    
    buildItemsList_Vide : function(poDiv){
		var loMenu = {} ;
//		loMenu.separateur = "---------" ;
		loMenu.RAS = {"name": "RAS"} ;

		return loMenu ;
     },
    
    BuildItemsList : function(poDiv){
        if (poDiv.className.match("contenuSéance") 
				|| poDiv.className.match("boutonJour")
				|| poDiv.className.match("boutonRessource")) {
			// Objet séance
			return this.buildItemsList_Seance() ;
		} else {
			console.log(this.cClassName + ".Menu_BuildItemsList : classe non reconnue : " + poDiv.className) ;
			return this.buildItemsList_Vide() ;
		}
     },
    
	// Construction du menu contextuel
	// poDiv	: élément DOM cliqué
	// psType	: type de cet élémént
	//			: "Séance" = séance
	// 			: "BoutonRessource" = bouton ressource
    buildItemsList_Seance: function(/*poDiv, psType*/){

		var loConfig = window.oPlanning.oStructure.oConfig ;
		var loInfos = this.getContextInfos() ;
		var lbReadOnly = !window.oPlanning.oStructure.getReadWrite(loInfos.dDate) ;
// LG 20180724 début
//		loInfos.oContexteCalculé = JSON.parse(loInfos.sJsonCalculé) ;
		var lsJSON = loInfos.sJsonCalculé ;
		var lsErreur = "" ;
		if (lsJSON.left(6) == "Erreur") {
			lsErreur = lsJSON ;
		} else {
			try {
				loInfos.oContexteCalculé = JSON.parse(lsJSON) ;
			} catch (e) {
				lsErreur = "Erreur : " + e.message ;
			}
		}
		if (lsErreur) {
			var loMenu = {} ;
			loMenu.itemErreur = {name: lsErreur
							//, icon: "copy"
							, disabled: false
							, lgCallback: null
							} ;
			LG_Ajax_Notifie(lsErreur, "Erreur", LG_Ajax_Notifie_callback) ;
			return loMenu ;
		}
// LG 20180724 fin
		var loAut = loInfos.oContexteCalculé.oAut ;
		var loInfosSupplSéance = loInfos.oContexteCalculé.oSéance ;
		var lbEstSemaineType = (loInfos.dDate && loInfos.dDate.getFullYear() < 1950) ;
		var lbSéanceVirtuelle = loInfos.oSeance ? loInfos.oSeance.lVirtuel : false ;
		var lbEstCaseDEchelle = false ;
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Définition du menu
		var loMenu = {} ;
		var liBar = 0 ;
		var liNbBarDepuisLastSépa = 0 ;

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Menu Edition : copier, coller, collage spécial
		if (loInfos.bEstSéance) {
			// Copier
			liBar += 1 ;  ;
			liNbBarDepuisLastSépa += 1 ;
			loMenu.copier = {name: "Copier la séance"
							, icon: "copy"
							, disabled: loInfos.bEstSéanceVierge || loInfos.bEstMultiSélection
							, lgCallback: window.oPlanning.oMenuSéance.onCopie
							} ;
//			key = "ctrl+C"

			// Coller
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ;
			var lbExisteSéanceCopiée = window.oPlanning.oControleur.clipboardContientSéance() /*goApp.oClipBoard.iType = diClipBoardType_Séance*/ ;
			if (lbExisteSéanceCopiée) var lcLblSeanceCopiee = window.oPlanning.oControleur.clipboard.cLbl ;
			else var lcLblSeanceCopiee = "" ;
			loMenu.coller = {name: 'Coller "' + lcLblSeanceCopiee + '"'
							, icon: "paste"
							, disabled: (!lbExisteSéanceCopiée || !loAut.bModifieSéance || loInfos.bEstMultiSélection)
							, lgCallback: window.oPlanning.oMenuSéance.onColle
							} ;
//			key = "ctrl+V"

			// Collage spécial
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ;
//			Key Ctrl+V, "Ctrl+Maj+V" ;
			loMenu.collageSpécial = {name: "Collage spécial ..."
									, icon: "pasteSpecial"
									, disabled: (!lbExisteSéanceCopiée || !loAut.bModifieSéance || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onCollageSpécial
									} ;

			// Séparateur
			liBar += 1 ;
			loMenu.sep1 = "---------" ;
			liNbBarDepuisLastSépa = 0 ;
		}

		
		if (loInfos.bEstBoutonRessource
				&& (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning
						|| loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Absences
						|| loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Présences)
				&& !loInfos.bEstSemaineType
				&& loInfos.sType_Res.dollar("IUGS")
				) {
			// Clic sur bouton de ressource d'une grille de saisie des absences
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ;
			var llExceptionnelle = loInfos.oContexteCalculé.oDivers.lEstHoraireSemaineExceptionnel ;
			loMenu.remetJournéeAuStandard = {name: "Remettre la journée au standard"
									, icon: ""
									, disabled: (lbReadOnly || !loAut.bPoseAbsence || !llExceptionnelle)
									, lgCallback: window.oPlanning.oMenuSéance.onRemetJournéeAuStandard
									} ;
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Enlever la séance
		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !loInfos.bEstCaseDEchelle
				) {
				
			// Enlever la séance
			liBar += 1 ; ;
			liNbBarDepuisLastSépa += 1 ;
			loMenu.supprSéance = {name: "Enlever la séance (Suppr.)"
									, icon: "delete"
									, disabled: (lbReadOnly || !loAut.bEnlèveSéance)
									, lgCallback: window.oPlanning.oMenuSéance.onSupprimeSéance
									} ;

			if (!loInfosSupplSéance.lEstPresence && !loInfosSupplSéance.lEstAbsence) {
				// Case de séance d'activité
				liBar += 1 ; ;
				liNbBarDepuisLastSépa += 1 ;
				loMenu.rendreSéanceExceptionnelle = {name: (lbSéanceVirtuelle?"Rendre la séance exceptionnelle":"Cette séance est exceptionnelle")
										, icon: ""
										, disabled: (lbReadOnly || !lbSéanceVirtuelle || !loAut.bModifieSéance || loInfos.bEstMultiSélection)
										, lgCallback: window.oPlanning.oMenuSéance.onRendSéanceExceptionnelle
										} ;
			}
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Enlever tout
		if (loInfos.bEstBoutonRessource) {
			// Cas du bouton de ressource
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.effacerTout = {name: "Enlever tout"
									, icon: ""
									, disabled: (lbReadOnly || !loAut.bEnlèveSéance)
									, lgCallback: window.oPlanning.oMenuSéance.onEffaceTout
									} ;
		}

		if (loInfos.bEstBoutonRessource
				&& !(loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Activité)
				&& !loInfos.bEstBoutonSemaine
				&& !(loInfos.sType_Res == "A")) {
			// Cas du bouton de ressource
			// Tous les modes sauf activité 

			// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 	
			// Gestion des modèles d'horaires
			loMenu.modèlesHoraires = {name: "Modèles horaires (temporairement inactif)"
									, icon: ""
									, disabled: (true || lbReadOnly || !loAut.bPoseAbsence)
									} ;
/*
			Local lcCurseur, liBarModèle, lcOrdre
			lcHoraireActuel = ""
			if !SéancesToHoraires(loData.cCurSéances ;			&& pcCurSéances
										, loInfos.sType_Res ;	&& psType_Res
										, liId_Res ;		&& piId_Res
										, ldDate ;				&& pdDate
										, @lcHoraireActuel ;		&& pcHoraire
										,  ;		&& piTypeSem
										,  ;		&& pbSemType
										,  ;		&& pbResAbs
										,  ;		&& pbAbsFixe
										,  ;		&& pbAbsJournée
										,  ;		&&  pbEtablissementFermé
										)
				&gsSetStepOn 
				lcHoraireActuel = ""
			Else
				lcHoraireActuel = Alltrim(lcHoraireActuel)
			}
			
			lbSkip = lbReadOnly ;
						|| !loAut.bPoseAbsence
			liBar += 1 ; 
			DEFINE BAR liBar OF popActivités PROMPT "Modèles horaires" SKIP FOR lbSkip
			DEFINE POPUP popActivitésModeles MARGIN Shortcut
			ON BAR liBar OF popActivités Activate Popup popActivitésModeles
			
			RtvSQLCurseur("Select * from Modeles_Horaires Order By cNom" ;	&& pcSQL
						, @lcCurseur ;				&& pcCurseur
						)
			Select (lcCurseur)
			liBarModèle = 0
			Scan 
				liBarModèle = liBarModèle + 1 
				DEFINE BAR liBarModèle OF popActivitésModeles ;
					PROMPT Alltrim(&lcCurseur..cNom) + " (" + uTronque(Alltrim(&lcCurseur..mHoraires), 50) + ")" ;
					SKIP FOR lbSkip
				lbMark = (AddSepa(Alltrim(&lcCurseur..mHoraires), ";") == AddSepa(lcHoraireActuel, ";"))
				Set Mark Of Bar liBarModèle Of popActivitésModeles To lbMark
				lcOrdre = "ON SELECTION BAR " + Transform(liBarModèle) + " OF popActivitésModeles loAppelant_PM.AppliqueModèleHoraire('" + Alltrim(&lcCurseur..mHoraires) + "')"
				ExecScript(lcOrdre)
			Endscan
			
			liBarModèle = liBarModèle + 1 
			DEFINE BAR liBarModèle OF popActivitésModeles PROMPT "Modèles ..."
			ON SELECTION BAR liBarModèle OF popActivitésModeles Ed_Modèles_Horaires()
			Use In (lcCurseur)

*/		
		}

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Choisir/Poser une séance		
		if ((loInfos.bEstSéanceVierge || loInfos.bEstBoutonRessource)
				&& !loInfos.bEstBoutonSemaine
				&& loConfig.lMontreActivités
				&& !(loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Activité)
				&& (loConfig.iTypeHoraires == window.oPlanning.oCst.eTypesHoraire.Chronologique)
				&& !(loConfig.iTypeAffichageRessources == window.oPlanning.oCst.eTypesAffichageRessources.RessourcesGroupées)
				) {

			// Choisir/Poser une séance
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.choisirActivité = {name: "Choisir/poser une activité ..."		// (Ins).
									, icon: ""
									, disabled: (lbReadOnly || !loAut.bPoseSéance || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onChoisitPoseActivité
									} ;
			
			// Séance d'une nouvelle activité
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.créerActivité = {name: "Créer/poser une activité ..." 		// (Ctrl+Maj+Ins)
									, icon: ""
									, disabled: (lbReadOnly || !loAut.bPoseSéance || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onCréePoseActivité
									} ;
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Poser une séance de l'activité éditée
		if ((loInfos.bEstSéanceVierge || loInfos.bEstBoutonRessource)
				&& !loInfos.bEstBoutonSemaine
				&& (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Activité || loConfig.lMontrePrésences)
				&& (loConfig.iTypeHoraires == window.oPlanning.oCst.eTypesHoraire.Chronologique)
				&& !(loConfig.iTypeAffichageRessources == window.oPlanning.oCst.eTypesAffichageRessources.RessourcesGroupées)
				) {

			// Poser une séance de l'activité éditée
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.poserSéance = {name: "Poser une séance (Ins.)"
									, icon: ""
									, disabled: (lbReadOnly || !loAut.bPoseSéance || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onPoseSeanceDeLActivité
									} ;
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Barre de séparation
		if (liNbBarDepuisLastSépa > 0) {
			liBar += 1 ;
			liNbBarDepuisLastSépa = 0 ;
			loMenu.sep2 = "---------" ;
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Barre "Ajuster à l'espace libre"
		if (!lbEstCaseDEchelle
				&& loInfos.bEstSéance
				&& (loConfig.iTypeHoraires == window.oPlanning.oCst.eTypesHoraire.Chronologique)
				&& !(loConfig.iTypeAffichageRessources == window.oPlanning.oCst.eTypesAffichageRessources.RessourcesGroupées)
				&& !loInfos.bEstBouton
				&& loConfig.bDevel) {
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.ajuster = {name: "Ajuster à l'espace libre (dével uniquement)"		// (Ctrl+Maj+A) ???
									, icon: ""
									, disabled: (lbReadOnly || !loAut.bModifieSéance || loInfos.bEstMultiSélection)
									, additionalClasses: "menuItemRed"
									} ;
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Poser une séance d'absence
		var lbAutoriseModifieAbsences = (!lbReadOnly
				&& loAut.bPoseAbsence
				&& (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning || loConfig.lMontreAbsences)	
				&& !(loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Activité)
				&& (loConfig.iTypeHoraires == window.oPlanning.oCst.eTypesHoraire.Chronologique)
				&& !(loConfig.iTypeAffichageRessources == window.oPlanning.oCst.eTypesAffichageRessources.RessourcesGroupées)
				&& !(loInfos.sType_Res == ""))

		if (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning)	{
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.poserAbsence = {name: "Absence"
									, icon: ""
									, disabled: !lbAutoriseModifieAbsences || loInfos.bEstMultiSélection
									} ;
		}

		if (lbAutoriseModifieAbsences) {
			// L'utilisateur en cours a le droit de modifier les absences
			// Passer toutes les raisons d'absence
			var loMenuAbsences ;
			if (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning) {
				// Dans le cas du planning, les absences sont dans un sous-menu
				loMenu.poserAbsence.items = {} ;
				loMenuAbsences = loMenu.poserAbsence.items ;
			} else {
				// Dans le cas de la grille d'absences, les absences sont dans le menu ppal
				// Barre de séparation
				if (liNbBarDepuisLastSépa > 0) {
					liBar += 1 ;
					liNbBarDepuisLastSépa = 0 ;
					loMenu.sep5 = "---------" ;
				}
				loMenuAbsences = loMenu ;
			}
			this.buildPopupTypeAbsences(loInfos.sType_Res 							// tcType_Res
										, loInfos.iId_Res 							// tiId_Res
										, loMenuAbsences 							// toNomMenuAbsences
										, loInfos.oContexteCalculé.aTypesAbsences	// paTypesAbsences
										, loInfosSupplSéance.iId_ActiBase 			// tiActiBaseProposeDatesNumérotation
										) ;
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Modifier la séance
		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !lbEstCaseDEchelle
				) {
			
			// Barre de séparation
			if (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Absences
					&& liNbBarDepuisLastSépa > 0) {
				liBar += 1 ;
				liNbBarDepuisLastSépa = 0 ;
				loMenu.sep3 = "---------" ;
			}

			// Modifier la séance
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.modifierSéance = {name: "Modifier la séance (M/P/S)"	// STYLE "B"	// Key Ctrl+M
									, icon: "edit"
//									, disabled: (lbReadOnly || lbSéanceVirtuelle || !loAut.bModifieSéance || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onModifieSéance
									, additionalClasses: "menuItemDefault"
									} ;
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Série de séances
		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !lbEstCaseDEchelle
				) {
			// Case de séance
			var llSérieSéancesDisabled = (lbReadOnly
											|| lbSéanceVirtuelle
											|| !loAut.bModifieSéance
											|| !(loInfosSupplSéance.lEstActivitéGénérique || loInfosSupplSéance.lEstActivitéDeSemaineSuivie)
											|| loInfosSupplSéance.lEstPresence || loInfosSupplSéance.lEstAbsence
											|| lbEstSemaineType) ;
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.série = {name: "Série"
							, icon: ""
							, disabled: (llSérieSéancesDisabled || loInfos.bEstMultiSélection)
							} ;

			if (!llSérieSéancesDisabled) {
				loMenu.série.items = {} ;
				loMenu.série.items.série_scinder = {name: "Scinder"
													, icon: ""
													, disabled: (!loInfosSupplSéance.lEstSérieSéance || loInfos.bEstMultiSélection)
													, iSérieSéance: loInfosSupplSéance.iSérieSéance
													, lgCallback: window.oPlanning.oMenuSéance.onScindeSérie
													} ;

				loMenu.série.items.sep1 = "---------" ;
				
				var loFréquences ;
				for (var i = 0; i < loInfos.oContexteCalculé.aFréquencesPrédéfiniesSéries.length; i++) {
					loFréquence = loInfos.oContexteCalculé.aFréquencesPrédéfiniesSéries[i] ;
					loMenu.série.items["série_fréquence" + i] = {name: loFréquence.cNom
																, iId_Fréquence: loFréquence.iId
																, lgCallback: window.oPlanning.oMenuSéance.onChangeFréquenceSérie
																} ;
				}
			}
		}
		
/*
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Accompagnement : DESACTIVE EN JAVASCRIPT
		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !lbEstCaseDEchelle
				) {
			Do case
			Case !lGèreAccompagnement()
				// RAS
			Case !lGèreAccompagnementParSéances()
				// RAS
			Otherwise 
				* Gestion des accompagnements sous forme de séances
				Local llAutoriseAccompagnement, liBarMenuAccompagnement
				llAutoriseAccompagnement =  !loInfos.bEstSéanceVierge ;
					&& !lbEstCaseDEchelle ;
					&& loInfos.bEstSéance ;
					&& loAut.bModifieSéance ;
					&& loInfos.sType_Res = "U" ;
					&& !(lbSéanceVirtuelle) ;
					&& !(loInfosSupplSéance.lEstPresence) ;
					&& !(loInfosSupplSéance.lEstAbsence)

				liBar += 1 ;
				DEFINE BAR liBar OF popActivités PROMPT "Accompagnement" ;
						SKIP FOR !llAutoriseAccompagnement
				DEFINE Popup popAccompagnement MARGIN SHORTCUT RELATIVE
				ON Bar liBar OF popActivités ACTIVATE POPUP popAccompagnement
				liBarMenuAccompagnement = 0

				Local lcLblSéance, liSéanceMaitre, llEstSéanceMaitre, loCurSéances
				loCurSéances = loAppelant_PM.ioCASESOURCE.ioRec.oCURSÉANCESGRILLE
				Do case
					Case !llAutoriseAccompagnement
						* RAS
						
					Case !EstAccompagnement( ;						&& pcNomCourtActiBase
												,  ;					&& piActiBase
												,  ;					&& piActi
												, liId_Séance ;	&& piSéance
												)
						* Cette séance n'est pas d'accompagnement
						Local liSéanceAccompagnement
						if EstSéanceDépendante(liId_Séance ;				&& piSéance
												,  ;							&& plEstSéanceMaitre
												, diRelationSéances_S2PrécèdeS1 ;&& piTypeDépendance
												,  ;							&& piSéanceMaitre
												, @liSéanceAccompagnement ;	&& piSéanceDépendante
												) 
							* Cette séance est dotée d'une séance d'accompagnement précédent
							liBarMenuAccompagnement = liBarMenuAccompagnement + 1 
							DEFINE BAR liBarMenuAccompagnement OF popAccompagnement Prompt [Enlever l'accompagnement avant]
							ExecScript("ON SELECTION BAR " + Transform(liBarMenuAccompagnement) + " OF popAccompagnement" ;
										+ " loRessource_PM.DétacheSéanceAccompagnement(" + Transform(liSéanceAccompagnement) ;
																		+ ", .T.)")
						Else
							* Cette séance n'est pas encore dotée d'une séance d'accompagnement précédent
							liBarMenuAccompagnement = liBarMenuAccompagnement + 1 
							DEFINE BAR liBarMenuAccompagnement OF popAccompagnement Prompt [Insérer un accompagnement avant]
							ExecScript("ON SELECTION BAR " + Transform(liBarMenuAccompagnement) + " OF popAccompagnement" ;
										+ " loRessource_PM.AttacheSéanceAccompagnement(" + Transform(liId_Séance) ;
																		+ ", " + Transform(diRelationSéances_S2PrécèdeS1) ;
																		+ ", " ;
																		+ ", " + ")")
						}
						if EstSéanceDépendante(liId_Séance ;				&& piSéance
												,  ;							&& plEstSéanceMaitre
												, diRelationSéances_S2SuitS1 ;	&& piTypeDépendance
												,  ;							&& piSéanceMaitre
												, @liSéanceAccompagnement ;	&& piSéanceDépendante
												) 
							* Cette séance est dotée d'une séance d'accompagnement précédent
							liBarMenuAccompagnement = liBarMenuAccompagnement + 1 
							DEFINE BAR liBarMenuAccompagnement OF popAccompagnement Prompt [Enlever l'accompagnement après]
							ExecScript("ON SELECTION BAR " + Transform(liBarMenuAccompagnement) + " OF popAccompagnement" ;
										+ " loRessource_PM.DétacheSéanceAccompagnement(" + Transform(liSéanceAccompagnement) ;
																		+ ", .T.)")
						Else
							* Cette séance n'est pas encore dotée d'une séance d'accompagnement suivant
							liBarMenuAccompagnement = liBarMenuAccompagnement + 1 
							DEFINE BAR liBarMenuAccompagnement OF popAccompagnement Prompt [Insérer un accompagnement après]
							ExecScript("ON SELECTION BAR " + Transform(liBarMenuAccompagnement) + " OF popAccompagnement" ;
										+ " loRessource_PM.AttacheSéanceAccompagnement(" + Transform(liId_Séance) ;
																		+ ", " + Transform(diRelationSéances_S2SuitS1) ;
																		+ ", " ;
																		+ ", " + ")")
						}
						
					Case EstSéanceDépendante(liId_Séance ;		&& piSéance
												, @llEstSéanceMaitre ;&& plEstSéanceMaitre
												,  ;					&& piTypeDépendance
												, @liSéanceMaitre ;	&& piSéanceMaitre
												)
						* Séance d'accompagnement déja dépendante
						* Proposer de briser le lien
						if llEstSéanceMaitre
							&gsSetStepOn, "_3MY0Z9ZOP"		&& LG 20121105 
						Else
							* Cette séance est déja rattachée à une autre
							* Proposer de détacher
							lcLblSéance = RtvLibelléActi_Réalisation( ;	&& piActi
																		, liSéanceMaitre ;		&& piRéal
																		, eiFormatLibelléInclActi_Base + eiFormatLibelléRéduit ;		&& piFormat
																		,  ;		&& piTaillePolice
																		)
							liBarMenuAccompagnement = liBarMenuAccompagnement + 1 
							DEFINE BAR liBarMenuAccompagnement OF popAccompagnement PROMPT [Détacher de "] + lcLblSéance + ["]
							ExecScript("ON SELECTION BAR " + Transform(liBarMenuAccompagnement) + " OF popAccompagnement" ;
									+ " loRessource_PM.DétacheSéanceAccompagnement(" + Transform(liId_Séance) + ")")
						}
							
							
					Otherwise 
						* Séance d'accompagnement indépendante
						* Proposer d'attacher
						Local liSéanceSuivante, liSéancePrécédente
						liSéanceSuivante = loCurSéances.RtvSéanceSuivante(loAppelant_PM.ioCASESOURCE.ioRec.iId, @lcLblSéance)
						if !Empty(liSéanceSuivante)
							liBarMenuAccompagnement = liBarMenuAccompagnement + 1 
							DEFINE BAR liBarMenuAccompagnement OF popAccompagnement Prompt [Attacher à "] + lcLblSéance + ["]
							ExecScript("ON SELECTION BAR " + Transform(liBarMenuAccompagnement) + " OF popAccompagnement" ;
										+ " loRessource_PM.AttacheSéanceAccompagnement(" + Transform(liSéanceSuivante) ;
																		+ ", " + Transform(diRelationSéances_S2PrécèdeS1) ;
																		+ ", " ;
																		+ ", " + Transform(liId_Séance) + ")")
						}
						
						liSéancePrécédente = loCurSéances.RtvSéancePrécédente(loAppelant_PM.ioCASESOURCE.ioRec.iId, @lcLblSéance)
						if !Empty(liSéancePrécédente)
							liBarMenuAccompagnement = liBarMenuAccompagnement + 1 
							DEFINE BAR liBarMenuAccompagnement OF popAccompagnement PROMPT [Attacher à "] + lcLblSéance + ["]
							ExecScript("ON SELECTION BAR " + Transform(liBarMenuAccompagnement) + " OF popAccompagnement" ;
										+ " loRessource_PM.AttacheSéanceAccompagnement(" + Transform(liSéancePrécédente) ;
																		+ ", " + Transform(diRelationSéances_S2SuitS1) ;
																		+ ", " ;
																		+ ", " + Transform(liId_Séance) + ")")
						}
				Endcase 
			Endcase 
		}
*/

		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !lbEstCaseDEchelle
				&& loInfosSupplSéance.cUserCréateur
				) {
			// Proposer de changer le créateur de cette séance
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			if (!loAut.bTout) {
				// Adiministrateur
				loMenu.propriétaire = {name: "Changer le propriétaire/créateur (" + loInfosSupplSéance.cUserCréateur + ")"
										, icon: ""
										, disabled: (lbReadOnly || lbSéanceVirtuelle || loInfos.bEstMultiSélection)
										, lgCallback: window.oPlanning.oMenuSéance.onChangePropriétaire
										} ;
			} else {
				// Sans droits d'admin
				loMenu.propriétaire = {name: "Propriétaire/créateur : " + loInfosSupplSéance.cUserCréateur
										, icon: ""
										, disabled: true
										} ;
			}
		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !lbEstCaseDEchelle
				&& !loInfos.bEstBouton
				&& (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning 
						|| loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Activité)
				&& !loInfosSupplSéance.lEstPresence
				&& !loConfig.bMontreConflitsDansPopUp
				) {
		
			// Voir les conflits
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ;
			loMenu.montreConflits = {name: "Voir les conflits"
									, icon: ""
									, disabled: (lbReadOnly || lbSéanceVirtuelle || !loAut.bTout || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onMontreConflits
									} ;
			
			// Assistant conflits
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.wzConflits = {name: "Assistant conflits"
									, icon: ""
									, disabled: (!loConfig.bDevel || lbReadOnly || lbSéanceVirtuelle || !loAut.bTout || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onWzConflits
									, additionalClasses: "menuItemRed"
									} ;

			// Assistant Réaffectation des usagers
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.wzRéaffectationUsagers = {name: "Assistant réaffectation des usagers (temporairement inactif)"
									, icon: ""
									, disabled: (!loConfig.bDevel || lbReadOnly || lbSéanceVirtuelle || !loAut.bTout || loInfos.bEstMultiSélection)
									, additionalClasses: "menuItemRed"
									, lgCallback: window.oPlanning.oMenuSéance.onWzRéaffectationUsagers
									} ;
		}	

		if (!lbEstCaseDEchelle
				&& (loConfig.iTypePrédéfini = window.oPlanning.oCst.eTypesGrille.Planning
						|| loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Activité)
				// && loInfos.sType_Res == "E"
				) {

			// Assistant absence établissement
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.wzAbsenceEtablissement = {name: "Assistant absence établissement"
									, icon: ""
									, disabled: (!loConfig.bDevel || lbReadOnly || lbEstSemaineType || !loAut.bTout || loInfos.bEstMultiSélection)
									, additionalClasses: "menuItemRed"
									, lgCallback: window.oPlanning.oMenuSéance.onWzAbsenceEtablissement
									} ;
		}

		llSécuAutoriseRemplacement = loAut.bTout/*bPoseAbsence*/ ;
		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !lbSéanceVirtuelle
				&& !lbEstCaseDEchelle
				&& (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning
						|| loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Activité)
				) {
				
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.remplacement = {name: "Gestion des remplacements (temporairement inactif)"
									, icon: ""
									, disabled: (true || lbReadOnly || !llSécuAutoriseRemplacement || loInfos.bEstMultiSélection)
									} ;

/*			
			if Empty(Nvl(poappelant.ioCASESOURCE.ioREC.iRemplace_Par, 0) + Nvl(poappelant.ioCASESOURCE.ioREC.iRemplace, 0))
				* Pas encore de remplacement sur cette séance
				* Remplacer par...
				liBar += 1 ;
				liNbBarDepuisLastSépa += 1 ; 
				DEFINE BAR liBar OF popActivités PROMPT "Remplacer par..." ;
								SKIP For lbReadOnly ;
									|| !llSécuAutoriseRemplacement ;
									|| lbEstSemaineType ;
									|| !" " + Alltrim(loInfos.sType_Res) + " " $ " I "
				ON SELECTION Bar liBar OF popActivités loRessource_PM.DoWz_Remplacement(liHeureDébut_PM ;
																					, liHeureFin_PM ;
																					, liActi_PM ;
																					, loAppelant_PM.ioCASESOURCE.iiId_Séance ;
																					, 3 ;
																					, loAppelant_PM.ioCASESOURCE ;
																					)


				* Assistant remplacements
				liBar += 1 ;
				liNbBarDepuisLastSépa += 1 ; 
				DEFINE BAR liBar OF popActivités PROMPT "Assistant remplacement rapide" ;
								SKIP For lbReadOnly ;
									|| !llSécuAutoriseRemplacement ;
									|| lbEstSemaineType ;
									|| !" " + Alltrim(loInfos.sType_Res) + " " $ " I "
				ON SELECTION Bar liBar OF popActivités loRessource_PM.DoWz_Remplacement(liHeureDébut_PM ;
																					, liHeureFin_PM ;
																					, liActi_PM ;
																					, loAppelant_PM.ioCASESOURCE.iiId_Séance ;
																					, 2 ;
																					, loAppelant_PM.ioCASESOURCE ;
																					)
			Else
				* Déja un remplacement sur cette séance
				* Annuler le remplacement
				liBar += 1 ;
				liNbBarDepuisLastSépa += 1 ; 
				DEFINE BAR liBar OF popActivités PROMPT "Annuler le remplacement" ;
								SKIP For lbReadOnly ;
									|| !llSécuAutoriseRemplacement ;
									|| lbEstSemaineType ;
									|| !" " + Alltrim(loInfos.sType_Res) + " " $ " I "
				ON SELECTION Bar liBar OF popActivités loRessource_PM.DoWz_Remplacement(liHeureDébut_PM ;
																					, liHeureFin_PM ;
																					, liActi_PM ;
																					, loAppelant_PM.ioCASESOURCE.iiId_Séance ;
																					, 4 ;
																					, loAppelant_PM.ioCASESOURCE ;
																					)
			} 
*/
		} 

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Barre de séparation
		if (liNbBarDepuisLastSépa > 0) {
			liBar += 1 ; 
			liNbBarDepuisLastSépa = 0 ;
			loMenu.sep4 = "---------" ;
		}
		
		// Modifier l'activité
		if (loInfos.bEstSéance
				&& !loInfos.bEstSéanceVierge
				&& !loInfos.bEstBouton
				&& !lbEstCaseDEchelle
				&& (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning)
				) {
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.modifierActivité = {name: "Modifier l'activité"
									, icon: ""
//									, disabled: (lbReadOnly || !loAut.bModifieActivitéDeSéance || lbSéanceVirtuelle || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onModifieActivité
									} ;

			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.changerActivité = {name: "Changer l'activité"
									, icon: ""
//									, disabled: (lbReadOnly || !loAut.bChangeActivitéDeSéance || lbSéanceVirtuelle || loInfos.bEstMultiSélection)
									, lgCallback: window.oPlanning.oMenuSéance.onChangeActivité
									} ;

		}
		
		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		if (loConfig.bDevel) {
			liBar += 1 ;
			liNbBarDepuisLastSépa += 1 ; 
			loMenu.testsDebug = {name: "Tests debug (dével uniquement)"
									, icon: ""
									, disabled: false
									, additionalClasses: "menuItemRed"
									, lgCallback: window.oPlanning.oMenuSéance.onTest
									} ;
		}

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		liBar += 1 ;
		liNbBarDepuisLastSépa += 1 ; 
		loMenu.options = {name: "Options"
								, icon: ""
								, disabled: false
								, lgCallback: window.oPlanning.oMenuSéance.onOptions
								} ;

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
//		loRessource_PM.ComplètePopUp("popActivités", liBar)
		liBar = this.complètePopUp(loMenu, liBar) ;
		
		if (liBar == 0) loMenu.sepFinal = "---------" ;
		
		return loMenu ;
	},
    
	// Code initialement dans temp.prg.buildPopupTypeAbsences
    buildPopupTypeAbsences: function(tcType_Res, tiId_Res, poMenuAbsences, paTypesAbsences, tiActiBaseProposeDatesNumérotation){
	
		var llKO = false ;
		var loTypeAbsence ;
		// Un élément de menu par type d'absence de la ressource
		for (i = 0; i < paTypesAbsences.length; i++) {
			loTypeAbsence = paTypesAbsences[i] ;
			poMenuAbsences["poserAbsence_Item" + i] = {name: loTypeAbsence.cNom
												, iId_ActiBase: loTypeAbsence.iId_ActiBase
												, disabled: false
												, lgCallback: window.oPlanning.oMenuSéance.onPoseAbsence
												} ;

/*			lsOrdre = "ON SELECTION BAR " + Transform(liBarSsMenu) + " OF " + tcNomMenuAbsences ;
																+ " loAppelant_PM.InsèreSéanceAbsence(" ;
																				+ Transform(liActivitéAbsence);
																				+ ", " + Transform(loTypeAbsence.iId_ActiBase) ;
																				+ ", '" +  Alltrim(Strtran(loTypeAbsence.cNom, "'", "")) + "'" ;
																				+ ", " + Transform(loTypeAbsence.iCouleur) ;
																				+ ")"
*/
		}

		// Options supplémentaires
		if (tiActiBaseProposeDatesNumérotation) {
			// Option de définition des dates de numérotation des absences
			poMenuAbsences.sep6 = "---------" ;
			poMenuAbsences.poserAbsence_datesNumérotation = {name: "Saisie des dates de début de numérotation"
												, disabled: false
												, iActiBaseProposeDatesNumérotation: tiActiBaseProposeDatesNumérotation
												, lgCallback: window.oPlanning.oMenuSéance.onSaisitDatesNumérotation
												} ;
//			ON SELECTION BAR <<liBarSsMenu>> OF <<tcNomMenuAbsences>> Ed_DatesRazNoSéances(<<tiId_Res>>, <<tiActiBaseProposeDatesNumérotation>>)
		}
		
		// Valeur de retour
		return !llKO ;
		
	},
    
	// Code initialement dans cGrilleHebdo4_ctnVisionneuse.ComplètePopUp
    complètePopUp: function(loMenu, liBar){
		var loConfig = window.oPlanning.oStructure.oConfig ;
 		
 		if (liBar > 0) {
	 		// Ajouter une barre de séparation
			liBar += 1 ; 
			loMenu.sep5 = "---------" ;
		}
		
		var liBarSsMenu ;
		liBarSsMenu = 0 ;

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Type d'affichage
		liBar += 1 ;
		loMenu.modeAffichage = {name: "Affichage"
								, icon: ""
								, items: {}
								} ;
		var liBarSsMenu = 0 ;
		
		console.log(loMenu.modeAffichage);
		
		// Affichage normal
		liBarSsMenu = liBarSsMenu + 1 ;
		loMenu.modeAffichage.items.modeAffichage_horairesDeJour = {name: "Horaires de jour"
//													, type:"checkbox"
													, type: 'radio'
													, radio: 'modeAffichage'
													, value: window.oPlanning.oCst.eTypesAffichageJours.Normal + ""
													, selected: (loConfig.cTypeAffichageJours == window.oPlanning.oCst.eTypesAffichageJours.Normal)
													, lgCallback: window.oPlanning.oMenuSéance.onModeAffichage_horairesDeJour
													, events: {click: function(e) {window.oPlanning.oMenuSéance.onModeAffichage_horairesDeJour($(this)); } }
													} ;
//		ON SELECTION BAR liBarSsMenu OF (lsSsMenu) ioVisionneusePopUp.LanceActionDuPopUp("ChangeModeAffichage('N')")
		
		// Affichage inverse
		liBarSsMenu = liBarSsMenu + 1 ;
		loMenu.modeAffichage.items.modeAffichage_horairesDeNuit = {name: "Horaires de nuit"
//													, type:"checkbox"
													, type: 'radio'
													, radio: 'modeAffichage'
													, value: window.oPlanning.oCst.eTypesAffichageJours.Inverse + ""
													, selected: (loConfig.cTypeAffichageJours == window.oPlanning.oCst.eTypesAffichageJours.Inverse)
													} ;
//		ON SELECTION BAR liBarSsMenu OF (lsSsMenu) ioVisionneusePopUp.LanceActionDuPopUp("ChangeModeAffichage('I')")
		
		// Affichage complet
		liBarSsMenu = liBarSsMenu + 1 ;
		loMenu.modeAffichage.items.modeAffichage_horaires24Heures = {name: "24h/24"
//													, type:"checkbox"
													, type: 'radio'
													, radio: 'modeAffichage'
													, value: window.oPlanning.oCst.eTypesAffichageJours.Complet + ""
													, selected: (loConfig.cTypeAffichageJours == window.oPlanning.oCst.eTypesAffichageJours.Complet)
													} ;
//		ON SELECTION BAR liBarSsMenu OF (lsSsMenu) ioVisionneusePopUp.LanceActionDuPopUp("ChangeModeAffichage('C')")

		// Eléments personnalisés d'horaires de grille
//		Local lcHoraires, lcHoraire, lcHeures, lcLibellé, lcOrdre
		var lcHoraires = loConfig.cLstHorairesGrille ;
		var laHoraires = lcHoraires.split("\n") ;
		var laHoraire, lcHeures, lcLibellé ;
// laHoraires.unshift("24h/24: " + window.oPlanning.oCst.eTypesAffichageJours.Inverse) ;
		laHoraires.push("Définir les horaires ... : Définir les horaires ...") ;
		for (var i = 0; i < laHoraires; i++) {
			laHoraire = laHoraires[i].split(":") ;
			lcLibellé = laHoraire[0].trim() ;
			lcHeures = laHoraire[1].trim() ;
			lcLibellé = lcLibellé + ((lcHeures) || lcHeures == lcLibellé) ? "" : " (" + lcHeures + ")" ;
			liBarSsMenu = liBarSsMenu + 1 ;
			loMenu.modeAffichage.items["modeAffichage_horairesNo" + i] = {name: lcHeures
														, type:"checkbox"
														, selected: (loConfig.cTypeAffichageJours == lcHeures)
														} ;
//			lcOrdre = "ON SELECTION BAR " + Transform(liBarSsMenu) + " OF " + lsSsMenu ;
//						+ " ioVisionneusePopUp.LanceActionDuPopUp([ChangeModeAffichage('" + lcHeures + "')])"
		}
		
 		// Ajouter une barre de séparation
		liBarSsMenu = liBarSsMenu + 1 ;
		loMenu.sep6 = "---------" ;
		
		// Affichage des heures : horizontales ou verticales
		liBarSsMenu = liBarSsMenu + 1 ;
		loMenu.modeAffichage.items.modeAffichage_heuresVerticales = {name: "Disposer les heures verticalement"
													, type:"checkbox"
													, selected: (loConfig.vertical)
//													, selected: ( fn.planning_sideBarConfig.onChangeOrientation("V"))
													} ;
//		ON SELECTION BAR liBarSsMenu OF (lsSsMenu) ioVisionneusePopUp.LanceActionDuPopUp("ChangeModeAffichage(, .T.)")

/*
		// Affichage du type horaire : AM PM(tailles activités fixes) ou Horaires Chronologiques
		if (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning) {
			liBarSsMenu = liBarSsMenu + 1 ;
			loMenu.modeAffichage.items.chronologique = {name: "Disposer les activités chronologiquement"
														, type:"checkbox"
														, selected: (loConfig.iTypeHoraires == window.oPlanning.oCst.eTypesHoraire.Chronologique)
														, disabled:(loConfig.lOptionsAffichageVerrouillées)
														} ;
//			ON SELECTION BAR liBarSsMenu OF (lsSsMenu) ioVisionneusePopUp.LanceActionDuPopUp("ChangeModeAffichage(, , ,.T.)")
		}

		// Séparer les ressources ou non
		if (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning) {
			liBarSsMenu = liBarSsMenu + 1 ;
			loMenu.modeAffichage.items.ressourcesSéparées = {name: "Séparer les ressources"
														, type:"checkbox"
														, selected: (loConfig.iTypeAffichageRessources = window.oPlanning.oCst.eTypesAffichageRessources.RessourcesSéparées)
														, disabled:(loConfig.lOptionsAffichageVerrouillées)
														} ;
//			ON SELECTION BAR liBarSsMenu OF (lsSsMenu) ioVisionneusePopUp.LanceActionDuPopUp("ChangeModeAffichage(, , , , .T.)")
		}
		
		// Affichage ou non des erreurs de placement des séances
		if (loConfig.iTypePrédéfini == window.oPlanning.oCst.eTypesGrille.Planning) {
			liBarSsMenu = liBarSsMenu + 1 ;
			loMenu.modeAffichage.items.ressourcesSéparées = {name: "Séparer les ressources"
														, type:"checkbox"
														, selected: (loConfig.iTypeAffichageRessources = window.oPlanning.oCst.eTypesAffichageRessources.RessourcesSéparées)
														, disabled:(loConfig.lOptionsAffichageVerrouillées)
														} ;
			DEFINE BAR (liBarSsMenu) OF (lsSsMenu) PROMPT "Afficher les anomalies sur toutes les séances"
			ON SELECTION BAR liBarSsMenu OF (lsSsMenu) ioVisionneusePopUp.LanceActionDuPopUp("ChangeModeAffichage(, , , , , .T.)")
			SET MARK OF BAR liBarSsMenu OF (lsSsMenu) ;
					TO loConfig.ibMontreConflitsSurSéances
		}

		// ----------------------------------------------------------------------------------------------------------------------------------------------------------- 
		// Couleurs des séances
		If This.oConfig.iiTypePrédéfini = eiTypeGrillePlanning
			liBar += 1 ;
			DEFINE BAR (liBar) OF (psNomMenu) PROMPT "Couleurs"
			lsSsMenu = "CouleursSéances"
			DEFINE Popup(lsSsMenu) MARGIN SHORTCUT RELATIVE
			ON Bar liBar OF (psNomMenu) ACTIVATE Popup(lsSsMenu)
			liBarSsMenu = 0

			// Couleurs des activités de base
			liBarSsMenu = liBarSsMenu + 1 ;
			This.ComplètePopup_AjouteMenuCouleurs(liBarSsMenu ;		&& piBar
												, lsSsMenu ;		&& psMenu
												, "Couleur des activités de bases" ;		&& psTitre	&& FB 20110825 : "Couleur des activités de base" changé en "Couleur des activités de bases"
												, eiTypeCouleurActivités ;		&& piTypeCouleur
												)
			
			// Couleurs de l'établissement
			liBarSsMenu = liBarSsMenu + 1 ;
			This.ComplètePopup_AjouteMenuCouleurs(liBarSsMenu ;		&& piBar
												, lsSsMenu ;		&& psMenu
												, "Couleur de l'établissement" ;		&& psTitre
												, eiTypeCouleurEtablissement ;		&& piTypeCouleur
												)
			
			// Couleur du service
			liBarSsMenu = liBarSsMenu + 1 ;
			This.ComplètePopup_AjouteMenuCouleurs(liBarSsMenu ;		&& piBar
												, lsSsMenu ;		&& psMenu
												, "Couleur du service" ;		&& psTitre
												, eiTypeCouleurService ;		&& piTypeCouleur
												)

			
			// Couleur selon confirmation du groupe
			liBarSsMenu = liBarSsMenu + 1 ;
			This.ComplètePopup_AjouteMenuCouleurs(liBarSsMenu ;		&& piBar
												, lsSsMenu ;		&& psMenu
												, "Couleur selon confirmation du groupe" ;		&& psTitre
												, eiTypeCouleurPresGpeConfirmée ;		&& piTypeCouleur
												)
			
			// Aucune couleur
			liBarSsMenu = liBarSsMenu + 1 ;
			This.ComplètePopup_AjouteMenuCouleurs(liBarSsMenu ;		&& piBar
												, lsSsMenu ;		&& psMenu
												, "Aucune couleur" ;		&& psTitre
												, eiTypeCouleurAucune ;		&& piTypeCouleur
												)
		Endif 
*/
		
		// Valeur de retour
		return liBar ;
	},

	// -------------------------------------------------------------------------------------------
	// Actions lors du clic sur un item
    onItemClick: function(key, options){
        
		if (key === "Supprimer"){
			// Supprimer le nouveau patient
			var liIdUsager = window.oPlanning.oVue.getIdUsager(options.$trigger[0].id) ;
			window.oPlanning.oControleur.supprimeUsager(liIdUsager);
			return ;
		}
		
		// Retrouver la référence de l'item cliqué
		var laKeys = key.split("_") ;
		var keyTmp = "";
		var oItem = options ;
		for (i = 0; i < laKeys.length; i++) {
			if (i > 0) keyTmp += "_" ;
			keyTmp += laKeys[i] ;
			oItem = oItem.items[keyTmp] ;
		}

		// Appeller le callback approprié
		if (oItem.lgCallback) {
			// Le callback est défini correctement
			oItem.lgCallback(key, options, oItem) ;
		} else {
			// Le callback n'est ps défini
			var m = "clicked: " + oItem.name + " " ;
			window.console && console.log(m) || alert(m); 
		}
	},
	
	// -------------------------------------------------------------------------------------------
	onCopie: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.copieSéance(loInfos.oDiv) ;
	},
	onColle: function(psKey, poOptions, poClickedItem, pbSpécial) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.colleSéance(pbSpécial, loInfos.oDiv) ;
	},
	onCollageSpécial: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.colleSéance(true, loInfos.oDiv) ;
	},
	
	// -------------------------------------------------------------------------------------------
	onRemetJournéeAuStandard: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.remetJournéeAuStandard(loInfos.sIdRessource, loInfos.dDate) ;
	},
	
	// -------------------------------------------------------------------------------------------
	onSupprimeSéance: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.supprimeSéance(loInfos.sIdSeance) ;
	},
	
	// -------------------------------------------------------------------------------------------
	onRendSéanceExceptionnelle: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.rendSéanceExceptionnelle(loInfos.sIdSeance) ;
	},
	
	// -------------------------------------------------------------------------------------------
	onEffaceTout: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.effaceTout(loInfos.sIdRessource, loInfos.dDate) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onChoisitPoseActivité: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		
		window.oPlanning.oControleur.choisitPoseActivité(loInfos.sIdRessource, loInfos.dDate, loInfos.iDebut, loInfos.iFin) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onCréePoseActivité: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.créePoseActivité(loInfos.sIdRessource, loInfos.dDate, loInfos.iDebut, loInfos.iFin) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onPoseSeanceDeLActivité: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.poseSeanceDeLActivité(loInfos.sIdRessource, loInfos.dDate, loInfos.iDebut, loInfos.iFin) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onPoseAbsence: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		var liActiBase = poClickedItem.iId_ActiBase ;
		resetContextMenu() ;
		window.oPlanning.oControleur.poseAbsence(loInfos.sIdRessource, loInfos.dDate, loInfos.iDebut, loInfos.iFin, liActiBase) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onSaisitDatesNumérotation: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		var liActiBaseProposeDatesNumérotation = poClickedItem.iActiBaseProposeDatesNumérotation ;
		resetContextMenu() ;
		window.oPlanning.oControleur.saisitDatesNumérotation(loInfos.sIdRessource, loInfos.dDate, liActiBaseProposeDatesNumérotation) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onModifieSéance: function(psKey, poOptions, poClickedItem) {
//MG Modification 20200225 Début
		loEntityFormPopup = newEntityFormPopup() ;
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		var idSeance = loInfos.sIdSeance.split(':')[1];
		loEntityFormPopup.popUpEntité("seancesTrad", idSeance, window.oPlanning.oControleur.refreshAfterAction);
		window.oPlanning.oVue.setDisabled();
//		resetContextMenu() ;
//		window.oPlanning.oControleur.modifieSéance(loInfos.sIdSeance) ;
//MG Modification 20200225 Fin
	},
 	
	// -------------------------------------------------------------------------------------------
	onScindeSérie: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		var liSérie = poClickedItem.iSérieSéance ;
		resetContextMenu() ;
		window.oPlanning.oControleur.scindeSérie(loInfos.sIdSeance, loInfos.dDate, liSérie) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onChangeFréquenceSérie: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		var liId_Fréquence = poClickedItem.iId_Fréquence ;
		resetContextMenu() ;
		window.oPlanning.oControleur.changeFréquenceSérie(loInfos.sIdSeance, liId_Fréquence, loInfos.sIdRessource, loInfos.dDate) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onChangePropriétaire: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.changePropriétaire(loInfos.sIdSeance) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onMontreConflits: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.montreConflits(loInfos.sIdSeance) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onWzConflits: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.wzConflits(loInfos.sIdSeance, loInfos.sIdRessource, loInfos.dDate, loInfos.iDebut, loInfos.iFin) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onWzRéaffectationUsagers: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.wzRéaffectationUsagers(loInfos.sIdSeance, loInfos.sIdRessource, loInfos.dDate, loInfos.iDebut, loInfos.iFin) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onWzAbsenceEtablissement: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.wzAbsenceEtablissement(loInfos.dDate, loInfos.iDebut, loInfos.iFin) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onModifieActivité: function(psKey, poOptions, poClickedItem) {
//MG Modification 20200225 Début
		loEntityFormPopup = newEntityFormPopup() ;
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		var idSeance = loInfos.sIdSeance.split(':')[1];
		loEntityFormPopup.popUpEntité("activitesTrad", idSeance, window.oPlanning.oControleur.refreshAfterAction);
//		resetContextMenu() ;
//		window.oPlanning.oControleur.modifieActivité(loInfos.sIdSeance) ;
//MG Modification 20200225 Fin
	},
 	
	// -------------------------------------------------------------------------------------------
	onChangeActivité: function(psKey, poOptions, poClickedItem) {
//MG Modification 20200225 Début
//		cGestionnaireActions_Symfony.onDoubleClicSéance();
//		resetContextMenu() ;
//		window.oPlanning.oControleur.changeActivité(loInfos.sIdSeance) ;
	
//		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
//		
//		var ldDate = loInfos.dDate ;
//		var ldDate = ldDate.getFullYear() 
//					+ "-" + (getMonth(ldDate, true)) 
//					+ "-" + padLeft(ldDate.getDate(), 2, '0') ;
//
//		var lsDebut = (minToHeures(loInfos.iDebut)).replace('h', ':') ;	// format HH:MM
//		var lsFin = (minToHeures(loInfos.iFin)).replace('h', ':') ;		// format HH:MM
//		var lsLstRes = loInfos.oRessource.sIdRessource ;
//		var lsLstResSelected = loInfos.oRessource.sIdRessource ;
//		var lsURL;
//		var idSeance = loInfos.sIdSeance.split(':')[1];
//		var titre;
//		lsURL = getBaseURL() + 'TestYohan/getIDActivites/'+lsLstRes+'/'+lsLstResSelected+'/'+ldDate+'/'+lsDebut+'/'+lsFin ;
//			titre = "Changer une activité";
//			loPopup = newPopup_getIdActivite(lsURL , titre);
//			loPopup.popup(window.oPlanning.oControleur.refreshAfterAction);
		
//		lsURL = getBaseURL() + 'TestYohan/getIDActivites/'+lsLstRes+'/'+lsLstResSelected+'/'+ldDate+'/'+lsDebut+'/'+lsFin ;
//		titre = "Poser une séance ou une participation";
//		loPopup = newPopup_getIdActivite(lsURL , titre);
	//MG Modification 20200306 Début
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		var cGestionnaireActions_Symfony = window.oPlanning.oControleur.getGestionnaireActions();
		cGestionnaireActions_Symfony.onCliqueDroitChangerActivité(loInfos, window.oPlanning.oControleur.refreshAfterAction);
	//MG Modification 20200306 Fin
//MG Modification 20200225 Fin
	},
 	
	// -------------------------------------------------------------------------------------------
	onTest: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.doTest(loInfos.sIdSeance, loInfos.sIdRessource, loInfos.dDate, loInfos.iDebut, loInfos.iFin) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onOptions: function(psKey, poOptions, poClickedItem) {
		var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
		resetContextMenu() ;
		window.oPlanning.oControleur.changeOptions(loInfos.sIdSeance) ;
	},
 	
	// -------------------------------------------------------------------------------------------
	onModeAffichage_horairesDeJour: function(poThis) {
		// selon https://github.com/swisnl/jQuery-contextMenu/issues/61
		var $item = poThis.closest('li');
		var data = $item.data();
		var lbCoché = data.contextMenu.items.modeAffichage_horairesDeJour.selected ;
// alert("test : " + (lbCoché)?"coché":"NON coché") ;
		
		// Essayer de désactiver les options : ne fonctionne pas car la propriété disabled ne modifie pas le CSS
		var p = data.contextMenuRoot.items ;
		var item ;
		for (var prop in p) {
			if (!p.hasOwnProperty(prop)) {
				//The current property is not a direct property of p
				continue;
			}
			//Do your logic with the property here
			item = p[prop] ;
			item.disabled = true ;
			
		}

	},
	
	// -------------------------------------------------------------------------------------------
	// Actions lors de la fermeture du menu
//    onFermeMenu: function(opt, poThis, poOldData, poNewData){
    onFermeMenu: function(opt, poThis, poNewData){
        var lbModifie = false ;
		var poOldData = this.oOldData ;
        
        if (poOldData.modeAffichage != poNewData.modeAffichage || poOldData.modeAffichage_heuresVerticales != poNewData.modeAffichage_heuresVerticales){
			// Le mode d'affichage a changé
            lbModifie = true;
        }
		if (lbModifie) {
			var loInfos = window.oPlanning.oMenuSéance.getContextInfos() ;
			resetContextMenu() ;
			window.oPlanning.oControleur.modeAffichageChangé(loInfos.sIdSeance, poNewData.modeAffichage, poNewData.modeAffichage_heuresVerticales) ;
		}
        
/*		
		if (lbModifie) {
			window.oPlanning.oControleur.setModifie(true) ;
		}
*/    }, 
    
    bidonPourFinDeClasse: 0
});
// window.oPlanning.oMenuSéance = new cMenuSéance();
