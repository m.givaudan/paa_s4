/**###############################################
 *      CLASSE RESSOURCE
 *     Enfant de la classe Journée, et parent de la classe seance
 *     @param {Array} aParaRes - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
 *     
 *     Ce tableau de paramètres contient les paramètres suivants : 
 *     @param {String} sIdRessource - Identifiant de la Ressource
 *     @param {String} cLibelle - Nom complet de la Ressource
 *     @param {String} cLibelleCourt - Nom court de la Ressource
 *     @param {String} cTypeRes - Le type de la Ressource    
 ###############################################*/
//var ressource = Class.create({
//    initialize: function (aParaRes, psIdJour, psIdTranche, pdDate){
var cRessource = Class.extend({
    init: function (aParaRes, psIdJour, psIdTranche, pdDate){
        if (aParaRes){
			this.sIdJour = psIdJour ;
			this.sIdTranche = psIdTranche ;
			this.sIdRessource = aParaRes.sIdRessource ;

			// cLibelle et cLibelleCourt sont optionnel. on prend l'id si aucun des deux n'est donnée
			if (!aParaRes.cLibelle && !aParaRes.cLibelleCourt) {
				this.cLibelle = this.sIdRessource;
				this.cLibelleCourt = this.sIdRessource;
			} else if (!aParaRes.cLibelle) {
				this.cLibelle = aParaRes.cLibelleCourt;
				this.cLibelleCourt = aParaRes.cLibelleCourt;
			} else if (!aParaRes.cLibelleCourt) {
				this.cLibelle = aParaRes.cLibelle;
				this.cLibelleCourt = aParaRes.cLibelle;
			} else {
				this.cLibelle = aParaRes.cLibelle;
				this.cLibelleCourt = aParaRes.cLibelleCourt;
			}
		}
		this.vertical = aParaRes.vertical ;
		this.cCouleur = aParaRes.cCouleur ;
		
		if (pdDate) this.dDate = pdDate ;

        this.toString();
    },
	// Renvoyer l'objet HTML représentant cette ressource
    getIdDiv: function () {
		return window.oPlanning.oStructure.getIdDivRessource(this.sIdRessource, this.sIdJour, this.sIdTranche) ;
    },
	// Renvoyer l'objet HTML représentant cette ressource
    getDiv: function (psIdJour, psIdTranche) {
//		return document.getElementById(this.sIdDivRessource) ;
		return document.getElementById(this.getIdDiv(psIdJour, psIdTranche)) ;
    },
	getIdDivTranche: function () {
		return window.oPlanning.oStructure.getIdDivTranche(this.sIdTranche, this.sIdJour) ;
	},
	getDivTranche: function () {
		return document.getElementById(this.getIdDivTranche()) ;
	},
	// Récupérer le code HTML correpondant à une séance à l'intérieur d'une ressource
	getTranche: function () {
		return window.oPlanning.oStructure.getTrancheAvecDiv(this.getIdDivTranche()) ;
	},
	
	// Renvoyer l'objet HTML représentant cette ressource
    rtvHTML: function (piPosition, piDimension, piDimension2, pbPremier) {
	
		var border = window.oPlanning.oCst.DimBordureRessources /*1*/ ;
		var codeCouleurFond = "";
// LG 20160930 old		if (window.Nav == 3 && (BrowserDetect.version == 8 || BrowserDetect.version == 7))
		if (isIE() && (BrowserDetect.version == 8 || BrowserDetect.version == 7))
			codeCouleurFond = "background-color : white; ";

		var lsHTML = "<div class='ressource'"
				+ " id='" + this.getIdDiv() + "'"
				+ " style='"
				+ "position: absolute; "
				+ codeCouleurFond
				+ "border-width: " + border + "px; "
				;
if (isIE()) {
	// Sous IE, les ressources doivent être colorées pour être clicables
//	lsHTML += "background-color: red; " ;
	lsHTML += "background-color: " + this.cCouleur + "; " ;
}
		if (this.vertical) {
			lsHTML = lsHTML
					+ "top: 0px; "
					+ "left: " + piPosition + "px; "
					+ "height: " + piDimension2 + "px; "
//					+ "width: " + parseInt(piDimension) + "px;";
					+ "width: " + piDimension + "px;";
			if (!pbPremier) {
// LG 20161013 déac				lsHTML += "border-left-color: " + this.cCouleur + "; " ;
			}
		} else {
			lsHTML = lsHTML + " left :0px; "
					+ "top:" + piPosition + "px; "
					+ "width: " + piDimension2 + "px; "
					+ "height: " + piDimension + "px;"
					;
			if (!pbPremier) {
// LG 20161013 déac				lsHTML += "border-top-color: " + this.cCouleur + "; " ;
			}
		}
		
		lsHTML += "'>";
		
		return lsHTML ;
	},
		
    /**
     * ajouteSeance, Ajoute une séance à la ressource Courante
     * Cette méthode crée une instance de la classe seance
     * 
     * @param {Array} aParaSeance - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
     * @see voir la classe seance pour la documentation du tableau de paramètres
     * @return {String} cRes - Contient le type de seance
     */
    ajouteSeance: function (aParaSeance) {
		aParaSeance.dDate = this.dDate ;
		var maSeance = new cSeance(aParaSeance, this.sIdRessource, this.sIdTranche);
        window.oPlanning.oStructure.aSeances.push(maSeance);
        return maSeance;
	},
    ajouteZoneInterdite: function (aParaZoneInterdite) {
        var laZoneInterdite = new cZoneInterdite(aParaZoneInterdite, this.sIdRessource, this.sIdTranche);
        window.oPlanning.oStructure.aZonesInterdites.push(laZoneInterdite);
        return laZoneInterdite;
	},

    /**
     *toString 
     *@return {String} renvoit les informations de l'instance courante de la ressource
     *
     */
    toString: function (){
        return "Ressource : " + this.cLibelle + ", ID : " + this.sIdRessource + ", Type : " + this.cTypeRes + ", Nom court : " + this.cLibelleCourt;
    },
	
	/**
	 * creerSeanceVide, Ajoute une seance vide à l'endroit ou l'on a cliqué
	 * Cette méthode calcule les paramètres à passer, puis fait un  ajouteSeance()
	 * 
	 * @param {Event} event - Evenement d'ou provient l'appel -> onclick
	 * @see voir la méthode ajouteSeance pour la documentation du tableau de paramètres calculé ici
	 */
	// NB : 20150318 appelé par select pour la creation d'une seance vide, on instancie une seance on lui 
	// ajoute les different element necessaire et on finit par l'ajouter au ressource 
	creerSeanceVide: function (event) {

//		if (!window.oPlanning.oStructure.lReadWrite) {
		if (!window.oPlanning.oStructure.getReadWrite()) {
			// Lecture seule
			return false ;
		}
		if (!(this.getDiv())) {
			// Anomalie (par exemple quand on clique pour avlider une cbcRessource, dont l'événement fait cacehr le planning)
			return false ;
		}
		
		// Déterminer l'heure cliquée
		var mouse = getCoordsInDiv(event, this.getDiv());
		var liPosition = (this.vertical)?mouse.mouseY:mouse.mouseX ;
		var loTranche = this.getTranche() ;
		if (!window.oPlanning.oStructure.getReadWrite(loTranche.dDate)) {
			// Lecture seule
			return false ;
		}
		
		var heureClick = loTranche.getHeureParCoord(liPosition);
		heureClick = arrondirHeure(heureClick, window.oPlanning.oStructure.pas, true) ;
		
		// Calculer la fin et corriger si nécessaire
		var debutSeance = heureClick ;
		if (debutSeance < loTranche.iDebut) debutSeance = loTranche.iDebut;
		var finSeance = debutSeance + window.oPlanning.oStructure.dureeDftSeance;
		if (finSeance > loTranche.iFin) {
			finSeance = loTranche.iFin;
			debutSeance = finSeance - window.oPlanning.oStructure.dureeDftSeance;
		}

		// Corriger si nécessaire
		// La séance doit rester dans les limites de la tranche
		var HeureSeanceSup = loTranche.iDebut ;
		var HeureSeanceInf = loTranche.iFin ;
		
		// La séance ne doit pas déborder sur les séances postérieures et antérieures de la même ressource
		var loVoisine ;
		var lsIdRessource = this.sIdRessource ;
		for (var i = 0; i < window.oPlanning.oStructure.aSeances.length; i++) {
			loVoisine = window.oPlanning.oStructure.aSeances[i] ;
			if (loVoisine.sIdRessource == lsIdRessource
					&& loVoisine.sIdTranche == loTranche.sIdTranche
					&& loVoisine.sIdJour == loTranche.sIdJour) {
				// Séance du même jour pour la même ressource
				if (parseInt(loVoisine.iFin) <= heureClick && parseInt(loVoisine.iFin) >= HeureSeanceSup)
					HeureSeanceSup = parseInt(loVoisine.iFin);
				if (parseInt(loVoisine.iDebut) >= heureClick && parseInt(loVoisine.iDebut) <= HeureSeanceInf)
					HeureSeanceInf = parseInt(loVoisine.iDebut);
			}
		}

		// Même chose avec les zones interdites : 
		var loZone ;
		for (i = 0; i < window.oPlanning.oStructure.aZonesInterdites.length; i++) {
			loZone = window.oPlanning.oStructure.aZonesInterdites[i] ;
			if (loZone.sIdRessource == lsIdRessource
					&& loZone.sIdTranche == loTranche.sIdTranche
					&& loZone.sIdJour == loTranche.sIdJour) {
				if (parseInt(loZone.iFin) <= heureClick && parseInt(loZone.iFin) >= HeureSeanceSup)
					HeureSeanceSup = parseInt(loZone.iFin);
				if (parseInt(loZone.iDebut) >= parseInt(heureClick) && parseInt(loZone.iDebut) <= parseInt(HeureSeanceInf))
					HeureSeanceInf = parseInt(loZone.iDebut);
			}
		}

		if ((HeureSeanceInf - HeureSeanceSup) < window.oPlanning.oStructure.dureeDftSeance) {
			// Si la place est insufisante, on ajuste avec la place disponible
			debutSeance = HeureSeanceSup;
			finSeance = HeureSeanceInf;
		} else if (finSeance > HeureSeanceInf) {
			// Si la séance vide déborde sur une séance et qu'il y a la place au dessus, on la relève. 
			finSeance = HeureSeanceInf;
			debutSeance = parseInt(HeureSeanceInf - window.oPlanning.oStructure.dureeDftSeance);
		}
		
		var lsIdTranche = loTranche.sIdTranche ;
		var lsIdJour = loTranche.sIdJour ;

		// Créer et renvoyer l'objet séance
		var laPS = {
			sIdSeance: 'S0_' + debutSeance,
			sIdRessource: lsIdRessource,
			sIdJour: lsIdJour,
			dDate: this.dDate,
			iDebut: debutSeance,
			iFin: finSeance,
			cLibelle: "",
			cInfobulle: "Séance vide",
			cCouleur: '#FFFFFF',
			iHeureMin: window.oPlanning.oGrille.iHeureMin,
			iHeureMax: window.oPlanning.oGrille.iHeureMax,
			vertical: this.vertical
		};
		var loSeance = new cSeance(laPS, lsIdRessource, lsIdTranche);
		return loSeance;
   }
    
});
