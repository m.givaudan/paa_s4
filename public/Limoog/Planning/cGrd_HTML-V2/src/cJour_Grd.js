/**###############################################
 *     CLASSE cJour
 *      
 *     @param {Array} aParajournee - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
 *     
 *     Ce tableau de paramètres contient les paramètres suivants :  
 *     @param {String} sIdDivJour - Identifiant de la journee
 *     @param {String} cLibelle - Nom complet de la Semaine
 *     @param {String} cCouleur - Couleur d'affichage de la journée
 *     @param {Number} iHeureDebut - Heure (en minutes) de début de l'affichage de la journee
 *     @param {Number} iHeureFin - Heure (en minutes) de fin de l'affichage de la journee
 ###############################################*/

 var cJour = Class.extend({
    init: function (aParajournee){
// test = erreur ;
		this.sIdJour = aParajournee.sIdJour;
        // NB : 20150324
        // Dans le cas du jour la div et l'id on la meme valeur
        this.sIdDivJour = aParajournee.sIdJour ;
        this.cLibelle = aParajournee.cLibelle ;
		this.cInfoBulle = aParajournee.cInfoBulle?aParajournee.cInfoBulle:aParajournee.cLibelle ;
		
        if (aParajournee.cCouleur) this.cCouleur = aParajournee.cCouleur;
        else this.cCouleur = "white";

/*		// aParajournee.sIdDivJour est de la forme 'J20121001'
		var lsDate = aParajournee.sIdJour;
		lsDate = lsDate.substring(1, 9);
		// sous IE, parseInt a besoin de la base 10 pour convertir cetains nombres qui, s'ils sont précédés de "0", sont pris comme de l'octal
		// (cf. http://stackoverflow.com/questions/8763396/javascript-parseint-with-leading-zeros)
		var ldDate = new Date(parseInt(lsDate.substring(0, 4), 10)
							, parseInt(lsDate.substring(4, 6), 10) - 1
							, parseInt(lsDate.substring(6, 8), 10));
		this.dDate = ldDate ;
*/
//		this.dDate = window.oPlanning.oData.sIdJourToDate(aParajournee.sIdJour) ;
		this.dDate = aParajournee.dDate ;
		this.vertical = aParajournee.vertical ;

    },
    getIdDiv: function (){return window.oPlanning.oStructure.getIdDivJour(this.sIdJour) ;},
    getDiv: function (){return document.getElementById(this.getIdDiv()) ;},
    /**
     *toString 
     *@return {String} renvoit les informations de l'instance courante de la journee
     *
     */
    toString: function () {return "jour : " + this.cLibelle + ", ID : " + this.sIdJour;},
    /**
     */
    ajouteTranche: function (trancheHoraire) {
        var aParaTranche = {
            cLibelle: this.cLibelle
			, sIdTranche: trancheHoraire.sIdTranche
//            , sIdDivTranche: 'tranche' + trancheHoraire.sIdTranche + "-" + this.sIdJour
            , cCouleur: this.cCouleur
            , iDebut: trancheHoraire.iDebut
            , iFin: trancheHoraire.iFin
            , dDate: this.dDate
			, vertical: this.vertical
			};
// LG 20160826 old        var matranche = new tranche(aParaTranche, this.sIdDivJour);
        var laTranche = new cTranche(aParaTranche, this.sIdJour);
        return laTranche;
    },
	
	// -----------------------------------------------------------------------------------
	// Renvoyer e code HTML de la div de jour
    rtvHTML: function (piPosition, piDimension) {
		var border = window.oPlanning.oCst.DimBordureJours ;

		var lsHTML = "" ;
		var lsIdDivJour = this.getIdDiv() ;
		lsHTML = lsHTML
				+ "<div "
				+ "class='jour' "
				+ "id='" + lsIdDivJour + "' "
				+ "style='"
					+ "text-align: center; "
					+ "background-color : " + this.cCouleur + ";"
					+ "text-align : center; "
					;
		if (this.vertical) {
			lsHTML = lsHTML
                                                 + "border-right: 1px solid #AAAAAA; "
                                                
						+ "left : " + piPosition + "px; "
						+ "height : 100%; "
						+ "width : " + piDimension + "px; " ;
		} else {
			lsHTML = lsHTML 
                                                 + "border-top: 1px solid #AAAAAA ; "
                                                 
						+ "height: " + piDimension + "px; "
						+ "top: " + piPosition + "px; "
						+ "width: 100%; " ;
		}
		lsHTML = lsHTML + "'>" ;
		
		// Bouton de jour
		lsHTML = lsHTML + this.rtvHTMLEnTêteJour(piDimension) ;
		
		// Boutons de ressources
		lsHTML = lsHTML + this.rtvHTMLEnTêteRessources(piDimension) ;
		
		// Tranches et ressources
		lsHTML = lsHTML + this.rtvHTMLTranchesEtRessources(piDimension) ;
		
		// Finalisation
		lsHTML = lsHTML + "</div>";
 
		return lsHTML ;
	},

	// -----------------------------------------------------------------------------------
	// Renvoyer le code HTML du bouton de jour
    rtvHTMLEnTêteJour: function (piDimension) {
		
		var lsIdDivJour = this.getIdDiv() ;
		var lsLibelle, lsTitle ;
//		var lsType = "Button" ;
		var lsType = "Div" ;
		var lsHTML = "<" + lsType + " "
				+ "class='context-menu-seance boutonJour button colléADroite' "
				+ "onmouseup='window.oPlanning.oControleur.clickBouton(event, \"" + lsIdDivJour + "\", \"\");' "
				+ "style='"
				;
		lsLibelle = this.cLibelle.toProperCase() ;
		if (this.dDate.getFullYear() > 1950)
			lsLibelle += " " + DTOC(this.dDate) ;
//		lsTitle = lsLibelle ;
		lsTitle = this.cInfoBulle ;
		var liRéduitTaille = 0;	// box-sizing: border-box permet de gérer pareil que pour les vrais boutons;
		if (this.vertical) {
			lsHTML = lsHTML
						+ "width: 100%; "
						+ "height: " + (window.oPlanning.oCst.HauteurBouton - liRéduitTaille) + "px; "
						+ "top: " + window.oPlanning.oCst.Marge + "px; "
					;
				if (isIE()) lsHTML = lsHTML + "position: absolute; " ;	// Sous IE, les boutons apparaissent translucides sinon
		} else {
			// Astuce pour compenser la mauvaise prise en charge de "text-align: center; " dans les boutons sur IE7
			lsLibelle = lsLibelle.substr(0, 2) ;
			if (this.dDate.getFullYear() > 1950) lsLibelle += " " + (DTOC(this.dDate)).substr(0, 5) ;
			lsLibelle = window.oPlanning.oVue.cvtLibelleBoutonPourModeHorizontal(lsLibelle) ;
			lsHTML = lsHTML 
						+ "text-align: center; "
						+ "padding: 0; "
						+ "line-height: 1em; "
						+ "float: left; "
						+ "height: " + piDimension + "px;"
						+ "width: " + (window.oPlanning.oCst.LargeurBouton - liRéduitTaille) + "px; "
						+ "left: " + window.oPlanning.oCst.Marge + "px; "
					;
		}
// 16777215
		lsHTML = lsHTML 
					+ "'"
					+ " title='" + lsTitle + "'"
					+ "/>"
					// Aligner le libellé verticalement
					+ "<table"
						+ " class = 'TablePourCentrage'"
						+ " style='"
//							+ "width:100%; "
//							+ "height:100%; "
//							+ "vertical-align: middle;"
//							+ "word-break: break-all;"
							+ "background-color: " + this.cCouleur + ";"
							+ "'"
						+ "><tr><td>"
						+ lsLibelle 
						+ "</td></tr></table>"
					+ "</" + lsType + ">"
					;
		
		return lsHTML ;
		
    },

	// -----------------------------------------------------------------------------------
	// Renvoyer le code HTML des boutons de ressources
    rtvHTMLEnTêteRessources: function (piDimension) {
	
		var iLargeurRessource, iHauteurRessource, lsLibelle ;
		var lsHTML = '';
		var lsType = 'Div' /*'Button'*/;
		var posCurrent = 0;
		var loObjRessource;
		var lsIdDivJour = this.sIdJour ;
		var liNbRessources = window.oPlanning.oStructure.getNbRessources() ;
        var currentPos = window.oPlanning.oCst.HauteurBouton + window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource ;
		var lsIdDivRessource ;
//		var liRéduitTaille = (lsType.toUpperCase() == 'BUTTON')?0:1;	// La bordure du bouton est incluse dedans
		var liRéduitTaille = 0;	// box-sizing: border-box permet de gérer pareil que pour les vrais boutons;
// LG 20171211 début
// 		iLargeurRessource = ((piDimension) / (liNbRessources));
		var border = window.oPlanning.oCst.DimBordureRessources /*1*/ ;
		iLargeurRessource = ((piDimension + border) / (liNbRessources)) - border * 2;
		var liIncrémentPosition = iLargeurRessource + border; /* * 2*/
// LG 20171211 fin

		for (i = 0; i < window.oPlanning.oStructure.aRessources.length; i++) {
			currentPos = window.oPlanning.oCst.HauteurBouton + window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource ;
			loObjRessource = window.oPlanning.oStructure.aRessources[i];
			lsIdDivRessource = loObjRessource.getIdDiv() ;
			if (!(loObjRessource.sIdTranche == "0")) 
				continue ;
			if (!(loObjRessource.sIdJour == lsIdDivJour))
				continue ;

			lsHTML = lsHTML + "<" + lsType + " "
					+ "id = 'btn" + lsIdDivRessource + "'"
					+ "class='context-menu-seance boutonRessource button colléADroite shadow-sm' "
					+ "onmouseup='window.oPlanning.oControleur.clickBouton(event, \"" + lsIdDivJour + "\", \"" + lsIdDivRessource + "\");'"
					+ "title='" + loObjRessource.cLibelle + "'"
					+ "style='" 
					;
			var lsLibelle = loObjRessource.cLibelleCourt ;
			if (this.vertical) {
// LG 20171211 passÃ© + haut				iLargeurRessource = ((piDimension) / (liNbRessources));
				lsHTML = lsHTML 
							+ "top: " + currentPos + "px; "
							+ "left: " + posCurrent + "px; "
							+ "height:" + (window.oPlanning.oCst.HauteurBouton - liRéduitTaille) + "px; "
							+ "width: " + parseFloat(iLargeurRessource) + "px; "
// LG 20171211 old				posCurrent = posCurrent + parseFloat(iLargeurRessource) /*+ 1*/ ;
				posCurrent = posCurrent + liIncrémentPosition ;
			
			} else {
				if (!window.oPlanning.oStructure.lBoutonRessourceLarge)
					lsLibelle = window.oPlanning.oVue.cvtLibelleBoutonPourModeHorizontal(loObjRessource.cLibelleCourt) ;
				currentPos = window.oPlanning.oCst.LargeurBouton + window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource ;
				iHauteurRessource = ((piDimension) / (liNbRessources));
				lsHTML = lsHTML 
							+ "left :" + currentPos + "px; "
							+ "top:" + posCurrent + "px; "
							+ "width : " + (window.oPlanning.oStructure.LargeurBoutonRessource - liRéduitTaille) + "px; "
// LG 20171211 old							+ "height: " + (iHauteurRessource - liRéduitTaille) + "px; "
							+ "height: " + (iHauteurRessource - border) + "px; "
							+ "line-height: 1em; "
							+ "padding: 0; "
// LG 20171211 old				posCurrent = posCurrent + parseFloat(iHauteurRessource) /*- 1*/ ;
				posCurrent = posCurrent + iHauteurRessource - border ;
			}
			lsHTML += "'"
					+ ">" 
					// Aligner le libellé verticalement
					+ "<table"
						+ " class = 'TablePourCentrage'"
						+ " style='"
//							+ "width:100%; "
//							+ "height:100%; "
//							+ "vertical-align: middle;"
//							+ "word-break: break-all;"
							+ "background-color: " + this.cCouleur + ";"
							+ "'"
						+ "><tr><td>"
						+ lsLibelle 
						+ "</td></tr></table>"
					+ "</" + lsType + ">" ;
		}
		return lsHTML;
	},

	// rtvHTMLTranchesEtRessources
	rtvHTMLTranchesEtRessources: function (piDimensionJour) {

//		var border = window.oPlanning.oCst.DimBordureTranches/*1*/;
		var loTrancheJour, liTaille ;

		// Passer toutes les tranches pour les ajouter à la journée
		var loObjTranchePrec = false;
/*
		var liIncrementPos = window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource ;
		if (this.vertical)
			var currentPos = window.oPlanning.oCst.HauteurBouton 
								+ liIncrementPos * 2 
								+ window.oPlanning.oCst.HauteurBouton + 3 ;
		else
			var currentPos = window.oPlanning.oCst.LargeurBouton 
								+ liIncrementPos * 2 
								+ window.oPlanning.oStructure.LargeurBoutonRessource + 3 ;
*/
		var currentPos = window.oPlanning.oStructure.getDimEnTeteJour() ;
		
		var lsHTML = "";
		for (var j = 0; j < window.oPlanning.oStructure.aTranchesJours.length; j++) {
			loTrancheJour = window.oPlanning.oStructure.aTranchesJours[j] ;
			
			// Cette tranche fait-elle partie de ce jour ?
			if (loTrancheJour.sIdJour != this.sIdJour) continue;

			if (loObjTranchePrec) {
				// Ce n'est pas la première tranche
				if (loTrancheJour.iDebut > loObjTranchePrec.iFin) {
					// Cette tranche a une dimension non nulle
					if (this.bAfficheInterTranches) {
						// On doit afficher les intertranches
						// Ajouter l'interTranche
						lsHTML += loTrancheJour.rtvHTMLInterTranche(currentPos);
					}
					// Incrémenter la position
					currentPos = currentPos + window.oPlanning.oStructure.tailleHorsLimite + 2 ;
				}
			}
			loObjTranchePrec = loTrancheJour;

			liTaille = loTrancheJour.iTaille ;
			lsHTML += loTrancheJour.rtvHTML(currentPos, liTaille, piDimensionJour) ;
			
			currentPos = currentPos + liTaille - 1 ;
		}

		return lsHTML ;
	},
	
});