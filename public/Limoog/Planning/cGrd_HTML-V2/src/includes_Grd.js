/*###############################################
 *      FICHIER DES INCLUDES
 ###############################################*/

 /*
// ---------------------------------------------------
// Inclure facilement des fichiers .js
// VOIR AUSSI http://sametmax.com/include-require-import-en-javascript
// Voir aussi includeJS_Autre ci-dessous
function includeJS(fileName){
    document.write("<script type='text/javascript' src='" + includes_Grd_absolutePath(fileName) + "'></script>");
}

// ---------------------------------------------------
// Inclure facilement des fichiers .css
function includeCSS(fileName){
	document.write('<link rel="stylesheet" href="' + includes_Grd_absolutePath(fileName) + '">') ;
}

// Selon allenhwkim, http://stackoverflow.com/questions/14780350/convert-relative-path-to-absolute-using-javascript
function includes_Grd_absolutePath(href) {
	if (window.cPathJavascript) {
		// Le chemin des javascripts a été fourni
		return window.cPathJavascript + "/Partages/" + href ;
	} else {
		return  href ;
	}
}
*/
// On suppose que C:\Luc\Projets VB et FoxPro\JavaScript\Partages\src\include.js 
// a été inclus auparavant

// ****************************************************************************
// Frameworks et fonctions téléchargés
// ****************************************************************************

// jQuery, rajoute de nombreuses fonctions prévues pour être crossBrower
//includeJS('Partages/lib/jquery-1.10.2.js');
//includeJS('Partages/lib/jquery-ui.js');
//includeCSS('Partages/lib/jquery-ui.css');

// LG 20200902 début : window.baseURLJS est maintenant écrit par twig dans menu.html.twig, sur la base de la variable d'environnement baseURLJS
////MG Ajout 20200703
////Suite au fonctionnement sans vhost, le chemin du dossier public doit être indiqué
////exemple : window.baseURLJS = '/applicationPAA_S4/public';
//// console.log(window.location.hostname) ;
//// LG 20200720 début : essai de généralisationde la modif MG pour Emma
////    window.baseURLJS = 'http://localhost:8888/PAA2/paa_s4/public';
//if (window.location.hostname == 'localhost') {
//    // Machine d'Emma
//    window.baseURLJS = 'http://localhost:8888/PAA2/paa_s4/public';
//} else {
//    window.baseURLJS = '';
//}
// LG 20200720 fin

if (typeof jQuery === 'undefined') {
	// JQuery n'est pas encore inclus : l'inclure maintenant
	includeJS(window.baseURLJS + 'vendor/jQuery-3.3.1/jquery-3.3.1.min.js');
	includeJS(window.baseURLJS + 'vendor/jQueryUI-1.12.1/jquery-ui.min.js');
	includeCSS(window.baseURLJS + 'vendor/jQueryUI-1.12.1/jquery-ui.min.css');
}

// Menus contextuels
includeJS(window.baseURLJS + '/vendor/jQuery.contextMenu/jquery.contextMenu.js');
includeCSS(window.baseURLJS + '/vendor/jQuery.contextMenu/jquery.contextMenu.css');

// Prototype
// Utile en particulier pour <ObjetDom.observe>
// includeJS('../Partages/lib/prototype.js');
// prototype et jquery surchargent tous les deux $(...), 
// pour éviter le conflit, jquery utilisera $j(...) au lieu de $(...);
// var $j = jQuery.noConflict();

// sprintf.js ajoute la fonction sprintf, proche de l'équivalant PHP ou C. 
includeJS(window.baseURLJS + '/vendor/sprintf.js');

// Pour inclure la gestion du JSON même sur les vieux navigateurs
includeJS(window.baseURLJS + '/vendor/JSON2.js');

// ****************************************************************************
// Correctifs pour simplifier la gestion des évenements.
// ****************************************************************************
//includeJS('../Partages/lib/EventHelpers.js');

// Ce fichier permet le Drag&Drop en HTML5, en cross-browser.
// Le w3c devant finir le standard de l'HTML5 d'ici 2014, ce fichier
// est amené à devenir obsolète. 
//includeJS('../Partages/lib/DragDropHelpers.js');

// ****************************************************************************
// code source générique
// ****************************************************************************
// Fonctions Limoog diverses
includeJS(window.baseURLJS + '/Limoog/Partages/src/fonctions.js');
includeJS(window.baseURLJS + '/Limoog/Partages/src/fonctions_Browser.js');

//Fichier permettant de gérer AJAX
includeJS(window.baseURLJS + '/Limoog/Partages/src/functions_AJAX.js');

//Fichier permettant de gérer les liens avec VFP
includeJS(window.baseURLJS + '/Limoog/Partages/src/functions_VFP.js');

// Gestion du drag&drop utilisant DragDropHelpers. 
//includeJS('src/drag_PermissionForm.js');

//Fichier permettant de créer des classes
includeJS(window.baseURLJS + '/Limoog/Partages/src/Class.js');

// ****************************************************************************
// code source spécifique à l'application
// ****************************************************************************

includeCSS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/css/seances.css');

includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/fonctions_Grd.js');		// fonctions spécifiques à l'application
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cConstantes_Grd.js');	// Toutes les constantes utiles à l'application.

includeJS(window.baseURLJS + '/Limoog/Partages/src/cControleurBase.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cControleur_Grd.js');
// LG 20190409 début
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cGestionnaireActions_VFP.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cGestionnaireActions_Symfony.js');
// LG 20190409 fin

includeJS(window.baseURLJS + '/Limoog/Partages/src/cDataBase.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cData_Grd.js');

includeJS(window.baseURLJS + '/Limoog/Partages/src/cVueBase.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cVue_Grd.js');

includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cStructure_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cSemaine_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cCalendrier_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cMois_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cJour_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cLegende_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cTranche_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cRessource_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cSeance_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cZoneInterdite_Grd.js');
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cSeanceSelectionnee_Grd.js');

// Fonctions pour la simulation client.
//includeJS('src/SimulationClient.js');

// Menus contextuels
includeJS(window.baseURLJS + '/Limoog/Planning/cGrd_HTML-V2/src/cMenu_Grd.js');

