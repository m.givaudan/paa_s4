// cGestionnaireActions_VFP.js
// LG 20190409

// ****************************************************************************************
// Classe de gestionnaire d'actions pour le planning, sous VFP
// ****************************************************************************************
var cGestionnaireActions_VFP = cGestionnaireActionsBase.extend({

	// ---------------------------------------------------------------------------------------
	// Gestion de la fin d'un déplacement de séance
	onFinDéplacementSéance: function(poMasque, e, poDragInfos, callback){
		
		if (poDragInfos) {
			var lsRessource = poDragInfos.sIdRessource ;
			var ldDate = poDragInfos.dDate ;
// popupWindow(ldDate) ;
		} else {
			var lsRessource = poMasque.sIdRessource ;
			var ldDate = poMasque.dDate ;
		}
		var liShiftAltCtrl = eventKeyCode(e);
		var lsJSON = [{nom:"sIdSeance", "valeur": poMasque.sIdSeance}
						, {nom:"sIdRessource", "valeur": lsRessource}
						, {nom:"dDate", "valeur": ldDate}
						, {nom:"iDebut", "valeur": poMasque.iDebut}
						, {nom:"iFin", "valeur": poMasque.iFin}
						, {nom:"iAltShiftCtrl", "valeur": liShiftAltCtrl}
						] ;
// alert("poMasque.iDebut = " + minToHeures(poMasque.iDebut)	+ "<br>" + "poMasque.iFin = " + minToHeures(poMasque.iFin)) ;
		LG_Ajax_GetText("onFinDéplacementSéance", lsJSON, "", "JSON", callback);
	},

	// ---------------------------------------------------------------------------------------
	// Gestion d'un double-clic sur une séance
	onDoubleClicSéance: function(poMasque, callback){
		var loData = [{nom:"sIdSeance", valeur: poMasque.sIdSeance}
							, {nom:"sIdRessource", valeur: poMasque.sIdRessource}
							, {nom:"dDate", valeur: poMasque.dDate}
							, {nom:"iDebut", valeur: poMasque.iDebut}
							, {nom:"iFin", valeur: poMasque.iFin}
							] ;
		LG_Ajax_GetText("onDoubleClicSéance", poData, "", "JSON", callback);
	},
	
	// ---------------------------------------------------------------------------------------
	// Gestion de la suppression d'une séance
	supprimeSéance: function(psLstSéances, callback){
		var lsJSON = [{nom:"sIdSeance", "valeur": psLstSéances}] ;
 		LG_Ajax_GetText("onSupprimeSéance", lsJSON, "", "JSON", callback);
	},
	
});

