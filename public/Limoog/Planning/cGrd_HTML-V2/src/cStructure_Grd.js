/* 
 * Classe cStructure pour l'édition du planning
 */

var cStructure = cDataBase.extend({
    cClassName: "cStructure",
	
	// Constantes calculées suivant les heures des tranches horaires
	iHeureMin: 10000,
	iHeureMax: 0,
	
	// Constructeur
	//poStructure	: objet issu du JSON de chargement du planing
	initProps:function(poStructure){
// window.oPlanning.oStructure.oConfig ;
// window.oPlanning.oStructure.oConfig.oAutorisations ;

		// Structure brute
		this.oConfig = poStructure.oConfig ;
		this.vertical = this.oConfig.vertical ;
		this.aTranchesBrutes = poStructure.aTranches ;
		this.aRessourcesBrutes = poStructure.aRessources ;
		this.aJoursBruts = poStructure.aJours ;
		
		// Configuration
		if (this.oConfig.iHeureMin) this.iHeureMin = this.oConfig.iHeureMin ;
		if (this.oConfig.iHeureMax) this.iHeureMax = this.oConfig.iHeureMax ;
		if (this.oConfig.dDateMin) this.dDateMin = CTOD(this.oConfig.dDateMin) ;
		if (this.oConfig.dDateMax) this.dDateMax = CTOD(this.oConfig.dDateMax) ;
		if (this.oConfig.pas) this.pas = this.oConfig.pas; else this.pas = 15;
		if (this.oConfig.dureeDftSeance) this.dureeDftSeance = this.oConfig.dureeDftSeance; else this.dureeDftSeance = 60;
		if (this.oConfig.tailleHorsLimite) this.tailleHorsLimite = this.oConfig.tailleHorsLimite; else this.tailleHorsLimite = 15;
		if (this.oConfig.lCouleursDegradees) this.lCouleursDegradees = this.oConfig.lCouleursDegradees; else this.lCouleursDegradees = false;
		
		// Dimension minimale d'une ressource
		if (this.oConfig.largeurRessourceMin) this.largeurRessourceMin = this.oConfig.largeurRessourceMin; 
			else this.largeurRessourceMin = window.oPlanning.oCst.largeurRessourceMin /*50*/ ;
		if (this.oConfig.hauteurRessourceMin) this.hauteurRessourceMin = this.oConfig.hauteurRessourceMin; 
			else this.hauteurRessourceMin = window.oPlanning.oCst.hauteurRessourceMin /*21*/ ;

		this.oConfig.iTaillePoliceSeancesDft = this.oConfig.iTaillePoliceSeancesDft?this.oConfig.iTaillePoliceSeancesDft:window.oPlanning.oCst.iTaillePoliceSeancesDft ;
		
		if (this.oConfig.lReadWrite) this.lReadWrite = this.oConfig.lReadWrite; else this.lReadWrite = false;
		
		// Ajouter les jours non fournis si nécessaire
		if (this.lGèreMois) this.calcMois() ;
		
		// En déduire la structure réelle
		this.aTranches = new Array();
		this.aTranchesJours = new Array();	// Tableau de objets JS tranches (une instance par jour)
		for (var i = 0; i < this.aTranchesBrutes.length ; i++) this.ajouteTrancheHoraire(this.aTranchesBrutes[i]) ;
		
		this.aJours = new Array();
		for (var i = 0; i < this.aJoursBruts.length ; i++) this.ajouteJour(this.aJoursBruts[i]) ;
		
		this.aRessources = new Array();
		for (var i = 0; i < this.aRessourcesBrutes.length ; i++) this.ajouteRessource(this.aRessourcesBrutes[i]) ;

		this.ajouteSéancesEtZonesInterdites() ;
		
		this.idSeanceSelect = '';
		this.aSeancesSelectionnee = new Array();
	},

	// Déterminer si l'objet fourni est en lecture seule ou en lecture écriture
	getReadWrite: function (pdDate, psRes) {
		if (!this.lReadWrite) return false ;
		if (!pdDate){
			//Date non fournie : readWrite par défaut
			return true ;
		} else {
			// Date fournie
			// Evaluer la date
			var loJour, lbTrouvé ;
			lbTrouvé = false ;
			for (var i = 0; i < this.aJoursBruts.length ; i++) {
				loJour = this.aJoursBruts[i] ;
				if (loJour.dDate.estMemeDate(pdDate)) {
					// On est sur la bonne date
					if (loJour.lReadOnly) return false ;
					else if (!psRes) return true ;
					
					lbTrouvé = true ;
					break ;
				}
			}
			if (!lbTrouvé) {
				// Date non retrouvée : lecture seule
				return false ;
			}
		}
		
		if (psRes){
			// Ressource fournie
			// Non implémenté
			console.log("getReadWrite appellée avec ressource: non pris en charge") ;
		}
		
		return false ;
		
	},

	// Tenir compte des jours pour déterminer les mois
	// Remplir this.aMois
	calcMois: function () {
		// S"assurer qu'on connait les dates min et max
		if (!(isDate(this.dDateMin) && isDate(this.dDateMax))) {
			// Au moins une date inconnue
			var ldDate ;
			for (var j = 0; j < this.aJoursBruts.length ; j++) {
				ldDate = this.aJoursBruts[j].dDate ;
				if (!this.dDateMin || ldDate.estInferieureA(this.dDateMin)) this.dDateMin = ldDate ;
				if (!this.dDateMax || this.dDateMax.estInferieureA(ldDate)) this.dDateMax = ldDate ;
			}
			this.dDateMin = new Date(this.dDateMin) ;
			this.dDateMax = new Date(this.dDateMax) ;
		}

		// Ajouter les jours manquants dans le tableau de jours
		this.insèreJoursManquants() ;
		
		// Créer le tableau des mois
		var liAnnée, liMois, loMois, lsLibelle ;
		liAnnée = parseInt(this.dDateMin.getFullYear()) ;
		liMois = parseInt(getMonth(this.dDateMin)) ;

		var ldDate = new Date(liAnnée, liMois, 1) ;
		loMois = {iAnnée: liAnnée, iMois: liMois} ;
		var loMoisDébut = loMois ;
		this.aMois = [loMois] ;
		
		while (ldDate.estInferieureA(this.dDateMax)) {
			liMois ++ ;
			if (liMois > 12) {
				liAnnée ++ ;
				liMois = 1 ;
			}
			ldDate = new Date(liAnnée, liMois, 1) ;
			loMois = {iAnnée: liAnnée, iMois: liMois} ;
			this.aMois.push(loMois) ;
		}
		var loMoisFin = loMois ;
		
		// Recalculer la date de début et de fin et rerajouter les jours manquants
		if (true) {
			this.dDateMin = new Date(loMoisDébut.iAnnée, loMoisDébut.iMois - 1, 1) ;
			this.dDateMax = new dateFinDeMois(new Date(loMoisFin.iAnnée, loMoisFin.iMois - 1, 28)) ;
		}
		this.insèreJoursManquants() ;
	},

	// Ajouter les jours manquants dans le tableau de jours
	// this.aJoursBruts doit inclure toutes les dates comprises entre this.dDateMin et this.dDateMax
	insèreJoursManquants: function () {
		// Ajouter les jours manquants
		var ldDate, loJour, lbFound, liNbJours, lbAdded ;
		liNbJours = this.aJoursBruts.length ;
		for (i = 0; i < dateDiff(this.dDateMin, this.dDateMax) + 1; i++) {
			ldDate = this.dDateMin.ajouteJours(i) ;
			lbFound = false ;
			for (var j = 0; j < liNbJours ; j++) {
				loJour = this.aJoursBruts[j] ;
//				if (loJour.dDate.getDate() === ldDate.getDate()) {
				if (loJour.dDate.estMemeDate(ldDate)) {
					// C'est la date cherchée
					lbFound = true ;
					break ;
				 }
			}
			if (!lbFound) {
				// Ce jour n'est pas encore dans la liste : l'ajouter
				lbAdded = true ;
				var loNouveauJour = {cLibelle: CDOW(ldDate)
							, sIdJour: "J" + DTOC(ldDate, 1)
							, dDate: ldDate
							, cCouleur: window.oPlanning.oCst.sCouleurDesJoursAjoutés
							, lReadOnly: true
						} ;
				this.aJoursBruts[this.aJoursBruts.length] = loNouveauJour ;
			}
		}
		
		if (lbAdded) {
			// Trier les jours
			this.aJoursBruts.sort(function(a, b){return (a.dDate.estInferieureA(b.dDate))?-1:1;}) ;
		}
		
	},

	// Ajoute les tranches horaires, communes à tout le planning        
	ajouteTrancheHoraire: function (poTranche) {
		var laTranche = {
			iDebut: poTranche.iDebut,
			iFin: poTranche.iFin,
			// l'Id de la tranche est son indice dans le tableau Document.Semaine.aTranches
			sIdTranche: this.aTranches.length
		};
		this.aTranches.push(laTranche);

		// Met à jour les limites de la semaine, en fonction des tranches
		for (var j = 0; j < this.aTranches.length; j++) {
			if (this.aTranches[j].iDebut < this.iHeureMin)
				this.iHeureMin = this.aTranches[j].iDebut;
			if (this.aTranches[j].iFin > this.iHeureMax)
				this.iHeureMax = this.aTranches[j].iFin;
		}
	},
	
	// Ajoute une journee, et, pour chaque tranche horaire de la semaine, va ajouter une tranche à la journée
	ajouteJour: function (aParaJournee){
		aParaJournee.vertical = this.vertical ;
		var laJournee = new cJour(aParaJournee);
		this.aJours.push(laJournee);

		if (!this.dDateMin) this.dDateMin = laJournee.dDate ;
		else if (this.dDateMin > laJournee.dDate) this.dDateMin = laJournee.dDate ;
		if (!this.dDateMax) this.dDateMax = laJournee.dDate ;
		else if (this.dDateMax < laJournee.dDate) this.dDateMax = laJournee.dDate ;

		var loTranche;
		for (var i = 0; i < this.aTranches.length; i++)
		{
			loTranche = laJournee.ajouteTranche(this.aTranches[i]);
			this.aTranchesJours.push(loTranche);
		}
		return laJournee;
	},
	
	/**
	 * ajouteRessource, Ajoute une ressource à la semaine Courante, pour CHAQUE journées
	 * Cette méthode crée une instance de la classe ressource, met l'instance dans le tableau aTranchesJours, et incrémente le compteur iNbtranche
	 * 
	 * @param {Array} aParaRes - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
	 * @see voir la classe ressource pour la documentation du tableau de paramètres
	 * @return {Objet ressource} maRessource - Renvoit l'instance de la journée créée
	 */
	ajouteRessource: function (aParaRes){
		var maRessource, tranche ;
		for (var i = 0; i < this.aTranchesJours.length; i++) {
			tranche = this.aTranchesJours[i] ;
			maRessource = tranche.ajouteRessource(aParaRes) ;
			this.aRessources.push(maRessource) ;
		}
		return maRessource;
	},
	
	ajouteSéancesEtZonesInterdites: function (){
		this.aSeances = new Array();
		for (var i = 0; i < window.oPlanning.oData.oActuel.aSeances.length ; i++) 
			this.ajouteSeance(window.oPlanning.oData.oActuel.aSeances[i], true) ;

		this.aZonesInterdites = new Array();
		for (var i = 0; i < window.oPlanning.oData.oActuel.aZonesInterdites.length ; i++) 
			this.ajouteZoneInterdite(window.oPlanning.oData.oActuel.aZonesInterdites[i]) ;
	},

	 /**
	 * ajouteSeance, Ajoute une seance dans une ressource
	 * Cette méthode crée une instance de la classe seance, met l'instance dans le tableau aSeances
	 * 
	 * @param {Array} aParaSeance - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
//	 * @param {Boolaeb} pbSansEffaceAvant - true pour indiquer qu'il est inutile d'effacer une éventuelle représentation précédente de la même séance
	 * @see voir la classe seance pour la documentation du tableau de paramètres
	 * @return {Objet ressource} maRessource - Renvoit l'instance de la seance créée
	 */
   ajouteSeance: function (aParaSeance, pbSansEffaceAvant) {
//		if (!pbSansEffaceAvant) this.effaceDivSeance(aParaSeance.sIdSeance);
		
		// Ajouter la séance à chacun des objets Ressources impléqués dans cette plage horaire
		var laRessources = window.oPlanning.oStructure.getCollRessourcesPlage(aParaSeance.sIdJour			// psIdJour
																	, aParaSeance.sIdRessource	// psIdRessource
																	, aParaSeance.iDebut		// piDébut
																	, aParaSeance.iFin			// piFin
																	) ;
		var loRessource, maSeance ;
		aParaSeance.vertical = this.vertical ;
		for (var i = 0; i < laRessources.length; i++) {
			loRessource = laRessources[i] ;
			maSeance = loRessource.ajouteSeance(aParaSeance) ;
		}

		return maSeance;
	},
	/**
	 * AjouteZoneInterdite, Ajoute une zone interdite dans une ressource
	 * Cette méthode crée une instance de la classe seance, met l'instance dans le tableau aSeances
	 * 
	 * @param {Array} aParaSeance - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
	 * @see voir la classe seance pour la documentation du tableau de paramètres
	 * @return {Objet ressource} maRessource - Renvoit l'instance de la seance créée
	 */
	ajouteZoneInterdite: function (aParaZoneInterdite) {
		aParaZoneInterdite.vertical = this.vertical ;
		var laRessources = window.oPlanning.oStructure.getCollRessourcesPlage(aParaZoneInterdite.sIdJour	// psIdJour
														, aParaZoneInterdite.sIdRessource	// psIdRessource
														, aParaZoneInterdite.iDebut	// piDébut
														, aParaZoneInterdite.iFin	// piFin
														) ;
		var loRessource, laZone ;
		for (var i = 0; i < laRessources.length; i++) {
			loRessource = laRessources[i] ;
			laZone = loRessource.ajouteZoneInterdite(aParaZoneInterdite) ;
		}

		return laZone ;

	},

	// ------------------------------------------------------------------------------------
	// Enlever une séance de l'affichage
	effaceSeance: function (poSeance) {
		if (poSeance.sIdSeance) {
			// On efface une case de séance : uniquement la participation
			window.oPlanning.oStructure.effaceSeance_Participation(poSeance.sIdSeance) ;
		}
		if (poSeance.iId_Seance) {
			// On efface toute la séance
			window.oPlanning.oStructure.effaceSeance_TouteLaSéance(poSeance.iId_Seance) ;
		}
	},

	// ------------------------------------------------------------------------------------
	// Enlever une séance de l'affichage : uniqument la séance correspondant à la participation
	effaceSeance_Participation: function (psIdSeance) {
		for (var i = this.aSeances.length - 1; i >= 0; i--){
			var loSeance = this.aSeances[i] ;
			if (loSeance.sIdSeance == psIdSeance) {
				// Cette séance est la bonne
				
				// L'effacer de l'affichage
				//var loDiv = loSeance.getDiv() ;
				//loDiv.parentNode.removeChild(loDiv) ;
				this.effaceDivSeance(loSeance.getIdDiv()) ;
				
				// L'effacer de la liste des séances
				arrayRemove(this.aSeances, i) ;
			}
		}
		
		return true ;
	},

	// ------------------------------------------------------------------------------------
	// Enlever une séance de l'affichage : toutes les cses correspondant à une séance (toutes participations de toutes les ressources)
	effaceSeance_TouteLaSéance: function (piId_Seance) {
		for (var i = this.aSeances.length - 1; i >= 0; i--){
			var loSeance = this.aSeances[i] ;
			if (loSeance.aParaSeance.iId_Seance == piId_Seance) {
				// Cette séance est la bonne
				
				// L'effacer de l'affichage
				//var loDiv = loSeance.getDiv() ;
				//loDiv.parentNode.removeChild(loDiv) ;
				this.effaceDivSeance(loSeance.getIdDiv()) ;
				
				// L'effacer de la liste des séances
				arrayRemove(this.aSeances, i) ;
			}
		}
		
		return true ;
	},

	// ------------------------------------------------------------------------------------
	// Ajouter une séance à l'affichage
	insereSeance: function (poSeance) {
obsolete('LG 20170120 : obsolète (mais fonctionnel au 20190520)') ;
		// Commencer par effacer si elle était déja présente
		this.effaceSeance(poSeance) ;
		// Puis ajouter
		this.ajouteSeance(poSeance) ;
		window.oPlanning.oGrille.afficherSeances(poSeance.sIdSeance) ;
	},

	/**
	 * effaceDivSeance, Supprime une seance dans une ressource
	 * Cette méthode supprime l'élement HTML, et enlève les traces de l'instance de la seance dans la semaine
	 * Elle ne supprime pas techniquement l'instance de la séance. 
	 * 
	 * @param {String} sIdSeance - Identifiant de la seance à supprimer
	 */
	effaceDivSeance: function (psIdDivSeance) {
		// Si la séance qu'on veut supprimer était selectionné (ex : seance vide) on vide idSeanceSelect.
		if (psIdDivSeance == this.idSeanceSelect) {
			this.idSeanceSelect = '';
		}
		var loDivSeance = document.getElementById(psIdDivSeance) ;
		if (loDivSeance)
			loDivSeance.parentNode.removeChild(loDivSeance) ;
		else
			popupWindow("non trouvé") ;

	},

	// Effacer toute la sélection de séances
	unSelect: function () {
		return this.select(null		//psIdDivSeance
							, false	//bInformeVFP
							, null	//event1
							, true	//nullSansSeanceVide
							) ;
	},

	//NB : 20150317
	// * @param {Event} event - Evenement d'ou provient l'appel -> onclick
	// * @param {psIdDivSeance} id seance a selectionner
	// * @param {b informer vfp : informer l'application principale
	// * @param {nullSansSeanceVide permet d'executé select avec psIdDivSeance comme valeur null mais ne pas créer de seance vide quand meme
	select: function (psIdDivSeance, bInformeVFP, event1, nullSansSeanceVide) {

		if (window.oPlanning.oStructure.iFinDéplacementPoignée + 50 >= now()) {
			// On vient à peine de terminer un déplacement par poignée
			return ;
		}
		if (!window.oPlanning.oStructure.getReadWrite()) {
			// Lecture seule
			return ;
		}

		var bFlag = false;
		if (psIdDivSeance != null) {
			psIdDivSeance = psIdDivSeance.replace("_Selection", "");
		}
		if (bInformeVFP) {
			console.log("select annulé car le programme attend une réponse de vfp");
		}
		// Si la séance selectionnée est déjà la bonne, on ne fait rien
		if (psIdDivSeance == this.idSeanceSelect)
			return;
		if (this.idSeanceSelect == 'S0' && psIdDivSeance != this.idSeanceSelect) {
			this.effaceDivSeance('S0');
		}

		// On parcourt le tableau des seances selectionnées
		for (var i = 0; i < this.aSeancesSelectionnee.length; i++) {
			if (i <= this.aSeancesSelectionnee.length) {
//				if (this.aSeancesSelectionnee[i].getIdDiv() == psIdDivSeance) {
//					//MG Modification 20203006 Début
//					//Erreur avec Mozilla lors du déplacement d'une séance
////					if (event1.ctrlKey) {
//////						this.aSeancesSelectionnee[i].oSéanceSource.unSelectThis();
////						this.aSeancesSelectionnee[i].effacer();
////						this.aSeancesSelectionnee.splice(i, 1);
////						i--;
////						bFlag = true;
////					} else {
////						bFlag = true;
////					}
////				} else {
////					if (!(event1.ctrlKey)) {
//////						this.aSeancesSelectionnee[i].oSéanceSource.unSelectThis();
////						this.aSeancesSelectionnee[i].effacer();
////						this.aSeancesSelectionnee.splice(i, 1);
////						i--;
////						//bFlag = false;
////					} else
////						bFlag = bFlag;
////				}
//					if(event1){
//						if (event1.ctrlKey) {
//							this.aSeancesSelectionnee[i].effacer();
//							this.aSeancesSelectionnee.splice(i, 1);
//							i--;
//							bFlag = true;
//						} else {
//							bFlag = true;
//						}
//					} else {
//						if (!(event1.ctrlKey)) {
//							this.aSeancesSelectionnee[i].effacer();
//							this.aSeancesSelectionnee.splice(i, 1);
//							i--;
//						} else
//							bFlag = bFlag;
//					}
//				}
//				
//				//MG Modification 20203006 Fin
                var lbCtrlKey = false ;
                if(event1){
                    lbCtrlKey = event1.ctrlKey ;
                }
                if (this.aSeancesSelectionnee[i].getIdDiv() == psIdDivSeance) {
// LG 20200814 début
////					MG Modification 20203006 Début
////					Erreur avec Mozilla lors du déplacement d'une séance
//					if (event1.ctrlKey) {
////						this.aSeancesSelectionnee[i].oSéanceSource.unSelectThis();
//						this.aSeancesSelectionnee[i].effacer();
//						this.aSeancesSelectionnee.splice(i, 1);
//						i--;
//						bFlag = true;
//					} else {
//						bFlag = true;
//					}
//				} else {
//					if (!(event1.ctrlKey)) {
////						this.aSeancesSelectionnee[i].oSéanceSource.unSelectThis();
//						this.aSeancesSelectionnee[i].effacer();
//						this.aSeancesSelectionnee.splice(i, 1);
//						i--;
//						//bFlag = false;
//					} else
//						bFlag = bFlag;
//				}
                    if (lbCtrlKey) {
                        this.aSeancesSelectionnee[i].effacer();
                        this.aSeancesSelectionnee.splice(i, 1);
                        i--;
                        bFlag = true;
                    } else {
                        bFlag = true;
                    }
                } else {
                    if (!(lbCtrlKey)) {
                        this.aSeancesSelectionnee[i].effacer();
                        this.aSeancesSelectionnee.splice(i, 1);
                        i--;
                    } else
                        bFlag = bFlag;
                }
			}
// LG 20200814 fin
		}

		var loMasque ;
		if (nullSansSeanceVide) {
			// RAS
		} else {
			// NB : 20150318 creation d'une seance vide
			if (psIdDivSeance == null) {
				var loSeance = this.creerSeanceVide(event1);
				if (loSeance) {
					// La création de la séance vide a réussi
					loMasque = loSeance.selectThis(bInformeVFP);
					this.aSeancesSelectionnee[this.aSeancesSelectionnee.length] = loMasque;
				}
			}
		}

		// on parcourt le tableau des seances pour determinée celle à selctionner
		if (!bFlag && (psIdDivSeance)) {
			// On doit sélectionner la séance demandée
// var lbTrouvée = false ;
			for (var i = 0; i < this.aSeances.length; i++) {
				if (this.aSeances[i].getIdDiv() == psIdDivSeance) {
					loMasque = this.aSeances[i].selectThis(bInformeVFP);
					this.aSeancesSelectionnee[this.aSeancesSelectionnee.length] = loMasque;
// lbTrouvée = true ;
				}
			}
// if (!lbTrouvée) 
// 	alert("non trouvée") ;
		}
		
// 		permissionForm.init();
	},

	getLstSeancesSelectionnees: function () {
		var lsLstSéances = "" ;
		for (i = 0; i < this.aSeancesSelectionnee.length ; i++){
// LG 20190607 old : on prend l'Id de case de grille + l'Id de séance + Id de ressource et pas seulement l'Id de case de grille			lsLstSéances += this.aSeancesSelectionnee[i].sIdSeance + "," ;
			lsLstSéances += this.aSeancesSelectionnee[i].aParaSeance.sIdSeance + ":" + this.aSeancesSelectionnee[i].aParaSeance.iId_Seance + ":" + this.aSeancesSelectionnee[i].aParaSeance.sIdRessource + "," ;
		}
// alert(lsLstSéances) ;
		return lsLstSéances ;
	},

	selectAvecObjet: function (poParaSeance, bInformeVFP) {
		// Déterminer l'Id de div correspondant à l'objet fourni
		var lsIdDiv ;
		for (var i = 0; i < this.aSeances.length; i++) {
			if (this.aSeances[i].sIdSeance !== poParaSeance.sIdSeance) {
				// Pas la bonne séance
			} else if (poParaSeance.sIdRessource && !(this.aSeances[i].sIdRessource == poParaSeance.sIdRessource)) {
				// On a spécifié une ressource, mais ce n'est pas la bonne
			} else {
				lsIdDiv = this.aSeances[i].getIdDiv() ;
// popupWindow(this.cClassName + ".selectAvecObjet " + lsIdDiv ) ;
				break ;
			}
		}
		if (lsIdDiv)
			return this.select(lsIdDiv, bInformeVFP, null, true) ;
	},

	/**
	 * creerSeanceVide, Ajoute une seance vide à l'endroit ou l'on a cliqué
	 * Cette méthode calcule les paramètres à passer, puis ajoute la seance
	 * 
	 * @param {Event} event - Evenement d'ou provient l'appel -> onclick
	 * @see voir la méthode ajouteSeance pour la documentation du tableau de paramètres calculé ici
	 */
	// NB : 20150318 appelé par select pour la creation d'une seance vide, on instancie une seance on lui 
	// ajoute les different element necessaire et on finit par l'ajouter au ressource 
	creerSeanceVide: function (event) {

		if (!event) {console.log("event not set") ; return false ;}
		if (isPrototype()) var element = event.element() ;
		else var element = event.target ;
		if (!element) {console.log("element not set") ; return false ;}
		if (!(element.className == "ressource")) {console.log("element n'est pas une ressource") ; return false ;}
		var loDivRessource = element ;
		var loRessource = window.oPlanning.oStructure.getRessourceAvecDiv(loDivRessource) ;

		return loRessource.creerSeanceVide(event) ;

	},
	
	// Renvoyer la collection des objets trancheJour qui correspondent à la plage fournie
	//(psIdJour)		: Id du jour (Dft : tous)
	// piDébut			: début de plage horaire (minutes depuis 0h)
	//(piFin)			: fin de plage horaire (minutes depuis 0h) (Dft : piDébut)
	getCollTrancheJours: function (psIdJour, piDébut, piFin) {
		piFin = (piFin)?piFin:piDébut ;
		var loTrancheJour ;
		var laTranchesJours = new Array() ;
		for (var i = 0; i < this.aTranchesJours.length; i++){
			loTrancheJour = this.aTranchesJours[i];
			if ((!psIdJour || psIdJour == loTrancheJour.sIdJour)	// Jour non fourni, ou le bon jour
					&& piDébut < loTrancheJour.iFin && piFin > loTrancheJour.iDebut				// Bonne tranche horaire
					) {
				// La plage demandée appartient à cette tranche de cette journée
				// On a trouvé le bon objet ressource, de la bonne tranche et de la bonne journée
				laTranchesJours.push(loTrancheJour) ;
			}
		}
		return laTranchesJours ;
	},
	
	// Renvoyer la collection des objets ressources qui correspondent à la plage fournie
	//(psIdJour)		: Id du jour (Dft : tous)
	//(psIdRessource)	: Id de ressource (Dft : tous)
	// piDébut			: début de plage horaire (minutes depuis 0h)
	// piFin			: fin de plage horaire (minutes depuis 0h) (Dft : piDébut)
	getCollRessourcesPlage: function (psIdJour, psIdRessource, piDébut, piFin) {
		piFin = (piFin)?piFin:piDébut ;
		var laRessources = new Array() ;
		var loTrancheJour ;
		for (var i = 0; i < this.aTranchesJours.length; i++){
			loTrancheJour = this.aTranchesJours[i];
// LG 20160826 old                    if ((!psIdJour || psIdJour == loTrancheJour.sIdDivJour)	// Jour non fourni, ou le bon jour
			if ((!psIdJour || psIdJour == loTrancheJour.sIdJour)	// Jour non fourni, ou le bon jour
					&& piDébut < loTrancheJour.iFin && piFin > loTrancheJour.iDebut				// Bonne tranche horaire
					) {
				// La plage demandée appartient à cette tranche de cette journée
				// Passer tous les objets ressources pour trouver le bon
				for (var j = 0; j < this.aRessources.length; j++) {
					loRessource = this.aRessources[j];
					if ((!psIdRessource || loRessource.sIdRessource == psIdRessource)	// Pas de ressource fournie ou bonne ressource
// LG 20160826 old                                    && loRessource.sIdDivTranche == loTrancheJour.sIdDivTranche	// Bonne tranche
							&& loRessource.sIdJour == loTrancheJour.sIdJour	// Bonne journée
							&& loRessource.sIdTranche == loTrancheJour.sIdTranche	// Bonne tranche
							) {
						// On a trouvé le bon objet ressource, de la bonne tranche et de la bonne journée
						laRessources.push(loRessource) ;
					}
				}
			}
		}
		return laRessources ;
	},			
	
	getNbTranches: function (){
		return this.aTranches.length ;
	},
	
	getDuréeTotaleTranches: function () {
		var liNb = 0 ;
		for (var j = 0; j < this.aTranches.length; j++) {
			liNb = liNb + (this.aTranches[j].iFin - this.aTranches[j].iDebut)
		}
		return liNb ;
	},
	
	getTailleTotaleTranches: function () {
		var liNb = 0 ;
		for (var i = 0; i < this.aTranches.length; i++) {
			liNb = parseInt(liNb) + parseInt(this.aTranches[i].iTaille);
		}
		return liNb ;
	},
	
	getNbRessources: function (){
		if (this.aRessources.length == 0 || this.aTranchesJours.length == 0) {
			console.log("Aucune ressource ou aucune tranche") ;
			return 1 ;
		} else {
			return this.aRessources.length / this.aTranchesJours.length ;
		}
	},
	
	getNbTranches: function (){
		return this.aTranches.length ;
	},
	
	/**
	 *getDureeHorsLimite  
	 *retourne le temps, en minutes, qui est hors des tranches horraires. 
	 * Exemple : de 10 à 14 et de 18 à 22 --> retourne 120
	 *@return {Number} tempsHorsLimite - Le temps hors limite en minutes
	 */
	getDureeHorsLimite: function () {
		var tempsHorsLimite = 0;
		if (this.aTranches.length > 1) {
			for (var i = 1; i < this.aTranches.length; i++) {
				var j = i - 1;
				if (this.aTranches[i].iDebut > this.aTranches[j].iFin) {
					tempsHorsLimite = tempsHorsLimite + (this.aTranches[i].iDebut - this.aTranches[j].iFin);
				}
			}
		}
		return tempsHorsLimite;
	},
	
	calcTailleTranches: function (dimTotale){
		var tailleTranche, ratioTailleTranche;
		var bordure = 1 ;
		var dimUtile = dimTotale - ((window.oPlanning.oStructure.getNbTranches() - 1) 
									* (window.oPlanning.oStructure.tailleHorsLimite + bordure));
		
		var infoHL = this.getDureeHorsLimite();

		// Pour chaque tranche on calcule : 
		for (var j = 0; j < this.aTranches.length; j++) {
			// Nombre d'heure enlevée non-affichées
			ratioTailleTranche = (this.aTranches[j].iFin - this.aTranches[j].iDebut) / ((this.iHeureMax - this.iHeureMin) - infoHL);
			tailleTranche = parseInt(dimUtile * ratioTailleTranche) ;
			// On indique à la tranche correspondante, de chaque journée, sa taille
			for (var i = 0; i < this.aTranchesJours.length; i++)
			{
				if (this.aTranchesJours[i].sIdTranche == this.aTranches[j].sIdTranche) {
					this.aTranchesJours[i].iTaille = tailleTranche;
				}
			}
			this.aTranches[j].iTaille = tailleTranche ;
		}
		return ;
	},

	// Récupérer un objet ressource en ayant la div qui la représente, ou son Id
	getRessourceAvecDiv: function (psIdDivRessource){
		if (!psIdDivRessource) return null ;
		if (psIdDivRessource.id) {
			// On a fourni une div : récupérer son Id
			psIdDivRessource = psIdDivRessource.id ;
		}
		var loRessource = null;
		for (var i = 0; i < this.aRessources.length; i++) {
			loRessource = this.aRessources[i] ;
			if (loRessource.getIdDiv() == psIdDivRessource) {
				return loRessource;
			}
		}
		// si on arrive là, on n'a pas trouvé la ressource
		console.log("structure.getRessourceAvecDiv : ressource " + psIdDivRessource + " non trouvée", "red");
		return loRessource;
	},
	
	// ------------------------------------------------------------------------------------
	// Renvoyer l'objet tranche dont on passe l'Id
	// psIdDivTranche : Id de tranche ou div tranche
	getTrancheAvecDiv: function (psIdDivTranche) {
		if (psIdDivTranche.id) {
			// On a fourni une div : récupérer son Id
			psIdDivTranche = psIdDivTranche.id ;
		}
		var loTranche = null;
		for (var i = 0; i < window.oPlanning.oStructure.aTranchesJours.length; i++) {
			if (window.oPlanning.oStructure.aTranchesJours[i].getIdDiv() == psIdDivTranche) {
				loTranche = window.oPlanning.oStructure.aTranchesJours[i];
				return loTranche;
			}
		}
		// si on arrive là, on n'a pas trouvé la tranche
		console.log("Tranche " + psIdDivTranche + " non trouvée", "red");
		return loTranche;
	},
	
	getIdDivMois: function (piAnnée, piMois) {
		return "M" + piAnnée + "_" + piMois ;
	},
	getIdDivJour: function (psIdJour){
		return psIdJour ;
	},
	getIdDivTranche: function (piTranche, psIdJour){
		return "tranche" + piTranche + "-" + this.getIdDivJour(psIdJour) ;
	},
	getIdDivRessource: function (psIdRessource, psIdJour, psIdTranche){
		return psIdRessource + "-" + this.getIdDivTranche(psIdTranche, psIdJour) ;
	},
	getIdDivZoneInterdite: function (psIdDivTranche, psIdRessource, psIdZoneInterdite) {
		return "ZoneInterdite_" + psIdDivTranche + "_" + psIdRessource + "_" + psIdZoneInterdite ;
	},
	getIdDivSeance: function (psIdSeance, psIdRessource, psIdJour, psIdTranche) {
		return this.getIdDivRessource(psIdRessource, psIdJour, psIdTranche) + "-" + psIdSeance
	},
	// Récupérer l'Id d'une ressource en ayant la div qui la représente
	getIdRessourceAvecDiv: function (poDivRessource){
		if (poDivRessource.id) var lsIdDiv = poDivRessource.id ;	// On a fourni une div
		else lsIdDiv = poDivRessource ;								// On a fourni l'id de la div
		lsIdDiv = lsIdDiv.split("-") ;
		return lsIdDiv[0] ;
	},

	// ------------------------------------------------------------------------------------
	// Renvoyer l'objet séance dont on passe l'Id
	getSeanceAvecDiv: function (psIdDivSéance) {
		if (psIdDivSéance.id) psIdDivSéance = psIdDivSéance.id ;	// On a fourni une div
		
		var loSeance ;
		if (psIdDivSéance.match("S0_")) {
			// Séance vierge
			for (var i = 0; i < this.aSeancesSelectionnee.length; i++) {
				loSeance = this.aSeancesSelectionnee[i]
				if (loSeance.getIdDiv() == psIdDivSéance) {
					return loSeance ;
				}
			}
			// si on arrive là, on n'a pas trouvé la séance
			console.log("Séance vierge" + psIdDivSéance + " non trouvée", "red");
			return null;
		}
		
		if (psIdDivSéance.match("_Selection")) psIdDivSéance = psIdDivSéance.substr(0, psIdDivSéance.length - "_Selection".length) ;
		for (var i = 0; i < this.aSeances.length; i++) {
			loSeance = this.aSeances[i];
			if (loSeance.getIdDiv() == psIdDivSéance) {
				return loSeance;
			}
		}
		// si on arrive là, on n'a pas trouvé la séance
		console.log("Séance " + psIdDivSéance + " non trouvée", "red");
		return null;
	},

	// ------------------------------------------------------------------------------------
	// Renvoyer l'objet séance sélectoinnée dont on passe l'Id
	getSeanceSelectionneeAvecDiv: function (psIdDivSéance) {
		for (var i = 0; i < this.aSeancesSelectionnee.length; i++) {
			loSeance = this.aSeancesSelectionnee[i];
			if (loSeance.getIdDiv() == psIdDivSéance) {
				return loSeance;
			}
		}
		// si on arrive là, on n'a pas trouvé la séance
		console.log("Séance " + psIdDivSéance + " non trouvée", "red");
		return loSeance;
	},

	// AC 20130109 Début
	rtvCollZonesInterdites: function(psIdJour, psIdTranche, psIdRessource) {
		var laZones = new Array() ;
		for (var i = 0; i < this.aZonesInterdites.length; i++) {
			if (this.aZonesInterdites[i].sIdJour == psIdJour 
					&& this.aZonesInterdites[i].sIdRessource == psIdRessource 
					&& this.aZonesInterdites[i].sIdTranche == psIdTranche){
				loZone = this.aZonesInterdites[i];
				laZones.push(loZone) ;
			}
		}
		return laZones;
	},

	getDimEnTeteJour: function() {
		var liDim ;
		if (this.vertical)
			liDim = window.oPlanning.oCst.HauteurBouton 
								+ window.oPlanning.oCst.HauteurBouton + 3 ;
		else
			liDim = window.oPlanning.oCst.LargeurBouton 
								+ window.oPlanning.oStructure.LargeurBoutonRessource + 3 ;
		liDim += window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource * 2 /*2*/ ;
		return liDim ;
	},

	calcPasLegende: function (){
		var liDimUtile = window.oPlanning.oStructure.getTailleTotaleTranches() ;
		var liNbPas = window.oPlanning.oCst.nbPasLegende ;
		var liDimParPas = liDimUtile / liNbPas ;
		var liDimMin = this.vertical?50:70 ;
		if (liDimParPas < liDimMin) {
			// Le nb de pas est trop grand : ca aboutirait à des intervalles trop rapprochés
			liNbPas = liDimUtile / liDimMin ;
		}
		
		var NombreTotalHeures = window.oPlanning.oStructure.getDuréeTotaleTranches() ;
		
		var laPasAutorisés = new Array(1, 5, 10, 15, 30, 60, 120, 180, 240, 300, 360, 420, 480, 540, 600); // valeures possibles
		var liPasTmp = NombreTotalHeures / liNbPas ;
		var liPas ;
		// On parcout les valeures possibles, pour se situer à l'interieur
		for (j = 0; j < laPasAutorisés.length; j++) {
			if (liPasTmp >= laPasAutorisés[j] && liPasTmp < laPasAutorisés[j + 1]) {
				liPas = laPasAutorisés[j];
				break ; 
			}
		}
		// AFFICHAGE Heure + trait 
		this.pasLegende = parseInt(liPas) ;

		},

});
