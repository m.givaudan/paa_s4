/* 
 * Classe de controleur spécifique pour l'édition du planning
 */
 
// Chargement du planning à vide
function initPlanning() {
	if (!window.oPlanning) window.oPlanning = {} ;
	window.oPlanning.oControleur = new cControleur_Grd();
	window.oPlanning.oControleur.masqueGrille();
}

// Chargement du planning avec des données
//(pvConfig)	: true pour "sans rafraichissement"
//				: objet config pour rechargement 
function chargePlanning(pvConfig) {
	if (!window.oPlanning) window.oPlanning = {} ;
	window.oPlanning.oControleur = new cControleur_Grd();
	window.oPlanning.oControleur.demandeChargement(pvConfig);
	return true ;
}

// Surcharge des fonctions définies dans cControleurBase.js
window.getPlanningoCst = function() {return window.oPlanning.oCst ;};
window.getPlanningoData = function() {return window.oPlanning.oData ;};
window.getPlanningoVue = function(){return window.oPlanning.oVue ;};
window.getPlanningoControleur = function(){return window.oPlanning.oControleur ;};

// Surcharge de la classe définie dans cControleurBase.js
var cControleur_Grd = cControleurBase.extend({
    cClassName: "cControleur",

	init: function () {
		this.créeSousObjetsSiNécessaire() ;
	},
	// ---------------------------------------------------------------------------------------
    // Lancer le chargement du planning
	// Après avoir récupéré les informations à tracer
	//(pvConfig)	: true pour "sans rafraichissement"
	//				: objet config pour rechargement 
	demandeChargement: function (pvConfig) {
		
		// A reprendre de beforeChargeGrille ci-dessous
		beforeChargeGrille() ;
		
		this.créeSousObjetsSiNécessaire() ;
		
		var lbSansRafraichissement = (pvConfig === true) ;
		if (lbSansRafraichissement && window.oPlanning.jsonChargé) {
			// Le planning a déja été chargé, on demande un simple rafraichissement
			// Réafficher les données déja chargées
			this.charge(window.oPlanning.jsonChargé) ;
			
		} else if (lbSansRafraichissement) {
			// Le planning n'a pas encore été chargé, on demande un simple rafraichissement
			// Masquer la grille
			this.masqueGrille() ;
			
		} else if (pvConfig) {
			// On fournit un objet de configuration
			// L'utiliser pour le chargement
            var lsURL = getBaseURL() ;
			lsURL = lsURL + "getPlanning" ;

			$.ajax({
				type : 'GET',
				// NB : la gestion de l'autorisation Cross-Origin est faite dans planning.php
				url : lsURL,
				dataType : 'text',
				data: {
					cLstRes: pvConfig.cLstRes
					, dDebut: pvConfig.dDebut
					, dFin: pvConfig.dFin
					, cStyleAffichage: pvConfig.cStyleAffichage
					, cLstJoursAffichage: pvConfig.cLstJoursAffichage
					, cTypeAffichageJours: pvConfig.cTypeAffichageJours
					, cOrientation: pvConfig.cOrientation
				},
				success : function(data){
					// Remplir le planning
//					if (window.oPlanning.oControleur.getGestionnaireActions().gereReponseAjax_DemandeLogin(data)) return false ;
					if (window.oPlanning.oControleur.gereReponseAjax(data)) return false ;
					else window.oPlanning.oControleur.onJSON(data) ;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					// Display error
// LG 20190517 old					console.log("Erreur AJAX : " + textStatus) ;
//					window.oPlanning.oControleur.getGestionnaireActions().gereReponseAjax_DemandeLogin(XMLHttpRequest.responseText) ;
					window.oPlanning.oControleur.gereReponseAjax(XMLHttpRequest.responseText, null, true) ;
					return false;
				}

			});

			
		} else {
			// Demander les données (asynchrone ou non)
			var lsJSON = LG_Ajax_GetText("planningJSON", null, "", "JSON", this.onJSON);
		}
		
    },

	// Actions standard de gestion d'une réponse Ajax dans les cas de :
	//		* erreur
	//		* demande de login
	//		* demande de dialogue
	// Renvoie true si la réponse a été gérée
	gereReponseAjax: function(psResponse, callback, pbEstErreur) {
		// Gérer le cas où la réponse est une demande de login
		if (window.oPlanning.oControleur.gereReponseAjax_DemandeLogin(psResponse, callback))  {
			// C'était une demande de login : terminer
			return true ;
		}

		// Y a-t-il eu erreur ?
		if (pbEstErreur) {
			var lsMsgErr = psResponse ;
		} else if (psResponse.match("Erreur : ")) {
			// Erreur sous forme texte
			var lsMsgErr = psResponse.substr("Erreur : ".length, psResponse.length - "Erreur : ".length) ;
			lsMsgErr = lsMsgErr?lsMsgErr:"Erreur inconnue" ;
		} else if (psResponse.match('{"Erreur": ')) {
			// Erreur sous forme JSON
			var loResponse = JSON.parse(psResponse);
			var lsMsgErr = loResponse.Erreur ;
			lsMsgErr = lsMsgErr?lsMsgErr:"Erreur inconnue" ;
		}
		if (typeof lsMsgErr !== 'undefined') {
			var lsHTMLDialogue = "<div id = 'refreshAfterActionDialog' title='Une erreur est survenue'><p>" + lsMsgErr + "</p></div>" ;
			var dialog = $(lsHTMLDialogue).dialog({
// MG Modif 20200915 Début
// Le background du modal est disabled
				modal:true,
// MG Modif 20200915 Fin
				width: 1000, 
				close: function (e, o) {
					if (callback) callback() ; dialog.dialog('destroy');}}) ;
			return true ;
		} 
		
		if (psResponse.match('{"Dialogue": ')) {
			// Il y a une demande de dialogue
			var loResponse = JSON.parse(psResponse);
			window.oPlanning.oControleur.refreshAfterAction_Response = loResponse ; 
			var loResponseDialogue = loResponse.Dialogue;
			var lsHTMLDialogue = "<div id = 'refreshAfterActionDialog' title='" + loResponseDialogue.Titre + "''><p>" + loResponseDialogue.Message + "</p></div>"
			var loBoutons = {} ;
			var lsBoutonFocused ;
			loResponseDialogue.Boutons.forEach(function(loBouton) {
				// Fonction appellée par le clic sur le bouton
// LG 20190920 début
//				loBoutons[loBouton.Texte] = eval("window.oPlanning.oControleur." + loBouton.Action) ;
				loBoutons[loBouton.text] = loBouton ;
				loBouton.click = eval("window.oPlanning.oControleur." + loBouton.Action) ;
				// Focus sur le bouton
				if (loBouton.focused) {
					if (lsBoutonFocused) {
						console.log("Un bouton a déja le focus") ;
					}
					lsBoutonFocused = loBouton.id ;
				}
// LG 20190920 fin
			});
			var loDialogOptions = {
					autoOpen: false,
					height: "auto",
					width: "auto",
					modal: true,
					buttons: loBoutons,
					//MG Modification 20200630
//					close: function (e, o) {
					close: function (event, o) {
						if ($(event.currentTarget).hasClass("ui-dialog-titlebar-close")
								|| (event.keyCode && event.keyCode == $.ui.keyCode.ESCAPE) ) {
							// triggered by clicking on dialog box X or pressing ESC
							// not triggered if a dialog button was clicked
							// Lancer l'action du bouton "Annuler"
							if (loBoutons["Annuler"]) loBoutons["Annuler"].click();
						}        
						// Vider tout pour restaurer les options qui ont pu être modifiées à l'utilisation	
						dialog.dialog('destroy');
					},
// LG 20190920 début
					open: function () {
						// Donner le focus au bouton souhaité	
						if (lsBoutonFocused) {
							// On a donné le focus à un bouton : lui donner le focus maintenant
							$(this).first().parent().find('#' + lsBoutonFocused).first().focus();
						}
						
					},
// LG 20190920 fin
			};
			var dialog = $(lsHTMLDialogue).dialog(loDialogOptions).dialog("open") ;
			return true ;
		}
	},

	// ---------------------------------------------------------------------------------------
	// Gérer le cas où une réponse est une erreur
	// Si c'est le cas, affiche le message dans un popup
	gereReponseAjax_Erreur: function(psResponse, callback){
		return this.gereReponseAjax(psResponse, callback, true) ;			
    },
	// ---------------------------------------------------------------------------------------
	// Détermine si la réponse est une demande de login
	estReponseAjax_DemandeLogin: function(psResponse){
//MG Modification 20200928 Début
// Mise en commun de la fonction pour la réutiliser à différents endroits
//		return psResponse.includes("<!--PAALoginForm-->") ;
		return estHtml_DemandeLogin(psResponse);
//MG Modification 20200928 Fin
    },

	// ---------------------------------------------------------------------------------------
	// Gérer le cas où une réponse est une demande de login
	// Si c'est le cas, affiche le popup de login et renvoie true
	// Problème : quand c'est le cas, PlanningController.php redirige sur l'URL de la page de planning
	//			, sans sélection des ressources -> la fenêtre réapparait vierge
	//			Il faudrait passer les informations de chargement du planning (basiquement ce qui est dans le data envoyé à l'URL de chargement du planning par window.oControleur_Grd.demandeChargement
	gereReponseAjax_DemandeLogin: function(psResponse, callback){
		
//MG Modification 20200929 Fin
//		if (this.estReponseAjax_DemandeLogin(psResponse)) {
//			var lsHTMLDialogue = "<div id = 'gereReponseAjax_DemandeLoginDialog' title='Merci de vous authentifier'><p>" + psResponse + "</p></div>" ;
//			var dialog = $(lsHTMLDialogue).dialog({
//// MG Modif 20200915 Début
//// Le background du modal est disabled
//				modal:true,
//// MG Modif 20200915 Fin
//				width: 430, 
//				close: function (e, o) {
//					if (callback) callback() ; dialog.dialog('destroy');}}) ;
//			return true ;			
//		}
		if (estHtml_DemandeLogin(psResponse)){
			var lsHTMLDialogue = "<div id='loginPopup' title='Déconnexion suite à une inactivité'>"
										+ "<p>" + psResponse + "</p>"
										+ "</div>";
			$(lsHTMLDialogue).dialog({
				modal:true,
				width: 400, 
				height: 600});

			$('.ui-dialog-titlebar-close').hide();
			$('form').append('<input type="text" id="authentificationParPopup" name="_authentificationParPopup" value="_authentificationParPopup" disabled="" hidden="">');
			$('#authentificationParPopup').val('_authentificationParPopup');
			$('.cadreIntérieur > form').submit(function (event) {
				event.preventDefault();
				lsURL = getBaseURL() + 'login_check';
				$.post(
						lsURL,
						$(this).serialize(),
						)
						.done(function (msg) {
							if (msg) {
									if (!estHtml_DemandeLogin(msg)) {
									$('#loginPopup').dialog("close");
									$('#loginPopup').remove();
									isLogout = false;
									$('#dialog-form').html(msg);
									$('#dialog-form').dialog("open");
								} else {
									if (typeof estModeDev === "function" && estModeDev()) {
										if (typeof LG_Ajax_Notifie === "function") {
											LG_Ajax_Notifie("Echec de l'authentification, la page reçue était : " + msg);
										}
									}
									$("#msgDuLayout").text("Echec de l'authentification");
								}

							}
						})
						.fail(function (xhr, status, error) {
//Lorsque la connexion réussie le responseText est vide, et fail car il attend il reponse JSON.
							if(xhr.responseText === ""){
								//fermeture de la popup
								$('#loginPopup').dialog("close");
								$('#loginPopup').remove();
								isLogout = false;
							} 
							else if (typeof LG_Ajax_Notifie === "function") {
								LG_Ajax_Notifie("La vérification du login a échoué : " + error);
							} else {
								alert('Echec de la vérification du login : ' + error);
							}
						});
			});
		}
//MG Modification 20200929 Fin
		
    },
//
//	// ---------------------------------------------------------------------------------------
//	// Gérer le cas où une réponse est une demande de login
//	// Si c'est le cas, affiche le popup de login et renvoie true
//	gereReponseAjax_DemandeLogin: function(psResponse){
//		if (window.oPlanning.oControleur.estReponseDemandeLogin(psResponse)) {
//			var lsErrText = "Erreur : " ;
//			if (psResponse.left(lsErrText.length) == lsErrText) psResponse = psResponse.right(psResponse.length - lsErrText.length) ;
//			var lsHTMLDialogue = "<div id = 'refreshAfterActionDialog' title='Merci de vous authentifier'><p>" + psResponse + "</p></div>" ;
//			var dialog = $(lsHTMLDialogue).dialog({width: 430, close: function (e, o) {window.oPlanning.oControleur.refreshAfterAction(""); dialog.dialog('destroy');}}) ;
//			return true ;			
//		}
//    },
	
	
	créeSousObjetsSiNécessaire: function () {
		// Créer l'objet Constantes si nécessaire
		if (!window.oPlanning.oCst) window.oPlanning.oCst = new cConstantes();
		
		// Créer l'objet Data si nécessaire
		if (!window.oPlanning.oData) window.oPlanning.oData = new cData();
		
		// Créer l'objet Vue si nécessaire
		if (!window.oPlanning.oVue) window.oPlanning.oVue = new cVue();	
	},
	
	// ---------------------------------------------------------------------------------------
	// Evénement sur fin de chargement
	afterCharge: function(psJSON) {
		// C'est le moment pour mettre en place les menus contextuels
		this.activePoupuMenu() ;
		
		if (window.lWebBrowserVFPControl) {
			// Avertir de la fin du chargement
			LG_Ajax_Notifie("FinChargement") ;
		}
		
		return true ;
    },
	
	// ---------------------------------------------------------------------------------------
	masqueGrille: function() {
		var lbKO = !window.oPlanning.oVue.masqueGrille() ;
		if (window.oPlanning) window.oPlanning.jsonChargé = "" ;
		return !lbKO ;
	},
	
	// ---------------------------------------------------------------------------------------
	activePoupuMenu: function() {
		initContextMenu() ;
    },
	
	// ---------------------------------------------------------------------------------------
	activePoupuMenuBIDON: function() {
		
		$(function() {
			$.contextMenu({
				selector: '.context-menu-one', 
				callback: function(key, options) {
					var m = "clicked: " + key;
					window.console && console.log(m) || alert(m); 
				},
				items: {
					"edit": {name: "Edit", icon: "edit"},
					"cut": {name: "Cut", icon: "cut"},
				   copy: {name: "Copy", icon: "copy"},
					"paste": {name: "Paste", icon: "paste"},
					"delete": {name: "Delete", icon: "delete"},
					"sep1": "---------",
					"quit": {name: "Quit", icon: function(){
						return 'context-menu-icon context-menu-icon-quit';
					}}
				}
			});

			$('.context-menu-one').on('click', function(e){
				console.log('clicked', this);
			});  
		});
	
    },

	// ---------------------------------------------------------------------------------------
	// Rafraichissement de la fenêtre après acquittement d'une action demandée par l'utilisateur
	refreshAfterAction: function(psResponse){
		psResponse = psResponse?psResponse:"";
//
//		// Gérer le cas où la réponse est une demande de login
//		if (window.oPlanning.oControleur.gereReponseAjax_DemandeLogin(psResponse))  {
//			// C'était une demande de login : terminer
//			return ;
//		}
//
//		// Y a-t-il eu erreur ?
//		if (psResponse.match("Erreur : ")) {
//			// Erreur sous forme texte
//			var lsMsgErr = psResponse.substr("Erreur : ".length, psResponse.length - "Erreur : ".length) ;
//			lsMsgErr = lsMsgErr?lsMsgErr:"Erreur inconnue" ;
//		}
//		if (psResponse.match('{"Erreur": ')) {
//			// Erreur sous forme JSON
//			var loResponse = JSON.parse(psResponse);
//			var lsMsgErr = loResponse.Erreur ;
//			lsMsgErr = lsMsgErr?lsMsgErr:"Erreur inconnue" ;
//		}
//		if (typeof lsMsgErr !== 'undefined') {
////			if ($("#refreshAfterActionDialog").length) {$("#refreshAfterActionDialog").remove() ;}
////			if ($("[aria-describedby='refreshAfterActionDialog']").length) {$("[aria-describedby='refreshAfterActionDialog']").remove() ;}
//
//			var lsHTMLDialogue = "<div id = 'refreshAfterActionDialog' title='Une erreur est survenue'><p>" + lsMsgErr + "</p></div>" ;
//			var dialog = $(lsHTMLDialogue).dialog({width: 1000, close: function (e, o) {window.oPlanning.oControleur.refreshAfterAction(""); dialog.dialog('destroy');}}) ;
////			// Terminer quand même
////			psResponse = "" ;
//			return ;
//		} 
//		
//		if (psResponse.match('{"Dialogue": ')) {
//			// Il y a une demande de diapogue
//			var loResponse = JSON.parse(psResponse);
//			window.oPlanning.oControleur.refreshAfterAction_Response = loResponse ; 
//			var loResponseDialogue = loResponse.Dialogue;
//			var lsHTMLDialogue = "<div id = 'refreshAfterActionDialog' title='" + loResponseDialogue.Titre + "''><p>" + loResponseDialogue.Message + "</p></div>"
//			var loBoutons = {} ;
//			loResponseDialogue.Boutons.forEach(function(loBouton) {
//				loBoutons[loBouton.Texte] = eval("window.oPlanning.oControleur." + loBouton.Action) ;
//			});
//			var loDialogOptions = {
//					autoOpen: false,
//					height: "auto",
//					width: "auto",
//					modal: true,
//					buttons: loBoutons,
//					close: function (e, o) {
//						// Vider tout pour restaurer les options qui ont pu être modifiées à l'utilisation	
//						dialog.dialog('destroy');
//            },
//					};
//			var dialog = $(lsHTMLDialogue).dialog(loDialogOptions).dialog("open") ;
//			return;
//		}

		// Rafraichir la vue
		window.oPlanning.oControleur.rafraichit(psResponse) ;
		if ($("#refreshAfterActionDialog").length > 0) {
			// Il existe une boite de dialogue : la fermer
			$("#refreshAfterActionDialog").dialog("close");
		}
		// Effacer le masque d'attente
		window.oPlanning.oVue.setDisabled(true) ;
	},
	// Lors de l'annulaion d'une boite de dialogue demandant confirmation de l'action
	refreshAfterAction_Dialog_Annuler: function() {
		// Lanceer le rafraichissement par défaut
		window.oPlanning.oControleur.refreshAfterAction() ;
	},
	// Lors de la validation d'une boite de dialogue demandant confirmation de l'action
	// (psResponse)		! texte de la réponse (Dft = "OK")
	refreshAfterAction_Dialog_OK: function(psResponse) {
		// Relancer la demande initiale en indiquant la réponse "OK"
		var loResponse = window.oPlanning.oControleur.refreshAfterAction_Response ;
		var loDemande = loResponse.DemandeInitiale ;
		if (typeof(psResponse) == "object") loDemande[loResponse.DialogueEnCours] = "OK" ;
		else loDemande[loResponse.DialogueEnCours] = psResponse?psResponse:"OK" ;
		window.oPlanning.oControleur.getGestionnaireActions().lanceActionSymfony(loDemande, window.oPlanning.oControleur.refreshAfterAction) ;
	},
	// Lors de la validation d'une boite de dialogue demandant la suppression de la participation
	refreshAfterAction_Dialog_SupprimeParticipation: function() {
		// Relancer la demande initiale en indiquant la réponse "SupprimeParticipation"
		window.oPlanning.oControleur.refreshAfterAction_Dialog_OK("SupprimeParticipation") ;
	},
	// Lors de la validation d'une boite de dialogue demandant la suppression de toute la séance
	refreshAfterAction_Dialog_SupprimeTouteLaSéance: function() {
		// Relancer la demande initiale en indiquant la réponse "SupprimeParticipation"
		window.oPlanning.oControleur.refreshAfterAction_Dialog_OK("SupprimeTouteLaSéance") ;
	},
	
//MG Ajout 20200616 Début
	refreshAfterAction_Dialog_CetteSeance : function(psResponse){
		var loResponse = window.oPlanning.oControleur.refreshAfterAction_Response ;
		var loDemande = loResponse.DemandeInitiale ;
		if (typeof(psResponse) == "object") loDemande[loResponse.DialogueEnCours] = "CetteSeance" ;
		else loDemande[loResponse.DialogueEnCours] = psResponse?psResponse:"CetteSeance" ;
		window.oPlanning.oControleur.getGestionnaireActions().lanceActionSymfony(loDemande, window.oPlanning.oControleur.refreshAfterAction) ;
	},
	
	refreshAfterAction_Dialog_TouteSerie : function(psResponse){
		var loResponse = window.oPlanning.oControleur.refreshAfterAction_Response ;
		var loDemande = loResponse.DemandeInitiale ;
		if (typeof(psResponse) == "object") loDemande[loResponse.DialogueEnCours] = "TouteSerie" ;
		else loDemande[loResponse.DialogueEnCours] = psResponse?psResponse:"TouteSerie" ;
		window.oPlanning.oControleur.getGestionnaireActions().lanceActionSymfony(loDemande, window.oPlanning.oControleur.refreshAfterAction) ;
	},
	
	refreshAfterAction_Dialog_Depart : function(psResponse){
		var loResponse = window.oPlanning.oControleur.refreshAfterAction_Response ;
		var loDemande = loResponse.DemandeInitiale ;
		if (typeof(psResponse) == "object") loDemande[loResponse.DialogueEnCours] = "Depart" ;
		else loDemande[loResponse.DialogueEnCours] = psResponse?psResponse:"Depart" ;
		window.oPlanning.oControleur.getGestionnaireActions().lanceActionSymfony(loDemande, window.oPlanning.oControleur.refreshAfterAction) ;
	},
	
	refreshAfterAction_Dialog_Scinder : function(psResponse){
		var loResponse = window.oPlanning.oControleur.refreshAfterAction_Response ;
		var loDemande = loResponse.DemandeInitiale ;
		if (typeof(psResponse) == "object") loDemande[loResponse.DialogueEnCours] = "Scinder" ;
		else loDemande[loResponse.DialogueEnCours] = psResponse?psResponse:"Scinder" ;
		window.oPlanning.oControleur.getGestionnaireActions().lanceActionSymfony(loDemande, window.oPlanning.oControleur.refreshAfterAction) ;
	},
	
	refreshAfterAction_Dialog_Hebdo : function(psResponse){
		var loResponse = window.oPlanning.oControleur.refreshAfterAction_Response ;
		var loDemande = loResponse.DemandeInitiale ;
		if (typeof(psResponse) == "object") loDemande[loResponse.DialogueEnCours] = "Hebdo" ;
		else loDemande[loResponse.DialogueEnCours] = psResponse?psResponse:"Hebdo" ;
		window.oPlanning.oControleur.getGestionnaireActions().lanceActionSymfony(loDemande, window.oPlanning.oControleur.refreshAfterAction) ;
	},
	
	
//MG Ajout 20200616 Fin
	
	// ---------------------------------------------------------------------------------------
	// Lancer le rafraichissement du planning avec un texte JSON connu
	// Cf structure dans this.getReponseBidon()
    rafraichit: function (psJson) {

// psJson = '{"aSeancesEffacees": [], "aSeances": [], "aSeancesSelectionnees": [{"sIdSeance": "S6", "sIdRessource": "I4"}]}'
	
		var lbKO = false ;
		if (!psJson) {
			// Réponse vide
			psJson = '{	"aSeancesEffacees": [],	"aSeances": [],"aSeancesSelectionnees":[]}' ;
			
		} else if (LG_Ajax_GetText_EstErreur(psJson)) {
			// Erreur
			if (window.oPlanning.oControleur.getModeExecution() == window.oPlanning.oCst.ModeExecution_TestNavigateur) {
				// Test en cours sur le navigateur
				psJson = this.getReponseBidon() ;
			} else {
				return false ;
			}
		} else {
			// Réponse correcte
		}
		
		var lo = JSON.parse(psJson) ;
		if (lo.oStructure) {
			// On a fourni un JSON complet
			// Faire le rechargement intégral
			return this.charge(psJson) ;
		}
		
		// Suppression des séances obsolètes
		var laSeancesEffacees = lo.aSeancesEffacees ;
		if (laSeancesEffacees && laSeancesEffacees.length) {
			for (var i = 0; i < laSeancesEffacees.length; i++) {
				if (window.oPlanning.oData.effaceSeance(laSeancesEffacees[i])) {
					// Succès de l'effacement des données : effacer de la vue
					window.oPlanning.oVue.effaceSeance(laSeancesEffacees[i]) ;
				}
			}
		}
		
		// Insertion/rafraichissement des séances nouvelles
		var laSeancesAInsérer = lo.aSeances ;
		if (laSeancesAInsérer && laSeancesAInsérer.length) {
			for (var i = 0; i < laSeancesAInsérer.length; i++) {
			if (window.oPlanning.oData.insereSeance(laSeancesAInsérer[i])) {
					// Succès de l'insertion des données : ajouter à la vue
					window.oPlanning.oVue.insereSeance(laSeancesAInsérer[i]) ;
				}
			}
		}
		
		// Sélection
		window.oPlanning.oStructure.unSelect() ;
		var laSeancesASélectionner = lo.aSeancesSelectionnees ;
		if (laSeancesASélectionner && laSeancesASélectionner.length) {
			for (var i = 0; i < laSeancesASélectionner.length; i++) {
				window.oPlanning.oVue.selectAvecObjet(laSeancesASélectionner[i]) ;
			}
		}
		
		return !lbKO ;
	},

	// ---------------------------------------------------------------------------------------
	// Actions générées par un double clic sur une séance (on fournit l'Id de la div du masque de sélection de la séance)
	onDoubleClickSeance: function (poSéance){
		var lsIdDivMasque = poSéance.getIdDiv() ;
		var loMasque = window.oPlanning.oStructure.getSeanceSelectionneeAvecDiv(lsIdDivMasque) ;

/*		var loDivMasque = document.getElementById(lsIdDivMasque) ;
		var lsTop = loMasque.getDivTop()  ;
		var liDebut = loMasque.iDebut ;
		var liDebutCalc = loMasque.getHeureParCoord(0) ;
		var lsMsg = "Double clic<br>"
					+ "loMasque.getDivTop() = " + loMasque.getDivTop() + "<br>"
					+ "getDivTop(loDivMasque) = " + getDivTop(loDivMasque) + "<br>"
					+ "loMasque.getCoordParHeure(loMasque.iDebut) = " + loMasque.getCoordParHeure(loMasque.iDebut) + "<br>"
					+ "loMasque.getHeureParCoord(0) = " + loMasque.getHeureParCoord(0) / 60 + "<br>"
					+ "loMasque.getHeureParCoord(50) = " + loMasque.getHeureParCoord(50) / 60 + "<br>"
					+ "loMasque.getHeureParCoord(100) = " + loMasque.getHeureParCoord(100) / 60 + "<br>"
					+ "loMasque.getHeureParCoord(200) = " + loMasque.getHeureParCoord(200) / 60 + "<br>"
					+ "loMasque.iDebut = " + loMasque.iDebut / 60 + "<br>"
					+ "loMasque.getHeureParCoord(loMasque.getDivTop()) = " + loMasque.getHeureParCoord(loMasque.getDivTop()) / 60 + "<br>"
					+ "loMasque.getHeureParCoord(loMasque.getDivTop() + loMasque.getDivHeight()) = " + loMasque.getHeureParCoord(loMasque.getDivTop() + loMasque.getDivHeight()) / 60 + "<br>"
					;
*///		popupWindow(lsMsg, true) ;
		
//var ldDate = loMasque.dDate ;
//popupWindow(ldDate + "<br>" + VFP_ToLitteral(ldDate));
//return ;

		window.oPlanning.oVue.setDisabled() ;
// popupWindow(loMasque.dDate + "<br>" + DTOC(loMasque.dDate)) ;
// LG 20190517 début
// 		var loData = [{nom:"sIdSeance", valeur: loMasque.sIdSeance}
//						, {nom:"sIdRessource", valeur: loMasque.sIdRessource}
//						, {nom:"dDate", valeur: loMasque.dDate}
//						, {nom:"iDebut", valeur: loMasque.iDebut}
//						, {nom:"iFin", valeur: loMasque.iFin}
//						] ;
//		LG_Ajax_GetText("onDoubleClicSéance", loData, "", "JSON", this.refreshAfterAction);
		this.getGestionnaireActions().onDoubleClicSéance(loMasque, this.refreshAfterAction);
// LG 20190517 fin

// popupWindow("onDoubleClickSeance: " + lsJSON, true) ;
/*
		// Etude du cas où l'événement "BeforeNavigate2" du webbrowser n'a pas été appellé
		// Bug du webbrowser qui arrive par exemple quand le processus VFP est "surchargé"
		var lsTexteNonRempli = LG_Ajax_GetText_RtvCst_NR() ; 
		if (lsJSON == lsTexteNonRempli){
			// Le texte n'a pas pu être lu instantanément
			// L'événement "BeforeNavigate2" du webbrowser n'a sans doutes pas été appellé
			
			// Préparer la div de réecption du JSON
//			var lsNomDiv = LG_Ajax_GetNomDivEWB("PlanningSynthese") ;
			var lsNomDiv = LG_Ajax_GetNomDivEWB(psNomFichJSON) ;
			document.getElementById(lsNomDiv).innerHTML = "" ;
			
			// Demander le chargement en asynchrone : VFP attend dans un timer que window.oPlanning.oControleur.lDemandeChargement devienne true
			window.oPlanning.oControleur.lDemandeChargement = true ;
			
			// Lancer le timer qui va attendre qu'il y aie des données
			window.oPlanning.oControleur.timerCheckPrésenceJSON = window.oPlanning.setInterval(window.oPlanning.oControleur.checkPrésenceJSON, 50) ;
		} else {
			// Le texte a pu être lu instantanément
			// Appeller directement la méthode de chargement
			window.oPlanning.oControleur.charge(lsJSON) ;
		}
*/

		
	},

	// ---------------------------------------------------------------------------------------
	// Gestion de la fin d'un dimensionnement de déance
	onFinDimesionnementSeance: function(poSeance, e){
		this.onFinDéplacementSéance(poSeance, e)
	},

	// ---------------------------------------------------------------------------------------
	// Gestion de la fin d'un déplacement de déance
	onFinDéplacementSéance: function(poMasque, e, poDragInfos){
		if (!poMasque.aParaSeance.iId_Seance) {
			// Séance vierge : RAS
			return ;
		}

		window.oPlanning.oVue.setDisabled() ;
		this.getGestionnaireActions().onFinDéplacementSéance(poMasque, e, poDragInfos, this.refreshAfterAction)
	},

	// ---------------------------------------------------------------------------------------
	// Appui d'une touche
	onKeyDown: function (e) {
/* LG 20170120 déac début
//		if (window.oPlanning.attenteReponse) {
//			// On attend déjà une réponse de VFP
//			console.log("onKeyDown annulé car le programme attend une réponse de vfp");
//			return;
//		}
LG 20170120 déac fin */
		
		if (!e) var e = window.event ;
		var liKeyCode ;
		if (e.keyCode) liKeyCode = e.keyCode;
		else if (e.which) liKeyCode = e.which;
		if (liKeyCode == 13 
				|| liKeyCode == 16 
				|| liKeyCode == 17 
				|| liKeyCode == 18 
				|| liKeyCode == 20
				) {
			// Cette touche ne nécessite pas d'appel à FoxPro
			liKeyCode = 0;
			return;
		}
		
		// Faire la liste des séances sélectionnées
		if (typeof window.oPlanning.oStructure === "undefined") return ;
		var lsLstSéances = window.oPlanning.oStructure.getLstSeancesSelectionnees() ;
		if (lsLstSéances) {
			// Il y a des séances sélectionnées
			if (liKeyCode == 46) {
				// Touche Suppr
				return window.oPlanning.oControleur.supprimeSéance(lsLstSéances) ;

			} else if (liKeyCode == 77) {
				// touche M
				return window.oPlanning.oControleur.modifieSéance(lsLstSéances, "M") ;

			} else if (liKeyCode == 80) {
				// touche P
				return window.oPlanning.oControleur.modifieSéance(lsLstSéances, "P") ;

			} else if (liKeyCode == 83) {
				// touche S
				return window.oPlanning.oControleur.modifieSéance(lsLstSéances, "S") ;
				
			} else if (liKeyCode == 116) {
				// Touche F5
				return false;
				
			} else {
				// Autre touche, pas besoin d'attendre de réponse
			}
		}
		
		return ;
	},
	
	// ---------------------------------------------------------------------------------------
	clickBouton: function(event, psIdDivJour, psIdDivRessource) {
		// console.log("Clic sur jour " + psIdDivJour + ", ressource" + psIdDivRessource) ;
	},

	// ---------------------------------------------------------------------------------------
	getReponseBidon: function(){
		var lsRep = '{'
					+ '"aSeancesEffacees":['
						+ '{"sIdSeance":"S6"}'
						+ ']'
					+ ', "aSeances":['
						+ '{"sIdJour": "J20160912"'
							+ ', "sIdRessource": "I4"'
							+ ', "sIdSeance": "S1"'
							+ ', "cLibelle": "S1 refaite : FP M2<BR><BR>"'
							+ ', "cInfobulle": "FP M2 "'
							+ ', "dDate": "16/09/2016 00:00:00"'
							+ ', "iDebut": 510'
							+ ', "iFin": 720'
							+ ', "iNbChevauchements": 1'
							+ ', "iRangChevauchement": 1'
							+ ', "cCouleur": "red"'
							+ ', "lVirtuel": true'
							+ ', "lLibre1": true'
							+ ', "lLibre2": true'
							+ '}'
						+ ']'
					+ ', "aSeancesSelectionnees":['
						+ '{"sIdSeance":"S1"'
							+ ', "sIdRessource": "I4"'
						+ '}'
						+ ']'
					+ '}' ;
		var lsRep = '{	"aSeancesEffacees": [],	"aSeances": [],"aSeancesSelectionnees":[]}' ;
		var lsRep = '{'
					+ '"aSeancesEffacees":['
//						+ '{"sIdSeance":"S6"}'
						+ ']'
					+ ', "aSeances":['
						+ '{"sIdJour": "J20160915"'
							+ ', "sIdRessource": "I4"'
							+ ', "sIdSeance": "S6"'
							+ ', "cLibelle": "S6 redimensionnée : FP M2<BR><BR>"'
							+ ', "cInfobulle": "FP M2 "'
							+ ', "iDebut": 600'
							+ ', "iFin": 700'
							+ ', "iNbChevauchements": 1'
							+ ', "iRangChevauchement": 1'
							+ ', "cCouleur": "blue"'
							+ ', "lVirtuel": false'
							+ ', "lLibre1": false'
							+ ', "lLibre2": false'
							+ '}'
						+ ']'
					+ ', "aSeancesSelectionnees":['
						+ '{"sIdSeance":"S6"'
							+ ', "sIdRessource": "I4"'
						+ '}'
						+ ']'
					+ '}' ;

		return lsRep ;
	},
	
	// Copier/coller une séane
	copieSéance: function(poDivSeance) {
		var loSeanceSource = window.oPlanning.oStructure.getSeanceAvecDiv(poDivSeance) ;
		if (!loSeanceSource) return ;
		
		this.clipboard = {} ;
		this.clipboard.sIdDivSeance = poDivSeance.id ;
		this.clipboard.cLbl = loSeanceSource.cInfobulle ;
		console.log(poDivSeance.id + "copiée") ;
	},
	
	clipboardContientSéance: function() {
		if (!this.clipboard) return false ;
		else if (!this.clipboard.sIdDivSeance) return false ;
		else return true ;
	},
	
	colleSéance: function(plPasteSpecial, poDivSeanceDest) {
		if (!this.clipboardContientSéance()) return ;
		var loSeanceSource = window.oPlanning.oStructure.getSeanceAvecDiv(this.clipboard.sIdDivSeance) ;
		var lsSéanceSource = loSeanceSource.sIdSeance ;
		
		var loSéanceDest = window.oPlanning.oStructure.getSeanceAvecDiv(poDivSeanceDest) ;

		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"lPasteSpecial", "valeur": ((!plPasteSpecial)?false:true)}
						, {nom:"sIdSeanceSource", "valeur": loSeanceSource.sIdSeance}
						, {nom:"sIdRessourceDest", "valeur": loSéanceDest.sIdRessource}
						, {nom:"dDateDest", "valeur": loSéanceDest.dDate}
						, {nom:"iDebutDest", "valeur": loSéanceDest.iDebut}
						, {nom:"iFinDest", "valeur": loSéanceDest.iFin}
						, {nom:"iSéanceDest", "valeur": loSéanceDest.sIdSeance}
						] ;
		LG_Ajax_GetText("onColleSéance", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	remetJournéeAuStandard: function(psIdRessource, pdDate) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"dDate", "valeur": pdDate}
						] ;
		LG_Ajax_GetText("onRemetJournéeAuStandard", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	supprimeSéance: function(psLstSéances) {
// LG 20190607 début
//		window.oPlanning.oVue.setDisabled() ;
//		var lsJSON = [{nom:"sIdSeance", "valeur": psLstSéances}] ;
// 		LG_Ajax_GetText("onSupprimeSéance", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
		this.getGestionnaireActions().supprimeSéance(psLstSéances, this.refreshAfterAction)
// LG 20190607 fin
	},
	
	rendSéanceExceptionnelle: function(psIdSeance) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}] ;
		LG_Ajax_GetText("onRendSéanceExceptionnelle", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	effaceTout: function(psIdRessource, pdDate) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"dDate", "valeur": pdDate}, {nom:"sIdRessource", "valeur": psIdRessource}] ;
		LG_Ajax_GetText("onEffaceTout", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	choisitPoseActivité: function(psIdRessource, pdDate, piDébut, piFin) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"dDate", "valeur": pdDate}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						] ;
		LG_Ajax_GetText("onChoisitPoseActivité", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	créePoseActivité: function(psIdRessource, pdDate, piDébut, piFin) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"dDate", "valeur": pdDate}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						] ;
		LG_Ajax_GetText("onCréePoseActivité", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	poseSeanceDeLActivité: function(psIdRessource, pdDate, piDébut, piFin) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"dDate", "valeur": pdDate}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						] ;
		LG_Ajax_GetText("onPoseSeanceDeLActivité", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	poseAbsence: function(psIdRessource, pdDate, piDébut, piFin, piId_ActiBaseAbsence) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"dDate", "valeur": pdDate}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						, {nom:"iId_ActiBaseAbsence", "valeur": piId_ActiBaseAbsence}
						] ;
		LG_Ajax_GetText("onPoseAbsence", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
 	
	saisitDatesNumérotation: function(psIdRessource, pdDate, piActiBaseProposeDatesNumérotation) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"dDate", "valeur": pdDate}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"iActiBaseProposeDatesNumérotation", "valeur": piActiBaseProposeDatesNumérotation}
						] ;
		LG_Ajax_GetText("onSaisitDatesNumérotation", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
 	
	// psIdSeance	: Id de séance à modifier
	//(psType)		! "M" (Dft), "P", "S"
	modifieSéance: function(psIdSeance, psType) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						, {nom:"sType", "valeur": (psType)?psType:"M"}
						] ;
		LG_Ajax_GetText("onModifieSéance", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
 	
	// -------------------------------------------------------------------------------------------
	scindeSérie: function(psIdSeance, pdDate, piSérieSéance) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						, {nom:"dDate", "valeur": pdDate}
						, {nom:"iSérieSéance", "valeur": piSérieSéance}
						] ;
		LG_Ajax_GetText("onScindeSérie", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
 	
	// -------------------------------------------------------------------------------------------
	changeFréquenceSérie: function(psIdSeance, piId_Fréquence, psIdRessource, pdDate) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						, {nom:"iId_Fréquence", "valeur": piId_Fréquence}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"dDate", "valeur": pdDate}
						] ;
		LG_Ajax_GetText("onChangeFréquenceSérie", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
 	
	// -------------------------------------------------------------------------------------------
	changePropriétaire: function(psIdSeance) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						] ;
		LG_Ajax_GetText("onChangePropriétaire", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
 	
	// -------------------------------------------------------------------------------------------
	montreConflits: function(psIdSeance) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						] ;
		LG_Ajax_GetText("onMontreConflits", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	wzConflits: function(psIdSeance, psIdRessource, pdDate, piDébut, piFin) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"dDate", "valeur": pdDate}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						] ;
		LG_Ajax_GetText("onWzConflits", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	wzRéaffectationUsagers: function(psIdSeance, psIdRessource, pdDate, piDébut, piFin) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"dDate", "valeur": pdDate}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						] ;
		LG_Ajax_GetText("onWzRéaffectationUsagers", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
 	
	wzAbsenceEtablissement: function(pdDate, piDébut, piFin) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"dDate", "valeur": pdDate}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						] ;
		LG_Ajax_GetText("onWzAbsenceEtablissement", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	modifieActivité: function(psIdSeance) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						] ;
		LG_Ajax_GetText("onModifieActivité", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	changeActivité: function(psIdSeance) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						] ;
		LG_Ajax_GetText("onChangeActivité", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	doTest: function(psIdSeance, psIdRessource, pdDate, piDébut, piFin) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						, {nom:"sIdRessource", "valeur": psIdRessource}
						, {nom:"dDate", "valeur": pdDate}
						, {nom:"iDébut", "valeur": piDébut}
						, {nom:"iFin", "valeur": piFin}
						] ;
		LG_Ajax_GetText("onTest", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	changeOptions: function(psIdSeance) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}] ;
		LG_Ajax_GetText("onOptions", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},
	
	modeAffichageChangé: function(psIdSeance, psModeAffichage, pbHeuresVerticales) {
		window.oPlanning.oVue.setDisabled() ;
		var lsJSON = [{nom:"sIdSeance", "valeur": psIdSeance}
						, {nom:"sModeAffichage", "valeur": psModeAffichage}
						, {nom:"bHeuresVerticales", "valeur": pbHeuresVerticales}] ;
		LG_Ajax_GetText("onModeAffichageChangé", lsJSON, "", "JSON", window.oPlanning.oControleur.refreshAfterAction);
	},

// LG 20190409 début
	// Renvoyer l'instance courante du gestionnaire des actions utilisateur
	getGestionnaireActions: function(){
		if (!this.oGestionnaireActions) {
			// Le gestionnaire d'actions n'existe pas encore : le créer
			var liModeExecution = this.getModeExecution() ;
			if (liModeExecution === window.getPlanningoCst().ModeExecution_VFPLocal) {
				this.oGestionnaireActions = new cGestionnaireActions_VFP() ;
			} else if (liModeExecution === window.getPlanningoCst().ModeExecution_ExecSymfony) {
				this.oGestionnaireActions = new cGestionnaireActions_Symfony() ;
			} else {
			}
		}
		return this.oGestionnaireActions ;
	},
	
	bidonPourFinDeClasse:0
}) ;

function beforeChargeGrille() {

/*	if (document.observe) {
		// prototype actif
		document.observe('click', creerSeanceVide);
	} else if ($) {
		// jQuery actif
		// http://stackoverflow.com/questions/2232988/is-this-port-of-prototype-to-jquery-correct
		$([document]).bind("click", creerSeanceVide);
	} else {
		console.log("Impossible de suivre le clic sur le document") ;
	}*/
	if (isPrototype()) {
		// prototype actif
		document.observe('click', creerSeanceVide);
	} else if ($) {
		// jQuery actif
		// http://stackoverflow.com/questions/2232988/is-this-port-of-prototype-to-jquery-correct
		$([document]).bind("click", creerSeanceVide);
	} else {
		console.log("Impossible de suivre le clic sur le document") ;
	}
	
	function creerSeanceVide(event) {
		var element = event.target || event.srcElement;
		if (element.className == "ressource") window.oPlanning.oStructure.select(null, false, event);
	}

	// Espionner les touches du clavier
// LG 20190824 début
	jQuery(document).off("keydown", window.oPlanning.oControleur.onKeyDown); 	
// LG 20190824 fin
	jQuery(document).on("keydown", window.oPlanning.oControleur.onKeyDown); 	
}

// ****************************************************************************************
// Classe de gestionnaire d'actions générique pour le planning
// ****************************************************************************************
var cGestionnaireActionsBase = Class.extend({

	// ---------------------------------------------------------------------------------------
	// Gestion de la fin d'un déplacement de séance
	onFinDéplacementSéance: function(poMasque, e, poDragInfos, callback){
		console.warn("Méthode virtuelle destinée à être surchargée");
		callback();
	},

	// ---------------------------------------------------------------------------------------
	// Gestion d'un double-clic sur une séance
	onDoubleClicSéance: function(poMasque, callback){
		console.warn("Méthode virtuelle destinée à être surchargée");
		callback();
	},

	// ---------------------------------------------------------------------------------------
	// Gestion de la suppression d'une séance
	supprimeSéance: function(psLstSéances, callback){
		console.warn("Méthode virtuelle destinée à être surchargée");
		callback();
	},
});
