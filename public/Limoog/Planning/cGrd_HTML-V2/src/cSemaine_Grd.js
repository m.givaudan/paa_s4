/**###############################################
 *      CLASSE SEMAINE
 *      
 *     @param {Array} aParaSemaine - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
 *     
 *     Ce tableau de paramètres contient les paramètres suivants :  
 *     @param {String} cLibelle - Nom complet de la Semaine
  *     @param {Number} top - Position haute, en pixel, du cadre contenant la Semaine
 *     @param {Number} bottom - Position Basse, en pixel, du cadre contenant la Semaine 
 *     @param {Number} height - Hauteur, en pixel, du cadre contenant la Semaine 
 *     @param {Number} width - Largeur Basse, en pixel, du cadre contenant la Semaine 
 ###############################################*/

//var Semaine = Class.create({
var cSemaine = Class.extend({
	cClassName: "cSemaine",
	init: function (){
		this.scrolled = false;
	},
	
	initProps: function (poContainerPlanning){
		var aParaSemaine = window.oPlanning.oStructure.oConfig ;

		// Paramètres Optionnels : 
		if (aParaSemaine.cLibelle)
			this.cLibelle = aParaSemaine.cLibelle;
		else
			this.cLibelle = "Sem";
		if (aParaSemaine.vertical)
			this.vertical = aParaSemaine.vertical;
		else
			this.vertical = false;
			
		
		if (poContainerPlanning) {
			// On a fourni un conteneur de planning : utiliser ses dimensions par défaut
			var $poContainerPlanning = $(poContainerPlanning);
			var liHeightDft = $poContainerPlanning.height() 
				- parseFloat($poContainerPlanning.css('border-top-width')) 
				- parseFloat($poContainerPlanning.css('border-bottom-width')) ;
			var liWidthDft = $poContainerPlanning.width() 
				- parseFloat($poContainerPlanning.css('border-left-width')) 
				- parseFloat($poContainerPlanning.css('border-right-width')) ;
//			var liLeftDft = parseFloat($poContainerPlanning.css('padding-left')) ;
//			var liTopDft = parseFloat($poContainerPlanning.css('padding-top')) ;
		} else {
			var liHeightDft = this.vertical?400:700 ;
			var liWidthDft = 800 ;
		}
		var liLeftDft = 0 ;
		var liTopDft = 0 ;
		if (aParaSemaine.top) this.top = aParaSemaine.top; else this.top = liTopDft ;
		if (aParaSemaine.left) this.left = aParaSemaine.left; else this.left = liLeftDft ;
		// le -13 correspond au marge au dessus et en dessous de la semaine
		// Ca permet d'intégrer le planning dans PAA, sans avoir de ScrollBar
		if (aParaSemaine.height) this.height = aParaSemaine.height - 13 ; else this.height = liHeightDft ;
		if (aParaSemaine.width) this.width = aParaSemaine.width; else this.width = liWidthDft ;

		this.heightDemandée = this.height ;
		this.widthDemandée = this.width ;

	},
	
	/**
	 *toString 
	 *@return {String} renvoit les informations de l'instance courante de la semaine 
	 */
	toString: function (){
		return "Semaine : " + this.cLibelle 
	},
	
	// ---------------------------------------------------------------------------------------
	masquerStructure: function() {
		return this.afficherStructure(true) ;
    },
	
	/**
	 * afficherSemaine
	 * Méthode qui, une fois toutes les instances créée, appelle leurs méthodes qui génére le code HTML, 
	 * et le concatène avec le code HTML du corp de la page
	 * 
	 * ordre du Code HTML : 
	 *   la semaine, 
	 *           Horaires à droite,
	 *           Horaires centres, 
	 *           tranches,
	 *               ressources,
	 *    Fin de la semaine,
	 *    ajout des séances,
	 *    ajout de la légende dans les horaires
	 * 
	 */
	afficherStructure: function (pbNoData) {

		// Nettoyer les objets éventuellement préexistants
		if (document.getElementById('mainSemaine')){
			var loCtn = document.getElementById('mainSemaine');
			try {
				var loParent = loCtn.parentElement ;
				var liOldHeight = $(loParent).height() ;
				loParent.removeChild(loCtn);
				if ($(loParent).height() < liOldHeight / 2) {
					// La hauteur du container a réduit
					// Ca arrive dans certains contextes (????)
					// Il suffit alors d'ajouter un élément (même invisible) pour que la heuteur redevienne normale
					loParent.innerHTML = "<div id = 'PourCorrigerBugHauteurContainer' style='visibility:hidden;'/>";
				}
			} catch (e){
				// RAS
				console.log("Impossibe de supprimer mainSemaine") ;
			}
		}
		
		// Récupérer l'objet DOM dans lequel mettre le planning
		if (document.getElementById("Planning")) {
			var loConteneur = document.getElementById("Planning") ;
			loConteneur.style.zIndex = 0 ;	// Pour être cohérent avec le z-index des éléments contenus dans le planning
			loConteneur.style.padding = 0 ;	// Pour que le planning vienne bien collé sur tous les bords
// loConteneur.innerHTML = "" ;
			var loConteneurDim = loConteneur ;
		} else {
			var loConteneur = document.body ;
		}
		
		if (pbNoData) {
			// Masquer la structure
			loConteneur.style.display = "table" ;	// Pour le centrage vertical
			loConteneur.innerHTML = loConteneur.innerHTML
					+ '<div id="mainSemaine"'
						+ 'style = "'
							+ 'height: 100%;'
							+ 'display: table-cell; '	// Pour le centrage vertical
							+ 'vertical-align: middle;'	// Pour le centrage vertical
							+ '"'
						+ ">"
					+ '<Div style = "color:blue;'
						+ " text-align: center;"		// Pour le centrage horizontal
						+ '"'
						+ ' class=""'
						+ ">"
					+ 'Sélectionnez une ressource dans les listes, '
					+ "<br/><br/>"
					+ 'Puis cliquez ici pour charger le planning.'
					+ "</Div>"
					+ "<Center>"
					+ "</Center>"
					+ "</div>" ;
			
			// Mettre en place le gestionnaire d'événements sur clic
			$('#mainSemaine').on('click', function(e){
				var event = jQuery.Event("onClicSurMasquePlanning");
				$("body").trigger(event);
			}) ;
			return ;
		}
		
		// Créer l'objet structure
		window.oPlanning.oStructure = new cStructure() ;
		window.oPlanning.oStructure.initProps(window.oPlanning.oData.oStructure) ;
		
		// Initialiser les propriétés internes
		this.initProps(loConteneurDim) ;
		
		// Calculer les dimensions
		this.calcDimJour() ;
		this.calcPositionTranches();
		window.oPlanning.oStructure.calcPasLegende();
		
		// Dessiner la structure
		// Pour empêcher la sélection de texte dans IE<9 (sinon, c'est géré par le CSS de conteneur)
		var lsDisableSelect = (isIE() && getInternetExplorerVersion() < 9)?" unselectable='on' onselectstart='return false;'":"" ;
		var lsTop = (this.top)?("top : " + this.top + "px; "):"" ;
		var lsLeft = (this.left)?("left : " + this.left + "px; "):"" ;
		var lsDimMainSemaine, liHeightSemaine, liWidthSemaine, liMargeConteneurSuppl, liMargeJoursSuppl ;
		if (window.oPlanning.oData.getModeExecution() == window.oPlanning.oCst.ModeExecution_VFPLocal) {
			// Mode VFP
			liMargeConteneurSuppl = 0 ;
			liMargeJoursSuppl = 0 ;
		} else {
			// Mode navigateur
			liMargeConteneurSuppl = 24 ;
			liMargeJoursSuppl = 4 ;
		}

		if (document.getElementById("Planning")) {
			lsDimMainSemaine = "width: 100%; "
					+ "height: 100%; " ;
		} else {
			lsDimMainSemaine = "width: " + (this.widthDemandée + (this.vertical?0:liMargeConteneurSuppl)) + "px; "
					+ "height: " + (this.heightDemandée + (this.vertical?liMargeConteneurSuppl:0)) + "px; " ;
		}
		
		var liDimEchelles = this.getDimEchelles() ;
		var lsDimConteneur, liHeightJours, liWidthJours ;
		liHeightJours = this.heightInterieur + liMargeJoursSuppl ;
		liWidthJours = this.widthInterieur + liMargeJoursSuppl ;
		if (this.vertical) {
			lsDimConteneur = "height : 100%; "
						+ "left : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px; "
						+ "right : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px;" ;
			liWidthJours = liWidthJours - liDimEchelles * 2 
							- window.oPlanning.oCst.Marge * 4 ;
		} else {
			lsDimConteneur = "width : 100%; "
						+ "top : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px; "
						+ "bottom : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px;" ;
			liHeightJours = liHeightJours - liDimEchelles * 2 
							- window.oPlanning.oCst.Marge * 4 ;
		}
liHeightJours -= 4 ;
liWidthJours -= 4 ;

		loConteneur.innerHTML = loConteneur.innerHTML
				+ "<div id='mainSemaine'"
					+ " class='mainSemaine shadow-sm'"
					+ lsDisableSelect
					+ " style='"
						+ lsDimMainSemaine
						+ "position: absolute;"
						+ lsTop + lsLeft
						
// + "background-color: red;"
						+ "overflow : hidden;"
						+ "'"
					+ ">"
				+ this.rtvHTMLLegende('gauche')
				+ this.rtvHTMLLegende('centre')
				+ this.rtvHTMLLegende('droite')
				+ "<div id='Conteneur' class='conteneur' "
					+ "style='"
					+ "position: absolute;"
					+ "overflow : auto;"
					+ lsDimConteneur
					+ "'"
					+ " onscroll=" + 'window.oPlanning.oGrille.onConteneurScrolled()'
					+ ">"
				+ "<div id='Jours' class='jours' "
					+ "style='"
					+ "position: absolute;"
					+ "height : " + liHeightJours + "px; "
					+ "width : " + liWidthJours + "px;"
					+ "'>"
				+ this.rtvHTMLJours()
				+ "</div>"
				+ "</div>"
				+ "</div>";

	},
	
	onConteneurScrolled: function (){
//		var loDiv = $("#Conteneur") ;
		var loDiv = document.getElementById("Conteneur") ;
// popupWindow("Scrolled : " + loDiv.scrollTop + ", " + loDiv.scrollLeft);

		var la = document.getElementsByClassName("colléEnHaut");
//		var la = GEBCN("colléEnHaut");
		for (i=0;i<la.length;i++) {
			if (this.vertical) {
//				if (!la[i].initialTop) la[i].initialTop = parseFloat(la[i].style.top) ;
//				la[i].style.top = (la[i].initialTop -loDiv.scrollTop) + "px" ;
				la[i].style.top = -loDiv.scrollTop + "px" ;
			} else {
//				if (!la[i].initialLeft) la[i].initialLeft = parseFloat(la[i].style.left) ;
//				la[i].style.left = (la[i].initialLeft -loDiv.scrollLeft) + "px" ;
				la[i].style.left = -loDiv.scrollLeft + "px" ;
			}
		}
		
		var la = document.getElementsByClassName("colléADroite");
//		var la = GEBCN("colléADroite");
		for (i=0;i<la.length;i++) {
			if (this.vertical) {
				if (!la[i].initialTop) la[i].initialTop = parseFloat(la[i].style.top) ;
				la[i].style.top = (la[i].initialTop + loDiv.scrollTop) + "px" ;
			} else {
				if (!la[i].initialLeft) la[i].initialLeft = parseFloat(la[i].style.left) ;
				la[i].style.left = (la[i].initialLeft + loDiv.scrollLeft) + "px" ;
			}
	   }		
		
	},
	
	// Dessiner les éléments variables : séances et zones interdites
	afficherData: function (){
		this.afficherZonesInterdites();
		this.afficherSeances();
// document.getElementById("Conteneur").onscroll = function(){popupWindow("onScroll") ;} ;
	},
	
	// Renvoyer le nb de jours à afficher (NB : pour surcharge par Calendrier)
	getNbJours: function () {
		var liNbJours = window.oPlanning.oStructure.aJours.length ;
		if (this.iNbJoursMax) liNbJours = Math.min(liNbJours, this.iNbJoursMax) ;
		return liNbJours ;
	},
	
	// Renvoyer la dimensiond de l'en-tête (NB : pour surcharge par Calendrier)
	getDimEnTete: function () {
		return 0 ;
	},
	
	calcDimJour: function () {
		if (this.iDimJour)  {	
			// Déja déterminé
			return this.iDimJour ;
		}
		
		var liNbJours = this.getNbJours() ;
		var iTotalMarge = window.oPlanning.oCst.Marge * (liNbJours - 1);
		var liDimRéservée = iTotalMarge 
							+ (this.getDimEchelles() + window.oPlanning.oCst.Marge * 2) * 2
							+ this.getDimEnTete() ;
		var liNbRessources = window.oPlanning.oStructure.getNbRessources() ;
		var liRetu, liDimJour ;
// var liTmp = this.height ;
		
		if (this.vertical) {
			var liDimJour = (this.width - liDimRéservée) / (liNbJours);
			liDimJour = parseFloat(liDimJour);
			if (liDimJour < (liNbRessources * window.oPlanning.oStructure.largeurRessourceMin)) {
				liDimJour = liNbRessources * window.oPlanning.oStructure.largeurRessourceMin;
				liDimJour = parseFloat(liDimJour);
			}
			liRetu = liDimJour ;
			
			// Recalculer la dimension totale
			this.width = liRetu * liNbJours + liDimRéservée ;
		} else {
			var liDimJour = (this.height - liDimRéservée) / (liNbJours) ;
			liDimJour = parseFloat(liDimJour);
			if (liDimJour < (liNbRessources * window.oPlanning.oStructure.hauteurRessourceMin)) {
				liDimJour = liNbRessources * window.oPlanning.oStructure.hauteurRessourceMin;
				liDimJour = parseFloat(liDimJour);
			}
			liRetu = liDimJour ;
			
			// Recalculer la dimension totale
			this.height = liRetu * liNbJours + liDimRéservée ;
			
			// Recalculer la largeur d'un bouton de ressource, selon qu'un jour est assez haut ou non
			if (liDimJour / liNbRessources < window.oPlanning.oCst.LargeurBouton * 2)
				window.oPlanning.oStructure.lBoutonRessourceLarge = true ;
			else 
				window.oPlanning.oStructure.lBoutonRessourceLarge = false ;
			if (window.oPlanning.oStructure.lBoutonRessourceLarge) window.oPlanning.oStructure.LargeurBoutonRessource = window.oPlanning.oCst.LargeurBoutonLarge ;
			else window.oPlanning.oStructure.LargeurBoutonRessource = window.oPlanning.oCst.LargeurBouton ;
			
		}

		// Recalculer les dimensions
		if (this.vertical) {
			this.widthInterieur = this.width ;
			this.widthInterieurJour = this.widthInterieur ;
			
			this.heightInterieur = Math.max(this.height, window.oPlanning.oCst.hauteurPlanningMin)
			this.heightInterieurJour = this.heightInterieur
									- (window.oPlanning.oCst.HauteurBouton + window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource) * 2 ;
		} else {
			this.widthInterieur = Math.max(this.width, window.oPlanning.oCst.largeurPlanningMin)
			this.widthInterieurJour = this.widthInterieur
									- (window.oPlanning.oCst.LargeurBouton + window.oPlanning.oStructure.LargeurBoutonRessource + window.oPlanning.oCst.EspaceEntreBoutonsJourEtRessource * 2) ;
			
			this.heightInterieur = this.height ;
			this.heightInterieurJour = this.heightInterieur ;
		}
		
		this.iDimJour = parseFloat(liRetu) ;
// alert("this.height = " + liTmp + ", this.height = " + this.height + ", liRetu = " + liRetu) ;
		return liRetu ;
	},
	
	/**
	 * rtvHTMLLegende
	 * Méthode Appelée par afficherSemaine, qui renvoit le code HTML correspondant à la DIV horaire à créer.
	 * Il y a trois div à créer : centre/droite/gauche
	 * 
	 * @param {String} position - [centre/droite/gauche] Position de la Div à créer. 
	 * @see voir la classe seance pour la documentation du tableau de paramètres
	 * @return {String} codeHTMLHoraires - Renvoit le code HTML de la div horaires
	 */
	rtvHTMLLegende: function (position, piDimension) {

		var aPJ = {cLibelle : position, sIdJour : position, cCouleur :'RGB(255,255,255)', vertical: this.vertical} ;
		if (position == 'centre') {
			aPJ.sClass = "legendeCentre" ;
//			aPJ.zIndex = -1 ;
			aPJ.bAfficheHeureLégende = false ;
			aPJ.bAfficheTraitsLégende = true ;
			aPJ.bAfficheInterTranches = true ;
		} else {
			aPJ.sClass = "legende" ;
			aPJ.bAfficheHeureLégende = true ;
			aPJ.bAfficheTraitsLégende = true ;
			aPJ.bAfficheInterTranches = true ;
		}
//		aPJ.backgroundColor = (position == 'droite')?"blue":"green" ;
//		aPJ.backgroundColor = "white" ;

		if (this.vertical) {
			aPJ.textAlign = "center" ;
		} else {
			aPJ.textAlign = "left" ;
		}
		
		if (this.vertical) {
			aPJ.top = false ;
			aPJ.bottom = false ;
//			aPJ.height = this.heightInterieur + "px" ;
			aPJ.height = (piDimension?piDimension:this.heightInterieur) + "px" ;
			aPJ.width = (window.oPlanning.oCst.DimEchellesV - window.oPlanning.oCst.DimBordureEchelles) + "px" ;
			
			var liPos = window.oPlanning.oCst.Marge ;
			if (position == 'centre') {
				aPJ.width = false ;
				aPJ.left = (liPos + this.getDimEnTete()) + "px" ;
				aPJ.right = liPos + "px" ;
				
			} else if (position == 'gauche') {
				aPJ.left = (liPos + this.getDimEnTete()) + "px" ;
				
			} else if (position == 'droite') {
				aPJ.right = liPos + "px" ;
				
			}
		} else {
			aPJ.height = (window.oPlanning.oCst.DimEchellesH - window.oPlanning.oCst.DimBordureEchelles) + "px" ;
//			aPJ.width = this.widthInterieur + "px" ;
			aPJ.width = (piDimension?piDimension:this.widthInterieur) + "px" ;
			aPJ.left = false ;
			aPJ.right = false ;
			
			var liPos = window.oPlanning.oCst.Marge / 2 ;
			if (position == 'centre') {
				aPJ.top = (liPos + this.getDimEnTete()) + 'px' ;
				aPJ.bottom = liPos + 'px' ;
				aPJ.height = false ;
				/*aPJ.left = (window.oPlanning.oCst.Marge + 15) + 'px' ;*/

			} else if (position == 'gauche') {
				aPJ.top = (liPos + this.getDimEnTete()) + 'px' ;
				aPJ.bottom = false ;
				
			} else if (position == 'droite') {
				aPJ.top = false ;
				aPJ.bottom = liPos + 'px' ;
			}
		}
		var loEchelle = new cLegende(aPJ) ;
		loEchelle.sPosition = position ;

		return loEchelle.rtvHTML() ;

	},
	
	// AC 20121126
	// Cette fonction permet de calculer la position des tranches
	calcPositionTranches: function () {
		// On récupère la surface totale     
		var dimTotale ;
		if (this.vertical) {
			dimTotale = this.heightInterieurJour 
					/*- (window.oPlanning.oCst.iAjustHeightHoraires + this.aTranches.length * 6 + 10)*/ ;
		} else {
			dimTotale = this.widthInterieurJour /*- (window.oPlanning.oCst.Marge) - 11*/ ;
		}
		
		// On calcule la surface utile
		window.oPlanning.oStructure.calcTailleTranches(dimTotale) ;

		return ;
	},

	rtvHTMLJours: function (piStartPos, poMois) {
		var border = window.oPlanning.oCst.DimBordureJours /*1*/;
		var cCodeHTMLJournees = '';
		var lcHTMLJour ;
		var loObjJour, lsIdDivJour, lsLibelle ;
		
		var liDimJour = this.calcDimJour() ;
//		var liPosition = window.oPlanning.oCst.Marge * 2 + this.getDimEchelles() ;
		var liPosition = piStartPos?piStartPos:0 ;

		// Puis on parcourt des jours 
		for (var i = 0; i < window.oPlanning.oStructure.aJours.length; i++) {
			loObjJour = window.oPlanning.oStructure.aJours[i];
			if (!poMois) {} else if (poMois.iAnnée == loObjJour.dDate.getFullYear() && poMois.iMois == getMonth(loObjJour.dDate)) {}
			else {
				// On a fourni un objet mois : ce jour n'en fait pas partie
				continue ;
			}
			lcHTMLJour = loObjJour.rtvHTML(liPosition, liDimJour) ;
			cCodeHTMLJournees = cCodeHTMLJournees + lcHTMLJour ;
			
			// Incrémentation des positions
			liPosition = parseFloat(liPosition) + parseFloat(liDimJour) + border + window.oPlanning.oCst.Marge ;
		}
		
		return cCodeHTMLJournees;
	},

	/**
	 * afficherZonesInterdites
	 * Méthode Appelée par afficherSemaine, qui ajoute dans le code la page HTML le code correspondant à l'ensemble des seances d'une semaine.
	 */
	afficherZonesInterdites: function () {
		var loZone, lcIdDivZone ;
		for (var i = 0; i < window.oPlanning.oStructure.aZonesInterdites.length; i++) {
			loZone = window.oPlanning.oStructure.aZonesInterdites[i] ;
			lcIdDivZone = loZone.getIdDiv()
			if (document.getElementById(lcIdDivZone)) {
				// La tranche est déja affichée : l'effacer
				console.log("La tranche est déja affichée : l'effacer") ;
				document.getElementById(lcIdDivZone).parentNode.removeChild(document.getElementById(lcIdDivZone)) ;
			}
			loZone.afficher() ;
		}
		return;
	},

	//(psIdSeance)	: pour limiter à un Id de séance
	afficherSeances: function (psIdSeance) {
		var loSeance ;

		for (var i = 0; i < window.oPlanning.oStructure.aSeances.length; i++){
			loSeance = window.oPlanning.oStructure.aSeances[i] ;
			if (psIdSeance && !(loSeance.sIdSeance == psIdSeance)) {
				// On a imposé une séance à rafraichir
				// Cette séance n'est pas la bonne
				continue ;
			}

			if (document.getElementById(loSeance.getIdDiv())) {
				// La div correspondant à cette séance existe déja
				console.log("La séance existe déja") ;
				// L'effacer de l'affichage
				var loDiv = loSeance.getDiv() ;
				loDiv.parentNode.removeChild(loDiv) ;				
			} 

			loSeance.afficher() ;

		}
		
		return;
	},
	
	getDimEchelles: function(){
		return (this.vertical)?window.oPlanning.oCst.DimEchellesV:window.oPlanning.oCst.DimEchellesH ;
	},

	bidonPourFinDeClasse:0
});

