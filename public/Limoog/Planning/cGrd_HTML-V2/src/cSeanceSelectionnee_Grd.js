/**###############################################
 *      CLASSE SEANCE SELECTIONNEE
 *
 * @param {Array} aParaSeance - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
 *
 * Ce tableau de paramètres contient les paramètres suivants :
 * @param {String} sIdSeance - Identifiant de la Ressource
 * @param {String} cLibelle - Nom complet de la Ressource
 * @param {String} cInfobulle - Message à afficher lors du passage de la souris sur la séance 
 * @param {String} dDate - Date de la séance 
 * @param {String} iDebut - Heure du début de la seance, en minutes 
 * @param {String} iFin - Heure du début de la seance, en minutes 
// LG 20160830 déac * @param {Objet tranche} tTranche - instance de la classe tranche, parent de la ressource courante
 * @param {String} cCouleur - Couleur d'affichage de la séance 
 ###############################################*/

var cSeanceSelectionnee = cSeance.extend({
	cClassName: "cSeanceSelectionnee",
//	cCSSClass: "context-menu-seance",		// Nom de classe, pour activation du menu contextuel
	init: function (aParaSeance, psIdRessource, psIdTranche) {

// LG 20161003 déac : inutilisé		this.lMasque = true ;
//		var lvRetu = $super(aParaSeance, psIdRessource, psIdTranche); 
		var lvRetu = this._super(aParaSeance, psIdRessource, psIdTranche); 
		
		this.sIdDivSeanceSource = this.getIdDiv() ;
		this.sIdSeance = this.sIdSeance + "_Selection";
		this.borderWidth = window.oPlanning.oCst.DimBordureSéanceSélectionnée ;
// alert(this.dDate) ;
		
		return lvRetu ;
	},

/*
	getDivTop: function () {
		return this._super() - this.posRessource.y ;
	},	
	getDivLeft: function () {
		return this._super() - this.posRessource.x ;
	},	
*/
	// Récupérer les coordonnées à donner au masque (qui est positionné dans la div Conteneur)
	// pour une heure donnée
	getCoordDeMasqueParHeure: function (piHeure, poRessource, poTranche) {
		if (poTranche) var liCoordDansRessource = poTranche.getCoordParHeure(piHeure) ;
		else var liCoordDansRessource = this.getCoordParHeure(piHeure) ;
		
		if (poRessource) {
			var pos = window.oPlanning.oVue.getAbsolutePosInConteneur(poRessource.getDiv()) ;
		} else var pos = this.posRessource ;
		var liDécalage = this.vertical?pos.y:pos.x ;
		
		var liCoordDansConteneur = liCoordDansRessource + liDécalage ;
		return liCoordDansConteneur + 1 ;
	},
	// Récupérer l'heure pour une coordonnée de masque (qui est positionné dans la div Conteneur)
	getHeureParCoordDeMasque: function (piPositionDansConteneur, pbArrondir, poRessource, poTranche) {

		if (poRessource) var pos = window.oPlanning.oVue.getAbsolutePosInConteneur(poRessource.getDiv()) ;
		else var pos = this.posRessource ;
		var liDécalage = this.vertical?pos.y:pos.x ;
		var liPositionDansRessource = piPositionDansConteneur - liDécalage ;

//		var liHeure = this.getHeureParCoord(liPositionDansRessource, pbArrondir) ;
		if (poTranche) var liHeure = poTranche.getHeureParCoord(liPositionDansRessource, pbArrondir) ;
		else var liHeure = this.getHeureParCoord(liPositionDansRessource, pbArrondir) ;

		return liHeure ;
	},
/**/	
	// Cacher le masque sur la séance
	effacer: function () {

		// Annuler la gestion du drag-drop sur cet élément
		// Inspiré de https://gist.github.com/Aymkdn/35a7504031642211101e
		this.setDraggable(false, true) ;
		// Fin de a gestion du dragDrop
		
		// Effacer le masque
		this.oSéanceSource.unSelectThis();
	},
	
	// Afficher le masque sur la séance
	afficher: function (pbSansAlaCréation, poSéanceSource) {
		// Traitement par défaut
		var lvRetu = this._super(pbSansAlaCréation) ;
		var loSéance = this;

		// LG 20160922 mémoriser la séance source
		this.oSéanceSource = poSéanceSource ;

		var loThisDiv = this.getDiv() ;
		var lsIdDivMasque = this.getIdDiv() ;
		var loDivSource = poSéanceSource.getDiv() ;
		var lsIdDivSource = poSéanceSource.getIdDiv() ;
		if (loDivSource != null) this.oldBorder = loDivSource.style.border ;
		
		// Style de la div de masque
		loThisDiv.style.zIndex = '4';
		loThisDiv.className += ' seanceSelectionnee';

		// Désactiver l'observateur de clic
		loThisDiv.onclick = null ;
		// Activer l'observateur de double-clic
		loThisDiv.ondblclick = function(){loSéance.onDblClick()} ;
		
		// Tracer les poignées et lancer les Espions
		var loTranche = this.getTranche() ;
		var lsCursor = this.vertical?"s-resize":"e-resize" ;

		if (this.iFin <= loTranche.iFin && !this.lVirtuel) {
			// Cette séance ne dépasse pas par son début, et elle n'est pas virtuelle
			// Elle est dimensionnable
			this.poigneeDebut = document.createElement('DIV');
			this.poigneeDebut.id = 'DragHandleBot' + lsIdDivMasque;
			this.poigneeDebut.className = 'poigneeDebut';
			this.poigneeDebut.style.backgroundColor = this.couleurBordure;
			this.poigneeDebut.innerHTML = minToHeures(this.iFin);
			this.poigneeDebut.style.cursor = lsCursor;
			this.poigneeDebut.style.display = 'inline';
			loThisDiv.appendChild(this.poigneeDebut);
// LG 20190828 début
//			if (isPrototype()) this.poigneeDebut.observe('mousedown', this.debuteDeplacementPoigneeFin.bind(this));
//			else addListener(this.poigneeDebut, 'mousedown', this.debuteDeplacementPoigneeFin.bind(this));
			$(this.poigneeDebut).on('mousedown', this.debuteDeplacementPoigneeFin.bind(this))
// LG 20190828 début
		}
		
		if (this.iDebut >= loTranche.iDebut && !this.lVirtuel) {
			// Cette séance ne dépasse pas par sa fin, et elle n'est pas virtuelle
			// Elle est dimensionnable
			this.poigneeFin = document.createElement('DIV');
			this.poigneeFin.id = 'DragHandleTop' + lsIdDivMasque;
			this.poigneeFin.className = 'poigneeFin';
			this.poigneeFin.style.backgroundColor = this.couleurBordure;
			this.poigneeFin.innerHTML = minToHeures(this.iDebut);
			this.poigneeFin.style.cursor = lsCursor;
// LG 20191001 deac			this.poigneeFin.style.display = 'inline';
			loThisDiv.appendChild(this.poigneeFin);
// LG 20190828 début
//			if (isPrototype()) this.poigneeFin.observe('mousedown', this.debuteDeplacementPoigneeDebut.bind(this));
//			else addListener(this.poigneeFin, 'mousedown', this.debuteDeplacementPoigneeDebut.bind(this));
			$(this.poigneeFin).on('mousedown', this.debuteDeplacementPoigneeDebut.bind(this))
// LG 20190828 fin
		}

		// Poser la div dans la div Conteneur au lieu de la div ressource
		var pos, loParent ;
		pos = window.oPlanning.oVue.getAbsolutePosInConteneur(loThisDiv) ;
		this.posRessource = window.oPlanning.oVue.getAbsolutePosInConteneur(loThisDiv.parentNode) ;
		loThisDiv.parentNode.removeChild(loThisDiv) ;
		document.getElementById("Conteneur").appendChild(loThisDiv) ;
// loThisDiv.style.borderColor = "red" ;
		if (this.vertical) {
			loThisDiv.style.width = (parseFloat(loThisDiv.style.width) 
							//+ window.oPlanning.oCst.DimBordureSéanceSélectionnée * 2
							//+ window.oPlanning.oCst.DimBordureRessources
							) + "px" ;
		} else  {
			loThisDiv.style.height = (parseFloat(loThisDiv.style.height) 
							// + window.oPlanning.oCst.DimBordureSéanceSélectionnée * 2
							// + window.oPlanning.oCst.DimBordureRessources
							) + "px" ;
		}
		pos.y += window.oPlanning.oCst.DimBordureRessources ;
		pos.x += window.oPlanning.oCst.DimBordureRessources ;
		loThisDiv.style.left = (pos.x) + "px" ;
		loThisDiv.style.top = (pos.y) + "px" ;
		
		// Gestion du drag-drop
		var lbEstDraggable = this.estDraggable() ;
		if (lbEstDraggable) loThisDiv.style.cursor = "move";
		this.setDraggable(lbEstDraggable, !lbEstDraggable) ;
		// Fin de a gestion du dragDrop

//ATTENTION : le centrage est très lent sous IE
		// Ajuster le texte
// contenuSéance
//		var loDivTexte = loThisDiv.childNodes[0].childNodes[0].childNodes[0].childNodes[0] ;
		var loDivTexte = this.getDivTexte(loThisDiv) ;
		loDivTexte.className += " contenuSéanceSélectionnée" ;
//		window.oPlanning.oVue.ajusterTaillePolices("contenuSéanceSélectionnée") ;
//		window.oPlanning.oVue.ajusterTaillePolices(".seanceSelectionnee", "contenuSéanceSélectionnée") ;

//		var loDivTexteSource = loDivSource.childNodes[0].childNodes[0].childNodes[0].childNodes[0] ;
		var loDivTexteSource = this.getDivTexte(loDivSource) ;
		loDivTexte.innerHTML = loDivTexteSource.innerHTML ;
		loDivTexte.style.fontSize = loDivTexteSource.style.fontSize ;
		
		// Activer le menu contextuel
		this.setMenuContextuel(true, loThisDiv)	;
		
		return lvRetu ;
	},
	
	// ----------------------------------------------------------------------
	// Initialiser les listeners pour le Drag/Drop
	setDraggable: function (pbDraggable, pbUnSet) {

		// Inspiré de https://gist.github.com/Aymkdn/35a7504031642211101e
		var loSéance = this;
		var loThisDiv = this.getDiv() ;
		!function() {
		  function draggable(element, pbUndraggable) {
			if (!element) {
				console.log("draggable + " + pbUndraggable + " : element not set") ;
				return ;
			}
			window.oPlanning.dragging = null;
			var dragTarget = element.setCapture ? element : document;
			if (!pbUndraggable) {
				// Mise en place du Drag/drop
// LG 20190829 début
//				addListener(element, "mousedown", onMouseDown);
//				addListener(element, "losecapture", onLoseCapture);
//				addListener(element, "mouseup", onMouseUp, true);
//				addListener(dragTarget, "mousemove", onMouseMove, true);
				$(element).on("mousedown", onMouseDown);
				$(element).on("losecapture", onLoseCapture);
				$(element).on("mouseup", onMouseUp);
				$(dragTarget).on("mousemove", onMouseMove);
// LG 20190829 fin
			} else {
				// Nettoyage du Drag/drop
// LG 20190829 début
//				removeListener(element, "mousedown", onMouseDown);
//				removeListener(element, "losecapture", onLoseCapture);
//				removeListener(element, "mouseup", onMouseUp, true);
//				removeListener(dragTarget, "mousemove", onMouseMove, true);
				$(element).off("mousedown", onMouseDown);
				$(element).off("losecapture", onLoseCapture);
				$(element).off("mouseup", onMouseUp);
				$(dragTarget).off("mousemove", onMouseMove);
// LG 20190829 fin
// LG 20190824 début
// Astuce malpropre pour éviter que la mauvaise séance soit traitée
// En effet, tous les fantomes des séances désélectionnées restent actifs et reçoivent cet événement
// On passe aussi loSéance.unSelected à true lors de la désélection pour le cas où on resélectionne la même séance
				loSéance.unSelected = true ;
// 20190824 fin
			}
			
			function onMouseDown(e) {
				var e = window.event || e;
//				if (e.button !== 0) return ;	// Ce n'est pas le bouton gauche
				if (eventButton(e) !== 0) return ;	// Ce n'est pas le bouton gauche

//				if (!window.oPlanning.oStructure.lReadWrite) {
				if (!window.oPlanning.oStructure.getReadWrite(this.dDate)) {
					// Lecture seule
					popupWindow("Désolé, le planning est en lecture seule", true) ;
					return false ;
				}

				window.oPlanning.dragging = {
					element: element,
					mouseX: e.clientX,
					mouseY: e.clientY,
					startX: parseInt(element.style.left),
					startY: parseInt(element.style.top),
					lastX: parseInt(element.style.left),
					lastY: parseInt(element.style.top),
					iDureeInitiale: loSéance.iFin - loSéance.iDebut,
					start: now(),
					observing: false
				  };
// popupWindow("onMouseDown : " + element.id + "<br>" + window.oPlanning.dragging.startY) ;
			  if (element.setCapture) element.setCapture();
			} 

			function onLoseCapture() {
				window.oPlanning.dragging = null;
			}  

			function onMouseUp(e) {
				if (!window.oPlanning.dragging) return ;
//				if (e.button !== 0) return ;	// Ce n'est pas le bouton gauche
				if (eventButton(e) !== 0) return ;	// Ce n'est pas le bouton gauche
				var loDragInfos = window.oPlanning.dragging ;
				window.oPlanning.dragging = null;
			
				var loDiv = loDragInfos.element ;
				var lbDragDéplacé = false ;
				if (!loDragInfos){//RAS
				} else if (now() - loDragInfos.start < 500) {
// popupWindow("onMouseUp : trop rapide") ;
					// trop rapide : on a juste cliqué ou double cliqué
				} else if (!loDragInfos.déplacementSignificatif) {
					// pas de déplacement significatif
// popupWindow("onMouseUp : pas de déplacement significatif") ;
				} else if (loDragInfos.startX == parseInt(loDiv.style.left)
							&& loDragInfos.startY == parseInt(loDiv.style.top)) {
					// pas de déplacement au final
// popupWindow("onMouseUp : pas de déplacement au final : " + loDiv.id + "<br>" + parseInt(loDiv.style.top)) ;
				} else {
// popupWindow("onMouseUp : " + loDragInfos.start + "<br>" + now() + "<br>" + (now() - loDragInfos.start)) ;

					// fin réelle d'un drag
					lbDragDéplacé = true ;
				}
				/*if (lbDragDéplacé)*/ loSéance.finDrag(e, lbDragDéplacé, loDragInfos) ;
				if (document.releaseCapture) document.releaseCapture();
			}  

			function onMouseMove(e) {
// LG 20190412 début
// Astuce malpropre pour éviter que la mauvaise séance soit traitée
// En effet, tous les fantomes des séances désélectionnées restent actifs et reçoivent cet événement
// On passe loSéance.oSéanceSource.selectionnée à false lors de la désélection
				if (!loSéance.oSéanceSource.selectionnée) return ;
// LG 20190412 fin
// 20190824 début
// On passe aussi loSéance.unSelected à true lors de la désélection pour le cas où on resélectionne la même séance
// On passe aussi loSéance.unSelected à true lors de la désélection pour le cas où on resélectionne la même séance
if (loSéance.unSelected) return ;
//// On ne peut observer qu'une seule séance
//if (window.oSéanceObservée) window.oSéanceObservée.unSelected = true ;
//window.oSéanceObservée = loSéance ;
// 20190824 fin
				if (pbDraggable) return loSéance.observeDrag(e) ;
			}
			
		  };    

		  draggable(loThisDiv, pbUnSet);
		}();
		
	},
	
	// ----------------------------------------------------------------------
	// Evénement Double clic
	onDblClick: function () {
		return window.oPlanning.oControleur.onDoubleClickSeance(this) ;
	},
	
	// ----------------------------------------------------------------------
	// Déterminer si la séance peur être draguée
	estDraggable: function () {
		if (this.lVirtuel) return false ;
		if (this.lDépasseDeTranche) return false ;
		return true ;
	},
	
	// ----------------------------------------------------------------------
	// Actions de suivi du drag de la séance
	observeDrag: function (e) {
		if (!window.oPlanning.dragging) return ;
//		if (e.button !== 0) return ;	// Ce n'est pas le bouton gauche
		if (eventButton(e) !== 0) return ;	// Ce n'est pas le bouton gauche

// 20190824 début
// Astuce malpropre pour éviter que la mauvaise séance soit traitée
// En effet, tous les fantomes des séances désélectionnées restent actifs et reçoivent cet événement
// On passe aussi loSéance.unSelected à true lors de la désélection pour le cas où on resélectionne la même séance
		if (this.unSelected) return ;
// 20190824 fin

// LG 20190412 début
// Pas besoin de faire ce calcul chaque milliseconde
if (!window.oPlanning.dragging.tDerniereObservationDrag) window.oPlanning.dragging.tDerniereObservationDrag = Date.now();
if (Date.now() - window.oPlanning.dragging.tDerniereObservationDrag < 10) return ;
window.oPlanning.dragging.tDerniereObservationDrag = Date.now();
// LG 20190412 fin

		// Flag pour soulager le processeur pendant le dragDrop
		if (window.oPlanning.dragging.observing) return ;
		window.oPlanning.dragging.observing = true ;
		
		// Empêcher le menu contextuel pendant le drag
		if (!window.oPlanning.dragging.menuContextuelDésactivé) {
			// Désactiver maintenant
			this.setMenuContextuel(false, window.oPlanning.dragging.element) ;
			window.oPlanning.dragging.menuContextuelDésactivé = true ;
		}
		
// popupWindow(this.cClassName + ".observeDrag " + now()) ;

		var lbKO = false ;
		var e = window.event || e;
		var déplacementY = (e.clientY - window.oPlanning.dragging.mouseY) ;
		var déplacementX = (e.clientX - window.oPlanning.dragging.mouseX) ;
		if (Math.abs(déplacementX) + Math.abs(déplacementY) > 10) {
			// Il y a eu déplacement
			window.oPlanning.dragging.déplacementSignificatif = true ;
		}
		var top = window.oPlanning.dragging.startY + déplacementY;
		var left = window.oPlanning.dragging.startX + déplacementX;

		// Trouver la ressource sous le pointeur et la rendre sélectionnée
// LG 20190412 old		var elements = allElementsFromPoint(e.clientX, e.clientY, "ressource", "ressource", 1) ;
		var elements = allElementsFromPoint(e.clientX, e.clientY, "ressource", "ressource", 10) ;
		var loDivRessourceSurvolée = elements[0] ;
// var loDivRessourceSurvolée = document.getElementById("I5-tranche1-J20160901") ;
		if (loDivRessourceSurvolée) {
			window.oPlanning.oVue.formateDivRessourcePourDragOver(loDivRessourceSurvolée, this) ;
		
			// Indiquer à la séance qu'elle achangé de jour et de ressource
			var loRessource = window.oPlanning.oStructure.getRessourceAvecDiv(loDivRessourceSurvolée) ;
			window.oPlanning.dragging.sIdRessource = loRessource.sIdRessource ;
			window.oPlanning.dragging.sIdJour = loRessource.sIdJour ;
			window.oPlanning.dragging.dDate = loRessource.dDate ;
		}
		
		// Changer la position de la div en cours de déplacement
		var pos = {x:(Math.max(0, left)), y:(Math.max(0, top)), dim:0};
		var posRessource = window.oPlanning.oVue.getAbsolutePosInConteneur(loDivRessourceSurvolée) ;
		if (false) {
			// Déplacement conforme à la souris
		} else if (!loDivRessourceSurvolée) {
			// Pas de div ressource survolée
			// Ne pas bouger
			// return ;
			lbKO = true ;
		} else if (this.vertical) {
			// Changement pour respecter la ressource parente, vertical
			pos.x = posRessource.x + window.oPlanning.oCst.DimBordureRessources ;
		} else {
			// Changement pour respecter la ressource parente, horizontal
			pos.y = posRessource.y + window.oPlanning.oCst.DimBordureRessources ;
		}
		
		if (!lbKO) {
			var loPosCorrigée = this.observeDrag_CorrigePosition(pos, window.oPlanning.dragging.iDureeInitiale, e, loDivRessourceSurvolée)
			if (!loPosCorrigée) lbKO = true ;	// Correction impossible
		}
		
		if (!lbKO) {
			if (this.vertical) {
				// Changement pour respecter le pas de déplacement, vertical
				var liNouvY = loPosCorrigée.iPos ;
				if (liNouvY < 0) liNouvY = window.oPlanning.dragging.lastY ;	// Déplacement refusé
				pos.y = liNouvY ;
			} else {
				// Changement pour respecter le pas de déplacement, horizontal
				var liNouvX = loPosCorrigée.iPos ;
				if (liNouvX < 0) liNouvX = window.oPlanning.dragging.lastX ;	// Déplacement refusé
				pos.x = liNouvX ;
			}

			// Changer la position de la div
			window.oPlanning.dragging.element.style.left = pos.x + "px";
			window.oPlanning.dragging.element.style.top = pos.y + "px";
			if (!loPosCorrigée.iDim) {}
			else if (this.vertical) {
				// La correction de position a amené une correction de dimension
				window.oPlanning.dragging.element.style.height = loPosCorrigée.iDim + "px";
			} else {
				// La correction de position a amené une correction de dimension
				window.oPlanning.dragging.element.style.width = loPosCorrigée.iDim + "px";
			}

			// Affichage de la nouvelle heure
			if (!loPosCorrigée) {
			} else {
				this.iDebut = loPosCorrigée.iDebut;
				this.iFin = loPosCorrigée.iFin ;
			}
			if (this.poigneeDebut) this.poigneeDebut.innerHTML = minToHeures(this.iFin);
			if (this.poigneeFin) this.poigneeFin.innerHTML = minToHeures(this.iDebut);

			this.gereSignePlus(e.ctrlKey, window.oPlanning.dragging.element) ;

// console.log(this.iFin + " / " + minToHeures(this.iFin)) ;
// if (this.iFin < 700);
			
			// Mémoriser la position actuelle
			window.oPlanning.dragging.lastX = pos.x ;
			window.oPlanning.dragging.lastY = pos.y ;
		}
		
		// Restaurer l'environnement
		window.oPlanning.dragging.observing = false ;
//
//var lsLog = 'e.clientX = ' + e.clientX
// 			+ ', déplacementX =' + déplacementX
// 			+ ', this.iFin = ' + this.iFin
// 			+ ', this.poigneeFin.innerHTML = ' + this.poigneeFin.innerHTML ;
//console.log(lsLog) ;
//popupWindow(lsLog) ;

		return !lbKO ;
	},

	// ----------------------------------------------------------------------
	// ajouteSignePlus
	gereSignePlus: function (pbInclut, poDiv) {
		if (!poDiv) 
			poDiv = this.getDiv() ;
		if (pbInclut) {
			// Inclure le signe +
			if (poDiv.classList) poDiv.classList.add("picPlus");
			else poDiv.className += " picPlus";
		} else {
			if (poDiv.classList) poDiv.classList.remove("picPlus");
			else poDiv.className.replace("picPlus", "");
			
		}
	},

	// ----------------------------------------------------------------------
	// Correction de la destination du drag de la séance
	observeDrag_CorrigePosition: function (poPositionDébut, piDureeInitiale, e, poDivRessourceSurvolée) {
		
		var piPositionDébut = this.vertical?poPositionDébut.y:poPositionDébut.x ;
		
		// Retrouver le contexte de la souris
		// var loDivTranche = allElementsFromPoint(e.clientX, e.clientY, "tranche")[0] ;
		var loDivRessource 
		if (poDivRessourceSurvolée) loDivRessource = poDivRessourceSurvolée ;
		else loDivRessource = allElementsFromPoint(e.clientX, e.clientY, "ressource", "tranche")[0] ;
		//if (!loDivTranche || !loDivRessource) {
		if (!loDivRessource) {
			// Pas de tranche ou de ressource survolée : pas de déplacement possible
			return null ;
		}
		var loDivTranche = loDivRessource.parentNode ;
		
		// Déterminer les bornes de la zone que la séance est autorisée à pénétrer
		var loTranche = window.oPlanning.oStructure.getTrancheAvecDiv(loDivTranche) ;
		var loRessource = window.oPlanning.oStructure.getRessourceAvecDiv(loDivRessource) ;

		// Calcul de la nouvelle heure brute
		if (piDureeInitiale) var liDuréeSéance = piDureeInitiale ;
		else var liDuréeSéance = (this.iFin - this.iDebut) ;
		var liNouvelleHeureDebut = this.getHeureParCoordDeMasque(piPositionDébut, true, loRessource, loTranche);
		var liNouvelleHeureFin = liNouvelleHeureDebut + liDuréeSéance ;
		
		// La nouvelle heure ne doit pas rendre Début > Fin
		if (liNouvelleHeureDebut > liNouvelleHeureFin - window.oPlanning.oStructure.pas) {
console.log("La séance devient trop courte") ;
			liNouvelleHeureDebut = liNouvelleHeureFin - window.oPlanning.oStructure.pas ;
		}

		var liPosPointeur = this.vertical?e.clientY:e.clientX ;
		var liHeurePointeur = this.getHeureParCoordDeMasque(liPosPointeur, false); ;
		var liDébutMin = loTranche.iDebut ;
		var liFinMax = loTranche.iFin ;
		
		var loZone ;
		var laZones = window.oPlanning.oStructure.rtvCollZonesInterdites(loTranche.sIdJour, loTranche.sIdTranche, loRessource.sIdRessource);
		for (var z = 0; z < laZones.length; z++) {
			loZone = laZones[z] ;
			if (liHeurePointeur >= parseInt(loZone.iDebut)
					&& liHeurePointeur <= parseInt(loZone.iFin)) {
				// Le pointeur se trouve dans une zone interdite
				return null ;
			}
			if (liHeurePointeur > parseInt(loZone.iFin)) {
				liDébutMin = Math.max(liDébutMin, loZone.iFin) ;
			}
			if (liHeurePointeur < parseInt(loZone.iDebut)) {
				liFinMax = Math.min(liFinMax, loZone.iDebut) ;
			}
		}
		if (liFinMax < liDébutMin) {
			return null ;
		}

		// Ajuster la séance dans les bornes autorisées
		if ((liNouvelleHeureDebut < liDébutMin
					&& liNouvelleHeureFin > liFinMax)
				|| (liDuréeSéance > (liFinMax - liDébutMin))){
			// La séance dépasse des deux bouts
			// ou sa durée est + longue que la plage
			liNouvelleHeureDebut = liDébutMin ;
			liNouvelleHeureFin = liFinMax ;
			liDuréeSéance = liNouvelleHeureFin - liNouvelleHeureDebut ;
		} else if (liNouvelleHeureDebut < liDébutMin) {
			liNouvelleHeureDebut = liDébutMin ;
			liNouvelleHeureFin = liNouvelleHeureDebut + liDuréeSéance ;
		} else if (liNouvelleHeureFin > liFinMax) {
			liNouvelleHeureFin = liFinMax ;
			liNouvelleHeureDebut = liNouvelleHeureFin - liDuréeSéance ;
		}
		
/*
		// La nouvelle heure ne doit pas dépasser les bornes réelles (???)
		var liKeyCode = eventKeyCode(event);
		if (liKeyCode % 2 == 1 && liNouvelleHeure > this.iHeureMaxReel) {
console.log("à quoi sert this.iHeureMaxReel ?") ;
			liNouvelleHeure = this.iHeureMaxReel;
		}
		if (liKeyCode % 2 == 1 && liNouvelleHeure < this.iHeureMinReel) {
console.log("à quoi sert this.iHeureMinReel ?") ;
			liNouvelleHeure = this.iHeureMinReel;
		}
*/
		
		// Arrondir
		liNouvelleHeureDebut = arrondirHeure(liNouvelleHeureDebut, window.oPlanning.oStructure.pas) ;
		liNouvelleHeureFin = liNouvelleHeureDebut + liDuréeSéance ;
		var liNouvellePositionDébut = this.getCoordDeMasqueParHeure(liNouvelleHeureDebut, loRessource, loTranche) ;

		// Valeur de retour
		var liNouvelleHauteur = 0 ;
		if ((this.iFin - this.iDebut) !== liDuréeSéance) {
			liNouvelleHauteur = this.getCoordDeMasqueParHeure(liNouvelleHeureFin, loRessource, loTranche) ;
			liNouvelleHauteur -= liNouvellePositionDébut ;
			liNouvelleHauteur -= window.oPlanning.oCst.DimBordureSéanceSélectionnée ;
		}
		return {iPos: liNouvellePositionDébut
				, iDim: liNouvelleHauteur
				, iDebut: liNouvelleHeureDebut
				, iFin: liNouvelleHeureFin
				} ;

	},

	// Actions de fin du drag de la séance
	finDrag: function (e, pbDragDéplacé, poDragInfos) {
// popupWindow(this.cClassName + ".finDrag " + now()) ;

		// Enlever le + éventuel
		this.gereSignePlus(false, poDragInfos.element) ;

		// Finir le survol de la ressource en cours
		window.oPlanning.oVue.formateDivRessourcePourDragOver() ;
		
		// Restaurer le menu contextuel qui a été inhibé pendant le drag
		this.setMenuContextuel(true, poDragInfos.element) ;

		if (pbDragDéplacé) {
			// Il y a eu déplacement
			// Déclancher l'événement de fin de déplacement
			window.oPlanning.oControleur.onFinDéplacementSéance(this, e, poDragInfos) ;
		}

	},

	// Remettre la séance comme initialement
	reset: function () {
		
		// Faire le reset du début et de la fin
		var loSeanceSource = window.oPlanning.oStructure.getSeanceAvecDiv(this.sIdDivSeanceSource) ;
		this.iDebut = loSeanceSource.iDebut ;
		this.iFin = loSeanceSource.iFin ;
		
		// Repositionner la Div
		var loDiv = this.getDiv() ;
		var loTranche = this.getTranche() ;
		var liPositionDébut = loTranche.getCoordParHeure(this.iDebut) ;
		var liPositionFin = loTranche.getCoordParHeure(this.iFin) ;
		if (this.vertical) {
			loDiv.style.top = liPositionDébut + 'px';
			loDiv.style.height = (liPositionFin - liPositionDébut) + 'px';
		} else {
			loDiv.style.left = liPositionDébut + 'px';
			loDiv.style.width = (liPositionFin - liPositionDébut) + 'px';
		}
		
		// Poignée du début
		if (this.poigneeDebut) this.poigneeDebut.innerHTML = minToHeures(this.iDebut);
		// Poignée de la fin
		if (this.poigneeFin) this.poigneeFin.innerHTML = minToHeures(this.iFin);
		
	},
	
	/**
	 *debuteDeplacementPoignee
	 *Lance les espions complémentaire pour calculer le redimensionnement
	 *@param {Event} event - L'évenement onClick qui à déclenché l'espion qui appelle cette méthode
	 *
	 */
	debuteDeplacementPoignee: function (event) {

// LG 20180724 old		if (!window.oPlanning.oStructure.lReadWrite) {
		if (!window.oPlanning.oStructure.getReadWrite()) {
			// Lecture seule
			popupWindow("Désolé, le planning est en lecture seule", true) ;
			return false ;
		}
		
		// Empêcher le menu contextuel pendant le drag
		this.setMenuContextuel(false) ;

		var liKeyCode = eventKeyCode(event);
		if (liKeyCode % 2 == 1) this.poigneeFin.style.backgroundColor = "blue";
		
		this.tempIFin = this.iFin;
		this.tempIDebut = this.iDebut;
		
		//Arrèt de la propagation
		if (event.stopPropagation) event.stopPropagation(); // Moz.
		else  event.cancelBubble = true; // IE

		//Empéche le drag and drop de la séance pendant le redimensionnement
		var loDiv = this.getDiv() ;
		
		if (isPrototype()) $(loDiv.parentNode).setAttribute('draggable', 'false');
		else $(loDiv.parentNode).attr('draggable', 'false');
		
		if (isPrototype()) loDiv.coordInitialesSouris = {x: event.pointerX(), y: event.pointerY()};
		else loDiv.coordInitialesSouris = {x: event.clientX, y: event.clientY};
		loDiv.coordInitialesSouris.top = parseFloat(this.getDivTop()) ;
		loDiv.coordInitialesSouris.left = parseFloat(this.getDivLeft()) ;
		loDiv.coordInitialesSouris.height = parseFloat(this.getDivHeight()) ;
		loDiv.coordInitialesSouris.width = parseFloat(this.getDivWidth()) ;
		
		//lancement des espions
		if (isPrototype()) Event.observe(document.body, 'mouseup', this.finDeplacementPoignee.bind(this));
		else $(document.body).bind("mouseup", this.finDeplacementPoignee.bind(this));

		return true ;
		
	},
	
	/**
	 *debuteDeplacementPoigneeFin
	 *Lance les espions complémentaire pour calculer le redimensionnement
	 *@param {Event} event - L'évenement onClick qui à déclenché l'espion qui appelle cette méthode
	 *
	 */
	debuteDeplacementPoigneeFin: function (event) {
		if (this.debuteDeplacementPoignee(event)) {	
			// Espion spécifique
			if (isPrototype()) Event.observe(document.body, 'mousemove', this.observeDeplacementPoigneeFin.bind(this));
			else $(document.body).bind("mousemove", this.observeDeplacementPoigneeFin.bind(this));
			return true ;
		} else return false ;
	},
	
	/**
	 *debuteDeplacementPoigneeDebut
	 *Lance les espions complémentaire pour calculer le redimenssionement
	 *@param {Event} event - L'évenement onClick qui à déclenché l'espion qui appelle cette méthode
	 *
	 */
	debuteDeplacementPoigneeDebut: function (event) {
		if (this.debuteDeplacementPoignee(event)) {		
			// Espion spécifique
			if (isPrototype()) Event.observe(document.body, 'mousemove', this.observeDeplacementPoigneeDebut.bind(this));
			else $(document.body).bind("mousemove", this.observeDeplacementPoigneeDebut.bind(this));
			return true ;
		} else return false ;
	},
			
	/**
	 *DeplacementPoignee_rtvHeureCorrigée
	 *corriger l'heure fournie pour que le déplacement respecte les règles
	 *@param {piHeureBrute}
	 */
	deplacementPoignee_rtvHeureCorrigée: function (piNouvellePosition, pbDébut, event) {

		// Calcul de la nouvelle heure brute
		var liNouvelleHeure = this.getHeureParCoordDeMasque(piNouvellePosition, true);
// popupWindow(piNouvellePosition + "<BR>" + minToHeures(liNouvelleHeure), true) ;
		
		// La nouvelle heure ne doit pas rendre Début > Fin
		if (pbDébut && liNouvelleHeure > this.iFin - window.oPlanning.oStructure.pas) {
console.log("La séance devient trop courte par le début") ;
			liNouvelleHeure = this.iFin - window.oPlanning.oStructure.pas ;
		}
		if ((!pbDébut) && liNouvelleHeure < this.iDébut - window.oPlanning.oStructure.pas) {
console.log("La séance devient trop courte par la fin") ;
			liNouvelleHeure = this.iDébut - window.oPlanning.oStructure.pas ;
		}

// return liNouvelleHeure ;
		
		// La nouvelle heure ne doit pas dépasser les limites de la tranche
		var loTranche = this.getTranche() ;
		if (liNouvelleHeure > loTranche.iFin) liNouvelleHeure = loTranche.iFin ;
		if (liNouvelleHeure < loTranche.iDebut) liNouvelleHeure = loTranche.iDebut ;

		// La nouvelle heure ne doit pas dépasser les bornes réelles (???)
		var liKeyCode = eventKeyCode(event);
		if (liKeyCode % 2 == 1 && liNouvelleHeure > this.iHeureMaxReel) {
console.log("à quoi sert this.iHeureMaxReel ?") ;
			liNouvelleHeure = this.iHeureMaxReel;
		}
		if (liKeyCode % 2 == 1 && liNouvelleHeure < this.iHeureMinReel) {
console.log("à quoi sert this.iHeureMinReel ?") ;
			liNouvelleHeure = this.iHeureMinReel;
		}

		// La nouvelle heure ne doit pas faire entrer dans une zone interdite
		var loZone ;
		var laZones = window.oPlanning.oStructure.rtvCollZonesInterdites(this.sIdJour, this.sIdTranche, this.sIdRessource);
		for (var z = 0; z < laZones.length; z++) {
			loZone = laZones[z] ;
			if (this.iDebut < parseInt(loZone.iDebut) 
					&& liNouvelleHeure > parseInt(loZone.iDebut)){
				// Cette zone interdite est plus tard que cette séance
				// et la nouvelle heure souhaitée ferait entrer dedans
				liNouvelleHeure = loZone.iDebut ;
			}
			if (this.iFin > parseInt(loZone.iFin) 
					&& liNouvelleHeure < parseInt(loZone.iFin)) {
				// Cette zone interdite est plus tôt que cette séance
				// et la nouvelle heure souhaitée ferait entrer dedans
				liNouvelleHeure = loZone.iFin ;
			}
		}

// console.log("liNouvelleHeure = " + liNouvelleHeure + ", " + liNouvelleHeure / 60) ;
		
		// Arrondir
		liNouvelleHeure = arrondirHeure(liNouvelleHeure, window.oPlanning.oStructure.pas) ;
		
		return liNouvelleHeure ;

	},
			
	/**
	 *observeDeplacementPoignee
	 *Méthode de déplacement de la div pour la poignée en bas à droite
	 *Effectue le redimenssionement de la séance
	 *@param {Event} event - L'évenement mousemove qui déclenche cette méthode pendant toute la durée du redimensionnement
	 */
	observeDeplacementPoignee: function (event, pbPoignéeDébut) {
//		if (event.buttons == 0) {
		if (eventButtons(event) == 0) {
			// Aucun bouton de la souris n'est appuyé : on a raté le mouseUp
			// Terminer le déplacement en l'annulant
			console.log("Aucun bouton de la souris n'est appuyé, annulation du déplacement") ;
			this.finDeplacementPoignee(false) ;
			return ;
		}
	
		var liKeyCode = eventKeyCode(event);
		var loPoignée = (pbPoignéeDébut)?this.poigneeDebut:this.poigneeFin ;
		if (!loPoignée) {/*RAS*/
		} else if (liKeyCode % 2 == 1) this.poigneeDebut.style.backgroundColor = "blue"; // On est sur un nombre impair, la touche shift est activée
		else this.poigneeDebut.style.backgroundColor = this.couleurBordure;

		var loDiv = this.getDiv() ;
//		if (isPrototype()) var size = loDiv.getDimensions(); // dimensions actuelle de la div séance
//		else var size = {width: $(loDiv).width(), height: $(loDiv).height()}; // dimensions actuelle de la div séance
		var size = {width: loDiv.coordInitialesSouris.width
					, height: loDiv.coordInitialesSouris.height
					};
		
		// Stocker le déplacement dans une variable
		if (isPrototype()) {
			var loDéplacement = {
				x: (event.pointerX() - loDiv.coordInitialesSouris.x)
				, y: (event.pointerY() - loDiv.coordInitialesSouris.y)
			}; 
//popupWindow("event.pointerY() = " + event.pointerY() + "<br> loDiv.coordInitialesSouris.y = " + loDiv.coordInitialesSouris.y) ;
		} else {
			var loDéplacement = {
				x: (event.clientX - loDiv.coordInitialesSouris.x)
				, y: (event.clientY - loDiv.coordInitialesSouris.y)
			}; 
// popupWindow("event.clientY = " + event.clientY + "<br> loDiv.coordInitialesSouris.y = " + loDiv.coordInitialesSouris.y) ;
		}
		
		// Y at-til réellement déplacement ?
		if (this.vertical && loDéplacement.y == 0) return ;
		if (!this.vertical && loDéplacement.x == 0) return ;
		
		// Récupérer les attributs css qui modifient le calcul
// LG 20191001 old		var sizeAdjustSeance = getAjust(loDiv);
		var sizeAdjustSeance = 0 ;
		var liNouvellePosition, liNouvelleHeure ;

		if (this.vertical){
			// ----------------------------------------------------------------------------------------------------
			// Mode affichege vertical
//			var liTop = parseFloat(this.getDivTop()) ;
var liTop = loDiv.coordInitialesSouris.top ;
			
			// Déterminer la nouvelle position
			liNouvellePosition = liTop + parseInt(loDéplacement.y) ;
//popupWindow("liTop = " + liTop + "<br> = loDéplacement.y" + loDéplacement.y) ;
			if (!pbPoignéeDébut) liNouvellePosition += size.height ;
//popupWindow("liNouvellePosition = " + liNouvellePosition + "<br> = sizeAdjustSeance = " + sizeAdjustSeance + "<br> = liNouvellePositionCorrigée" + liNouvellePositionCorrigée) ;

			//calcul de la nouvelle heure
			liNouvelleHeure = this.deplacementPoignee_rtvHeureCorrigée(liNouvellePosition, pbPoignéeDébut, event) ;
		
			// Calcul de la nouvelle position, corrigée
			var liNouvellePositionCorrigée = this.getCoordDeMasqueParHeure(liNouvelleHeure);
// var liNouvellePositionCorrigée = liNouvellePosition ;
//popupWindow(this.getCoordDeMasqueParHeure(17 * 60)) ;
			
			var decimal = liNouvellePositionCorrigée % 1;
			liNouvellePositionCorrigée = liNouvellePositionCorrigée - decimal;
//popupWindow(loDéplacement.y + "<br>" + liNouvellePosition + "<br>" + liNouvellePositionCorrigée) ;
			loDéplacement.y = loDéplacement.y - (liNouvellePosition - liNouvellePositionCorrigée);
			if (pbPoignéeDébut) {
				// Poignée du début
				liNouvellePositionCorrigée = liTop + parseInt(loDéplacement.y) ;
				
				//Empéche la séance de "descendre"
				var liHeureLimite = this.iFin - window.oPlanning.oStructure.pas;
				var liPositionLimite = this.getCoordDeMasqueParHeure(liHeureLimite);
				if (liNouvellePositionCorrigée > liPositionLimite) liNouvellePositionCorrigée = liPositionLimite;
			}
			
			//calcul de la nouvelle hauteur
			var nouvelle_hauteur ;
			if (!pbPoignéeDébut) {
				// Poignée de la fin
				nouvelle_hauteur = size.height + loDéplacement.y - sizeAdjustSeance;
//				nouvelle_hauteur = size.height + loDéplacement.y ;
				if (isPrototype()) nouvelle_hauteur -= sizeAdjustSeance;
// popupWindow(size.height + "<br>" + loDéplacement.y + "<br>" + sizeAdjustSeance) ;
			} else {
				// Poignée du début
				var pos_bas = this.getCoordDeMasqueParHeure(this.iFin);
				nouvelle_hauteur = (pos_bas - liNouvellePositionCorrigée) - sizeAdjustSeance;
				loDiv.style.top = liNouvellePositionCorrigée + 'px' ;
			}
			if (nouvelle_hauteur > 0) loDiv.style.height = (nouvelle_hauteur + 1) + "px";

		} else {
			// ----------------------------------------------------------------------------------------------------
			// Mode affichege horizontal
//			var liLeft = parseFloat(this.getDivLeft()) ;
var liLeft = loDiv.coordInitialesSouris.left ;
			
			// Déterminer la nouvelle position
			liNouvellePosition = liLeft + parseInt(loDéplacement.x);
			if (!pbPoignéeDébut) liNouvellePosition += size.width ; // Poignée du début

			// Calcul de la nouvelle heure
			liNouvelleHeure = this.deplacementPoignee_rtvHeureCorrigée(liNouvellePosition, pbPoignéeDébut, event) ;
			// Nouvelle position
			var liNouvellePositionCorrigée = this.getCoordDeMasqueParHeure(liNouvelleHeure) ;
			decimal = liNouvellePositionCorrigée % 1;
			liNouvellePositionCorrigée = liNouvellePositionCorrigée - decimal;
			loDéplacement.x = loDéplacement.x - (liNouvellePosition - liNouvellePositionCorrigée);
			if (pbPoignéeDébut) {
				// Poignée de la fin
				liNouvellePositionCorrigée = liLeft + parseInt(loDéplacement.x) ;
				
				//Empéche la séance de "descendre"
				var liHeureLimite = this.iFin - window.oPlanning.oStructure.pas;
				var liPositionLimite = this.getCoordDeMasqueParHeure(liHeureLimite);
				if (liNouvellePositionCorrigée > liPositionLimite) liNouvellePositionCorrigée = liPositionLimite;
			}
			
			//calcul de la nouvelle largeur
			var nouvelle_largeur ;
			if (!pbPoignéeDébut) {
				// Poignée du début
				nouvelle_largeur = size.width + loDéplacement.x - sizeAdjustSeance ;
			} else {
				// Poignée de la fin
				var pos_right = this.getCoordDeMasqueParHeure(this.iFin);
				nouvelle_largeur = (pos_right - liNouvellePositionCorrigée) - sizeAdjustSeance ;
				loDiv.style.left = liNouvellePositionCorrigée + 'px';
			}
			if (nouvelle_largeur > 0) loDiv.style.width = (nouvelle_largeur + 1) + 'px';

		}
		
		// Redéfinir les coordonnées de la souris
//		loDiv.coordInitialesSouris = {
//			x: loDiv.coordInitialesSouris.x + loDéplacement.x,
//			y: loDiv.coordInitialesSouris.y + loDéplacement.y
//		};
//popupWindow(loDiv.coordInitialesSouris.x) ;			

		// Affichage de la nouvelle heure
		if (!pbPoignéeDébut) {
			// Poignée du début
			this.poigneeDebut.innerHTML = minToHeures(liNouvelleHeure);
			this.iFin = liNouvelleHeure;
		} else {
			// Poignée de la fin
			this.poigneeFin.innerHTML = minToHeures(liNouvelleHeure);
			this.iDebut = liNouvelleHeure;
		}
		
		return ;
	},
			
	/**
	 *observeDeplacementPoigneeFin
	 *Méthode de déplacement de la div pour la poignée en bas à droite
	 *Effectue le redimenssionement de la séance
	 *@param {Event} event - L'évenement mousemove qui déclenche cette méthode pendant toute la durée du redimensionnement
	 */
	observeDeplacementPoigneeFin: function (event) {
		var lvRetu = this.observeDeplacementPoignee(event, false) ;		
		return lvRetu ;
	},
	
	/**
	 *observeDeplacementPoigneeDebut
	 *Méthode de déplacement de la div pour la poignée en haut à gauche
	 *Effectue le redimenssionement de la séance
	 *@param {Event} event - L'évenement mousemove qui déclenche cette méthode pendant toute la durée du redimmenssionement
	 */
	observeDeplacementPoigneeDebut: function (event) {
		var lvRetu = this.observeDeplacementPoignee(event, true) ;		
		return lvRetu ;
	},
			
	/**
	 *finDeplacementPoignee
	 *Supprime les espions pour calculer le redimensionemment
	 *Indique que la séance est de nouveau draggable
	 *@param {Event} event - L'évenement mouseup (pas sur de la syntaxe) qui a déclenché l'espion qui appele cette méthode
	 */
	finDeplacementPoignee: function (event){
		window.oPlanning.oStructure.iFinDéplacementPoignée = now() ;

// removeListener
		if (isPrototype()) {
			Event.stopObserving(document.body, 'mousemove');
			Event.stopObserving(document.body, 'mouseup');
		} else {			
			$(document.body).unbind("mousemove");
			$(document.body).unbind("mouseup");
		}
		
		if (!event) {
			// Evénement non fourni, ou false
			// Annuler le déplacement
			this.reset() ;
		}
		
		var loDiv = this.getDiv() ;
		loDiv.coordInitialesSouris = {x: 0,y: 0};
		
		// Restaurer le menu contextuel qui a été inhibé pendant le drag
		this.setMenuContextuel(true) ;
		
		if (this.sIdSeance.substr(0, 2) != "S0"
				&& this.sIdSeance != "S0_Selection" 
				&& (this.tempIFin != this.iFin || this.tempIDebut != this.iDebut)
				) {
			// Déclancher l'événement de fin de déplacement
			window.oPlanning.oControleur.onFinDimesionnementSeance(this, event) ;
		}

	},
	
	// Ajouter/enlever la capacité d'afficher le menu contextuel
	setMenuContextuel: function(pbSet, poDiv){
		var loDiv = poDiv ;
		if (!loDiv) loDiv = this.getDiv() ;
		loDiv = document.getElementById("contenu-" + loDiv.id) ;
		var lsClass = "context-menu-seance" ;
		if (pbSet && !((' ' + loDiv.className + ' ').indexOf(' ' + lsClass + ' ') > -1)) {
			// La div n'a pas la classe dans sa liste
			// L'ajouter
			loDiv.className += " " + lsClass ;
		}
		if (!pbSet && ((' ' + loDiv.className + ' ').indexOf(' ' + lsClass + ' ') > -1)) {
			// La div a déja la classe dans sa liste
			// L'enlever
			loDiv.className = loDiv.className.replace(lsClass, "") ;
		}
		
	},
});
