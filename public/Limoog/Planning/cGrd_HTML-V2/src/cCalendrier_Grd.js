/**###############################################
 *      CLASSE Calendrier
 ###############################################*/

var cCalendrier = cSemaine.extend({
	cClassName: "cCalendrier",
	iNbJoursMax: 31,

	/**
	 *toString 
	 *@return {String} renvoit les informations de l'instance courante de la semaine 
	 */
	toString: function (){
		return "Calendrier : " + this.cLibelle ;
	},
	
	/**
	 */
	afficherStructure_OLD20190110: function (pbNoData) {

// LG 20190927 début
// Copié de cSemaine_GRD
		// Nettoyer les objets éventuellement préexistants
		if (document.getElementById('mainSemaine')){
			var loCtn = document.getElementById('mainSemaine');
			try {
				var loParent = loCtn.parentElement ;
				var liOldHeight = $(loParent).height() ;
				loParent.removeChild(loCtn);
				if ($(loParent).height() < liOldHeight / 2) {
					// La hauteur du container a réduit
					// Ca arrive dans certains contextes (????)
					// Il suffit alors d'ajouter un élément (même invisible) pour que la heuteur redevienne normale
					loParent.innerHTML = "<div id = 'PourCorrigerBugHauteurContainer' style='visibility:hidden;'/>";
				}
			} catch (e){
				// RAS
				console.log("Impossibe de supprimer mainSemaine") ;
			}
		}
		
		// Récupérer l'objet DOM dans lequel mettre le planning
		if (document.getElementById("Planning")) {
			var loConteneur = document.getElementById("Planning") ;
			loConteneur.style.zIndex = 0 ;	// Pour être cohérent avec le z-index des éléments contenus dans le planning
			loConteneur.style.padding = 0 ;	// Pour que le planning vienne bien collé sur tous les bords
// loConteneur.innerHTML = "" ;
			var loConteneurDim = loConteneur ;
		} else {
			var loConteneur = document.body ;
		}
		
		if (pbNoData) {
			// Masquer la structure
			loConteneur.style.display = "table" ;	// Pour le centrage vertical
			loConteneur.innerHTML = loConteneur.innerHTML
					+ '<div id="mainSemaine"'
						+ 'style = "'
							+ 'height: 100%;'
							+ 'display: table-cell; '	// Pour le centrage vertical
							+ 'vertical-align: middle;'	// Pour le centrage vertical
							+ '"'
						+ ">"
					+ '<Div style = "color:blue;'
						+ " text-align: center;"		// Pour le centrage horizontal
						+ '"'
						+ ' class=""'
						+ ">"
					+ 'Sélectionnez une ressource dans les listes, '
					+ "<br/><br/>"
					+ 'Puis cliquez ici pour charger le planning.'
					+ "</Div>"
					+ "<Center>"
					+ "</Center>"
					+ "</div>" ;
			
			// Mettre en place le gestionnaire d'événements sur clic
			$('#mainSemaine').on('click', function(e){
				var event = jQuery.Event("onClicSurMasquePlanning");
				$("body").trigger(event);
			}) ;
			return ;
		}
// LG 20190927 fin

// LG 20190927 début
// LG 20190927 fin
		
		// Créer l'objet structure
		window.oPlanning.oStructure = new cStructure() ;
		window.oPlanning.oStructure.lGèreMois = true ;
		window.oPlanning.oStructure.initProps(window.oPlanning.oData.oStructure) ;
		
		// Initialiser les propriétés internes
		this.initProps() ;
		
		// Calculer les dimensions
		this.calcDimJour() ;
		this.calcDimMois() ;
		
		// Dessiner la structure
		// Pour empêcher la sélection de texte dans IE<9 (sinon, c'est géré par le CSS de conteneur)
		var lsDisableSelect = (isIE() && getInternetExplorerVersion() < 9)?" unselectable='on' onselectstart='return false;'":"" ;
		var lsTop = (this.top)?("top : " + this.top + "px; "):"" ;
		var lsLeft = (this.left)?("left : " + this.left + "px; "):"" ;
		var lsDimConteneur, liHeightSemaine, liWidthSemaine
		var liHeightSemaine, liWidthSemaine, liMargeConteneurSuppl, liMargeJoursSuppl ;
		if (window.oPlanning.oData.getModeExecution() == window.oPlanning.oCst.ModeExecution_VFPLocal) {
			// Mode VFP
			liMargeConteneurSuppl = 0 ;
			liMargeJoursSuppl = 0 ;
		} else {
			// Mode navigateur
			liMargeConteneurSuppl = 24 ;
			liMargeJoursSuppl = 4 ;
		}

		lsDimConteneur = "width: " + (this.widthDemandée + (this.vertical?0:liMargeConteneurSuppl)) + "px; "
				+ "height: " + (this.heightDemandée + (this.vertical?liMargeConteneurSuppl:0)) + "px; "
				;
		
		var liDimEchelles = this.getDimEchelles() ;
		var lsDimContientJours, liHeightJours, liWidthJours ;
		liHeightJours = this.height ;
		liWidthJours = this.width ;
		if (!this.vertical) {
			lsDimContientJours = "height : 100%; "
						+ "left : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px; "
						+ "right : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px;" ;
			liWidthJours = liWidthJours - liDimEchelles * 2 
							- window.oPlanning.oCst.Marge * 4 ;
		} else {
			lsDimContientJours = "width : 100%; "
						+ "top : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px; "
						+ "bottom : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px;" ;
			liHeightJours = liHeightJours - liDimEchelles * 2 
							- window.oPlanning.oCst.Marge * 4 ;
		}

		if (document.getElementById('Conteneur')){
			var loCtn = document.getElementById('Conteneur');
			document.body.removeChild(loCtn);
		}
// LG 20190927 début
//		document.body.innerHTML = document.body.innerHTML
//				+ "<div id='Conteneur'"
//					+ " class='conteneur'"
		loConteneur.innerHTML = loConteneur.innerHTML
				+ "<div id='mainSemaine'"
					+ " class='mainSemaine'"
// LG 20190927 fin
					+ lsDisableSelect
					+ " style='"
					+ lsDimConteneur
					+ "position: absolute;"
					+ lsTop + lsLeft
					+ "overflow : auto;"
					+ "'"
					+ " onscroll=" + 'window.oPlanning.oGrille.onConteneurScrolled()'
					+ ">"
				+ "<div id='Mois' class='mois' "
					+ "style='"
					+ "position: absolute;"
					+ "height : " + liHeightJours + "px; "
					+ "width : " + liWidthJours + "px;"

					+ "'>"
				+ this.rtvHTMLMois()
				+ "</div>"
				+ "</div>";
	},
	afficherStructure: function (pbNoData) {

// LG 20190927 début
// Copié de cSemaine_GRD
		// Nettoyer les objets éventuellement préexistants
		if (document.getElementById('mainSemaine')){
			var loCtn = document.getElementById('mainSemaine');
			try {
				var loParent = loCtn.parentElement ;
				var liOldHeight = $(loParent).height() ;
				loParent.removeChild(loCtn);
				if ($(loParent).height() < liOldHeight / 2) {
					// La hauteur du container a réduit
					// Ca arrive dans certains contextes (????)
					// Il suffit alors d'ajouter un élément (même invisible) pour que la heuteur redevienne normale
					loParent.innerHTML = "<div id = 'PourCorrigerBugHauteurContainer' style='visibility:hidden;'/>";
				}
			} catch (e){
				// RAS
				console.log("Impossibe de supprimer mainSemaine") ;
			}
		}
		
		// Récupérer l'objet DOM dans lequel mettre le planning
		if (document.getElementById("Planning")) {
			var loConteneur = document.getElementById("Planning") ;
			loConteneur.style.zIndex = 0 ;	// Pour être cohérent avec le z-index des éléments contenus dans le planning
			loConteneur.style.padding = 0 ;	// Pour que le planning vienne bien collé sur tous les bords
// loConteneur.innerHTML = "" ;
			var loConteneurDim = loConteneur ;
		} else {
			var loConteneur = document.body ;
		}
		
		if (pbNoData) {
			// Masquer la structure
			loConteneur.style.display = "table" ;	// Pour le centrage vertical
			loConteneur.innerHTML = loConteneur.innerHTML
					+ '<div id="mainSemaine"'
						+ 'style = "'
							+ 'height: 100%;'
							+ 'display: table-cell; '	// Pour le centrage vertical
							+ 'vertical-align: middle;'	// Pour le centrage vertical
							+ '"'
						+ ">"
					+ '<Div style = "color:blue;'
						+ " text-align: center;"		// Pour le centrage horizontal
						+ '"'
						+ ' class=""'
						+ ">"
					+ 'Sélectionnez une ressource dans les listes, '
					+ "<br/><br/>"
					+ 'Puis cliquez ici pour charger le planning.'
					+ "</Div>"
					+ "<Center>"
					+ "</Center>"
					+ "</div>" ;
			
			// Mettre en place le gestionnaire d'événements sur clic
			$('#mainSemaine').on('click', function(e){
				var event = jQuery.Event("onClicSurMasquePlanning");
				$("body").trigger(event);
			}) ;
			return ;
		}
// LG 20190927 fin
		
		// Créer l'objet structure
		window.oPlanning.oStructure = new cStructure() ;
		window.oPlanning.oStructure.lGèreMois = true ;
		window.oPlanning.oStructure.initProps(window.oPlanning.oData.oStructure) ;
		
		// Initialiser les propriétés internes
// LG 20191001 old		this.initProps() ;
		this.initProps(loConteneurDim) ;
		
		// Calculer les dimensions
		this.calcDimJour() ;
		this.calcDimMois() ;
		
		// Dessiner la structure
		// Pour empêcher la sélection de texte dans IE<9 (sinon, c'est géré par le CSS de conteneur)
		var lsDisableSelect = (isIE() && getInternetExplorerVersion() < 9)?" unselectable='on' onselectstart='return false;'":"" ;
		var lsTop = (this.top)?("top : " + this.top + "px; "):"" ;
		var lsLeft = (this.left)?("left : " + this.left + "px; "):"" ;
// LG 20191001 deac		var lsDimConteneur, liHeightSemaine, liWidthSemaine
		var lsDimMainSemaine, liHeightSemaine, liWidthSemaine, liMargeConteneurSuppl, liMargeJoursSuppl ;
		if (window.oPlanning.oData.getModeExecution() == window.oPlanning.oCst.ModeExecution_VFPLocal) {
			// Mode VFP
			liMargeConteneurSuppl = 0 ;
			liMargeJoursSuppl = 0 ;
		} else {
			// Mode navigateur
			liMargeConteneurSuppl = 24 ;
			liMargeJoursSuppl = 4 ;
		}

// LG 20191001 début
//		lsDimConteneur = "width: " + (this.widthDemandée + (this.vertical?0:liMargeConteneurSuppl)) + "px; "
//				+ "height: " + (this.heightDemandée + (this.vertical?liMargeConteneurSuppl:0)) + "px; "
//				;
		if (document.getElementById("Planning")) {
			lsDimMainSemaine = "width: 100%; "
					+ "height: 100%; " ;
		} else {
			lsDimMainSemaine = "width: " + (this.widthDemandée + (this.vertical?0:liMargeConteneurSuppl)) + "px; "
					+ "height: " + (this.heightDemandée + (this.vertical?liMargeConteneurSuppl:0)) + "px; " ;
		}
// LG 20191001 fin
		
		var liDimEchelles = this.getDimEchelles() ;
// LG 20191001 début
// 		var lsDimContientJours, liHeightJours, liWidthJours ;
//		liHeightJours = this.height ;
//		liWidthJours = this.width ;
		var lsDimConteneur, liHeightJours, liWidthJours ;
		liHeightJours = this.heightInterieur + liMargeJoursSuppl ;
		liWidthJours = this.widthInterieur + liMargeJoursSuppl ;
		if (!this.vertical) {
			/*lsDimContientJours*/lsDimConteneur = "height : 100%; "
						+ "left : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px; "
						+ "right : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px;" ;
			liWidthJours = liWidthJours - liDimEchelles * 2 
							- window.oPlanning.oCst.Marge * 4 ;
		} else {
			/*lsDimContientJours*/lsDimConteneur = "width : 100%; "
						+ "top : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px; "
						+ "bottom : " + (liDimEchelles + window.oPlanning.oCst.Marge * 2) + "px;" ;
			liHeightJours = liHeightJours - liDimEchelles * 2 
							- window.oPlanning.oCst.Marge * 4 ;
		}

// LG 20191001 début
//		if (document.getElementById('Conteneur')){
//			var loCtn = document.getElementById('Conteneur');
//			document.body.removeChild(loCtn);
//		}
//		document.body.innerHTML = document.body.innerHTML
//				+ "<div id='Conteneur'"
//					+ " class='conteneur'"
//					+ lsDisableSelect
//					+ " style='"
//					+ lsDimConteneur
//					+ "position: absolute;"
//					+ lsTop + lsLeft
//					+ "overflow : auto;"
//					+ "'"
//					+ " onscroll=" + 'window.oPlanning.oGrille.onConteneurScrolled()'
//					+ ">"
//				+ "<div id='Mois' class='mois' "
//					+ "style='"
//					+ "position: absolute;"
//					+ "height : " + liHeightJours + "px; "
//					+ "width : " + liWidthJours + "px;"
//
//					+ "'>"
//				+ this.rtvHTMLMois()
//				+ "</div>"
//				+ "</div>";
liHeightJours -= 4 ;
liWidthJours -= 4 ;
		loConteneur.innerHTML = loConteneur.innerHTML
				+ "<div id='mainSemaine'"
					+ " class='mainSemaine'"
				  
					+ lsDisableSelect
					+ " style='"
						+ lsDimMainSemaine
						+ "position: absolute;"
						+ lsTop + lsLeft
						+ "border: 1px solid black ;"
// + "background-color: red;"
						+ "overflow : hidden;"
						+ "'"
					+ ">"
				+ this.rtvHTMLLegende('gauche')
				+ this.rtvHTMLLegende('centre')
				+ this.rtvHTMLLegende('droite')
				+ "<div id='Conteneur' class='conteneur' "
					+ "style='"
					+ "position: absolute;"
					+ "overflow : auto;"
					+ lsDimConteneur
					+ "'"
					+ " onscroll=" + 'window.oPlanning.oGrille.onConteneurScrolled()'
					+ ">"
				+ "<div id='Jours' class='jours' "
					+ "style='"
					+ "position: absolute;"
					+ "height : " + liHeightJours + "px; "
					+ "width : " + liWidthJours + "px;"

					+ "'>"
				+ this.rtvHTMLMois()
				+ "</div>"
				+ "</div>"
				+ "</div>";
// LG 20191001 fin
	},
	
	onConteneurScrolled: function (){
		var loDiv = document.getElementById("Conteneur") ;
// popupWindow("Scrolled : " + loDiv.scrollTop + ", " + loDiv.scrollLeft);
/*		var la = document.getElementsByClassName("colléEnHaut");
		for (i=0;i<la.length;i++) {
			if (this.vertical) {
//				if (!la[i].initialTop) la[i].initialTop = parseFloat(la[i].style.top) ;
//				la[i].style.top = (la[i].initialTop -loDiv.scrollTop) + "px" ;
				la[i].style.top = -loDiv.scrollTop + "px" ;
			} else {
				la[i].style.left = -loDiv.scrollLeft + "px" ;
			}
		}
*/		
		var la = document.getElementsByClassName("colléAGaucheDuCalendrier");
		for (i=0;i<la.length;i++) {
			if (!this.vertical) {
				if (!la[i].initialTop) la[i].initialTop = parseFloat(la[i].style.top) ;
				la[i].style.top = (la[i].initialTop + loDiv.scrollTop) + "px" ;
			} else {
				if (!la[i].initialLeft) la[i].initialLeft = parseFloat(la[i].style.left) ;
				la[i].style.left = (la[i].initialLeft + loDiv.scrollLeft) + "px" ;
			}
	   }		
		
	},
	
	// Dessiner les éléments variables : séances et zones interdites
	afficherData: function (){
//console.log("afficherData désactivée") ;
 		this.afficherZonesInterdites();
 		this.afficherSeances();
	},
	
	calcDimMois: function () {
		if (this.iDimMois)  {	
			// Déja déterminé
			return this.iDimMois ;
		}
		
		var liNbMois = window.oPlanning.oStructure.aMois.length ;
		var iTotalMarge = window.oPlanning.oCst.Marge * (liNbMois - 1);
		var liDimRéservée = (iTotalMarge + (this.getDimEchelles() + window.oPlanning.oCst.Marge * 2) * 2) ;

//		this.largeurJourMin = 100 ;
//		this.hauteurJourMin = 100 ;
		
		if (!this.vertical) {
			// Horaires horizontaux -> jours empilés -> mois côte-à-côte
			var liDim = parseFloat(this.width - liDimRéservée) / (liNbMois);
			liDim = Math.max(parseFloat(liDim), window.oPlanning.oCst.largeurMoisMin) ;
			
			// Recalculer la dimension totale
			this.width = liDim * liNbMois + liDimRéservée ;
			var liHeightMin = (window.oPlanning.oCst.hauteurJourMin - window.oPlanning.oCst.Marge) * 31 ;
			if (this.height < liHeightMin) this.height = liHeightMin ;

		} else {
			// Horaires verticaux -> jours côte-à-côte -> mois empilés
			var liDim = parseFloat(this.height - liDimRéservée) / (liNbMois) ;
			liDim = Math.max(parseFloat(liDim), window.oPlanning.oCst.hauteurMoisMin);
			
			// Recalculer la dimension totale
			this.height = liDim * liNbMois + liDimRéservée ;
			var liWidthMin = (window.oPlanning.oCst.largeurJourMin - window.oPlanning.oCst.Marge) * 31 ;
			if (this.width < liWidthMin) this.width = liWidthMin ;
		}
		liDim = parseFloat(liDim) ;
		this.iDimMois = liDim ;
		return liDim ;
	},
	
	rtvHTMLMois: function () {
		var lsHTML = '';
		var lsHTMLMois, loMois, lsIdDivJour, lsLibelle ;
		
		var liDimMois = this.calcDimMois() ;
		var liPosition = window.oPlanning.oCst.Marge 
							+ window.oPlanning.oCst.DimBordureMois ;

		// Puis on parcourt des jours 
		for (var i = 0; i < window.oPlanning.oStructure.aMois.length; i++) {
			loMois = window.oPlanning.oStructure.aMois[i];
			loMois = new cMois(loMois) ;
			loMois.width = this.width ;
			loMois.height = this.height ;
			
			lsHTMLMois = loMois.rtvHTML(liPosition, liDimMois) ;
			lsHTML += lsHTMLMois ;
			
			// Incrémentation des positions
			liPosition = parseFloat(liPosition) 
							+ parseFloat(liDimMois) 
							+ window.oPlanning.oCst.DimBordureMois 
							+ window.oPlanning.oCst.Marge ;
		}
		
		return lsHTML ;
	},

	bidonPourFinDeClasse:0
});

