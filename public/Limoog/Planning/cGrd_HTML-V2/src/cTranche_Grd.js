/**###############################################
 *     CLASSE tranche
 *      
 *     @param {Array} aParatranche - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
 *     
 *     Ce tableau de paramètres contient les paramètres suivants :  
 *     @param {String} sIdTranche - Identifiant de la tranche
 *     @param {String} cLibelle - Nom complet de la Semaine
 *     @param {String} cCouleur - Couleur d'affichage de la journée
 *     @param {Number} iHeureDebut - Heure (en minutes) de début de l'affichage de la tranche
 *     @param {Number} iHeureFin - Heure (en minutes) de fin de l'affichage de la tranche
 ###############################################*/
//var tranche = Class.create({
//    initialize: function (aParaTranche, psIdJour){
var cTranche = Class.extend({
    init: function (aParaTranche, psIdJour){
        if (aParaTranche){
			this.sIdJour = psIdJour ;
			this.sIdTranche = aParaTranche.sIdTranche ;
			this.cLibelle = aParaTranche.cLibelle;
			this.cCouleur = aParaTranche.cCouleur;
			this.iDebut = aParaTranche.iDebut;
			this.iFin = aParaTranche.iFin;
			this.aRessources = new Array();
			this.dDate = aParaTranche.dDate ;
			this.vertical = aParaTranche.vertical ;
		}
    },
    getIdDiv: function () {
		var lsId = window.oPlanning.oStructure.getIdDivTranche(this.sIdTranche, this.sIdJour)
        return lsId ;
    },
    getDiv: function () {
        return document.getElementById(this.getIdDiv()) ;
    },
    /**
     *toString 
     *@return {String} renvoi les informations de l'instance courante de la tranche
     *
     */
    toString: function () {
        return "tranche : " + this.cLibelle + ", ID : " + this.sIdTranche;
    },
    /**
     * ajouteRessource
     * Permet d'ajouter une ressource à une seule tranche (contrairemant à ajouteRessource de la classe Semaine)
     * Cette méthode ne devrait pas être utilisée. 
     *
     * @param {Array} aParaRes - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
     * @see voir la classe ressource pour la documentation du tableau de paramètres
     * @return {Objet ressource} maRessource - Renvoi l'instance de la journée créée
     */
    ajouteRessource: function (aParaRes) {
// LG 20160826 old        var laRessource = new ressource(aParaRes, this.sIdDivJour, this.sIdDivTranche, this.dDate);
        aParaRes.vertical = this.vertical ;
        aParaRes.cCouleur = this.cCouleur ;
		var laRessource = new cRessource(aParaRes, this.sIdJour, this.sIdTranche, this.dDate);
        this.aRessources.push(laRessource);
        return laRessource;
    },
	
	// Renvoyer le code HTML correspondant à la DIV de cette tranche pour cette journée
    rtvHTMLInterTranche: function (piPosition) {
		
		var lcStyleHeight, lcStyleWidth
		if (this.vertical) {
			lcStyleWidth = "100%";
			lcStyleHeight = parseInt(window.oPlanning.oStructure.tailleHorsLimite - window.oPlanning.oCst.DimBordureTranches * 2) + "px";
		} else {
			lcStyleWidth = parseInt(window.oPlanning.oStructure.tailleHorsLimite - window.oPlanning.oCst.DimBordureTranches * 2) + "px";
			lcStyleHeight = "100%";
		}

		var lsHTML = "<div id='hl" + this.getIdDiv() + "' "
					+ "class= interTranche "
					+ "style='position : absolute; "
						+ (this.vertical?"top":"left") + " : " + piPosition + "px; "
						+ "width : " + lcStyleWidth + "; "
						+ "height: " + lcStyleHeight + ";"
// + "background-color: blue; "
					+ "'"
					+ "></div>";
		
		return lsHTML ;
    },
	
	// Renvoyer le code HTML correspondant à la DIV de cette tranche pour cette journée
    rtvHTML: function (piPosition, piDimensionTranche, piDimensionJour, psIdDiv
						, pbSansRessourcesNiCouleurs
						, pbAvecTraitsLégende, pbAvecHeureLégende
						, psIdJour) {

		this.iDimension = piDimensionTranche ;
						
		var lsWidth, lsHeight, lsTop, lsLeft ;
		if (this.vertical){
			lsWidth = "100%" ;
			lsHeight = (piDimensionTranche + 1) + "px" ;
			lsTop = piPosition + "px" ;
			lsLeft = "0" ;
		} else {
			lsWidth = (piDimensionTranche + 1) + "px" ;
			lsHeight = "100%" ;
			lsTop = "0" ;
			lsLeft = piPosition + "px" ;
		}
		
		var lsIdDiv = psIdDiv?psIdDiv:this.getIdDiv() ;
		var lsCouleur = pbSansRessourcesNiCouleurs?"":("background-color :" + this.cCouleur + "; ") ;
		var lsHTML =
				"<div "
				+ "class='tranche' "
				+ "id='" + lsIdDiv + "' "
				+ "style='"
					+ "position: absolute; "
// + "background-color :red; "
+ lsCouleur
					
					+ "height: " + lsHeight + "; "
					+ "width: " + lsWidth + "; "
					+ "top: " + lsTop + "; "
					+ "left: " + lsLeft + "; "
				+ "'"
				+ ">" ;
		
		var lsHTMLRessources = "" ;
		if (!pbSansRessourcesNiCouleurs) {
			lsHTMLRessources = this.rtvHTMLRessources(piDimensionTranche
															, piDimensionJour
															);
		}
		
		var lsHTMLLégende = "" ;
 		if (pbAvecTraitsLégende || pbAvecHeureLégende) {
			lsHTMLLégende = this.rtvHTMLHeuresLégendes(psIdJour/*piDimensionTranche*/
															, piDimensionJour
															, psIdDiv, pbAvecTraitsLégende, pbAvecHeureLégende) ;
		}

		lsHTML +=
				lsHTMLRessources
				+ lsHTMLLégende
				+ "</div>"
				;
		
        return lsHTML ;
    },

	/* rtvHTMLRessources
	 * Méthode Appelée par Affichertranche, qui renvoit le code HTML correspondant à l'ensemble des ressources d'une journée.
	 * piHauteurDisponible		: hauteur disponible dans la tranche parente (sera utilisée par toutes les ressources pour cette tranche)
	 * piLargeurDisponible		: largeur disponible dans la tranche parente (à partager entre les ressources)
	 * @return {String} cCodeHTMLRessources - Renvoit le code HTML des ressources
	 */
	rtvHTMLRessources: function (piHauteurDisponible
								, piLargeurDisponible) {
								
		var liDimension1, liDimension2 ;
		var border = window.oPlanning.oCst.DimBordureRessources /*1*/ ;
		var iLargeurRessource, iHauteurRessource ;
		var loObjRessource ;
		var liBorderParent = window.oPlanning.oCst.DimBordureTranches ;
		var lsIdDivJour = this.sIdJour ;
		var liNbRessources = window.oPlanning.oStructure.getNbRessources() ;
		
		// Parcourir ttes les ressources
		var lsHTML = '';
		var liPosition = 0;
		var liIncrémentPosition ;
		
		liDimension2 = piHauteurDisponible - border ;
//		if (this.vertical) {
//			liDimension1 = ((piLargeurDisponible) / (liNbRessources)) - border * 2;
//		} else {
//			liDimension1 = ((piLargeurDisponible) / (liNbRessources)) - border * 2;
//		}
		liDimension1 = ((piLargeurDisponible + border) / (liNbRessources)) - border * 2;
//		liDimension1 = parseInt(liDimension1) ;
		liIncrémentPosition = liDimension1 + border * 2 ;
		
		for (i = 0; i < this.aRessources.length; i++) {
			loObjRessource = this.aRessources[i];

			lsHTML += loObjRessource.rtvHTML(liPosition, liDimension1, liDimension2, (i == 0)) ;
			lsHTML += "</div>" ;
			
			liPosition = liPosition + liIncrémentPosition ;
			liPosition = liPosition - border ;		// Décrémenter de 1px : les ressources se chevauchent sur l'épaisseur du trait

		}
		return lsHTML ;
	},

	rtvHTMLHeuresLégendes: function (legende, toto, psIdDiv, pbAvecTraitsLégende, pbAvecHeureLégende){

		var codeHeureHTML = "";
		var div = document.getElementById(psIdDiv) ;
		var left, top, bordure, chaine ;
		var llTraceTrait, llTraceHeure, liHeure;
		var lcHeure, lcBorderLeft, lcBorderTop, lcPosition, lcTextAlign, lcWidth, lcHeight ;

		var liPas = window.oPlanning.oStructure.pasLegende ;
		var liDébut = this.iDebut ;
 		liDébut = Math.ceil(this.iDebut / liPas, 0) * liPas;
		var nombreHeuresTranche = Math.ceil((this.iFin - liDébut) / liPas); // Math.ceil --> entier supérieur

		for (var i = 0; i < nombreHeuresTranche; i++) {
			liHeure = Math.min(this.iFin, liDébut + (liPas * i));
			
			llTraceTrait = pbAvecTraitsLégende && ((liDébut > this.iDebut) || legende == "centre") ;
			llTraceHeure = pbAvecHeureLégende ;
			
			bordure = ((liHeure % (liPas * 2) == 0) ? "1px solid black" : "1px dashed black")
			
			if (this.vertical) {
				top = this.getCoordParHeure(liHeure);
				left = 0 ;
				lcBorder = "border-top:" + bordure ;
				lcTextAlign = "center" ;
				lcWidth = "100%" ;
				lcHeight = "0" ;
			} else {
				top = 0 ;
				left = this.getCoordParHeure(liHeure);
				lcBorder = "border-left:" + bordure ;
				lcTextAlign = "left" ;
				lcWidth = "0" ;
				lcHeight = "100%" ;
			}
			if (llTraceTrait){
				// Tracé des traits de légende
				codeHeureHTML = codeHeureHTML + "<div "
						+ "id = 'idDiv" + legende + i + "'"
						+ " class = 'traitLegende'"
						+ " style = '"
//							+ "position: absolute; "
							+ lcBorder + "; "
							+ "top: " + top + "px; "
							+ "left: " + left + "px; "
							+ "width: " + lcWidth + "; "
							+ "height: " + lcHeight + ";"
							+ "'"
						+ ">"
						+ "</div>";
			}
			if (llTraceHeure) {
				// Tracé des textes indiquant les heures
				lcHeure = minToHeures(liHeure)
				if (this.vertical) {
					lcHeure = lcHeure.replace('00', '')
					lcHeure = this.formatageHeure(lcHeure, false);
				} else {
					if (liHeure < 10 * 60) left = parseInt(left + 6) /*décaler car il n'y a pas de "1" au début de l'heure*/;
					lcHeure = this.formatageHeure(lcHeure, true);
				}
				top = Math.max(0, parseInt(top - 13)) ;
				left = Math.max(0, parseInt(left - 14)) ;
				codeHeureHTML = codeHeureHTML + "<div "
						+ "id = 'idDiv" + legende + i + "'"
						+ " class = 'texteLegende'"
						+ " style = '"
//							+ "position : absolute; "
							+ "text-align : " + lcTextAlign + "; "
							+ "font-size : 10px; "	// Nécéssaire car détermine la hauteur, et donc le décalage qu'il faut dinner au top/left
							+ "left: " + left + "px; "
							+ "top: " + top + "px; "
//							+ "width : 100%; "
//							+ "height: 14px;"
//							+ "filter: alpha(opacity=100);"		// pour IE en mode calendrier
							+ "'"
						+ ">"
						+ lcHeure
						+ "</div>";
			}
		}
		return codeHeureHTML ;
	},


	/**
	 *getCoordParHeure  
	 *retourne le top/left en fonction de l'heure
	 *@param {Number} piHeure - heure de début de la séance, en minutes
	 *@return {Number} top/left - Renvoit la position en pixel du haut/gauche de la séance
	 */
	getCoordParHeure: function (piHeure) {
		
		if (!this.iDimension) {
			console.log("tranche.getCoordParHeure a été appellée avant le calcul de iDimension (qui se fait dans tranche.rtvHTML") ;
			return 0 ;
		}
		
		if (piHeure > this.iFin) piHeure = this.iFin;
		if (piHeure < this.iDebut) piHeure = this.iDebut;

		var ratio = (piHeure - this.iDebut) / ((this.iFin - this.iDebut)) ;
		var liCoord ;
		if (this.vertical) {
			var top = ratio * (this.iDimension);
			top = top ;
			liCoord = top;
		} else {
			var left = ratio * (this.iDimension);
			left = left - 2 ;
			liCoord = left;
		}
		
		return liCoord ;
	},
	
	/**
	 *getHeureParCoord 
	 *retourne l'heure en fonction des coordonnées
	 *@param {Number} piPosition - Position du haut ou bas de la séance, en pixel
	 *@param {boolean} pbArrondir - true pour obtenir un arrondi à window.oPlanning.oGrille.pas
	 */
	getHeureParCoord: function (piPosition, pbArrondir) {

		var liHeure ;
		if (!this.iDimension) {
			console.log("tranche.getHeureParCoord a été appellée avant le calcul de iDimension (qui se fait dans tranche.rtvHTML") ;
			return 0 ;
		}

		var loThisDiv = this.getDiv() ;
/*
// Pour déogage
var liTmp1, liTmp2 ;
liTmp1 = getDivBottom(loThisDiv) ;
liTmp2 = getDivTop(loThisDiv) ;
if (!(piPosition === liTmp1) && !(piPosition === liTmp2)) {
	console.log("Début : " + liTmp1 + "/" + this.getHeureParCoord(liTmp1) / 60 + "/" + this.iDebut / 60) ;
	console.log("Fin : " + liTmp2 + "/" + this.getHeureParCoord(liTmp2) / 60 + "/" + this.iFin / 60) ;
}
*/
		// Si on est sur une deuxième tranche horaire, il n'y a pas besoin de tenir compte des marges
		var iMargeTop = 0 ;
		if (this.sIdTranche != 0) {
			iMargeTop = 4; // dépend des bordure, à calculer
		}
		var top, ratio;
		if (this.vertical) {
			top = piPosition - iMargeTop;
			ratio = top / (loThisDiv.clientHeight - iMargeTop);
		} else {
			top = piPosition - iMargeTop;
			ratio = top / (loThisDiv.clientWidth - iMargeTop);
		}
		
		liHeure = this.iDebut + (ratio * (this.iFin - this.iDebut));
		
		if (pbArrondir) {
			// Arrondir l'heure à Xmin près
//			var pas = window.oPlanning.oStructure.pas // valeur actuelle du Pas
//			liHeure = Math.round(parseInt(liHeure) / pas) * pas;
			liHeure = arrondirHeure(liHeure, window.oPlanning.oStructure.pas) ;
		} ;
			
		if (liHeure > this.iFin) liHeure = this.iFin;
		if (liHeure < this.iDebut) liHeure = this.iDebut;
		
		return liHeure;

	},

	getPositionSéance: function(poDivRessource, piDébut, piFin, piBordure, piNbChevauchements, piRangChevauchement) {
		
		if (!piNbChevauchements) piNbChevauchements = 1 ;
		if (!piRangChevauchement) piRangChevauchement = 1 ;
		var liWidth, liLeft, liRight, liHeight, liTop, liBottom ;
/*
		if (window.oPlanning.oGrille.vertical) {
			var liWidthDispo = Math.max(5, parseFloat(poDivRessource.style.width) - piBordure * 2) ;
			if (piNbChevauchements != false) {
				liWidth = liWidthDispo / (piNbChevauchements);
				liLeft = ((piRangChevauchement - 1) * liWidth);
			} else {
				liWidth = liWidthDispo ;
				liLeft = 0 ;
			}
			liTop = this.getCoordParHeure(piDébut) - window.oPlanning.oCst.DimBordureRessources ;
			liBottom = this.getCoordParHeure(piFin) - window.oPlanning.oCst.DimBordureRessources ;
			liHeight = liBottom - liTop ;
			liHeight = liHeight - piBordure * 2 + 1 ;
			liTop = liTop + 1 ;
		} else {
			var liHeightDispo = Math.max(5, parseFloat(poDivRessource.style.height) - piBordure * 2) ;
			if (piNbChevauchements != false) {
				liHeight = liHeightDispo / (piNbChevauchements);
				liTop = (piRangChevauchement - 1) * liHeight;
			} else {
				liHeight = liHeightDispo;
				liTop = 0 ;
			}
			liLeft = this.getCoordParHeure(piDébut) - window.oPlanning.oCst.DimBordureRessources ;
			liRight = this.getCoordParHeure(piFin) - window.oPlanning.oCst.DimBordureRessources ;
			liWidth = liRight - liLeft;
			liWidth = liWidth - piBordure * 2 + 1 ;
			liLeft = liLeft + 1 ;
		}
*/
		if (window.oPlanning.oGrille.vertical) {
			var liWidthDispo = Math.max(5, parseFloat(poDivRessource.style.width) - parseFloat(poDivRessource.style.borderWidth) * 2 /*- piBordure * 2*/) ;
			if (piNbChevauchements != false) {
				liWidth = liWidthDispo / (piNbChevauchements);
				liLeft = ((piRangChevauchement - 1) * liWidth);
			} else {
				liWidth = liWidthDispo ;
				liLeft = 0 ;
			}
			liTop = this.getCoordParHeure(piDébut) ;
			liBottom = this.getCoordParHeure(piFin) ;
			liHeight = liBottom - liTop ;
		} else {
			var liHeightDispo = Math.max(5, parseFloat(poDivRessource.style.height) - parseFloat(poDivRessource.style.borderWidth) * 2 /*- piBordure * 2*/) ;
			if (piNbChevauchements != false) {
				liHeight = liHeightDispo / (piNbChevauchements);
				liTop = (piRangChevauchement - 1) * liHeight;
			} else {
				liHeight = liHeightDispo;
				liTop = 0 ;
			}
			liLeft = this.getCoordParHeure(piDébut) ;
			liRight = this.getCoordParHeure(piFin) ;
			liWidth = liRight - liLeft;
		}

		var pos = {} ;
		pos.iWidth = liWidth ;
		pos.iLeft = liLeft ;
		pos.iRight = liRight ;
		pos.iHeight = liHeight ;
		pos.iTop = liTop ;
		pos.iBottom = liBottom ;
		
		return pos ;
	}, 
	
	// Cette fonction permet de formater l'heure en mettant un espace entre l'heure et les minutes, puis en rajoutant h entre l'heure et les minutes.
	formatageHeure: function (chaine, needEspace) {
		var entreHeureMinute = "";
		if (needEspace)
			entreHeureMinute = " ";
		var aHeure = chaine.split('h');
//		return "<span style='font-size : 12px; font-weight:bold'>" + aHeure[0] + "</span>"
//				+ entreHeureMinute + "<span style='font-size : 8px;'>" + aHeure[1] + "</span>";
		return "<span class = 'lblHeure' >" + aHeure[0] + "</span>"
				+ entreHeureMinute + "<span class = 'lblMinute''>" + aHeure[1] + "</span>";
	},

});
