/* 
 * Classe de vue d'affichage des plannings
 */

// Classe selon John Resig
var cVue = cVueBase.extend({
    cClassName: "cVue",
    charge: function (poData) {
		
		if (isIE() && getInternetExplorerVersion() < 8){
			// IE est en version < 8 : avertir
			var lsMsg = "Attention, votre navigateur est trop ancien pour que le planning s'affiche correctement.<br> Il vous faut disposer de Internet Explorer en version 8 au moins, ou alors de FireFox ou de Chrome." ;
			popupWindow(lsMsg, true);
		}
		
		// Création de l'objet semaine
		if (window.oPlanning.oData.oStructure.oConfig.cStyle && window.oPlanning.oData.oStructure.oConfig.cStyle == window.oPlanning.oCst.eStyles.Calendrier) {
			// La configuration demande le mode calendrier
			window.oPlanning.oGrille = new cCalendrier();
		} else {
			// Toute autre configuration : mode semaine
			window.oPlanning.oGrille = new cSemaine();
		}
		
		// Création des éléments du DOM : structure de la semaine
		window.oPlanning.oGrille.afficherStructure() ;
		
		// Création des éléments du DOM : séances
		window.oPlanning.oGrille.afficherData() ;

		// Ajuster le texte des séances
		this.ajusterTaillePolices() ;
		
		return true ;
	},
	
	// ---------------------------------------------------------------------------------------
	masqueGrille: function() {
		window.oPlanning.oSemaine = new cSemaine() ;
		window.oPlanning.oSemaine.masquerStructure() ;
	},

	// ------------------------------------------------------------------------------------
	//ATTENTION : le centrage est très lent sous IE
	ajusterTaillePolices: function (etendue, parentBoxClass) {
		if (!window.oPlanning.oStructure.oConfig.lAjustePolice) {
			// Pas d'ajustement (très lent sous IE quand beaucoup de cases)
			return ;
		}
		
		var date = new Date();
		var parentBoxClass = parentBoxClass?parentBoxClass:'seance' ;
		etendue = etendue?etendue:".contenuSéance" ;
		var liFontSizeMin = window.oPlanning.oStructure.oConfig.iTaillePoliceSeancesMin ? window.oPlanning.oStructure.oConfig.iTaillePoliceSeancesMin : 8 ;
		var liFontSizeDft = window.oPlanning.oStructure.oConfig.iTaillePoliceSeancesDft ;
		var liMinFontSizeBeforeLineBreakReset = Math.min(liFontSizeDft, liFontSizeMin + 2) ;
		var lbAllowResetLineBreak = true ;

		var options = {minFontSize: liFontSizeMin
						, decrementBy: 1
						, allowResetLineBreak: lbAllowResetLineBreak	// Accepte-t-on d'enlever les sauts de ligne pour un meilleur ajustement ?
						, minFontSizeBeforeLineBreakReset: liMinFontSizeBeforeLineBreakReset	// Si on accepte d'enlever les sauts de ligne, taille mini de police avant d'essayer
						, parentBoxClass: parentBoxClass				// Si fourni, le nom de la classe CSS de la div qui est la box dans laquelle on doit ajuster le texte
																		// Si non fourni, on prend le premier parent
						};
		if (window.oPlanning.oData.oActuel.aSeances.length > 50) {
			// Beaucoup de séances : préférer l'ajustement simple
			$(etendue).ajusterTaillePolice_Simple(options);
		} else {
			// Assez peu de séances : on peut se permettre l'ajustement complet
			$(etendue).ajusterTaillePolice_Exact(options);
		}
		var ms = (new Date()) - date ;
		if (ms > 2000) {
			var msg = "Attention, la durée de l'ajustement de la taille de police semble ralentir l'affichage du planning : " 
							+ parseFloat(ms/1000).toFixed(2) + " secondes sont nécessaires pour ce traitement." ;
			popupWindow(msg, true) ;
		}
	},

	// ------------------------------------------------------------------------------------
	effaceSeance: function (poSeance) {
		return window.oPlanning.oStructure.effaceSeance(poSeance) ;
	},

	// ------------------------------------------------------------------------------------
	insereSeance: function (poSeance) {
		return window.oPlanning.oStructure.insereSeance(poSeance) ;
	},

	selectAvecObjet: function (poParaSeance, bInformeVFP) {
		return window.oPlanning.oStructure.selectAvecObjet(poParaSeance, bInformeVFP) ;
	},

	
	// ------------------------------------------------------------------------------------
	// Renvoyer l'objet tranche dont on passe l'Id
	// psIdDivTranche : Id de tranche ou div tranche
	// AC 20130110 Début 
	trouveTranche: function (psIdDivTranche) {
/*		if (psIdDivTranche.id) {
			// On a fourni une div : récupérer son Id
			psIdDivTranche = psIdDivTranche.id ;
		}
		var loTranche = null;
		for (var i = 0; i < window.oPlanning.oStructure.aTranchesJours.length; i++) {
			if (window.oPlanning.oStructure.aTranchesJours[i].getIdDiv() == psIdDivTranche) {
				loTranche = window.oPlanning.oStructure.aTranchesJours[i];
				return loTranche;
			}
		}
		// si on arrive là, on n'a pas trouvé la tranche
		console.log("...trouveTranche : tranche N°" + psIdDivTranche + " non trouvée", "red");
		return loTranche;
*/
console.log("Obsolète : utiliser window.oPlanning.oStructure.getTrancheAvecDiv") ;
		return window.oPlanning.oStructure.getTrancheAvecDiv(psIdDivTranche) ;
	},
	
	// Formater une ressource pour faire apparaitre le DragOver d'une séance
    //(poDivRessource)	: élément DOM ressource survolé à formatter
	//					: non fourni ou Null pour ne formatter aucune ressource
	//(poSéance)		: instance de cSeance actuellement dragguée
	//					: non fourni ou Null pour ne formatter aucune ressource
	oDivRessourceDragOver: null,
	formateDivRessourcePourDragOver: function (poDivRessource, poSéance) {
	
		// A-t-on changé de ressource
		if (!this.oDivRessourceDragOver) {
			// Pas de ressource survolée auparavant
			
		} else if (poDivRessource && poDivRessource.id == this.oDivRessourceDragOver.id) {
			// Pas de changement
			return true ;
		} else {
			// Changement de ressource survolée : déformatter la ressource précédente
// popupWindow(this.cClassName + ".formateDivRessourcePourDragOver : off") ;
			var liDiffBorderWidth = parseInt(this.oDivRessourceDragOver.borderWidthSaved) - parseInt(this.oDivRessourceDragOver.style.borderWidth) ;

			this.oDivRessourceDragOver.style.borderColor = this.oDivRessourceDragOver.borderColorSaved ;
			this.oDivRessourceDragOver.style.borderWidth = this.oDivRessourceDragOver.borderWidthSaved ;
			this.oDivRessourceDragOver.style.height = this.oDivRessourceDragOver.heightSaved ;
			this.oDivRessourceDragOver.style.width = this.oDivRessourceDragOver.widthSaved ;
			
			this.décaleContenuDivRessource(this.oDivRessourceDragOver, liDiffBorderWidth) ;
		}
		
		if (poDivRessource) {
			// Formatter la nouvelle ressource survolée
// popupWindow(this.cClassName + ".formateDivRessourcePourDragOver : on") ;
			var lsIdResSource = window.oPlanning.oStructure.getIdRessourceAvecDiv(poSéance.getIdDivRessource()) ;
			var lsIdResDest = window.oPlanning.oStructure.getIdRessourceAvecDiv(poDivRessource.id) ;
			var lsColor = (lsIdResSource == lsIdResDest)?"black":"blue" ;

			poDivRessource.borderColorSaved = poDivRessource.style.borderColor ;
			poDivRessource.style.borderColor = lsColor ;
			poDivRessource.borderWidthSaved = poDivRessource.style.borderWidth ;
// LG 20161013 : on ne met plus la bordure grasse car ca décale la séance
			poDivRessource.style.borderWidth = window.oPlanning.oCst.DimBordureSéanceSélectionnée + "px" ;
			var liDiffBorderWidth = parseInt(poDivRessource.style.borderWidth) - parseInt(poDivRessource.borderWidthSaved) ;

			poDivRessource.heightSaved = poDivRessource.style.height ;
			poDivRessource.style.height = (parseInt(poDivRessource.style.height) - liDiffBorderWidth) + "px" ;
			poDivRessource.widthSaved = poDivRessource.style.width ;
			poDivRessource.style.width = (parseInt(poDivRessource.style.width) - liDiffBorderWidth) + "px" ;
			
			this.décaleContenuDivRessource(poDivRessource, liDiffBorderWidth) ;
		}
		this.oDivRessourceDragOver = poDivRessource ;
		
		return true ;
		
    },

	// Décaler le contenu d'une DIV, pour répercuter le changement de dimension de bordure
	décaleContenuDivRessource: function(poDiv, piDiffBorderWidth) {
		if (!piDiffBorderWidth) return ;
		var loDiv
		for (var i = 0; i < poDiv.childNodes.length ; i++) {
			loDiv = poDiv.childNodes[i] ;
			if (window.oPlanning.oStructure.vertical) {
				loDiv.style.top = (parseFloat(loDiv.style.top) - piDiffBorderWidth) + "px" ;
				loDiv.style.width = (parseFloat(loDiv.style.width) - piDiffBorderWidth) + "px" ;
			} else {
				loDiv.style.left = (parseFloat(loDiv.style.left) - piDiffBorderWidth) + "px" ;
				loDiv.style.height = (parseFloat(loDiv.style.height) - piDiffBorderWidth) + "px" ;
			}
		}
    },

	// Retrouver les positions x et y de la div fournie dans la div "Conteneur"
	getAbsolutePosInConteneur: function(poDiv) {
		var pos, loParent ;
		pos = {x:0, y:0} ;
		if (!poDiv) {
//			console.log("poDiv est null") ;
			return pos ;
		}
		
		pos.x = parseFloat(poDiv.style.left) ;
		pos.y = parseFloat(poDiv.style.top) ;
		loParent = poDiv.parentNode ;
		while (loParent.id !== "Conteneur") {
			if (loParent.style.left) pos.x += parseFloat(loParent.style.left) ;
			if (loParent.style.top) pos.y += parseFloat(loParent.style.top) ;
			loParent = loParent.parentNode ;
		}
		
		return pos ;
	},
	
    setDisabled: function (plEnable) {
		var lsIdDivAttenuateur = "masqueAttenuateur" ;
// LG 20171212 old		var loDivConteneur = document.getElementById("Conteneur") ;
		var loDivConteneur = document.getElementById("Jours") ;
        if (plEnable){}
		else if (document.getElementById(lsIdDivAttenuateur)) {}
		else {
			// Créer la div atténuateur
			var loDivAttenuateur = document.createElement("div") ;
			loDivAttenuateur.id = lsIdDivAttenuateur ;
			loDivAttenuateur.className = "masqueAttenuateur" ;	// style.backgroundColor = "red" ;
			loDivConteneur.appendChild(loDivAttenuateur) ;
		}
		if (!plEnable){}
		else if (!document.getElementById(lsIdDivAttenuateur)) {}
		else {
			// Supprimer la div atténuateur
			var loDivAttenuateur = document.getElementById(lsIdDivAttenuateur) ;
			loDivConteneur.removeChild(loDivAttenuateur) ;		
		}
    },

	cvtLibelleBoutonPourModeHorizontal: function (psLibelle) {
		var tempLibelle ;
		var aTmp = psLibelle.split("");
		var sp = (isIE() && getInternetExplorerVersion() < 8 ? " " : "");
		tempLibelle = sp + aTmp[0];
		for (car = 1; car < aTmp.length; car++)
			tempLibelle = tempLibelle + /*'\n'*/ "<br>" + (car < aTmp.length - 1 ? sp : "") + aTmp[car];
		return tempLibelle ;
	},

/*
    getIdDivGroupe: function (poGroupe) {
        return "G" + poGroupe.iId;
    },
    getIdDivIntervenant: function (poIntervenant) {
        return "G" + poIntervenant.iGroupe + "-I" + poIntervenant.iId;
    },
    getIdDivUsager: function (loUsager) {
        var lsPositionUsager = "S" + loUsager.iService 
								+ "-St" + loUsager.iStatut 
								+ "-I" + loUsager.iIntervenant 
								+ "-Patient";

        return lsPositionUsager;
    },
    getIdUsager: function (psIdDiv) {
        psIdDiv = psIdDiv.split("-");
        var lsIdUsager = psIdDiv[3].substring(1);

        return lsIdUsager;
    }
	,
	 getIdGroupe: function (psIdDiv) {
        psIdDiv = psIdDiv.split("-");
        var lsIdGroupe = psIdDiv[0].substring(1);

        return lsIdGroupe;
    }
*/
});



