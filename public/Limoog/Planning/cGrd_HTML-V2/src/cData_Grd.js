/* 
 * Classe cData pour l'édition du planning
 */

var cData = cDataBase.extend({
    cClassName: "cData",
	charge: function (psJson) {
		if (!psJson) {
			// Json vide
			return false ;
		}
		var lo = tryParseJSON(psJson) ;
		if (LG_Ajax_GetText_EstErreur(psJson)) {
            this.oInitial = null;
            this.oActuel = null;
            this.cLastErreur = "Impossible de charger les données : " + psJson ;
            return false;

        } else if (!lo) {
			// JSON invalide
            this.cLastErreur = "Impossible de charger les données : " + psJson ;
            return false;
			
        } else {
			// Charger les données avec JSON
// document.getElementById("Erreurs").innerHTML = psJson ;
// alert(psJson) ;
			lo = JSON.parse(psJson) ;
			this.oInitial = lo.oData ;
			
			lo = JSON.parse(psJson) ;
            this.oActuel = lo.oData ;
			
			// Objet structure (créé temporairement pour le rendre disponible à window.oPlanning.oVue)
			this.oStructure = lo.oStructure ;
			
			// Calculer les données déduites des valeurs brutes
			// Calculer les dates des jours
			this.calcDatesJours() ;
//			if (lo.oStructure.oConfig.lRecalcChevauchements) {
//				// Calculer les chevauchements
//				this.calcChevauchements() ;
//			}
			
			this.lModifié = false ;
	
            return true;
        }
    },

//	// Calculer les chevauchements de séances
//	calcChevauchements: function() {
//	
//    },

	// Calculer la date des jours
	calcDatesJours: function() {
		var loJour ;
		for (var i = 0; i < this.oStructure.aJours.length ; i++) {
			loJour = this.oStructure.aJours[i] ;
			if (!loJour.dDate) {
				// La propriété date n'existe pas encore : l'ajouter
				loJour.dDate = this.sIdJourToDate(loJour.sIdJour) ;
			}
		}
	},

	// Convertir un sIdJour en date        
	// aParajournee.sIdJour est de la forme 'J20121001'
	sIdJourToDate: function (psIdJour) {
		var lsDate = psIdJour ;
		lsDate = lsDate.substring(1, 9);
		// sous IE, parseInt a besoin de la base 10 pour convertir cetains nombres qui, s'ils sont précédés de "0", sont pris comme de l'octal
		// (cf. http://stackoverflow.com/questions/8763396/javascript-parseint-with-leading-zeros)
		var ldDate = new Date(parseInt(lsDate.substring(0, 4), 10)
							, parseInt(lsDate.substring(4, 6), 10) - 1
							, parseInt(lsDate.substring(6, 8), 10));
		return ldDate ;
	},
	
	// Surcharge en remplacment du comportement normal
	getModeExecution: function(){
		if (typeof this.oStructure === "undefined") return window.getPlanningoCst().ModeExecution_Inconnu ;
		if (typeof this.oStructure.oConfig === "undefined") return window.getPlanningoCst().ModeExecution_Inconnu ;
		return this.oStructure.oConfig.nModeExecution ;
    },
	setModeExecution: function(piModeExecution){
		this.oStructure.oConfig.nModeExecution = piModeExecution ;
    },

	// ------------------------------------------------------------------------------------
	// Enlever une séance de la liste
	effaceSeance: function (poSeance) {
		var lsIdSeance = poSeance.sIdSeance ;
		for (var i = this.oActuel.aSeances.length - 1; i >= 0; i--){
			loSeance = this.oActuel.aSeances[i] ;
			if (loSeance.sIdSeance == lsIdSeance) {
				// Cette séance est la bonne: l'effacer de la liste des séances
				arrayRemove(this.oActuel.aSeances, i) ;
			}
		}
		
		return true ;
	},

	// ------------------------------------------------------------------------------------
	// Ajouter une séance dans la liste
	insereSeance: function (poSeance) {
		// Commencer par l'effacer si elle existe déja
		this.effaceSeance(poSeance) ;
		// Puis l'insérer
		this.oActuel.aSeances.push(poSeance) ;
		return true ;
	},
	
	bidonFinitClasse: 0
});
