/* ############################################################
 *  CLASSE CONSTANTES
 *      
 *     Permet d'indiquer dans quel mode d'éxécution nous sommes pour ne pas avoir constamment des erreurs.
 *     
 * ############################################################
 */

//var cConstantes = Class.create({
//    initialize: function (){
var cConstantes = Class.extend({
    init: function (){
        // Constantes
        this.ModeExecution_Inconnu = null ;
        this.ModeExecution_TestNavigateur = 0 ;
        this.ModeExecution_VFPLocal = 1 ;
        this.ModeExecution_VFPNet = 2 ;
        this.ModeExecution_ExecNavigateur = 3 ;
        this.ModeExecution_ExecSymfony = 4 ;
		
		this.Marge = 2 ;
		this.DimEchellesH = 15 ;			// Largeur en Pixel des div Horaires, sans Marging / Padding
		this.DimEchellesV = 25 ;			// Largeur en Pixel des div Horaires, sans Marging / Padding
		this.nbPasLegende = 8 ;				// Nb de traits approximatifs à afficher dans une légende
		this.iAjustHeightHoraires = 2 ; 	// Hauteur en Pixel des div Horaires, avec Marging / Padding
		
		this.HauteurBouton = 22 ;
		this.LargeurBouton = 18 ;
		this.LargeurBoutonLarge = 70 ;
		this.EspaceEntreBoutonsJourEtRessource = 2 ;
		
		this.DimBordureEchelles = 1 ;		// Largeur en Pixel des div Horaires, sans Marging / Padding
		this.DimBordureMois = 1 ;
		this.DimBordureJours = 0 ;
		this.DimBordureTranches = 0 ;
		this.DimBordureRessources = 1 ;
		this.DimBordureZoneInterdite = 1 ;
		this.DimBordureSéance = 1 ;
		this.DimBordureSéanceSélectionnée = 2 ;
		
		this.largeurPlanningMin = 500 ;
		this.hauteurPlanningMin = 250 ;

		this.largeurMoisMin = 200 ;
		this.hauteurMoisMin = 200 ;

		this.largeurJourMin = 50 ;
		this.hauteurJourMin = 22 ;
		
		this.largeurRessourceMin = 50 ;
		this.hauteurRessourceMin = 21 ;

		this.sCouleurDesJoursAjoutés = "lightGrey" ;
		this.iTaillePoliceSeancesDft = 10 ;
		
		// Types de grilles prédéfinies (cf. grille.h)
		this.eTypesGrille = {Absences: 1					// Type de grille prédéfini : absences 
							, Activité: 2					// Type de grille prédéfini : répartition d'une activité
							, Planning: 3					// Type de grille prédéfini : planning
							, PlanningSansAbsStd: 6			// Type de grille prédéfini : planning sans les absences standard
							, Présences: 4					// Type de grille prédéfini : présences
							, PrésencesEtAbsences: 5		// Type de grille prédéfini : présences + absences
							, Horaires: 7					// Type de grille prédéfini : horaires en saisie clavier
							} ;

		// Types d'affichage des horaires (cf. grille.h)
		this.eTypesHoraire = {Chronologique: 1
								, MatinPM: 2
								, Matin: 3
								, PM: 4
								, Journée: 5
								} ;
	
		// Types d'affichage des ressources (cf. grille.h)
		this.eTypesAffichageRessources = {RessourcesSéparées: 1
										, RessourcesGroupées: 2
										} ;

		// Types d'affichage des jours
		this.eTypesAffichageJours = {Normal: "N"			// Normal, on affiche les jours d'ouverture (+ filtre éventuel de pvLstJours), sur les 
									, Inverse: "I"			// Inverse, on affiche l'inverse des horaires d'ouverture, sur les 7 jours (+ filtre éventuel de pvLstJours)
									, Complet: "C"			// Complet, on affiche tous les jours, 24h/24 (+ filtre éventuel de pvLstJours)
									} ;

		// Types d'affichage de la grille
		this.eStyles = {Semaine: "Semaine"					// Mode d'affichage "Semaine", ou jours à le suite les uns des autres
						, Calendrier: "Calendrier"			// Mode affichage Calendrier
						} ;
    }
});

