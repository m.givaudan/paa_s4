// cGestionnaireActions_Symfony.js
// LG 20190409

// ****************************************************************************************
// Classe de gestionnaire d'actions pour le planning, sous Symfony
// ****************************************************************************************
var cGestionnaireActions_Symfony = cGestionnaireActionsBase.extend({

	// ---------------------------------------------------------------------------------------
	// Lancer une action
	// On devrait aussi traiter de cette manière le cas de la demande de chargement du planning : window.oControleur_Grd.demandeChargement
	lanceActionSymfony: function(poJSONAction, callback){
//		var thisObject = this ;
		var lsURL = poJSONAction.URL ;
        var lsURL = getBaseURL() + lsURL ;
		var lsJSON = JSON.stringify(poJSONAction) ;
		console.log(lsJSON);
		$.ajax({                   
            type: "POST",
            url: lsURL,
            data: lsJSON,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status, xhr) {
				if (window.oPlanning.oControleur.gereReponseAjax(xhr.responseText, callback)) {
					// La réponse a été gérée en standard
					return ;
				}
				callback(xhr.responseText) ;
				
            },
            error: function (xhr, status, error) {
				console.error("Erreur de lanceActionSymfony") ;
// LG 20190926 début
//				window.oPlanning.oControleur.gereReponseAjax_Erreur(xhr.responseText || error.message, callback) ;
				var lsMsg = "" ;
//MG Modification 202009292 Début
				if (estHtml_DemandeLogin(xhr.responseText)){
					return window.oPlanning.oControleur.gereReponseAjax_DemandeLogin(xhr.responseText, callback) ;
				}
//if (error.message) lsMsg = "<STRONG>" + error.message + "</STRONG>" ;
				else if (error.message){
					lsMsg = "<STRONG>" + error.message + "</STRONG>" ;
				}
//MG Modification 202009292 Fin			
				if (xhr.responseText){
					var lsResponseText = xhr.responseText ;
					if (lsMsg) {
						lsMsg += "<HR>" ;
						// Traduire les balises HTML pour les afficher en clair  plutôt que leur visualisation par le browser
						// selon https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
						lsResponseText = lsResponseText.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
													   return '&#'+i.charCodeAt(0)+';';
													}) ;
					}
					lsMsg += lsResponseText ;
				}
//MG ajout 20200306 Début
//Dans le cas de xhr.responseText vide 
				else {
					lsMsg += ' : <STRONG> La réponse est vide </STRONG>';
				}
//MG ajout 20200306 Fin
				window.oPlanning.oControleur.gereReponseAjax_Erreur(lsMsg, callback) ;
// LG 20190926 fin

            },
            complete: function (xhr, status) {
//				callback() ;
            },
		});
		
	},
	
	// ---------------------------------------------------------------------------------------
	// Gestion de la fin d'un déplacement de déance
	onFinDéplacementSéance: function(poMasque, e, poDragInfos, callback){

		if (poDragInfos) {
			var lsRessource = poDragInfos.sIdRessource ;
			var ldDateTime = poDragInfos.dDate ;
		} else {
			var lsRessource = poMasque.sIdRessource ;
			var ldDateTime = poMasque.dDate ;
		}
		var ldDate = ldDateTime.getFullYear() 
				+ "-" + padLeft(ldDateTime.getMonth() + 1, 2, '0') 
				+ "-" + padLeft(ldDateTime.getDate(), 2, '0') ;
		var liShiftAltCtrl = eventKeyCode(e);
		var lsURL = "onFinDéplacementSéance";
		var loJSON = {"URL": lsURL
						, "sIdSeance": poMasque.sIdSeance.split("_")[0]
						, "iId_Seance": poMasque.aParaSeance.iId_Seance
						, "sIdRessource": lsRessource
						, "sIdRessource_Old": poMasque.sIdRessource
						, "dDate": ldDate
						, "iDebut": poMasque.iDebut
						, "iFin": poMasque.iFin
						, "sLibelle": poMasque.aParaSeance.cLibelle
						, "cCouleur": poMasque.aParaSeance.cCouleur
						, "lDuplique": liShiftAltCtrl & 2				// Ctrl
						, "lAjusteParticipation": liShiftAltCtrl & 1	// Shift
// LG 20190824 début
						, "iRangSousGroupe": poMasque.aParaSeance.iRangSousGroupe
						, "iNbSousGroupes": poMasque.aParaSeance.iNbSousGroupes
						, "cNomSousGroupe": poMasque.aParaSeance.cNomSousGroupe
// LG 20190824 fin
						} ;
		this.lanceActionSymfony(loJSON, callback);
	},

	// ---------------------------------------------------------------------------------------
	// Gestion d'un double-clic sur une séance
	onDoubleClicSéance: function(poMasque, callback){
//		if (false) {
//			// shunter la demande d'activité pour accélérer les tests
//			var loJSON = {"URL":"insèreSéance","sIdSeanceSource":"S0_840_Selection","sIdRessource":"I1000125","iActi":"1246","dDate":"2019-05-13","iDebut":"600","iFin":"660"} ;
//			this.lanceActionSymfony(loJSON, callback);
//			return ;
//		}
		
		var thisObject = this ;
		
		var ldDate = poMasque.dDate ;
        var ldDate = ldDate.getFullYear() 
					+ "-" + (getMonth(ldDate, true)) 
					+ "-" + padLeft(ldDate.getDate(), 2, '0') ;

		var lsDebut = (minToHeures(poMasque.iDebut)).replace('h', ':') ;	// format HH:MM
		var lsFin = (minToHeures(poMasque.iFin)).replace('h', ':') ;		// format HH:MM
		var lsLstRes = poMasque.sIdRessource ;
		var lsLstResSelected = poMasque.sIdRessource ;
		var lsURL;
		var idSeance = poMasque.aParaSeance.iId_Seance;
		var titre;
		if(idSeance === -2){
//                    Dans le cas de l'activité "Hors établissement", on ne doit pas pouvoir modifier
			return;
		}else if(idSeance){			
//                  Dans le cas où on double clique sur une seance, on affiche le modal permettant la modification
            lsURL = getBaseURL() + 'entityForActivite/' + idSeance ;
			titre = "Modification de la séance";
			loEntityFormPopup = newEntityFormPopup() ;
//MG Modification 20200225 Début
//                    loEntityFormPopup.popUpEntité("", idSeance);
		loEntityFormPopup.popUpEntité("seancesTrad", idSeance, private_onDoubleClicSéanceExistante_onOK);
//MG Modification 20200225 Fin
		} else {
//                  Dans le cas où on double clique sur une emplacement du planning vide, on affiche le modal permettant la création d'une séance
                        lsURL = getBaseURL() + 'TestYohan/getIDActivites/'+lsLstRes+'/'+lsLstResSelected+'/'+ldDate+'/'+lsDebut+'/'+lsFin ;
			titre = "Poser une séance ou une participation";
			loPopup = newPopup_getIdActivite(lsURL , titre);
//MG Modification 20200131 Début
//                    loPopup.popup(private_onDoubleClicSéance_onOK);
			loPopup.popup(private_onDoubleClicSéanceVierge_onOK);
//MG Modification 20200131 Fin
                }
		
//MG Modification 20200131 Début	
//		function private_onDoubleClicSéance_onOK(poChoix){	
		function private_onDoubleClicSéanceVierge_onOK(poChoix){
//MG Modification 20200131 Fin
			if (poChoix) {
//				alert("Date : "+poChoix.date
//						+ "\nheure debut: "+poChoix.Tdeb
//						+ "\nHeure fin : "+poChoix.Tfin 
//						+ "\nRessource Selected : "+ poChoix.PlstRes
//						+ "\nGroupeActi : "+poChoix.groupeActi
//						+ "\nInclure activite générale : "+poChoix.InnactiGen
//						+ "\nUniquement ressource Concener : "+poChoix.UniRessConce
//						+ "\nInc Activité système "+poChoix.IncActSys
//						+ "\nParam : "+poChoix.param
//						+ "\nActivite : "+poChoix.iActi ;
				var lsURL = "insèreSéance";
				var loJSON = {"URL": lsURL
								, "sIdSeanceSource": poMasque.sIdSeance
								, "sIdRessource": poMasque.sIdRessource
								, "iActi": poChoix.iActi
								, "dDate": poChoix.date
								, "iDebut": HeureToMin(poChoix.Tdeb)
								, "iFin": HeureToMin(poChoix.Tfin)
								, "OngletActif": poChoix.OngletActif
								, "SSOngletActif": poChoix.SSOngletActif
								, "aLstResAInclure": poChoix.aLstResAInclure
								} ;
				thisObject.lanceActionSymfony(loJSON, callback);
			}
			callback() ;
		}

//MG Ajout 20200131 Début
		function private_onDoubleClicSéanceExistante_onOK(){
			var lsURL = "rafraichirSéance";
//MG Ajout 20200306 Début
//Utilisation de la même fonction que private_onPopUpChangeActivitéSéance, les détails sont nécéssaires
			var loJSON = {"URL": lsURL
								, "iId_Seance" : idSeance
								, "sIdSeanceSource": poMasque.sIdSeance
								, "sIdRessource": poMasque.sIdRessource
								, "dDate": window.oPaa.getDate(poMasque.dDate, 'YYYY-MM-DD')
								, "iDebut": poMasque.iDebut
								, "iFin": poMasque.iFin
								, "aLstResAInclure": poMasque.aLstResAInclure
								, "lDuplique": false
								} ;
//MG Ajout 20200306 Fin
			thisObject.lanceActionSymfony(loJSON, callback);
			callback();
		}
//MG Ajout 20200131 Fin
	},
	
	// ---------------------------------------------------------------------------------------
	// Gestion de la suppression d'une séance
	supprimeSéance: function(psLstSéances, callback){
		var lsURL = "supprimeSéance";
		var loJSON = {"URL": lsURL
						, "sLstSeances": psLstSéances
						} ;
		this.lanceActionSymfony(loJSON, callback);
	},
	
//MG Ajout 20200305 Début
	onCliqueDroitChangerActivité: function(poMasque, callback){
		var thisObject = this ;
		var ldDate = poMasque.dDate ;
        var ldDate = ldDate.getFullYear() 
					+ "-" + (getMonth(ldDate, true)) 
					+ "-" + padLeft(ldDate.getDate(), 2, '0') ;

		var lsDebut = (minToHeures(poMasque.iDebut)).replace('h', ':') ;	// format HH:MM
		var lsFin = (minToHeures(poMasque.iFin)).replace('h', ':') ;		// format HH:MM
		var lsLstRes = poMasque.sIdRessource ;
		var lsLstResSelected = poMasque.sIdRessource ;
		var lsURL = getBaseURL() + 'TestYohan/getIDActivites/'+lsLstRes+'/'+lsLstResSelected+'/'+ldDate+'/'+lsDebut+'/'+lsFin ;;
		var idSeance = poMasque.sIdSeance.split(':')[1];
		var titre = "Changer une activité";
		loPopup = newPopup_getIdActivite(lsURL , titre);
		loPopup.popup(private_onPopUpChangeActivitéSéance);
		function private_onPopUpChangeActivitéSéance(poChoix){
			if (poChoix) {
				var lsURL = "rafraichirSéance";
				var loJSON = {"URL": lsURL
								, "iId_Seance" : idSeance
								, "sIdSeanceSource": poMasque.sIdSeance
								, "sIdRessource": poMasque.sIdRessource
								, "iActi": poChoix.iActi
								, "dDate": poChoix.date
								, "iDebut": HeureToMin(poChoix.Tdeb)
								, "iFin": HeureToMin(poChoix.Tfin)
//								, "OngletActif": poChoix.OngletActif
//								, "SSOngletActif": poChoix.SSOngletActif
//								, "aLstResAInclure": poChoix.aLstResAInclure
								, "lDuplique": false
								} ;
				thisObject.lanceActionSymfony(loJSON, callback);
			}
			callback() ;
		}
	}
//MG Ajout 20200305 Début
});



