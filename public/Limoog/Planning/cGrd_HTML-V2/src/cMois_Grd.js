﻿/**###############################################
 *     CLASSE cMois
 ###############################################*/

var cMois = cSemaine.extend({
	cClassName: "cMois",
	iNbJoursMax: 31,
	
    // poMois : {iAnnée: liAnnée, iMois: liMois}
	init: function (poMois){
		this.oMois = poMois ;
		this.initProps() ;
	},
	
	getIdDiv: function () {
		return window.oPlanning.oStructure.getIdDivMois(this.oMois.iAnnée, this.oMois.iMois) ;
	},
	
	// Renvoyer la dimension de l'en-tête (NB : pour surcharge de Semaine)
	getDimEnTete: function () {
		return (this.vertical?window.oPlanning.oCst.LargeurBouton:window.oPlanning.oCst.HauteurBouton) 
				+ window.oPlanning.oCst.Marge * 2 ;
	},
	
	// Renvoyer le nb de jours à afficher (NB : surcharge de Semaine)
//	getNbJours: function () {
//		return Math.min(window.oPlanning.oStructure.aJours.length, 31) ;
//	},
	
	// Surcharge de cSemaine
	calcDimJour: function () {
		if (this.iDimJour)  {	
			// Déja déterminé
			return this.iDimJour ;
		}
		var liDimJour = this._super() ;
		
		// Recalculer les dimensions
		this.heightInterieurJour = this.heightInterieurJour / window.oPlanning.oStructure.aMois.length
								- window.oPlanning.oStructure.getDimEnTeteJour() ;
		this.widthInterieurJour = this.widthInterieurJour / window.oPlanning.oStructure.aMois.length
								- window.oPlanning.oStructure.getDimEnTeteJour() ;

		this.heightInterieur = this.heightInterieur / window.oPlanning.oStructure.aMois.length ;
		this.widthInterieur = this.widthInterieur / window.oPlanning.oStructure.aMois.length ;
		
		return liDimJour ;
		
	},
	
	rtvHTML: function (piPosition, piDimension) {
	
		var lsHTML = '';
		var loMois = this.oMois ;
		var lsIdDivMois = this.getIdDiv(loMois) ;
		
		this.calcDimJour() ;
		this.calcPositionTranches();
//		this.calcPasLegende();
		window.oPlanning.oStructure.calcPasLegende();
		
		lsHTML = lsHTML
				+ "<div "
				+ "class='mois' "
				+ "id='" + lsIdDivMois + "' "
				+ "style='"
					+ "text-align: center; "
					+ "position: absolute; "
//					+ "background-color : " + this.cCouleur + ";"
					+ "border: " + window.oPlanning.oCst.DimBordureMois + "px solid black; "
					+ "text-align : center; "
					;
		if (!this.vertical) {
			lsHTML = lsHTML
						+ "height : 100%; "
						+ "left : " + piPosition + "px; "
						+ "width : " + piDimension + "px; " ;
		} else {
			lsHTML = lsHTML 
						+ "height: " + piDimension + "px; "
						+ "top: " + piPosition + "px; "
						+ "width: 100%; " ;
		}
		lsHTML = lsHTML + "'>" ;
		lsHTML = lsHTML + this.rtvHTMLEnTête(loMois) ;
		
		lsHTML = lsHTML + this.rtvHTMLLegende('gauche', piDimension)
		lsHTML = lsHTML + this.rtvHTMLLegende('centre', piDimension)
		lsHTML = lsHTML + this.rtvHTMLLegende('droite', piDimension)

		var liStartPos = (this.vertical?window.oPlanning.oCst.LargeurBouton:window.oPlanning.oCst.HauteurBouton) 
							+ (this.vertical?window.oPlanning.oCst.DimEchellesV:window.oPlanning.oCst.DimEchellesH)
							+ window.oPlanning.oCst.Marge * 3
		lsHTML = lsHTML + this.rtvHTMLJours(liStartPos, loMois) ;
		lsHTML = lsHTML + "</Div>" ;
		
		return lsHTML ;
	},

	// -----------------------------------------------------------------------------------
	// Renvoyer le code HTML du bouton de jour
    rtvHTMLEnTête: function () {
		
		var loMois = this.oMois ;
		var lsIdDivMois = this.getIdDiv(loMois) ;
		var lsLibelle, lsTitle ;
		var lsType = 'Div' /*'Button'*/;
//		var liRéduitTaille = (lsType.toUpperCase() == 'BUTTON')?0:1;	// La bordure du bouton est incluse dedans
		var liRéduitTaille = 0;	// box-sizing: border-box permet de gérer pareil que pour les vrais boutons;
		var lsHTML = "<" + lsType + " "
				+ "class='context-menu-seance boutonMois button colléAGaucheDuCalendrier' "
				+ "onmouseup='window.oPlanning.oControleur.clickBouton(event, \"" + lsIdDivMois + "\", \"\");' "
				+ "style='"
//					+ "position: absolute; "
//					+ "z-index: 100; "
				;
		lsLibelle = CMONTH(loMois.iMois).toProperCase() ;
		if (loMois.iAnnée > 1950) lsLibelle += " " + loMois.iAnnée ;
		lsTitle = lsLibelle ;
		if (!this.vertical) {
			lsHTML = lsHTML
						+ "width: 100%; "
						+ "height: " + (window.oPlanning.oCst.HauteurBouton - liRéduitTaille) + "px; "
						+ "top: " + window.oPlanning.oCst.Marge + "px; "
					;
				if (isIE()) lsHTML = lsHTML + "position: absolute; " ;	// Sous IE, les boutons apparaissent translucides sinon
		} else {
			// Astuce pour compenser la mauvaise prise en charge de "text-align: center; " dans les boutons sur IE7
			lsLibelle = lsLibelle.substr(0, 3) ;
			if (loMois.iAnnée > 1950) lsLibelle += " " + loMois.iAnnée ;
			lsLibelle = window.oPlanning.oVue.cvtLibelleBoutonPourModeHorizontal(lsLibelle) ;
			lsHTML = lsHTML 
//						+ "text-align: center; "
//						+ "padding: 0; "
//						+ "font-size: 8pt; "
//						+ "line-height: 1em; "
//						+ "float: left; "
						+ "height: 100%;"
						+ "width: " + (window.oPlanning.oCst.LargeurBouton - liRéduitTaille) + "px; "
						+ "left: " + window.oPlanning.oCst.Marge + "px; "
					;
		}
		lsHTML = lsHTML 
						+ "'"
					+ "title='" + lsLibelle + "'"
					+ "/>"
					+ "<table"
						+ " class = 'TablePourCentrage'"
						+ " style='"
//							+ "width:100%; "
//							+ "height:100%; "
//							+ "vertical-align: middle;"
//							+ "word-break: break-all;"
							+ "'"
						+ "><tr><td>"
						+ lsLibelle 
						+ "</td></tr></table>"
					+ "</" + lsType + ">"
					;
		
		return lsHTML ;
		
    },

	bidonPourFinDeClasse:0
});
