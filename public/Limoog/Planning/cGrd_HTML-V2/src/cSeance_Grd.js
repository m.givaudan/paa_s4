/**###############################################
 *      CLASSE SEANCE
 *
 * @param {Array} aParaSeance - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
 *
 * Ce tableau de paramètres contient les paramètres suivants :
 * @param {String} sIdSeance - Identifiant de la Ressource
 * @param {String} cLibelle - Nom complet de la Ressource
 * @param {String} cInfobulle - Message à afficher lors du passage de la souris sur la séance 
 * @param {String} dDate - Date de la séance 
 * @param {String} iDebut - Heure du début de la seance, en minutes 
 * @param {String} iFin - Heure du début de la seance, en minutes 
// LG 20160830 déac * @param {Objet tranche} tTranche - instance de la classe tranche, parent de la ressource courante
 * @param {String} cCouleur - Couleur d'affichage de la séance 
// LG 20160830 déac  * @param {Number} iHeureMin - Heure, en minutes, de début de l'affichage de la semaine
// LG 20160830 déac  * @param {Number} iHeureMax - Heure, en minutes, de la fin de l'affichage de la semaine
 ###############################################*/

//var seance = Class.create({
//	initialize: function (aParaSeance, psIdRessource, psIdTranche) {
var cSeance = Class.extend({
	cClassName: "cSeance",
//	cCSSClass: "",		// Nom de classe, pour activation du menu contextuel
	init: function (aParaSeance, psIdRessource, psIdTranche) {
		this.aParaSeance = aParaSeance ;

		// Paramètres obligatoires
		this.sIdTranche = psIdTranche ;
		this.sIdRessource = psIdRessource ;
		this.sIdJour = aParaSeance.sIdJour;
		this.sIdRessource = aParaSeance.sIdRessource;
		this.sIdSeance = aParaSeance.sIdSeance;
		this.iDebut = aParaSeance.iDebut;
		this.iFin = aParaSeance.iFin;
		this.semaine = aParaSeance.sSemaine;
		this.cLibelle = aParaSeance.cLibelle ;
		this.cInfobulle = aParaSeance.cInfobulle;
		this.oldBorder = '';
		
		// Pour une raison inconnue, l'envoi de '&quot;' en mode webBorwser (uniquement) empêche le décodage de la chaine JSON
		this.cLibelle = this.cLibelle.replace(/@quot;/gm, '&quot;') ;

		// Paramètres optionnels
		// 
		if (aParaSeance.iHeureMinReel) {
console.log("à quoi sert this.iHeureMinReel ?") ;
			this.iHeureMinReel = aParaSeance.iHeureMinReel;
		} else
			this.iHeureMinReel = this.iDebut;
		if (aParaSeance.iHeureMaxReel) {
console.log("à quoi sert this.iHeureMaxReel ?") ;
			this.iHeureMaxReel = aParaSeance.iHeureMaxReel;
		} else
			this.iHeureMaxReel = this.iFin;
		if (aParaSeance.dDate)
			this.dDate = aParaSeance.dDate;
		else
			this.dDate = "Date indéfinie";
		if (aParaSeance.couleurBordure)
			this.couleurBordure = aParaSeance.couleurBordure;
		else
			this.couleurBordure = "black";
		if (aParaSeance.iNbChevauchements)
			this.iNbChevauchements = parseInt(aParaSeance.iNbChevauchements);
		else
			this.iNbChevauchements = false;
		if (aParaSeance.iRangChevauchement)
			this.iRangChevauchement = parseInt(aParaSeance.iRangChevauchement);
		else
			this.iRangChevauchement = 0;
		if (aParaSeance.cCouleur)
			this.cCouleur = aParaSeance.cCouleur;
		else
			this.cCouleur = "White";
		if (aParaSeance.dDate)
			this.dDate = aParaSeance.dDate;
		else
			this.dDate = "Date indéfinie";
		if (aParaSeance.lVirtuel)
			this.lVirtuel = aParaSeance.lVirtuel;
		else
			this.lVirtuel = false;

		this.vertical = aParaSeance.vertical ;
	},

	/**
	 *toString
	 *@return {String} renvoit les informations de l'instance courante de la seance
	 *
	 */
	toString: function () {
		return '<b>Seance : ' + this.sName + '</b><br />' 
				+ "Top : " + this.getDivTop() + '<br />' 
				+ "Height : " + this.getDivHeight() + '<br />' 
				+ "Width : " + this.getDivWidth() + '<br />' 
				+ "z-Index : " + this.getsZIndex();
	},

	getIdDivTranche: function () {
		return window.oPlanning.oStructure.getIdDivTranche(this.sIdTranche, this.sIdJour) ;
	},
	getDivTranche: function () {
		return document.getElementById(this.getIdDivTranche()) ;
	},
	// Récupérer le code HTML correpondant à une séance à l'intérieur d'une ressource
	getTranche: function () {
		return window.oPlanning.oStructure.getTrancheAvecDiv(this.getIdDivTranche()) ;
	},
	getIdDivRessource: function () {
		return window.oPlanning.oStructure.getIdDivRessource(this.sIdRessource, this.sIdJour, this.sIdTranche) ;
	},
	getDivRessource: function () {
		return document.getElementById(this.getIdDivRessource()) ;
	},
	getRessource: function () {
		return window.oPlanning.oStructure.getRessourceAvecDiv(this.getIdDivRessource()) ;
	},
	getIdDiv: function () {
		return window.oPlanning.oStructure.getIdDivSeance(this.sIdSeance, this.sIdRessource, this.sIdJour, this.sIdTranche) ;
	},
	getDiv: function () {
		var lsId = this.getIdDiv() ;
		return document.getElementById(lsId) ;
	},
	getDivTexte: function (poDivSeance) {
		poDivSeance = poDivSeance?poDivSeance:this.getDiv() ;
		var loDivTable = poDivSeance.childNodes[0] ;
		if ((" " + loDivTable.className + " ").indexOf( " TablePourCentrage " ) < 0) {
			// On a une flèche qui occupe le premier fils
			loDivTable = poDivSeance.childNodes[1] ;
		}
		if ((" " + loDivTable.className + " ").indexOf( " TablePourCentrage " ) < 0) {
			// On a une flèche qui occupe le second fils
			loDivTable = poDivSeance.childNodes[2] ;
		}
		var loDivTexte = loDivTable.childNodes[0].childNodes[0].childNodes[0]
		return loDivTexte ;
	},
	
	// Afficher la séance
	afficher: function (pbSansAlaCréation) {
		var loDivRessource = this.getDivRessource() ;
		var lsHTML = this.rtvHTML(loDivRessource) ;
		loDivRessource.innerHTML += lsHTML ;
		
		//if (!pbSansAlaCréation) {
			// Complète la méthode init qui ne peut pas tout faire tant que l'object html n'a pas été créé.
			// var loDiv = this.getDiv();
			// loDiv.parentNode.setAttribute('draggable', 'false');
			// this.width = '0';
			// this.zIndex = '0';
		//}
	},
	
	// Récupérer le code HTML correpondant à une séance à l'intérieur d'une ressource
	rtvHTML: function (poDivRessource) {
		
		// Déterminer les dimensions
		var bordure = this.borderWidth?this.borderWidth:window.oPlanning.oCst.DimBordureSéance ;
		var loTranche = this.getTranche() ;

		var pos = loTranche.getPositionSéance(poDivRessource, this.iDebut, this.iFin, bordure, this.iNbChevauchements, this.iRangChevauchement) ;
		var liWidth, liLeft, liRight, liHeight, liTop, liBottom ;
		liWidth = pos.iWidth ;
		liLeft = pos.iLeft ;
		liRight = pos.iRight ;
		liHeight = pos.iHeight ;
		liTop = pos.iTop ;
		liBottom = pos.iBottom ;
		
		// Déterminer les dégradés de couleur
		var lsBackGround = "" ;
		if (this.cCouleur.toUpperCase() == "BLACK" || this.cCouleur.toUpperCase().replace(" ", "") == "RGB(0,0,0)" || this.cCouleur == "#000000"){
			// Noir pur : interdit
			this.cCouleur = "white" ;
		}
		if (window.oPlanning.oStructure.lCouleursDegradees) {
			// On doit utiliser les dégradés de couleur
// LG 20201001 début
//			if (this.cCouleur == "white" || this.cCouleur == "White" || this.cCouleur == "#FFFFFF"){
//				tempCouleur = "#DDDDDD";
//			} else tempCouleur = this.cCouleur;
//			lsBackGround = "background-color: #D3D3D3 ; "
			if (this.cCouleur == "white" || this.cCouleur == "White" || this.cCouleur == "#FFFFFF"){
				// Utiliser un gris clair au lieu du gris
                this.cCouleur = "#D3D3D3";
			}
// LG 20201001 fin
console.log("Les dégradés de couleur sont désactivés") ;
        }

		// Dessiner la case
		var lsIdDiv = this.getIdDiv() ;
		var lsClass = "seance" ;
		if (this.lVirtuel) lsClass+= " seanceVirtuelle" ;
		if (this.aParaSeance.lLibre1) lsClass+= " seanceLibre1" + (this.vertical?"V":"H") ;
		if (this.aParaSeance.lLibre2) lsClass+= " seanceLibre2" + (this.vertical?"V":"H") ;
		var lsBorder = bordure + "px black " ;
		if (this.lVirtuel) lsBorder+= "dashed" ;
		else lsBorder+= "solid" ;
		var lsInfoBulle = this.cInfobulle ;
		lsInfoBulle = lsInfoBulle.replace(/[']+/g, "&apos;") ;
		lsInfoBulle = lsInfoBulle.replace(/\<br\>/g, "\n") ;
		lsInfoBulle = lsInfoBulle.replace(/\<BR\>/g, "\n") ;
		var lcHTML = "<div "
						+ "id='" + lsIdDiv + "' "
						+ "class='" + lsClass + " shadow rounded-lg' "
						+ "title='" + lsInfoBulle + "' "
						+ "onclick='window.oPlanning.oStructure.select(\"" + lsIdDiv + "\", true);' "
						+ "style='"
							+ "font-size : " + window.oPlanning.oStructure.oConfig.iTaillePoliceSeancesDft + "px; "
							+ "background-color: " + this.cCouleur + "; "
							+ "top : " + liTop + "px; "
							+ "left : " + liLeft + "px; "
							+ "width : " + liWidth + "px; "
							+ "height:" + liHeight + "px; "
// LG 20201001 old							+ lsBackGround
							+ "'>"
							;
// LG 20200720 début : pour atténuer la couleur de la majeure partie de la séance
        lcHTML += "<div class='AttenuationCouleur'></div>" ;
// LG 20200720 fin

		// Dessiner le contenu de la case
		var lsFlècheAvant = "", lsFlècheAprès = "" ;
		this.lDépasseDeTranche = false ;
		if (this.iDebut < loTranche.iDebut /*&& !this.lMasque*/) {
			// Cette séance dépasse par son début et ce n'est pas un masque de sélection
			this.lDépasseDeTranche = true ;
			lsFlècheAvant = window.oPlanning.oGrille.vertical?'flecheHaut':'flecheGauche' ;
			lsFlècheAvant = "<div "
								+ "id='fleche" + lsIdDiv + "' "
								+ "class = " + lsFlècheAvant + " " 
								+ ">"
								+ "</div>" ;
		}
		if (this.iFin > loTranche.iFin /*&& !this.lMasque*/) {
			// Cette séance dépasse par sa fin et ce n'est pas un masque de sélection
			this.lDépasseDeTranche = true ;
			lsFlècheAprès = window.oPlanning.oGrille.vertical?'flecheBas':'flecheDroite' ;
			lsFlècheAprès = "<div "
								+ "id='fleche-" + lsIdDiv + "' "
								+ "class = " + lsFlècheAprès + " " 
								+ ">"
								+ "</div>" ;
		}

// LG 20190824 début
		if (this.aParaSeance.cNomSousGroupe  && this.aParaSeance.cNomSousGroupe.length > 0) {
			// Cette séance est celle d'un sous-groupe
			// Afficher le libellé du sous-groupe
			lcHTML = lcHTML + '<div style="'
//							+ " background-color: red;"
							+ " font-size: 12px;"
							+ " font-weight: bold;"
							+ " color: black;"
							+ " display: block;"
							+ " border: solid 1px black;" ;
			if (window.oPlanning.oGrille.vertical) {					
				lcHTML = lcHTML + " width: 100%;"
								+ " Height: 20px;" ;
			} else {
				lcHTML = lcHTML + " width: 20px;"
								+ " Height: 100%;"
								+ " writing-mode: vertical-rl;"
								+ " transform: rotate(180deg);" ;
			}
			lcHTML = lcHTML + ' ">'
							+ this.aParaSeance.cNomSousGroupe + "</div>" ;
		}
// LG 20190824 fin

		var lsLibelle = rTrimHTML(this.cLibelle) ;
		lcHTML = lcHTML + "<table"
						+ " class = 'TablePourCentrage'"
						+ " style='"
// LG 20190824 début
							+ "top:0px; position:absolute;"
// LG 20190824 fin
							+ "'"
						+ "><tr>"
						+ lsFlècheAvant
						+ "<td "
							+ "id=contenu-" + lsIdDiv + " "
//							+ "class='" + this.cCSSClass + " contenuSéance'>"
							+ "class='contenuSéance'>"
						+ lsLibelle
						+ "</td>"
						+ lsFlècheAprès
						+ "</tr></table>"
						+ "</div>"
						;

		return lcHTML;
	},

	// NB 20150318 : Permet la selection, crée le masque et garde l'original en dessous du masque (copie)
	// creer une seance selectionnée
	selectThis: function (bInformeVFP) {

		// La div d'origine devient de style "seanceMasquee" (hachures)
		var loThisDiv = this.getDiv() ;
		if (loThisDiv != null) {
			this.classNameSaved = loThisDiv.className ;
			loThisDiv.className = 'seanceMasquee' ;
			
			// Permettre au masque de hachure d'apparaitre (cas où on affiche le dégradé)
			this.backgroundImageSaved = loThisDiv.style.backgroundImage ;
			loThisDiv.style.backgroundImage = "" ;
		}

		// Créer le masque
		var loMasque = new cSeanceSelectionnee(this.aParaSeance, this.sIdRessource, this.sIdTranche);
		loMasque.afficher(true, this) ;
		
// LG 20190412 début
// Astuce malpropre pour éviter que la mauvaise séance soit traitée
// En effet, tous les fantomes des séances désélectionnées restent actifs et reçoivent cet événement
// On passe loSéance.oSéanceSource.selectionnée à true lors de la sélection
		this.selectionnée = true ;
// LG 20190412 fin

		return loMasque ;
	},

	// NB : 20150318 : On retire supprime les element possednt "_selection" qui signifie qu'ils sont 
	// selectionnée et on repasse l'element en forme non selectionnée 
	unSelectThis: function () {
		var lsIdDivSelection = this.getIdDiv() + "_Selection"; //"SeanceSelectionne"
		if (document.getElementById(lsIdDivSelection))
			document.getElementById(lsIdDivSelection).parentNode.removeChild(document.getElementById(lsIdDivSelection));
		if(this.getDiv() != null) {
			var loThisDiv = document.getElementById(this.getIdDiv()) ;
//			loThisDiv.className = "seance";
			loThisDiv.className = this.classNameSaved ;
			// Restaurer l'image de fond qui a été enlevée pour l'affichage de la hachure
			if (this.backgroundImageSaved) loThisDiv.style.backgroundImage = this.backgroundImageSaved ;
		}
// LG 20190412 début
// Astuce malpropre pour éviter que la mauvaise séance soit traitée
// En effet, tous les fantomes des séances désélectionnées restent actifs et reçoivent cet événement
// On passe loSéance.oSéanceSource.selectionnée à false lors de la désélection
		this.selectionnée= false ;
// LG 20190412 fin
// 20190824 début
// On passe aussi loSéance.unSelected à true lors de la désélection pour le cas où on resélectionne la même séance
		this.unSelected = true ;
// 20190824 fin
	},
		
	getDivTop: function () {
		return getDivTop(this.getDiv()) ;
	},	
	getDivLeft: function () {
		return getDivLeft(this.getDiv()) ;
	},	
//	getDivBottom: function () {
// 		return getDivBottom(this.getDiv()) ;
//	},
	getDivWidth: function () {
		return getDivWidth(this.getDiv()) ;
	},
	getDivHeight: function () {
		return getDivHeight(this.getDiv()) ;
	},
	
	getCoordParHeure: function (piHeure) {
		var loTranche = this.getTranche() ;
// LG 20190824 début
//		return loTranche.getCoordParHeure(piHeure) ;
		if (loTranche) {
			return loTranche.getCoordParHeure(piHeure) ;
		} else {
			console.log("this.getTranche() a renvoyé NULL") ;
		}
// LG 20190824 fin
	},
	getHeureParCoord: function (piPosition, pbArrondir) {
		var loTranche = this.getTranche() ;
// LG 20190824 début
// 		return loTranche.getHeureParCoord(piPosition, pbArrondir) ;
		if (loTranche) {
			return loTranche.getHeureParCoord(piPosition, pbArrondir) ;
		} else {
			// Se produit sur les séaces sélectionnées
			// Quand on a sélectionné une séance puis changé les dates et rechargé, puis qu'on déplace la nouvelle séance sélectionnée
			// Probablement que l'objet séancesélectionnée initiale est toujours vivante et observée
			console.log("this.getTranche() a renvoyé NULL") ;
		}
// LG 20190824 fin
	},

	estSéanceVierge: function () {
		return (this.sIdSeance.substr(0, 3) == "S0_") ;
	},

	bidonPourFinDeClasse: 0
});
