/*
 * Ce fichier contient les fonctions communes du projet POO_Planning 
 */

/**
 * minToHeures
 * Renvoie l'heure passée en paramètre sous la forme d'une chaine de caractères.
 * @param {number} temps - Une heure, écrite en minutes
 * @param {plForce2CarPourHeure} true pour que les heures soient toujours sur deux caractères
 * @return {number} tempsHeure+tempsMinutes; - L'heure renvoyé au format hhHmm
 */
function minToHeures(temps, plForce2CarPourHeure) {
    //Math floor renvoie la partie entière d'un nomnbre, 
    //cela permet d'arrondir au nombre inférieur le plus proche et au nombre supérieur le plus proche.
    var tempsHeure = Math.floor(temps / 60);
    var tempsMinutes = temps % 60 ;
    //sprintf est une fonction rajouté, dans le fichier lib/sprintf. Elle est similaire à celle en PHP ou C
    //Ici, elle est utilisé pour avoir deux décimales
    tempsMinutes = sprintf('%02d', tempsMinutes);
	if (!plForce2CarPourHeure)
		return tempsHeure + 'h' + tempsMinutes;
	else
		return (tempsHeure < 10 ? "0" : "") + tempsHeure + 'h' + tempsMinutes;
	
}

/**
 * HeuresToMin
 * Renvoie l'heure passée en paramètre sous la forme d'un nombre de minutes
 * @param {heure} Une heure, écrite en hhHmm ou hh:mm
 */
function HeureToMin(heure) {
	if (heure.includes(":")) {
		var la = heure.split(':')
	} else if (heure.includes("h")) {
		var la = heure.split('h')
	} else {
		console.error("cas non prévu") ;
	}
	return parseInt(la[0]) * 60 + parseInt(la[1]) ;
}

// Arrondir l'heure à Xmin près
// piHeure		: heure en minutes
//(piPas)		: pas d'arrondi en minutes (Dft : 5 minutes)
//(pbFloor)		: true -> le calcul renvoie la valeur inférieure de la plage du pas
//				: false -> le calcul renvoie la valeur haute ou basse selon qu'on est au-dessous ou au-dessus du milieu du pas
function arrondirHeure(piHeure, piPas, pbFloor) {
	piPas = piPas?piPas:5 ;
	if (pbFloor) return Math.floor(parseInt(piHeure) / piPas) * piPas ;
	else return Math.round(parseInt(piHeure) / piPas) * piPas ;
} ;

			
/*#########################################
 Fonctions retournant les valeurs de la position de la séance, sous la forme d'une chaine de caractères. 
 * getDivLeft Renvoie une chaine indiquant la position left, en pixel, de la div fournie
 * @return {String} - chaine sous la forme "top : X"
 */
function getDivLeft(poDiv) {
	var lsDim, lsChaine ;
	if (poDiv.style.left != null) {
		lsChaine = poDiv.style.left;
		lsDim = lsChaine.replace('px', '');
	} else lsDim = "0" ;
	return lsDim ;
}
function getDivTop(poDiv) {
	var lsDim, lsChaine ;
	if (poDiv.style.top != null) {
		lsChaine = poDiv.style.top;
		lsDim = lsChaine.replace('px', '');
	} else lsDim = "0" ;
	return lsDim ;
}
function getDivBottom(poDiv) {
	var lsDim, lsChaine ;
	if (poDiv.style.bottom != null) {
		lsChaine = poDiv.style.bottom;
		lsDim = lsChaine.replace('px', '');
	} else lsDim = "0" ;
	return lsDim ;
}
function getDivWidth(poDiv) {
	var lsDim, lsChaine ;
	if (poDiv.style.width != null) {
		lsChaine = poDiv.style.width;
		lsDim = lsChaine.replace('px', '');
	} else lsDim = "0" ;
	return lsDim ;
}
function getDivHeight(poDiv) {
	var lsDim, lsChaine ;
	if (poDiv.style.height != null) {
		lsChaine = poDiv.style.height ;
		lsDim = lsChaine.replace('px', '') ;
	} else lsDim = "0" ;
	return lsDim ;
}

// AC 20121126
// Cette fonction stock dans un tableau les coordonnées de la souris lorsque l'on est dans une DIV, puis elle nous retourne les valeurs du tableau.
function getCoordsInDiv(e, div) {
    //get the position of the container, prototype functionality
	if (isPrototype()) {
		var containerLeft = Position.page($(div))[0];
		var containerTop = Position.page($(div))[1];
		
	} else {
		var pos = div.getBoundingClientRect() ;
		var containerLeft = pos.left ;
		var containerTop = pos.top ;
	}
	//get the mouse coordinates
    if (isPrototype()) {
		mouseX = Event.pointerX(e);
		mouseY = Event.pointerY(e);
	} else {
		mouseX = e.clientX;
		mouseY = e.clientY;
	}
    //calculate the absolute mouse position in the div,
    //by mouseposition minus left position of the container
    horizontalPosition = mouseX - containerLeft;
    verticalPosition = mouseY - containerTop;
	
    //use prototypes function to get the dimension
    //this is a VERY usefull function because it also checks for borders
//    containerDimensions = $(div).getDimensions();
	if (isPrototype()) var containerDimensions = $(div).getDimensions(); // dimensions actuelle de la div séance
	else var containerDimensions = {width: $(div).width(), height: $(div).height()}; // dimensions actuelle de la div séance
	
    height = containerDimensions.height;
    width = containerDimensions.width;
    //check if the mouse is out or inside the div
    //this if statement checks if the cursor is inside the div
    /* Dans notre cas d'utilisation, on sera toujours dans l'élement, puisque l'event est un onclick */
    /*if(horizontalPosition < 0 || verticalPosition < 0 || mouseX > (width + containerLeft) || mouseY > (height + containerTop) ){
     //just some testing output
     console.log('MOUSE OUT OF DIV mouseX:' + horizontalPosition + '-- mouseY:' + verticalPosition);
     }else{
     //just some testing output
     console.log('MOUSE IN DIV mouseX:' + horizontalPosition + '-- mouseY:' + verticalPosition);
     }*/
    var arrayRetour = {
        mouseX: horizontalPosition,
        mouseY: verticalPosition
    }
    return arrayRetour;
}

/**
 *rtvIdDivParenteDeLaClasse
 * Permet de connaitre l'indentifiant de la séance correspondant à l'élement HTML
 * Retourne false si on a cliqué ailleurs que dans une div
 * @param {Element HTML} element - Chemin et nom du fichier à récuperer et interpreter.
 * @param {String} classe - la classe CSS à attraper.
 * @return {String} id - l'indentifiant de la div correspondant, ou false.
 */
function rtvIdDivParenteDeLaClasse(element, classe) {
    var parent = element	/*document.getElementById(element.id)*/;
    var id = false;
    if (parent.className == classe) {
		// L'élément fourni est déja de la classe demandée
        id = parent.id;
    } else {
		// L'élément fourni n'est pas de la classe demandée
		// Chercher parmi les parents
// LG 20160905 old        while (true) {
        while (parent.parentNode) {
            parent = parent.parentNode ;
            if (parent.className == classe) {
                id = parent.id;
                break;
            }
        }
    }
    return id;
}

/**
 *getAjust
 * Renvoie une valeur, en pixel, de l'ajustement (bordure et la marge)
 * @param {Element HTML} element - élement (souvent une Div) dont on souhaite connaitre la taille de l'ajustement
 * @return {Number} sizeAdjust - valeur calculée de l'ajustement
 */
function getAjust(element)
{
    /* Border adds to dimensions */
    var borderStyle = element.style.border;
    var borderSize;
    if (borderStyle != null)
    {
        // borderStyle = "red 2px solid"
        // Certaines versions d'IE
        if (!borderStyle.split(' ')[1].replace(/[^0-9]/g, '') == 0)
        {
            borderSize = borderStyle.split(' ')[1].replace(/[^0-9]/g, '');
        }
        // borderStyle = "2px solid red"
        // La plupart des navigateurs
        else if (!borderStyle.split(' ')[0].replace(/[^0-9]/g, '') == 0)
        {
            borderSize = borderStyle.split(' ')[0].replace(/[^0-9]/g, '');
        }
        // borderStyle = "2px solid red"
        // Cas par default, normalement ne doit pas être appelé. 
        else
        {
            borderSize = 0;
            myAlert('split : ' + borderStyle.split(' ')[1].replace(/[^0-9]/g, ''));
        }
    }
    else
    {
        borderSize = 0;
    }
    /* Padding adds to dimensions */
    var paddingStyle = element.style.padding;
    var paddingSize;
    if (paddingStyle != null)
    {
        paddingSize = paddingStyle.split(' ')[0].replace(/[^0-9]/g, '');
    }
    else
    {
        paddingSize = 0;
    }
    /* Add things up that change dimensions */
    var sizeAdjust = (borderSize * 2) + (paddingSize * 2);
    /* Update container's size */
    return sizeAdjust;
}

// poRes : objet qui a obligatoirement une propriété sId_Res
// La fonction renseigne les propriétés (facultatives) sType_Res et iId_Res de l'objet fourni
function decomposeRessource(poRes) {
	poRes.sType_Res = poRes.sId_Res.substr(0, 1) ;
	poRes.iId_Res = parseInt(poRes.sId_Res.substr(1, poRes.sId_Res.length - 1)) ;
	return poRes ;
}
