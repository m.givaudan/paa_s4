/**###############################################
*      CLASSE zoneInterdite
*
* @param {Array} aParaSeance - Utilisé pour rassembler les paramètres, pour faciliter la lecture/écriture du code
*
* Ce tableau de paramètres contient les paramètres suivants :
* @param {String} sIdZone - Identifiant de la Ressource
* @param {String} cLibelle - Nom complet de la Ressource
* @param {String} cInfobulle - Message à afficher lors du passage de la souris sur la séance 
* @param {String} dDate - Date de la séance 
* @param {String} iDebut - Heure du début de la seance, en minutes 
* @param {String} iFin - Heure du début de la seance, en minutes 
* @param {String} cCouleur - Couleur d'affichage de la séance (Inutilisé ?)
###############################################*/
//var zoneInterdite = Class.create({
//    initialize: function (aParaSeance, psIdRessource, psIdTranche/*psIdDivTranche*/) {
var cZoneInterdite = Class.extend({
    init: function (aParaSeance, psIdRessource, psIdTranche) {

		this.sIdJour = aParaSeance.sIdJour ;
		this.sIdZoneInterdite = aParaSeance.sIdZoneInterdite ;
		this.sIdRessource = psIdRessource ;
//		this.sIdDivTranche = psIdDivTranche ;
		this.sIdTranche = psIdTranche ;
//        this.sIdZone = aParaSeance.sIdSeance;
        this.iDebut = aParaSeance.iDebut;
        this.iFin = aParaSeance.iFin;
        this.semaine = aParaSeance.sSemaine;
        if (!aParaSeance.cLibelle && !aParaSeance.cInfobulle) {
            this.cLibelle = minToHeures(this.iDebut) + " à " + minToHeures(this.iFin);
            this.cInfobulle = minToHeures(this.iDebut) + " à " + minToHeures(this.iFin);
        } else if (!aParaSeance.cLibelle) {
            this.cLibelle = aParaSeance.cInfobulle;
            this.cInfobulle = aParaSeance.cInfobulle;
        } else if (!aParaSeance.cInfobulle) {
            this.cLibelle = aParaSeance.cLibelle;
            this.cInfobulle = aParaSeance.cLibelle;
        } else {
            this.cLibelle = aParaSeance.cLibelle;
            this.cInfobulle = aParaSeance.cInfobulle;
        }
        if (aParaSeance.dDate) this.dDate = aParaSeance.dDate;
        else this.dDate = "Date indéfinie";
		this.vertical = aParaSeance.vertical ;
    },
	
	// Récupérer l'Id de la div qui représente cette zone interdite pour la tranche et la ressource fournie 
    getIdDiv: function () {
		return window.oPlanning.oStructure.getIdDivZoneInterdite(this.sIdTranche/*sIdDivTranche*/, this.sIdRessource, this.sIdZoneInterdite) ;
    },
    getIdDivRessource: function () {
		return window.oPlanning.oStructure.getIdDivRessource(this.sIdRessource, this.sIdJour, this.sIdTranche/*sIdDivTranche*/) ;
    },
    getDivRessource: function () {
		return document.getElementById(this.getIdDivRessource()) ;
    },
	getTranche: function () {
		return window.oPlanning.oStructure.getTrancheAvecDiv(this.getIdDivTranche()) ;
	},
    getIdDivTranche: function () {
		return window.oPlanning.oStructure.getIdDivTranche(this.sIdTranche/*sIdDivTranche*/, this.sIdJour) ;
    },
    getDivTranche: function () {
		return document.getElementById(this.getIdDivTranche()) ;
    },
			
	// Afficher la séance
	afficher: function () {
		var loDivRessource = this.getDivRessource() ;
		var lsHTML = this.rtvHTML(loDivRessource) ;
		loDivRessource.innerHTML += lsHTML ;
	},
	
    rtvHTML: function (poDivRessource) {
		var liBordure = window.oPlanning.oCst.DimBordureZoneInterdite ;
//		var loRessource = this.getDivRessource() ;
//		var loTranche = window.oPlanning.oStructure.getTrancheAvecDiv(this.getIdDivTranche()) ;
		var loTranche = this.getTranche() ;
/*		if (this.vertical) {
			liWidth = loDivRessource.offsetWidth - 3;
			liTop = loTranche.getCoordParHeure(this.iDebut);
			liBot = loTranche.getCoordParHeure(this.iFin);
			liHeight = liBot - liTop ;
			liHeight = liHeight - liBordure * 2 ;
			liLeft = 0 ;
			lcPos = "absolute" ;
		} else {
			var liMargeBouton = (loDivRessource.sIdTranche == "0")?15:0;
			liHeight = loDivRessource.offsetHeight - 3;
			liLeft = loTranche.getCoordParHeure(this.iDebut);
			liLeft = liLeft + liMargeBouton;
			liRight = loTranche.getCoordParHeure(this.iFin);
			liRight = liRight - liLeft;
			liWidth = liRight - liMargeBouton;
			liWidth = liWidth - liBordure * 2;
			liTop = 0 ;
			lcPos = "relative" ;
		}
*/
		var pos = loTranche.getPositionSéance(poDivRessource, this.iDebut, this.iFin, liBordure) ;
		var liWidth, liLeft, liRight, liHeight, liTop, liBottom ;
		liWidth = pos.iWidth ;
		liLeft = pos.iLeft ;
		liRight = pos.iRight ;
		liHeight = pos.iHeight ;
		liTop = pos.iTop ;
		liBottom = pos.iBottom ;

		var lcHTML = "<div id='" + this.getIdDiv() + "' "
					+ "class='zoneInterdite' "
					+ "title='" + this.cLibelle + "' "
					+ "draggable='false' "
					+ "style='"
						+ "border-width : " + liBordure + "px; "
						+ "width: " + liWidth + "px; "
						+ "height: " + liHeight + "px; "
						+ "left: " + liLeft + "px; "
						+ "top: " + liTop + "px; "
				+ "'>"
				+ "<table class = 'zoneInterditeContenu TablePourCentrage'>"
				+ "<tr><td>" + this.cLibelle + "</td></tr>"
				+ "</table>"
				+ "</div>";
		
		return lcHTML ;
		
    }
});
