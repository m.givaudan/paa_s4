// Planning_sideBarConfig_2.js
// Définition du menu latéral de configuration du planning
// MG 20191211

!function ($) {

    $.fn.planning_sideBarConfig = function () {
        this.addClass("navbar navbar-m2p sidebar d-flex align-items-start p-0 m-0 z-index");
        var currentSelection = this;
        var currentElement = this[0];

        // Objet contrôleur
        oSideBar = {};
        oSideBar.oControleur = {
            thisSideBar: currentElement
            , bAuto: currentSelection.data("auto")
            , bAuto: false
            , bModifié: false
            , bOuvert: false
                    // Fin de l'auto-include de la sideBar
            , onSideBarLoaded: function (psIdDiv, pbOK, pvDétails) {
                // Renseigner les valeurs des inputs
                $("#selectStyleAffichage").val(oSideBar.oData.cStyleAffichage);
                $("#selectLstJoursAffichage").val(oSideBar.oData.cLstJoursAffichage);
//				$("#selectTypeHoraire").val(oSideBar.oData.cTypeHoraire) ;
                $("#selectTypeAffichageJours").val(oSideBar.oData.cTypeAffichageJours);
                $("#selectOrientation").val(oSideBar.oData.cOrientation);
                $("#selectTypeCouleur").val(oSideBar.oData.cTypeCouleur);
                $("#txtDureeDftSeance").val(oSideBar.oData.nDureeDftSeance);
                $("#selectTypeArrondi").val(oSideBar.oData.cTypeArrondi);
                $("#chkDisposeChronologiquement").prop("checked", oSideBar.oData.bDisposeChronologiquement);
                ;
                $("#chkSepareRessources").prop("checked", oSideBar.oData.bSepareRessources);
                ;
                $("#chkDecoupeSeances").prop("checked", oSideBar.oData.bDecoupeSeances);
                ;
                $("#chkAfficheAnomaliesToutes").prop("checked", oSideBar.oData.bAfficheAnomaliesToutes);
                ;
                $("#chkAfficheAnomaliesSéanceActive").prop("checked", oSideBar.oData.bAfficheAnomaliesSéanceActive);
                ;
                $("#chkAfficheAnomaliesPlacement").prop("checked", oSideBar.oData.bAfficheAnomaliesPlacement);
                ;

                if (oSideBar.oControleur.bAuto) {
                    // Activer le redimensionnement auto au survol par la souris
                    currentSelection.on('mouseover', oSideBar.oControleur.montreSideBarSiAuto);
                    currentSelection.on('mouseleave', oSideBar.oControleur.masqueSideBarSiAuto);
                } else {
                    // / Activer le redimensionnement auto sur clic
                    $('body').find(".config-btn").on('click', oSideBar.oControleur.inverseSideBar);
                }
                // Activer les gestionnaires d'événements internes
                /*
                 $("#orientationHorizontal").on('click', function() {
                 oSideBar.oControleur.onChangeOrientation("horizontal") ;
                 });
                 $("#orientationVertical").on('click', function() {
                 oSideBar.oControleur.onChangeOrientation("vertical") ;
                 });
                 */
                $("#selectStyleAffichage").on('change', function () {
                    var lvNewVal = $("#selectStyleAffichage").val();
                    oSideBar.oControleur.onChangeStyleAffichage(lvNewVal);
                });
                $("#selectLstJoursAffichage").on('change', function () {
                    var lvNewVal = $("#selectLstJoursAffichage").val();
                    oSideBar.oControleur.onChangeLstJoursAffichage(lvNewVal);
                });
//				$("#selectTypeHoraire").on('change', function() {
//					var lvNewVal = $("#selectTypeHoraire").val() ;
//					oSideBar.oControleur.onChangeTypeHoraire(lvNewVal) ;
//				});
                $("#selectTypeAffichageJours").on('change', function () {
                    var lvNewVal = $("#selectTypeAffichageJours").val();
                    oSideBar.oControleur.onChangeTypeAffichageJours(lvNewVal);
                });
                $("#selectOrientation").on('change', function () {
                    var lvNewVal = $("#selectOrientation").val();
                    oSideBar.oControleur.onChangeOrientation(lvNewVal);
                });
                $("#txtDureeDftSeance").on('change', function () {
                    var lvNewVal = $("#txtDureeDftSeance").val();
                    oSideBar.oControleur.onChangeDureeDftSeance(lvNewVal);
                });
                $("#selectTypeArrondi").on('change', function () {
                    var lvNewVal = $("#selectTypeArrondi").val();
                    oSideBar.oControleur.onChangeTypeArrondi(lvNewVal);
                });

                // Bouton de validation (renommé pour éviter d'avoir des doublons d'Id dans la page globale)
                var lsIdcmdValider = oSideBar.oControleur.thisSideBar.id + "_cmdValider";
                $("#cmdValider").attr("id", lsIdcmdValider);
                $("#" + lsIdcmdValider).on('click', function () {
                    oSideBar.oControleur.onValide();
                });

                $('.stopclicpropagation').click(function (e) {
                    e.stopPropagation(); //This will prevent the event from bubbling up and close the dropdown when you type/click on text boxes.
                });

//				if (oSideBar.oControleur.bAuto)
                oSideBar.oControleur.masqueSideBar();
//				else
//					oSideBar.oControleur.montreSideBar() ;
            }

            // Afficher/montrer la scrollbar
            , montreSideBarSiAuto: function () {
                if (oSideBar.oControleur.bAuto)
                    oSideBar.oControleur.montreSideBar();
            }
            , masqueSideBarSiAuto: function () {
                if (oSideBar.oControleur.bAuto)
                    oSideBar.oControleur.masqueSideBar();
            }
            , inverseSideBar: function () {
                if (oSideBar.oControleur.bOuvert)
                    oSideBar.oControleur.masqueSideBar();
                else
                    oSideBar.oControleur.montreSideBar();
            }
            , montreSideBar: function () {
                // Redimensionner la sideBar
                $("#Planning").css("margin-left", "0px");
                $("#" + oSideBar.oControleur.thisSideBar.id)
                        .css("margin-left", "0px")
                        .css("overflow-x", "hidden")
                        .css("overflow-y", "auto");

                //MG 20191211
                //Afficher les options
                $(".showoption").attr("style", "display:true;");

                // Avertir l'environnement
                var event = jQuery.Event("onMontre");
                $("#" + oSideBar.oControleur.thisSideBar.id).trigger(event);

                // Forcer le rafraichissement correct (sinon la place des scrollbars pose pb)
                forceRefreshElement("#" + oSideBar.oControleur.thisSideBar.id);

                oSideBar.oControleur.bModifié = false;
                oSideBar.oControleur.bOuvert = true;
            }
            , masqueSideBar: function () {
                // Redimensionner la sideBar
                $("#Planning").css("margin-left", "-20px");
                $("#" + oSideBar.oControleur.thisSideBar.id)
                        .css("margin-left", "-260px")
                        .css("overflow-x", "hidden")
                        .css("overflow-y", "hidden");

                //MG 20191211
                //Cacher les options
                $(".showoption").attr("style", "display:none;");
                $(".showonchange").attr("style", "display:none;");

                // LG 20170515
                // Fermer toutes les sous-options
                $("#" + oSideBar.oControleur.thisSideBar.id).find('.open').removeClass("open");

                // Avertir l'environnement
                var event = jQuery.Event("onMasque");
                $("#" + oSideBar.oControleur.thisSideBar.id).trigger(event, oSideBar.oData);

                // Forcer le rafraichissement correct (sinon la place des scrollbars pose pb)
                forceRefreshElement("#" + oSideBar.oControleur.thisSideBar.id);
                oSideBar.oControleur.bOuvert = false;
            }

            // Lors d'une action de l'utilisateur sur la configuration
            , onChangeConfig() {
                oSideBar.oControleur.bModifié = true;
                var event = jQuery.Event("onChangeConfig");
                $("#" + oSideBar.oControleur.thisSideBar.id).trigger(event);
//MG Modification 20200303 Début                               
//				$(".showonchange").attr("style", "display:true;");
                oSideBar.oControleur.onValide();
//MG Modification 20200303 Fin 
            }
            , onValide() {
                oSideBar.oControleur.masqueSideBarSiAuto();
                if (oSideBar.oControleur.bModifié) {
                    var event = jQuery.Event("onValide");
                    $("#" + oSideBar.oControleur.thisSideBar.id).trigger(event, oSideBar.oData);
//MG Ajout 20200303 Début 
                    oSideBar.oControleur.masqueSideBar();
                }
            }
            , onChangeStyleAffichage: function (lvNewVal) {
                oSideBar.oData.cStyleAffichage = lvNewVal;
                oSideBar.oControleur.onChangeConfig();
            }
            , onChangeLstJoursAffichage: function (lvNewVal) {
                oSideBar.oData.cLstJoursAffichage = lvNewVal;
                oSideBar.oControleur.onChangeConfig();
            }
//			, onChangeTypeHoraire: function (lvNewVal) {
//				oSideBar.oData.cTypeHoraire = lvNewVal ;
//				oSideBar.oControleur.onChangeConfig() ;
//			}
            , onChangeTypeAffichageJours: function (lvNewVal) {
                oSideBar.oData.cTypeAffichageJours = lvNewVal;
                oSideBar.oControleur.onChangeConfig();
            }
            , onChangeOrientation: function (lvNewVal) {
                oSideBar.oData.cOrientation = lvNewVal;
                oSideBar.oControleur.onChangeConfig();
            }
            , onChangeDureeDftSeance: function (lvNewVal) {
                oSideBar.oData.nDureeDftSeance = lvNewVal;
                oSideBar.oControleur.onChangeConfig();
            }
            , onChangeTypeArrondi: function (lvNewVal) {
                oSideBar.oData.nDureeDftSeance = lvNewVal;
                oSideBar.oControleur.onChangeConfig();
            }
        };

        // Objet Data
//		, cTypeHoraire: "I"
        oSideBar.oData = {
//			vertical: false
            cOrientation: "H"
            , cStyleAffichage: "Semaine"
            , cLstJoursAffichage: "<Auto>"
            , cTypeAffichageJours: "N"
            , cTypeCouleur: "1"
            , nDureeDftSeance: 1
            , cTypeArrondi: "2"
            , bDisposeChronologiquement: true
            , bSepareRessources: true
            , bDecoupeSeances: false
            , bAfficheAnomaliesToutes: false
            , bAfficheAnomaliesSéanceActive: false
            , bAfficheAnomaliesPlacement: false
        };

        // Chargement du contenu de la sidebar
        return this.each(function () {
            includeHTML("../../Limoog/Planning/SideBarConfig/Planning_sideBarConfig.html", this.id, oSideBar.oControleur.onSideBarLoaded);
        });

    };

    // Auto-chargement de tout élément dont le data-role est "planning_sideBarConfig"
    $(function () {
        $("[data-role=planning_sideBarConfig]").planning_sideBarConfig();
    });

}(window.jQuery);

